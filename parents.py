#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import pywikibot
import urllib.parse
from wikidatafun import *
class parents:
    query = '''
    SELECT ?item ?itemLabel ?should_link_via_P40_to ?should_link_via_P40_toLabel
WHERE
{
	?should_link_via_P40_to wdt:P22 ?item .
#    ?item wdt:P27 wd:Q183.
	FILTER NOT EXISTS { ?item wdt:P40 ?should_link_via_P40_to } .
	SERVICE wikibase:label { bd:serviceParam wikibase:language "cs,sk,en" } .

}


    '''

    def run(self):
        url = 'https://query.wikidata.org/bigdata/namespace/wdq/sparql?query=%s' % (urllib.parse.quote(self.query))
        url = '%s&format=json' % (url)
        print(url)
        site = pywikibot.Site('wikidata', 'wikidata')
        repo = site.data_repository()
        sparql = getURL(url)
        json1 = loadSPARQL(sparql=sparql)
        print(json1)
        qlist = []
        for result in json1['results']['bindings']:
            try:
                arr = {}
                arr['item'] = result['item']['value'].split('/entity/')[1]
                arr['link'] = result['should_link_via_P40_to']['value'].split('/entity/')[1]
                if arr not in qlist:
                    qlist.append(arr)
            except IndexError as e:
                pass

        for q in qlist:
            try:
                item = pywikibot.ItemPage(repo, q['item'])
                link = pywikibot.ItemPage(repo, q['link'])
                new_claim = pywikibot.Claim(repo, 'P40')
                new_claim.setTarget(link)
                item.addClaim(new_claim)
            except Exception as e:
                pass

        print(qlist)

par = parents()
par.run()