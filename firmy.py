import sys
import csv
import pywikibot
import re
from logger import Logger
# from openpyxl import Workbook
# import unicodecsv as unicodecsv

class excel_semicolon(csv.Dialect):
    """Describe the usual properties of Excel-generated CSV files."""
    delimiter = ';'
    quotechar = '"'
    doublequote = True
    skipinitialspace = True
    lineterminator = '\r\n'
    quoting = csv.QUOTE_ALL

class firmy:


    def labelClean(self):
        sql = '''
        SELECT ?item ?label WHERE {
  ?item wdt:P31 wd:Q79007.
  ?item wdt:P17 wd:Q213.

               #FILTER (CONTAINS(?itemLabel,")"))
      ?item rdfs:label ?label .
  FILTER(CONTAINS (LCASE(?label), LCASE("("))).
  FILTER(lang(?label) = "en")

               }

        '''

        site = pywikibot.Site('cs', 'wikipedia')
        repo = site.data_repository()
        from pywikibot.data import sparql
        query_object = sparql.SparqlQuery()
        data = query_object.select(sql, full_data=True)
        if data:
            for r in data:
                item = pywikibot.ItemPage(repo, r['item'].getID())
                label = str(r['label'])
                regex = r"(.*) \((.*)\)"
                try:
                    matches = re.search(regex,label)

                    g = matches.groups()
                    newlabel = g[0]
                    labels = {}
                    labels.setdefault('labels', {}).update({
                        'en': {
                            'language': 'en',
                            'value': newlabel
                        }
                    })
                    labels.setdefault('aliases', {}).update({
                        'en': {
                            'language': 'en',
                            'value': label
                        },
                        'cs': {
                            'language': 'cs',
                            'value': label
                        }
                    })
                    self.user_edit_entity(item, labels, summary='correct en label czech streets (brackets out)')
                except Exception as e:
                    print(label)

                # self.logger.logComplete(ruian)
        sys.exit()

    def run(self):
        print('firmy')
        self.site = pywikibot.Site('cs', 'wikipedia')
        self.repo = self.site.data_repository()
        logger_name = "firmy"
        self.logger = Logger(logger_name + u'Fill', 'saved')

    def create_item_for_page(self, page=None, data=None, summary = None, repo=None):
        if not summary:
            summary = (u'Bot: New item with sitelink from %s'
                       % page.title(asLink=True, insite=self.repo))

        if data is None:
            data = {}
            data.setdefault('sitelinks', {}).update({
                page.site.dbName(): {
                    'site': page.site.dbName(),
                    'title': page.title()
                }
            })
            data.setdefault('labels', {}).update({
                page.site.lang: {
                    'language': page.site.lang,
                    'value': page.title()
                }
            })





        if (not page):
            pywikibot.output('Creating item')
            item = pywikibot.ItemPage(repo)
        else:
            pywikibot.output('Creating item for %s...' % page)
            item = pywikibot.ItemPage(page.site.data_repository())

        result = self.user_edit_entity(item, data, summary=summary)
        if result:
            return item
        else:
            return None

    def user_edit_entity(self, item, data=None, summary=None):
        """
        Edit entity with data provided, with user confirmation as required.

        @param item: page to be edited
        @type item: ItemPage
        @param data: data to be saved, or None if the diff should be created
          automatically
        @kwarg summary: revision comment, passed to ItemPage.editEntity
        @type summary: str
        @kwarg show_diff: show changes between oldtext and newtext (default:
          True)
        @type show_diff: bool
        @kwarg ignore_server_errors: if True, server errors will be reported
          and ignored (default: False)
        @type ignore_server_errors: bool
        @kwarg ignore_save_related_errors: if True, errors related to
          page save will be reported and ignored (default: False)
        @type ignore_save_related_errors: bool
        @return: whether the item was saved successfully
        @rtype: bool
        """
        return self._save_page(item, item.editEntity, data)

    def _save_page(self, page, func, *args):
        """
        Helper function to handle page save-related option error handling.

        @param page: currently edited page
        @param func: the function to call
        @param args: passed to the function
        @param kwargs: passed to the function
        @kwarg ignore_server_errors: if True, server errors will be reported
          and ignored (default: False)
        @kwtype ignore_server_errors: bool
        @kwarg ignore_save_related_errors: if True, errors related to
        page save will be reported and ignored (default: False)
        @kwtype ignore_save_related_errors: bool
        @return: whether the page was saved successfully
        @rtype: bool
        """
        func(*args)
        return True

    def compareDatavalue(self, old_claim, new_value ):
        if ( old_claim._type != old_claim._type ):
            return False
        if ( old_claim._type == 'string' ):
            return old_claim.target_equals(new_value)
        if ( old_claim._type == 'quantity' ):
            return old_claim.target_equals(new_value)
        if ( old_claim._type == 'time' ):
            return old_claim.target_equals(new_value)
        if ( old_claim._type == 'globecoordinate' ):
            return old_claim.target_equals(new_value)
        if ( old_claim._type == 'monolingualtext' ):
            return old_claim.target_equals(new_value)
        if ( old_claim._type == 'wikibase-item' ):
            return old_claim.target_equals(new_value)

    def load_csv(self, list_only = False):
        with open('firma_utf.csv', newline='',encoding='utf-8-sig') as csvfile:
            # csv.register_dialect('excel_semicolon')
            reader = csv.DictReader(csvfile,dialect=excel_semicolon)
            for row in reader:
                print(row)
                if not self.logger.isCompleteFile(row['P4156'].replace('"','')):
                    if row['qid'] == '':
                        print('new')
                        data = {}

                        data.setdefault('labels', {}).update({
                            'en': {
                                'language': 'en',
                                'value': row['Len']
                            },
                            'cs': {
                                'language': 'cs',
                                'value': row['Lcs']
                            },
                        })

                        data.setdefault('descriptions', {}).update({
                            'en': {
                                'language': 'en',
                                'value': row['Den']
                            },
                            'cs': {
                                'language': 'cs',
                                'value': row['Dcs']
                            },
                        })

                        data.setdefault('aliases', {}).update({
                            'cs': {
                                'language': 'cs',
                                'value': row['Acs']
                            },
                        })

                        try:
                            page = self.create_item_for_page(page=None, data=data, repo=self.repo,
                                                             summary=u'creating czech company item')

                            sources = []
                            source = pywikibot.Claim(self.repo, 'P854', isReference=True)
                            source.setTarget(row['S854'].replace('"', ''))
                            sources.append(source)

                            # LEI
                            if (row['P1278'] == ''):
                                pass
                            else:
                                try:
                                    del (old_claims)
                                except UnboundLocalError:
                                    old_claims = []
                                try:
                                    old_claims = page.claims['P1278']
                                except KeyError:
                                    old_claims = []
                                    exist = False
                                lei = pywikibot.Claim(self.repo, 'P1278')
                                exist = False
                                for claim in old_claims:
                                    exist = claim.target_equals(row['P1278'].replace('"', ''))

                                if not exist:
                                    lei.setTarget(row['P1278'].replace('"', ''))
                                    page.addClaim(lei)
                                    lei.addSource(source)

                            if (row['P1448'] == ''):
                                pass
                            else:
                                del (old_claims)
                                try:
                                    old_claims = page.claims['P1448']
                                except KeyError:
                                    old_claims = []
                                    exist = False
                                nazev = pywikibot.Claim(self.repo, 'P1448')
                                arr = row['P1448'].split(':', maxsplit=1)
                                if arr[1][0] == '"' and arr[1][1] == '"':
                                    monoling_nazev = pywikibot.WbMonolingualText(arr[1].replace('""', '"'), arr[0])
                                else:
                                    monoling_nazev = pywikibot.WbMonolingualText(arr[1].replace('"', ''), arr[0])

                                exist = False
                                for claim in old_claims:
                                    exist = claim.target_equals(monoling_nazev)

                                if not exist:
                                    nazev.setTarget(monoling_nazev)
                                    page.addClaim(nazev)
                                    nazev.addSource(source)

                            if (row['P1454'] == ''):
                                pass
                            else:
                                del (old_claims)
                                try:
                                    old_claims = page.claims['P1454']
                                except KeyError:
                                    old_claims = []
                                    exist = False
                                forma = pywikibot.Claim(self.repo, 'P1454')
                                exist = False
                                for claim in old_claims:
                                    exist = claim.target_equals(row['P1454'])

                                if not exist:
                                    forma.setTarget(pywikibot.ItemPage(self.repo, row['P1454']))
                                    page.addClaim(forma)
                                    forma.addSource(source)

                            if (row['P159'] == ''):
                                pass
                            else:
                                del (old_claims)
                                try:
                                    old_claims = page.claims['P159']
                                except KeyError:
                                    old_claims = []
                                    exist = False
                                sidlo = pywikibot.Claim(self.repo, 'P159')
                                exist = False
                                for claim in old_claims:
                                    exist = claim.target_equals(row['P159'])

                                if not exist:
                                    sidlo.setTarget(pywikibot.ItemPage(self.repo, row['P159']))
                                    page.addClaim(sidlo)
                                    sidlo.addSource(source)
                                    if (row['qal131'] == ''):
                                        pass
                                    else:
                                        adminjednotka = pywikibot.Claim(self.repo, 'P131')
                                        adminjednotka.setTarget(pywikibot.ItemPage(self.repo, row['qal131']))
                                        sidlo.addQualifier(adminjednotka)

                                    if (row['qal131_2'] == ''):
                                        pass
                                    else:
                                        adminjednotka = pywikibot.Claim(self.repo, 'P131')
                                        adminjednotka.setTarget(pywikibot.ItemPage(self.repo, row['qal131_2']))
                                        sidlo.addQualifier(adminjednotka)

                                    if (row['qal281'] == ''):
                                        pass
                                    else:
                                        psc = pywikibot.Claim(self.repo, 'P281')
                                        psc.setTarget(row['qal281'].replace('"', ''))
                                        sidlo.addQualifier(psc)

                                    if (row['qal969'] == ''):
                                        pass
                                    else:
                                        adresa = pywikibot.Claim(self.repo, 'P281')
                                        adresa.setTarget(row['qal969'].replace('"', ''))
                                        sidlo.addQualifier(adresa)

                            if (row['P17'] == ''):
                                pass
                            else:
                                del (old_claims)
                                try:
                                    old_claims = page.claims['P17']
                                except KeyError:
                                    old_claims = []
                                    exist = False
                                stat = pywikibot.Claim(self.repo, 'P17')
                                exist = False
                                for claim in old_claims:
                                    exist = claim.target_equals(row['P17'])

                                if not exist:
                                    stat.setTarget(pywikibot.ItemPage(self.repo, row['P17']))
                                    page.addClaim(stat)
                                    stat.addSource(source)

                            if (row['P31'] == ''):
                                pass
                            else:
                                del (old_claims)
                                try:
                                    old_claims = page.claims['P31']
                                except KeyError:
                                    old_claims = []
                                    exist = False
                                instance = pywikibot.Claim(self.repo, 'P31')
                                exist = False
                                for claim in old_claims:
                                    exist = claim.target_equals(row['P31'])

                                if not exist:
                                    instance.setTarget(pywikibot.ItemPage(self.repo, row['P31']))
                                    page.addClaim(instance)
                                    instance.addSource(source)

                            if (row['P4156'] == ''):
                                pass
                            else:
                                del (old_claims)
                                try:
                                    old_claims = page.claims['P4156']
                                except KeyError:
                                    old_claims = []
                                    exist = False
                                ico = pywikibot.Claim(self.repo, 'P4156')
                                exist = False
                                for claim in old_claims:
                                    exist = claim.target_equals(row['P4156'].replace('"', ''))

                                if not exist:
                                    ico.setTarget(row['P4156'].replace('"', ''))
                                    page.addClaim(ico)
                                    ico.addSource(source)

                            self.logger.logComplete(row['P4156'].replace('"', ''))

                        except pywikibot.OtherPageSaveError as e:
                            self.logger.logError(row['Lcs'])
                            self.logger.logComplete(row['P4156'].replace('"', ''))
                            # page = pywikibot.ItemPage(self.repo, row['qid'])


                    else:
                        page = pywikibot.ItemPage(self.repo,row['qid'])
                        page.get()
                        aliases = {'cs':[row['Acs']]}
                        page.editAliases(aliases)
                        descriptions = {'cs':row['Dcs'], 'en':row['Den']}
                        page.editDescriptions(descriptions)
                        labels = {'cs':row['Lcs'], 'en':row['Len']}
                        page.editLabels(labels)

                        sources = []
                        source = pywikibot.Claim(self.repo, 'P854', isReference=True)
                        source.setTarget(row['S854'].replace('"',''))
                        sources.append(source)

                        # LEI
                        if (row['P1278'] == ''):
                            pass
                        else:
                            try:
                                del(old_claims)
                            except UnboundLocalError:
                                old_claims = []
                            try:
                                old_claims = page.claims['P1278']
                            except KeyError:
                                old_claims = []
                                exist = False
                            lei = pywikibot.Claim(self.repo, 'P1278')
                            exist = False
                            for claim in old_claims:
                                exist = claim.target_equals(row['P1278'].replace('"', ''))

                            if not exist:
                                lei.setTarget(row['P1278'].replace('"',''))
                                page.addClaim(lei)
                                lei.addSource(source)

                        if (row['P1448'] == ''):
                            pass
                        else:
                            del(old_claims)
                            try:
                                old_claims = page.claims['P1448']
                            except KeyError:
                                old_claims = []
                                exist = False
                            nazev = pywikibot.Claim(self.repo, 'P1448')
                            arr = row['P1448'].split(':',maxsplit=1)
                            if arr[1][0] == '"' and arr[1][1] == '"':
                                monoling_nazev = pywikibot.WbMonolingualText(arr[1].replace('""', '"'), arr[0])
                            else:
                                monoling_nazev = pywikibot.WbMonolingualText(arr[1].replace('"',''),arr[0])

                            exist = False
                            for claim in old_claims:
                                exist = claim.target_equals(monoling_nazev)

                            if not exist:
                                nazev.setTarget(monoling_nazev)
                                page.addClaim(nazev)
                                nazev.addSource(source)

                        if (row['P1454'] == ''):
                            pass
                        else:
                            del (old_claims)
                            try:
                                old_claims = page.claims['P1454']
                            except KeyError:
                                old_claims = []
                                exist = False
                            forma = pywikibot.Claim(self.repo, 'P1454')
                            exist = False
                            for claim in old_claims:
                                exist = claim.target_equals(row['P1454'])

                            if not exist:
                                forma.setTarget(pywikibot.ItemPage(self.repo, row['P1454']))
                                page.addClaim(forma)
                                forma.addSource(source)

                        if (row['P159'] == ''):
                            pass
                        else:
                            del (old_claims)
                            try:
                                old_claims = page.claims['P159']
                            except KeyError:
                                old_claims = []
                                exist = False
                            sidlo = pywikibot.Claim(self.repo, 'P159')
                            exist = False
                            for claim in old_claims:
                                exist = claim.target_equals(row['P159'])

                            if not exist:
                                sidlo.setTarget(pywikibot.ItemPage(self.repo, row['P159']))
                                page.addClaim(sidlo)
                                sidlo.addSource(source)
                                if (row['qal131'] == ''):
                                    pass
                                else:
                                    adminjednotka = pywikibot.Claim(self.repo, 'P131')
                                    adminjednotka.setTarget(pywikibot.ItemPage(self.repo, row['qal131']))
                                    sidlo.addQualifier(adminjednotka)

                                if (row['qal131_2'] == ''):
                                    pass
                                else:
                                    adminjednotka = pywikibot.Claim(self.repo, 'P131')
                                    adminjednotka.setTarget(pywikibot.ItemPage(self.repo, row['qal131_2']))
                                    sidlo.addQualifier(adminjednotka)

                                if (row['qal281'] == ''):
                                    pass
                                else:
                                    psc= pywikibot.Claim(self.repo, 'P281')
                                    psc.setTarget(row['qal281'].replace('"',''))
                                    sidlo.addQualifier(psc)

                                if (row['qal969'] == ''):
                                    pass
                                else:
                                    adresa= pywikibot.Claim(self.repo, 'P281')
                                    adresa.setTarget(row['qal969'].replace('"',''))
                                    sidlo.addQualifier(adresa)



                        if (row['P17'] == ''):
                            pass
                        else:
                            del (old_claims)
                            try:
                                old_claims = page.claims['P17']
                            except KeyError:
                                old_claims = []
                                exist = False
                            stat = pywikibot.Claim(self.repo, 'P17')
                            exist = False
                            for claim in old_claims:
                                exist = claim.target_equals(row['P17'])

                            if not exist:
                                stat.setTarget(pywikibot.ItemPage(self.repo, row['P17']))
                                page.addClaim(stat)
                                stat.addSource(source)

                        if (row['P31'] == ''):
                            pass
                        else:
                            del (old_claims)
                            try:
                                old_claims = page.claims['P31']
                            except KeyError:
                                old_claims = []
                                exist = False
                            instance = pywikibot.Claim(self.repo, 'P31')
                            exist = False
                            for claim in old_claims:
                                exist = claim.target_equals(row['P31'])

                            if not exist:
                                instance.setTarget(pywikibot.ItemPage(self.repo, row['P31']))
                                page.addClaim(instance)
                                instance.addSource(source)

                        if (row['P4156'] == ''):
                            pass
                        else:
                            del (old_claims)
                            try:
                                old_claims = page.claims['P4156']
                            except KeyError:
                                old_claims = []
                                exist = False
                            ico = pywikibot.Claim(self.repo, 'P4156')
                            exist = False
                            for claim in old_claims:
                                exist = claim.target_equals(row['P4156'].replace('"',''))

                            if not exist:
                                ico.setTarget(row['P4156'].replace('"',''))
                                page.addClaim(ico)
                                ico.addSource(source)



                        self.logger.logComplete(row['P4156'].replace('"',''))

f = firmy()
f.labelClean()
# f.run()
# list = f.load_csv()
# print(f.csv)