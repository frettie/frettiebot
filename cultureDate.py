#!/usr/bin/python
# -*- coding: utf-8 -*-
import pywikibot
import sys
import re
import unicodecsv
from time import gmtime, strftime
import StringIO
import pandas as pd
import logging
import xml.etree.ElementTree as ET
import urllib2
from datetime import date
import time
from bs4 import BeautifulSoup


class Coordinate():
    pass

class NotHaveBothCoordinates(BaseException):
    def __str__(self):
        return self.message.encode('ascii', 'replace')
    pass

class NotHaveCoordinates(BaseException):
    def __str__(self):
        return self.message.encode('ascii', 'replace')
    pass

class Coordinator:

    printExceptions = False
    name = u'coordinator main'
    category = ''
    pages = list()
    coordinates = list()
    fileName = ''
    categoryWithCoords = ''

    def run(self):
        print "start"
        print strftime("%Y-%m-%d %H:%M:%S")
        print "--------"
        try:
            print "start getting pages from category"
            print strftime("%Y-%m-%d %H:%M:%S")
            print "--------"
            self.getPagesFromCategory()
            print "stop getting pages from category"
            print strftime("%Y-%m-%d %H:%M:%S")
            print "--------"
            print "start rotating pages and getting coordinates"
            print strftime("%Y-%m-%d %H:%M:%S")
            print "--------"
            self.pagesRotator()
            print "stop rotating pages and getting coordinates"
            print strftime("%Y-%m-%d %H:%M:%S")
            print "--------"
            print "start writing csv file"
            print strftime("%Y-%m-%d %H:%M:%S")
            print "--------"
            self.csvWrite()
            print "stop writing csv file"
            print strftime("%Y-%m-%d %H:%M:%S")
            print "--------"
        except ValueError as val:
            print val

        print "end"
        print strftime("%Y-%m-%d %H:%M:%S")
        print "--------"

    def pagesRotator(self):
        for page in self.pages:
            self.getCoordsListFromPage(page)

    def getCoordsListFromPage(self, page):
        print page.title()

    def setCategory(self, value):
        self.category = value

    def templateToDict(self, template):
        templateDict = dict()
        for param in template[1]:
            res = re.split('=', param)
            if len(res) > 1:
                templateDict[res[0]] = res[1]

        return templateDict

    def isCategoryWithCoords(self, template):
        res = re.split(':', template[0].title())
        if (res[1] == self.categoryWithCoords):
            return True
        else:
            return False

    def addCoordinate(self, lat, lon, name, pageName):
        coordinate = Coordinate()
        coordinate.lat = lat
        coordinate.lon = lon
        coordinate.name = name
        coordinate.pageName = pageName
        self.coordinates.append(coordinate)

    def getPagesFromCategory(self):
        site = pywikibot.Site('cs', 'wikipedia');
        if (self.category != ''):
            self.pages = pywikibot.Category(site, self.category).articles(recurse=4)
        else:
            raise ValueError('Category is not set')

    def csvWrite(self):
        with open(self.fileName, 'w') as csvfile:
            fieldnames = ['lat', 'lon', 'name', 'pageName']
            writer = unicodecsv.DictWriter(csvfile, fieldnames=fieldnames)
            writer.writeheader()
            for coordinate in self.coordinates:
                writer.writerow({'lat': coordinate.lat, 'lon': coordinate.lon, 'name': coordinate.name, 'pageName': coordinate.pageName})
                
    def getInfoFromPage(self, monumnetId, name):
        url = 'http://monumnet.npu.cz/pamfond/list.php?hledani=1&CiRejst=' + monumnetId
        
        
        page = urllib2.urlopen(url)

        soup = BeautifulSoup(page)
        
        table = soup.find("table", "info")
        #print table
        if table:
            for row in table.findAll("tr"):            
                head = row.findAll("th")
                cells = row.findAll("td")
                if (unicode(head[0].find(text=True)) == u'Památkou od :'):                
                    dateSplitted = unicode(cells[0].find(text=True)).split('.')
                    dateFrom = date(int(dateSplitted[2]), int(dateSplitted[1]), int(dateSplitted[0]))
                    print name + ' - ' + dateFrom.isoformat()
                    return dateFrom.isoformat()
                #print cells
        return None
        
    

class CultureMonument(Coordinator):

    category = u'Seznamy kulturních památek v Česku'
    categoryWithCoords = u'Památky v Česku'
    actualPage = ''
    fileName = 'culturemonuments.csv'

    def haveDate(self, templateDict):
        try:
            if (len(templateDict[u'Památkou_od']) > 0):
                return True
            else:
                return False
        except KeyError as ke:
            print ke
            return False    

    def getCoordsListFromPage(self, page):
        self.actualPage = page
        print self.actualPage.title()
        oldPos = 0
        text = page.get()
        entranceToProcess = False
        for template in page.templatesWithParams():
            if self.isCategoryWithCoords(template):
                dictatedTemplate = self.templateToDict(template)
                if not self.haveDate(dictatedTemplate):
                    #not have picture, continue pls                    
                    try:
                        dateFrom = self.getInfoFromPage(dictatedTemplate[u'Id_objektu'], dictatedTemplate[u'Název'])
                        #print type(page.get())
                        string = dictatedTemplate[u'Id_objektu']
                        if dateFrom:
                            
                            #string = u"table"
                            text = re.sub(u"(" + string + ")\n(.*)", string + u"\n" + u"|Památkou_od=" + dateFrom, text, flags=re.IGNORECASE) 

                        else:
                            dateFrom = u"1958-05-03"
                            text = re.sub(u"(" + string + ")\n(.*)", string + u"\n" + u"|Památkou_od=" + dateFrom, text, flags=re.IGNORECASE) 
                        entranceToProcess = True
                    except NotHaveBothCoordinates as nhbc:
                        if self.printExceptions:
                            print nhbc
                    except NotHaveCoordinates as nhc:
                        if self.printExceptions:
                            print nhc
        if (entranceToProcess):
            page.text = text
            page.save(comment=u'Robotické doplnění parametru Památkou od')
            
            #time.sleep(1)


coordinator = CultureMonument()
coordinator.run()
