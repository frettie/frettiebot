#!/usr/bin/python
# -*- coding: utf-8 -*-
import json
import re
import unicodecsv
import requests
import pywikibot

class excel_semicolon(unicodecsv.Dialect):
    """Describe the usual properties of Excel-generated CSV files."""
    delimiter = ';'
    quotechar = '"'
    doublequote = True
    skipinitialspace = False
    lineterminator = '\r\n'
    quoting = unicodecsv.QUOTE_MINIMAL

def prepare():
    with open('obce.csv', 'rb') as f:
        unicodecsv.register_dialect('excel_semicolon', excel_semicolon)
        reader = unicodecsv.reader(f, encoding='utf-8', dialect=excel_semicolon)
        # headers = next(reader)
        csv = list(reader)

    cities = {}
    site = pywikibot.Site('wikidata', 'wikidata')
    repo = site.data_repository()
    # try:
    text = ''
    for line in csv:
        matchKraj = re.search(r'\bkraj\b', line[0], re.IGNORECASE)
        matchMesto = re.search(r'\bměsto\b', line[0], re.IGNORECASE)
        matchMagistrat = re.search(r'\bmagistrát\b', line[0], re.IGNORECASE)
        matchMestys = re.search(r'\bměstys\b', line[0], re.IGNORECASE)
        matchObec = re.search(r'\bobec\b', line[0], re.IGNORECASE)
        matchObecni = re.search(r'\bobecní\b', line[0], re.IGNORECASE)
        matchCast = re.search(r'\bčást\b', line[0], re.IGNORECASE)
        matchCasti = re.search(r'\bčásti\b', line[0], re.IGNORECASE)
        matchObvod = re.search(r'\bobvod\b', line[0], re.IGNORECASE)
        matchObvodu = re.search(r'\bobvodu\b', line[0], re.IGNORECASE)
        matchUjezd = re.search(r'\bvojenský\b', line[0], re.IGNORECASE)
        if matchKraj:

            # print(line)
            pass
        elif matchMesto or matchMagistrat:
            # print(line)
            kod_mesta = line[4]
            urlmesto = "https://tools.wmflabs.org/hub/P782:CZ" + kod_mesta + "?lang=cs&format=json"
            response = requests.get(urlmesto)
            json_mesto = response.text
            data_mesto = json.loads(json_mesto)
            try:
                wd_mesto = data_mesto['origin']['qid']
                item = pywikibot.ItemPage(repo, wd_mesto)
                item.get()
                # print(item)
                checkAndAddProperty(site, item, line, 'P4156')
                checkAndAddProperty(site, item, line, 'P856')
                checkAndAddProperty(site, item, line, 'P968')
            except:
                pass



            pass
        elif matchObec or matchObecni:
            # print(line)
            pass
        elif matchMestys:
            # print(line)
            pass
        elif matchCast or matchCasti or matchObvod or matchObvodu:
            # print(line)
            pass
        elif matchUjezd:
            # print(line)
            pass
        else:
            print(line)
            pass
        # print(line)
    # print text
    # sys.exit()
    return cities

def checkAndAddProperty(site, item, line, property):
    prefix = ""
    suffix = ""
    if property == 'P4156':
        num_in_line = 1
    elif property == 'P856':
        num_in_line = 3
        suffix = "/"
    elif property == 'P968':
        num_in_line = 2
        prefix = "mailto:"
    if line[num_in_line] == '':
        return None
    try:
        if item.claims and item.claims[property]:
            # existuje ico property
            if item.claims[property][0].target == prefix+line[num_in_line]+suffix:
                # nedelej nic, ico je stejny v dokumentu i na wd
                # print('info je naplneno na wd a sedi s dokumentem')
                # print('info v dokumentu: ' + prefix+line[num_in_line]+suffix)
                # print('info na wd: ' + item.claims[property][0].target)
                pass
            else:
                print('lisi se info v dokumentu a na wd u property ' + property)
                print('info v dokumentu: ' + prefix+line[num_in_line]+suffix)
                print('info na wd: ' + item.claims[property][0].target)
    except KeyError:
        print('plnim property ' + property)
        print('info v dokumentu: ' + prefix + line[num_in_line]+suffix)
        claim = pywikibot.Claim(site, property)
        claim.setTarget(prefix + line[num_in_line])
        item.addClaim(claim)




prepare()