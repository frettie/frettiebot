#!/usr/bin/python
# -*- coding: utf-8 -*-

import json
import datetime



file = 'houredits.json'

with open(file,'r') as f:
    jsonfile = json.load(f)
    # print(jsonfile['items'][0]['results'])
    # date_time_str = '2018-06-29 08:15:27.243860'
    # date_time_obj =
    for line in jsonfile['items']:
        # print(line)
        datum = datetime.datetime.strptime(line['timestamp'], '%Y%m%d%H').strftime('%d. %m. %Y %H:%M')
        viewcount = int(line['views'])
        print(datum+';'+str(viewcount))
