import csv
import sys

import requests
import json
from typing import Dict, List, NoReturn, Any
from typing import Union
import os.path
from os import path
from shapely.geometry import shape, GeometryCollection

import pywikibot

from logger import Logger

class shape_to_commons:
    LICENCE:str = "CC0-1.0"
    SOURCES:str = "ČÚZK"
    CZ_DESCRIPTION:str = 'Mapa obce {city}, okr. {county}.'
    EN_DESCRIPTION:str = 'Map of municipality {city}, {county} District.'
    FILE_NAME:str = 'Data:Map of {city}, {county} District.map'
    ZOOM:int = 11

class village_shapes:

    shapes:list = []

    wd_items:dict = {}

    sources:list = []

    def __init__(self) -> NoReturn:
        print('shapes generator')
        self.logger = Logger(u'village_shape' + u'Fill', 'complete')
        self.site = pywikibot.Site('commons', 'commons')
        self.repo = self.site.data_repository()

        self.get_wd_items()
        counties = self.get_counties()
        self.get_shapes(counties)
        self.save_to_commons()
        # self.logger_to_lobendava()



    def logger_to_lobendava(self):
        for city in self.shapes:
            self.logger.logComplete(str(city['city_code']))
            if (city['city_name'] == 'Lobendava'):
                sys.exit()


    #commons
    #shapes@6gms9u8rlkl512ui8se2hspg5j93hqpc
    def save_to_commons(self) -> NoReturn:
        for city in self.shapes:
            commons_file_name = shape_to_commons.FILE_NAME.format(city=city['city_name'], county=city['county_name'])
            commons_file_data = self.create_file_text(city)
            page = pywikibot.page.Page(self.site, commons_file_name)
            page.text = commons_file_data
            self.site.editpage(page=page, summary='new shape for cz municipality')
            claim = pywikibot.Claim(self.repo, 'P3896')
            target = pywikibot.WbGeoShape(page)
            claim.setTarget(target)
            city['item'].addClaim(claim)
            source = pywikibot.Claim(self.repo, 'P248')
            source.setTarget(pywikibot.ItemPage(self.repo, 'Q12049125'))
            sources = []
            sources.append(source)
            claim.addSources(sources)
            self.logger.logComplete(str(city['city_code']))

    def create_file_text(self, city:dict):
        dict_text = {
            'license' : shape_to_commons.LICENCE,
            'description' : {
                'cs' : shape_to_commons.CZ_DESCRIPTION.format(city=city['city_name'], county=city['county_name']),
                'en' : shape_to_commons.EN_DESCRIPTION.format(city=city['city_name'], county=city['county_name']),
            },
            'sources' : shape_to_commons.SOURCES,
            'zoom' : shape_to_commons.ZOOM,
            'latitude' : city['centroid'].y,
            'longitude' : city['centroid'].x,
            'data' : city['city_geometry']
        }
        return json.dumps(dict_text)

    def get_counties(self) -> List:
        county_json_url = "http://ags.cuzk.cz/arcgis/rest/services/RUIAN/Prohlizeci_sluzba_nad_daty_RUIAN/MapServer/15/query?where=1%3D1&text=&objectIds=&time=&geometry=&geometryType=esriGeometryEnvelope&inSR=&spatialRel=esriSpatialRelIntersects&relationParam=&outFields=kod%2Cnazev&returnGeometry=false&returnTrueCurves=false&maxAllowableOffset=&geometryPrecision=&outSR=&having=&returnIdsOnly=false&returnCountOnly=false&orderByFields=&groupByFieldsForStatistics=&outStatistics=&returnZ=false&returnM=false&gdbVersion=&historicMoment=&returnDistinctValues=false&resultOffset=&resultRecordCount=&queryByDistance=&returnExtentOnly=false&datumTransformation=&parameterValues=&rangeValues=&quantizationParameters=&featureEncoding=esriDefault&f=pjson"
        r = requests.get(county_json_url)
        json = r.json()
        counties = []
        for county in json['features']:
            code = county['attributes']['kod']
            name = county['attributes']['nazev']
            county_to_list = {
                'code' : code,
                'name' : name,
            }
            counties.append(county_to_list)
        return counties

    def get_wd_item_from_hub(self, lau:int) -> pywikibot.ItemPage:
        kod_mesta = lau
        urlmesto = "https://tools.wmflabs.org/hub/P782:CZ" + str(lau) + "?lang=cs&format=json"
        response = requests.get(urlmesto)
        json_mesto = response.text
        data_mesto = json.loads(json_mesto)
        try:
            wd_mesto = data_mesto['origin']['qid']
            item = pywikibot.ItemPage(self.repo, wd_mesto)
            return item
        except:
            pass

    def get_wd_items(self):
        url = "https://query.wikidata.org/sparql?query=select%20%2a%20where%20%7B%0A%3Fobec%20wdt%3AP31%20wd%3AQ5153359.%0A%20%20%3Fobec%20wdt%3AP782%20%3Flau%0A%7D&format=json"
        r = requests.get(url)
        wd_json = r.json()
        for village in wd_json['results']['bindings']:
            qid = village['obec']['value'].replace('http://www.wikidata.org/entity/','')
            lau = int(village['lau']['value'].replace('CZ',''))
            item = pywikibot.ItemPage(self.repo, qid)
            self.wd_items[lau] = item


    def get_shapes(self, counties):
        for county in counties:
            filename = 'county_geojsons/' + str(county['code']) + '.geojson'
            if path.exists(filename):
                with open(filename, 'r') as file:
                    county_json = json.load(file)
            else:
                url = "http://ags.cuzk.cz/arcgis/rest/services/RUIAN/Prohlizeci_sluzba_nad_daty_RUIAN/MapServer/12/query?where=okres%3D" + str(county['code']) + "&text=&objectIds=&time=&geometry=&geometryType=esriGeometryEnvelope&inSR=&spatialRel=esriSpatialRelIntersects&relationParam=&outFields=*&returnGeometry=true&returnTrueCurves=false&maxAllowableOffset=&geometryPrecision=&outSR=&having=&returnIdsOnly=false&returnCountOnly=false&orderByFields=&groupByFieldsForStatistics=&outStatistics=&returnZ=false&returnM=false&gdbVersion=&historicMoment=&returnDistinctValues=false&resultOffset=&resultRecordCount=&queryByDistance=&returnExtentOnly=false&datumTransformation=&parameterValues=&rangeValues=&quantizationParameters=&featureEncoding=esriDefault&f=geojson"

                r = requests.get(url)
                county_json = r.json()
                with open(filename, 'w') as outfile:
                    json.dump(county_json, outfile)
            for city in county_json['features']:
                # shape_dump = json.dumps(city)
                if (not self.logger.isCompleteFile(str(city['properties']['kod']))):
                    shapeCollection = GeometryCollection([shape(city["geometry"])])
                    centroid = shapeCollection.centroid
                    shape_data = {
                        'county_name' : county['name'],
                        'county_code' : county['code'],
                        'city_name' : city['properties']['nazev'],
                        'city_code' : city['properties']['kod'],
                        'city_object_id' : city['properties']['objectid'],
                        'city_geometry' : city,
                        'centroid' : centroid,
                        'item' : self.wd_items[city['properties']['kod']]
                    }

                    # shape_data_to_csv = [ county['name'], county['code'], city['properties']['nazev'], city['properties']['kod'], city['properties']['objectid']]
                    self.add_shape(shape_data)

    def add_shape(self, shape):
        self.shapes.append(shape)

shapes = village_shapes()