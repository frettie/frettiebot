#!/usr/bin/python
# -*- coding: utf-8 -*-
import overpy
import sys
from logger import Logger
import time

import pywikibot
from pywikibot import pagegenerators

class OsmGet():


    def __init__(self):
        self.wd = pywikibot.Site('wikidata')
        self.logger = Logger('osmnew', 'save')

    def run_all(self):
        query = '''
        select * WHERE {
            ?item wdt:P31 wd:Q5153359
            FILTER(?item in (wd:Q959574)
        }
        '''
        genFactory = pagegenerators.GeneratorFactory(site=self.wd)
        genFactory.handleArg('-sparql:' + query)
        generator = genFactory.getCombinedGenerator()
        for page in pagegenerators.PreloadingGenerator(generator):
            self.run_one(page)

    def run_one(self, page):
        # wikidata = 'Q1740707'
        if (self.logger.isCompleteFile(page.id)):
            return True
        osm = self.get_and_parse_neighbourghs(page.id)
        wd = self.get_and_parse_wikidata(page)
        # to_delete = list(set(wd['neighbourghs']).difference(osm))
        to_delete = list(set(wd['neighbourghs']) - set(osm))
        to_add = list(set(osm)-set(wd['neighbourghs']))
        self.find_claim_in_borders(to_delete, wd['page'])
        self.logger.logComplete(page.id)



    def find_claim_in_borders(self, target, page):
        try:
            borders_with = page.text['claims']['P47']
        except KeyError as ke:
            print('claim P47 error')
            print(page.id)
            borders_with = []
            return True
        claims = []
        for border in borders_with:
            if border.target.id in target:
                print(page.id)
                print(border)
                claims.append(border)
                page.removeClaims(border, summary = 'Delete bad borders with, by OSM')


    def get_and_parse_wikidata(self, page):
        # page = pywikibot.ItemPage(self.wd, wikidata)
        try:
            # page.get()
            neighbourghs = []
            borders_with = page.text['claims']['P47']
            for border in borders_with:
                # print(border.target.id)
                neighbourghs.append(border.target.id)
        except Exception as e:
            print(e)
            print('problem')

        return {'page' : page, 'neighbourghs' : neighbourghs}


    def get_and_parse_neighbourghs(self, wikidata = ''):
        api = overpy.Overpass()
        link = '''(rel["wikidata"="''' + wikidata.strip() + '''"]["admin_level"="8"][boundary=administrative];way(r);rel(bw);)->.c;(rel.c["admin_level"="8"][boundary=administrative];/*way(r);node(w);*/)->.d;.d out tags;'''
        try:

            result = api.query(link)
            neighbourghs = []
            for relation in result.relations:
                try:
                    if (relation.tags['wikidata'] != wikidata):
                        neighbourghs.append(relation.tags['wikidata'])
                except KeyError as ke:
                    print(ke)
                    print(relation.tags)
                    print(wikidata)
                    # sys.exit()
        except overpy.exception.OverpassTooManyRequests:
            time.sleep(5)
            print('sleeping ... ')

        # print(neighbourghs)
        return neighbourghs


r = OsmGet()
r.run_all()