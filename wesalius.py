#!/usr/bin/python3
import pywikibot

site = pywikibot.Site("wikidata", "wikidata")
repo = site.data_repository()
item = pywikibot.ItemPage(repo, "Q18216")
item_dict = item.get()
claim_dict = item_dict["claims"]
en_label = item_dict["labels"]["en"]


def get_claims_2868(claim_dict):  # role
    claims = []  # list tj. pole
    claim_list_2868 = claim_dict["P2868"]
    for claim in claim_list_2868:
        target = claim.getTarget()
        labels = target.get('labels')
        claims.append(labels['labels']['en'])
    return claims


try:
    claims = get_claims_2868(claim_dict)
except Exception as e:
    print(e)
    pass

try:
    string_to_print = "Values of P2868 for " + en_label + " are" + "".join(claims) #pythoní styl výpisu
    print(string_to_print)
    #takovej čitelnější styl výpisu
    print("Values of P2868 for " + en_label + " are")
    for claim in claims:
        print(claim)


except:
    print("P2868 for", en_label, "are not entered")
    pass