#!/usr/bin/python
# -*- coding: utf-8 -*-
import pywikibot
import sys
# import exceptions
from logger import Logger
import fill_wikidata_dictionary
import re
# import curses
import redis
import argparse
import boto3
from boto3.dynamodb.conditions import Key, Attr

VILLAGE = u'village'
CITY = u'city'
MESTYS = u'mestys'


class fillWikidata:
    def __init__(self, logger_name):
        self.errors = {}
        print 'fillWikidata'
        self.logger = Logger(logger_name + u'Fill', 'saved')
        self.actual_page = None
        self.wikidata = None
        self.only_view = False
        self.cities = None
        # self.languageWikipedia = 'cs';
        # self.family = 'wikipedia'
        self.error_lang = u"chyba "
        self.birth_category_lang = u'Narození'
        self.death_category_lang = u'Úmrtí'
        # self.wikidataFamily = 'wikidata'
        self.lang_filling = u'Plnim - '
        self.lang_not_filling = u'Neplnim - '
        self.selected_city = u''
        self.type_of_entity = u''
        self.all_entities = True
        self.only_one_city = False
        parser = argparse.ArgumentParser(
            description='Process places of birth and death on Wikidata from Wikipedia - primary cs.')
        parser.add_argument('--city', help='city to process')
        parser.add_argument('--language', help='wikipedia language, default: cs', default='cs')
        parser.add_argument('--wikidatafamily', help='wikidata family, default: wikidata', default='wikidata')
        parser.add_argument('--family', help='wikipedia family, default: wikipedia', default='wikipedia')
        parser.add_argument('--type', help='type of entity')
        parser.add_argument('--letters', help='first letters')
        args = parser.parse_args()
        self.language_wikipedia = args.language
        self.wikidata_family = args.wikidatafamily
        self.family = args.family
        self.set_selected_city(args)
        self.set_type(args)
        self.letters = args.letters

    def base_of_cities(self):
        dicti = fill_wikidata_dictionary.Dictionary()
        dicti.base_of_cities()
        self.cities = dicti.cities

    def set_type(self, args):
        type_of_entity = args.type
        if type_of_entity:
            self.type_of_entity = unicode(type_of_entity, 'utf-8')
            self.all_entities = False

    def set_selected_city(self, args):
        city = args.city
        if city:
            self.only_one_city = True
            self.selected_city = unicode(city, 'utf-8')

    def load_pages(self, type_of_category, end_of_category, check_run_only, type_of_entity):
        self.base_of_cities()
        site = pywikibot.getSite(self.language_wikipedia, self.family)

        page = pywikibot.Page(site, u'Category:' + type_of_category + end_of_category)

        generator_check = pywikibot.Category(page).subcategories(recurse=0)
        generator = pywikibot.Category(page).subcategories(recurse=0)
        stop_run_because_not_cat_in_cities = False
        if check_run_only:
            for page in generator_check:
                match = re.search(u'(' + type_of_category + u' ve|' + type_of_category + u' v) (.*)', page.title())
                if not match:
                    continue
                if match.group(2) not in self.cities.keys():
                    print self.error_lang + page.title()
                    stop_run_because_not_cat_in_cities = True

            if stop_run_because_not_cat_in_cities:
                return stop_run_because_not_cat_in_cities
                # sys.exit()

        print "----------------------------"
        print "----- run classic -----"
        if not check_run_only:
            for page in generator:
                match = re.search(u'(' + type_of_category + u' ve|' + type_of_category + u' v) (.*)', page.title())
                if match:
                    if self.only_one_city:
                        if self.selected_city == self.cities[match.group(2)]:
                            print self.cities[match.group(2)]
                            cat = page.title().split(':')
                            # print cat[1]
                            self.page_process(self.cities[match.group(2)], cat[1], type_of_category)
                        else:
                            pass
                    else:
                        if self.type_of_entity == type_of_entity or self.all_entities == True:
                            cat = page.title().split(':')
                            # print cat[1]
                            if self.letters is not None:
                                count_of_letters = len(unicode(self.letters, 'utf-8'))
                                first_letters = self.cities[match.group(2)][0:count_of_letters]
                                if first_letters == unicode(self.letters, 'utf-8'):
                                    print self.cities[match.group(2)]
                                    self.page_process(self.cities[match.group(2)], cat[1], type_of_category)
                            else:
                                print self.cities[match.group(2)]
                                self.page_process(self.cities[match.group(2)], cat[1], type_of_category)

            print "----------------------------"
            print self.errors
            return self.errors

    def page_process(self, city, category_name, type_of_category):
        """

        :param type_of_category:
        :param city:
        :type category_name: unicode
        """
        if type_of_category == self.birth_category_lang:
            prop = u'P19'
        if type_of_category == self.death_category_lang:
            prop = u'P20'

        site = pywikibot.getSite(self.language_wikipedia, self.family)
        # siteEn = pywikibot.getSite('en', 'wikipedia')
        site_wikidata = pywikibot.getSite(self.wikidata_family)
        name_item = pywikibot.Page(site, city).data_item()

        # name_item = pywikibot.Page.DataPage(site_wikidata,u'Q131734')

        page = pywikibot.Page(site, u'Category:' + category_name)
        # print page.title()
        generator = pywikibot.Category(page).articles(recurse=True)
        for page in generator:
            # not(self.logger.isCompleteFile(page.title())):

            if not (self.logger.isCompleteFile(page.title())) and (page.namespace() == 0):

                self.actual_page = page
                try:
                    self.wikidata = self.actual_page.data_item()

                    # if u'P31' not in self.wikidata.claims:#Q5 = clovek
                    if prop not in self.wikidata.claims and self.wikidata.claims['P31'][0].getTarget().title() == 'Q5':
                        print self.lang_filling + page.title()
                        if not self.only_view:
                            new_claim = pywikibot.Claim(site_wikidata, prop)
                            new_claim.setTarget(name_item)
                            self.wikidata.addClaim(new_claim)
                    else:
                        if not self.only_view:
                            print self.lang_not_filling + page.title()
                    if not self.only_view:
                        self.logger.logComplete(page.title())
                except Exception, e:
                    arr = {'name': self.actual_page.title(), 'err': e}
                    self.errors[self.actual_page.title()] = arr
                    pass

            else:
                if not self.only_view:
                    ##print u'Neresim - ' + page.title()
                    pass

    def run(self):
        site = pywikibot.getSite(self.language_wikipedia, self.family)
        # siteEn = pywikibot.getSite('en', 'wikipedia')
        site_wikidata = pywikibot.getSite(self.wikidata_family)
        category_name = u''
        page = pywikibot.Page(site, u'Category:' + self.category_name)
        generator = pywikibot.Category(page).articles(recurse=True)
        for page in generator:
            # not(self.logger.isCompleteFile(page.title())):

            if not (self.logger.isCompleteFile(page.title())) and (page.namespace() == 0):

                self.actual_page = page
                try:
                    self.wikidata = self.actual_page.data_item()

                    # if u'P31' not in self.wikidata.claims:#Q5 = clovek
                    if u'P20' not in self.wikidata.claims and self.wikidata.claims['P31'][0].getTarget().title() == 'Q5':
                        print self.lang_filling + page.title()
                        if not self.only_view:
                            new_claim = pywikibot.Claim(site_wikidata, u'P20')
                            # new_claim.setTarget(name_item)
                            # self.wikidata.addClaim(new_claim)
                    else:
                        if (not self.only_view):
                            print self.lang_not_filling + page.title()
                except Exception, e:
                    print "chyba - asi chybí prvek"
                    print self.actual_page.title()
                    print
                    print e
                    sys.exit()
                    pass
                if not self.only_view:
                    self.logger.logComplete(page.title())

            else:
                if not self.only_view:
                    # print u'Neresim - ' + page.title()
                    pass

    def is_human(self):
        categories = [u'Kategorie:Muži', u'Kategorie:Ženy']
        article_categories = []

        for ac in list(self.actual_page.categories()):
            article_categories.append(ac.title())
        for category in categories:
            if category in article_categories:
                return True

        return False


class Runner:
    def __init__(self):
        self.errors_buffer = []

    @staticmethod
    def insert_to_redis():
        fw = fillWikidata(u'narozeni')
        fw.base_of_cities()
        r = redis.StrictRedis(host='sedlacek.zcom.cz', port=6379, db=0)

        # for city in fw.cities:
        # r.set(city, fw.cities[city])

        print r.get(u'Praze')

        sys.exit()

    def run(self):
        cesko = True
        rakousko = False
        nemecko = False
        slovensko = False
        polsko = False
        runs = []

        if (cesko):
            f1 = fillWikidata(u'narozeni')
            runs.append(f1.load_pages(u'Narození', u' v Česku podle vesnic', True, VILLAGE))

            f2 = fillWikidata(u'narozeni')
            runs.append(f2.load_pages(u'Narození', u' v Česku podle měst', True, CITY))

            f3 = fillWikidata(u'narozeni')
            runs.append(f3.load_pages(u'Narození', u' v Česku podle městysů', True, MESTYS))

            f4 = fillWikidata(u'umrti')
            runs.append(f4.load_pages(u'Úmrtí', u' v Česku podle měst', True, CITY))

            f5 = fillWikidata(u'umrti')
            runs.append(f5.load_pages(u'Úmrtí', u' v Česku podle městysů', True, MESTYS))

            f6 = fillWikidata(u'umrti')
            runs.append(f6.load_pages(u'Úmrtí', u' v Česku podle vesnic', True, VILLAGE))

        if rakousko:
            f7 = fillWikidata(u'narozeni')
            runs.append(f7.load_pages(u'Narození', u' v Rakousku podle měst', True, CITY))

            f9 = fillWikidata(u'umrti')
            runs.append(f9.load_pages(u'Úmrtí', u' v Rakousku podle měst', True, CITY))

        if polsko:
            f7 = fillWikidata(u'narozeni')
            runs.append(f7.load_pages(u'Narození', u' v Polsku podle měst', True, CITY))

            f9 = fillWikidata(u'umrti')
            runs.append(f9.load_pages(u'Úmrtí', u' v Polsku podle měst', True, CITY))

        if nemecko:
            f8 = fillWikidata(u'narozeni')
            runs.append(f8.load_pages(u'Narození', u' v Německu podle měst', True, CITY))

            f10 = fillWikidata(u'umrti')
            runs.append(f10.load_pages(u'Úmrtí', u' v Německu podle měst', True, CITY))

        if slovensko:
            f11 = fillWikidata(u'narozeni')
            runs.append(f11.load_pages(u'Narození', u' na Slovensku podle vesnic', True, VILLAGE))

            f12 = fillWikidata(u'narozeni')
            runs.append(f12.load_pages(u'Narození', u' na Slovensku podle měst', True, CITY))

            f13 = fillWikidata(u'umrti')
            runs.append(f13.load_pages(u'Úmrtí', u' na Slovensku podle měst', True, CITY))

            f14 = fillWikidata(u'umrti')
            runs.append(f14.load_pages(u'Úmrtí', u' na Slovensku podle vesnic', True, VILLAGE))

        # for run in runs:
        #     if run:
        #         sys.exit()

        if cesko:
            self.errors_buffer.append(f1.load_pages(u'Narození', u' v Česku podle vesnic', False, VILLAGE))
            print self.errors_buffer

            self.errors_buffer.append(f2.load_pages(u'Narození', u' v Česku podle měst', False, CITY))
            print self.errors_buffer

            self.errors_buffer.append(f3.load_pages(u'Narození', u' v Česku podle městysů', False, MESTYS))
            print self.errors_buffer

            self.errors_buffer.append(f4.load_pages(u'Úmrtí', u' v Česku podle měst', False, CITY))
            print self.errors_buffer

            self.errors_buffer.append(f5.load_pages(u'Úmrtí', u' v Česku podle městysů', False, MESTYS))
            print self.errors_buffer

            self.errors_buffer.append(f6.load_pages(u'Úmrtí', u' v Česku podle vesnic', False, VILLAGE))
            print self.errors_buffer

        if rakousko:
            self.errors_buffer.append(f7.load_pages(u'Narození', u' v Rakousku podle měst', False, CITY))
            print self.errors_buffer

            self.errors_buffer.append(f9.load_pages(u'Úmrtí', u' v Rakousku podle měst', False, CITY))
            print self.errors_buffer

        if polsko:
            self.errors_buffer.append(f7.load_pages(u'Narození', u' v Polsku podle měst', False, CITY))
            print self.errors_buffer
            self.errors_buffer.append(f7.load_pages(u'Úmrtí', u' v Polsku podle měst', False, CITY))
            print self.errors_buffer

        if nemecko:
            self.errors_buffer.append(f8.load_pages(u'Narození', u' v Německu podle měst', False, CITY))
            print self.errors_buffer

            self.errors_buffer.append(f10.load_pages(u'Úmrtí', u' v Německu podle měst', False, CITY))
            print self.errors_buffer

        if slovensko:
            self.errors_buffer.append(f11.load_pages(u'Narození', u' na Slovensku podle vesnic', False, VILLAGE))
            print self.errors_buffer

            self.errors_buffer.append(f12.load_pages(u'Narození', u' na Slovensku podle měst', False, CITY))
            print self.errors_buffer

            self.errors_buffer.append(f13.load_pages(u'Úmrtí', u' na Slovensku podle měst', False, CITY))
            print self.errors_buffer

            self.errors_buffer.append(f14.load_pages(u'Úmrtí', u' na Slovensku podle vesnic', False, VILLAGE))
            print self.errors_buffer


f = Runner()
# f.insert_to_redis()
f.run()
