#!/usr/bin/python
# -*- coding: utf-8 -*-

import codecs

class Logger:

    def __init__(self, entityType, logType='complete'):
        self.entityType = entityType
        self.logType = logType

    def log(self, text, errorText = False, printLog = True):

        if (self.logType == 'err'):
            if (errorText):
                text = text + ';' + errorText
            self.logError(text)
        else:
            self.logComplete(text)

        self.text = text
        if printLog:
            self.printLog()

    def printLog(self):
        if (self.logType == 'err'):
            splits = self.text.split(';')
            print('Error: ' + splits[1] + ' - ' + splits[0])
        else:
            print('OK: ' + self.text)

    def logComplete(self, title):
        file = codecs.open("log_" + self.entityType + ".txt", "a", "utf-8")
        file.write(title + '\n')
        file.close()

    def logError(self, title):
        file = codecs.open("log_err_" + self.entityType + ".txt", "a", "utf-8")
        file.write(title + '\n')
        file.close()

    def isCompleteFile(self, entity):
        try:
            file = codecs.open("log_" + self.entityType + ".txt", "r", "utf-8")
            lines = file.readlines()
            list_entity = list()
            file.close()
            for it in lines:
                list_entity.append(it.strip())
            #TODO opravit!

            try:
                list_entity.index(entity)
                return True
            except ValueError:
                return False
        except IOError:
            print("Not exist file")
            codecs.open('log_' + self.entityType + '.txt', 'w', 'utf-8')

