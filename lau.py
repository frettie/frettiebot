#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import pywikibot
import urllib.parse


from wikidatafun import *
class parents:
    query = '''
    SELECT ?obec ?lau WHERE {
  ?obec wdt:P31 wd:Q5153359.
  ?obec wdt:P782 ?lau
#        SERVICE wikibase:label { bd:serviceParam wikibase:language "cs". }
MINUS{filter (CONTAINS(str(?lau),"CZ"))}
}



    '''

    def run(self):
        url = 'https://query.wikidata.org/bigdata/namespace/wdq/sparql?query=%s' % (urllib.parse.quote(self.query))
        url = '%s&format=json' % (url)
        print(url)
        site = pywikibot.Site('wikidata', 'wikidata')
        repo = site.data_repository()
        sparql = getURL(url)
        json1 = loadSPARQL(sparql=sparql)
        print(json1)
        qlist = []
        for result in json1['results']['bindings']:
            try:
                arr = {}
                arr['obec'] = result['obec']['value'].split('/entity/')[1]
                arr['lau'] = result['lau']['value']
                if arr not in qlist:
                    qlist.append(arr)
            except IndexError as e:
                pass

        for q in qlist:
            try:
                item = pywikibot.ItemPage(repo, q['obec'])
                lau = 'CZ' + q['lau']
                claims = item.text['claims']['P782'][0]

                assert isinstance(claims, pywikibot.Claim)
                claims.changeTarget(lau)

            except Exception as e:
                pass

        print(qlist)

par = parents()
par.run()