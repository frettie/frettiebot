#!/usr/bin/python
# -*- coding: utf-8 -*-
# import unicodedata

import pywikibot
# from lxml import objectify, etree
# from httplib2 import Http
# from urllib import urlencode
# import xmltodict
# import codecs
# import locale
import sys
import re
# import time
# from optparse import OptionParser
# import datetime
from string import Template
# import math
# import workdictionary
from lxml import etree
import MyDescription
import pyproj
import MyLogger
import requests
import csv
from bs4 import BeautifulSoup
import peewee

# import PeopleDescription
# from sortedcontainers import SortedList

def getKey(item):
    return item['year']

class ParishDescription(MyDescription.Description):
    def __init__(self):
        super().__init__()
        print("Parish")
        types = {u'en':u'Parish', u'cs':u'Sbor'}
        self.setType(types)

class VillageDescription(MyDescription.Description):
    def __init__(self):
        super().__init__()
        print('Village')
        self.define()
        self.setEntityType('village')
        self.setLangWord('cs', 'type', 'Vesnice')
        self.setLangWord('en', 'type', 'Village')
        self.setLangWord('cs', 'region', 'Kraj')
        self.setLangWord('en', 'region', 'Region')
        self.setNameOfCategoryToGetCounty('vesnice v okrese')
        self.setMustBeInCategory('vesnice')
        self.setExcludeType('none')

class DeanDescription(MyDescription.Description):
    def __init__(self):
        super().__init__()
        print('Dean')

        #nahradit desc farnost za dean

class ChurchDescription(MyDescription.Description):
    def __init__(self):
        super().__init__()
        self.define()
        print('Church')
        types = {u'en':u'Church', u'cs':u'Kostel'}
        self.setType(types)
        self.setNameOfCategoryToGetCounty('kostely v okrese')
        self.setExcludeByCategory('kaple')
        self.setExcludeByCategory('seznam')

class ChapelDescription(MyDescription.Description):
    def __init__(self):
        super().__init__()
        self.define()
        print('Chapel')
        #types = {u'en':u'Chapel', u'cs':u'Kaple'}
        #afterPrepend = {u'en':u'in', u'cs':u'v'}
        self.setLangWord('cs', 'type', 'Kaple')
        self.setLangWord('en', 'type', 'Chapel')
        self.setLangWord('cs', 'region', 'Kraj')
        self.setLangWord('en', 'region', 'Region')
        #self.setType(types)
        self.setNameOfCategoryToGetCounty('kaple v okrese')
        self.setCityCategory('kaple')
        self.setExcludeByArticleName('kostel')
        self.setExcludeByArticleName('seznam')
        self.setExcludeType('article')

class NatureMonumentsListDescription(MyDescription.Description):
    def __init__(self):
        super().__init__()
        self.define()
        print("Nature Monuments list")
        self.setEntityType('nature_monument_list')
        self.setLangWord('cs', 'type', 'Seznam chráněných území')
        self.setLangWord('en', 'type', 'List of natural monuments')
        self.setLangWord('cs', 'region', 'Kraj')
        self.setLangWord('en', 'region', 'Region')
        self.setNameOfCategoryToGetCounty(u'chráněná území v okrese')
        self.setMustBeInCategory(u'seznamy chráněných území')
        self.setExcludeType('none')

        descriptionFormatGeneralStyle = {u'en' : u'$type in $county county', u'cs':u'$type v okrese $county'}
        self.getData('Seznamy chráněných území v Česku podle okresu', descriptionFormatGeneralStyle)

    def setDescriptions(self):
        for lang in self.langVersions:
            s = Template(self.getInText(lang))
            self.descriptions[lang] = s.substitute(type=self.getLangWord(lang, 'type'), county=self.county)

        for lang in self.langVersions:
            if (lang in self.item.descriptions.keys()):
                self.descriptions.pop(lang)

#start = PeopleDescription.PeopleDescription()
#start.runAll()

class CheckMech():
    def __init__(self):
        self.seznam = [u'hlevík polní',
                        u'termovka kadeřavá',]
    def run(self):
        site = pywikibot.Site('cs','wikipedia') #  any site will work, this is just an example
        print('{|')
        print('|+Počty odkazuje sem stránek')
        print('! článek !! počet (jmený prostor článek)')

        for s in self.seznam:
            page = pywikibot.Page(site, s)
            pocet = str(sum(1 for itp in page.backlinks(namespaces=0)))
            print('|-')
            print(u'| ' + s + u' || ' + pocet)
        print('|}')

def process_element(elem, fout):
    global counter
    # normalized = unicodedata.normalize('NFKD', \
    #         unicode(elem.text)).encode('ASCII','ignore').lower()
    # print >>fout, normalized.replace('\n', ' ')
    if counter % 10000 == 0: print("Doc " + str(counter))
    counter += 1


class CoordinatesCorrector():
    def __init__(self):
        print("coordinates corrector")
        self.entityType = 'coordinates_corrector'
        self.errLog = MyLogger.Logger(self.entityType, 'err')
        self.completeLog = MyLogger.Logger(self.entityType, 'complete')



        self.counter = 0
        self.coordList = {}
        self.site = pywikibot.Site('cs','wikipedia') #  any site will work, this is just an example
        page = pywikibot.Page(self.site, u'Registr územní identifikace, adres a nemovitostí')
        self.ruianOnWikidata = pywikibot.ItemPage.fromPage(page)

    def loadFile(self):
        return 'obce.xml'

    def getObce(self):
        site = pywikibot.Site('cs', 'wikipedia')
        articlesObce = pywikibot.Category(site, 'Obce v Česku podle okresu').articles(recurse=1)
        for article in articlesObce:
            if (not self.completeLog.isCompleteFile(article.title())):
                self.getObec(article)
                self.completeLog.log(article.title())

        articlesMesta = pywikibot.Category(site, 'Města v Česku podle okresu').articles(recurse=1)
        for article in articlesMesta:
            if (not self.completeLog.isCompleteFile(article.title())):
                self.getObec(article)
                self.completeLog.log(article.title())

    def getObec(self, page):
        try:
            item = pywikibot.ItemPage.fromPage(page) #  this can be used for any page object
        except pywikibot.NoPage:
            return False
        except AttributeError:
            return False
        if (u'P782' in item.claims.keys()):
            lau = item.claims.get('P782')[0].target
        else:
            return False

        if (u'P625' in item.claims.keys()):
            coords = item.claims.get('P625')[0]
            removeClaim = []
            removeClaim.append(coords)
            item.removeClaims(removeClaim)

        site = item.site
        if (not lau in self.coordList.keys()):
            return False
        newClaim = pywikibot.Claim(site, u'P625')
        newCoord = pywikibot.Coordinate(self.coordList[lau]['coord']['lat'], self.coordList[lau]['coord']['lon'], dim=100)
        newClaim.setTarget(newCoord)
        item.addClaim(newClaim)

        #uvedeno v
        source = pywikibot.Claim(site, u'P248')
        source.setTarget(self.ruianOnWikidata)

        #pristup
        dateOfRuianData = pywikibot.WbTime(2014,12,31)
        qualifier = pywikibot.Claim(site, u'P813')
        qualifier.setTarget(dateOfRuianData)

        sourceList = []
        sourceList.append(source)
        sourceList.append(qualifier)
        newClaim.addSources(sourceList)




    def run(self):
        coords = self.getCoords()
        #print coords
        self.parseCoords(coords)
        #self.getObec(u'Třebíč')
        self.getObce()

    def parseCoords(self, coords):
        count = 0
        for coord in coords:
            self.coordList[coord['key']] = {}
            self.coordList[coord['key']]['nazev'] = coord['nazev']
            self.coordList[coord['key']]['coord'] = self.jtskToWgs(self.coordToDict(coord['coord']))
            count = count + 1

        #print count

    def jtskToWgs(self,coord):

        wgs84=pyproj.Proj("+init=EPSG:4326")
        jtsk=pyproj.Proj("+init=EPSG:5514")
        lon = coord['lon']
        lat = coord['lat']

        lon, lat = pyproj.transform(jtsk, wgs84, lon, lat)
        list = {}
        list['lat'] = lat
        list['lon'] = lon
        return list

    def coordToDict(self, coord):
        coordtmp = coord.split(' ')
        coordRet = {}
        coordtmp2 = {}
        coordtmp2['lon'] = re.findall('([0-9\.]+)', coordtmp[0])
        coordtmp2['lat'] = re.findall('([0-9\.]+)', coordtmp[1])



        coordRet['lon'] = 0-float(coordtmp2['lon'][0])
        coordRet['lat'] = 0-float(coordtmp2['lat'][0])


        return coordRet

    def getCoords(self):
        coords = etree.parse("obce.xml").getroot()
        coords_list = []
        for coord in coords:

            for child in coord.getchildren():
                if (child.tag == '{urn:cz:isvs:ruian:schemas:VymennyFormatTypy:v1}Obce'):
                    for child2 in child.getchildren():
                        for child3 in child2.getchildren():
                            if child3.tag == '{urn:cz:isvs:ruian:schemas:ObecIntTypy:v1}Kod':
                                obeckod = child3.text
                            if child3.tag == '{urn:cz:isvs:ruian:schemas:ObecIntTypy:v1}Nazev':
                                obecnazev = child3.text
                            if child3.tag == '{urn:cz:isvs:ruian:schemas:ObecIntTypy:v1}Geometrie':
                                for child4 in child3.getchildren():
                                    for child5 in child4.getchildren():
                                        for child6 in child5.getchildren():
                                            for child7 in child6.getchildren():
                                                for child8 in child7.getchildren():
                                                    this = {}
                                                    #print obeckod
                                                    #print child8.text
                                                    this['key'] = obeckod
                                                    this['coord'] = child8.text
                                                    this['nazev'] = obecnazev
                                                    coords_list.append(this)

        return coords_list


class ChangeLinks():

    def __init__(self):
        self.page = u"R10"
        self.newLink = u"Rychlostní silnice R10"
        self.firstStart = False
        self.railroads = {}

    def czechRailroads(self):
        self.addToList(self.railroads, 'Trať 010', 'Železniční trať Kolín – Česká Třebová')
        self.addToList(self.railroads, 'Trať 011', 'Železniční trať Praha–Kolín')
        self.addToList(self.railroads, 'Trať 012', 'Železniční trať Pečky–Kouřim')
        self.addToList(self.railroads, 'Trať 013', 'Železniční trať Bošice–Bečváry')
        self.addToList(self.railroads, 'Trať 014', 'Železniční trať Kolín–Ledečko')
        self.addToList(self.railroads, 'Trať 015', 'Železniční trať Přelouč–Prachovice')
        self.addToList(self.railroads, 'Trať 016', 'Železniční trať Chrudim – Borohrádek')
        self.addToList(self.railroads, 'Trať 017', 'Železniční trať Třebovice v Čechách – Skalice nad Svitavou')
        self.addToList(self.railroads, 'Trať 018', 'Železniční trať Choceň–Litomyšl')
        self.addToList(self.railroads, 'Trať 019', 'Železniční trať Rudoltice v Čechách – Lanškroun')
        self.addToList(self.railroads, 'Trať 020', 'Železniční trať Velký Osek – Choceň')
        self.addToList(self.railroads, 'Trať 021', 'Železniční trať Týniště nad Orlicí – Letohrad')
        self.addToList(self.railroads, 'Trať 022', 'Železniční trať Častolovice–Solnice')
        self.addToList(self.railroads, 'Trať 023', 'Železniční trať Doudleby nad Orlicí – Rokytnice v Orlických horách')
        self.addToList(self.railroads, 'Trať 024', 'Železniční trať Ústí nad Orlicí – Lichkov – Štíty/Międzylesie')
        self.addToList(self.railroads, 'Trať 025', 'Železniční trať Dolní Lipka – Hanušovice')
        self.addToList(self.railroads, 'Trať 026', 'Železniční trať Týniště nad Orlicí – Otovice zastávka')
        self.addToList(self.railroads, 'Trať 028', 'Železniční trať Opočno pod Orlickými horami – Dobruška')
        self.addToList(self.railroads, 'Trať 030', 'Železniční trať Pardubice–Jaroměř–Liberec')
        self.addToList(self.railroads, 'Trať 031', 'Železniční trať Pardubice–Jaroměř–Liberec')
        self.addToList(self.railroads, 'Trať 032', 'Železniční trať Jaroměř–Trutnov')
        self.addToList(self.railroads, 'Trať 033', 'Železniční trať Starkoč–Václavice–(Náchod)')
        self.addToList(self.railroads, 'Trať 034', 'Železniční trať Smržovka – Josefův Důl')
        self.addToList(self.railroads, 'Trať 035', 'Železniční trať Železný Brod – Tanvald')
        self.addToList(self.railroads, 'Trať 036', 'Železniční trať Liberec–Tanvald–Harrachov')
        self.addToList(self.railroads, 'Trať 037', 'Železniční trať Liberec–Zawidów')
        self.addToList(self.railroads, 'Trať 038', 'Železniční trať Raspenava – Bílý Potok pod Smrkem')
        self.addToList(self.railroads, 'Trať 039', 'Železniční trať Frýdlant v Čechách – Jindřichovice pod Smrkem')
        self.addToList(self.railroads, 'Trať 040', 'Železniční trať Chlumec nad Cidlinou – Trutnov')
        self.addToList(self.railroads, 'Trať 041', 'Železniční trať Hradec Králové – Turnov')
        self.addToList(self.railroads, 'Trať 042', 'Železniční trať Martinice v Krkonoších – Rokytnice nad Jizerou')
        self.addToList(self.railroads, 'Trať 043', 'Železniční trať Trutnov – Královec – Lubawka/Žacléř')
        self.addToList(self.railroads, 'Trať 044', 'Železniční trať Kunčice nad Labem – Vrchlabí')
        self.addToList(self.railroads, 'Trať 045', 'Železniční trať Trutnov – Svoboda nad Úpou')
        self.addToList(self.railroads, 'Trať 046', 'Železniční trať Hněvčeves–Smiřice')
        self.addToList(self.railroads, 'Trať 047', 'Železniční trať Trutnov – Teplice nad Metují')
        self.addToList(self.railroads, 'Trať 060', 'Železniční trať Poříčany–Nymburk')
        self.addToList(self.railroads, 'Trať 061', 'Železniční trať Nymburk–Jičín')
        self.addToList(self.railroads, 'Trať 062', 'Železniční trať Chlumec nad Cidlinou – Křinec')
        self.addToList(self.railroads, 'Trať 063', 'Železniční trať Bakov nad Jizerou – Kopidlno')
        self.addToList(self.railroads, 'Trať 064', 'Železniční trať Mladá Boleslav – Stará Paka')
        self.addToList(self.railroads, 'Trať 070', 'Železniční trať Praha–Turnov')
        self.addToList(self.railroads, 'Trať 071', 'Železniční trať Nymburk – Mladá Boleslav')
        self.addToList(self.railroads, 'Trať 072', 'Železniční trať Lysá nad Labem – Ústí nad Labem')
        self.addToList(self.railroads, 'Trať 073', 'Železniční trať Ústí nad Labem – Děčín')
        self.addToList(self.railroads, 'Trať 074', 'Železniční trať Čelákovice–Neratovice')
        self.addToList(self.railroads, 'Trať 076', 'Železniční trať Mladá Boleslav – Mělník')
        self.addToList(self.railroads, 'Trať 080', 'Železniční trať Bakov nad Jizerou – Jedlová')
        self.addToList(self.railroads, 'Trať 081', 'Železniční trať Děčín – Benešov nad Ploučnicí – Rumburk/Česká Lípa')
        self.addToList(self.railroads, 'Trať 083', 'Železniční trať Rumburk–Sebnitz')
        self.addToList(self.railroads, 'Trať 084', 'Železniční trať Rumburk–Panský–Mikulášovice')
        self.addToList(self.railroads, 'Trať 085', 'Železniční trať Krásná Lípa – Panský')
        self.addToList(self.railroads, 'Trať 086', 'Železniční trať Liberec – Česká Lípa')
        self.addToList(self.railroads, 'Trať 087', 'Železniční trať Lovosice – Česká Lípa')
        self.addToList(self.railroads, 'Trať 088', 'Železniční trať Rumburk–Ebersbach')
        self.addToList(self.railroads, 'Trať 089', 'Železniční trať Liberec–Rybniště/Seifhennersdorf')
        self.addToList(self.railroads, 'Trať 090', 'Železniční trať Praha–Vraňany–Děčín')
        self.addToList(self.railroads, 'Trať 091', 'Železniční trať Praha–Vraňany–Děčín')
        self.addToList(self.railroads, 'Trať 092', 'Železniční trať Neratovice – Kralupy nad Vltavou')
        self.addToList(self.railroads, 'Trať 093', 'Železniční trať Kralupy nad Vltavou – Kladno')
        self.addToList(self.railroads, 'Trať 094', 'Železniční trať Vraňany – Lužec nad Vltavou')
        self.addToList(self.railroads, 'Trať 095', 'Železniční trať Vraňany–Libochovice')
        self.addToList(self.railroads, 'Trať 096', 'Železniční trať Roudnice nad Labem – Zlonice')
        self.addToList(self.railroads, 'Trať 097', 'Železniční trať Lovosice – Teplice v Čechách')
        self.addToList(self.railroads, 'Trať 098', 'Železniční trať Děčín – Dresden-Neustadt')
        self.addToList(self.railroads, 'Trať 110', 'Železniční trať Kralupy nad Vltavou – Louny')
        self.addToList(self.railroads, 'Trať 111', 'Železniční trať Kralupy nad Vltavou – Velvary')
        self.addToList(self.railroads, 'Trať 113', 'Železniční trať Čížkovice–Obrnice')
        self.addToList(self.railroads, 'Trať 114', 'Železniční trať Lovosice–Postoloprty')
        self.addToList(self.railroads, 'Trať 120', 'Železniční trať Praha–Rakovník')
        self.addToList(self.railroads, 'Trať 121', 'Železniční trať Hostivice–Podlešín')
        self.addToList(self.railroads, 'Trať 122', 'Železniční trať Praha-Smíchov – Hostivice – Rudná u Prahy')
        self.addToList(self.railroads, 'Trať 123', 'Železniční trať Žatec–Obrnice')
        self.addToList(self.railroads, 'Trať 124', 'Železniční trať Lužná u Rakovníka – Chomutov')
        self.addToList(self.railroads, 'Trať 125', 'Železniční trať Krupá–Kolešovice')
        self.addToList(self.railroads, 'Trať 126', 'Železniční trať Most–Rakovník')
        self.addToList(self.railroads, 'Trať 130', 'Železniční trať Ústí nad Labem – Chomutov')
        self.addToList(self.railroads, 'Trať 131', 'Železniční trať Ústí nad Labem – Bílina')
        self.addToList(self.railroads, 'Trať 132', 'Železniční trať Děčín – Oldřichov u Duchcova')
        self.addToList(self.railroads, 'Trať 133', 'Železniční trať Chomutov–Jirkov')
        self.addToList(self.railroads, 'Trať 134', 'Železniční trať Teplice v Čechách – Litvínov')
        self.addToList(self.railroads, 'Trať 135', 'Moldavská horská dráha')
        self.addToList(self.railroads, 'Trať 137', 'Železniční trať Chomutov–Vejprty')
        self.addToList(self.railroads, 'Trať 140', 'Železniční trať Chomutov–Cheb')
        self.addToList(self.railroads, 'Trať 141', 'Železniční trať Karlovy Vary – Merklín')
        self.addToList(self.railroads, 'Trať 142', 'Železniční trať Karlovy Vary – Johanngeorgenstadt')
        self.addToList(self.railroads, 'Trať 143', 'Železniční trať Chodov – Nová Role')
        self.addToList(self.railroads, 'Trať 144', 'Železniční trať Nové Sedlo u Lokte – Loket')
        self.addToList(self.railroads, 'Trať 145', 'Železniční trať Sokolov–Kraslice')
        self.addToList(self.railroads, 'Trať 146', 'Železniční trať Cheb – Tršnice – Luby u Chebu')
        self.addToList(self.railroads, 'Trať 147', 'Železniční trať Františkovy Lázně – Bad Brambach')
        self.addToList(self.railroads, 'Trať 148', 'Železniční trať Cheb – Hranice v Čechách')
        self.addToList(self.railroads, 'Trať 149', 'Železniční trať Karlovy Vary – Mariánské Lázně')
        self.addToList(self.railroads, 'Trať 160', 'Železniční trať Plzeň–Žatec')
        self.addToList(self.railroads, 'Trať 161', 'Železniční trať Rakovník – Bečov nad Teplou')
        self.addToList(self.railroads, 'Trať 162', 'Železniční trať Rakovník–Mladotice')
        self.addToList(self.railroads, 'Trať 163', 'Železniční trať Protivec–Bochov')
        self.addToList(self.railroads, 'Trať 164', 'Železniční trať Kadaň – Vilémov u Kadaně – Kaštice/Kadaňský Rohozec')
        self.addToList(self.railroads, 'Trať 170', 'Železniční trať Beroun–Plzeň–Cheb')
        self.addToList(self.railroads, 'Trať 171', 'Železniční trať Praha–Beroun')
        self.addToList(self.railroads, 'Trať 172', 'Železniční trať Zadní Třebaň – Lochovice')
        self.addToList(self.railroads, 'Trať 173', 'Železniční trať Praha – Rudná u Prahy – Beroun')
        self.addToList(self.railroads, 'Trať 174', 'Železniční trať Beroun–Rakovník')
        self.addToList(self.railroads, 'Trať 175', 'Železniční trať Rokycany–Nezvěstice')
        self.addToList(self.railroads, 'Trať 176', 'Železniční trať Chrást u Plzně – Radnice')
        self.addToList(self.railroads, 'Trať 177', 'Železniční trať Pňovany–Bezdružice')
        self.addToList(self.railroads, 'Trať 178', 'Železniční trať Svojšín–Bor')
        self.addToList(self.railroads, 'Trať 180', 'Železniční trať Plzeň – Furth im Wald')
        self.addToList(self.railroads, 'Trať 181', 'Železniční trať Nýřany – Heřmanova Huť')
        self.addToList(self.railroads, 'Trať 182', 'Železniční trať Staňkov–Poběžovice')
        self.addToList(self.railroads, 'Trať 183', 'Železniční trať Plzeň – Klatovy – Železná Ruda')
        self.addToList(self.railroads, 'Trať 184', 'Železniční trať Domažlice – Planá u Mariánských Lázní')
        self.addToList(self.railroads, 'Trať 185', 'Železniční trať Horažďovice–Domažlice')
        self.addToList(self.railroads, 'Trať 190', 'Železniční trať Plzeň – České Budějovice')
        self.addToList(self.railroads, 'Trať 191', 'Železniční trať Nepomuk–Blatná')
        self.addToList(self.railroads, 'Trať 192', 'Železniční trať Číčenice – Týn nad Vltavou')
        self.addToList(self.railroads, 'Trať 193', 'Železniční trať Dívčice–Netolice')
        self.addToList(self.railroads, 'Trať 194', 'Železniční trať České Budějovice – Černý Kříž – Volary/Nové Údolí')
        self.addToList(self.railroads, 'Trať 195', 'Železniční trať Rybník – Lipno nad Vltavou')
        self.addToList(self.railroads, 'Trať 196', 'Železniční trať České Budějovice – Summerau')
        self.addToList(self.railroads, 'Trať 197', 'Železniční trať Číčenice–Volary')
        self.addToList(self.railroads, 'Trať 198', 'Železniční trať Strakonice–Volary')
        self.addToList(self.railroads, 'Trať 199', 'Železniční trať České Budějovice – Gmünd')
        self.addToList(self.railroads, 'Trať 194', 'Železniční trať České Budějovice – Černý Kříž – Volary/Nové Údolí')
        self.addToList(self.railroads, 'Trať 200', 'Železniční trať Zdice–Protivín')
        self.addToList(self.railroads, 'Trať 201', 'Železniční trať Tábor–Ražice')
        self.addToList(self.railroads, 'Trať 202', 'Elektrická dráha Tábor–Bechyně')
        self.addToList(self.railroads, 'Trať 203', 'Železniční trať Březnice–Strakonice')
        self.addToList(self.railroads, 'Trať 204', 'Železniční trať Březnice – Rožmitál pod Třemšínem')
        self.addToList(self.railroads, 'Trať 210', 'Železniční trať Praha – Vrané nad Vltavou – Čerčany/Dobříš')
        self.addToList(self.railroads, 'Trať 212', 'Železniční trať Čerčany – Světlá nad Sázavou')
        self.addToList(self.railroads, 'Trať 220', 'Železniční trať Benešov u Prahy – České Budějovice')
        self.addToList(self.railroads, 'Trať 221', 'Železniční trať Praha – Benešov u Prahy')
        self.addToList(self.railroads, 'Trať 222', 'Železniční trať Benešov u Prahy – Trhový Štěpánov')
        self.addToList(self.railroads, 'Trať 223', 'Železniční trať Olbramovice–Sedlčany')
        self.addToList(self.railroads, 'Trať 224', 'Železniční trať Tábor – Horní Cerekev')
        self.addToList(self.railroads, 'Trať 225', 'Železniční trať Havlíčkův Brod – Veselí nad Lužnicí')
        self.addToList(self.railroads, 'Trať 226', 'Železniční trať České Velenice – Veselí nad Lužnicí')
        self.addToList(self.railroads, 'Trať 227', 'Železniční trať Kostelec u Jihlavy – Slavonice')
        self.addToList(self.railroads, 'Trať 228', 'Železniční trať Jindřichův Hradec – Obrataň')
        self.addToList(self.railroads, 'Trať 229', 'Železniční trať Jindřichův Hradec – Nová Bystřice')
        self.addToList(self.railroads, 'Trať 230', 'Železniční trať Kolín – Havlíčkův Brod')
        self.addToList(self.railroads, 'Trať 231', 'Železniční trať Praha – Lysá nad Labem – Kolín')
        self.addToList(self.railroads, 'Trať 232', 'Železniční trať Lysá nad Labem – Milovice')
        self.addToList(self.railroads, 'Trať 233', 'Železniční trať Čelákovice–Mochov')
        self.addToList(self.railroads, 'Trať 235', 'Železniční trať Kutná Hora – Zruč nad Sázavou')
        self.addToList(self.railroads, 'Trať 236', 'Železniční trať Čáslav–Třemošnice')
        self.addToList(self.railroads, 'Trať 237', 'Železniční trať Havlíčkův Brod – Humpolec')
        self.addToList(self.railroads, 'Trať 238', 'Železniční trať Pardubice – Havlíčkův Brod')
        self.addToList(self.railroads, 'Trať 240', 'Železniční trať Brno–Jihlava')
        self.addToList(self.railroads, 'Trať 241', 'Železniční trať Znojmo–Okříšky')
        self.addToList(self.railroads, 'Trať 242', 'Železniční trať Dobronín–Polná')
        self.addToList(self.railroads, 'Trať 243', 'Železniční trať Moravské Budějovice – Jemnice')
        self.addToList(self.railroads, 'Trať 244', 'Železniční trať Brno – Moravské Bránice – Hrušovany nad Jevišovkou/Oslavany')
        self.addToList(self.railroads, 'Trať 245', 'Železniční trať Hrušovany nad Jevišovkou – Hevlín')
        self.addToList(self.railroads, 'Trať 246', 'Železniční trať Břeclav–Znojmo')
        self.addToList(self.railroads, 'Trať 247', 'Železniční trať Břeclav–Lednice')
        self.addToList(self.railroads, 'Trať 248', 'Železniční trať Znojmo–Retz')
        self.addToList(self.railroads, 'Trať 250', 'Železniční trať Havlíčkův Brod – Kúty')
        self.addToList(self.railroads, 'Trať 251', 'Železniční trať Žďár nad Sázavou – Tišnov')
        self.addToList(self.railroads, 'Trať 252', 'Železniční trať Křižanov–Studenec')
        self.addToList(self.railroads, 'Trať 253', 'Železniční trať Vranovice–Pohořelice')
        self.addToList(self.railroads, 'Trať 254', 'Železniční trať Šakvice – Hustopeče u Brna')
        self.addToList(self.railroads, 'Trať 255', 'Železniční trať Hodonín–Zaječí')
        self.addToList(self.railroads, 'Trať 256', 'Železniční trať Čejč–Ždánice')
        self.addToList(self.railroads, 'Trať 257', 'Železniční trať Kyjov–Mutěnice')
        self.addToList(self.railroads, 'Trať 260', 'Železniční trať Brno – Česká Třebová')
        self.addToList(self.railroads, 'Trať 261', 'Železniční trať Svitavy – Žďárec u Skutče')
        self.addToList(self.railroads, 'Trať 262', 'Železniční trať Třebovice v Čechách – Skalice nad Svitavou')
        self.addToList(self.railroads, 'Trať 270', 'Železniční trať Česká Třebová – Přerov – Bohumín')
        self.addToList(self.railroads, 'Trať 271', 'Železniční trať Prostějov–Chornice')
        self.addToList(self.railroads, 'Trať 272', 'Železniční trať Rudoltice v Čechách – Lanškroun')
        self.addToList(self.railroads, 'Trať 273', 'Železniční trať Červenka–Prostějov')
        self.addToList(self.railroads, 'Trať 274', 'Železniční trať Litovel–Mladeč')
        self.addToList(self.railroads, 'Trať 275', 'Železniční trať Olomouc – Senice na Hané')
        self.addToList(self.railroads, 'Trať 276', 'Železniční trať Suchdol nad Odrou – Budišov nad Budišovkou')
        self.addToList(self.railroads, 'Trať 277', 'Železniční trať Suchdol nad Odrou – Fulnek')
        self.addToList(self.railroads, 'Trať 278', 'Železniční trať Suchdol nad Odrou – Nový Jičín')
        self.addToList(self.railroads, 'Trať 279', 'Železniční trať Studénka–Bílovec')
        self.addToList(self.railroads, 'Trať 280', 'Železniční trať Hranice na Moravě – Púchov')
        self.addToList(self.railroads, 'Trať 281', 'Železniční trať Valašské Meziříčí – Rožnov pod Radhoštěm')
        
        self.addToList(self.railroads, 'Trať 282', 'Železniční trať Vsetín – Velké Karlovice')
        self.addToList(self.railroads, 'Trať 283', 'Železniční trať Horní Lideč – Bylnice')
        self.addToList(self.railroads, 'Trať 290', 'Železniční trať Olomouc–Šumperk')
        self.addToList(self.railroads, 'Trať 291', 'Železniční trať Zábřeh na Moravě – Šumperk')
        self.addToList(self.railroads, 'Trať 292', 'Železniční trať Šumperk–Krnov')
        self.addToList(self.railroads, 'Trať 293', 'Železnice Desná')
        self.addToList(self.railroads, 'Trať 294', 'Železniční trať Hanušovice – Staré Město pod Sněžníkem')
        self.addToList(self.railroads, 'Trať 295', 'Železniční trať Lipová Lázně – Javorník ve Slezsku')
        self.addToList(self.railroads, 'Trať 296', 'Železniční trať Velká Kraš – Vidnava')
        self.addToList(self.railroads, 'Trať 297', 'Železniční trať Mikulovice – Zlaté Hory')
        self.addToList(self.railroads, 'Trať 298', 'Železniční trať Třemešná ve Slezsku – Osoblaha')
        self.addToList(self.railroads, 'Benešov u Prahy', 'Benešov')
        self.addToList(self.railroads, 'Kostelec u Jihlavy', 'Kostelec (okres Jihlava)')
        self.addToList(self.railroads, 'JHMD', 'Jindřichohradecké místní dráhy')
        self.addToList(self.railroads, 'Stará Tišnovka', 'Železniční trať Brno–Tišnov')
        
        self.addToList(self.railroads, 'Hustopeče u Brna', 'Hustopeče')
        self.addToList(self.railroads, 'Rudoltice v Čechách', 'Rudoltice')
        self.addToList(self.railroads, 'Zábřeh na Moravě', 'Zábřeh')
        self.addToList(self.railroads, 'Connex Morava', 'Arriva Morava')
        self.addToList(self.railroads, 'Staré Město pod Sněžníkem', 'Staré Město (okres Šumperk)')
        self.addToList(self.railroads, 'Javorník ve Slezsku', 'Javorník (okres Jeseník)')
        self.addToList(self.railroads, 'Třemešná ve Slezsku', 'Třemešná')
        
        self.addToList(self.railroads, 'Trať 300', 'Železniční trať Brno–Přerov')
        self.addToList(self.railroads, 'Trať 301', 'Železniční trať Nezamyslice–Olomouc')
        self.addToList(self.railroads, 'Trať 302', 'Železniční trať Nezamyslice–Morkovice')
        self.addToList(self.railroads, 'Trať 303', 'Železniční trať Kojetín – Valašské Meziříčí')
        self.addToList(self.railroads, 'Trať 305', 'Železniční trať Kroměříž–Zborovice')
        self.addToList(self.railroads, 'Trať 310', 'Železniční trať Olomouc–Opava')
        self.addToList(self.railroads, 'Trať 311', 'Železniční trať Valšov–Rýmařov')
        self.addToList(self.railroads, 'Trať 312', 'Železniční trať Bruntál – Malá Morávka')
        self.addToList(self.railroads, 'Trať 313', 'Železniční trať Milotice nad Opavou – Vrbno pod Pradědem')
        self.addToList(self.railroads, 'Trať 314', 'Železniční trať Opava východ – Svobodné Heřmanice')
        self.addToList(self.railroads, 'Trať 315', 'Železniční trať Opava východ – Hradec nad Moravicí')
        self.addToList(self.railroads, 'Trať 316', 'Železniční trať Ostrava-Svinov – Opava východ')
        self.addToList(self.railroads, 'Trať 317', 'Železniční trať Opava východ – Hlučín')
        self.addToList(self.railroads, 'Trať 318', 'Železniční trať Kravaře ve Slezsku – Chuchelná')
        self.addToList(self.railroads, 'Trať 320', 'Železniční trať Bohumín–Čadca')
        self.addToList(self.railroads, 'Trať 322', 'Železniční trať Český Těšín – Frýdek-Místek')
        self.addToList(self.railroads, 'Trať 323', 'Železniční trať Ostrava – Valašské Meziříčí')
        self.addToList(self.railroads, 'Trať 324', 'Železniční trať Frýdlant nad Ostravicí – Ostravice')
        self.addToList(self.railroads, 'Trať 325', 'Železniční trať Studénka–Veřovice')
        self.addToList(self.railroads, 'Trať 326', 'Železniční trať Hostašovice – Nový Jičín')
        self.addToList(self.railroads, 'Trať 330', 'Železniční trať Přerov–Břeclav')
        self.addToList(self.railroads, 'Trať 331', 'Železniční trať Otrokovice–Vizovice')
        self.addToList(self.railroads, 'Trať 332', 'Železniční trať Hodonín–Holíč')
        self.addToList(self.railroads, 'Trať 334', 'Železniční trať Kojetín–Tovačov')
        self.addToList(self.railroads, 'Trať 342', 'Železniční trať Bzenec – Moravský Písek')
        self.addToList(self.railroads, 'Trať 346', 'Železniční trať Újezdec u Luhačovic – Luhačovice')
        self.addToList(self.railroads, 'Staré Město u Uherského Hradiště', 'Staré Město (okres Uherské Hradiště)')
        self.addToList(self.railroads, 'Lanovka Oldřichovice – Javorový vrch', 'Lanová dráha Oldřichovice – Javorový vrch')
        self.addToList(self.railroads, 'Kravaře ve Slezsku', 'Kravaře')
        self.addToList(self.railroads, 'Ostrava-Svinov', 'Svinov (Ostrava)')
        self.addToList(self.railroads, 'OKD, Doprava', 'Advanced World Transport')
        self.addToList(self.railroads, 'Trať 901', 'Železniční trať Česká Kamenice – Česká Lípa')
        self.addToList(self.railroads, 'Lanová dráha Bohosudov – Kněžiště', 'Lanová dráha Bohosudov – Komáří vížka')
        self.addToList(self.railroads, 'Lanová dráha Janské Lázně - Černá hora', 'Lanová dráha Janské Lázně – Černá hora')
        self.addToList(self.railroads, 'Lanová dráha Pec pod Sněžkou - Sněžka', 'Lanová dráha Pec pod Sněžkou – Sněžka')
        self.addToList(self.railroads, 'Lanová dráha Oldřichovice - Javorový vrch', 'Lanová dráha Oldřichovice – Javorový vrch')
        self.addToList(self.railroads, 'Praha-Libeň', 'Nádraží Praha-Libeň')
        self.addToList(self.railroads, 'Roztoky u Prahy', 'Roztoky (okres Praha-západ)')
        self.addToList(self.railroads, 'Planá u Mariánských Lázní', 'Planá')
        self.addToList(self.railroads, 'Rudná u Prahy', 'Rudná (okres Praha-západ)')
        self.addToList(self.railroads, 'Vilémov u Kadaně', 'Vilémov (okres Chomutov)')
        self.addToList(self.railroads, 'Radonice u Kadaně', 'Radonice (okres Chomutov)')
        self.addToList(self.railroads, 'Hranice v Čechách', 'Hranice (okres Cheb)')
        self.addToList(self.railroads, 'Luby u Chebu', 'Luby')
        self.addToList(self.railroads, 'VIAMONT', 'Viamont')
        self.addToList(self.railroads, 'Nové Sedlo u Lokte', 'Nové Sedlo')
        self.addToList(self.railroads, 'Moldava v Krušných horách', 'Moldava')
        self.addToList(self.railroads, 'Teplice v Čechách', 'Teplice')
        self.addToList(self.railroads, 'Oldřichov u Duchcova', 'Oldřichov (Jeníkov)')
        self.addToList(self.railroads, 'Bílina (okres Teplice)', 'Bílina (město)')
        self.addToList(self.railroads, 'Lužná u Rakovníka', 'Lužná (okres Rakovník)')
        self.addToList(self.railroads, 'Železniční trať Česká Kamenice - Česká Lípa', 'Železniční trať Česká Kamenice – Česká Lípa')
        self.addToList(self.railroads, 'Železniční trať Velké Březno - Verneřice - Úštěk', 'Železniční trať Velké Březno – Verneřice – Úštěk')
        self.addToList(self.railroads, 'Železniční trať Frýdlant v Čechách - Heřmanice', 'Železniční trať Frýdlant v Čechách – Heřmanice')
        #self.addToList(self.railroads, 'Międzylesie', 'Mezilesí (Polsko)')
        self.addToList(self.railroads, 'Železniční trať Rudoltice v Čechách - Lanškroun', 'Železniční trať Rudoltice v Čechách – Lanškroun')
        self.addToList(self.railroads, 'Koněspřežná dráha České Budějovice - Linec', 'Koněspřežná dráha České Budějovice – Linec')
        self.addToList(self.railroads, 'Železniční trať Louky nad Olší - Doubrava - Bohumín', 'Železniční trať Louky nad Olší – Doubrava – Bohumín')
        self.addToList(self.railroads, 'Zubrnická museální železnice', 'Železniční trať Velké Březno – Verneřice – Úštěk')
        self.addToList(self.railroads, 'Železniční trať Svor - Jablonné v Podještědí', 'Železniční trať Svor – Jablonné v Podještědí')
        self.addToList(self.railroads, 'Železniční trať Brno - Tišnov', 'Železniční trať Brno–Tišnov')
        self.addToList(self.railroads, 'Železniční trať Kuřim - Veverská Bítýška', 'Železniční trať Kuřim – Veverská Bítýška')
        self.addToList(self.railroads, 'Železniční trať Petrovice u Karviné - Karviná', 'Železniční trať Petrovice u Karviné – Karviná')
        self.addToList(self.railroads, 'Železniční trať Ostrov nad Ohří - Jáchymov', 'Železniční trať Ostrov nad Ohří – Jáchymov')
        self.addToList(self.railroads, 'Železniční trať Vojkovice nad Ohří - Kyselka', 'Železniční trať Vojkovice nad Ohří – Kyselka')
        self.addToList(self.railroads, 'Železniční trať Ondrášov - Dvorce na Moravě', 'Železniční trať Ondrášov – Dvorce na Moravě')
        self.addToList(self.railroads, 'Železniční trať Ostrava - Rychvald', 'Železniční trať Ostrava–Rychvald')
        self.addToList(self.railroads, 'Železniční trať Nová huť - Vítkovické železárny', 'Železniční trať Nová huť – Vítkovické železárny')
        self.addToList(self.railroads, 'Železniční trať Nemotice - Koryčany', 'Železniční trať Nemotice–Koryčany')
        self.addToList(self.railroads, 'Železniční trať Hrušovany u Brna - Židlochovice', 'Železniční trať Hrušovany u Brna – Židlochovice')
        self.addToList(self.railroads, 'Železniční zkušební okruh u Velimi', 'Železniční zkušební okruh Cerhenice')
        #self.addToList(self.railroads, 'Benediktini', 'Řád svatého Benedikta')
        #self.addToList(self.railroads, 'Václavské náměstí (Praha)', 'Václavské náměstí')
        #self.addToList(self.railroads, 'Viktorin z Poděbrad', 'Viktorín z Poděbrad')
        self.addToList(self.railroads, 'Národní výbor (1945-1990)', 'Národní výbor (1945–1990)')
        #self.addToList(self.railroads, 'Obchodní akademie Dr. Albína Bráfa', 'Obchodní akademie Dr. Albína Bráfa a Jazyková škola s právem státní jazykové zkoušky Třebíč')
        self.addToList(self.railroads, 'Věci Veřejné', 'Věci veřejné')
        #self.addToList(self.railroads, 'Volby do zastupitelstev obcí 1994', 'Volby do zastupitelstev obcí v Česku 1994')
        #self.addToList(self.railroads, 'Volby do zastupitelstev obcí 1998', 'Volby do zastupitelstev obcí v Česku 1998')
        #self.addToList(self.railroads, 'Volby do zastupitelstev obcí 2002', 'Volby do zastupitelstev obcí v Česku 2002')
        #self.addToList(self.railroads, 'Volby do zastupitelstev obcí 2006', 'Volby do zastupitelstev obcí v Česku 2006')
        #self.addToList(self.railroads, 'Volby do zastupitelstev obcí 2010', 'Volby do zastupitelstev obcí v Česku 2010')
        self.addToList(self.railroads, 'Unie svobody - Demokratická unie', 'Unie svobody – Demokratická unie')
        #
        self.addToList(self.railroads, 'Katedrála v Remeši', 'Katedrála Notre Dame (Remeš)')
        #self.addToList(self.railroads, 'Liséna', 'Lizéna')
        self.addToList(self.railroads, 'Gotický sloh', 'Gotika')
        #self.addToList(self.railroads, 'Karel Budischowsky', 'Carl Budischowsky')
        self.addToList(self.railroads, 'Horácký fotbalový klub Třebíč', 'HFK Třebíč')
        #self.addToList(self.railroads, 'Rusko-turecká válka (1877-1878)', 'Rusko-turecká válka (1877–1878)')
        self.addToList(self.railroads, 'Zámek Třebíč', 'Třebíč (zámek)')
        #self.addToList(self.railroads, 'Doubravický hrad', 'Doubravice nad Svitavou (hrad)')
        self.addToList(self.railroads, 'Minorité', 'Řád menších bratří konventuálů')
        #self.addToList(self.railroads, 'Zdeněk ze Šternberka', 'Zdeněk Konopišťský ze Šternberka')
        #self.addToList(self.railroads, 'Štokrava', 'Stockerau')
        #self.addToList(self.railroads, 'Coelodonta antiquitatis', 'Nosorožec srstnatý')
        self.addToList(self.railroads, 'Latinsky', 'Latina')
        #self.addToList(self.railroads, 'Hej Slované', 'Hej, Slované')
        #self.addToList(self.railroads, 'Zastávka u Brna', 'Zastávka (okres Brno-venkov)')
        #self.addToList(self.railroads, 'Podklášteří (Třebíč)', 'Podklášteří')
        #self.addToList(self.railroads, 'Gustav Kliment', 'Augustin Kliment')
        #self.addToList(self.railroads, 'Městská policie', 'Obecní policie')
        #self.addToList(self.railroads, 'Stařečka (Třebíč)', 'Stařečka')
        #self.addToList(self.railroads, 'ZMVŠ', 'Západomoravská vysoká škola Třebíč')
        #self.addToList(self.railroads, 'Hvězdoňovice (okres Třebíč)', 'Hvězdoňovice')
        #self.addToList(self.railroads, 'Bačice (okres Třebíč)', 'Bačice')
        #self.addToList(self.railroads, 'Bačkovice (okres Třebíč)', 'Bačkovice')
        #self.addToList(self.railroads, 'Bochovice (okres Třebíč)', 'Bochovice')
        #self.addToList(self.railroads, 'Benetice (okres Třebíč)', 'Benetice')
        #self.addToList(self.railroads, 'Biskupice-Pulkov (okres Třebíč)', 'Biskupice-Pulkov')
        #self.addToList(self.railroads, 'Želetava (okres Třebíč)', 'Želetava')
        #self.addToList(self.railroads, 'Bohušice (okres Třebíč)', 'Bohušice')
        #self.addToList(self.railroads, 'Bochovice (okres Třebíč)', 'Bochovice')
        #self.addToList(self.railroads, 'Jaroměřice nad Rokytnou (okres Třebíč)', 'Jaroměřice nad Rokytnou')
        #self.addToList(self.railroads, 'Bransouze (okres Třebíč)', 'Bransouze')
        #self.addToList(self.railroads, 'Číchov (okres Třebíč)', 'Číchov')
        #self.addToList(self.railroads, 'Březník (okres Třebíč)', 'Březník')
        #self.addToList(self.railroads, 'Třebíč (okres Třebíč)', 'Třebíč')
        #self.addToList(self.railroads, 'Budišov (okres Třebíč)', 'Budišov')
        #self.addToList(self.railroads, 'Čáslavice (okres Třebíč)', 'Čáslavice')
        #self.addToList(self.railroads, 'Častohostice (okres Třebíč)', 'Častohostice')
        #self.addToList(self.railroads, 'Čechočovice (okres Třebíč)', 'Čechočovice')
        #self.addToList(self.railroads, 'Čechtín (okres Třebíč)', 'Čechtín')
        #self.addToList(self.railroads, 'Číhalín (okres Třebíč)', 'Číhalín')
        #self.addToList(self.railroads, 'Čikov (okres Třebíč)', 'Čikov')
        #self.addToList(self.railroads, 'Dědice (okres Třebíč)', 'Dědice')
        #self.addToList(self.railroads, 'Dolní Lažany (okres Třebíč)', 'Dolní Lažany')
        #self.addToList(self.railroads, 'Dolní Vilémovice (okres Třebíč)', 'Dolní Vilémovice')
        #self.addToList(self.railroads, 'Domamil (okres Třebíč)', 'Domamil')
        #self.addToList(self.railroads, 'Dukovany (okres Třebíč)', 'Dukovany')
        #self.addToList(self.railroads, 'Hartvíkovice (okres Třebíč)', 'Hartvíkovice')
        #self.addToList(self.railroads, 'Heraltice (okres Třebíč)', 'Heraltice')
        #self.addToList(self.railroads, 'Rouchovany (okres Třebíč)', 'Rouchovany')
        #self.addToList(self.railroads, 'Hluboké (okres Třebíč)', 'Hluboké')
        #self.addToList(self.railroads, 'Hodov (okres Třebíč)', 'Hodov')
        #self.addToList(self.railroads, 'Kralice nad Oslavou (okres Třebíč)', 'Kralice nad Oslavou')
        #self.addToList(self.railroads, 'Horní Smrčné (okres Třebíč)', 'Horní Smrčné')
        #self.addToList(self.railroads, 'Horní Vilémovice (okres Třebíč)', 'Horní Vilémovice')
        #self.addToList(self.railroads, 'Hornice (okres Třebíč)', 'Hornice')
        #self.addToList(self.railroads, 'Předín (okres Třebíč)', 'Předín')
        #self.addToList(self.railroads, 'Hrotovice (okres Třebíč)', 'Hrotovice')
        #self.addToList(self.railroads, 'Hroznatín (okres Třebíč)', 'Hroznatín')
        #self.addToList(self.railroads, 'Hvězdoňovice (okres Třebíč)', 'Hvězdoňovice')
        #self.addToList(self.railroads, 'Chotěbudice (okres Třebíč)', 'Chotěbudice')
        #self.addToList(self.railroads, 'Moravské Budějovice (okres Třebíč)', 'Moravské Budějovice')
        #self.addToList(self.railroads, 'Jakubov u Moravských Budějovic (okres Třebíč)', 'Jakubov u Moravských Budějovic')
        #self.addToList(self.railroads, 'Náměšť nad Oslavou (okres Třebíč)', 'Náměšť nad Oslavou')
        #self.addToList(self.railroads, 'Jemnice (okres Třebíč)', 'Jemnice')
        #self.addToList(self.railroads, 'Jinošov (okres Třebíč)', 'Jinošov')
        #self.addToList(self.railroads, 'Jiratice (okres Třebíč)', ' Jiratice')
        #self.addToList(self.railroads, 'Kdousov (okres Třebíč)', 'Kdousov')
        #self.addToList(self.railroads, 'Kladeruby nad Oslavou (okres Třebíč)', 'Kladeruby nad Oslavou')
        #self.addToList(self.railroads, 'Kojatice (okres Třebíč)', 'Kojatice')
        #self.addToList(self.railroads, 'Kojatín (okres Třebíč)', 'Kojatín')
        #self.addToList(self.railroads, 'Koněšín (okres Třebíč)', 'Koněšín')
        #self.addToList(self.railroads, 'Kostníky (okres Třebíč)', 'Kostníky')
        #self.addToList(self.railroads, 'Kožichovice (okres Třebíč)', 'Kožichovice')
        #self.addToList(self.railroads, 'Stařeč (okres Třebíč)', 'Stařeč')
        #self.addToList(self.railroads, 'Krahulov (okres Třebíč)', 'Krahulov')
        #self.addToList(self.railroads, 'Nové Syrovice (okres Třebíč)', 'Nové Syrovice')
        #self.addToList(self.railroads, 'Krokočín (okres Třebíč)', 'Krokočín')
        #self.addToList(self.railroads, 'Průtok (hydrologie)', 'Průtok vodního toku')
        #self.addToList(self.railroads, 'Teplická Bystřice', 'Bystřice (přítok Bíliny)')
        self.addToList(self.railroads, 'Rameno řeky', 'Říční rameno')
        self.addToList(self.railroads, 'Koryto řeky', 'Říční koryto')
        #self.addToList(self.railroads, 'MKAD', 'Moskevský dálniční okruh')
        #self.addToList(self.railroads, 'Rokytná (park)', 'Přírodní park Rokytná')
        self.addToList(self.railroads, 'Železniční trať Brno - Jihlava', 'Železniční trať Brno–Jihlava')
        #self.addToList(self.railroads, 'Zámek v Třebíči', 'Třebíč (zámek)')
        self.addToList(self.railroads, 'Světová hospodářská krize', 'Velká hospodářská krize')
        self.addToList(self.railroads, 'JE Dukovany', 'Jaderná elektrárna Dukovany')
        self.addToList(self.railroads, 'JEDU', 'Jaderná elektrárna Dukovany')
        self.addToList(self.railroads, 'TWh', 'Watthodina')
        #self.addToList(self.railroads, 'Zahrádkářská kolonie', 'Zahrádkářská osada')
        self.addToList(self.railroads, 'Rolník', 'Zemědělec')
        #self.addToList(self.railroads, 'Třebíčský židovský hřbitov', 'Židovský hřbitov v Třebíči')
        #self.addToList(self.railroads, 'Kondominium (právo)', 'Kondominium (mezinárodní právo)')
        #self.addToList(self.railroads, 'Přední synagoga', 'Přední synagoga (Třebíč)')
        #self.addToList(self.railroads, 'Zadní synagoga', 'Zadní synagoga (Třebíč)')
        #self.addToList(self.railroads, 'Lokativ', 'Lokál')
        self.addToList(self.railroads, 'Bota', 'Obuv')
        self.addToList(self.railroads, 'Export', 'Vývoz')
        self.addToList(self.railroads, 'Import', 'Dovoz')
        #self.addToList(self.railroads, 'Západomoravské strojírny', 'Uniplet Třebíč')
        #self.addToList(self.railroads, 'Ministerstvo školství, mládeže a tělovýchovy ČR', 'Ministerstvo školství, mládeže a tělovýchovy České republiky')
        #self.addToList(self.railroads, 'Ministerstvo kultury', 'Ministerstvo kultury České republiky')
        self.addToList(self.railroads, 'Eratický balvan', 'Bludný balvan')
        #self.addToList(self.railroads, 'Třebíčský evangelický kostel', 'Evangelický kostel (Třebíč)')
        #self.addToList(self.railroads, 'Marie (matka Ježíšova)', 'Maria (matka Ježíšova)')
        #self.addToList(self.railroads, 'Kapucíni', 'Řád menších bratří kapucínů')
        #self.addToList(self.railroads, 'Služebnost', 'Věcné břemeno')
        #self.addToList(self.railroads, 'Vlastimil Protivínský', 'Vlastimil Vojtěch Protivínský')
        self.addToList(self.railroads, 'SPŠT', 'Střední průmyslová škola Třebíč')
        self.addToList(self.railroads, 'Junák - svaz skautů a skautek ČR', 'Junák – svaz skautů a skautek ČR')
        self.addToList(self.railroads, 'IČO', 'Identifikační číslo osoby')
        #self.addToList(self.railroads, 'WAGGGS', 'Světové sdružení skautek')
        #self.addToList(self.railroads, 'Karel XIV. Jan', 'Karel XIV.')
        self.addToList(self.railroads, 'Dobytčí mor', 'Mor skotu')
        self.addToList(self.railroads, 'Táborité', 'Táborský svaz')
        self.addToList(self.railroads, 'Pražané', 'Pražský svaz')
        #self.addToList(self.railroads, 'Jan Žižka z Trocnova', 'Jan Žižka')
        self.addToList(self.railroads, 'Wi-fi', 'Wi-Fi')
        #self.addToList(self.railroads, 'Chinaski (hudební skupina)', 'Chinaski')
        self.addToList(self.railroads, 'Tata bojs', 'Tata Bojs')
        self.addToList(self.railroads, 'Tatabojs', 'Tata Bojs')
        self.addToList(self.railroads, 'Kryštof (skupina)', 'Kryštof (hudební skupina)')
        self.addToList(self.railroads, 'Vypsaná fixa', 'Vypsaná fiXa')
        #self.addToList(self.railroads, '1. světová válka', 'První světová válka')
        #self.addToList(self.railroads, 'Fotbalista', 'Fotbal')
        self.addToList(self.railroads, 'Fotbalový záložník', 'Záložník (fotbal)')
        self.addToList(self.railroads, 'Fotbalový obránce', 'Obránce (fotbal)')
        #self.addToList(self.railroads, 'Česká republika', 'Česko')
        #self.addToList(self.railroads, 'Paleontolog', 'Paleontologie')
        #self.addToList(self.railroads, 'Scenárista', 'Scénář')
        #self.addToList(self.railroads, 'Československá fotbalová liga', 'Fotbalová liga Československa')
        #self.addToList(self.railroads, 'Řapík', 'List')
        self.addToList(self.railroads, 'Televizní seriál', 'Seriál')
        #self.addToList(self.railroads, 'Děj', 'Proces')
        self.addToList(self.railroads, 'USA', 'Spojené státy americké')
        self.addToList(self.railroads, 'Francouzská obec', 'Obec (Francie)')
        #self.addToList(self.railroads, 'Saint-Barthélemy', 'Svatý Bartoloměj (ostrov)')
        #self.addToList(self.railroads, 'Publicista', 'Publicistika')
        #self.addToList(self.railroads, 'Grafik', 'Grafika')
        #self.addToList(self.railroads, 'Bubeník', 'Bicí souprava')
        #self.addToList(self.railroads, 'Ledviny', 'Ledvina')
        #self.addToList(self.railroads, 'ČEZ Aréna (Plzeň)', 'Home Monitoring Aréna')
        #self.addToList(self.railroads, 'Duhová arena', 'Tipsport Arena (Pardubice)')
        #self.addToList(self.railroads, 'ČEZ Arena (Pardubice)', 'Tipsport Arena (Pardubice)')
        #self.addToList(self.railroads, 'Kajot Arena', 'hala Rondo')
        #self.addToList(self.railroads, 'Tesla Aréna', 'Tipsport Arena (Praha)')
        #self.addToList(self.railroads, 'Tesla Arena', 'Tipsport Arena (Praha)')
        #self.addToList(self.railroads, 'Tipsport arena (Praha)', 'Tipsport Arena (Praha)')
        #self.addToList(self.railroads, 'Lipovské úpolínové louky', 'Lipovské upolínové louky')
        self.addToList(self.railroads, 'Okresy na území Česka', 'Okresy v Česku')
        self.addToList(self.railroads, 'Slovenské kraje', 'Kraje na Slovensku')
        self.addToList(self.railroads, 'Pole (zemědělství)', 'Pole')
        self.addToList(self.railroads, 'Půda (pedologie)', 'Půda')
        self.addToList(self.railroads, 'hlava (anatomie)', 'hlava')
        
        self.addToList(self.railroads, 'Invaze do Československa', 'Invaze vojsk Varšavské smlouvy do Československa')
        self.addToList(self.railroads, 'Velká vlastenecká válka', 'Východní fronta (druhá světová válka)')
        self.addToList(self.railroads, 'Disk Operating System', 'DOS')
        self.addToList(self.railroads, 'Evoluční teorie', 'Evoluce')
        self.addToList(self.railroads, 'Halogen', 'Halogeny')
        self.addToList(self.railroads, 'Redukce (chemie)', 'Redoxní reakce')
        self.addToList(self.railroads, 'Oxidace', 'Redoxní reakce')
        self.addToList(self.railroads, 'Zásada (chemie)', 'Zásady (chemie)')
        self.addToList(self.railroads, 'Kyselina', 'Kyseliny')
        
    def addToList(self, list, key, value):
        list[key] = value

    def runAll(self):
        self.czechRailroads()

        for i in self.railroads:
            self.page = i
            self.newLink = self.railroads[i]

            print("-------------------------------------")
            print(self.page)
            print(self.newLink)
            print("-------------------------------------")
            self.getPages()


    def getPages(self):
        site = pywikibot.Site('cs', 'wikipedia');
        pages = pywikibot.Page(site, self.page).backlinks()
        countPages = pywikibot.Page(site, self.page).backlinks()
        
        countOfPages = sum(1 for i in countPages)
        
        for page in pages:
            countOfPages = countOfPages - 1
            if (countOfPages % 10 == 0):
                print("-------------------------------------")
                print(u"zbyva: " + countOfPages)
                print("-------------------------------------")
            self.pageRun(page)

    def pageRun(self, page):
        text = page.get()
        count = text.count(self.page)
        countLower = text.count(self.page.lower())
        #page.text = text.replace(self.page, self.newLink)
        #print page.title()
        string = self.page.replace('(', '\(').replace(')', '\)')
        
        remy = re.search(u'\[\[(' + string + u')\]\]', text, flags=re.IGNORECASE)
        if remy:          
            newlink = remy.group(1)
            page.text = re.sub(u'\[\[(' + string + u')\]\]',u'[[' + self.newLink +  u'|' + newlink + u']]', text, flags=re.IGNORECASE)
            #page.text = re.sub(u'\[\[(' + string + u')\]\]',u'[[' + self.newLink + u']]', text, flags=re.IGNORECASE)
        
        text = page.text
        page.text = re.sub(u'\[\[' + string + u'\|', u'[[' + self.newLink + u'|', text, flags=re.IGNORECASE)
        
        try:
            page.save(comment=u'narovnání odkazu - ' + self.page + u' -> ' + self.newLink)
            #print "save"
        except pywikibot.exceptions.LockedPage:
            print('locked')
        except pywikibot.exceptions.Error:
            print(page.title())
            print('err')
        if self.firstStart:
            sys.exit()

class DeleteCoordsFromTpl():

    def __init__(self):
        print("delete coords")

    def getPages(self):
        site = pywikibot.Site('cs', 'wikipedia')
        infobox = pywikibot.Page(site, u'Šablona:Infobox - česká obec')
        backlinks = infobox.embeddedin()

        #re.search('')

class PivoBox():

    def __init__(self):
        print("pivobox")

    def getPages(self):
        site = pywikibot.Site('cs', 'wikipedia')
        infobox = pywikibot.Page(site, u'Šablona:Infobox Krasobruslař')
        backlinks = infobox.embeddedin()
        for page in backlinks:

            reg = re.compile(u'\|( *)(obrázek)(.)*=(.)*\[\[(soubor|Image|image|Soubor|file|File):(.+)\|(.*)\|(.*)\|(.*)\]\]',flags=re.IGNORECASE)
            reg2 = re.compile(u'\|( *)(obrázek)(.)*=(.)*\[\[(soubor|Image|image|Soubor|file|File):(.+)\|(.*)\|(.*)\]\]',flags=re.IGNORECASE)
            reg3 = re.compile(u'\|( *)(obrázek)(.)*=(.)*\[\[(soubor|Image|image|Soubor|file|File):(.+)\|(.*)\]\]',flags=re.IGNORECASE)
            reg4 = re.compile(u'\|( *)(obrázek)(.)*=(^[a-z]*)',flags=re.IGNORECASE)
            reg5 = re.compile(u'\|( *)(popisek foto)(.)*=(.*)',flags=re.IGNORECASE)
            #print match

            match = reg.search(page.get())
            match2 = reg2.search(page.get())
            match3 = reg3.search(page.get())
            match4 = reg4.search(page.get())
            save = True
            if match:
                page.text = reg.sub(u'| obrázek = ' + match.group(6),page.get());
                page.text = reg5.sub(u'| popisek foto = ' + match.group(9), page.text)
                
                #print match.group(6)
                #print u'_____'+match.group(9)
            else:
                if (match2):
                    page.text = reg2.sub(u'| obrázek = ' + match2.group(6),page.get())
                    if not match2.group(8).find(u'px'):
                        #print "fsd";
                        text = ''
                        page.text = reg5.sub(u'| popisek foto = ' + match2.group(8), text)

                else:
                    if match3:
                        page.text = reg3.sub(u'| obrázek = ' + match3.group(6),page.get())
                        #text2 = reg5.sub(u'| popisek foto = ' + match.group(9), text)
                    else:
                        if match4:                            
                            page.text = reg4.sub(u'| obrázek = ' + match4.group(3),page.get())
                            

                        else:
                            save = False
                            print(page.title())
                #print "fds"
            #print
            #print '-------------------------------------------------------------------------------'
            if save:
                page.save(comment=u'oprava fotografie v infoboxu dle standardu, tj. pouze jmeno souboru')



#pivo = PivoBox()
#pivo.getPages()

class GetData():

    URL = "http://rejskol.msmt.cz/VREJVerejne/VerejneRozhrani.aspx"
    BASE_URL = "http://rejskol.msmt.cz"

    def __init__(self):
        print("get")
        self.pravnickaOsoba = {}
        self.tableData = []
        self.schools = []
        self.viewstate = ""
        self.eventval = ""
        self.soup = False
        self.session = requests.Session()
        self.params = {}
        self.redizo = ""
        self.obory = []
        self.oboryFinal = []


    def getViewState(self):
        #Finding viewstate
        viewstate = self.soup.findAll("input", {"type" : "hidden", "name" : "__VIEWSTATE"})
        self.viewstate = viewstate[0]['value']
        #print v

    def getEventVal(self):
        #Finding eventvalidation
        eventval = self.soup.findAll("input", {"type" : "hidden", "name" : "__EVENTVALIDATION"})
        self.eventval = eventval[0]['value']
        #print e

    def getBaseData(self):
        c = self.session.get(self.URL)

        self.soup = BeautifulSoup(c.content)
        self.getViewState()
        self.getEventVal()

        #Setting Parameters
        self.params = {
          '__VIEWSTATE' : self.viewstate,
          'ctl17_txt'  : self.redizo,
          'btnVybrat'   : 'Vybrat',
          '__EVENTVALIDATION' : self.eventval
          #Need ASP.NET_SessionId Key : Value here
        }

    def getOborData(self, oborCount, href):
        obor = self.session.get(self.BASE_URL + href['href'][2:])
        soupObor = BeautifulSoup(obor.content)

        if (oborCount > 1):
            id = 'DataListOboryVzdelani'+str(oborCount)
        else:
            id = 'DataListOboryVzdelani'

        table = soupObor.find('table', {'id' : id})
        if (table == None):
            return None
        rows = table.find_all('tr')

        tableObor = []
        for row in rows:
            cols = row.find_all('td')
            if (len(cols) > 1):
                cols = [ele.text.strip() for ele in cols]
                tableObor.append([ele for ele in cols]) # Get rid of empty values

        #if (oborCount > 1):
            #tableObor.pop(0)

        #print tableObor

        return tableObor

    def preparePravnickaOsobaTable(self, soup):
        table = soup.find('table', {'id' : 'DataListSeznam'})
        rows = table.find_all('tr', recursive=False)
        print(table)
        for row in rows:
            #print row
            row2 = row.find('tr')
            if (row2 != None):
                cols = row2.find_all('td')
                cols = [ele.text.strip() for ele in cols]

                self.tableData.append([ele for ele in cols]) # Get rid of empty values
            else:
                cols = row.find_all('td')
                cols = [ele.text.strip() for ele in cols]
                self.tableData.append([ele for ele in cols]) # Get rid of empty values



        self.repairTableData()

    def getPravnickaOsobaData(self, osoba):
        soup = BeautifulSoup(osoba.content)

        if (soup.find_all('span', {'id' : 'lblZrizovatel'})):
            zrizovatel = soup.find_all('span', {'id' : 'lblZrizovatel'})[0].text
        else:
            zrizovatel = u"Právnická osoba"

        pravnickaOsoba = {
            'nazev' : soup.find_all('span', {'id' : 'lblJmenoPravOsoby'})[0].text,
            'adresa' : soup.find_all('span', {'id' : 'lblAdresa'})[0].text,
            'misto' : soup.find_all('span', {'id' : 'lblMisto'})[0].text,
            'psc' : soup.find_all('span', {'id' : 'lblPsc'})[0].text,
            'ico' : soup.find_all('span', {'id' : 'lblICO'})[0].text,
            'red_izo' : soup.find_all('span', {'id' : 'lblRedIzo'})[0].text,
            'reditel' : soup.find_all('span', {'id' : 'lblReditel'})[0].text,
            'pravni_forma' : soup.find_all('span', {'id' : 'lblPravniForma'})[0].text,
            'zrizovatel' : zrizovatel
        }

        self.pravnickaOsoba = pravnickaOsoba

        self.preparePravnickaOsobaTable(soup)

        #data = {'ctl17_txt':red_izo, 'btnVybrat':'Vybrat'}
        #req = requests.Request('POST', url, data, cookies=mycookies)
        #post = s.prepare_request(req)

        #resp = s.send(post)

    def postAndGetData(self):
        #Posting
        post = self.session.post(self.URL, data=self.params, cookies=self.session.cookies)
        #print(post.content)
        soup = BeautifulSoup(post.content)
        pubuidpo = soup.findAll("a", text="Detail")

        prav_osoba = self.session.get(self.BASE_URL + pubuidpo[0]['href'][2:])
        pubuidpo.pop(0)
        oborCount = 1

        for hrefs in pubuidpo:
            ob = self.getOborData(oborCount, hrefs)
            if (ob):
                self.obory.append(ob)
                oborCount = oborCount + 1


        self.repairOboryTableData()
        self.getPravnickaOsobaData(prav_osoba)

    def getList(self, red_izo):
        self.redizo = red_izo

        #print red_izo
        #sys.exit()

        self.getBaseData()
        self.postAndGetData()

    def repairTableData(self):
        #print self.tableData[0]
        #print self.tableData[1]
        #print self.tableData[2]
        self.tableData.pop(0)
        header = self.tableData[0]
        self.tableData.pop(0)

        for td in self.tableData:
            school = {}
            i = 0
            countlen = 0
            for l in td:
                countlen = countlen + 1

            if (countlen == 9):
                td.pop(0)

            schoolIsActive = False
            #print header
            print(td)
            for head in header:
                school[head] = td[i]
                if (head == u'Platnost zařízení'):
                    schoolIsActive = True


                i = i + 1
            if (u'Platnost zařízení' in school):
                school.pop(u'Platnost zařízení')
            if (schoolIsActive):
                self.schools.append(school)

            #print td

    def repairOboryTableData(self):

        for oborLine in self.obory:
            header = oborLine[0]
            oborLine.pop(0)

            for td in oborLine:
                obor = {}
                i = 0

                schoolIsActive = False
                oborIsActive = False
                if (len(td) == len(header)):
                    for head in header:
                        #print head
                        #print td[i]
                        obor[head] = td[i]
                        if (head == u'Platnost'):
                            oborIsActive = True


                        i = i + 1;

                    if u'Platnost' in obor:
                        obor.pop(u'Platnost')
                    if (oborIsActive):
                        self.oboryFinal.append(obor)

        #print self.oboryFinal

            #print td


class excel_semicolon(csv.Dialect):
    """Describe the usual properties of Excel-generated CSV files."""
    delimiter = ';'
    quotechar = '"'
    doublequote = True
    skipinitialspace = False
    lineterminator = '\r\n'
    quoting = csv.QUOTE_MINIMAL

# create a peewee database instance -- our models will use this database to
# persist information
database = peewee.MySQLDatabase('schools', password="mamppro", user="root")

# model definitions -- the standard "pattern" is to define a base model class
# that specifies which database to use.  then, any subclasses will automatically
# use the correct storage.
class BaseModel(peewee.Model):
    class Meta:
        database = database

# the user model specifies its fields (or columns) declaratively, like django
class Skola(BaseModel):
    wikiname= peewee.CharField()
    phone= peewee.CharField()
    county = peewee.CharField()
    cityname = peewee.CharField()
    city = peewee.CharField()
    email = peewee.CharField()
    email2 = peewee.CharField()
    shortname = peewee.CharField()
    creator = peewee.CharField()
    orp = peewee.CharField()
    fullname = peewee.CharField()
    web = peewee.CharField()
    redizo = peewee.CharField(unique=True)
    ic = peewee.CharField()
    street = peewee.CharField()
    streetnum = peewee.CharField()
    psc = peewee.CharField()
    director = peewee.CharField()
    fax = peewee.CharField()

    class Meta:
        order_by = ('wikiname',)

# a dead simple one-to-many relationship: one user has 0..n messages, exposed by
# the foreign key.  because we didn't specify, a users messages will be accessible
# as a special attribute, User.message_set
class Osoba(BaseModel):
    skola = peewee.ForeignKeyField(Skola)
    redizo = peewee.CharField(unique=True)
    ic = peewee.CharField()
    pravni_forma = peewee.CharField()
    director = peewee.CharField()
    place = peewee.CharField()
    creator = peewee.CharField()
    psc = peewee.CharField()
    creator = peewee.CharField()
    street = peewee.CharField()
    name = peewee.CharField()

    class Meta:
        order_by = ('name',)

class Obor(BaseModel):
    skola = peewee.ForeignKeyField(Skola)
    id_zarizeni = peewee.CharField()
    type = peewee.CharField()
    capacity = peewee.CharField()
    lang = peewee.CharField()
    street = peewee.CharField()
    typecode = peewee.CharField()
    city = peewee.CharField()
    platne = peewee.BooleanField()

class My():
    def create_tables(self):
        database.connect()
        database.create_tables([Skola, Osoba, Obor])

#n = My();
#n.create_tables()



class School():
    def __init__(self):
        print("school")
        self.file = ""
        self.result = []
        self.getdata = GetData()
        self.rez_izo = ""
        self.schoolInfo = {}
        self.okresy = {}
        self.kraje = {}
        self.createOkresy()
        self.createKraje()
        self.entityType = "gymnazium"
        self.completeLog = MyLogger.Logger(self.entityType, 'complete')

    def loadFile(self):
        csv.register_dialect("excel_semicolon", excel_semicolon)
        reader = csv.DictReader(open(self.file), dialect='excel_semicolon')

        for row in reader:
            self.result.append(row)

    def getInfo(self):
        for school in self.result:
            self.schoolInfo = school

            schoolModel = Skola()
            schoolModel.wikiname = self.schoolInfo['Wiki']
            schoolModel.phone = self.schoolInfo['Telefon']
            schoolModel.county = self.schoolInfo['Uzemi']
            schoolModel.cityname = self.schoolInfo['Nazev mesto']
            schoolModel.city = self.schoolInfo['Misto']
            schoolModel.email = self.schoolInfo['Email 1']
            schoolModel.email2 = self.schoolInfo['Email 2']
            schoolModel.shortname = self.schoolInfo['Zkraceny nazev']
            schoolModel.creator = self.schoolInfo['Zrizovatel']
            schoolModel.orp = self.schoolInfo['ORP']
            schoolModel.fullname = self.schoolInfo['Plny nazev']
            schoolModel.web = self.schoolInfo['WWW']
            schoolModel.redizo = self.schoolInfo['RED_IZO']
            schoolModel.ic = self.schoolInfo['ICO']
            schoolModel.street = self.schoolInfo['Ulice']
            schoolModel.psc = self.schoolInfo['PSC']
            schoolModel.director = self.schoolInfo['Reditel']
            schoolModel.fax = self.schoolInfo['Fax']
            schoolModel.streetnum = self.schoolInfo['c.p.']
            schoolModel.save()
            #print self.schoolInfo
            #sys.exit();



            #if (self.schoolInfo['Wiki'] == ''):
            #    self.rez_izo = school['RED_IZO']
            #    if (not self.completeLog.isCompleteFile(school['RED_IZO'])):
            #        self.getdata.getList(self.rez_izo)
            #        #print school
            #        #print self.getdata.pravnickaOsoba
            #        print self.getdata.schools
            #        sys.exit()
            #        #self.savePage()
            #        self.completeLog.log(school['RED_IZO'])

            #else:
            #    pass

    def createOkresy(self):
        self.okresy[u'CZ0100'] = u'Praze'
        self.okresy[u'CZ020C'] = u'Rakovník'
        self.okresy[u'CZ020B'] = u'Příbram'
        self.okresy[u'CZ020A'] = u'Praha-západ'
        self.okresy[u'CZ0209'] = u'Praha-východ'
        self.okresy[u'CZ0208'] = u'Nymburk'
        self.okresy[u'CZ0207'] = u'Mladá Boleslav'
        self.okresy[u'CZ0206'] = u'Mělník'
        self.okresy[u'CZ0205'] = u'Kutná Hora'
        self.okresy[u'CZ0204'] = u'Kolín'
        self.okresy[u'CZ0203'] = u'Kladno'
        self.okresy[u'CZ0202'] = u'Beroun'
        self.okresy[u'CZ0201'] = u'Benešov'
        self.okresy[u'CZ0317'] = u'Tábor'
        self.okresy[u'CZ0316'] = u'Strakonice'
        self.okresy[u'CZ0315'] = u'Prachatice'
        self.okresy[u'CZ0314'] = u'Písek'
        self.okresy[u'CZ0313'] = u'Jindřichův Hradec'
        self.okresy[u'CZ0312'] = u'Český Krumlov'
        self.okresy[u'CZ0311'] = u'České Budějovice'
        self.okresy[u'CZ0327'] = u'Tachov'
        self.okresy[u'CZ0326'] = u'Rokycany'
        self.okresy[u'CZ0325'] = u'Plzeň-sever'
        self.okresy[u'CZ0324'] = u'Plzeň-jih'
        self.okresy[u'CZ0323'] = u'Plzeň-město'
        self.okresy[u'CZ0322'] = u'Klatovy'
        self.okresy[u'CZ0321'] = u'Domažlice'
        self.okresy[u'CZ0413'] = u'Sokolov'
        self.okresy[u'CZ0412'] = u'Karlovy Vary'
        self.okresy[u'CZ0411'] = u'Cheb'
        self.okresy[u'CZ0427'] = u'Ústí nad Labem'
        self.okresy[u'CZ0426'] = u'Teplice'
        self.okresy[u'CZ0425'] = u'Most'
        self.okresy[u'CZ0424'] = u'Louny'
        self.okresy[u'CZ0423'] = u'Litoměřice'
        self.okresy[u'CZ0422'] = u'Chomutov'
        self.okresy[u'CZ0421'] = u'Děčín'
        self.okresy[u'CZ0514'] = u'Semily'
        self.okresy[u'CZ0513'] = u'Liberec'
        self.okresy[u'CZ0512'] = u'Jablonec nad Nisou'
        self.okresy[u'CZ0511'] = u'Česká Lípa'
        self.okresy[u'CZ0525'] = u'Trutnov'
        self.okresy[u'CZ0524'] = u'Rychnov nad Kněžnou'
        self.okresy[u'CZ0523'] = u'Náchod'
        self.okresy[u'CZ0522'] = u'Jičín'
        self.okresy[u'CZ0521'] = u'Hradec Králové'
        self.okresy[u'CZ0534'] = u'Ústí nad Orlicí'
        self.okresy[u'CZ0533'] = u'Svitavy'
        self.okresy[u'CZ0532'] = u'Pardubice'
        self.okresy[u'CZ0531'] = u'Chrudim'
        self.okresy[u'CZ0635'] = u'Žďár nad Sázavou'
        self.okresy[u'CZ0634'] = u'Třebíč'
        self.okresy[u'CZ0632'] = u'Jihlava'
        self.okresy[u'CZ0631'] = u'Havlíčkův Brod'
        self.okresy[u'CZ0633'] = u'Pelhřimov'
        self.okresy[u'CZ0647'] = u'Znojmo'
        self.okresy[u'CZ0646'] = u'Vyškov'
        self.okresy[u'CZ0645'] = u'Hodonín'
        self.okresy[u'CZ0644'] = u'Břeclav'
        self.okresy[u'CZ0643'] = u'Brno-venkov'
        self.okresy[u'CZ0642'] = u'Brno-město'
        self.okresy[u'CZ0641'] = u'Blansko'
        self.okresy[u'CZ0711'] = u'Jeseník'
        self.okresy[u'CZ0715'] = u'Šumperk'
        self.okresy[u'CZ0714'] = u'Přerov'
        self.okresy[u'CZ0712'] = u'Olomouc'
        self.okresy[u'CZ0713'] = u'Prostějov'
        self.okresy[u'CZ0806'] = u'Ostrava-město'
        self.okresy[u'CZ0805'] = u'Opava'
        self.okresy[u'CZ0804'] = u'Nový Jičín'
        self.okresy[u'CZ0803'] = u'Karviná'
        self.okresy[u'CZ0802'] = u'Frýdek-Místek'
        self.okresy[u'CZ0801'] = u'Bruntál'
        self.okresy[u'CZ0723'] = u'Vsetín'
        self.okresy[u'CZ0722'] = u'Uherské Hradiště'
        self.okresy[u'CZ0721'] = u'Kroměříž'
        self.okresy[u'CZ0724'] = u'Zlín'

    def createKraje(self):
        self.kraje[u'CZ0100'] = u'v Praze'
        self.kraje[u'CZ020C'] = u've Středočeském kraji'
        self.kraje[u'CZ020B'] = u've Středočeském kraji'
        self.kraje[u'CZ020A'] = u've Středočeském kraji'
        self.kraje[u'CZ0209'] = u've Středočeském kraji'
        self.kraje[u'CZ0208'] = u've Středočeském kraji'
        self.kraje[u'CZ0207'] = u've Středočeském kraji'
        self.kraje[u'CZ0206'] = u've Středočeském kraji'
        self.kraje[u'CZ0205'] = u've Středočeském kraji'
        self.kraje[u'CZ0204'] = u've Středočeském kraji'
        self.kraje[u'CZ0203'] = u've Středočeském kraji'
        self.kraje[u'CZ0202'] = u've Středočeském kraji'
        self.kraje[u'CZ0201'] = u've Středočeském kraji'
        self.kraje[u'CZ0317'] = u'v Jihočeském kraji'
        self.kraje[u'CZ0316'] = u'v Jihočeském kraji'
        self.kraje[u'CZ0315'] = u'v Jihočeském kraji'
        self.kraje[u'CZ0314'] = u'v Jihočeském kraji'
        self.kraje[u'CZ0313'] = u'v Jihočeském kraji'
        self.kraje[u'CZ0312'] = u'v Jihočeském kraji'
        self.kraje[u'CZ0311'] = u'v Jihočeském kraji'
        self.kraje[u'CZ0327'] = u'v Plzeňském kraji'
        self.kraje[u'CZ0326'] = u'v Plzeňském kraji'
        self.kraje[u'CZ0325'] = u'v Plzeňském kraji'
        self.kraje[u'CZ0324'] = u'v Plzeňském kraji'
        self.kraje[u'CZ0323'] = u'v Plzeňském kraji'
        self.kraje[u'CZ0322'] = u'v Plzeňském kraji'
        self.kraje[u'CZ0321'] = u'v Plzeňském kraji'
        self.kraje[u'CZ0413'] = u'v Karlovarském kraji'
        self.kraje[u'CZ0412'] = u'v Karlovarském kraji'
        self.kraje[u'CZ0411'] = u'v Karlovarském kraji'
        self.kraje[u'CZ0427'] = u'v Ústeckém kraji'
        self.kraje[u'CZ0426'] = u'v Ústeckém kraji'
        self.kraje[u'CZ0425'] = u'v Ústeckém kraji'
        self.kraje[u'CZ0424'] = u'v Ústeckém kraji'
        self.kraje[u'CZ0423'] = u'v Ústeckém kraji'
        self.kraje[u'CZ0422'] = u'v Ústeckém kraji'
        self.kraje[u'CZ0421'] = u'v Ústeckém kraji'
        self.kraje[u'CZ0514'] = u'v Libereckém kraji'
        self.kraje[u'CZ0513'] = u'v Libereckém kraji'
        self.kraje[u'CZ0512'] = u'v Libereckém kraji'
        self.kraje[u'CZ0511'] = u'v Libereckém kraji'
        self.kraje[u'CZ0525'] = u'v Libereckém kraji'
        self.kraje[u'CZ0524'] = u'v Královéhradeckém kraji'
        self.kraje[u'CZ0523'] = u'v Královéhradeckém kraji'
        self.kraje[u'CZ0522'] = u'v Královéhradeckém kraji'
        self.kraje[u'CZ0521'] = u'v Královéhradeckém kraji'
        self.kraje[u'CZ0534'] = u'v Pardubickém kraji'
        self.kraje[u'CZ0533'] = u'v Pardubickém kraji'
        self.kraje[u'CZ0532'] = u'v Pardubickém kraji'
        self.kraje[u'CZ0531'] = u'v Pardubickém kraji'
        self.kraje[u'CZ0635'] = u'v kraji Vysočina'
        self.kraje[u'CZ0634'] = u'v kraji Vysočina'
        self.kraje[u'CZ0632'] = u'v kraji Vysočina'
        self.kraje[u'CZ0631'] = u'v kraji Vysočina'
        self.kraje[u'CZ0633'] = u'v kraji Vysočina'
        self.kraje[u'CZ0647'] = u'v Jihomoravském kraji'
        self.kraje[u'CZ0646'] = u'v Jihomoravském kraji'
        self.kraje[u'CZ0645'] = u'v Jihomoravském kraji'
        self.kraje[u'CZ0644'] = u'v Jihomoravském kraji'
        self.kraje[u'CZ0643'] = u'v Jihomoravském kraji'
        self.kraje[u'CZ0642'] = u'v Jihomoravském kraji'
        self.kraje[u'CZ0641'] = u'v Jihomoravském kraji'
        self.kraje[u'CZ0711'] = u'v Olomouckém kraji'
        self.kraje[u'CZ0715'] = u'v Olomouckém kraji'
        self.kraje[u'CZ0714'] = u'v Olomouckém kraji'
        self.kraje[u'CZ0712'] = u'v Olomouckém kraji'
        self.kraje[u'CZ0713'] = u'v Olomouckém kraji'
        self.kraje[u'CZ0806'] = u'v Moravskoslezském kraji'
        self.kraje[u'CZ0805'] = u'v Moravskoslezském kraji'
        self.kraje[u'CZ0804'] = u'v Moravskoslezském kraji'
        self.kraje[u'CZ0803'] = u'v Moravskoslezském kraji'
        self.kraje[u'CZ0802'] = u'v Moravskoslezském kraji'
        self.kraje[u'CZ0801'] = u'v Moravskoslezském kraji'
        self.kraje[u'CZ0723'] = u've Zlínském kraji'
        self.kraje[u'CZ0722'] = u've Zlínském kraji'
        self.kraje[u'CZ0721'] = u've Zlínském kraji'
        self.kraje[u'CZ0724'] = u've Zlínském kraji'

    def createPageText(self):
        pageText = u'';
        pageText = pageText + self.createInfobox() + self.getStartParagraph() + self.createTable() + self.createOboryTable() + self.getEndParagraph() + self.getCategoriesText()
        return pageText

    def getCategoriesText(self):
        okres = self.okresy[self.schoolInfo['Území']]
        kraj = self.kraje[self.schoolInfo['Území']]
        cats = []

        if (okres == u'Praze'):
            cats.append(u'[[Kategorie:Střední školy v Praze]]')
        else:
            cats.append(u'[[Kategorie:Střední školy v okrese '+okres+u']]')

        cats.append(u'[[Kategorie:Gymnázia '+kraj+u']]')

        text = u''''''
        for cat in cats:
            text = text + u'''
            ''' + cat + u''''''
        return text


    def savePage(self):
        site = pywikibot.Site('cs', 'wikipedia')
        #newPage = pywikibot.Page(site, unicode(self.schoolInfo[u'Zkraceny nazev'], 'utf-8'))

        names = self.checkNames()

        print(names)

        newPage = pywikibot.Page(site, names[u'Nazev mesto'])
        newPage.text = self.createPageText()
        print(self.createPageText())
        #newPage.save(comment=u'Nová stránka, Gymnázium' + names[u'Nazev mesto'])

        mesto = names[u'Nazev mesto']

        del names[u'Nazev mesto']


        for key, article in names.iteritems():
            newPageRedir = pywikibot.Page(site, article)
            newPageRedir.text = u'#PŘESMĚRUJ [[' + mesto + u']]'
            #newPageRedir.save(comment=u'redirect na ' + mesto)

    def checkNames(self):
        fields = []
        fields.append(u'Zkraceny nazev')
        fields.append(u'Nazev mesto')
        fields.append(u'Plny nazev')
        #fields.append(u'Wiki') #Not yet, az po rovnacce

        values = {}
        for field in fields:
            if (not self.schoolInfo[field] in values.values()):
                values[field] = self.schoolInfo[field]

        return values



    def getStartParagraph(self):
        print(type(self.schoolInfo[u'Zkraceny nazev']))
        print(type(self.okresy[self.schoolInfo['Území']]))
        print(type(self.getdata.pravnickaOsoba['zrizovatel']))
        nazev = self.schoolInfo[u'Zkraceny nazev']
        okres = self.okresy[self.schoolInfo['Území']]

        if (okres == u'Praze'):
            okresLink = u'''[[Praha|Praze]]'''
        else:
            okresLink = u'''[[Okres ''' + okres + u'''|okrese ''' + okres + u''']]'''
        return u'''
\'\'\'''' + nazev + u'''\'\'\' je gymnázium v ''' + okresLink + u'''. Zřizovatelem školy je ''' + self.getdata.pravnickaOsoba['zrizovatel'] + u'''.'''

    def getEndParagraph(self):
        text = u'''
== Odkazy ==
=== Externí odkazy ===
* [''' + self.getWeb() + u''' Oficiální webové stránky]
=== Zdroje ===
* [http://rejskol.msmt.cz Rejstřík škol], [[Ministerstvo školství, mládeže a tělovýchovy České republiky]]
* [http://rejspo.msmt.cz Rejstřík školských právnických osob], Ministerstvo školství, mládeže a tělovýchovy České republiky
* [http://eagri.cz/public/app/eagricis/Forms/Lists/Nuts/NutsListsPage.aspx Data ke stažení], Číselníky územně správních jednotek, eAGRI
        '''
        return text

    def getOboryNames(self):
        text = u''''''
        count = 1
        for obor in self.getdata.oboryFinal:
            text = text + u'''
| obor''' + str(count) + u''' = ''' + obor[u'Popis oboru']
            count = count + 1

        return text

    def getWeb(self):
        text = u''

        if (self.schoolInfo['WWW'].find(u'http') > 0):
            text = self.schoolInfo['WWW']
        else:
            text = u'http://' + self.schoolInfo['WWW']

        return text


    def createInfobox(self):
        infobox = u'''{{Infobox - střední škola
| obrázek =
| logo =
| název = ''' + self.getdata.pravnickaOsoba['nazev'] + u'''
| zkratka =
| zřizovatel = ''' + self.getdata.pravnickaOsoba['zrizovatel'] + u'''
| ředitel = ''' + self.getdata.pravnickaOsoba['reditel'] + u'''
| zástupce =
| zeměpisná šířka =
| zeměpisná délka =
| adresa = ''' + self.getdata.pravnickaOsoba['adresa'] + u''', ''' + self.getdata.pravnickaOsoba['misto'] + u''' ''' + self.getdata.pravnickaOsoba['psc'] + u'''
| založení =
| počet žáků =
| web = ''' + self.getWeb() + self.getOboryNames() + u'''
}}'''
        return infobox

    def createTable(self):
        tableLine = []
        topTable = u'''
== Právnické osoby ==

{| class="wikitable sortable"'''
        headTable = u'''
|-
! ID zařízení !! Název zařízení !! Kapacita !! Ulice !! Obec !! Typ'''
        lineBreaker = u'''
|-
'''
        footTable = u'''
|}
'''
        for line in self.getdata.schools:
            #{u'Id. za\u0159\xedzen\xed': u'108037738', u'N\xe1zev za\u0159\xedzen\xed': u'Z\xe1kladn\xed \u0161kola', u'Kapacita': u'240', u'Ciz\xed vyu\u010dovac\xed jazyk': u'', u'Ulice': u'Kon\u011bvova 34', u'Typ': u'B00', u'Obec': u'Krupka'}
            tableLine.append(u'''| ''' + line[u'Id. zařízení'] + ''' || ''' + line[u'Název zařízení'] + ''' || ''' + line[u'Kapacita'] + ''' || ''' + line[u'Ulice'] + ''' || ''' + line[u'Obec'] + ''' || ''' + line[u'Typ'] + ''' ''')

        table = topTable + headTable

        for l in tableLine:
            table = table + lineBreaker + l

        table = table + footTable

        return table

    def createOboryTable(self):
        tableLine = []
        topTable = u'''
== Vyučované obory ==

{| class="wikitable sortable"'''
        headTable = u'''
|-
! Popis oboru !! Forma vzdělání !! Kód oboru !! Kapacita oboru !! Délka vzdělání '''
        lineBreaker = u'''
|-
'''
        footTable = u'''
|}
'''

        for line in self.getdata.oboryFinal:
            if (u'Popis oboru' in line):
                popisOboru = line[u'Popis oboru']
            else:
                popisOboru = u'-'

            if (u'Forma vzdělávání' in line):
                if (line[u'Forma vzdělávání'].strip() == u''):
                    formaVzdelani = u'-'
                else:
                    formaVzdelani = line[u'Forma vzdělávání']
            else:
                formaVzdelani = u'-'

            if (u'Kód oboru' in line):
                kodOboru = line[u'Kód oboru']
            else:
                kodOboru = u'-'

            if (u'Kapacita oboru' in line):
                kapacitaOboru = line[u'Kapacita oboru']
            else:
                kapacitaOboru = u'-'

            if (u'Délka vzdělávání' in line):
                delkaVzdelani = line[u'Délka vzdělávání']
            else:
                delkaVzdelani = u'-'

            tableLine.append(u'''| ''' + popisOboru + ''' || ''' + formaVzdelani + ''' || ''' + kodOboru + ''' || ''' + kapacitaOboru + ''' || ''' + delkaVzdelani + u''' ''')

        table = topTable + headTable

        for l in tableLine:
            table = table + lineBreaker + l

        table = table + footTable

        return table

class Gymnasium(School):

    def setFile(self):
        self.file = "gymnazia.csv"


#school = Gymnasium()
#school.setFile()
#school.loadFile()
#school.getInfo()







#getd = GetData()
#getd.getList('600015343');
#corr = CoordinatesCorrector();
#corr.run()

changer = ChangeLinks()
changer.runAll()

#deletecoord = DeleteCoordsFromTpl()
#deletecoord.getPages()



#start = Starter()
#start = BiographyHockey()
#start = BiographyActor()
#start.start()
#start.process()

#relatives = Relatives()
#relatives.run()
#people = preparePeople()
#people.run()

#people = getPeople()

