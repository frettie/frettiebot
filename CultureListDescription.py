#!/usr/bin/python
# -*- coding: utf-8 -*-

import MyDescription

class CultureListDescription(MyDescription.Description):
    def __init__(self):
        self.define();
        print "Culture list"
        self.setEntityType('culture_list')
        self.setLangWord('cs', 'type', 'Seznam kulturních památek')
        self.setLangWord('en', 'type', 'List of cultural monuments')
        self.setLangWord('cs', 'region', 'Kraj')
        self.setLangWord('en', 'region', 'Region')
        self.setMustBeInCategory(u'seznamy kulturních památek v česku')
        self.setExcludeType('none')

        descriptionFormatGeneralStyle = {u'en' : u'$type in municipality $municipality in $county county', u'cs':u'$type v obci $municipality v okrese $county'}
        self.getData('Seznamy kulturních památek v Česku podle sídla', descriptionFormatGeneralStyle)

    def prepareCounty(self, article):
        self.county = False
        templates = article.templatesWithParams()
        for template in templates:
            tpl_title = template[0].title()
            testTpl = re.search(u'památky v česku/úvod', tpl_title.lower())
            if (testTpl):
                tpl_params = template[1]
                for param in tpl_params:
                    paramArray = param.split('=')
                    if (paramArray[0] == 'okres'):
                        self.setCounty(paramArray[1])
                    if (paramArray[0] == 'obec'):
                        self.setLangWord('cs', 'municipality', paramArray[1])
                        self.setLangWord('en', 'municipality', paramArray[1])

    def setDescriptions(self):
        for lang in self.langVersions:
            s = Template(self.getInText(lang))
            self.descriptions[lang] = s.substitute(type=self.getLangWord(lang, 'type'), county=self.county, municipality=self.getLangWord(lang, 'municipality'))

        for lang in self.langVersions:
            if (lang in self.item.descriptions.keys()):
                self.descriptions.pop(lang)

