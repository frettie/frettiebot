#!/usr/bin/python
# -*- coding: utf-8 -*-

import MyDescription

class CzechListsDescription(MyDescription.Description):
    def __init__(self):
        self.define();
        print "Czech lists"
        self.setEntityType('czech_lists')
        self.setMustBeInCategory(u'seznamy')
        self.setExcludeType('none')

        descriptionFormatGeneralStyle = {u'cs' : u'$name', u'cs':u'$type v okrese $county'}
        self.getData('Seznamy chráněných území v Česku podle okresu', descriptionFormatGeneralStyle)

    def setDescriptions(self):
        for lang in self.langVersions:
            s = Template(self.getInText(lang))
            self.descriptions[lang] = s.substitute(type=self.getLangWord(lang, 'type'), county=self.county)

        for lang in self.langVersions:
            if (lang in self.item.descriptions.keys()):
                self.descriptions.pop(lang)
