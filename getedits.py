#!/usr/bin/python
# -*- coding: utf-8 -*-

import json
import datetime



file = 'edits_2016.json'

with open(file,'r') as f:
    jsonfile = json.load(f)
    # print(jsonfile['items'][0]['results'])
    editcount = 0
    count = 0
    nejvic = 0
    nejmin = 999999
    nejvicden = ''
    nejminden = ''
    editsByMonth = {}
    editsByWeek = {}
    editsByWeekDay = {}

    # date_time_str = '2018-06-29 08:15:27.243860'
    # date_time_obj =
    for line in jsonfile['items'][0]['results']:
        # print(line)
        count+=1
        if (int(line['edits']) > nejvic):
            nejvic = int(line['edits'])
            nejvicden = datetime.datetime.strptime(line['timestamp'], '%Y-%m-%dT%H:%M:%S.%fZ').strftime('%d. %m. %Y')


        if (int(line['edits']) < nejmin ):
            nejmin = int(line['edits'])
            nejminden = datetime.datetime.strptime(line['timestamp'], '%Y-%m-%dT%H:%M:%S.%fZ').strftime('%d. %m. %Y')

        editcount = editcount + int(line['edits'])
        dateobject = datetime.datetime.strptime(line['timestamp'], '%Y-%m-%dT%H:%M:%S.%fZ')
        try:
            editsByMonth[dateobject.month] += int(line['edits'])
        except:
            editsByMonth[dateobject.month] = 0
            editsByMonth[dateobject.month] += int(line['edits'])

        try:
            editsByWeek[int(dateobject.strftime('%W'))] += int(line['edits'])
        except:
            editsByWeek[int(dateobject.strftime('%W'))] = 0
            editsByWeek[int(dateobject.strftime('%W'))] += int(line['edits'])

        try:
            # print(dateobject)
            # print(dateobject.strftime('%a'))
            editsByWeekDay[dateobject.strftime('%a')] += int(line['edits'])
        except:
            editsByWeekDay[dateobject.strftime('%a')] = 0
            editsByWeekDay[dateobject.strftime('%a')] += int(line['edits'])

    # print(count)
    print('pocet editaci: ' + str(editcount))
    print('prumer na den: ' + str(editcount/count))
    print('nejvic za den: ' + str(nejvic) + ' den: ' + nejvicden)
    print('nejmin za den: ' + str(nejmin) + ' den: ' + nejminden)
    print('----------')
    moncount = 0
    nejvicmon = 0
    nejminmon = 999999
    countmons = 0
    for mon in editsByMonth.items():
        countmons+=1
        moncount += mon[1]
        # print(str(mon[0]) + " : " + str(mon[1]))
        if (mon[1] > nejvicmon):
            nejvicmon = mon[1]
            nejvicmonth = mon[0]
        if (mon[1] < nejminmon):
            nejminmon = mon[1]
            nejminmonth = mon[0]

    # print(countmons)
    print('pocet editaci: ' + str(moncount))
    print('prumer na mesic: ' + str(moncount / countmons))
    print('nejvic za mesic: ' + str(nejvicmon) + ' mesic: ' + str(nejvicmonth))
    print('nejmin za mesic: ' + str(nejminmon) + ' mesic: ' + str(nejminmonth))
    print('----------')
    weekcount = 0
    nejvicweek = 0
    nejminweek = 999999
    countweeks = 0
    for week in editsByWeek.items():
        countweeks += 1
        weekcount += week[1]
        # print(str(week[0]) + " : " + str(week[1]))
        if (week[1] > nejvicweek):
            nejvicweek = week[1]
            nejviWeekNum = week[0]
        if (week[1] < nejminweek):
            nejminweek = week[1]
            nejminWeekNum = week[0]

    # print(countmons)
    print('pocet editaci: ' + str(weekcount))
    print('prumer na tyden: ' + str(weekcount / countweeks))
    print('nejvic za tyden: ' + str(nejvicweek) + ' tyden: ' + str(nejviWeekNum))
    print('nejmin za tyden: ' + str(nejminweek) + ' tyden: ' + str(nejminWeekNum))
    print('----------')
    weekdaycount = 0
    nejvicweekday = 0
    nejminweekday = 999999
    countweekday = 0
    for weekday in editsByWeekDay.items():
        countweekday += 1
        weekdaycount += weekday[1]
        # print(str(weekday[0]) + " : " + str(weekday[1]))
        if (weekday[1] > nejvicweekday):
            nejvicweekday = weekday[1]
            nejvicweekdayNum = weekday[0]
        if (weekday[1] < nejminweekday):
            nejminweekday = weekday[1]
            nejminweekdayNum = weekday[0]

    # print(countmons)
    print('pocet editaci: ' + str(weekdaycount))
    print('prumer na den v tydnu: ' + str(weekdaycount / countweekday))
    print('nejvic za den v tydnu: ' + str(nejvicweekday) + ' den v tydnu: ' + str(nejvicweekdayNum))
    print('nejmin za den v tydnu: ' + str(nejminweekday) + ' den v tydnu: ' + str(nejminweekdayNum))