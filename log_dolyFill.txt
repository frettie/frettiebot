Důl Albert
Důl Alexander
Důl Anselm
Důl Antonín (Moravská Ostrava)
Důl Bedřich (Zábřeh nad Odrou)
Důl Bedřich (Slezská Ostrava)
Důl Bohatý slojemi
Doly Severní dráhy Ferdinandovy
Důl Ema a Lucie
Důl Ferdinand (Ostrava)
Důl František (Ostrava-Přívoz)
Důl Františka (Slezská Ostrava)
Důl Heřmanice
Důl Hlubina (Ostrava)
Hrušovský důl
Důl Hubert
Důl Ida
Důl Ignát
Jaklovecká dědičná štola
Jaklovecké doly
Důl Jakub
Důl Jan SDF (Slezská Ostrava)
Důl Jan Maria
Důl Jeremenko
Důl Jindřich (Moravská Ostrava)
Důl Jindřich (Slezská Ostrava)
Důl Jiří
Důl Josef (Slezská Ostrava)
Důl Josef SDF (Slezská Ostrava)
Kamenouhelné doly Ferdinandovo štěstí
Důl Karolina
Důl Luční
Důl Michal
Důl Michálka
Důl Neumann
Nové hlučínské kamenouhelné doly
Důl Odra (stará) Ostrava-Přívoz
Důl Oskar (Heřmanice)
Důl Oskar (Petřkovice)
Důl Petr a Pavel
Důl Petr Bezruč
Důl Prokop (Ostrava)
Důl Rychvald
Důl Salm I – Ignát
Důl Salm II – Leopoldina
Důl Salm VII - Hugo
Staré hlučínské kamenouhelné doly
Důl Svatá Anna
Důl svaté Trojice
Důl Svinov
Důl Šalomoun
Šilheřovické doly
Důl Trojice (Lhotka)
Důl Eduard Urx 5
Větrní jáma Vrbice
Důl Vilém
Vrchnostenské doly hraběte Wilczka
Důl Zárubek
Důl Zwierzina – Josef
Důl Zwierzina VII
Žentourová jáma
Důl Žentourový
Důl Barbora (Karviná)
Důl Doubrava
Důl Dukla (Dolní Suchá)
Důl František
Důl Františka (Karviná)
Důl Gabriela
Důl Hlavní jáma
Důl Hlubina (Karviná)
Důl Hohenegger
Důl Jindřich (Karviná)
Důl Kukla
Důl Max
Důl Václav
Důl Žofie
Důl Lazy
Důl ČSA
Důl ČSM
Důl Darkov
Důl 9. květen
Důl Frenštát
Důl Jan Karel
Českomoravské doly
OKD
Důl Paskov
Svatá Barbora (sdružení)
Důl Guido
Důl Morcinek
Důl Nowy Wirek
Důl Bogdanka
Důl Borynia
Důl Ziemowit
Halda v Rydułtowech
Jastrzębska Spółka Węglowa
Kompania Węglowa
Důl Pniówek
Důl Krupiński
Důl Zofiówka
Důl Marie
Důl Prokop
Důl Vojtěch
Rudné doly na Příbramsku
Ševčinský důl
Důl Bratrství
Důl svatého Jiří
Důl Svornost
Grafitový důl v Českém Krumlově
Valachov
Důl Rudý říjen Olší
Důl Marie Majerové
Lom Bílina
Lom ČSA
Lom Družba
Lom Jan Šverma
Lom Jiří
Lom Nástup - Tušimice
Lom Vršany
Důl Eduard
Důl Josef (Jáchymov)
Důl Rovnost
Pinky na žíle Schweizer
Důl Rožná I
Seznam štol v Jáchymově
Štola č. 1 (Jáchymov)
Věž smrti
Černý důl (přírodní památka, okres Opava)
Děvín, Ostrý a Schachtstein
Důl Schweidrich
Eliášova dědičná štola
Herlíkovické štoly
Hornické muzeum Příbram
Hornický skanzen Mayrau
Chrustenická šachta
Důl Jeroným
Ledová jáma
Na černé rudě
Orty
Osel (důl)
Podzemní továrna Richard
Rudolfovský rudní revír
Řimbaba (důl)
Štola Jan Evangelista
Štola svatého Josefa
Štola svatého Vojtěcha
Vlčí jáma (Potůčky)
Amerika (vápencové lomy)
Bezděkovský lom
Bischofův lom
Černá skála (přírodní památka, okres Blansko)
Červený vrch (přírodní památka)
Ejpovické útesy
Hrádek (přírodní památka, okres Uherské Hradiště)
Hřídelecká hůra
Chvalský lom
Jezírko u Dobříše
Ježovský lom
Kaňk (národní přírodní památka)
Kienberg
Kladrubská hora
Kobyla (přírodní rezervace)
Kotýz
Křtinský lom
Kurovický lom
Lom Holý vrch
Lom Chlum
Lom Katzenholz
Lom Kozolupy
Lom Masty
Lom Mírová
Lom Na plachtě
Lom Osmóza
Lom Rasová
Lom Strážné
Lom Svatá Anna
Lom u Červených Peček
Lom u Chrástu
Lom u Nové Vsi
Lom u Radimi
Losky
Lůmek u Bečvár
Maršovický vrch
Medlovický lom
Mexiko (lom)
Miocenní sladkovodní vápence
Muckovské vápencové lomy
Mutěnínský lom
Na Pískách (lom)
Na Voskopě
Nerestský lom
Ohrozim-Horka
Opatřilka - Červený lom
Opukový lom u Přední Kopaniny
Ortocerový lůmek
Pacova hora
Panský lom
Pod školou (přírodní památka, Hlubočepy)
Požáry
Pusté kostely
Razovské tufity
Rohožník - lom v Dubči
Růžičkův lom
Rychlebské zatopené lomy
Skalice u České Lípy (přírodní památka)
Solvayovy lomy
Soseňský lom
Starkočský lom
Státní lom
Stříbrné jezírko
Teletínský lom
Tlustec
Toužínské stráně
U Devíti Křížů
U Nového mlýna
U Radošína
U skal
Uhlířský vrch (přírodní památka, okres Bruntál)
Vinařická hora
Zhůřský lom
Zlatý kůň
Žákova skála
Žermanický lom
Žraločí zuby
Kamenický kopec
Klučky
Maršovický vrch (Ralská pahorkatina)
Milštejn
Panská skála
Riedlova jeskyně
Tachovský vrch
Jezírko u Kyzu
Malá Amerika
Velká Amerika
Vodní nádrž Ejpovice
