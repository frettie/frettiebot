#!/usr/bin/python
# -*- coding: utf-8 -*-
import re
from logger import Logger
import untangle
import pywikibot
import requests
import urllib.parse
from wikidatafun import *


import unicodecsv
from pywikibot import pagegenerators, WikidataBot
from pywikibot.textlib import mwparserfromhell as parser

class okresy():

    def __init__(self):
        self.site = pywikibot.Site('cs', 'wikipedia')
        self.repo = self.site.data_repository()

    def names(self):
        # self.logger = Logger(u'newStreetssquare' + u'Fill', 'saved')
        cities = {}
        query = '''
        SELECT DISTINCT ?item ?csitemLabel ?deitemLabel WHERE {
	?item wdt:P31 wd:Q106658 .
	?item wdt:P17 wd:Q183 .
    #?item rdfs:label ?csitemLabel.
     # FILTER((LANG(?csitemLabel)) = "cs")
  ?item rdfs:label ?deitemLabel.
  FILTER((LANG(?deitemLabel)) = "de")
}
        '''

        from pywikibot.data import sparql
        query_object = sparql.SparqlQuery()
        data = query_object.select(query, full_data=True)
        if data:
            for r in data:
                item_id = r['item'].getID()
                item = pywikibot.ItemPage(self.repo, item_id)
                csName = item.text[u'labels'].get(u'cs', '')
                deName = r['deitemLabel']

                # datas = item.get()
                try:
                    # coords = pywikibot.Claim(self.repo, 'P625')
                    pywikibot.output('de label a cs label %s ' % unicode(csName).strip() + ', ' + unicode(deName))
                    pywikibot.output('de label %s ' % unicode(deName).strip())
                    pywikibot.output('cs label %s ' % unicode(csName).strip())
                    match = re.search(u'(Kreis) (.*)',unicode(deName))
                    if match:
                        groups = match.groups()
                        newName = u'Okres ' + groups[1]
                    else:
                        match = re.search(u'(Landkreis) (.*)', unicode(deName))
                        if match:
                            groups = match.groups()
                            newName = u'Zemský okres ' + groups[1]
                        else:
                            newName = pywikibot.input(u'Nove jmeno')
                    if csName == newName:
                        continue
                    pywikibot.output('new cs label %s ' % unicode(newName).strip())
                    choice = pywikibot.input_choice('Ulozit?',
                                                    [('Yes', 'y'),
                                                     ('No', 'n')],
                                                    default='Y',
                                                    automatic_quit=False)

                    if choice == 'n':
                        pass

                    if choice == 'y':
                        item.editLabels(labels={'cs': unicode(newName).strip()}, summary="Correct cs label")



                except AttributeError as e:
                    # self.logger.logError(street_name)
                    # self.logger.logError(u'coord problems')
                    pass
                except UnboundLocalError as e:
                    # self.logger.logError(street_name)
                    # self.logger.logError(u'coord problems')
                    pass

    def mesta(self):
        # self.logger = Logger(u'newStreetssquare' + u'Fill', 'saved')
        cities = {}
        query = '''
            SELECT ?item ?itemLabel ?cantonLabel WHERE {
                ?item wdt:P31/wdt:P279* wd:Q262166  .
                ?item wdt:P17 wd:Q183 .
                ?item wdt:P131 ?canton .
                ?canton wdt:P31 wd:Q106658
                SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],cs". }
            }
        '''

        from pywikibot.data import sparql
        query_object = sparql.SparqlQuery()
        data = query_object.select(query, full_data=True)
        if data:
            for r in data:
                item_id = r['item'].getID()
                item = pywikibot.ItemPage(self.repo, item_id)
                existDesc = item.text[u'descriptions'].get(u'cs', '')
                okresLabel = r['cantonLabel']
                cityLabel = r['itemLabel']

                # datas = item.get()
                try:
                    # coords = pywikibot.Claim(self.repo, 'P625')
                    # pywikibot.output('de label a cs label %s ' % unicode(csName).strip() + ', ' + unicode(deName))
                    # pywikibot.output('de label %s ' % unicode(deName).strip())
                    pywikibot.output('city label %s ' % unicode(cityLabel).strip())
                    match = re.search(u'(Zemský okres) (.*)', unicode(okresLabel))
                    if match:
                        groups = match.groups()
                        newDesc = u'obec v zemském okresu ' + groups[1] + u' v Německu'
                    else:
                        match = re.search(u'(Okres) (.*)', unicode(okresLabel))
                        if match:
                            groups = match.groups()
                            newDesc = u'obec v okresu ' + groups[1] + u' v Německu'
                        else:
                            newDesc = ''
                    # if csName == newName:
                    #     continue
                    pywikibot.output('new cs desc %s ' % unicode(newDesc).strip())

                    if existDesc == newDesc:
                        continue

                    if existDesc != '':

                        pywikibot.output('old cs desc %s ' % unicode(existDesc).strip())
                        choice = pywikibot.input_choice('Prepsat?',
                                                    [('Yes', 'y'),
                                                     ('No', 'n')],
                                                    default='Y',
                                                        force=True,
                                                    automatic_quit=False)

                        if choice == 'n':
                            pass

                        if choice == 'y':
                            item.editDescriptions(descriptions={'cs': unicode(newDesc).strip()}, summary="Correct cs description", asynchronous=True)
                    else:
                        item.editDescriptions(descriptions={'cs': unicode(newDesc).strip()}, summary="Correct cs description", asynchronous=True)


                except AttributeError as e:
                    # self.logger.logError(street_name)
                    # self.logger.logError(u'coord problems')
                    pass
                except UnboundLocalError as e:
                    # self.logger.logError(street_name)
                    # self.logger.logError(u'coord problems')
                    pass
okr = okresy()
okr.mesta()