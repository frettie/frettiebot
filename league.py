#!/usr/bin/python
# -*- coding: utf-8 -*-
import pywikibot
import sys
import exceptions
from logger import Logger
import re
import curses
from pandas import *

logger = Logger(u'league' + u'Fill','saved')

siteWikidata = pywikibot.getSite('wikidata')
# site = pywikibot.getSite('cs', 'wikipedia')
site = pywikibot.getSite('en', 'wikipedia')
# category = u"Česká florbalová extraliga mužů"
category = u"World Judo Championships"
page = pywikibot.Page(site, u'Category:' + category)
# instancePageTitle = u"Česká florbalová extraliga mužů"
instancePageTitle = u"World Judo Championships"
instanceWikidata = pywikibot.Page(site, instancePageTitle).data_item()
# pageArticleName = u"Česká florbalová extraliga mužů"
pageArticleName = u"World Judo Championships"
czechTitle = u"Česko"
czechoslovakiaTitle = u"Československo"
en_version = True
if not en_version:
    czechWikidata = pywikibot.Page(site, czechTitle).data_item()
    czechoslovakiaWikidata = pywikibot.Page(site, czechoslovakiaTitle).data_item()

lengthOfYearTo = 2 # pokud je rok za lomítkem ve formátu 1970, tak 4
lengthOfYearFrom = 4 # pokud je rok před lomítkem ve formátu 1970, tak 4
yearDiff = 2

generator = pywikibot.Category(page).articles()
for page in generator:
    if not(logger.isCompleteFile(page.title())) and (page.namespace() == 0) :
        if en_version:
            match = re.search(u'([0-9]{' + unicode(lengthOfYearFrom) + u'}) (' + pageArticleName + ')', page.title())
            # match = re.search(u'(' + pageArticleName + ') ([0-9]{' + unicode(lengthOfYearFrom) + u'})', page.title())
        else:
            match = re.search(u'(' + pageArticleName + ') ([0-9]{' + unicode(lengthOfYearFrom) + u'})/([0-9]{' + unicode(lengthOfYearTo) + u'})', page.title())
        #print match.groups()
        if (match):
            if en_version:
                name = match.group(2)
                yearFrom = match.group(1)
            else:
                name = match.group(1)
                yearFrom = match.group(2)
                yearTo = match.group(3)
                century = yearFrom[0] + yearFrom[1]


            instanceProp = 'P31'
            countryProp = 'P17'
            fromProp = 'P580'
            toProp = 'P582'
            previousProp = 'P155'
            nextProp = 'P156'
            try:
                wikidataPage = page.data_item()
            except pywikibot.exceptions.NoPage as e:
                continue
            instanceClaim = pywikibot.Claim(siteWikidata,instanceProp)
            instanceClaim.setTarget(instanceWikidata)

            if not en_version:
                if (lengthOfYearTo == 4):
                    yearToDefinition = yearTo
                else:
                    yearToDefinition = century + yearTo

                condition = int(yearToDefinition) < 1993

                if (condition):
                    countryClaim = pywikibot.Claim(siteWikidata,countryProp)
                    countryClaim.setTarget(czechoslovakiaWikidata)
                else:
                    countryClaim = pywikibot.Claim(siteWikidata,countryProp)
                    countryClaim.setTarget(czechWikidata)

            yearTimeFrom = pywikibot.WbTime(year=int(yearFrom))
            fromClaim = pywikibot.Claim(siteWikidata,fromProp)
            fromClaim.setTarget(yearTimeFrom)

            if not en_version:
                yearTimeTo = pywikibot.WbTime(year=int(yearToDefinition))
                toClaim = pywikibot.Claim(siteWikidata,toProp)
                toClaim.setTarget(yearTimeTo)
            claims = wikidataPage.get()
            try:
                if en_version:
                    previousPageTitle = unicode(int(yearFrom) - yearDiff) + ' ' + pageArticleName
                    # previousPageTitle = pageArticleName + ' ' + unicode(int(yearFrom) - yearDiff)
                else:
                    previousPageTitle = pageArticleName + ' ' + unicode(int(yearFrom) - 1) + '/' + unicode(int(yearToDefinition) - 1)[4-lengthOfYearTo:4]
                previousWikidata = pywikibot.Page(site, previousPageTitle ).data_item()
                previousClaim = pywikibot.Claim(siteWikidata,previousProp)
                previousClaim.setTarget(previousWikidata)

                if (not claims[u'claims'].get(u'P155', False)):
                    wikidataPage.addClaim(previousClaim)
            except pywikibot.exceptions.NoPage, e:
                print u"prev nopage: " + previousPageTitle
                pass

            try:
                if en_version:
                    nextPageTitle = unicode(int(yearFrom) + yearDiff) + ' ' + pageArticleName
                    # nextPageTitle = pageArticleName + ' ' + unicode(int(yearFrom) + yearDiff)
                else:
                    nextPageTitle = pageArticleName + ' ' + unicode(int(yearFrom) + 1) + '/' + unicode(int(yearToDefinition) + 1)[4-lengthOfYearTo:4]
                nextWikidata = pywikibot.Page(site, nextPageTitle ).data_item()
                nextClaim = pywikibot.Claim(siteWikidata,nextProp)
                nextClaim.setTarget(nextWikidata)
                if (not claims[u'claims'].get(u'P156', False)):
                    wikidataPage.addClaim(nextClaim)
            except pywikibot.exceptions.NoPage, e:
                print u"next nopage: " + nextPageTitle
                pass

            if (not claims[u'claims'].get(u'P31', False)):
                wikidataPage.addClaim(instanceClaim)
            if not en_version:
                wikidataPage.addClaim(countryClaim)
                wikidataPage.addClaim(fromClaim)
                wikidataPage.addClaim(toClaim)


            print "--------------"
            print u'page: ' + page.title()
            print u'instance: ' + instancePageTitle
            print u'country: ' + u'Czech'
            print u'od: ' + yearFrom
            if not en_version:
                print u'do: ' + yearToDefinition

            print u'predchozi: ' + previousPageTitle
            print u'nasledujici: ' +  nextPageTitle
            logger.logComplete(page.title())

#f = FillWikidata(u'umrti')
#f.loadPages(u'Úmrtí', u' podle měst Česka')
