import svgwrite

class ctverce:
    def __init__(self):
        self.file = svgwrite.Drawing('sifra.svg', profile='tiny', size=[6000,500])
        self.color = svgwrite.rgb(0, 0, 0, '%')
        self.text = 'Zelena vas povede dal tam kde vlak uz nestavi Ukol na cestu mate 1'

        self.pismena = {}

        self.pismena['1'] = [
            [1, 1],
            [1, 1, 1, 1, 1],
            [1, 1],
            [1, 1, 1, 1, 1],
            [1, 1]]

        self.pismena['z'] = [
            [0,0],
            [1,1,1,0,1],
            [1,1],
            [1,0,1,1,1],
            [0,0]]

        self.pismena['e'] = [
            [0, 0],
            [0, 1, 1, 1, 1],
            [0, 0],
            [0, 1, 1, 1, 1],
            [0, 0]]

        self.pismena['l'] = [
            [1, 1],
            [0, 1, 1, 1, 1],
            [1, 1],
            [0, 1, 1, 1, 1],
            [0, 0]]

        self.pismena['n'] = [
            [1, 1],
            [0, 0, 1, 1, 0],
            [1, 1],
            [0, 1, 1, 0, 0],
            [1, 1]]

        self.pismena['a'] = [
            [0, 0],
            [0, 1, 1, 1, 0],
            [0, 0],
            [0, 1, 1, 1, 0],
            [1, 1]]

        self.pismena[' '] = [
            [0, 0],
            [0, 0, 0, 0, 0],
            [0, 0],
            [0, 0, 0, 0, 0],
            [0, 0]]

        self.pismena['v'] = [
            [1, 1],
            [1, 0, 1, 0, 1],
            [1, 1],
            [1, 1, 1, 1, 1],
            [1, 1]]

        self.pismena['s'] = [
            [0, 0],
            [0, 1, 1, 1, 1],
            [0, 0],
            [1, 1, 1, 1, 0],
            [0, 0]]

        self.pismena['p'] = [
            [0, 0],
            [0, 1, 1, 1, 0],
            [0, 0],
            [0, 1, 1, 1, 1],
            [1, 1]]

        self.pismena['o'] = [
            [0, 0],
            [0, 1, 1, 1, 0],
            [1, 1],
            [0, 1, 1, 1, 0],
            [0, 0]]

        self.pismena['d'] = [
            [1, 1],
            [0, 0, 1, 1, 1],
            [1, 1],
            [0, 0, 1, 1, 1],
            [1, 1]]

        self.pismena['t'] = [
            [0, 0],
            [1, 1, 0, 1, 1],
            [1, 1],
            [1, 1, 0, 1, 1],
            [1, 1]]

        self.pismena['m'] = [
            [1, 1],
            [0, 0, 1, 0, 0],
            [1, 1],
            [0, 1, 1, 1, 0],
            [1, 1]]

        self.pismena['k'] = [
            [1, 1],
            [1, 1, 0, 0, 1],
            [1, 1],
            [1, 1, 0, 0, 1],
            [1, 1]]

        self.pismena['y'] = [
            [1, 1],
            [1, 0, 1, 0, 1],
            [1, 1],
            [1, 1, 0, 1, 1],
            [1, 1]]

        self.pismena['u'] = [
            [1, 1],
            [0, 1, 1, 1, 0],
            [1, 1],
            [0, 1, 1, 1, 0],
            [0, 0]]

        self.pismena['i'] = [
            [1, 1],
            [1, 1, 0, 1, 1],
            [1, 1],
            [1, 1, 0, 1, 1],
            [1, 1]]

        self.pismena['c'] = [
            [0, 0],
            [0, 1, 1, 1, 1],
            [1, 1],
            [0, 1, 1, 1, 1],
            [0, 0]]


    def save(self):
        self.file.save()

    def run(self):
        pismena = list(self.text)
        iterace = 0
        for pismeno in pismena:
            start = (iterace * 90) + 30
            self.ctverec(pismeno, start, iterace)
            iterace = iterace+1

    def ctverec(self, pismeno, start, iterace):
        end = start + 30
        field = self.pismena[pismeno.lower()]

        lines=['x','y','x','y','x']
        ys = [1,1,2,2,3]
        ysblockiter = [0,1,4,2,5,3]
        iter = 0

        for line in field:
            type = lines[iter]

            blockiter = 1
            for block in line:
                y = ys[iter] * 30
                if block == 0:
                    blockiter = blockiter + 1
                    continue
                if (type == 'x'):
                    append = 30 * blockiter
                    end = start + 30 + append

                    self.file.add(self.file.line((start+append, y), (end, y), stroke_width=3, stroke=self.color))
                if (type == 'y'):

                    prepareiter = ysblockiter[blockiter]
                    if prepareiter == 0:
                        pass
                    if prepareiter == 4:
                        if iter == 1:
                            append = 30 * 1
                            end = y + 30
                            self.file.add(self.file.line((start + append, y), (start + append + 30, end), stroke_width=3, stroke=self.color))
                        else:
                            append = 30 * 1
                            end = y + 30
                            self.file.add(
                                self.file.line((start + append + 30, y), (start + append, end), stroke_width=3, stroke=self.color))
                    if prepareiter == 5:
                        if iter == 1:
                            append = 30 * 2
                            end = y + 30
                            self.file.add(self.file.line((start + append + 30, y), (start + append, end), stroke_width=3, stroke=self.color))
                        else:
                            append = 30 * 2
                            end = y + 30
                            self.file.add(self.file.line((start + append, y), (start + append + 30, end), stroke_width=3, stroke=self.color))
                    if prepareiter in [1,2,3]:
                        append = 30 * prepareiter
                        end = y + 30
                        self.file.add(self.file.line((start+append, y), (start+append, end), stroke_width=3, stroke=self.color))
                    pass
                blockiter = blockiter + 1
            iter = iter + 1
        self.file.save()
        return self

ctverce = ctverce()
# ctverce.ctverec('z', 30).save()
ctverce.run()