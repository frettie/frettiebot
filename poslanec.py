#!/usr/bin/python
# -*- coding: utf-8 -*-
import re
from logger import Logger
import untangle
import pywikibot
import requests


import unicodecsv
from pywikibot import pagegenerators, WikidataBot
from pywikibot.textlib import mwparserfromhell as parser

# import sys
# reload(sys) # just to be sure
# sys.setdefaultencoding('utf-8')
# from math import radians, cos, sin, asin, sqrt

class function():
    od = ''
    do = ''
    funkce = ''
    predchudce = ''
    nastupce = ''


class poslanec():

    def load(self):
        self.site = pywikibot.Site('cs', 'wikipedia')
        self.repo = self.site.data_repository();
        # self.repo = self.site.data_repository()
        self.genFactory = pagegenerators.GeneratorFactory(site=self.site)
        self.genFactory.handleArg('-cat:Kategorie:Poslanci československého Federálního shromáždění')
        self.generator = self.genFactory.getCombinedGenerator()
        if not self.generator:
            self.generator = self.genFactory.getCombinedGenerator()

        pages = {}
        for page in pagegenerators.PreloadingGenerator(self.generator):
            code = parser.parse(page.text)
            self.wd = pywikibot.Site('wikidata')

            source = pywikibot.Claim(self.wd, 'P248')
            source.setTarget(pywikibot.ItemPage(self.wd, 'Q41593640'))

            property = pywikibot.Claim(self.wd, 'P625')

            for template in code.ifilter_templates():
                if not template.name.matches(u'Infobox - politik'):
                    continue
                if template.has('Úřad', ignore_empty=True):
                    assert isinstance(template, parser.wikicode.Template)
                    try:
                        wikidata = template.get('Wikidata').value
                    except ValueError as e:
                        continue

                    if self.logger.isCompleteFile(str(wikidata)):
                        continue

                    # self.repo = self.site.data_repository()
                    entity = pywikibot.ItemPage(self.wd, wikidata)

                    # obec = unicode(template.get(u'Obec').value)
                    # lat = str(template.get(u'Zeměpisná_šířka').value).strip()
                    # lon = str(template.get(u'Zeměpisná_délka').value).strip()
                    # if lat:
                    #     continue  # todo: warning
                    # if lon:
                    #     continue  # todo: warning


p = poslanec()
p.load()