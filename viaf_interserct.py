import csv

result = []
viafarr = []
wdarr = []
with open('viaf_nkcr_only_viaf.txt') as viaf:
    for v in viaf:
        viafarr.append(v.strip())

with open('viaf_wd_only.csv') as wd:
    for w in wd:
        wdarr.append(w.strip())

#print(wdarr)
def intersection(lst1, lst2):
    # Use of hybrid method
    temp = set(lst2)
    lst3 = [value for value in lst1 if value in temp]
    return lst3

# result = []
#for d in viafarr:
#  if (d in wdarr):
#      result.append(d)
nkcr_fin = {}
wd_fin = {}

with open('viaf_wd_fin.csv') as wd2:
    for w in wd2:
        sp = w.strip().split(',')
        try:
            nkcr_fin[sp[1]] = {'nkc': '', 'wd': sp[0]}
        except KeyError as e:
            pass

with open('viaf_nkcr_fin.txt') as wd:
    for w in wd:
        sp = w.strip().split(';')
        try:
            nkcr_fin[sp[0]]['nkc'] = sp[1]
        except KeyError as e:
            pass


vys = {}
result = intersection(wdarr,viafarr)
print(len(result))
for res in result:
    vys[res] = nkcr_fin[res]
    # print(vys[res])
with open('viaf_to_import.csv', 'w') as csvfile:
    fieldnames = ['viaf', 'nkc', 'wd']
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
    writer.writeheader()
    for v in vys:
        d = vys[v]
        d['viaf'] = v
        writer.writerow(d)