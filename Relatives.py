#!/usr/bin/python
# -*- coding: utf-8 -*-
import pywikibot

class Relatives:
    def __init__(self):
        print 'Relatives'
        self.templateName = u"Příbuzenstvo"
        self.typeEntity = 'relatives'

    def run(self):
        site = pywikibot.getSite('cs', 'wikipedia')
        page = pywikibot.Page(site, u'Template:' + self.templateName)
        generator = pagegenerators.ReferringPageGenerator(page)
        for page in generator:
            print '----------------'
            print page.title() + ' - check'
            print '----------------'
            if not(self.isCompleteFile(page.title())):
                self.process(page)

    def process(self, page):
        if (page.namespace() == 0):
            print '----------------'
            print page.title() + ' - zpracovavam'
            print '----------------'
            templates = page.templatesWithParams()
            for t in templates:
                if (t[0] == u'Příbuzný'):
                    self.process_template(t, page)
            self.logComplete(page.title())


    def process_template(self, tpl, page):
        wikidata = pywikibot.DataPage(page)

        obj = RelativeObject(tpl[1][0], tpl[1][1], wikidata, page)
        obj.save()


    def logComplete(self, title):
        file = codecs.open("log_" + self.typeEntity + ".txt", "a", "utf-8")
        file.write(title + '\n')
        file.close()

    def logError(self, title):
        file = codecs.open("log_error_" + self.typeEntity + ".txt", "a", "utf-8")
        file.write(title + '\n')
        file.close()

    def isCompleteFile(self, entity):
        try:
            file = codecs.open("log_" + self.typeEntity + ".txt", "r", "utf-8")
            lines = file.readlines()
            list_entity = list()
            file.close()
            for it in lines:
                list_entity.append(it.strip())
            #TODO opravit!

            try:
                list_entity.index(entity)
                return True
            except ValueError:
                return False
        except IOError:
            print "Not exist file"
            codecs.open('log_' + self.typeEntity + '.txt', 'w', 'utf-8')

class RelativeObject(Relatives):
    def __init__(self, reltype, name, wikidata, page):
        self.typeEntity = 'relatives'
        self.type = reltype
        self.name = name
        self.link = ""
        self.page = page
        self.property = 0
        self.wikidata = wikidata
        self.types = {
            u'manželka' : '26',
            u'manžel' : '26',
            u'otec' : '22',
            u'matka' : '25',
            u'bratr' : '7',
            u'sestra' : '9',
            u'syn' : '40',
            u'dcera' : '40',
            u'dědeček' : '45',
            u'babička' : '45',
            u'děd' : '45',
            u'kmotr' : '1290',
            u'otčím' : '43',
            u'macecha' : '44',
            u'partner' : '451',
            u'životní partner' : '451'
            }

        self.legalTypes = ['26', '22', '25', '7', '9', '40', '45', '1290', '43', '44', '451']
        self.clean()

    def clean(self):
        if (re.search('\[\[', self.name)):
            print self.name
            try:
                mat = re.match('\[\[(\D+)\]\]', self.name)
                array = mat.groups()
                self.name = array[0]
            except AttributeError:
                self.name = self.name

    def logNew(self, title):
        file = codecs.open("log_new_" + self.typeEntity + ".txt", "a", "utf-8")
        file.write(title + '\n')
        file.close()


    def myprint(self):
        print self.name + ' - ' + self.type

    def getRelativeEntityId(self):
        site = pywikibot.getSite('cs', 'wikipedia')
        entity = pywikibot.Page(site, self.name)
        entity_wikidata = pywikibot.DataPage(entity)
        entity_add = entity_wikidata.getID()

        return entity_add

    def getClaims(self):
        dictionary = self.wikidata.get()
        claims = list()
        for c in dictionary['claims']:
            claims.append(c['m'][1])

        return claims

    def createPerson(self, page):
        datapage = pywikibot.DataPage(page)
        cats = page.categories();
        male = False
        for cat in cats:
            if (re.search(u'Muži', cat.title())):
                male = True

        datapage.createitem(summary='New item from wikipedia page')
        refes = {('P143','Q191168')}
        datapage.editclaim(31, 'Q5', refs=refes, override=False)
        if male:
            datapage.editclaim(21, 'Q6581097', refs=refes, override=False)
        else:
            datapage.editclaim(21, 'Q6581072', refs=refes, override=False)

        page.put(page.get())


    def save(self):
        refes = {('P143','Q191168')}
        continueWithSaving = True

        if (re.search(u'manžel', self.type)):
            self.type = u'manžel'

        self.type = self.type.lower()

        if (re.search('\|', self.name)):
            res = re.split('\|', self.name)
            self.name = res[0]

        if (self.type in self.types.keys()):
            self.property = self.types[self.type]
        else:
            print u"neni zaveden typ pribuzenstvi " + self.type
            continueWithSaving = False

        try:
            claims = self.getClaims()
            if not(int(self.property) in claims) and continueWithSaving:

                entity_add = self.getRelativeEntityId()
                print u'vlozeno ' + self.name + ' - ' + self.type
                self.wikidata.editclaim(self.property, entity_add, refs=refes, override=False)
        except pywikibot.exceptions.InvalidTitle:
            print u'spatne vyplneny titulek v tpl u ' + self.page.title()
            self.logError(self.name)
        except pywikibot.exceptions.NoPage:
            print u"neexistuje stránka " + self.page.title()
            self.createPerson(self.page)
            self.logNew(self.name)