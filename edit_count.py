import json
import datetime

import requests
from attrdict import AttrDict
from pageviewapi.client import ZeroOrDataNotLoadedException, ThrottlingException
__version__ = "0.4.0"
EDITS_ENDPOINT = "edited-pages/top-by-edits"
EDITS_ARGS = "{project}/{editor_type}/{page_type}/{year}/{month}/{day}"
PROJECT_URL = "https://github.com/Commonists/pageview-api"
UA = "Python pageview-api client v{version} <{url}>"
USER_AGENT = {
    'User-Agent': UA.format(url=PROJECT_URL, version=__version__)
}

API_BASE_URL = "https://wikimedia.org/api/rest_v1/metrics"
import pageviewapi
top_pages = {}
daymonth = {
    1:31,
    2:28,
    3:31,
    4:30,
    5:31,
    6:30,
    7:31,
    8:31,
    9:30,
    10:31,
    11:30,
    12:31,
}
year = 2019
# f = open('top_edits_2019.csv', 'w')
# f = open('edit_2019.csv', 'w')
# for month in daymonth:
#     days_in_month = daymonth[month]
#
#     for day in range(1,days_in_month+1):
#         day_format = "{:02d}".format(day)
#         month_format = "{:02d}".format(month)
#         data = pageviewapi.top('cs.wikipedia', year, month_format, day_format, access='all-access')
#         articles = data['items'][0]['articles']
#         for article in articles:
#             page = article['article']
#             page_count = article['views']
#             # page = article['article']
#
#             if page in top_pages.keys():
#                 top_pages[page] = top_pages[page] + page_count
#             else:
#                 top_pages[page] = page_count
#
# for top_page in top_pages:
#     f.write(top_page)
#     f.write(';')
#     f.write(str(top_pages[top_page]))
#     f.write('\n')

def edits(project, year, month, day, editor_type='all-editor-types', page_type='all-page-types'):
    #{project}/{editor-type}/{page-type}/{year}/{month}/{day}
    """Top 1000 most visited articles from project on a given date.

    >>> import pageviewapi
    >>> views = pageviewapi.top('fr.wikipedia', 2015, 11, 14)
    >>> views['items'][0]['articles'][0]
    {u'article': u'Wikip\xe9dia:Accueil_principal', u'rank': 1,
    u'views': 1600547}
    """
    args = EDITS_ARGS.format(project=project,
                           editor_type=editor_type,
                           page_type=page_type,
                           year=year,
                           month=month,
                           day=day)
    return api(EDITS_ENDPOINT, args)

def api(end_point, args, api_url=API_BASE_URL):
    """Calling API."""
    url = "/".join([api_url, end_point, args])
    response = requests.get(url, headers=USER_AGENT)
    if response.status_code == 200:
        # Everything went fine!
        return AttrDict(response.json())
    elif response.status_code == 404:
        raise ZeroOrDataNotLoadedException
    elif response.status_code == 429:
        raise ThrottlingException
    else:
        response.raise_for_status()
#

f = open('edits_all_2019.csv', 'w')
for month in daymonth:
    days_in_month = daymonth[month]

    for day in range(1,days_in_month+1):
        day_format = "{:02d}".format(day)
        month_format = "{:02d}".format(month)
        if (month == 12):
            print('tu')
        data = edits('cs.wikipedia', year, month_format, day_format, editor_type='all-editor-types', page_type='content')
        try:
            articles = data['items'][0]['results'][0]['top']
            for article in articles:
                page = article['page_title']
                page_count = article['edits']
                # page = article['article']

                if page in top_pages.keys():
                    top_pages[page] = top_pages[page] + page_count
                else:
                    top_pages[page] = page_count
        except IndexError as e:
            print(e)
            print(page)

for top_page in top_pages:
    f.write(top_page)
    f.write(';')
    f.write(str(top_pages[top_page]))
    f.write('\n')
# f = open('daily_views_2019.csv', 'w')
# daycount = {}
# daycount_txt = {}
# day_of_weeks = {}
# data = pageviewapi.aggregate('cs.wikipedia', '2019010100', '2019123124', agent='user')
# try:
#     days = data['items']
#     for day_item in days:
#         timestamp = day_item['timestamp']
#         year = timestamp[0:4]
#         month = timestamp[4:6]
#         day = timestamp[6:8]
#         hour = timestamp[8:10]
#         date = datetime.date(int(year), int(month), int(day))
#
#
#         day_of_week = date.timetuple().tm_wday
#
#         view = int(day_item['views'])
#         if int(month) in daycount.keys():
#             pass
#         else:
#             daycount[int(month)] = {}
#
#         if day_of_week in day_of_weeks.keys():
#             day_of_weeks[day_of_week] = day_of_weeks[day_of_week] + view
#         else:
#             day_of_weeks[day_of_week] = view
#
#         if (int(day) in daycount[int(month)].keys()):
#             daycount[int(month)][int(day)] = daycount[int(month)][int(day)] + view
#             daycount_txt[str(int(month)) + '_' + str(int(day))] = daycount_txt[str(int(month)) + '_' + str(int(day))] + view
#         else:
#             daycount[int(month)][int(day)] = view
#             daycount_txt[str(int(month)) + '_' + str(int(day))] = view
#
# except IndexError as e:
#     print(e)
#
# for key in daycount_txt:
#     f.write(key)
#     f.write(';')
#     f.write(str(daycount_txt[key]))
#     f.write('\n')
#
# print(day_of_weeks)
# print(data)