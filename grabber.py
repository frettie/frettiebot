#!/usr/bin/python
# -*- coding: utf-8 -*-
import pywikibot
import sys
import exceptions
from logger import Logger
import re
import curses
from pandas import *

class Grabber:
    def __init__(self):
        print 'fillWikidata'
        self.logger = Logger(u'grabber','saved')

    def loadPages(self, category):

        self.errors = {}
        site = pywikibot.getSite('cs', 'wikipedia')

        page = pywikibot.Page(site, u'Category:' + category)

        generator = pywikibot.Category(page).articles()
        stopRunBecauseNotCatInCities = False

        for page in generator:
            match = re.search(u'(.*)([0-9]+)', page.title())
            if (not match):
                continue
            #match = re.search(u'2014', page.title())
            match = True
            if (match):
                self.pageProcess(page)


    def pageProcess(self, page):
        start = page.get().find('{|')
        end = page.get().find('|}')
        tablew = page.get()[start:end+2]

        #print tablew
        match = re.search(u'(.*) ([0-9]{4})', page.title())
        if (match):
            name = match.group(1)
        else:
            name = page.title()
        rows = tablew.split('|-')
        header = []
        table = []
        for i in rows:
             line = i.strip()
             if ((line.startswith('|')) or (line.startswith('!'))):
                data = line[2:].replace("'''","").split('||')
                data2 = []
                for k in data:
                    if (re.search('bgcolor(.*)',k)):

                        rep = re.sub(u'{{(.*)}}',u'', k)

                        rep2 = re.sub(u'\[\[Soubor(.*)x\]\]',u'', rep)

                        col = rep2.split('|')

                        if (len(col) > 3):
                            country = col[2].replace('[[','').strip()
                        else:
                            country = col[2].replace('[[','').replace(']]','').strip()

                        data2.append(col[1].strip())
                        if (country == u'Československá basketbalová reprezentace'):
                            country = u'Československo'
                        data2.append(country)
                    elif (re.search('colspan(.*)',k)):
                        data2.append(u'nazev')
                        data2.append(k.split('|')[2].replace(']]',''))
                    else:
                        rep = re.sub(u'\[\[Soubor(.*)x\]\]',u'', k)

                        data2.append(rep.strip())

                if (len(data2)):
                    table.append(data2)

        data = {}
        grabbed = []
        row = {}
        for j in table:

            if (j[0] == u'Datum'):
                match = re.search(u'\[\[(.*)\|(.*)\]\] – \[\[(.*)\|(.*)\]\] \[\[(.*)\]\]', j[1])#
                datum = {}
                datum[u'start_den'] = match.group(1)
                datum[u'end_den'] = match.group(3)
                datum[u'rok'] = match.group(5)
                row[u'Datum'] = datum;
            elif (j[0] == u'Města'):
                neco = j[1].replace('<br />', '').replace('[[','').replace(']]','').replace('\n','')
                row[u'Město'] = neco
            else:
                if (len(j) > 1):
                    #print j[1]
                    match = re.search(u'\[\[(.*)(\|(.*)|)\]\]', j[1])#
                    if (match):
                        spl = match.group(1).split('|')
                        if (len(spl) > 1):
                            vys = spl[0]
                        else:
                            vys = match.group(1)
                        row[j[0]] = vys
                    else:
                        row[j[0]] = j[1]


        infobox = u'''
        {{Infobox - mistrovství světa
            | název = ''' + row[u'nazev'] + u'''
            | země = ''' + row[u'Země'] + u'''
            | země odkaz = ''' + row[u'Země'] + u'''
            | město = ''' + row[u'Město'] + u'''
            | město odkaz = ''' + row[u'Město'] + u'''
            | datum start = ''' + row[u'Datum'][u'start_den'] + u'''
            | datum konec = ''' + row[u'Datum'][u'end_den'] + u'''
            | rok = ''' + row[u'Datum'][u'rok'] + u'''
            | zlato = ''' + row[u'Zlato'] + u'''
            | stříbro = ''' + row[u'Stříbro'] + u'''
            | bronz = ''' + row[u'Bronz'] + u'''
            | sport = házená
        }}
        '''

        tablew =  infobox + page.get()[end+2:]
        #print tablew
        #print grabbed
        df = DataFrame(data)
        page.text = tablew
        page.save()

f = Grabber()
f.loadPages(u'ME ve volejbale mužů')

#f = FillWikidata(u'umrti')
#f.loadPages(u'Úmrtí', u' podle měst Česka')
