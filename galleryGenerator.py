from datetime import datetime
import sys
import csv
import requests

import pywikibot
import re
# from pywikibot import pagegenerators
# import dateutil.parser
import time
import json
from logger import Logger


class GalleryGenerator:

    def __init__(self):
        print('galleryGenerator')
        self.setup()
        # self.loadUsers()
        # self.loadUploads()
        # self.getFilesWithoutStructuredData('Frettie', 2017)
        self.getFilesWithoutStructuredData('Frettie')
        # self.filesByDump()

    def setup(self):
        self.site = pywikibot.Site('cs', 'wikipedia')
        self.commons = self.site.image_repository()
        self.users = []

    def loadUsers(self):
        page = pywikibot.Page(self.commons, 'User:Frettiebot/Galleries/Users')
        text = page.get().splitlines()
        regex = "\*\s*(.*)"

        for line in text:
            matches = re.match(regex, line)
            self.users.append(matches.groups()[0])

    def getLastMonth(self, user):
        try:
            page = pywikibot.Page(self.commons, 'User:Frettiebot/Galleries/' + user)
            text = page.get().splitlines()
            regex = "\*\s*\[\[.*\|(\d{4})\/(\d{1,2})\]\]"
            lastline = text[-1]
            matches = re.match(regex, lastline)
            try:
                return matches.groups()
            except AttributeError as a:
                return None
        except pywikibot.exceptions.NoPage as n:
            return None




    def loadUploads(self):
        for user in self.users:
            self.getUploads(user)

    def getUploads(self, user):
        # self.genFactory = pagegenerators.GeneratorFactory(site=self.commons)
        # self.genFactory.handleArg('-limit:2500')
        # self.generator = self.genFactory.getCombinedGenerator()
        # if not self.generator:
        date = self.getLastMonth(user)
        if date is not None:
            year = date[0]
            month = date[1]
            from datetime import timezone
            import datetime
            dt = datetime.datetime(int(year), int(month), 1, 0, 0, 0)
            timestamp = dt.replace(tzinfo=timezone.utc).timestamp()
            data = self.commons.logevents(logtype='upload', user=user, start=timestamp, reverse=True)
        else:
            data = self.commons.logevents(logtype='upload', user=user, reverse=True)




        files = {'years' : {}}
        page = pywikibot.Page(self.commons, 'User:Frettiebot/Galleries/' + user)
        pageExist = True
        try:
            page.get()
        except pywikibot.exceptions.NoPage as n:
            pageExist = False
        textUserPage = 'Galerie pro uživatele [[User:' + user + '|' + user + ']].\n'
        for file in data:


            title = file.data['title']
            timestamp = file.data['timestamp']
            print(timestamp)
            comment = file.data['comment']


            year = dateutil.parser.parse(file.data['timestamp']).year
            month = dateutil.parser.parse(file.data['timestamp']).month
            day = dateutil.parser.parse(file.data['timestamp']).day

            desc = ''
            try:
                for template in file.page().raw_extracted_templates:
                    if (template[0] == 'Information'):
                        desc = template[1]['description']
            except KeyError as k:
                pass
            try:
                files['years'][year][month][day].append({'title': title, 'timestamp': timestamp, 'comment': comment, 'description': desc})
            except KeyError as e:
                if year not in files['years']:
                    files['years'][year] = {}
                if month not in files['years'][year]:
                    files['years'][year][month] = {}

                files['years'][year][month][day] = []
                files['years'][year][month][day].append({'title': title, 'timestamp': timestamp, 'comment': comment, 'description': desc})




        for year in files['years']:
            months = files['years'][year]

            for m in months:

                textMonthPage = 'Načtené fotografie uživatele ' + user + ' za ' + str(m) + '/' + str(year) + '\n\n'

                append = '* [[User:Frettiebot/Galleries/' + user + '/' + str(year) + '/' + str(m) + '|' + str(year) + '/' + str(m) + ']]'
                textUserPage = textUserPage + '\n' + append
                for d in months[m]:
                    textMonthPage = textMonthPage + '== ' + str(d) + ' ==\n\n'
                    textMonthPage = textMonthPage + '<gallery>\n'
                    for f in months[m][d]:

                        textMonthPage = textMonthPage + f['title'] + '|' + ''.join(f['description'].splitlines()) + '\n'
                    textMonthPage = textMonthPage + '</gallery>\n\n'
                print(textMonthPage)
                monthPage = pywikibot.Page(self.commons, 'User:Frettiebot/Galleries/' + user + '/' + str(year) + '/' + str(m))
                monthPage.text = textMonthPage
                monthPage.save()

        if pageExist:
            #only append
            tlines = page.get().splitlines()
            del tlines[-1]
            textUserPageAppend = ''
            for l in tlines:
                textUserPageAppend = textUserPageAppend + l + '\n'

            textUserPageAppend = textUserPageAppend + append
            page.text = textUserPageAppend
            page.save()
        else:
            #create new
            page.text = textUserPage
            page.save()
        # logger = Logger(u'magic' + u'Fill', 'process')
        # pages = {}
        # iter = 0

    def get_timestamp(self):
        now = datetime.now()
        timestamp = datetime.timestamp(now)
        return timestamp

    def getFilesWithoutStructuredData(self, user, year = None, month = None, endDay = None):
        # self.genFactory = pagegenerators.GeneratorFactory(site=self.commons)
        # self.genFactory.handleArg('-limit:2500')
        # self.generator = self.genFactory.getCombinedGenerator()
        # if not self.generator:
        import time
        if (year is not None):
            if (month is None):
                start_month = '01'
                end_month = '12'
            else:
                start_month = month
                end_month = month
            s = str(year) + '-' + start_month + '-01 00:00:01'
            start = datetime.strptime(s, '%Y-%m-%d %H:%M:%S')
            if endDay is None:
                endDay = '31'
            e = str(year) + '-' + end_month + '-' + endDay + ' 23:59:59'
            end = datetime.strptime(e, '%Y-%m-%d %H:%M:%S')
        print('start')
        print(time.ctime())
        if (year is not None):
            data = self.commons.logevents(logtype='upload', user=user, reverse=False, start = end, end = start)
        else:
            data = self.commons.logevents(logtype='upload', user=user, reverse=False)

        itered = list(data)

        iter_length = len(itered)

        print('celkem: ' + str(iter_length))




        logger = Logger('sdcFiles')
        files = {'years' : {}}
        # page = pywikibot.Page(self.commons, 'User:Frettiebot/Structured/' + user)
        pageExist = True
        # try:
            # page.get()
        # except pywikibot.exceptions.NoPage as n:
        #     pageExist = False
        textUserPage = 'Galerie pro uživatele [[User:' + user + '|' + user + ']].\n'
        myCats = []
        excludeCats = []

        count = 0
        start_timestamp = self.get_timestamp()
        for file in itered:
            count = count + 1
            percent = 100 * count / iter_length
            if (count % 100 == 0):
                now_timestamp = self.get_timestamp()
                diff = now_timestamp-start_timestamp
                start_timestamp = now_timestamp
                print('* ' + str(count) + ' z ' + str(iter_length) + ' ... procenta: ' + str(percent) + ' % ... ' + str(diff) + ' s od predchoziho')
            #
            title = file.data['title']
            # timestamp = file.data['timestamp']
            # print(timestamp)
            # comment = file.data['comment']
            try:
                if not logger.isCompleteFile(title):
                    page = file.page()
                    while page.isRedirectPage() != False:
                        page = page.getRedirectTarget()

                    revs = page.revisions()
                    hasStructuredData = False
                    latest_rev = page.latest_revision
                    if 'mediainfo' in latest_rev.slots:
                        slot = latest_rev.slots['mediainfo']['*']
                        props = json.loads(slot)
                        statements = props['statements']
                        if 'P180' in statements:
                            hasStructuredData = True


                    # for rev in revs:
                    #     if 'mediainfo' in rev.slots:
                    #         hasStructuredData = True
                    if hasStructuredData:
                        logger.logComplete(title)

                    if (not hasStructuredData):
                        cats = page.categories()
                        for cat in cats:
                            if (not (cat.title() in myCats)):
                                if (not cat.title() in excludeCats):
                                    myCats.append(cat.title())
                                    print(title)
                                    print('* [[:' + cat.title() + '|' + cat.title() + ']]')
            except Exception as e:
                logger.logComplete(title)
                pass

        print('end')
        print(time.ctime())

    def filesByDump(self):
        categories = {}
        url = "https://query.wikidata.org/sparql?query=select%20%3Fitem%20%3Fimg%20%3Fsitelink%20where%20%7B%0A%0A%3Fitem%20wdt%3AP6736%20%5B%5D.%0A%3Fitem%20wdt%3AP18%20%3Fimg%20.%0Aminus%20%7B%3Fsitelink%20schema%3Aabout%20%3Fitem.%20%3Fsitelink%20schema%3AisPartOf%20%3Chttps%3A%2F%2Fcommons.wikimedia.org%2F%3E%20.%7D%0A%0A%7D&format=json"
        r = requests.get(url)
        wd_json = r.json()
        items = []
        for imgitem in wd_json['results']['bindings']:
            qid = imgitem['item']['value'].replace('http://www.wikidata.org/entity/', '')
            img = imgitem['img']['value'].replace('http://commons.wikimedia.org/wiki/Special:FilePath/', '')

            items.append({'qid' : qid, 'img' : img})

        url = "https://query.wikidata.org/sparql?query=select%20%2a%20where%20%7B%0A%3Fitem%20wdt%3AP17%20wd%3AQ213.%0A%20%20%3Fitem%20wdt%3AP373%20%3Fcategory%0A%7D&format=json"
        r = requests.get(url)
        wd_json = r.json()
        connected_cats = []
        for imgitem in wd_json['results']['bindings']:
            qid = imgitem['item']['value'].replace('http://www.wikidata.org/entity/', '')
            cat = imgitem['category']['value']

            connected_cats.append(cat)

        url = "https://query.wikidata.org/sparql?query=select%20%3Fitem%20%3Fimg%20%3Fsitelink%20where%20%7B%0A%0A%3Fitem%20wdt%3AP17%20wd%3AQ213.%0A%3Fsitelink%20schema%3Aabout%20%3Fitem.%20%3Fsitelink%20schema%3AisPartOf%20%3Chttps%3A%2F%2Fcommons.wikimedia.org%2F%3E%20.%0A%0A%7D&format=json"
        r = requests.get(url)
        wd_json = r.json()
        for imgitem in wd_json['results']['bindings']:
            qid = imgitem['item']['value'].replace('http://www.wikidata.org/entity/', '')
            cat = imgitem['sitelink']['value'].replace("https://commons.wikimedia.org/wiki/", '').replace('_', ' ')

            connected_cats.append(cat)
        general_cats = ['CC-BY-SA-3.0','CC-BY-SA-4.0','Images from Wiki Loves Monuments 2014','Images from Wiki Loves Monuments 2014 in the Czech Republic','Mediagrant:Foto českých obcí','Mediagrant 2014–2015','Files by User:Harold','CC-BY-SA-2.0','Winter in Czechia','Photos by SchiDD/2017','Statues of Jan Hus in Czechia','Bedřich Smetana','Unassessed QI candidates','Winter in Kutná Hora','Czech Photo Workshop – 2010/II','Taken with Olympus FE-350 / X-865','Sculptures by Jan Tříska','Incorrect date','Files with no machine-readable source',
                        'Cultural monuments in the Czech Republic with known IDs','Uploaded via Campaign:wlm-cz','Pages with maps','Pages with local camera coordinates and missing SDC coordinates','Statues of Saint Peter in the Czech Republic','Statues of Saint Paul in the Czech Republic','License migration redundant','Statues in Brno','Mediagrant 2019','Photos by User:Jklamo','Uploaded with pattypan','Statues of the Pensive Christ in the Czech Republic','Baroque sculptures in the Czech Republic','Ruins of bell towers','Mediagrant:Fotoworkshop Maršovice 2015','Mediagrant:Fotoworkshop Nové Město','Taken with Olympus E-400','Files by User:Gumruch from cs.wikipedia/Objekty','Czech inscriptions before 1842 orthography reform','Photos by SchiDD/2011','Taken with Panasonic Lumix DMC-LZ5','Taken with VQ DigitalCAM',
                        'Self-published work','John of Nepomuk with fur mozzetta in the Czech Republic','Images from Wiki Loves Monuments 2012','Images from Wiki Loves Monuments 2012 in the Czech Republic','Statues of Jesus in the Czech Republic','Images by User:Podzemnik','Images of Czechia by User:Podzemnik','GFDL','Files with derivative versions','Files by User:Dezidor','Statues of Saint Barbara in the Czech Republic','Mediagrant:Wikiexpedice Orlické hory','Images from Wiki Loves Monuments 2013','Images from Wiki Loves Monuments 2013 in the Czech Republic','Pages with local object coordinates and missing SDC coordinates','Mobile to desktop upload','Taken with Asus Memo Pad HD 7','Media uploaded from placeholders on Czech Wikipedia/Revised','Monuments and memorials to writers','Bronze statues','Flickr images reviewed by FlickreviewR 2',
                        'Statues of Francis Xavier in the Czech Republic','Statues of Immaculata in South Moravian Region','Statues of Saint Ignatius of Loyola in the Czech Republic','Images from Wiki Loves Monuments 2013','Images from Wiki Loves Monuments 2013 in the Czech Republic','Wooden bell towers in the Czech Republic','CC-BY-3.0','Files by User:Chmee2','First year of the Czech Municipalities Photographs grant','Maria columns with Immaculata in the Czech Republic','CC-BY-SA-2.5,2.0,1.0','CC-BY-SA-3.0-migrated','Media lacking a description','Photos by SchiDD/2015','Media by User:Aktron (converted from RAW)','Taken with Canon EOS 600D','Uploaded with Android WLM App','Statues of Salvator mundi','Photographs by Anonymous Hong Kong Photographer 1','Photos by SchiDD/2014','Wayside crosses in Třešť','Taken with Canon EOS 1000D',
                        'License migration completed','Stone walls in the Czech Republic','Mediagrant 2016–2017','Photos taken with WMCZ camera','Uploaded with VicuñaUploader','Photos by SchiDD/2016','Mediagrant:Fotíme Česko','Taken with Canon PowerShot SX200 IS','Statues in the Smetana Park in Hořice','Third year of the Czech Municipalities Photographs grant','2012 in Prague','Files by User:Jedudedek','PD-user','Taken with Jenoptik JD C 3.1 z3','CC-BY-SA-2.0-DE','CC-BY-2.5','Statues of Jan Žižka','Files by Jiří Komárek','Taken with Xiaomi Mi 2','Taken with Sony DSC-H9','Taken with ZTE','Mediagrant 2018','Mediagrant:Fotoworkshop Pelhřimov 2015','Files by User:RomanM82','Mediagrant 2018','Religion in Chomutov','Taken with Sony DSC-F828','Pictures taken during WikiMěsto Litovel','Column shrines in Třeboň','Mediagrant 2020',
                        'User-created GFDL images','Walls in Ústí nad Labem Region','Statues of Saint Matthew in the Czech Republic','Mediagrant 2011–2013','Files by User:Richenza','Taken with Nikon D40x','Files by User:palickap','CC-BY-4.0','Sculptures in Brandýs nad Labem','CC-BY-SA-3.0,2.5,2.0,1.0','Uploaded with Commonist','Files by Juandev without description','Statues in Hrob','Flickr images reviewed by File Upload Bot (Magnus Manske)','CC-BY-SA-4.0,3.0,2.5,2.0,1.0','Statues in Hradec Králové','CC-Zero','Statues in Česká Lípa','Uploaded with Mobile/Android','Taken with Panasonic Lumix DMC-FS6','Files by User:Jan Polák','Second year of the Czech Municipalities Photographs grant','Sculptures of chalices','Photographs by Vít Švajcr','Files by User:MIGORMCZ/Wikiexpedice Orlické hory','Mediagrant:Fotíme Česko:2020 in Fotíme Česko (temporary)',
                        'PD-self','Media with locations','Taken with Canon PowerShot G16','Brněnské focení, 2016-04-30','Photos by User:Bazi','CC-BY-SA-2.5','Items with OTRS permission confirmed','P6305 SDC','Created with Qtpfsgui','Files by User:Jagro','Statues in Hořice','Mediagrant:Fotoworkshop WikiMěsto','Files by Juandev','Files by Juandev without categories','CC-BY-2.0','Files from Steve Collis Flickr stream','2014 in Prague','July 2014 Czech Republic photographs','Taken with Olympus E-PL1','Zahrada Vlastivědného musea Česká Lípa','Uploaded with Mobile/Android (Nov 2017-Aug 2019)','Mediagrant:Wikiexpedice Jih','Geocoded images of User:Aktron','Mediagrant:1.2. Aktivity pro komunitu','Statues of Jesus Christ as the Good Shepherd','Náměstí (Horní Planá)','Taken with Panasonic Lumix DMC-LX7'
                        ]
        i = 0
        for image in items:
            i = i + 1
            file = pywikibot.FilePage(self.commons, 'File:'+image['img'])
            cats = []
            for cat in file.categories():
                # cat.data_item())
                if (cat.title().replace('Category:','') not in connected_cats):
                    if (cat.title().replace('Category:', '')) not in general_cats:
                        regex = u"District|Region"

                        match = re.search(regex, cat.title().replace('Category:', ''), re.IGNORECASE)
                        if not match:
                            regex = u"taken on"

                            match = re.search(regex, cat.title().replace('Category:', ''), re.IGNORECASE)
                            if not match:
                                regex = u"in the Czech|Cultural monuments|Sculptures in|Statues in|Buildings in|Religion in|Built in|Chapels in"

                                match = re.search(regex, cat.title().replace('Category:', ''), re.IGNORECASE)
                                if not match:
                                    cats.append(cat.title())
                                else:
                                    pass
                                    # print(cat.title())
                            else:
                                pass
                                # print(cat.title())
                        else:
                            pass
                            # print(cat.title())
                    else:
                        pass
                        # print(cat.title())
                else:
                    pass
                    # print(cat.title())
            if (i % 500 == 0):
                print(cat.title())
            if (len(cats) > 0):
                categories[image['qid']] = cats
            with open('img_cat.csv', 'w', newline='') as csvfile:
                writer = csv.writer(csvfile, delimiter=";", quotechar="'", quoting=csv.QUOTE_ALL)
                for key, value in categories.items():
                    qid = key
                    cats = value
                    writer.writerow([qid] + cats)








g = GalleryGenerator()