#!/usr/bin/python
# -*- coding: utf-8 -*-

from restkit import Resource
import simplejson as json
res = Resource('https://tools.wmflabs.org:443/wikidata-primary-sources')

r = res.get('/entities/any')

body = r.body_string()

print json.dumps(body)
