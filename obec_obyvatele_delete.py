#!/usr/bin/python
# -*- coding: utf-8 -*-
import pywikibot
import codecs
import re


class ObyvatelDeleter:
    def __init__(self):
        print 'ObyvatelDeleter'
        self.templateName = u"Infobox - česká obec"
        self.typeEntity = 'obecinfoboxcheck'

    def run(self):
        site = pywikibot.getSite('cs', 'wikipedia')
        generator = pywikibot.Page(site, u'Template:' + self.templateName).embeddedin()
        counter = 0
        for page in generator:

            #print '----------------'
            #print page.title() + ' - check'
            #print '----------------'
            if not(self.isCompleteFile(page.title())):
                counter = counter + 1
                self.process(page)
            if counter == 100:
                exit()

    def process(self, page):
        if (page.namespace() == 0):
            #print '----------------'
            print page.title() + ' - zpracovavam'
            #print '----------------'

            vyskov = page.get().replace('@','zavinatz')
            vyskov2 = vyskov.replace('</ref>','@')
            #print vyskov2
            skip = False
            vysName = re.search(u'\n([ ]+)\|([ ]*)(počet obyvatel|obyvatelé datum)([ ]*)=(.*)name(.*)',vyskov2,flags=re.IGNORECASE)
            staturad = re.search(u'\n([ ]+)\|([ ]*)(počet obyvatel|obyvatelé datum)([ ]*)=(.*)name(.*)',vyskov2,flags=re.IGNORECASE)
            vysRef = re.search(u'\n([ ]+)\|([ ]*)(počet obyvatel|obyvatelé datum)([ ]*)=(.*)ref(.*)',vyskov2,flags=re.IGNORECASE)
            if vysName:
                skip = True
                if staturad:
                    skip = False

            if vysRef:
                vys = re.search(u'\n([ ]+)\|([ ]*)(počet obyvatel|zeměpisná šířka|zeměpisná délka|obyvatelé datum)([ ]*)=([ ]*)([^@]*)@',vyskov2,flags=re.MULTILINE)
                #print vys.groups()
                mytext = re.sub(u'\n([ ]+)\|([ ]*)(počet obyvatel|obyvatelé datum)([ ]*)=([ ]*)([^@]*)@', u'', vyskov2, flags=re.MULTILINE)
                mytext2 = re.sub(u'\n([ ]+)\|([ ]*)(zeměpisná šířka|zeměpisná délka)([ ]*)=([ ]*)(.*)', u'', mytext, flags=re.MULTILINE)

            else:
                mytext = re.sub(u'\n([ ]+)\|([ ]*)(počet obyvatel|obyvatelé datum)([ ]*)=([ ]*)(.*)', u'', vyskov2, flags=re.MULTILINE)
                mytext2 = re.sub(u'\n([ ]+)\|([ ]*)(zeměpisná šířka|zeměpisná délka)([ ]*)=([ ]*)(.*)', u'', mytext, flags=re.MULTILINE)
            mytext3 = mytext2.replace('@','</ref>')
            mytext4 = mytext3.replace('zavinatz','@')
            if (skip):
                print u"skipped " + page.title()
                self.logComplete(page.title())
                exit()
            else:
                page.text = mytext4
                length = page.text.index("'''",0,len(page.text))
                print page.text[0:length+20]
                page.save(summary = u'Úprava infoboxu dle konsenzu')


            self.logComplete(page.title())
            #exit()

    def check(self):
        site = pywikibot.getSite('cs', 'wikipedia')
        generator = pywikibot.Page(site, u'Template:' + self.templateName).embeddedin()
        for page in generator:

            if not(self.isCompleteFile(page.title())):
                self.checkPage(page)

    def checkPage(self, page):
        if (page.namespace() == 0):
            #print page.title() + ' - zpracovavam'
            length = page.text.index("'''",0,len(page.text))
            text = page.text[0:length+20]

            vys = re.search(u'(počet obyvatel|zeměpisná šířka|zeměpisná délka|obyvatelé datum)',text,flags=re.IGNORECASE)

            if vys:
                print vys.groups()
                print page.title() + 'chyba'


            self.logComplete(page.title())
            #exit()

    def process_template(self, tpl, page):
        print ''
        exit()


    def logComplete(self, title):
        file = codecs.open("log_" + self.typeEntity + ".txt", "a", "utf-8")
        file.write(title + '\n')
        file.close()

    def logError(self, title):
        file = codecs.open("log_error_" + self.typeEntity + ".txt", "a", "utf-8")
        file.write(title + '\n')
        file.close()

    def isCompleteFile(self, entity):
        try:
            file = codecs.open("log_" + self.typeEntity + ".txt", "r", "utf-8")
            lines = file.readlines()
            list_entity = list()
            file.close()
            for it in lines:
                list_entity.append(it.strip())
            #TODO opravit!

            try:
                list_entity.index(entity)
                return True
            except ValueError:
                return False
        except IOError:
            print "Not exist file"
            codecs.open('log_' + self.typeEntity + '.txt', 'w', 'utf-8')

class RelativeObject(ObyvatelDeleter):
    def __init__(self, reltype, name, wikidata, page):
        self.typeEntity = 'relatives'
        self.type = reltype
        self.name = name
        self.link = ""
        self.page = page
        self.property = 0
        self.wikidata = wikidata
        self.types = {
            u'manželka' : '26',
            u'manžel' : '26',
            u'otec' : '22',
            u'matka' : '25',
            u'bratr' : '7',
            u'sestra' : '9',
            u'syn' : '40',
            u'dcera' : '40',
            u'dědeček' : '45',
            u'babička' : '45',
            u'děd' : '45',
            u'kmotr' : '1290',
            u'otčím' : '43',
            u'macecha' : '44',
            u'partner' : '451',
            u'životní partner' : '451'
            }

        self.legalTypes = ['26', '22', '25', '7', '9', '40', '45', '1290', '43', '44', '451']
        self.clean()

    def clean(self):
        if (re.search('\[\[', self.name)):
            print self.name
            try:
                mat = re.match('\[\[(\D+)\]\]', self.name)
                array = mat.groups()
                self.name = array[0]
            except AttributeError:
                self.name = self.name

    def logNew(self, title):
        file = codecs.open("log_new_" + self.typeEntity + ".txt", "a", "utf-8")
        file.write(title + '\n')
        file.close()


    def myprint(self):
        print self.name + ' - ' + self.type

    def getRelativeEntityId(self):
        site = pywikibot.getSite('cs', 'wikipedia')
        entity = pywikibot.Page(site, self.name)
        entity_wikidata = pywikibot.DataPage(entity)
        entity_add = entity_wikidata.getID()

        return entity_add

    def getClaims(self):
        dictionary = self.wikidata.get()
        claims = list()
        for c in dictionary['claims']:
            claims.append(c['m'][1])

        return claims

    def createPerson(self, page):
        datapage = pywikibot.DataPage(page)
        cats = page.categories();
        male = False
        for cat in cats:
            if (re.search(u'Muži', cat.title())):
                male = True

        datapage.createitem(summary='New item from wikipedia page')
        refes = {('P143','Q191168')}
        datapage.editclaim(31, 'Q5', refs=refes, override=False)
        if male:
            datapage.editclaim(21, 'Q6581097', refs=refes, override=False)
        else:
            datapage.editclaim(21, 'Q6581072', refs=refes, override=False)

        page.put(page.get())


    def save(self):
        refes = {('P143','Q191168')}
        continueWithSaving = True

        if (re.search(u'manžel', self.type)):
            self.type = u'manžel'

        self.type = self.type.lower()

        if (re.search('\|', self.name)):
            res = re.split('\|', self.name)
            self.name = res[0]

        if (self.type in self.types.keys()):
            self.property = self.types[self.type]
        else:
            print u"neni zaveden typ pribuzenstvi " + self.type
            continueWithSaving = False

        try:
            claims = self.getClaims()
            if not(int(self.property) in claims) and continueWithSaving:

                entity_add = self.getRelativeEntityId()
                print u'vlozeno ' + self.name + ' - ' + self.type
                self.wikidata.editclaim(self.property, entity_add, refs=refes, override=False)
        except pywikibot.exceptions.InvalidTitle:
            print u'spatne vyplneny titulek v tpl u ' + self.page.title()
            self.logError(self.name)
        except pywikibot.exceptions.NoPage:
            print u"neexistuje stránka " + self.page.title()
            self.createPerson(self.page)
            self.logNew(self.name)


n = ObyvatelDeleter()
n.check()
