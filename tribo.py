import itertools as IT
import numpy

M = numpy.matrix([[1, 1, 0], [1, 0, 1], [1, 0, 0]], dtype=float)


def tribonacci():
    a, b, c = 0, 0, 1
    while 1:
        yield c
        a, b, c = b, c, a + b + c


def tripow(N):
    return int((M ** N)[0, 0])


print list(IT.islice(tribonacci(), 38))  # -> [1, 1, 2, 4, 7, 13, 24, 44, 81, 149]
print tripow(36)  # -> 149
x = list(IT.islice(tribonacci(), 1000))
print x[-1] / float(x[-2])  # -> 1.83928675521

# 38 faces (32 triangular and 6 square), 60 edges, and 24 vertices.
h = 60
v = 24
s = 38

print tripow(36)

# 1, 2, 4, 7, 13, 24, 44, 81, 149, 274, 504, 927, 1705, 3136, 5768, 10609, 19513, 35890, 66012, 121415, 223317, 410744, 755476, 1389537, 2555757, 4700770, 8646064, 15902591, 29249425, 53798080, 98950096, 181997601, 334745777, 615693474, 1132436852, 2082876103, 3831006429, 7046319384, 12960201916, 23837527729, 43844049029, 80641778674, 148323355432, 272809183135, 501774317241, 922906855808, 1697490356184, 3122171529233, 5742568741225, 10562230626642, 19426970897100, 35731770264967, 65720971788709, 120879712950776, 222332455004452, 408933139743937, 752145307699165.


a = (tripow(h-v-7) % 10000) - tripow(15) - tripow(11) - tripow(8) - tripow(6) - tripow(4)
b = ((tripow(h-s-1) % 100000) + tripow(12) + tripow(8) + tripow(6) + tripow(5) + tripow(3))
print float(float(a)/1000)
print float(float(b)/1000)
# A = (Trib(H-V) MOD 10000) - Trib(15) - Trib(11) - Trib(8) - Trib(6) - Trib (4)
# B = (Trib(H-S) MOD 100000) + Trib(12) + Trib(8) + Trib(6) + Trib(5) + Trib(3)