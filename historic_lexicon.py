#!/usr/bin/python
# -*- coding: utf-8 -*-
import collections
import csv
import datetime

import pywikibot
from logger import Logger
# from boto3.dynamodb.conditions import Key, Attr

# Import the email modules we'll need




class HistoricLexicon:
    def __init__(self, logger_name):
        self.errors = {}
        print('historicLexiconHouses')

        self.logger = Logger(logger_name + u'Fill', 'saved')
        self.actual_page = None
        self.wikidata = None
        self.language_wikipedia = 'cs'
        self.wikidata_family = 'wikidata'
        self.family = 'wikipedia'

        self.load_file('nove-domy.csv')



    def load_file(self, file_name):
        #'ukazka-dat-obyvatelstvo-2001.csv'
        # radek;entity;property;house; qualifier-time; time; qualifier-count; count; source; reference
        # 1;Q1001619;P527;Q3947;P585;+1869 - 01 - 01T00:00:00Z / 09;P1114;33;S248;Q28708818

        # 29676	Q56417401	P1082	357	P585	+2001-00-00T00:00:00Z/09	P459	Q56229410	P1013	Q56229498	S248	Q28708818	S813	+2017-07-18T00:00:00Z/11

        wikidata = pywikibot.getSite('wikidata')
        property = pywikibot.Claim(wikidata, 'P1082')

        source = pywikibot.Claim(wikidata, 'P248')
        source.setTarget(pywikibot.ItemPage(wikidata, 'Q28708818'))

        ##+2017-07-18T00:00:00Z/11
        source_time = pywikibot.Claim(wikidata, 'P813')
        date_source = pywikibot.WbTime(year=2017, month=7, day=18, precision=11)
        source_time.setTarget(date_source)

        qualifier_time = pywikibot.Claim(wikidata, 'P585')
        qualifier_determ = pywikibot.Claim(wikidata, 'P459')
        qualifier_criterion = pywikibot.Claim(wikidata, 'P1013')
        # qualifier_count = pywikibot.Claim(wikidata, 'P1114')

        # house = pywikibot.ItemPage(wikidata, 'Q3947')

        # property.
        try:
            with open(file_name, 'rt') as csvfile:
                reader = csv.DictReader(csvfile, delimiter=';')
                for line in reader:
                    if not self.logger.isCompleteFile(line['radek']):
                        entity = pywikibot.ItemPage(wikidata, line['entity'])
                        # +2001-01-01T00:00:00Z/09
                        property.sources = []
                        property.qualifiers = collections.OrderedDict()
                        datetime_object = datetime.datetime.strptime(line['time'], '+%Y-%m-%dT00:00:00Z/09')

                        date = pywikibot.WbTime(year=datetime_object.year)
                        qualifier_time.setTarget(date)

                        qualifier_determ.setTarget(pywikibot.ItemPage(wikidata, line['determ']))
                        qualifier_criterion.setTarget(pywikibot.ItemPage(wikidata, line['criterion']))

                        count = pywikibot.WbQuantity(line['count'], site=wikidata)

                        property.setTarget(count)

                        # property.addQualifier(qualifier)
                        # property.addSource(source)
                        entity.addClaim(property)
                        # if (datetime_object.year == 2011):
                        #     property.changeRank(u'preferred')


                        property.addQualifier(qualifier_time)
                        property.addQualifier(qualifier_determ)
                        property.addQualifier(qualifier_criterion)

                        property.addSources([source,source_time])

                        self.logger.logComplete(line['radek'])
                        try:
                            print(entity.labels[u'cs'] + u' ' + str(datetime_object.year))
                        except ValueError:
                            print(u"chyba labelu")
        except pywikibot.data.api.APIError as e:
            self.load_file('nove-domy.csv')

    def page_process(self, page):
        pass

    def run(self):
        pass

f = HistoricLexicon('lexiconHousesNew')
# f.insert_to_redis()
f.run()
