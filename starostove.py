#!/usr/bin/python
# -*- coding: utf-8 -*-
import json
import re
import unicodecsv
import requests
import pywikibot

class excel_semicolon(unicodecsv.Dialect):
    """Describe the usual properties of Excel-generated CSV files."""
    delimiter = ';'
    quotechar = '"'
    doublequote = True
    skipinitialspace = False
    lineterminator = '\r\n'
    quoting = unicodecsv.QUOTE_MINIMAL

def prepare():
    urlstarostove = "https://cro.justice.cz/verejnost/api/funkcionari?filter%5B0%5D.field=concatenatedWorkingPositions&filter%5B0%5D.operation=CONTAINS&filter%5B0%5D.value=starosta&filter%5B1%5D.field=workingFrom&filter%5B1%5D.operation=GTE&filter%5B1%5D.value=2018-10-06&sort=created&order=DESC&page=0&pageSize=10"

    response = requests.get(urlstarostove)
    jsontext = response.text
    data = json.loads(jsontext)
    for line in data['items']:
        for position in line['workingPositions']:
            if position['workingPosition']['name'] == 'starosta':
                print(line['firstName'], line['lastName'], line['concatenatedWorkingPositions'])

prepare()