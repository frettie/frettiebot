#!/usr/bin/python
# -*- coding: utf-8 -*-
import time

import json
from datetime import datetime

import requests
import urllib.parse

# print(pages[lang])
# time.sleep(3)
lang = 'cs'
page = 'Pandemie covidu-19 v Česku'
#/w/api.php?action=query&format=json&prop=revisions&titles=Pandemie%20covidu-19%20v%20%C4%8Cesku&rvprop=timestamp%7Cuser%7Ccomment&rvlimit=max&
api = 'https://'+lang+'.wikipedia.org/w/api.php?action=query&prop=revisions&titles='+urllib.parse.quote_plus(page)+'&rvlimit=500&rvprop=timestamp%7Cuser%7Cids%7Ccomment&rvdir=newer&format=json'
api_basic = 'https://'+lang+'.wikipedia.org/w/api.php?action=query&prop=revisions&titles='+urllib.parse.quote_plus(page)+'&rvlimit=500&rvprop=timestamp%7Cuser%7Cids%7Ccomment&rvdir=newer&format=json'

ready_to_next = True
users = []
anon_users = []
logged_users = []
revision_count = 0
revision_ids = []
hours = {}
for i in range(0,24):
    hours[i] = 0

format = "%Y-%m-%dT%H:%M:%SZ"

while ready_to_next == True:
    response = requests.get(api)
    page = json.loads(response.text)
    if 'continue' in page.keys():
        ready_to_next = True
        cont = page['continue']['rvcontinue']
        append = '&rvcontinue=' + cont
    else:
        ready_to_next = False
        append = ''

    revisions = page['query']['pages']['1570967']['revisions']

    for revision in revisions:
        if revision['user'] not in users:
            users.append(revision['user'])
            if 'anon' in revision.keys():
                anon_users.append(revision['user'])
            else:
                logged_users.append(revision['user'])

        if revision['revid'] not in revision_ids:
            revision_count = revision_count + 1
        date_time_obj = datetime.strptime(revision['timestamp'], format)
        hours[date_time_obj.hour] = hours[date_time_obj.hour] + 1


    api = api_basic + append

print('pocet_uzivatelu: ' + str(len(users)))
print('pocet_anonymnich_uzivatelu: ' + str(len(anon_users)))
print('pocet_registrovanych_uzivatelu: ' + str(len(logged_users)))
print('pocet_editaci: ' + str(revision_count))
for hour in hours.keys():
    print('editovano v hodinu: ' + str(hour) + ' : ' + str(hours[hour]))


    # for key,page in page['query']['pages'].items():
    #     print(lang + ' : ' + str(len(page['revisions'])))
