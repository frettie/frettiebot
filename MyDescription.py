#!/usr/bin/python
# -*- coding: utf-8 -*-
import MyLogger
import pywikibot
import re
import sys
from string import Template
class Description:

    def define(self):
        self.name = ""
        self.inText = dict()
        self.type = dict()
        self.excludeByCategoryList = list()
        self.excludeByArticleNameList = list()
        self.excludeType = 'category'
        self.county = ""
        self.nameOfCategoryToGetCounty = ''
        self.langVersions = list(['cs', 'en'])
        self.langWord = dict()
        self.descriptions = dict()
        self.cityCategory = ""
        self.entityType = ""
        self.inCategory = list()


    def __init__(self):
        print "label start"

    def setEntityType(self, entityType):
        self.entityType = entityType
        self.errLog = MyLogger.Logger(self.getEntityType(), 'err')
        self.completeLog = MyLogger.Logger(self.getEntityType(), 'complete')

    def getEntityType(self):
        return self.entityType

    def setExcludeByCategory(self, text):
        self.excludeByCategoryList.append(text)

    def getExcludeByCategory(self):
        return self.excludeByCategoryList

    def setExcludeType(self, text):
        self.excludeType = text

    def getExcludeType(self):
        return self.excludeType

    def setExcludeByArticleName(self, text):
        self.excludeByArticleNameList.append(text)

    def getExcludeByArticleName(self):
        return self.excludeByArticleNameList

    def getName(self):
        return self.name

    def setName(self, name):
        self.name = name

    def setInText(self, inText):
        self.inText = inText

    def getInText(self, lang):
        return self.inText[lang]

    def setType(self, type):
        self.type = type

    def getType(self, lang):
        return self.type[lang]

    def setLangWord(self, lang, key, word):
        if self.langWord.has_key(lang):
            if (isinstance(word, unicode)):
                self.langWord[lang][key] = word
            else:
                self.langWord[lang][key] = unicode(word,'utf-8')
        else:
            self.langWord[lang] = dict()
            if (isinstance(word, unicode)):
                self.langWord[lang][key] = word
            else:
                self.langWord[lang][key] = unicode(word,'utf-8')

    def getLangWord(self, lang, key):
        return self.langWord[lang][key]

    def setNameOfCategoryToGetCounty(self, name):
        self.nameOfCategoryToGetCounty = name

    def setCityCategory(self, name):
        self.cityCategory = name;

    def setMustBeInCategory(self, nameList):
        self.inCategory = nameList

    def getMustBeInCategory(self, asstring=False):
        if (asstring):
            text = ''
            for incat in self.inCategory:
                text = text + ' ' + incat
            return text
        else:
            return self.inCategory

    def getInfo(self):
        try:
            site = pywikibot.Site('cs','wikipedia') #  any site will work, this is just an example
            page = pywikibot.Page(site, self.name)
            self.item = pywikibot.ItemPage.fromPage(page) #  this can be used for any page object
            self.repo = site.data_repository()  # this is a DataSite object
            return True
        except pywikibot.exceptions.NoPage:
            self.errLog.log(self.getName(), 'No Page')
            return False
        except Exception:
            self.errLog.log(self.getName(), 'Neznama err')
            return False

    def setDescriptions(self):
        for lang in self.langVersions:
            s = Template(self.getInText(lang))
            self.descriptions[lang] = s.substitute(type=self.getLangWord(lang, 'type'), county=self.county, region=self.getLangWord(lang, 'region'))


        for lang in self.langVersions:
            if (lang in self.item.descriptions.keys()):
                self.descriptions.pop(lang)

    def getDescriptions(self,lang = False):
        if (lang):
            return self.descriptions[lang]
        else:
            return self.descriptions

    def hasDescriptions(self):
        langs = list()
        langs.append(u'en')
        langs.append(u'cs')
        hasLangs = list()
        existLangs = False
        for lang in langs:
            if lang in self.item.descriptions.keys():
                print self.item.descriptions[lang]
                hasLangs.append(lang)
        if len(hasLangs) == 2:
            existLangs = True

        return existLangs


    def saveDescription(self):
        if (len(self.getDescriptions())):
            for lang in self.langVersions:
                if (lang in self.getDescriptions().keys()):
                    print lang + ': ' + self.getDescriptions(lang)
                    length = len(self.descriptions[lang])
                    if length > 249:
                        self.descriptions[lang] = self.descriptions[lang][0:249]



            self.item.editDescriptions(self.getDescriptions())


            self.completeLog.log(self.getName())
        else:
            self.completeLog.log(self.getName())
            #print "Err: jsou obsazeny oboji jazyky: " + self.getName()

    def save(self):
        try:
            self.setDescriptions()
            self.saveDescription()

        except pywikibot.exceptions.NoPage:
            self.errLog.log(self.getName(), 'No Page')
        except KeyError:
            self.errLog.log(self.getName(), 'Key Error')
        except pywikibot.data.api.APIError:
            self.errLog.log(self.getName(), 'API Error')




    def getData(self, topCategory, inText):
        site = pywikibot.Site('cs', 'wikipedia');
        articles = pywikibot.Category(site, unicode(topCategory, 'utf-8')).articles(recurse=4)


        for article in articles.__iter__():
            self.setName(article.title())

            print article.title()
            if self.completeLog.isCompleteFile(self.getName()):
               continue
            if not (self.getInfo()):
                continue
            if self.hasDescriptions():
                self.completeLog.log(self.getName())
                continue

            if (self.getExcludeType() == 'category'):
                excludeFound = self.excludeByCategory(article)
            elif (self.getExcludeType() == 'article'):
                excludeFound = self.excludeByArticleName(article)
            else:
                excludeFound = False

            self.prepareCounty(article)

            isInRightCat = self.mustBeInCat(article)

            if not(isInRightCat):
                #err, neni ve spravne kategorii!
                self.errLog.log(self.getName(), 'notChange - not in right category ' + self.getMustBeInCategory(asstring=True))
                self.completeLog.log(self.getName(), printLog=False)
                continue

            if (excludeFound):
                self.errLog.log(self.getName(), 'notChange - exclude by ' + self.getExcludeType())
                #nemenit, neni kostel - je to v excludu (tj. je to kaple)
            elif not (self.county):
                #self.errLog.log(self.getName(), 'notChange - not found county')
                self.completeLog.log(self.getName(), printLog=False)
            elif not (self.prepareText(article)):
                #self.errLog.log(self.getName(), 'notChange - not found county')
                self.completeLog.log(self.getName(), printLog=False)
            else:
                #menit
                #print cat.title()

                self.setInText(inText)
                self.save()

    def prepareText(self):
        return True

    def setCounty(self, county):
        self.county = county

    def prepareCounty(self, article):
        self.county = False
        for cat in article.categories().__iter__():
            testCounty = re.search(self.nameOfCategoryToGetCounty, cat.title().lower())
            testInPrague = re.search('Praze', cat.title())
            if (testCounty or testInPrague):
                res = re.split('okres[e|u] ', cat.title())
                if (testInPrague):
                    self.county = u"Hlavní město Praha"
                    continue
                if len(res) == 1:
                    self.county = False
                else:
                    self.setCounty(res[1])




    def excludeByCategory(self, article):
        excludeFound = False
        for cat in article.categories().__iter__():
            for excl in self.getExcludeByCategory():
                test = re.search(excl, cat.title().lower())
                if (test):
                    excludeFound = True
        return excludeFound

    def mustBeInCat(self, article):
        isInCat = False
        neco = article.categories()
        for cat in neco:
            incatlist = self.getMustBeInCategory()
            for incat in incatlist:
                test = re.search(incat, cat.title().lower())
                if (test):
                    isInCat = True
        return isInCat

    def excludeByArticleName(self, article):
        excludeFound = False

        for excl in self.getExcludeByArticleName():
            test = re.search(excl, article.title().lower())
            if (test):
                excludeFound = True
        return excludeFound

    def getRegions(self):
        regions = list()
        descriptionFormatGeneralStyle = {u'en' : u'$type in $county county of $region region', u'cs':u'$type v okrese $county v $region kraji'}
        descriptionFormatZlinskyStyle = {u'en' : u'$type in $county county of $region region', u'cs':u'$type v okrese $county ve $region kraji'}
        descriptionFormatVysocinaStyle = {u'en' : u'$type in $county county of $region region', u'cs':u'$type v okrese $county v kraji $region'}

        categoryFormatGeneralStyle = 'Seznam kulturních památek v$region kraji'
        categoryFormatZlinskyStyle = 'Seznam kulturních památek ve $region kraji'
        categoryFormatVysocinaStyle = 'Seznam kulturních památek v Kraji $region'

        #regions.append({'name':'Olomoucký', 'en': 'Olomouc', 'cs':'Olomouckém', 'categoryFormat':categoryFormatGeneralStyle, 'descriptionFormat':descriptionFormatGeneralStyle})
        #regions.append({'name':'Moravskoslezský', 'en': 'Moravian Silesian', 'cs':'Moravskoslezském', 'categoryFormat':categoryFormatGeneralStyle, 'descriptionFormat':descriptionFormatGeneralStyle})
        #regions.append({'name':'Zlínský', 'en': 'Zlín', 'cs':'Zlínském', 'categoryFormat':categoryFormatZlinskyStyle, 'descriptionFormat':descriptionFormatZlinskyStyle})
        #regions.append({'name':'Vysočina', 'en': 'Vysočina', 'cs':'Vysočina', 'categoryFormat':categoryFormatVysocinaStyle, 'descriptionFormat':descriptionFormatVysocinaStyle})
        #regions.append({'name':'Jihomoravský', 'en': 'South Moravian', 'cs':'Jihomoravském', 'categoryFormat':categoryFormatGeneralStyle, 'descriptionFormat':descriptionFormatGeneralStyle})
        #regions.append({'name':'Pardubický', 'en': 'Pardubice', 'cs':'Pardubickém', 'categoryFormat':categoryFormatGeneralStyle, 'descriptionFormat':descriptionFormatGeneralStyle})
        #regions.append({'name':'Královéhradecký', 'en': 'Hradec Králové', 'cs':'Královéhradeckém', 'categoryFormat':categoryFormatGeneralStyle, 'descriptionFormat':descriptionFormatGeneralStyle})
        regions.append({'name':'Liberecký', 'en': 'Liberec', 'cs':'Libereckém', 'categoryFormat':categoryFormatGeneralStyle, 'descriptionFormat':descriptionFormatGeneralStyle})
        #regions.append({'name':'Ústecký', 'en': 'Ústí nad Labem', 'cs':'Ústeckém', 'categoryFormat':categoryFormatGeneralStyle, 'descriptionFormat':descriptionFormatGeneralStyle})
        #regions.append({'name':'Středočeský', 'en': 'Central Bohemian', 'cs':'Středočeském', 'categoryFormat':categoryFormatZlinskyStyle, 'descriptionFormat':descriptionFormatZlinskyStyle})
        #regions.append({'name':'Plzeňský', 'en': 'Plzeň', 'cs':'Plzeňském', 'categoryFormat':categoryFormatGeneralStyle, 'descriptionFormat':descriptionFormatGeneralStyle})
        #regions.append({'name':'Jihočeský', 'en': 'South Bohemian', 'cs':'Jihočeském', 'categoryFormat':categoryFormatGeneralStyle, 'descriptionFormat':descriptionFormatGeneralStyle})
        #regions.append({'name':'Karlovarský', 'en': 'Karlovy Vary', 'cs':'Karlovarském', 'categoryFormat':categoryFormatGeneralStyle, 'descriptionFormat':descriptionFormatGeneralStyle})

        return regions

    def runAll(self):
        regions = self.getRegions()
        for region in regions:
            s = Template(region['categoryFormat'])
            category = s.substitute(region=region['cs'])
            self.setLangWord('cs', 'region', region['cs'])
            self.setLangWord('en', 'region', region['en'])
            #print category
            self.getData(category, region['descriptionFormat'])

