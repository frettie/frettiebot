#!/usr/bin/python
# -*- coding: utf-8 -*-
import csv

csfds = set()

with open('csfd.csv', 'rt') as f:
    reader = csv.DictReader(f, delimiter=';')
    for r in reader:
        csfds.add(r['csfd'])

with open('csfd_ben.csv', 'w') as f2:

    for i in range(1,436480):
        if (not str(i) in csfds):
            f2.write(str(i) + '\n')
