#!/usr/bin/python
# -*- coding: utf-8 -*-
import pywikibot
import sys
import re



class ArticleCountTable():

	def __init__(self):
		self.site = pywikibot.Site('cs', 'wikipedia');
		self.categories = ['Články k wikifikaci', 'Články k úpravě', 'Údržba:Články s vyhýbavými slovy', 'Údržba:Články k rozdělení','Údržba:Aktualizovat','Údržba:Články reagující na aktuální události','Údržba:Fakta k ověření','Články k jazykové úpravě','Články ke kategorizaci','Údržba:Články ke sloučení','Údržba:Články obsahující nedoložená tvrzení']
		self.tableLines = {}
		self.tableLines['nums'] = [u'1',u'2',u'3',u'4',u'5',u'6',u'7',u'8',u'9',u'0']
		self.tableLines['letters'] = [u'a', u'á',u'b',u'c',u'č',u'd',u'đ',u'e',u'ě',u'é',u'f',u'g',u'h',u'ch',u'i',u'í',u'j',u'k',u'l',u'm',u'n',u'ň',u'o',u'ó',u'p',u'q',u'r',u'ř',u's',u'š',u't',u'ť',u'u',u'ú',u'ů',u'ū',u'v',u'w',u'x',u'y',u'z',u'ž']
		self.tableLines['lettersExtra'] = [u'ç',u'ď',u'į',u'ӄ',u'ł', u'ö', u'ü',u'ş',u'ľ']
		self.tableLines['symbols'] = [u'.',u'¡',u'-']
		
	def run(self):
		for category in self.categories:
			self.process(category)
		
	def createTable(self, repository):
		table = u"""{| class="wikitable"
		|+ Počet článků v kategorii dle písmen v abecedě
		"""
		longestLine = len(self.tableLines['letters'])
		
		for k,lines in self.tableLines.items():
			if (k == 'letters'):
				
				table = table + u"!"
				countLetters = 0
				for letter in lines:
					if (countLetters == 0):
						table = table + letter.upper()
					else:
						table = table + u" !! " + letter.upper()
						
					countLetters = countLetters + 1
				if (countLetters < longestLine):
					for x in range(0, longestLine - countLetters):
						table = table + u" !! "
					
				table = table + u"""
				|-
				"""
				table = table + u"|"
				countLetters = 0
				for letter in lines:
					if (countLetters == 0):
						table = table + str(repository[letter])
					else:
						table = table + " || " + str(repository[letter])
						
					countLetters = countLetters + 1
					
				if (countLetters < longestLine):
					for x in range(0, longestLine - countLetters):
						table = table + u" || "
				
				table = table + u"""
				|-
				"""		
			
		table = table + u"""
		|}"""
		
		return table
		
	def countArticles(self, articles, repository):
		for k,lines in self.tableLines.items():
			for pismeno in lines:
				repository[pismeno] = 0

		for article in articles:
			#print article.title()[:1]
			if (article.title()[:1].lower() == u"c"):
				if (article.title()[:2].lower() == u"ch"):
					repository[article.title()[:2].lower()] = repository[article.title()[:2].lower()] + 1
				else:
					repository[article.title()[:1].lower()] = repository[article.title()[:1].lower()] + 1
			else:
				repository[article.title()[:1].lower()] = repository[article.title()[:1].lower()] + 1
				
		return repository
	
	def process(self, cat):
		category = pywikibot.Category(self.site, unicode(cat, 'utf-8'))
		articles = category.articles()

		repository = {}
		
		repository = self.countArticles(articles, repository)

		table = self.createTable(repository);

		

		#print cat.get()

		leng = len(category.get())

		vys = re.sub('\{\|(.\n*)\|\}', table, category.get(), re.MULTILINE)
		for m in re.finditer("\{\|", category.get()):
			tableStart = m.start()
	
		for m in re.finditer("\|\}", category.get()):
			tableEnd = m.end()

		try:
			vyslednyText = vys[:tableStart]
			vyslednyText = vyslednyText + table
			vyslednyText = vyslednyText + vys[tableEnd:]
		except (UnboundLocalError):
			vyslednyText = vys[:leng]
			vyslednyText = vyslednyText + table			
		
		print vyslednyText
		
	
		category.put(vyslednyText)

runner = ArticleCountTable()
runner.run()