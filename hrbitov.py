import pywikibot
import urllib.request
import csv
import json

def my_range(start, end, step):
    while start <= end:
        yield start
        start += step

def merge_two_dicts(x, y):
    z = x.copy()   # start with x's keys and values
    z.update(y)    # modifies z with y's keys and values & returns None
    return z

class hrbitov():
    def loadData(self):
        for x in my_range(0, 5000, 40):
            self.getFile(x)

    def getFile(self, num):
        file = 'http://gis.trebic.cz/services/mapserver/local/zidovske_mesto/gservice?typeName=pomniky-b&service=WFS&request=GetFeature&outputFormat=json&version=1.1.0&srsName=EPSG:4326&cql_filter=cislo>='+str(num)
        webfile = urllib.request.urlopen(file)
        jsondata = webfile.read()
        parsed_json = json.loads(jsondata)
        print('dfs')

        csv_file_path = 'zidovsky_hrbitov' + ".csv"
        processed_data = []
        header = []
        first = True
        for hrob in parsed_json['features']:


            attr = hrob['properties']
            attr['memotext'] = attr['memotext'].replace('\n', ' ').replace('\r', '')
            attr['memopoz'] = attr['memopoz'].replace('\n', ' ').replace('\r', '')

            geodict = {}
            geodict['x'] = hrob['geometry']['coordinates'][0]
            geodict['y'] = hrob['geometry']['coordinates'][1]
            geo = geodict
            reduced_item = merge_two_dicts(attr, geo)

            # if first:
            header += reduced_item.keys()
            # first = False

            processed_data.append(reduced_item)

        header = list(set(header))
        header.sort()

        with open(csv_file_path, 'a+') as f:
            writer = csv.DictWriter(f, header, quoting=csv.QUOTE_ALL)
            writer.writeheader()
            for row in processed_data:
                writer.writerow(row)

        print("Just completed writing csv file with %d columns" % len(header))


hrbitov = hrbitov()
hrbitov.loadData()