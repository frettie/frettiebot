import math
import re
from datetime import datetime
from typing import Dict, List, NoReturn
from typing import Union

import geojson
import csv
import geopy.distance
import requests
import untangle as untangle
from shapely.geometry import Point, Polygon

import pywikibot.data.sparql
import pywikibot

from logger import Logger

DISTANCE = 10
POLYGON_COUNT = 50

class pair:
    longitude_min: float = 12.08477
    longitude_max: float = 18.86321

    latitude_min: float = 48.54292
    latitude_max: float = 51.06426

    longitude_diff: float = 0.0
    latitude_diff: float = 0.0

    polygons: Dict[int, Polygon] = {}

    from_wd: List[Dict] = []
    from_wd_by_polygon: Dict[int, list] = {}
    from_wd_list: List[str] = []

    external: List[Dict] = []
    external_by_polygon: Dict[int, list] = {}
    external_list: List[str] = []

    wikidata_container: List[Dict] = []

    found_count: int = 0

    csv_lines: List[Dict] = []

    distance: Union[int, str] = DISTANCE

    sparql_to_create_wikidata_csv_file = ''''''

    wikidata_csv_external_id_field_name: str = 'sochy_a_mesta_id'
    external_csv_external_id_field_name: str = 'sochy_a_mesta_id'
    external_id_property: str = 'P8521'

    wikidata_file_map: dict = {}

    external_data_map: dict = {}

    final_field_map: List[str] = []

    match_map: dict = {}

    wikidata_file_name: str = ''
    external_file_name: str = ''
    final_file_name: str = ''

    connected: List[str] = []

    def __init__(self) -> NoReturn:
        print('run')

    def create_wikidata_file(self):
        if (self.sparql_to_create_wikidata_csv_file == ''):
            raise ValueError('there is not sparql query')
        if (self.wikidata_csv_external_id_field_name == ''):
            raise ValueError('there is not wikidata external identificator field')
        if (self.wikidata_file_name == ''):
            raise ValueError('there is not wikidata file name')

        self.run_sparqls()
        self.print_csv_from_wd()

    def pair_data(self):
        if (self.longitude_min == ''):
            raise ValueError('there is not longitude min')
        if (self.latitude_min == ''):
            raise ValueError('there is not latitude min')
        if (self.longitude_max == ''):
            raise ValueError('there is not longitude max')
        if (self.latitude_max == ''):
            raise ValueError('there is not latitude max')
        if (len(self.wikidata_file_map) == 0):
            raise ValueError('there is not wikidata file mapping')
        if (len(self.external_data_map) == 0):
            raise ValueError('there is not external file mapping')
        if (self.wikidata_file_name == ''):
            raise ValueError('there is not wikidata file name')
        if (self.external_file_name == ''):
            raise ValueError('there is not external file name')
        if (len(self.match_map) == 0):
            raise ValueError('there is not match map')
        if (len(self.final_field_map) == 0):
            raise ValueError('there is not final field map')
        if (self.wikidata_csv_external_id_field_name == ''):
            raise ValueError('there is not wikidata csv external id field name')
        if (self.external_csv_external_id_field_name == ''):
            raise ValueError('there is not external csv external id field name')
        if (self.final_file_name == ''):
            raise ValueError('there is not final csv file name')
        self.count_width_height_diffs()
        self.create_polygons()
        self.count_wikidata_items()
        self.count_external_data()
        self.find_correct_values()
        self.print_csv()

    def count_width_height_diffs(self) -> NoReturn:
        '''Count height and width of polygons'''
        coords_1 = (self.longitude_min, self.latitude_min)
        coords_2 = (self.longitude_max, self.latitude_min)

        coords_3 = (self.longitude_min, self.latitude_min)
        coords_4 = (self.longitude_min, self.latitude_max)

        width = geopy.distance.vincenty(coords_1, coords_2).km
        height = geopy.distance.vincenty(coords_3, coords_4).km

        self.longitude_diff = (self.longitude_max - self.longitude_min) / math.ceil(width / POLYGON_COUNT)
        self.latitude_diff = (self.latitude_max - self.latitude_min) / math.ceil(height / POLYGON_COUNT)

    def create_polygon_from_coords(self, value: str, start_lat: str, end_lat: str) -> Polygon:
        '''
        Create shapely polygon from coords

        --------
        |       |
        |       |
        |       |
        |       |
        --------

        value : str
            Longitude coords splitted by :
        start_lat : str
            Latitude coord
        end_lat : str
            Latitude max coord
        '''
        spl = value.split(":")
        start_lon = spl[0]
        end_lon = spl[1]
        # print(start_lat,end_lat,start_lon,end_lon)
        coords = [
            (float(start_lon), float(start_lat)),
            (float(start_lon), float(end_lat)),
            (float(end_lon), float(end_lat)),
            (float(end_lon), float(start_lat)),
            (float(start_lon), float(start_lat)),
        ]
        poly = Polygon(coords)
        return poly

    def create_polygons(self) -> Dict[int, Polygon]:
        '''
        Create all polygons by longitude sequences and afterthat latitude sequence
        Method create polygon between start_latitude and end_latitude and start_longitude and end_longitude coords
        '''
        lons = {}
        lon_start = self.longitude_min
        lon_count = 0

        while lon_start < self.longitude_max:
            lon_count = lon_count + 1
            start = lon_start
            end = lon_start + self.longitude_diff
            # print(str(start) + ':' + str(end))
            lon_start = lon_start + self.longitude_diff
            lons[lon_count] = str(start) + ':' + str(end)

        lats = {}
        lat_start = self.latitude_min
        lat_count = 0

        while lat_start < self.latitude_max:
            lat_count = lat_count + 1
            start = lat_start
            end = lat_start + self.latitude_diff
            # print(str(start) + ':' + str(end))
            start_lat = start
            end_lat = end
            for k, v in lons.items():
                poly = self.create_polygon_from_coords(v, start_lat, end_lat)
                self.polygons[len(self.polygons) + 1] = poly
            lat_start = lat_start + self.latitude_diff
            lats[lat_count] = lat_start
            # print(num)
        # print(math.ceil(height))
        # print(lons)
        # print(lats)
        # print(polygons)
        return self.polygons

    def print_csv_from_wd(self):
        with open(self.wikidata_file_name, 'w', newline='') as csvfile:
            writer = csv.writer(csvfile, delimiter=";", quotechar="'", quoting=csv.QUOTE_ALL)
            writer.writerow(
                ['qid'] + ['lat'] + ['lon'] +
                ['name'] + ['instance'] + ['instance_name'] +
                ['place_name'] + [self.wikidata_csv_external_id_field_name]
            )
            for line in self.wikidata_container:
                writer.writerow(
                    [line['qid']] + [line['lat']] + [line['lon']] +
                    [line['name']] + [line['instance']] + [line['instance_name']] +
                    [line['place_name']] + [line[self.wikidata_csv_external_id_field_name]]
                )

    def run_sparqls(self):
        # print(self.sparql_to_create_wikidata_csv_file)
        sq = pywikibot.data.sparql.SparqlQuery()
        queryresult = sq.select(self.sparql_to_create_wikidata_csv_file)
        for resultitem in queryresult:
            qid = resultitem.get('item').replace(u'http://www.wikidata.org/entity/', u'')
            instance = resultitem.get('P31').replace(u'http://www.wikidata.org/entity/', u'')
            row = {}
            row['qid'] = qid
            row['lat'] = resultitem.get('lat')
            row['lon'] = resultitem.get('lon')
            row['name'] = resultitem.get('item_label')
            row['instance'] = instance
            row['instance_name'] = resultitem.get('P31_label_cs')
            row['place_name'] = resultitem.get('P131_label_cs')
            row[self.wikidata_csv_external_id_field_name] = resultitem.get(self.external_id_property)
            self.wikidata_container.append(row)
        self.print_csv_from_wd()

    def fill_wikidata(self, row: list) -> dict:
        '''Fill wikidata data to polygons'''
        data = {}

        # 'qid';'lat';'lon';'name';'instance';'instance_name';'place_name';'drobne_pamatky_id'
        for key in self.wikidata_file_map.keys():
            data[key] = row[self.wikidata_file_map[key]]

        if 'latitude' not in self.wikidata_file_map.keys():
            raise IndexError('latitude is required')

        if 'longitude' not in self.wikidata_file_map.keys():
            raise IndexError('longitude is required')

        if 'qid' not in self.wikidata_file_map.keys():
            raise IndexError('item (qid) is required')

        if (data['latitude'] == '' or data['longitude'] == ''):
            raise IndexError('lat or lon empty')

        point = Point(float(data['longitude']), float(data['latitude']))
        data['polygon'] = 0
        polygon_found = False
        for k, pol in self.polygons.items():
            if polygon_found:
                break
            if (point.within(pol)):
                data['polygon'] = k
                polygon_found = True
        return data

    def count_wikidata_items(self) -> NoReturn:
        '''Read and parse wikidata'''
        fpwd = open(self.wikidata_file_name)
        spamreader = csv.reader(fpwd, delimiter=";", quotechar="'")
        count = 0

        for row in spamreader:
            if count == 0:
                count = count + 1
                continue
            try:
                data = self.fill_wikidata(row)
                if (data[self.wikidata_csv_external_id_field_name]):
                    self.connected.append(data[self.wikidata_csv_external_id_field_name])
                else:
                    self.from_wd.append(data)
                    if data['polygon'] in self.from_wd_by_polygon:
                        self.from_wd_by_polygon[data['polygon']].append(data)
                    else:
                        self.from_wd_by_polygon[data['polygon']] = []
                        self.from_wd_by_polygon[data['polygon']].append(data)
            except IndexError as e:
                pass

    def fill_external_data(self, row: list) -> dict:
        '''Fill external data to polygons'''
        data = {}
        # nazev;cislo_zaznamu;sirka;delka

        if 'latitude' not in self.external_data_map.keys():
            raise IndexError('latitude is required')

        if 'longitude' not in self.external_data_map.keys():
            raise IndexError('longitude is required')

        if self.external_csv_external_id_field_name not in self.external_data_map.keys():
            raise IndexError('external identificator is required')

        for key in self.external_data_map.keys():
            data[key] = row[self.external_data_map[key]]

        point = Point(float(data['longitude']), float(data['latitude']))
        data['polygon'] = 0
        polygon_found = False
        for k, pol in self.polygons.items():
            if polygon_found:
                break
            if (point.within(pol)):
                data['polygon'] = k
                polygon_found = True
        return data

    def count_external_data(self) -> NoReturn:
        '''Read and parse external data'''
        fp = open(self.external_file_name)
        spamreader = csv.reader(fp, delimiter=";", quotechar="'")
        count = 0

        for row in spamreader:
            if count == 0:
                count = count + 1
                continue
            try:
                data = self.fill_external_data(row)
                if data[self.external_csv_external_id_field_name] not in self.connected:
                    self.external.append(data)
                    if data['polygon'] in self.external_by_polygon:
                        self.external_by_polygon[data['polygon']].append(data)
                    else:
                        self.external_by_polygon[data['polygon']] = []
                        self.external_by_polygon[data['polygon']].append(data)
            except IndexError as e:
                pass

    def print_csv(self) -> NoReturn:
        '''Print mixed CSV result'''
        if self.distance == 0:
            self.distance = 'text_pair'
        with open(self.final_file_name + '_found_distance_' + str(self.distance) + '.csv', 'w', newline='') as csvfile:
            writer = csv.writer(csvfile, delimiter=";", quotechar="'", quoting=csv.QUOTE_ALL)

            writer.writerow(self.final_field_map)

            for line in self.csv_lines:
                row_to_write = []
                for field in self.final_field_map:
                    row_to_write.append(line[field])
                writer.writerow(row_to_write)

    def pair_by_name(self) -> NoReturn:
        '''Pair external and WD items by name'''
        self.list_from_dict(self.from_wd, self.from_wd_list, 'name')
        self.list_from_dict(self.external, self.external_list, 'name')
        maybe_pair = []
        from_wd_maybe = []
        external_maybe = []
        for wd in self.from_wd_list:
            if wd in self.external_list:
                maybe_pair.append(wd)
        for row in self.from_wd:
            if row['name'] in maybe_pair:
                from_wd_maybe.append(row)
        for row in self.external:
            if row['name'] in maybe_pair:
                external_maybe.append(row)

        for wd in from_wd_maybe:
            name = wd['name']
            for external in external_maybe:
                if name == external['name']:
                    coords_orig = (external['lat'], external['lon'])
                    coords_2 = (wd['lat'], wd['lon'])
                    distance = geopy.distance.vincenty(coords_orig, coords_2).m
                    if (distance < 15000):
                        line = {
                            'name_wd': wd['name'],
                            'name_dibavod': external['name'],
                            'distance': distance,
                            'qid': wd['qid'],
                            'polygon': 0,
                            'okres': external['region'],
                            'okres_qid': external['region_qid'],
                            'dibavod_id': external['identifikator'],
                            'osm_id': external['osm_id'],
                            'plocha': external['area'],
                            'nadm_vyska': external['nadmorska_vyska'],
                            'hloubka': external['hloubka'],
                        }
                        self.csv_lines.append(line)
                        self.found_count = self.found_count + 1
        pass

    def find_correct_values(self) -> NoReturn:
        '''search correct data'''
        for key in self.external_by_polygon.keys():
            for external in self.external_by_polygon[key]:
                coords_orig = (external.get('latitude'), external.get('longitude'))
                found = False
                try:
                    for wd in self.from_wd_by_polygon[key]:
                        coords_2 = (wd.get('latitude'), wd.get('longitude'))

                        if (geopy.distance.vincenty(coords_orig, coords_2).m < self.distance):

                            line = {}
                            for match_key in self.match_map.keys():
                                if (self.match_map[match_key]['class'] == 'wd'):
                                    line[match_key] = wd.get(self.match_map[match_key]['field'])
                                elif (self.match_map[match_key]['class'] == 'external'):
                                    line[match_key] = external.get(self.match_map[match_key]['field'])

                            line['distance'] = geopy.distance.vincenty(coords_orig, coords_2).m
                            line['polygon'] = key
                            line['external_' + self.external_csv_external_id_field_name] = external.get(
                                self.external_csv_external_id_field_name)
                            line['wd_' + self.wikidata_csv_external_id_field_name] = wd.get(
                                self.wikidata_csv_external_id_field_name)
                            self.csv_lines.append(line)
                            found = True

                    if found:
                        # print(water)
                        self.found_count = self.found_count + 1
                except KeyError as e:
                    pass

    def list_from_dict(self, start_list: list, final_list: list, key: str) -> NoReturn:
        '''Create list from dict'''
        for row in start_list:
            final_list.append(row.get(key))


class vodni_plochy(pair):
    '''Pair CZ ponds to WD ponds'''

    def __init__(self) -> NoReturn:
        super().__init__()


    def fill_wikidata(self, row: list) -> Dict[str,str]:
        data = {}
        data['qid'] = row[0]
        data['name'] = row[1]
        data['lon'] = row[2]
        data['lat'] = row[3]
        point = Point(float(data['lon']), float(data['lat']))
        data['polygon'] = 0
        for k, pol in self.polygons.items():
            if (point.within(pol)):
                data['polygon'] = k
        return data

    def count_wikidata_items(self) -> NoReturn:
        fpwd = open('vodni_plochy_other_wd.csv')
        spamreader = csv.reader(fpwd, delimiter=',', quotechar='"')
        count = 0

        for row in spamreader:
            if count == 0:
                count = count + 1
                continue
            try:
                data = self.fill_wikidata(row)
                self.from_wd.append(data)
                if data['polygon'] in self.from_wd_by_polygon:
                    self.from_wd_by_polygon[data['polygon']].append(data)
                else:
                    self.from_wd_by_polygon[data['polygon']] = []
                    self.from_wd_by_polygon[data['polygon']].append(data)
            except IndexError as e:
                pass

    def fill_external_data(self, row: Dict) -> Dict[str, str]:
        arr = {}
        arr['osm_id'] = row['OSM_ID']
        arr['name'] = row['NAME']
        arr['area'] = row['PLOCHA_M2']
        arr['region'] = row['REGION']
        arr['region_qid'] = row['REGION_WIK']
        arr['identifikator'] = row['NADR_GID']
        if (row['KOTA_HLADI']):
            arr['nadmorska_vyska'] = row['KOTA_HLADI']
        else:
            arr['nadmorska_vyska'] = 0

        if (row['HLOUBKA']):
            arr['hloubka'] = row['HLOUBKA']
        else:
            arr['hloubka'] = 0
        stred = geojson.Point([row['XCOORD'], row['YCOORD']])
        arr['stred'] = stred
        arr['lon'] = row['XCOORD']
        arr['lat'] = row['YCOORD']

        point = Point(float(arr['lon']), float(arr['lat']))
        arr['polygon'] = 0
        for k, pol in self.polygons.items():
            if (point.within(pol)):
                arr['polygon'] = k
        return arr

    def count_external_data(self) -> NoReturn:
        fp = open('final-water.geojson.txt')
        all = geojson.load(fp)

        for line in all.features:
            props = line.properties
            arr = self.fill_external_data(props)
            # if arr['area'] is None:
            #     arr['area'] = 0
            # if (int(arr['area']) > 100000):
            self.external.append(arr)

            if arr['polygon'] in self.external_by_polygon:
                self.external_by_polygon[arr['polygon']].append(arr)
            else:
                self.external_by_polygon[arr['polygon']] = []
                self.external_by_polygon[arr['polygon']].append(arr)

    def find_correct_values(self) -> NoReturn:
        '''Search by distance only'''
        for key in self.external_by_polygon.keys():
            for external in self.external_by_polygon[key]:
                coords_orig = (external['lat'], external['lon'])
                found = False
                try:
                    for wd in self.from_wd_by_polygon[key]:
                        coords_2 = (wd['lat'], wd['lon'])

                        if (geopy.distance.vincenty(coords_orig, coords_2).m < self.distance):
                            line = {
                                'name_wd' : wd['name'],
                                'name_dibavod' : external['name'],
                                'distance' : geopy.distance.vincenty(coords_orig, coords_2).m,
                                'qid' : wd['qid'],
                                'polygon' : key,
                                'okres' : external['region'],
                                'okres_qid' : external['region_qid'],
                                'dibavod_id' : external['identifikator'],
                                'osm_id' : external['osm_id'],
                                'plocha' : external['area'],
                                'nadm_vyska' : external['nadmorska_vyska'],
                                'hloubka' : external['hloubka'],
                            }
                            self.csv_lines.append(line)
                            # print(wd)
                            # print(water)
                            # print(geopy.distance.vincenty(coords_orig, coords_2).m)
                            found = True

                    if found:
                        # print(water)
                        self.found_count = self.found_count + 1
                except KeyError as e:
                    pass

    def print_csv(self) -> NoReturn:
        if self.distance == 0:
            self.distance = 'text_pair'
        with open('vodni_plochy_found_distance_' + str(self.distance) + '.csv', 'w', newline='') as csvfile:
            writer = csv.writer(csvfile, delimiter=";", quotechar="'", quoting=csv.QUOTE_ALL)
            writer.writerow(
                ['name_wd'] + ['name_dibavod'] + ['distance'] +
                ['qid'] + ['okres'] + ['okres_qid'] +
                ['dibavod_id'] + ['osm_id'] + ['polygon'] +
                ['plocha_m2'] + ['nadm_vyska'] + ['hloubka']
            )
            for line in self.csv_lines:
                writer.writerow(
                    [line['name_wd']] + [line['name_dibavod']] + [line['distance']] +
                    [line['qid']] + [line['okres']] + [line['okres_qid']] +
                    [line['dibavod_id']] + [line['osm_id']] + [line['polygon']] +
                    [line['plocha']] + [line['nadm_vyska']] + [line['hloubka']]
                )

class sochy_a_mesta(pair):

    sparql_to_create_wikidata_csv_file = '''
        select distinct ?item (SAMPLE(?item_label) as ?item_label) (SAMPLE(?coord) as ?coord) (SAMPLE(?lat) as ?lat) (SAMPLE(?lon) as ?lon) (SAMPLE(?P31) as ?P31) (SAMPLE(?P31_label_cs) as ?P31_label_cs) (SAMPLE(?P131_label_cs) as ?P131_label_cs)  (SAMPLE(?P8521) as ?P8521) where {

  {?item wdt:P31/wdt:P279* wd:Q1746392 .} union {?item wdt:P31 wd:Q4989906 .}
  ?item wdt:P17 wd:Q213 .
  ?item wdt:P625 ?coord .
  SERVICE wikibase:label { bd:serviceParam wikibase:language "cs,en". }
  minus {?item wdt:P8521 [] .}
  minus {?item wdt:P180/wdt:P411 [] .}
  minus {?item wdt:P31/wdt:P279* wd:Q24398318 .}
  minus {?item wdt:P31 wd:Q3395121 .}
  minus {?item wdt:P31 wd:Q1640496 .}
  minus {?item wdt:P31 wd:Q17156398 .}
  minus {?item wdt:P31 wd:Q12029081 .}
  minus {?item wdt:P31/wdt:P279* wd:Q173387 .}
  BIND(geof:latitude(?coord) AS ?lat). 
  BIND(geof:longitude(?coord) AS ?lon).
  OPTIONAL { ?item wdt:P31 ?P31 } 
    OPTIONAL { ?item rdfs:label ?item_label. FILTER(LANG(?item_label)='cs') } 
  OPTIONAL { ?item wdt:P31 ?P31. ?P31 rdfs:label ?P31_label_cs. FILTER(LANG(?P31_label_cs)='cs') } 
  OPTIONAL { ?item wdt:P131 ?P131. ?P131 rdfs:label ?P131_label_cs. FILTER(LANG(?P131_label_cs)='cs') } 
  OPTIONAL { ?item wdt:P8521 ?P8521 }  
  
} group by ?item
        '''

    wikidata_csv_external_id_field_name: str = 'sochy_a_mesta_id'
    external_csv_external_id_field_name: str = 'sochy_a_mesta_id'
    external_id_property: str = 'P8521'

    wikidata_file_map: dict = {
        'qid': 0,
        'name': 3,
        'longitude': 2,
        'latitude': 1,
        'instance_qid': 4,
        'instance_name': 5,
        'place_name': 6,
        'sochy_a_mesta_id': 7
    }

    external_data_map: dict = {
        'name': 0,
        'longitude': 3,
        'latitude': 2,
        'sochy_a_mesta_id': 1
    }

    match_map: dict = {
        'name_wd': {'class': 'wd', 'field': 'name'},
        'name_external': {'class': 'external', 'field': 'name'},
        'qid': {'class': 'wd', 'field': 'qid'},
        'wd_instance_qid': {'class': 'wd', 'field': 'instance_qid'},
        'wd_place_name': {'class': 'wd', 'field': 'place_name'},
    }

    wikidata_file_name: str = 'sochy_a_mesta_from_wd.csv'
    external_file_name: str = 'sochyamesta-csv.csv'
    final_file_name: str = 'sochy_a_mesta'

    final_field_map = [
        'name_wd', 'name_external', 'distance', 'qid', 'external_' + external_csv_external_id_field_name,
                                                       'wd_' + wikidata_csv_external_id_field_name,
        'polygon', 'wd_instance_qid',
        'wd_place_name'
    ]


class drobne_pamatky(pair):

    drobne_pamatky_container: List[Dict] = []
    connected: List[str] = []
    def create_sparql(self, polygon):
        bounds = polygon.bounds
        start_lon = bounds[0]
        end_lon = bounds[2]
        start_lat = bounds[1]
        end_lat = bounds[3]
        query = '''
        SELECT ?item (SAMPLE(?lat) AS ?lat) (SAMPLE(?lon) AS ?lon) (SAMPLE(?label_cs) AS ?label_cs) (SAMPLE(?P31) AS ?P31) (SAMPLE(?P31_label_cs) AS ?P31_label_cs) (SAMPLE(?P131_label_cs) AS ?P131_label_cs) (SAMPLE(?P6736) AS ?P6736) 
WHERE 
{ 
  BIND(geof:latitude(?coords) AS ?lat). 
  BIND(geof:longitude(?coords) AS ?lon). 
  { BIND('8953_5613' AS ?tileName)  
   { 
     { VALUES ?cls 
              { 
                wd:Q108325 wd:Q10861631 wd:Q11734477 wd:Q11741382 wd:Q12029081 wd:Q12045520 wd:Q14552192 wd:Q1516537 wd:Q1549521 wd:Q1640496 wd:Q17489160 wd:Q179700 wd:Q193475 wd:Q1953071 wd:Q200334 wd:Q2309609 wd:Q245117 wd:Q26789694 wd:Q3328263 wd:Q3395121 wd:Q376 wd:Q38395546 wd:Q38411643 wd:Q478798 wd:Q4817 wd:Q4989906 wd:Q5003624 wd:Q575759 wd:Q721747 wd:Q838948 wd:Q860861 wd:Q921099 wd:Q961082 
              }. 
      ?item wdt:P31 ?cls 
     } UNION 
     { ?item wdt:P6736 [] } 
   } 
   SERVICE wikibase:box { ?item wdt:P625 ?coords. bd:serviceParam wikibase:cornerWest "Point(''' + str(start_lon) + ''',''' + str(start_lat) + ''')"^^geo:wktLiteral. bd:serviceParam wikibase:cornerEast "Point(''' + str(end_lon) + ''',''' + str(end_lat) + ''')"^^geo:wktLiteral. }
   OPTIONAL { ?item rdfs:label ?label_cs. FILTER(LANG(?label_cs)='cs') } OPTIONAL { ?item wdt:P31 ?P31 } OPTIONAL { ?item wdt:P31 ?P31. ?P31 rdfs:label ?P31_label_cs. FILTER(LANG(?P31_label_cs)='cs') } OPTIONAL { ?item wdt:P131 ?P131. ?P131 rdfs:label ?P131_label_cs. FILTER(LANG(?P131_label_cs)='cs') } OPTIONAL { ?item wdt:P6736 ?P6736 }  } } GROUP BY ?item ?tileName
        
        '''
        print(query)
        sq = pywikibot.data.sparql.SparqlQuery()
        queryresult = sq.select(query)
        for resultitem in queryresult:
            qid = resultitem.get('item').replace(u'http://www.wikidata.org/entity/', u'')
            instance = resultitem.get('P31').replace(u'http://www.wikidata.org/entity/', u'')
            row = {}
            row['qid'] = qid
            row['lat'] = resultitem.get('lat')
            row['lon'] = resultitem.get('lon')
            row['name'] = resultitem.get('label_cs')
            row['instance'] = instance
            row['instance_name'] = resultitem.get('P31_label_cs')
            row['place_name'] = resultitem.get('P131_label_cs')
            row['drobne_pamatky_id'] = resultitem.get('P6736')
            self.drobne_pamatky_container.append(row)
            # if resultitem.get('artukid'):
            #     result[qid] = resultitem.get('id')
        self.print_csv_from_wd()

    def run_sparqls(self):
        for key,polygon in self.polygons.items():
            self.create_sparql(polygon)

    def print_csv_from_wd(self):
        with open('drobne_pamatky_from_wd.csv', 'w', newline='') as csvfile:
            writer = csv.writer(csvfile, delimiter=";", quotechar="'", quoting=csv.QUOTE_ALL)
            writer.writerow(
                ['qid'] + ['lat'] + ['lon'] +
                ['name'] + ['instance'] + ['instance_name'] +
                ['place_name'] + ['drobne_pamatky_id']
            )
            for line in self.drobne_pamatky_container:
                writer.writerow(
                    [line['qid']] + [line['lat']] + [line['lon']] +
                    [line['name']] + [line['instance']] + [line['instance_name']] +
                    [line['place_name']] + [line['drobne_pamatky_id']]
                )

    def fill_wikidata(self, row: list) -> dict:
        data = {}
        #'qid';'lat';'lon';'name';'instance';'instance_name';'place_name';'drobne_pamatky_id'
        data['qid'] = row[0]
        data['name'] = row[3]
        data['lon'] = row[2]
        data['lat'] = row[1]
        data['instance_qid'] = row[4]
        data['instance_name'] = row[5]
        data['place_name'] = row[6]
        data['drobne_pamatky_id'] = row[7]
        point = Point(float(data['lon']), float(data['lat']))
        data['polygon'] = 0
        polygon_found = False
        for k, pol in self.polygons.items():
            if polygon_found:
                break
            if (point.within(pol)):
                data['polygon'] = k
                polygon_found = True
        return data

    def count_wikidata_items(self) -> NoReturn:
        fpwd = open('drobne_pamatky_from_wd.csv')
        spamreader = csv.reader(fpwd, delimiter=";", quotechar="'")
        count = 0

        for row in spamreader:
            if count == 0:
                count = count + 1
                continue
            try:
                data = self.fill_wikidata(row)
                if (data['drobne_pamatky_id']):
                    self.connected.append(data['drobne_pamatky_id'])
                else:
                    self.from_wd.append(data)
                    if data['polygon'] in self.from_wd_by_polygon:
                        self.from_wd_by_polygon[data['polygon']].append(data)
                    else:
                        self.from_wd_by_polygon[data['polygon']] = []
                        self.from_wd_by_polygon[data['polygon']].append(data)
            except IndexError as e:
                pass

    def translate_drobne_pamatky_druh_to_instance_qid(self, drobneType: str) -> Union[None, str]:
        types = {
            'Altán': 'Q961082',
            'Boží muka': 'Q3395121',
            'Dopravní památka': 'Q5003624',
            'Hodiny': 'Q376',
            'Hraniční kámen': 'Q921099',
            'Kaple': 'Q108325',
            'Kaplička': 'Q14552192',
            'Kašna': 'Q12029081',
            'Krajinné umění': None,
            'Křížový kámen': 'Q38411643',
            'Menhir': 'Q193475',
            'Něco jiného': None,
            'Neznámý': None,
            'Obrázek': 'Q478798',
            'Památná dlažba': 'Q3328263',
            'Památník': 'Q5003624',
            'Památný kámen': 'Q11734477',
            'Pamětní deska': 'Q721747',
            'Pamětní kříž': 'Q2309609',
            'Plastika': 'Q12045520',
            'Pomník': 'Q4989906',
            'Pomník padlým': 'Q575759',
            'Reliéf': 'Q245117',
            'Sloup': 'Q4817',
            'Smírčí kříž': 'Q1640496',
            'Socha': 'Q179700',
            'Technická památka': 'Q1516537',
            'Zvonice': 'Q200334',
            'Zvonička': 'Q10861631',
        }
        qid = types.get(drobneType, None)
        return qid

    def fill_external_data(self, row: list) -> dict:
        data = {}
        # name;Druh památky;latitude;longitude;Okres;Obec; řazení;Katastrální území;Datum;URL
        data['name'] = row[0]
        data['lon'] = row[3]
        data['lat'] = row[2]
        data['instance_qid'] = self.translate_drobne_pamatky_druh_to_instance_qid(row[1])
        data['instance_name'] = row[1]
        data['place_name'] = row[5]
        data['district_name'] = row[4]
        data['drobne_pamatky_id'] = row[8].replace(u'https://www.drobnepamatky.cz/node/', u'')
        point = Point(float(data['lon']), float(data['lat']))
        data['polygon'] = 0
        polygon_found = False
        for k, pol in self.polygons.items():
            if polygon_found:
                break
            if (point.within(pol)):
                data['polygon'] = k
                polygon_found = True
        return data

    def count_external_data(self) -> NoReturn:
        fp = open('drobnepamatky-2020-03-17.csv')
        spamreader = csv.reader(fp, delimiter=";", quotechar="'")
        count = 0

        for row in spamreader:
            if count == 0:
                count = count + 1
                continue
            try:
                data = self.fill_external_data(row)
                if data['drobne_pamatky_id'] not in self.connected:
                    self.external.append(data)
                    if data['polygon'] in self.external_by_polygon:
                        self.external_by_polygon[data['polygon']].append(data)
                    else:
                        self.external_by_polygon[data['polygon']] = []
                        self.external_by_polygon[data['polygon']].append(data)
            except IndexError as e:
                pass

    def find_correct_values(self) -> NoReturn:

        for key in self.external_by_polygon.keys():
            for external in self.external_by_polygon[key]:
                coords_orig = (external['lat'], external['lon'])
                found = False
                try:
                    for wd in self.from_wd_by_polygon[key]:
                        coords_2 = (wd['lat'], wd['lon'])

                        if (geopy.distance.vincenty(coords_orig, coords_2).m < self.distance):
                            line = {
                                'name_wd' : wd['name'],
                                'name_external' : external['name'],
                                'distance' : geopy.distance.vincenty(coords_orig, coords_2).m,
                                'qid' : wd['qid'],
                                'polygon' : key,
                                'okres_external' : external['district_name'],
                                'external_drobne_pamatky_id' : external['drobne_pamatky_id'],
                                'wd_drobne_pamatky_id' : wd['drobne_pamatky_id'],

                                'external_instance_qid' : external['instance_qid'],
                                'wd_instance_qid' : wd['instance_qid'],
                                'external_instance_name' : external['instance_name'],
                                'external_place_name' : external['place_name'],
                                'wd_place_name' : wd['place_name'],
                            }
                            self.csv_lines.append(line)
                            # print(wd)
                            # print(water)
                            # print(geopy.distance.vincenty(coords_orig, coords_2).m)
                            found = True

                    if found:
                        # print(water)
                        self.found_count = self.found_count + 1
                except KeyError as e:
                    pass

    def print_csv(self) -> NoReturn:
        if self.distance == 0:
            self.distance = 'text_pair'
        with open('drobne_pamatky_found_distance_' + str(self.distance) + '.csv', 'w', newline='') as csvfile:
            writer = csv.writer(csvfile, delimiter=";", quotechar="'", quoting=csv.QUOTE_ALL)
            writer.writerow(
                ['name_wd'] + ['name_external'] + ['distance'] +
                ['qid'] + ['okres_external'] + ['external_drobne_pamatky_id'] +
                ['wd_drobne_pamatky_id'] + ['polygon'] +
                ['external_instance_qid'] + ['wd_instance_qid'] + ['external_instance_name'] +
                ['external_place_name'] + ['wd_place_name']
            )

            for line in self.csv_lines:
                writer.writerow(
                    [line['name_wd']] + [line['name_external']] + [line['distance']] +
                    [line['qid']] + [line['okres_external']] + [line['external_drobne_pamatky_id']] +
                    [line['wd_drobne_pamatky_id']] + [line['polygon']] +
                    [line['external_instance_qid']] + [line['wd_instance_qid']] + [line['external_instance_name']] +
                    [line['external_place_name']] + [line['wd_place_name']]
                )

class vodni_plochy_to_wd():
    external: List[Dict] = []
    dibavod_exist:List[str] = []
    def fill_geo_json(self, row: Dict) -> Dict[str,str]:
        arr = {}
        arr['osm_id'] = row['OSM_ID']
        arr['name'] = row['NAME']
        arr['area'] = row['PLOCHA_M2']
        arr['region'] = row['REGION']
        arr['region_qid'] = row['REGION_WIK']
        arr['identifikator'] = row['NADR_GID']
        if (row['KOTA_HLADI']):
            arr['nadmorska_vyska'] = row['KOTA_HLADI']
        else:
            arr['nadmorska_vyska'] = 0

        if (row['HLOUBKA']):
            arr['hloubka'] = row['HLOUBKA']
        else:
            arr['hloubka'] = 0
        stred = geojson.Point([row['XCOORD'], row['YCOORD']])
        arr['stred'] = stred
        arr['lon'] = row['XCOORD']
        arr['lat'] = row['YCOORD']

        return arr

    def count_geo_json_items(self) -> NoReturn:
        self.iter = 0
        self.countInMinute = 0
        curr_datetime = datetime.now()
        self.minute = curr_datetime.minute
        fp = open('final-water.geojson.txt')
        all = geojson.load(fp)
        self.site = pywikibot.Site('cs', 'wikipedia')
        self.repo = self.site.data_repository()
        processed = []
        count = 0
        for line in all.features:
            props = line.properties
            arr = self.fill_geo_json(props)

            if arr['area'] is None:
                arr['area'] = 0
            # print((arr['area']))

            if (int(arr['area']) > 4500 and str(arr['identifikator'])):
                # self.external.append(arr)
                if (arr['identifikator'] not in processed):
                    processed.append(arr['identifikator'])
                    if (not self.logger.isCompleteFile(str(arr['identifikator']))):
                        print('ok' + ' - ' + arr['name'] + ' ' + str(arr['area']))
                        self.insert_to_wd(arr)
                        count = count + 1

            else:
                pass
                # print('not' + ' - ' + arr['name'] + ' ' + str(arr['area']))
        print(count)

    def create_item_for_page(self, data=None, summary='creating NKCR aut', repo=None, name=''):
        self.iter = self.iter + 1
        self.countInMinute = self.countInMinute + 1
        curr_datetime = datetime.now()
        actual_minute = curr_datetime.minute
        if (actual_minute != self.minute):
            pywikibot.output('Iteration: ' + str(self.iter))
            pywikibot.output('Created in last minute: ' + str(self.countInMinute))
            average = 60/self.countInMinute
            pywikibot.output('Creating time average: ' + str(average) + ' sec')
            self.minute = actual_minute

            self.countInMinute = 0

        # pywikibot.output('Creating item ' + str(self.iter) + ' (' + name + ')')
        item = pywikibot.ItemPage(repo)

        result = self.user_edit_entity(item, data, summary=summary)
        if result:
            return item
        else:
            return None

    def user_edit_entity(self, item, data=None, summary=None):
        """
        Edit entity with data provided, with user confirmation as required.

        @param item: page to be edited
        @type item: ItemPage
        @param data: data to be saved, or None if the diff should be created
          automatically
        @kwarg summary: revision comment, passed to ItemPage.editEntity
        @type summary: str
        @kwarg show_diff: show changes between oldtext and newtext (default:
          True)
        @type show_diff: bool
        @kwarg ignore_server_errors: if True, server errors will be reported
          and ignored (default: False)
        @type ignore_server_errors: bool
        @kwarg ignore_save_related_errors: if True, errors related to
          page save will be reported and ignored (default: False)
        @type ignore_save_related_errors: bool
        @return: whether the item was saved successfully
        @rtype: bool
        """
        return self._save_page(item, item.editEntity, data)


    def _save_page(self, page, func, *args):
        """
        Helper function to handle page save-related option error handling.

        @param page: currently edited page
        @param func: the function to call
        @param args: passed to the function
        @param kwargs: passed to the function
        @kwarg ignore_server_errors: if True, server errors will be reported
          and ignored (default: False)
        @kwtype ignore_server_errors: bool
        @kwarg ignore_save_related_errors: if True, errors related to
        page save will be reported and ignored (default: False)
        @kwtype ignore_save_related_errors: bool
        @return: whether the page was saved successfully
        @rtype: bool
        """
        func(*args)
        return True

    def insert_to_wd(self, pole):
        label = pole['name']
        typeOfInstance = "rybník"

        regex = u"v\. n\."
        match = re.search(regex, label, re.IGNORECASE)
        if match:
            typeOfInstance = 'nádrž'

        regex = u"nádrž"
        match = re.search(regex, label, re.IGNORECASE)
        if match:
            typeOfInstance = 'nádrž'

        regex = u"jezero"
        match = re.search(regex, label, re.IGNORECASE)
        if match:
            typeOfInstance = 'jezero'

        description = typeOfInstance + ' v ' + pole['region'].replace('okres', 'okrese') + ' (DIBAVOD ' + str(pole['identifikator']) + ')'
        print(description)
        # sources = []

        # source = pywikibot.Claim(self.repo, 'P248')
        # source.setTarget(pywikibot.ItemPage(self.repo, 'Q91659223'))
        # sources.append(source)
        # source = pywikibot.Claim(self.repo, 'P813')
        # source.setTarget(pywikibot.WbTime(year=2019, month=9, day=2))
        # sources.append(source)
        # source = pywikibot.Claim(self.repo, 'P7227')
        # source.setTarget(str(pole['identifikator']))
        # sources.append(source)

        from geopy.geocoders import GoogleV3,Nominatim
        geolocator = Nominatim()
        # geolocator = GoogleV3(api_key='AIzaSyCYZQG-JR48o76XEiXu008ghUsYU633QRQ')
        adminOk = False
        try:
            location = geolocator.reverse(str(pole['lat']) + "," + str(pole['lon']), addressdetails=True)
            if location:
                raw = location.raw['address']
                # raw = locations[0].raw
                try:
                    city = raw['village']  # select first location
                except KeyError as ke:
                    city = raw['city']
                district = raw['municipality']  # select first location
                page = pywikibot.Page(source=self.site, title=city + ' (' + district + ')')
                while page.isRedirectPage() != False:
                    page = page.getRedirectTarget()
                admin = pywikibot.ItemPage.fromPage(page)
                adminPlace = pywikibot.Claim(self.repo, 'P131')
                adminPlace.setTarget(admin)
                adminOk = True
        except Exception as e:
            pass
        # instance = pywikibot.Claim(self.repo, 'P31')
        if typeOfInstance == 'rybník':
            # instance.setTarget(pywikibot.ItemPage(self.repo, 'Q3253281'))
            qid = 'Q3253281'
            numeric = 3253281
        elif typeOfInstance == 'nádrž':
            # instance.setTarget(pywikibot.ItemPage(self.repo, 'Q131681'))
            qid = 'Q131681'
            numeric = 131681
        elif typeOfInstance == 'jezero':
            # instance.setTarget(pywikibot.ItemPage(self.repo, 'Q23397'))
            qid = 'Q23397'
            numeric = 23397
        else:
            # instance.setTarget(pywikibot.ItemPage(self.repo, 'Q3253281'))
            qid = 'Q3253281'
            numeric = 3253281

    # instance.addSources(sources)

        # country = pywikibot.Claim(self.repo, 'P17')
        # country.setTarget(pywikibot.ItemPage(self.repo, 'Q213'))
        # country.addSources(sources)

        # coords = pywikibot.Claim(self.repo, 'P625')
        # coord = pywikibot.Coordinate(float(pole['lat']), float(pole['lon']), precision=1e-07, globe='earth', site=self.repo)
        # coords.setTarget(coord)
        # coords.addSources(sources)

        unit = pywikibot.ItemPage(self.repo, 'Q25343')
        # area = pywikibot.Claim(self.repo, 'P2046')
        # area.setTarget(pywikibot.WbQuantity(amount=pole['area'], unit=unit))
        # area.addSources(sources)

        # dibavod = pywikibot.Claim(self.repo, 'P7227')
        # dibavod.setTarget(str(pole['identifikator']))
        # dibavod.addSources(sources)

        # item = pywikibot.ItemPage(self.repo)

        # labels = {'cs': label, 'en': label, 'sk': label}
        # item.editLabels(labels)
        # try:
        #     descriptions = {'cs': description}
        #     item.editDescriptions(descriptions)

        # except pywikibot.exceptions.OtherPageSaveError:
        #
        #     descriptions = {'cs': description + ' (DIBAVOD ' + str(pole['identifikator']) + ')'}
        #     item.editDescriptions(descriptions)

        data = {}

        data.setdefault('labels', {}).update({
            'en': {
                'language': 'en',
                'value': label
            },
            'cs': {
                'language': 'cs',
                'value': label
            },
            'sk': {
                'language': 'sk',
                'value': label
            },
        })

        data.setdefault('descriptions', {}).update({
            'cs': {
                'language': 'cs',
                'value': description
            },
        })

        sourceDate = pywikibot.WbTime(year=2019, month=9, day=2)

        refs = [
                    {
                        "snaks": {
                            "P248": [
                                {
                                    "snaktype": "value",
                                    "property": "P248",
                                    "datavalue": {
                                        "value": {
                                            "entity-type": "item",
                                            "numeric-id": 91659223,
                                            "id": "Q91659223"
                                        },
                                        "type": "wikibase-entityid"
                                    },
                                    "datatype": "wikibase-item"
                                }
                            ],
                            "P813": [
                                {
                                    "snaktype": "value",
                                    "property": "P813",
                                    "datavalue": {
                                        "value": {'time': sourceDate.toTimestr(),
                                                  'precision': 9,
                                                  'after': sourceDate.after,
                                                  'before': sourceDate.before,
                                                  'timezone': sourceDate.timezone,
                                                  'calendarmodel': sourceDate.calendarmodel
                                                  },
                                        "type": "time",
                                    },
                                    "datatype": "wikibase-item"
                                }
                            ],
                            "P7227": [
                                {
                                    "snaktype": "value",
                                    "property": "P7227",
                                    "datavalue": {
                                        "value": str(pole['identifikator']).strip(),
                                        "type": "string",
                                    },
                                    "datatype": "wikibase-item"
                                }
                            ],
                        }
                    }
                ]

        di = []
        if (pole['identifikator'] != ''):
            #dibavod
            dibavod = {
                "mainsnak": {
                    "snaktype": "value",
                    "property": "P7227",
                    "datavalue": {
                        "value": str(pole['identifikator']).strip(),
                        "type": "string",
                    }},
                "type": "statement",
                "rank": "normal",
                "references": refs
            }
            di.append(dibavod)

        if (pole['area']):
            quantity = pywikibot.WbQuantity(amount=pole['area'], unit=unit, site=self.site)

            amtowd = quantity.toWikibase()
            area = {
                "mainsnak": {
                    "snaktype": "value",
                    "property": "P2046",

                    "datavalue": {
                        "value":
								amtowd
							,
							"type": "quantity"}},
                "type": "statement",
                "rank": "normal",
                "references": refs
            }
            di.append(area)

        coord = pywikibot.Coordinate(float(pole['lat']), float(pole['lon']), precision=1e-07, globe='earth',
                                     site=self.repo)
        #coord
        # coord.toWikibase()
        coords = {
            "mainsnak": {
                "snaktype": "value",
                "property": "P625",
                "datavalue": {
                    "value": coord.toWikibase(),
                    "type": "globecoordinate"
                }},
            "type": "statement",
            "rank": "normal",
            "references": refs
        }
        di.append(coords)

        country = {
            "mainsnak": {
                "snaktype": "value",
                "property": "P17",
                "datavalue": {
                    "value": {
								"entity-type": "item",
								"numeric-id": 213,
								"id": "Q213"
							},
							"type": "wikibase-entityid"
                }},
            "type": "statement",
            "rank": "normal",
            "references": refs
        }
        di.append(country)

        instance = {
            "mainsnak": {
                "snaktype": "value",
                "property": "P31",
                "datavalue": {
                    "value": {
                        "entity-type": "item",
                        "numeric-id": numeric,
                        "id": qid
                    },
                    "type": "wikibase-entityid"
                }},
            "type": "statement",
            "rank": "normal",
            "references": refs
        }
        di.append(instance)

        if adminOk:
            admin = {
                "mainsnak": {
                    "snaktype": "value",
                    "property": "P131",
                    "datavalue": {
                        "value": {
                            "entity-type": "item",
                            "numeric-id": admin.getID(numeric=True),
                            "id": admin.getID()
                        },
                        "type": "wikibase-entityid"
                    }},
                "type": "statement",
                "rank": "normal",
                "references": refs
            }
            di.append(admin)

        data['claims'] = di
        data['labels']['en']['value'] = label
        data['labels']['cs']['value'] = label
        data['labels']['sk']['value'] = label
        try:
            item = self.create_item_for_page(data, 'Creating DIBAVOD pond - ' + str(pole['identifikator']), self.repo, label)
        except pywikibot.exceptions.OtherPageSaveError as e:
            pywikibot.output(e.message)
            pywikibot.output(pole['identifikator'])


        # item.addClaim(instance)
        # item.addClaim(country)
        # item.addClaim(coords)
        # item.addClaim(area)
        # item.addClaim(dibavod)
        # if (adminOk):
        #     item.addClaim(adminPlace)

        self.logger.logComplete(str(pole['identifikator']))



    def load_completed_divavods(self):
        self.logger = Logger(u'vodni_plochy' + u'Fill', 'saved')
        query = '''
        select * where {

  ?item wdt:P7227 [].
  ?item wdt:P7227 ?dibavod

}
        '''

        print(query)
        sq = pywikibot.data.sparql.SparqlQuery()
        queryresult = sq.select(query)
        for resultitem in queryresult:
            qid = resultitem.get('item').replace(u'http://www.wikidata.org/entity/', u'')
            self.dibavod_exist.append(resultitem.get('dibavod'))
            # if resultitem.get('artukid'):
            #     result[qid] = resultitem.get('id')
# pamatky = drobne_pamatky()
# pamatky.count_width_height_diffs()
# pamatky.create_polygons()
# pamatky.count_wikidata_items()
# pamatky.count_geo_json_items()
# pamatky.find_correct_values()
# pamatky.print_csv()

# pamatky.runSparqls()


# plochy = vodni_plochy()
# plochy.countWidthHeightDiffs()
# plochy.createPolygons()
# plochy.countWikidataItems()
# plochy.countGeoJsonItems()
# plochy.pairByName()
# plochy.findCorrectValues()
# plochy.printCsv()
# print(plochy.found_count)

# plochy = vodni_plochy_to_wd()
# plochy.load_completed_divavods()
# plochy.count_geo_json_items()

sochy_a_mesta = sochy_a_mesta()
# sochy_a_mesta.run_sparqls()
# sochy_a_mesta.count_width_height_diffs()
# sochy_a_mesta.create_polygons()
# sochy_a_mesta.count_wikidata_items()
# sochy_a_mesta.count_external_data()
# sochy_a_mesta.find_correct_values()
# sochy_a_mesta.print_csv()

sochy_a_mesta.pair_data()