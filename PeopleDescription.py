#!/usr/bin/python
# -*- coding: utf-8 -*-
import MyDescription
import pywikibot
import workdictionary
import re
import sys
from string import Template

class PeopleDescription(MyDescription.Description):
    def __init__(self):
        self.define()
        print "People Description"
        self.setEntityType('people_desc')
        self.setLangWord('cs', 'occupation', 'fotografové')
        self.setLangWord('en', 'occupation', 'fotografové')
        incatlist = list()
        incatlist.append(u'muži')
        incatlist.append(u'ženy')
        self.countries = dict();
        self.setMustBeInCategory(incatlist)
        self.setExcludeType('none')
        self.langDictionary()
        self.occupations = {u'en' : list(), u'cs':list()}
        self.sex = u'male'
        self.czechoslovakia = False
        #self.sexText = {u'en' : {u'female':u'czech', u'male':u'czech'}, u'cs':{u'female':u'Česká', u'male':u'Český'}}
        #self.sexText = {u'en' : {u'female':u'slovak', u'male':u'slovak'}, u'cs':{u'female':u'Slovenská', u'male':u'Slovenský'}}
        self.sexText = {u'en' : {u'female':u'austrian', u'male':u'austrian'}, u'cs':{u'female':u'Rakouská', u'male':u'Rakouský'}}
        self.sexTextCzechoslovakia = {u'en' : {u'female':u'czechoslovakia', u'male':u'czechoslovakia'}, u'cs':{u'female':u'československá', u'male':u'československý'}}

        descriptionStyle = {u'en' : u'$sextext $occupation', u'cs':u'$sextext $occupation'}
        self.getData('Rakušané podle činnosti', descriptionStyle)

    def occupationReset(self):
        self.occupations = {u'en' : list(), u'cs':list()}

    def addOccupation(self, lang, occupation):
        if (self.dictionary[occupation]['append']):
            self.occupations[lang].append(self.dictionary[occupation][lang][self.sex])

    def getOccupationText(self, lang):
        text = ''
        iter = 0

        andText = {u'en' : u'and', u'cs':u'a'}
        occupationBox = list()
        for cleanOccupation in self.occupations[lang]:

            if cleanOccupation not in occupationBox:
                occupationBox.append(cleanOccupation)

        self.occupations[lang] = occupationBox

        for occupation in self.occupations[lang]:
            iter = iter + 1
            if (iter == len(self.occupations[lang])):
                if (len(self.occupations[lang]) == 1):
                    text = occupation
                else:
                    text = text + ' ' + andText[lang] + ' ' + occupation
            elif (iter == 1):
                text = occupation
            else:
                text = text + ', ' + occupation

        return text

    def prepareCounty(self, article):
        self.county = True



    def langDictionary(self):
        self.dictionary = dict()

        self.dictionary = workdictionary.WorkDictionary().load().getDictionary()

    def prepareText(self, article):
        neco = article.categories()
        self.occupationReset()
        falAction = False

        female = False
        for cat in neco:
            sexTest = re.search(u'ženy', cat.title().lower())
            if (sexTest):
                female = True

        if (female):
            self.sex = u'female'
        else:
            self.sex = u'male'
        self.czechoslovakia = False
        for cat in neco:
            testCesti = re.search(u'čeští', cat.title().lower())
            testCeska = re.search(u'česka', cat.title().lower())
            testVysadku = re.search(u'výsadků', cat.title().lower())

            testArcivevodove = re.search(u'arcivévodové', cat.title().lower())

            testSlovensti = re.search(u'slovenští', cat.title().lower())
            testSlovenska = re.search(u'slovenska', cat.title().lower())
            testSlovenske = re.search(u'slovenské', cat.title().lower())
            testSlovenskej = re.search(u'slovenskej', cat.title().lower())

            testRakousti = re.search(u'rakouští', cat.title().lower())
            testRakouska = re.search(u'rakouska', cat.title().lower())
            testRakouske = re.search(u'rakouské', cat.title().lower())
            testRakousko = re.search(u'rakousko', cat.title().lower())

            testToskansti = re.search(u'toskánští', cat.title().lower())

            testTesinska = re.search(u'těšínská', cat.title().lower())

            testUhersti = re.search(u'uherští', cat.title().lower())

            testChorvatsti = re.search(u'chorvatští', cat.title().lower())

            testTyrolska = re.search(u'tyrolská', cat.title().lower())

            testModensti = re.search(u'modenští', cat.title().lower())

            testSpanelsti = re.search(u'španělští', cat.title().lower())

            testKastilsti = re.search(u'kastilští', cat.title().lower())

            testBurgundsti = re.search(u'burgundští', cat.title().lower())
            testBurgundska = re.search(u'burgundská', cat.title().lower())

            testLimbursti = re.search(u'limburští', cat.title().lower())

            testLucembursti = re.search(u'lucemburští', cat.title().lower())

            testBrabantsti = re.search(u'brabantští', cat.title().lower())

            testFlanderska = re.search(u'flanderská', cat.title().lower())



            testCeskoslovensti = re.search(u'českoslovenští', cat.title().lower())
            testCeskoslovenske = re.search(u'československé', cat.title().lower())

            testCeske = re.search(u'české', cat.title().lower())

            if (testCeskoslovenske or testCeskoslovensti):
                self.czechoslovakia = True


            testHejtmani = re.search(u'hejtmani', cat.title().lower());
            testVyucujici = re.search(u'vyučující', cat.title().lower());
            if (testCeska or testCesti or testCeske or testCeskoslovensti or testCeskoslovenske or testSlovensti or testSlovenska or testSlovenske or testVysadku or testRakouska or testRakouske or testRakousti or testArcivevodove or testToskansti or testRakousko or testTesinska or testUhersti or testChorvatsti or testTyrolska or testFlanderska or testBrabantsti or testBurgundska or testBurgundsti or testLucembursti or testLimbursti or testSpanelsti or testKastilsti or testModensti):
                try:
                    self.addOccupation('cs', cat.title())
                    self.addOccupation('en', cat.title())
                except:
                    self.errLog.log(article.title(), cat.title())
                    falAction = True

        if (falAction):
            return False
        return True

    def setDescriptions(self):
        self.setLangWord('cs', 'occupation', self.getOccupationText('cs'))
        self.setLangWord('en', 'occupation', self.getOccupationText('en'))



        for lang in self.langVersions:

            if self.czechoslovakia:
                sexTextFinal = self.sexTextCzechoslovakia
            else:
                sexTextFinal = self.sexText

            s = Template(self.getInText(lang))
            self.descriptions[lang] = s.substitute(occupation=self.getLangWord(lang, 'occupation'), sextext=sexTextFinal[lang][self.sex])

        for lang in self.langVersions:
            if (lang in self.item.descriptions.keys()):
                self.descriptions.pop(lang)

            if (len(self.occupations[lang]) == 0):
                self.descriptions.pop(lang)
                self.errLog.log(self.name, 'ERR - jen ceska')


