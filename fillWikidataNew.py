#!/usr/bin/python
# -*- coding: utf-8 -*-
import pywikibot
import sys
import exceptions
from logger import Logger
import re
import curses

class FillWikidata:
    def __init__(self):
        print 'fillWikidata'
        self.categoryName = u"Střední školy v Praze"
        self.logger = Logger(u'fillWikidataNew','saved')
        self.actualPage = None
        self.wikidata = None
        self.claimToAdd = None
        self.site = pywikibot.getSite('cs', 'wikipedia')
        self.siteWikidata = pywikibot.getSite('wikidata')
        self.claimsToAdd = []


    def prepareTimeClaimToAdd(self, year, property):

        timestamp = pywikibot.WbTime(year=year)
        newClaim = pywikibot.Claim(self.siteWikidata,property) #instance
        newClaim.setTarget(timestamp)
        self.claimsToAdd.append({u'property' : property, u'claim' : newClaim})

    def prepareClaimToAdd(self, name, property):
        nameItem = pywikibot.Page(self.site, name).data_item()
        newClaim = pywikibot.Claim(self.siteWikidata,property) #instance
        newClaim.setTarget(nameItem)
        self.claimsToAdd.append({u'property' : property, u'claim' : newClaim})

    def run(self):
        page = pywikibot.Page(self.site, u'Category:' + self.categoryName)
        generator = pywikibot.Category(page).articles(recurse=True)
        for page in generator:
            if not(self.logger.isCompleteFile(page.title())):
                print page.title()
                self.actualPage = page
                self.claimsToAdd = []
                self.prepareClaimToAdd(u'Česko', u'P17')
                self.prepareClaimToAdd(u'Praha', u'P131')
                
                try:
                    self.wikidata = self.actualPage.data_item()
                    if u'P17' not in self.wikidata.claims:
                        #self.wikidata.addClaim(self.claimToAdd)
                        for claim in self.claimsToAdd:
                            self.wikidata.addClaim(claim['claim'])

                except:
                    pass

                self.logger.logComplete(page.title())

f = FillWikidata()
f.run()
