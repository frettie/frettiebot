#!/usr/bin/python
# -*- coding: utf-8 -*-
import json
import re
import unicodecsv
import requests
import unicodedata

import pywikibot
from pywikibot import pagegenerators
# import IOError




class excel_semicolon(unicodecsv.Dialect):
    """Describe the usual properties of Excel-generated CSV files."""
    delimiter = ';'
    quotechar = '"'
    doublequote = True
    skipinitialspace = False
    lineterminator = '\r\n'
    quoting = unicodecsv.QUOTE_MINIMAL

def check_names():
    query = '''
    select ?ico ?icoLabel where {
?ico wdt:P4156 []
     SERVICE wikibase:label { bd:serviceParam wikibase:language "cs". }
}
    '''
    from pywikibot.data import sparql
    query_object = sparql.SparqlQuery()
    data = query_object.select(query, full_data=True)
    site = pywikibot.Site('wikidata', 'wikidata')
    repo = site.data_repository()
    if data:
        for r in data:
            label = str(r['icoLabel'])
            regex = r"([^,]*)([, ]*(s\.r\.o\.|a\.s\.|k\.s\.|SE$|sro\.|spol\. s r\.o\.)[ ]*)"
            ma = re.search(regex, label)
            if not ma:
                pass
            else:
                grp = ma.groups()
                # print(grp[0].strip())
                item_id = r['ico'].getID()
                print(item_id)
                item = pywikibot.ItemPage(repo, item_id)
                item.get()
                data = {}
                data['labels'] = item.labels
                data['aliases'] = item.aliases
                print(data)
                data.setdefault('labels', {}).update({
                    'en': {
                        'language': 'en',
                        'value': grp[0].strip()
                    },
                    'cs': {
                        'language': 'cs',
                        'value': grp[0].strip()
                    },
                })

                data.setdefault('aliases', {}).update({
                    'en': {
                        'language': 'en',
                        'value': label
                    },
                    'cs': {
                        'language': 'cs',
                        'value': label
                    },
                })
                try:
                    user_edit_entity(item,data,'Edit company labels and aliases')
                except pywikibot.exceptions.OtherPageSaveError:
                    pass


def sokol():

    site = pywikibot.Site('wikidata', 'wikidata')
    repo = site.data_repository()
    # page = repo.page_from_repository('Q58952621')

    genFactory = pagegenerators.GeneratorFactory(site=site)
    # genFactory.handleArg('-ns:0')
    generator = genFactory.getCombinedGenerator()
    if not generator:
        genFactory.handleArg(u'-ref:Q58952621')
        generator = genFactory.getCombinedGenerator()

    for gendata in generator:
        item = pywikibot.ItemPage(repo, gendata.title())
        item.get()
        # print(item.labels['cs'])

        regex = r"(T\.J\.|TJ) (.*)"
        ma = re.search(regex, item.labels['cs'])
        try:
            try:
                if (item.claims['P4156']):
                    raise IOError('hotovo')
            except KeyError:
                pass
            if not ma:
                # print('nenalezeno: ' + item.labels['cs'])
                raise IOError('Nenalezeno')
            else:
                grp = ma.groups()
                print('nalezeno: ' + grp[1])
                name = "".join(aChar
                               for aChar in unicodedata.normalize("NFD", grp[1])
                               if not unicodedata.combining(aChar))
                urlmesto = "http://127.0.0.1:5000/ico/Telocvicna jednota " + name

                response = requests.get(urlmesto)
                jsontext = response.text
                icdata = json.loads(jsontext)
                # print(data)

                try:
                    ico = icdata['ic']
                except KeyError:

                    urlmesto = "http://127.0.0.1:5000/ico/" + name
                    response = requests.get(urlmesto)
                    jsontext = response.text
                    icdata = json.loads(jsontext)
                    ico = icdata['ic']

                urlmesto = "https://tools.wmflabs.org/hub/P4156:" + ico + "?lang=cs&format=json"
                response = requests.get(urlmesto)
                jsontext = response.text
                data = json.loads(jsontext)

                mess = data['message']
                print('neexistuje firma: ' + item.labels['cs'] + ' IC: ' + ico)
                urlmesto = "https://tools.wmflabs.org/csfd/ico/" + ico
                response = requests.get(urlmesto)
                jsontext = response.text
                dataCompany = json.loads(jsontext)
                data = {}

                data.setdefault('labels', {}).update({
                    'en': {
                        'language': 'en',
                        'value': dataCompany['name']
                    },
                    'cs': {
                        'language': 'cs',
                        'value': dataCompany['name']
                    },
                })

                data.setdefault('descriptions', {}).update({
                    'en': {
                        'language': 'en',
                        'value': 'Czech company'
                    },
                    'cs': {
                        'language': 'cs',
                        'value': 'česká společnost'
                    },
                })
                # if (line[2] != ''):
                #     item = pywikibot.ItemPage(repo, line[2])
                #     data = None
                # else:
                # page = None
                # item = create_item_for_page(page=gendata, data=data, repo=repo, summary='Creating czech company')

                sources = []

                source = pywikibot.Claim(repo, 'P248')
                source.setTarget(pywikibot.ItemPage(repo, 'Q8182488'))

                instance = pywikibot.Claim(repo, 'P31')
                instance.setTarget(pywikibot.ItemPage(repo, 'Q4830453'))

                country = pywikibot.Claim(repo, 'P17')
                country.setTarget(pywikibot.ItemPage(repo, 'Q213'))

                pravniforma = pywikibot.Claim(repo, 'P1454')
                pravniforma.setTarget(pywikibot.ItemPage(repo, dataCompany['forma_wd']))


                icoclaim = pywikibot.Claim(repo, 'P4156')
                icoclaim.setTarget(ico)


                nazev = pywikibot.Claim(repo, 'P1448')
                nazev.setTarget(pywikibot.WbMonolingualText(dataCompany['name'],'cs'))


                datumzalozeni = pywikibot.Claim(repo, 'P571')
                regex = r"\+([0-9]{4})-([0-9]{2})-([0-9]{2})T[0-9:]+Z"
                ma = re.search(regex, dataCompany['datum_vzniku'])
                if not ma:
                    okDatumZalozeni = False
                    pass
                else:
                    grp = ma.groups()
                    date = pywikibot.WbTime(year=int(grp[0]),month=int(grp[1]), day=int(grp[2]), precision=11)
                    datumzalozeni.setTarget(date)
                    okDatumZalozeni = True

                psc = pywikibot.Claim(repo, 'P281')
                psc.setTarget(dataCompany['psc'])

                if (len(dataCompany['wd_ulice'])>0):
                    ulice = pywikibot.Claim(repo, 'P669')
                    ulice.setTarget(pywikibot.ItemPage(repo, dataCompany['wd_ulice']))

                cp = pywikibot.Claim(repo, 'P4856')
                cp.setTarget(dataCompany['cp'])

                co = pywikibot.Claim(repo, 'P670')
                co.setTarget(dataCompany['co'])
                if (len(dataCompany['wd_mesto'])>0):
                    sidloreditelstvi = pywikibot.Claim(repo, 'P159')
                    sidloreditelstvi.setTarget(pywikibot.ItemPage(repo, dataCompany['wd_mesto']))



                # item.addClaim(instance)
                # item.addClaim(country)
                item.addClaim(pravniforma)
                item.addClaim(icoclaim)
                item.addClaim(nazev)
                if (okDatumZalozeni):
                    item.addClaim(datumzalozeni)
                if (len(dataCompany['wd_mesto']) > 0):
                    item.addClaim(sidloreditelstvi)
                if (len(dataCompany['wd_mesto']) > 0):
                    if len(dataCompany['psc'])>0:
                        sidloreditelstvi.addQualifier(psc)
                    if len(dataCompany['wd_ulice'])>0:
                        sidloreditelstvi.addQualifier(ulice)
                    if len(dataCompany['cp'])>0:
                        sidloreditelstvi.addQualifier(cp)
                    if (len(dataCompany['co'])>0):
                        sidloreditelstvi.addQualifier(co)
                if (len(dataCompany['wd_mesto']) > 0):
                    sidloreditelstvi.addSource(source)
                nazev.addSource(source)
                icoclaim.addSource(source)
                # instance.addSource(source)
                # country.addSource(source)
                pravniforma.addSource(source)
                if (okDatumZalozeni):
                    datumzalozeni.addSource(source)
        except pywikibot.exceptions.OtherPageSaveError:
            pass
        except KeyError:
            pass
        except json.decoder.JSONDecodeError:
            pass
        except IOError:
            pass
        # return True


def prepare():
    with open('merk_15mld.csv', 'rb') as f:
        unicodecsv.register_dialect('excel_semicolon', excel_semicolon)
        reader = unicodecsv.reader(f, encoding='utf-8', dialect=excel_semicolon)
        # headers = next(reader)
        csv = list(reader)

    cities = {}
    site = pywikibot.Site('wikidata', 'wikidata')
    repo = site.data_repository()
    # try:
    text = ''
    for line in csv:
        ico = line[1]
        urlmesto = "https://tools.wmflabs.org/hub/P4156:" + ico + "?lang=cs&format=json"
        response = requests.get(urlmesto)
        jsontext = response.text
        data = json.loads(jsontext)
        try:
            mess = data['message']
            print('neexistuje firma: ' + line[0] + ' IC: ' + line[1])
            urlmesto = "https://tools.wmflabs.org/csfd/ico/" + line[1]
            response = requests.get(urlmesto)
            jsontext = response.text
            dataCompany = json.loads(jsontext)
            data = {}

            data.setdefault('labels', {}).update({
                'en': {
                    'language': 'en',
                    'value': dataCompany['name']
                },
                'cs': {
                    'language': 'cs',
                    'value': dataCompany['name']
                },
            })

            data.setdefault('descriptions', {}).update({
                'en': {
                    'language': 'en',
                    'value': 'Czech company'
                },
                'cs': {
                    'language': 'cs',
                    'value': 'česká společnost'
                },
            })
            if (line[2] != ''):
                item = pywikibot.ItemPage(repo, line[2])
                data = None
            else:
                page = None
                item = create_item_for_page(page=page, data=data, repo=repo, summary='Creating czech company')

            sources = []

            source = pywikibot.Claim(repo, 'P248')
            source.setTarget(pywikibot.ItemPage(repo, 'Q8182488'))

            instance = pywikibot.Claim(repo, 'P31')
            instance.setTarget(pywikibot.ItemPage(repo, 'Q4830453'))

            country = pywikibot.Claim(repo, 'P17')
            country.setTarget(pywikibot.ItemPage(repo, 'Q213'))

            pravniforma = pywikibot.Claim(repo, 'P1454')
            pravniforma.setTarget(pywikibot.ItemPage(repo, dataCompany['forma_wd']))


            icoclaim = pywikibot.Claim(repo, 'P4156')
            icoclaim.setTarget(line[1])


            nazev = pywikibot.Claim(repo, 'P1448')
            nazev.setTarget(pywikibot.WbMonolingualText(dataCompany['name'],'cs'))


            datumzalozeni = pywikibot.Claim(repo, 'P571')
            regex = r"\+([0-9]{4})-([0-9]{2})-([0-9]{2})T[0-9:]+Z"
            ma = re.search(regex, dataCompany['datum_vzniku'])
            if not ma:
                okDatumZalozeni = False
                pass
            else:
                grp = ma.groups()
                date = pywikibot.WbTime(year=int(grp[0]),month=int(grp[1]), day=int(grp[2]), precision=11)
                datumzalozeni.setTarget(date)
                okDatumZalozeni = True

            psc = pywikibot.Claim(repo, 'P281')
            psc.setTarget(dataCompany['psc'])

            if (len(dataCompany['wd_ulice'])>0):
                ulice = pywikibot.Claim(repo, 'P669')
                ulice.setTarget(pywikibot.ItemPage(repo, dataCompany['wd_ulice']))

            cp = pywikibot.Claim(repo, 'P4856')
            cp.setTarget(dataCompany['cp'])

            co = pywikibot.Claim(repo, 'P670')
            co.setTarget(dataCompany['co'])
            if (len(dataCompany['wd_mesto'])>0):
                sidloreditelstvi = pywikibot.Claim(repo, 'P159')
                sidloreditelstvi.setTarget(pywikibot.ItemPage(repo, dataCompany['wd_mesto']))



            item.addClaim(instance)
            item.addClaim(country)
            item.addClaim(pravniforma)
            item.addClaim(icoclaim)
            item.addClaim(nazev)
            if (okDatumZalozeni):
                item.addClaim(datumzalozeni)
            if (len(dataCompany['wd_mesto']) > 0):
                item.addClaim(sidloreditelstvi)
            if (len(dataCompany['wd_mesto']) > 0):
                if len(dataCompany['psc'])>0:
                    sidloreditelstvi.addQualifier(psc)
                if len(dataCompany['wd_ulice'])>0:
                    sidloreditelstvi.addQualifier(ulice)
                if len(dataCompany['cp'])>0:
                    sidloreditelstvi.addQualifier(cp)
                if (len(dataCompany['co'])>0):
                    sidloreditelstvi.addQualifier(co)
            if (len(dataCompany['wd_mesto']) > 0):
                sidloreditelstvi.addSource(source)
            nazev.addSource(source)
            icoclaim.addSource(source)
            instance.addSource(source)
            country.addSource(source)
            pravniforma.addSource(source)
            if (okDatumZalozeni):
                datumzalozeni.addSource(source)
        except pywikibot.exceptions.OtherPageSaveError:
            pass
        except KeyError:
            pass
        except json.decoder.JSONDecodeError:
            pass
        except BaseException:
            pass
    return cities

def create_item_for_page(page=None, data=None, summary = None, repo=None):
        if not summary:
            summary = (u'Bot: New item with sitelink from %s'
                       % page.title(asLink=True, insite=repo))

        if data is None:
            data = {}
            data.setdefault('sitelinks', {}).update({
                page.site.dbName(): {
                    'site': page.site.dbName(),
                    'title': page.title()
                }
            })
            data.setdefault('labels', {}).update({
                page.site.lang: {
                    'language': page.site.lang,
                    'value': page.title()
                }
            })

        if (not page):
            pywikibot.output('Creating item')
            item = pywikibot.ItemPage(repo)
        else:
            pywikibot.output('Creating item for %s...' % page)
            item = pywikibot.ItemPage(page.site.data_repository())

        result = user_edit_entity(item, data, summary=summary)
        if result:
            return item
        else:
            return None

def user_edit_entity(item, data=None, summary=None):
    """
    Edit entity with data provided, with user confirmation as required.

    @param item: page to be edited
    @type item: ItemPage
    @param data: data to be saved, or None if the diff should be created
      automatically
    @kwarg summary: revision comment, passed to ItemPage.editEntity
    @type summary: str
    @kwarg show_diff: show changes between oldtext and newtext (default:
      True)
    @type show_diff: bool
    @kwarg ignore_server_errors: if True, server errors will be reported
      and ignored (default: False)
    @type ignore_server_errors: bool
    @kwarg ignore_save_related_errors: if True, errors related to
      page save will be reported and ignored (default: False)
    @type ignore_save_related_errors: bool
    @return: whether the item was saved successfully
    @rtype: bool
    """
    return _save_page(item, item.editEntity, data)

def _save_page(page, func, *args):
    """
    Helper function to handle page save-related option error handling.

    @param page: currently edited page
    @param func: the function to call
    @param args: passed to the function
    @param kwargs: passed to the function
    @kwarg ignore_server_errors: if True, server errors will be reported
      and ignored (default: False)
    @kwtype ignore_server_errors: bool
    @kwarg ignore_save_related_errors: if True, errors related to
    page save will be reported and ignored (default: False)
    @kwtype ignore_save_related_errors: bool
    @return: whether the page was saved successfully
    @rtype: bool
    """
    func(*args)
    return True

def firmyDoplnSidlo():
    query = '''
        SELECT ?item ?label ?_image WHERE {
      ?item wdt:P31 wd:Q6881511.
      ?item wdt:P17 wd:Q213
      SERVICE wikibase:label {
        bd:serviceParam wikibase:language "cs" . 
        ?item rdfs:label ?label
      }
      MINUS{?item wdt:P159 []}
    OPTIONAL { ?item wdt:P18 ?_image. }
    }
        '''
    from pywikibot.data import sparql
    query_object = sparql.SparqlQuery()
    firmy = query_object.select(query, full_data=True)
    site = pywikibot.Site('wikidata', 'wikidata')
    repo = site.data_repository()



    for gendata in firmy:
        item = pywikibot.ItemPage(repo, gendata['item'].getID())
        item.get()
        # print(item.labels['cs'])


        try:
            ico = item.claims['P4156'][0].getTarget()

            urlmesto = "https://tools.wmflabs.org/csfd/ico/" + ico
            response = requests.get(urlmesto)
            jsontext = response.text
            dataCompany = json.loads(jsontext)

            sources = []

            source = pywikibot.Claim(repo, 'P248')
            source.setTarget(pywikibot.ItemPage(repo, 'Q8182488'))

            psc = pywikibot.Claim(repo, 'P281')
            psc.setTarget(dataCompany['psc'])

            if (len(dataCompany['wd_ulice']) > 0):
                ulice = pywikibot.Claim(repo, 'P669')
                ulice.setTarget(pywikibot.ItemPage(repo, dataCompany['wd_ulice']))

            cp = pywikibot.Claim(repo, 'P4856')
            cp.setTarget(dataCompany['cp'])

            co = pywikibot.Claim(repo, 'P670')
            co.setTarget(dataCompany['co'])
            if (len(dataCompany['wd_mesto']) > 0):
                sidloreditelstvi = pywikibot.Claim(repo, 'P159')
                sidloreditelstvi.setTarget(pywikibot.ItemPage(repo, dataCompany['wd_mesto']))

            if (len(dataCompany['wd_mesto']) > 0):
                item.addClaim(sidloreditelstvi)
            if (len(dataCompany['wd_mesto']) > 0):
                if len(dataCompany['psc']) > 0:
                    sidloreditelstvi.addQualifier(psc)
                if len(dataCompany['wd_ulice']) > 0:
                    sidloreditelstvi.addQualifier(ulice)
                if len(dataCompany['cp']) > 0:
                    sidloreditelstvi.addQualifier(cp)
                if (len(dataCompany['co']) > 0):
                    sidloreditelstvi.addQualifier(co)
            if (len(dataCompany['wd_mesto']) > 0):
                sidloreditelstvi.addSource(source)
        except pywikibot.exceptions.OtherPageSaveError:
            pass
        except KeyError:
            pass
        except json.decoder.JSONDecodeError:
            pass
        except IOError:
            pass
        # return True

# prepare()
# sokol()
firmyDoplnSidlo()