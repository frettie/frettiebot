#!/usr/bin/python
# -*- coding: utf-8 -*-
import time

import json
import requests
import urllib.parse

pages = {
    'ar': 'مقراب أفق الحدث',
    'ca': 'Event Horizon Telescope',
    'cs': 'Event Horizon Telescope',
    'cy': 'Telesgop Event Horizon',
    'da': 'Event Horizon Telescope',
    'de': 'Event Horizon Telescope',
    'en': 'Event Horizon Telescope',
    'es': 'Event Horizon Telescope',
    'eu': 'Event Horizon Telescope',
    'fa': 'تلسکوپ افق رویداد',
    'fr': 'Event Horizon Telescope',
    'gl': 'Telescopio de Horizonte de Eventos',
    'id': 'Event Horizon Telescope',
    'it': 'Event Horizon Telescope',
    'ja': 'イベントホライズンテレスコープ',
    'pl': 'Teleskop Horyzontu Zdarzeń',
    'pt': 'Event Horizon Telescope',
    'ro': 'Event Horizon Telescope',
    'ru': 'Телескоп горизонта событий',
    'sk': 'Event Horizon Telescope',
    'sr': 'Телескоп хоризонта догађаја',
    'sv': 'Event Horizon Telescope',
    'th': 'กล้องโทรทรรศน์ขอบฟ้าเหตุการณ์',
    'tl': 'Event Horizon Telescope',
    'uk': 'Телескоп горизонту подій',
    'vi': 'Kính thiên văn Chân trời sự kiện',
    # 'zh_min_nan': 'Event Horizon Telescope',
    'zh': '事件視界望遠鏡'
}


for lang in pages:
    # print(pages[lang])
    # time.sleep(3)
    api = 'https://'+lang+'.wikipedia.org/w/api.php?action=query&prop=revisions&titles='+urllib.parse.quote_plus(pages[lang])+'&rvlimit=500&rvprop=timestamp%7Cuser%7Ccomment&rvstart=2019-04-10T00:00:00Z&rvdir=newer&format=json'
    response = requests.get(api)
    page = json.loads(response.text)
    for key,page in page['query']['pages'].items():
        print(lang + ' : ' + str(len(page['revisions'])))
