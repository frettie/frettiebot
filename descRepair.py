#!/usr/bin/python
# -*- coding: utf-8 -*-
import pywikibot
import sys
import exceptions
from logger import Logger
import re
import curses

class FillWikidata:
    def __init__(self):

        print 'fillWikidata'
        self.categoryName = u"Češi podle činnosti"
        self.logger = Logger(u'descRepair','saved')
        self.actualPage = None
        self.wikidata = None
        self.claimToAdd = None
        self.site = pywikibot.getSite('cs', 'wikipedia')
        self.siteWikidata = pywikibot.getSite('wikidata')
        self.claimsToAdd = []


    def prepareTimeClaimToAdd(self, year, property):

        timestamp = pywikibot.WbTime(year=year)
        newClaim = pywikibot.Claim(self.siteWikidata,property) #instance
        newClaim.setTarget(timestamp)
        self.claimsToAdd.append({u'property' : property, u'claim' : newClaim})

    def prepareClaimToAdd(self, name, property):
        nameItem = pywikibot.Page(self.site, name).data_item()
        newClaim = pywikibot.Claim(self.siteWikidata,property) #instance
        newClaim.setTarget(nameItem)
        self.claimsToAdd.append({u'property' : property, u'claim' : newClaim})

    def run(self):
        page = pywikibot.Page(self.site, u'Category:' + self.categoryName)
        generator = pywikibot.Category(page).articles(recurse=True)
        for page in generator:
            if not(self.logger.isCompleteFile(page.title())):
                print page.title()
                self.actualPage = page

                
                self.wikidata = self.actualPage.data_item()

                data = self.wikidata.get()
                #print data['descriptions']
                #print data['descriptions'][u'en'];
                newDesc = {}
                save = False
                if u'cs' in data['descriptions']:
                    if u'Č' in data['descriptions'][u'cs'][:1]:
                        endDesc = data['descriptions'][u'cs'][1:]
                        newDesc[u'cs'] = u'č' + endDesc
                        save = True

                if u'en' in data['descriptions']:
                    if u'C' in data['descriptions'][u'en'][:1]:
                        endDesc = data['descriptions'][u'en'][1:]
                        newDesc[u'en'] = u'c' + endDesc
                        save = True

                if save:
                    self.wikidata.editDescriptions(newDesc)

                self.logger.logComplete(page.title())

f = FillWikidata()
f.run()
