#!/usr/bin/python
# -*- coding: utf-8 -*-
import unicodecsv

from logger import Logger
import untangle
import pywikibot
import re

from pywikibot import pagegenerators
from pywikibot.textlib import mwparserfromhell as parser

from math import radians, cos, sin, asin, sqrt

def haversine(lon1, lat1, lon2, lat2):
    """
    Calculate the great circle distance between two points
    on the earth (specified in decimal degrees)
    """
    # convert decimal degrees to radians
    lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])
    # haversine formula
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
    c = 2 * asin(sqrt(a))
    km = 6367 * c;
    return km

class farnost:
    def old(self):
        logger_name = "farnosti_wd"
        self.errors = {}
        print('farnost')

        self.logger = Logger(logger_name + u'Fill', 'saved')
        self.actual_page = None
        self.wikidata = None
        self.language_wikipedia = 'cs'
        self.wikidata_family = 'wikidata'
        self.family = 'wikipedia'

        self.dataCoords = {}

        self.site = pywikibot.Site('cs', 'wikipedia')
        self.repo = self.site.data_repository()

        self.genFactory = pagegenerators.GeneratorFactory(site=self.site)
        # self.genFactory.handleArg('-ns:0')
        self.generator = self.genFactory.getCombinedGenerator()
        if not self.generator:
            self.genFactory.handleArg(u'-catr:Zaniklé farnosti litoměřické diecéze‎')
            self.generator = self.genFactory.getCombinedGenerator()
            # self.genFactory.handleArg(u'-page:' + u'Seznam kulturních památek v okrese Klatovy')
            # self.generator = self.genFactory.getCombinedGenerator()
        full = []
        for page in pagegenerators.PreloadingGenerator(self.generator):
            assert isinstance(page, pywikibot.Page)
            cats = page.categories()
            arr = []
            for cat in cats:
                assert isinstance(cat, pywikibot.Category)
                match = re.search(u'Farnosti', cat.title())

                if not match:
                    # pywikibot.output(cat.title())
                    empty = True
                else:
                    arr.append(cat.title())

            # pywikibot.output(page.title() + ';' + ''.join(arr))
            full.append(page.title() + ';' + ''.join(arr))
            # item = page.data_item()
            # assert isinstance(item, pywikibot.ItemPage)
            # pywikibot.output('fds')
        pywikibot.output('konec')
        for l in full:
            pywikibot.output(l)

    def dupli(self):
        file = open('farnosti.csv','r')
        for line in file:
            pywikibot.output(line)
#

f = farnost()