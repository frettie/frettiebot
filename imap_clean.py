
HOST = 'mail2.praguebest.cz'
PORT = '993'
LOGIN = 'sedlacek@praguebest.cz'
PASSWORD = 'd0ebef4c88'

from imap_tools import MailBox, AND

# get list of email subjects from INBOX folder
with MailBox(HOST).login(LOGIN, PASSWORD, initial_folder='Trash') as mailbox:
    # for msg in mailbox.fetch(AND(subject='Chyba')):
    for msg in mailbox.fetch():
        print(msg.subject, msg.date.year, msg.date.month, msg.date.day)
        subj = msg.subject
        hotovo = False
        is_chyba = "Chyba na serveru" in subj
        if is_chyba and not hotovo:
            mailbox.delete(msg.uid)
            hotovo = True

        is_chyba = "Fatal error na serveru" in subj
        if is_chyba and not hotovo:
            mailbox.delete(msg.uid)
            hotovo = True

        is_chyba = "error occurred" in subj
        if is_chyba and not hotovo:
            mailbox.delete(msg.uid)
            hotovo = True

        is_chyba = "Order no." in subj
        if is_chyba and not hotovo:
            mailbox.delete(msg.uid)
            hotovo = True

        is_chyba = "registrace do newsletteru" in subj
        if is_chyba and not hotovo:
            mailbox.delete(msg.uid)
            hotovo = True

        is_chyba = "chyba formuláře" in subj
        if is_chyba and not hotovo:
            mailbox.delete(msg.uid)
            hotovo = True

        is_chyba = "chyba form" in subj
        if is_chyba and not hotovo:
            mailbox.delete(msg.uid)
            hotovo = True

        is_chyba = "Contact us" in subj
        if is_chyba and not hotovo:
            mailbox.delete(msg.uid)
            hotovo = True

        is_chyba = "Napište nám" in subj
        if is_chyba and not hotovo:
            mailbox.delete(msg.uid)
            hotovo = True

        is_chyba = "Objednávka č." in subj
        if is_chyba and not hotovo:
            mailbox.delete(msg.uid)
            hotovo = True

        is_chyba = "Chyba v ČSOB CEB" in subj
        is_chyba = "OB CEB" in subj
        if is_chyba and not hotovo:
            mailbox.delete(msg.uid)
            hotovo = True

        is_chyba = "Export objed" in subj
        if is_chyba and not hotovo:
            mailbox.delete(msg.uid)
            hotovo = True

        is_chyba = "FlexiBee" in subj
        if is_chyba and not hotovo:
            mailbox.delete(msg.uid)
            hotovo = True

        is_chyba = "VEMZU objed" in subj
        if is_chyba and not hotovo:
            mailbox.delete(msg.uid)
            hotovo = True

        spam_words = ['rozchodu', 'hmyzu', 'strop', 'grilo', 'radar', 'kredit', 'outlet'
                      'slevu', 'teplo', 'teple', 'birkenstock', 'sadou', 'americ', 'zahrad',
                      'festival', 'horku', 'vyzvednu', 'teplu', 'dalekohled', 'slevy',
                      'superlevne', 'zbrusu', 'byty', 'slev', 'tlak', 'balen', 'domu',
                      'startuj', 'hadice', 'hodinky', 'pomilovat', 'hyundai', 'hork', 'korado',
                      'firem', 'desku', 'tepla', 'super', 'pokoje', 'semin', 'ixtent', 'nemoc',
                      'prach', 'penis', 'centimetr', 'prach', 'telefon', 'cenu', 'idiot',
                      'mobilom', 'Lacoste', 'vůně', 'gelem', 'gel', 'auta', 'sprej', 'pokut',
                      'zahrad', 'chlat', 'future', 'radar', 'žárov', 'malov', 'bytu', 'vlas',
                      'pásk', 'lůžkovin', 'lak', 'nehodu', 'by si', 'absenci', 'skladem', 'okap',
                      'maluj', 'r/3', 'storage', 'returned', 'sadu', 'vzory', 'vedr', 'teras',
                      'rychlost', 'sotva', 'hmyz', 'ochranu', 'zakryj', 'obnov', 'sen', 'brýl',
                      'srážka', 'nehoda', 'dvoř', 'svít', 'prolákl', 'kryj', 'pro_web', 'lamp',
                      'teraz', 'pinů', 'sklo', 'potvrzeni', 'proti', 'viditel', 'metr', 'relax', 'generace',
                      'baterk', 'nářad', 'silnic', 'pbs-test', 'poptávka', 'bestell', 'vstup', 'žil', 'žíl',
                      'kutil', 'objednávku', 'způsobu', 'přátelství', 'platb', 'testing', 'advent', 'kamer', 'free',
                      'fixa', 'usnad'
                      ]

        for sp in spam_words:
            is_chyba = sp.lower() in subj.lower()
            if is_chyba and not hotovo:
                hotovo = True
                mailbox.delete(msg.uid)

        if (hotovo):
            print('deleted: ', subj)

    mailbox.logout()