import csv

result = []
viafarr = []
wdarr = []
# with open('viaf-nkc.txt') as viaf:
#     for v in viaf:
#         viafarr.append(v.strip())

# with open('viaf_s_nkcr_z_wd.csv') as wd:
#     for w in wd:
#         wdarr.append(w.strip())

#print(wdarr)
def intersection(lst1, lst2):
    # Use of hybrid method
    temp = set(lst2)
    lst3 = [value for value in lst1 if value in temp]
    return lst3

# result = []
#for d in viafarr:
#  if (d in wdarr):
#      result.append(d)
nkcr_fin = {}
wd_fin = {}

with open('viaf_s_nkcr_z_wd.csv') as wd2:
    for w in wd2:
        sp = w.strip().split(',')
        try:
            nkcr_fin[sp[1]] = {'nkcr': sp[1],'viaf': '', 'wd': sp[0]}
        except KeyError as e:
            pass

with open('viaf-nkc.txt') as wd:
    for w in wd:
        sp = w.strip().split(';')
        try:
            nkcr_fin[sp[1]]['viaf'] = sp[0]
        except KeyError as e:
            pass


vys = {}
result = intersection(wdarr,viafarr)
print(len(result))

with open('viaf_to_import_v2.csv', 'w') as csvfile:
    fieldnames = ['viaf', 'nkcr', 'wd']
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
    writer.writeheader()
    for v in nkcr_fin:
        d = nkcr_fin[v]
        if (d['viaf'] != ''):
        # d['viaf'] = v
            writer.writerow(d)