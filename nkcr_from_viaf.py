#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Bot to import  National Thesaurus for Author Names ID (P1006) statements from viaf.

See https://www.wikidata.org/wiki/Property:P1006

Viaf contains some broken links so check that the entry at http://data.bibliotheken.nl/ actually exists
and links back to the same viaf item.

"""
import csv

import pywikibot
from pywikibot import pagegenerators
import pywikibot.data.sparql
import requests
import datetime
import re
from logger import Logger

class ViafImportBot:
    """
    Bot to import NTA links from VIAF
    """

    def __init__(self, generator):
        """
        Arguments:
            * generator    - A generator that yields wikidata item objects.
        """
        self.repo = pywikibot.Site().data_repository()
        # self.generator = pagegenerators.PreloadingItemGenerator(generator)

        f = open('viaf_to_import.csv')
        self.reader = csv.DictReader(f, delimiter=',')

        self.viafitem = pywikibot.ItemPage(self.repo, u'Q54919')
        self.logger = Logger(u'nkcr' + u'Fill', 'saved')

    def run(self):
        '''
        Work on all items
        '''
        validlinks = 0
        brokenlinks = 0
        complete = 0

        # for item in self.generator:
        for it in self.reader:
            complete = complete + 1
            if (self.logger.isCompleteFile(it['wd'])):
                if (complete % 200 == 0):
                    pywikibot.output(u'Počítáme (preskočeno) %s' % (complete))
                continue
            item = pywikibot.ItemPage(self.repo,it['wd'])


            if not item.exists():
                self.logger.logComplete(item.title())
                continue

            if item.isRedirectPage():
                item = item.getRedirectTarget()

            data = entity.get()
            claims = data.get('claims')

            if u'P214' in claims:

                pywikibot.output(u'No viaf found, skipping')
                pywikibot.output(u"WD: " + it['wd'])
                continue

            if claims.get('P691'):
                # Already has National Thesaurus for Author Names ID, great!
                self.logger.logComplete(item.title())
                continue

            viafid = claims.get(u'P214')[0].getTarget()

            if not viafid:
                # pywikibot.output(u'Viaf is set to novalue, skipping')
                # continue
                viafid = it['viaf']

            # viafurl = u'http://viaf.org/viaf/%s' % (viafid,)
            # viafurljson = u'%s/justlinks.json' % (viafurl,)
            # try:
            #     viafPage = requests.get(viafurljson)
            # except requests.HTTPError as e:
            #     pywikibot.output('On %s the VIAF link %s returned this HTTPError %s: %s' % (
            #     item.title(), viafurljson, e.errno, e.strerror))
            # except requests.exceptions.ConnectionError as e:
            #     pywikibot.output('On %s the VIAF link %s returned this HTTPError %s: %s' % (
            #     item.title(), viafurljson, e.errno, e.strerror))
            #
            # try:
            #     viafPageDataDataObject = viafPage.json()
            # except ValueError:
            #     pywikibot.output(
            #         'On %s the VIAF link %s returned this junk:\n %s' % (item.title(), viafurljson, viafPage.text))
            #     continue
            #
            # if not isinstance(viafPageDataDataObject, dict):
            #     pywikibot.output(
            #         'On %s the VIAF link %s did not return a dict:\n %s' % (item.title(), viafurljson, viafPage.text))
            #     continue

            # if viafPageDataDataObject.get(u'NKC'):
            if it['nkc']:
                # nkcr = viafPageDataDataObject.get(u'NKC')[0]
                nkcr = it['nkc']

                nkcrurl = u'https://aleph.nkp.cz/F/?func=find-c&local_base=aut&ccl_term=ica=%s' % (nkcr,)
                nkcrpage = requests.get(nkcrurl)
                if not nkcrpage.status_code == 200:
                    pywikibot.output(u'Viaf %s points to broken NKC url: %s' % ('', nkcrurl,))
                    brokenlinks = brokenlinks + 1
                    continue

                validviaffound = False

                if nkcr in nkcrpage.text:
                    validviaffound = True
                    pywikibot.output(u'Adding NKČR for Author Names ID %s claim to %s (based on bidirectional viaf<->nkcr links)' % (nkcr, item.title(),))
                    summary = u'based on VIAF %s (with bidirectional viaf<->nkcr links)' % (viafid,)
                else:
                    pass
                if not validviaffound:
                    continue

                newclaim = pywikibot.Claim(self.repo, u'P691')
                newclaim.setTarget(nkcr)

                item.addClaim(newclaim, summary=summary)

                pywikibot.output('Adding new reference claim to %s' % item)

                refurl = pywikibot.Claim(self.repo, u'P214')
                refurl.removeQualifier()
                refurl.setTarget(viafid)


                refsource = pywikibot.Claim(self.repo, u'P248')
                refsource.setTarget(self.viafitem)

                refdate = pywikibot.Claim(self.repo, u'P813')
                today = datetime.datetime.today()
                date = pywikibot.WbTime(year=today.year, month=today.month, day=today.day)
                refdate.setTarget(date)

                newclaim.addSources([refurl, refsource, refdate])
                validlinks = validlinks + 1
                if (complete % 200 == 0):
                    pywikibot.output(u'Počítáme %s' % (complete))
            else:
                if (complete % 200 == 0):
                    pywikibot.output(u'Počítáme %s' % (complete))
            self.logger.logComplete(item.title())

        pywikibot.output(u'I found %s valid links and %s broken links' % (validlinks, brokenlinks,))


def getCurrentNTA():
    '''
    Build a cache so we can easility skip Qid's
    '''
    result = {}

    query = u'SELECT ?item ?id { ?item wdt:P1006 ?id }'
    sq = pywikibot.data.sparql.SparqlQuery()
    queryresult = sq.select(query)

    for resultitem in queryresult:
        qid = resultitem.get('item').replace(u'http://www.wikidata.org/entity/', u'')
        if resultitem.get('artukid'):
            result[qid] = resultitem.get('id')
    pywikibot.output(u'The query "%s" returned %s items with a NTA links' % (query, len(result),))
    return result


def ntaBacklinksGenerator():
    """
    Do a SPARQL query at the NTA to get backlinks to Wikidata
    :return:
    """
    basequery = u"""SELECT ?item ?person {
      SERVICE <http://data.bibliotheken.nl/sparql> {
  SELECT ?item ?person WHERE {
  ?person rdf:type <http://schema.org/Person> .
 ?person owl:sameAs ?item .
 FILTER REGEX(STR(?item), "http://www.wikidata.org/entity/") .
} OFFSET %s
LIMIT %s
      }
  # The URI (wdtn) links don't seem to be fully populated
  #MINUS { ?item wdtn:P1006 ?person } .
  MINUS { ?item wdt:P1006 [] } .
  #MINUS { ?item owl:sameAs ?item2 . ?item2 wdtn:P1006 ?person }
  MINUS { ?item owl:sameAs ?item2 . ?item2 wdt:P1006 [] }
}"""
    repo = pywikibot.Site().data_repository()
    step = 10000
    limit = 122000
    for i in range(0, limit, step):
        query = basequery % (i, limit)
        gen = pagegenerators.WikidataSPARQLPageGenerator(query, site=repo)
        for item in gen:
            # Add filtering
            yield item


def viafDumpGenerator(filename=u'../Downloads/viaf-20180807-links.txt'):
    """
    Use a viaf dump to find Wikidata items to check.
    It will filter out the Wikidata items that already have a link
    :return:
    """
    repo = pywikibot.Site().data_repository()
    currentNTAitems = getCurrentNTA()
    wdregex = re.compile(u'^http\:\/\/viaf\.org\/viaf\/(\d+)\tWKP\|(Q\d+)$')
    ntaregex = re.compile(u'^http\:\/\/viaf\.org\/viaf\/(\d+)\tNTA\|([^\s]+)$')
    currentviaf = None
    currentwikidata = None
    currentnta = None
    with open(filename, 'rb') as f:
        for line in f:
            wdmatch = wdregex.match(line)
            ntamatch = ntaregex.match(line)
            if not (wdmatch or ntamatch):
                continue
            if not currentviaf:
                if wdmatch:
                    currentviaf = wdmatch.group(1)
                    currentwikidata = wdmatch.group(2)
                elif ntamatch:
                    currentviaf = ntamatch.group(1)
                    currentnta = ntamatch.group(2)
            elif currentviaf:
                if wdmatch and currentviaf != wdmatch.group(1):
                    currentviaf = wdmatch.group(1)
                    currentwikidata = wdmatch.group(2)
                    currentnta = None
                elif ntamatch and currentviaf != ntamatch.group(1):
                    currentviaf = ntamatch.group(1)
                    currentwikidata = None
                    currentnta = ntamatch.group(2)
                elif wdmatch and currentviaf == wdmatch.group(1) and currentnta:
                    currentwikidata = wdmatch.group(2)
                    print(u'Viaf: %s, Wikidata: %s, NTA: %s' % (currentviaf, currentwikidata, currentnta))
                    if not currentwikidata in currentNTAitems:
                        yield pywikibot.ItemPage(repo, title=currentwikidata)
                    currentviaf = None
                    currentwikidata = None
                    currentnta = None
                elif ntamatch and currentviaf == ntamatch.group(1) and currentwikidata:
                    currentnta = ntamatch.group(2)
                    print(u'Viaf: %s, Wikidata: %s, NTA: %s' % (currentviaf, currentwikidata, currentnta))
                    if not currentwikidata in currentNTAitems:
                        yield pywikibot.ItemPage(repo, title=currentwikidata)
                    currentviaf = None
                    currentwikidata = None
                    currentnta = None

class ViafNKCRFiller:

    def __init__(self, generator):
        self.repo = pywikibot.Site().data_repository()
        self.not_fill = []
        with open('VIAF-preskocit.csv') as wd:
            for w in wd:
                sp = w.strip().split(';')
                try:
                    self.not_fill.append(sp[0])
                except KeyError as e:
                    pass

        f = open('viaf_to_import_v2.csv')
        self.reader = csv.DictReader(f, delimiter=',')

        self.viafitem = pywikibot.ItemPage(self.repo, u'Q54919')
        self.logger = Logger(u'nkcrfill' + u'Fill', 'saved')

    def run(self):
        '''
        Work on all items
        '''
        validlinks = 0
        brokenlinks = 0
        complete = 0

        # for item in self.generator:
        for it in self.reader:
            complete = complete + 1
            if (self.logger.isCompleteFile(it['wd'])):
                if (complete % 5000 == 0):
                    pywikibot.output(u'Počítáme (preskočeno) %s' % (complete))
                continue
            item = pywikibot.ItemPage(self.repo,it['wd'])


            if not item.exists():
                self.logger.logComplete(item.title())
                continue
            oldTitle = item.title()
            redir = False
            if item.isRedirectPage():
                redir = True
                item = item.getRedirectTarget()

            data = item.get()
            claims = data.get('claims')



            # if u'P691' in claims:
            #
            #     pywikibot.output(u'No NKC found, skipping')
            #     pywikibot.output(u"WD: " + it['wd'])
            #     continue

            # if u'P214' in claims:
            #     # Already has National Thesaurus for Author Names ID, great!
            #     self.logger.logComplete(item.title())
            #     continue

            # viafid = claims.get(u'P214')[0].getTarget()

            # if not viafid:
                # pywikibot.output(u'Viaf is set to novalue, skipping')
                # continue
                # viafid = it['viaf']

            # viafurl = u'http://viaf.org/viaf/%s' % (viafid,)
            # viafurljson = u'%s/justlinks.json' % (viafurl,)
            # try:
            #     viafPage = requests.get(viafurljson)
            # except requests.HTTPError as e:
            #     pywikibot.output('On %s the VIAF link %s returned this HTTPError %s: %s' % (
            #     item.title(), viafurljson, e.errno, e.strerror))
            # except requests.exceptions.ConnectionError as e:
            #     pywikibot.output('On %s the VIAF link %s returned this HTTPError %s: %s' % (
            #     item.title(), viafurljson, e.errno, e.strerror))
            #
            # try:
            #     viafPageDataDataObject = viafPage.json()
            # except ValueError:
            #     pywikibot.output(
            #         'On %s the VIAF link %s returned this junk:\n %s' % (item.title(), viafurljson, viafPage.text))
            #     continue
            #
            # if not isinstance(viafPageDataDataObject, dict):
            #     pywikibot.output(
            #         'On %s the VIAF link %s did not return a dict:\n %s' % (item.title(), viafurljson, viafPage.text))
            #     continue



            # if viafPageDataDataObject.get(u'NKC'):
            if it['viaf']:
                # nkcr = viafPageDataDataObject.get(u'NKC')[0]
                viaf = it['viaf']
                if it['viaf'] in self.not_fill:
                    self.logger.logComplete(item.title())
                    if (redir):
                        self.logger.logComplete(oldTitle)
                    continue
                newclaim = pywikibot.Claim(self.repo, u'P214')
                newclaim.setTarget(viaf)

                summary = "add VIAF by NKC"
                item.addClaim(newclaim, summary=summary)

                # pywikibot.output('Adding new reference claim to %s' % item)
                pywikibot.output('Adding new claim %s' % item)

                refurl = pywikibot.Claim(self.repo, u'P214')
                # refurl.removeQualifier()
                refurl.setTarget(it['viaf'])


                refsource = pywikibot.Claim(self.repo, u'P248')
                refsource.setTarget(self.viafitem)

                refdate = pywikibot.Claim(self.repo, u'P813')
                today = datetime.datetime.today()
                date = pywikibot.WbTime(year=today.year, month=today.month, day=today.day)
                refdate.setTarget(date)

                newclaim.addSources([refurl, refsource, refdate])
                validlinks = validlinks + 1
                if (complete % 50 == 0):
                    pywikibot.output(u'Počítáme %s' % (complete))
            else:
                if (complete % 50 == 0):
                    pywikibot.output(u'Počítáme %s' % (complete))
            self.logger.logComplete(item.title())
            if (redir):
                self.logger.logComplete(oldTitle)

        pywikibot.output(u'I found %s valid links and %s broken links' % (validlinks, brokenlinks,))


def main():
    repo = pywikibot.Site().data_repository()
    query = u"""SELECT ?item WHERE {
  ?item wdt:P214 ?viafid .

  ?item wdt:P31 wd:Q5 .
  MINUS { ?item wdt:P691 [] } .

  } LIMIT 400000"""


    query = u'''
SELECT ?item WHERE {
  ?item wdt:P214 ?viafid .
{ ?item wdt:P27 wd:Q183 } UNION { ?item wdt:P27 wd:Q40 } UNION { ?item wdt:P27 wd:Q39 } .

  ?item wdt:P31 wd:Q5 .
  MINUS { ?item wdt:P691 [] } .

  }
  order by ?item
  LIMIT 400000
'''
    # This query will get all the Qid's for which NTA has a link, but the Qid doesn't have a link
    # The commented out lines will also make mismatched links visible. Too much for this bot now.

# { ?item wdt:P27 wd:Q213 } UNION { ?item wdt:P27 wd:Q33946 } UNION { ?item wdt:P27 wd:Q214 } .

#     query = u"""SELECT ?item ?person {
#       SERVICE <http://data.bibliotheken.nl/sparql> {
#   SELECT ?item ?person WHERE {
#   ?person rdf:type <http://schema.org/Person> .
#  ?person owl:sameAs ?item .
#  FILTER REGEX(STR(?item), "http://www.wikidata.org/entity/") .
# }
#       }
#   # The URI (wdtn) links don't seem to be fully populated
#   #MINUS { ?item wdtn:P1006 ?person } .
#   MINUS { ?item wdt:P1006 [] } .
#   #MINUS { ?item owl:sameAs ?item2 . ?item2 wdtn:P1006 ?person }
#   MINUS { ?item owl:sameAs ?item2 . ?item2 wdt:P1006 [] }
# }"""



    # generator = pagegenerators.PreloadingItemGenerator(ntaBacklinksGenerator())
    # generator = pagegenerators.PreloadingItemGenerator(pagegenerators.WikidataSPARQLPageGenerator(query, site=repo))
    generator = None

    # viafImportBot = ViafImportBot(generator)
    # viafImportBot.run()

    nkcr_filler = ViafNKCRFiller(generator)
    nkcr_filler.run()

if __name__ == "__main__":
    main()