import csv
import json
import sys
from typing import Dict, List, NoReturn, Any
from typing import Union
from urllib.request import Request, urlopen

from bs4 import BeautifulSoup
from pywikibot.data import sparql

import pywikibot

import os.path
from os import path

class dp_file:
    categories: List[str] = []

    description: str = ''

    licence: str = ''

    sdc: List[Dict] = []

    source_url:str = ''

    file_name:str = ''

    file_page:pywikibot.FilePage

    commons_file_name: str = ''

    template = '''{{{{Information
|Description=
{{{{en|{en_description}.}}}}
{{{{cs|{cs_description}.}}}}
|Source={source}
|Date={date_y_m_d}
|Author={author}
}}}}'''

    commons: pywikibot.site.APISite

    drobne_pamatky_id: str = ''

    def __init__(self, site:pywikibot.site.APISite) -> NoReturn:
        self.commons = site

    def add_category(self, category_name:str) -> NoReturn:
        if (len(category_name) > 0):
            self.categories.append(category_name)

    def reset_categories(self):
        self.categories = []

    def set_description(self, data:Dict) -> NoReturn:
        self.description = self.template.format(**data)

    def set_licence(self, licence:str) -> NoReturn:
        self.licence = licence

    def add_structured_data(self, property:str, value:str) -> NoReturn:
        sdc = {
            property: value
        }
        self.sdc.append(sdc)

    def get_categories_to_content(self) -> str:
        category_text = ''
        for category in self.categories:
            category_text = category_text + '\n[[Category:' + category + ']]'
        return category_text

    def get_content(self) -> str:
        content = '''
        
== {{{{int:filedesc}}}} ==
{description}
{{{{Import Drobnepamatky.cz}}}}
== {{{{int:license-header}}}} ==
{{{{self|{licence}}}}}
        
        {categories}
        '''

        return content.format(description=self.description,licence=self.licence, categories=self.get_categories_to_content())

    def set_source_url(self, source_url:str) -> NoReturn:
        # https://www.drobnepamatky.cz/files/2020/okres-vyskov-obec-slavkov-u-brna-ku-slavkov-u-brna_62.jpg
        import re

        regex = "https://www.drobnepamatky.cz/files/(\d{4})/(.*)"

        matches = re.match(regex, source_url)

        gr = matches.groups()

        self.file_name = gr[1]
        self.source_url = source_url

    def set_drobne_pamatky_id(self, drobne_pamatky_id:str) -> NoReturn:
        self.drobne_pamatky_id = drobne_pamatky_id

    def set_commons_file_name(self, commons_file_name) -> NoReturn:
        if (self.file_name.find('jpg')):
            self.commons_file_name = commons_file_name + '.jpg'
        else:
            self.commons_file_name = commons_file_name + '.png'

        self.commons_file_name = self.commons_file_name.replace('/', ' ')
        self.file_page = pywikibot.FilePage(self.commons, self.commons_file_name)

    def upload(self, year):
        source = 'drobne_pamatky/' + year + '/' + self.file_name
        self.commons.upload(
            filepage=self.file_page,
            # source_url=self.source_url,
            source_filename=source,
            comment='upload Drobné Památky file',
            watch=True,
            text=self.get_content(),
            report_success=True
        )



class drobne_pamatky:

    site : pywikibot.site.APISite = None

    token: str = ''

    def __init__(self):
        print('drobne pamatky uploader')
        self.site = pywikibot.Site('commons', 'commons')
        self.repo = self.site.data_repository()



        loginman = pywikibot.site.api.LoginManager(user='Frettiebot',
                                                   password="shapes@6gms9u8rlkl512ui8se2hspg5j93hqpc", site=self.site)
        loginman.login()

        if (self.token == ''):
            tokenWallet = pywikibot.site.TokenWallet(site=self.site)
            self.token = tokenWallet['csrf']
        # item = pywikibot.Page(source=self.site, title='File:Presentation of Třebíč at Regiontour 2010.jpg')
        # it = pywikibot.ItemPage(title='M9789438',site=self.repo)
        # print(it.title())
        # self.create_file()
        self.load_files(year = '2021')
        # self.test()
        # self.test_node(2426)
        # self.load_list()

    def get_data(self):
        pass

    def load_list(self):
        last_page = 789
        first_page = 640
        with open('drobne_pamatky_img.csv', 'a', newline='') as csvfile:
            writer = csv.writer(csvfile, delimiter=";", quotechar="'", quoting=csv.QUOTE_ALL)
            writer.writerow(
                ['name'] + ['author'] + ['city'] +
                ['district'] + ['node'] + ['titly_img_autor'] +
                ['titly_img_title'] + ['img_title'] + ['img_alt'] +
                ['img_src']
            )
        for page in range(first_page, last_page+1):
            url = "https://www.drobnepamatky.cz/vsechny-pamatky?page=" + str(page)
            print(url)
            req = Request(url)
            req.add_header('Accept-Encoding', '')
            resp = urlopen(req)
            # print resp.read()
            import gzip

            if resp.info().get('Content-Encoding') == 'gzip':
                # buf = StringIO(resp.read())
                content = gzip.decompress(resp.read())
                decomp_req = content.splitlines()
                # for line in decomp_req:
                #     print(line.decode('utf-8'))
                # data = file.read()
            else:
                content = resp.read()

            # from pywikibot.data import sparql
            # parser = etree.HTMLParser(remove_comments=True, remove_blank_text=True, strip_cdata=True)
            #  zparsuje HTML a vytvoří XML DOM
            contnew = content.decode('utf-8')

            soup = BeautifulSoup(contnew, 'html.parser')
            [x.extract() for x in soup.findAll('script')]
            prett = soup.prettify()
            ##content > div > div > div.view-content > table > tbody > tr.odd.views-row-first
            rows = soup.select('.field-content > a')
            count = 0
            for row in rows:
                count = count + 1
                node = int(row['href'].strip().replace('/node/',''))
                self.load_detail(node)


    def load_detail(self, node_id):
        url = 'https://www.drobnepamatky.cz/node/' + str(node_id)

        req = Request(url)
        req.add_header('Accept-Encoding', '')
        resp = urlopen(req)
        # print resp.read()
        import gzip

        if resp.info().get('Content-Encoding') == 'gzip':
            # buf = StringIO(resp.read())
            content = gzip.decompress(resp.read())
            decomp_req = content.splitlines()
            # for line in decomp_req:
            #     print(line.decode('utf-8'))
            # data = file.read()
        else:
            content = resp.read()

        # from pywikibot.data import sparql
        # parser = etree.HTMLParser(remove_comments=True, remove_blank_text=True, strip_cdata=True)
        #  zparsuje HTML a vytvoří XML DOM
        contnew = content.decode('utf-8')

        soup = BeautifulSoup(contnew, 'html.parser')
        [x.extract() for x in soup.findAll('script')]
        prett = soup.prettify()
        nazev = soup.select('#nadpis')
        autor = soup.select('.field-field-com-vlozil > div > div')
        obec = soup.select('.field-field-com-obec > div > div > a')
        okres = soup.select('.field-field-com-okres > div > div > a')
        pictures = soup.select('.field-field-obrazek > div > div')
        pics_to_save = []
        for picture in pictures:

            picture_main_href = picture.select('div > a')
            img_href = str(picture_main_href[0]['href']).strip()
            picture_main_img = picture.select('div> a > img')
            img_alt = str(picture_main_img[0]['alt']).strip()
            img_title = str(picture_main_img[0]['title']).strip()
            titly = picture.select('div > div.titly > div')
            pic_to_save = {}
            if (len(titly) > 0):
                titly_img_title = titly[0].string
                # print("titulek_z_titly: " + titly_img_title)
                pic_to_save['titly_img_title'] = titly_img_title
                try:
                    titly_img_autor = titly[1].span.string.replace('Autor fotky: ', "")

                except IndexError as e:
                    titly_img_autor = ''
                pic_to_save['titly_img_autor'] = titly_img_autor
                # print("autor_z_titly: " + titly_img_autor)
            else:
                pic_to_save['titly_img_autor'] = ''
                pic_to_save['titly_img_title'] = ''
            # title = titly.select('.imgtitle')
            # img_autor = titly.select('.imgautor > span')

            # print("orig_src_img: " + img_href)
            pic_to_save['img_src'] = img_href
            # print("alt_img: " + img_alt)
            pic_to_save['img_alt'] = img_alt
            # print("title_img: " + img_title)
            pic_to_save['img_title'] = img_title
            pics_to_save.append(pic_to_save)




        nazev_clear = str(nazev[0].contents[0]).strip()
        autor_clear = str(autor[0].contents[2]).strip()
        obec_clear = str(obec[0].string).strip()
        okres_clear = str(okres[0].string).strip()

        # print("Nazev: " + nazev_clear)
        # print("Autor: " + autor_clear)

        # print("Obec: " + obec_clear)
        # print("Okres: " + okres_clear)
        for pic in pics_to_save:
            csv_row = {
                'name' : nazev_clear,
                'author' : autor_clear,
                'city' : obec_clear,
                'district' : okres_clear,
                'node' : str(node_id),
                'titly_img_autor' : pic['titly_img_autor'],
                'titly_img_title' : pic['titly_img_title'],
                'img_title' : pic['img_title'],
                'img_alt' : pic['img_alt'],
                'img_src' : pic['img_src'],
            }

            with open('drobne_pamatky_img.csv', 'a', newline='') as csvfile:
                writer = csv.writer(csvfile, delimiter=";", quotechar="'", quoting=csv.QUOTE_ALL)

                writer.writerow(
                    [csv_row['name']] + [csv_row['author']] + [csv_row['city']] +
                    [csv_row['district']] + [csv_row['node']] + [csv_row['titly_img_autor']] +
                    [csv_row['titly_img_title']] + [csv_row['img_title']] + [csv_row['img_alt']] +
                    [csv_row['img_src']]
                )

    def test(self):
        filename = 'File:Ryb závody 5.jpg'
        page = pywikibot.page.Page(self.site, filename)
        pageId = page.pageid
        self.add_sdc(filename)

    def test_node(self, node_id):
        self.load_detail(node_id)


    def add_sdc(self, filename, qid):
        """
        {
	"action": "wbeditentity",
	"format": "json",
	"site": "commonswiki",
	"title": "File:Ryb závody 5.jpg",
	"token": "9e9703e80a567d501a5699eb3bb53ff85f10588c+\\",
	"data": "{\"claims\":[{\"mainsnak\":{\"snaktype\":\"value\",\"property\":\"P180\",\"datavalue\":{\"value\":{\"entity-type\":\"item\",\"id\":\"Q134307\"},\"type\":\"wikibase-entityid\"}},\"type\":\"statement\",\"rank\":\"normal\"}]}"
}
        """
        data = {
            'claims' : [
                {
                    "mainsnak" : {
                        "snaktype" : "value",
                        "property" : "P180",
                        "datavalue" : {
                            "value" : {
                                "entity-type" : "item",
                                "id" : qid,
                            },
                            "type" : "wikibase-entityid"
                        }
                    },
                    "type" : "statement",
                    "rank" : "normal"
                }
            ]
        }
        datajson = json.dumps(data)

        params = {
            'action': 'wbeditentity',
            'format': 'json',
            'site': 'commonswiki',
            'title': 'File:' + filename,
            'data': datajson
        }

        # logintok = loginman.get_login_token()



        params['token'] = self.token
        res = pywikibot.site.api.Request(parameters=params, site=self.site)
        res.submit()
        print('sdc added File:' + filename)

    def load_files(self, year):
        with open('drobne_pamatky_img2_frettiemu2.csv', 'r', newline='\n') as csvfile:
            reader = csv.reader(csvfile, dialect='category_dialect')
            line = 0
            for row in reader:
                line = line + 1

                if line <= 1:
                    continue
                if row[15].find(year) > 0:
                    file_to_upload = dp_file(self.site)
                    # drobne_pamatky_id = '12345'
                    author = row[1]
                    data = dict(
                        en_description=row[4],
                        cs_description=row[3],
                        source="https://www.drobnepamatky.cz/node/" +row[2],
                        date_y_m_d="{{Other date|?}}",
                        author=author,
                    )

                    file_to_upload.set_commons_file_name(row[0])

                    file_to_upload.set_description(data)

                    file_to_upload.set_licence('cc-0')

                    file_to_upload.add_structured_data('P180', row[14])
                    # file_to_upload.add_structured_data('P179', 'Q1234567')
                    # file_to_upload.add_structured_data('P190', 'Q1234567')
                    file_to_upload.reset_categories()
                    file_to_upload.add_category(row[5])
                    file_to_upload.add_category(row[6])
                    file_to_upload.add_category(row[7])
                    file_to_upload.add_category(row[8])
                    file_to_upload.add_category(row[9])
                    file_to_upload.add_category(row[10])
                    file_to_upload.add_category(row[11])
                    file_to_upload.add_category(row[12])
                    file_to_upload.add_category(row[13])
                    cwd = os.getcwd()
                    file_to_upload.set_source_url(row[15])
                    source = cwd + '/drobne_pamatky/' + year + '/' + file_to_upload.file_name
                    if path.exists(source):
                        try:
                            file_to_upload.upload(year)
                            self.add_sdc(file_to_upload.commons_file_name, row[14])
                            # print(file_to_upload.get_content())
                            print('uploaded File:' + file_to_upload.commons_file_name)
                            os.remove(source)
                        except pywikibot.data.api.UploadWarning as e:
                            exc = str(e.message)
                            f = exc.find('bad')
                            if exc.find('exists') > 0:
                                os.remove(source)
                            if exc.find('bad') > 0:
                                print(file_to_upload.commons_file_name)
                            if exc.find('duplicate') > 0:
                                print(file_to_upload.commons_file_name + ';' + e.message.replace("Uploaded file is a duplicate of ['", '').replace("'].", ''))
                            # print(e.message)

                    else:
                        pass
                        # print('not exists' + file_to_upload.file_name)



    def create_file(self):
        pass

    def upload_file(self):
        pass

class wikidata_down_drobne:

    def download(self):

        with open('drobne_pamatky_z_wd.csv', 'w', newline='') as csvfile:
            writer = csv.writer(csvfile, delimiter=";", quotechar="'", quoting=csv.QUOTE_ALL)
            writer.writerow(
                ['item'] + ['label'] + ['lat'] + ['lon']
            )

        arr = range(250)
        for num in arr:
            limit = 200
            offset = (num) * limit
            query = '''
            select distinct ?item ?itemLabel ?Coord where {
    
      values ?instance {
      wd:Q1746392
      wd:Q108325
      wd:Q4989906
      wd:Q200334
      wd:Q3148886
      wd:Q4817
      wd:Q961082
      }
    
      
      {?item wdt:P6736 [] .} union {?item wdt:P31/wdt:P279* ?instance .}
      ?item wdt:P17 wd:Q213 .
      ?item wdt:P625 ?Coord .
                      
    SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],cs". }
      
    } LIMIT ''' + str(limit) + ''' OFFSET ''' + str(offset)

            query_object = sparql.SparqlQuery()
            data = query_object.select(query, full_data=True)



            for line in data:
                qid = str(line['item']).replace('http://www.wikidata.org/entity/', '')
                label = str(line['itemLabel'])
                coord = str(line['Coord']).replace('Point(', '').replace(')','').split(' ')

                with open('drobne_pamatky_z_wd.csv', 'a', newline='') as csvfile:
                    writer = csv.writer(csvfile, delimiter=";", quotechar="'", quoting=csv.QUOTE_ALL)

                    writer.writerow(
                        [qid] + [label] + [coord[0]] + [coord[1]]
                    )

class category_dialect(csv.Dialect):
    """Describe the usual properties of Excel-generated CSV files."""
    delimiter = ';'
    quotechar = '"'
    doublequote = True
    skipinitialspace = False
    lineterminator = '\r\n'
    quoting = csv.QUOTE_MINIMAL

csv.register_dialect("category_dialect", category_dialect)

class categories:

    def __init__(self):
        print("categories run")

    def create_category(self, name, content):

        site = pywikibot.Site('commons', 'commons')
        category = pywikibot.Category(
            site, name)

        category.text = content
        category.save()

    def load(self):
        with open('nove-kategorie.csv', 'r', newline='\n') as csvfile:
            reader = csv.reader(csvfile, dialect='category_dialect')
            line = 0
            for row in reader:
                line = line + 1
                if line == 1:
                    continue
                name = row[0]
                content = row[1].replace('\\n','\n')
                self.create_category(name, content)


class uploaded:

    def getUploads(self, user):

        names_from_commons = []



        site = pywikibot.Site('commons', 'commons')
        data = site.logevents(logtype='upload', user=user, reverse=False)

        for file in data:

            title = file.data['title']
            names_from_commons.append(title.lower())
            # timestamp = file.data['timestamp']
            # comment = file.data['comment']

        with open('drobne_pamatky_img2_frettiemu2.csv', 'r', newline='\n') as csvfile:
            reader = csv.reader(csvfile, dialect='category_dialect')
            line = 0
            for row in reader:
                name = 'File:' + row[0].replace('/',' ').replace('  ', ' ') + '.jpg'

                if (name.lower() not in names_from_commons):

                    if (row[15].find('2014') > 0):
                        line = line + 1
                        print('wget ' + row[15])
                    if (line % 50 == 0):
                        print('sleep 2')

            print('------')

            with open('drobne_pamatky_img2_frettiemu2.csv', 'r', newline='\n') as csvfile:
                reader = csv.reader(csvfile, dialect='category_dialect')
                line = 0
                for row in reader:
                    name = 'File:' + row[0].replace('/', ' ').replace('  ', ' ') + '.jpg'

                    if (name.lower() not in names_from_commons):

                        if (row[15].find('2015') > 0):
                            line = line + 1
                            print('wget ' + row[15])
                        if (line % 50 == 0):
                            print('sleep 2')

            print('------')

            with open('drobne_pamatky_img2_frettiemu2.csv', 'r', newline='\n') as csvfile:
                reader = csv.reader(csvfile, dialect='category_dialect')
                line = 0
                for row in reader:
                    name = 'File:' + row[0].replace('/', ' ').replace('  ', ' ') + '.jpg'

                    if (name.lower() not in names_from_commons):

                        if (row[15].find('2016') > 0):
                            line = line + 1
                            print('wget ' + row[15])
                        if (line % 50 == 0):
                            print('sleep 2')

            print('------')

            with open('drobne_pamatky_img2_frettiemu2.csv', 'r', newline='\n') as csvfile:
                reader = csv.reader(csvfile, dialect='category_dialect')
                line = 0
                for row in reader:
                    name = 'File:' + row[0].replace('/', ' ').replace('  ', ' ') + '.jpg'

                    if (name.lower() not in names_from_commons):

                        if (row[15].find('2017') > 0):
                            line = line + 1
                            print('wget ' + row[15])
                        if (line % 50 == 0):
                            print('sleep 2')

            print('------')

            with open('drobne_pamatky_img2_frettiemu2.csv', 'r', newline='\n') as csvfile:
                reader = csv.reader(csvfile, dialect='category_dialect')
                line = 0
                for row in reader:
                    name = 'File:' + row[0].replace('/', ' ').replace('  ', ' ') + '.jpg'

                    if (name.lower() not in names_from_commons):

                        if (row[15].find('2018') > 0):
                            line = line + 1
                            print('wget ' + row[15])
                        if (line % 50 == 0):
                            print('sleep 2')

            print('------')

            with open('drobne_pamatky_img2_frettiemu2.csv', 'r', newline='\n') as csvfile:
                reader = csv.reader(csvfile, dialect='category_dialect')
                line = 0
                for row in reader:
                    name = 'File:' + row[0].replace('/', ' ').replace('  ', ' ') + '.jpg'

                    if (name.lower() not in names_from_commons):

                        if (row[15].find('2019') > 0):
                            line = line + 1
                            print('wget ' + row[15])
                        if (line % 50 == 0):
                            print('sleep 2')

                print('------')

                with open('drobne_pamatky_img2_frettiemu2.csv', 'r', newline='\n') as csvfile:
                    reader = csv.reader(csvfile, dialect='category_dialect')
                    line = 0
                    for row in reader:
                        name = 'File:' + row[0].replace('/', ' ').replace('  ', ' ') + '.jpg'

                        if (name.lower() not in names_from_commons):

                            if (row[15].find('2020') > 0):
                                line = line + 1
                                print('wget ' + row[15])
                            if (line % 50 == 0):
                                print('sleep 2')

                print('------')

                with open('drobne_pamatky_img2_frettiemu2.csv', 'r', newline='\n') as csvfile:
                    reader = csv.reader(csvfile, dialect='category_dialect')
                    line = 0
                    for row in reader:
                        name = 'File:' + row[0].replace('/', ' ').replace('  ', ' ') + '.jpg'

                        if (name.lower() not in names_from_commons):

                            if (row[15].find('2021') > 0):
                                line = line + 1
                                print('wget ' + row[15])
                            if (line % 50 == 0):
                                print('sleep 2')
            # print(line)








# down = wikidata_down_drobne()
# down.download()
uploader = drobne_pamatky()

# uploads = uploaded()
# uploads.getUploads('Frettiebot')


# dpl = drobne_pamatky()
# dpl.load_list()

# cat = categories()
# cat.load()