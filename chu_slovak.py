#!/usr/bin/python
# -*- coding: utf-8 -*-
import pywikibot
import sys
import re
import csv
from pywikibot import i18n, textlib, pagegenerators

class excel_semicolon(csv.Dialect):
    """Describe the usual properties of Excel-generated CSV files."""
    delimiter = ';'
    quotechar = '"'
    doublequote = True
    skipinitialspace = False
    lineterminator = '\r\n'
    quoting = csv.QUOTE_MINIMAL


class Slovak:

    def __init__(self):
        self.result = []
        self.counties = {}
        #self.loadFile()
        #self.run()
        #self.loadFileForTable()
        #self.runTable()
        self.loadNotWantedPictures()
        self.getPagesByTemplate()

    def loadNotWantedPictures(self):
        self.notWantedPictures = []
        self.notWantedPictures.append('Commons-logo.svg')
        self.notWantedPictures.append('Flag of Slovakia.svg')
        self.notWantedPictures.append('Red pog.png')
        self.notWantedPictures.append('Slovakia location map.svg')
        self.notWantedPictures.append('Wiki letter w.svg')
        self.notWantedPictures.append('Canada location map.svg')
        self.notWantedPictures.append('Czech Republic location map.svg')
        self.notWantedPictures.append('Green pog.svg')
        self.notWantedPictures.append('Flag of Canada.svg')
        self.notWantedPictures.append('Flag of Germany.svg')
        self.notWantedPictures.append('Flag of the Czech Republic.svg')
        self.notWantedPictures.append('Flag of the United States.svg')
        self.notWantedPictures.append('Flag of the United States.svg')
        self.notWantedPictures.append('Mergefrom.svg')
        self.notWantedPictures.append('Template-info.svg')
        self.notWantedPictures.append('Brno (znak).svg')
        self.notWantedPictures.append('Birthdaycake 1 year.JPG')
        self.notWantedPictures.append('Barnstar - protected areas in the Czech Republic.png')
        self.notWantedPictures.append('Barnstar - protected areas in the Czech Republic Hires.svg')
        self.notWantedPictures.append('Yellowstone Nationalpark3.jpg')
        self.notWantedPictures.append('Usa edcp relief location map.png')
        self.notWantedPictures.append('CR-okresy-a-kraje-2007.svg')
        self.notWantedPictures.append('Do you know you can edit Wikipedia.jpg')
        self.notWantedPictures.append('Template doc page.svg')
        self.notWantedPictures.append('Yes check.svg')
        self.notWantedPictures.append('Fire.svg')
        self.notWantedPictures.append('Stripe-marked trail yellow.svg')
        self.notWantedPictures.append('Stripe-marked trail red.svg')
        self.notWantedPictures.append('Stripe-marked trail green.svg')
        self.notWantedPictures.append('Stripe-marked trail blue.svg')
        self.notWantedPictures.append('Relief Map of Slovakia 2.png')
        self.notWantedPictures.append('Red Fire.svg')
        self.notWantedPictures.append('Szlak żółty.svg')
        self.notWantedPictures.append('Crystal Clear action loopnone.png')
        self.notWantedPictures.append('Nuvola apps bookcase.png')
        self.notWantedPictures.append('WikiProjekt chranena uzemi - reklama.png')
        self.notWantedPictures.append('Žamberk-dvě-lípy-velkolisté-památné-stromy2008.jpg')
        self.notWantedPictures.append('Viteze.gif')
        self.notWantedPictures.append('Flag of Poland.svg')
        self.notWantedPictures.append('Flag of Ukraine.svg')
        self.notWantedPictures.append('Deleatur.svg')
        self.notWantedPictures.append('Icon - upload photo 2.svg')
        self.notWantedPictures.append('Image manquante 2.svg')
        self.notWantedPictures.append('Flag of Hungary.svg')
        self.notWantedPictures.append('Szlak czerwony.svg')
        self.notWantedPictures.append('Szlak niebieski.svg')
        self.notWantedPictures.append('Szlak zielony.svg')
        self.notWantedPictures.append('Szlak żółty.svg')
        self.notWantedPictures.append('Merge-arrow.svg')
        self.notWantedPictures.append('Relief Map of Czech Republic.png')

    def getPagesByTemplate(self):
        site = pywikibot.Site('cs','wikipedia')
        page = pywikibot.Page(site, u'Template:Infobox chráněné území na Slovensku')
        generator = pagegenerators.ReferringPageGenerator(page)
        completelist = []
        count = 0
        for article in generator:
            count += 1
            #print article.title()
            cats = u''
            pageInfoList = dict()
            pageInfoList['name'] = article.title()
            counties = []
            if count < 1500:
                for cat in article.categories():
                    #print cat.title()

                    match = re.match(u'Kategorie:Chráněná území v okrese (.*)',cat.title())
                    if match:
                        cats = cats + u' ' + match.group(1)
                        counties.append(match.group(1))

                pageInfoList['counties'] = cats
                commonscat = u''
                for tpl in article.templatesWithParams():

                    if (tpl[0].title() == u'Šablona:Commonscat'):
                        if len(tpl[1]):
                            commonscat = tpl[1][0]
                        else:
                            commonscat = article.title()

                pageInfoList['commonscat'] = commonscat

                images = u''
                for img in article.imagelinks():
                    #print img.title()

                    match = re.match('Soubor:(.*)',img.title())
                    if not (match.group(1) in self.notWantedPictures):
                        images = images + u' [[:Soubor:' + match.group(1) + u']]'

                pageInfoList['images'] = images

                for c in counties:
                    pageInfoList['county'] = c
                    completelist.append(pageInfoList)


                #print completelist
        text = u"""Seznam obrázků do seznamu.
{| class="wikitable sortable"
|-
! Název !! Okres !! Obrázky !! Commonscat !! Stav"""
        for cl in completelist:
            text = text + u"""
            |-
            | [[""" + cl['name'] + u"""]] || [[Seznam chráněných území v okrese """ + cl['county'] + u"""| """ + cl['county'] + u""" ]] || """ + cl['images'] + u""" || """ + cl['commonscat'] + u""" || {{Nevyřešeno}}
            """

        text = text + u"""
|}
        """

        mypage = pywikibot.Page(site,'Wikipedista:Frettie/ListCHUSK')
        mypage.text = text
        mypage.save()




    def loadFile(self):
        csv.register_dialect("excel_semicolon", excel_semicolon)
        reader = csv.DictReader(open('Zoznam_MCHU.csv'), dialect='excel_semicolon')

        for row in reader:
            self.result.append(row)

    def loadFileForTable(self):
        csv.register_dialect("excel_semicolon", excel_semicolon)
        reader = csv.DictReader(open('Zoznam_MCHU.csv'), dialect='excel_semicolon')

        for row in reader:
            self.result.append(row)

    def getCounties(self):
        for line in self.result:
            county = self.getValue(line,'okres')
            moreCounties = re.search(',', county, flags=re.IGNORECASE)
            if (not moreCounties):
                self.counties[county] = []

    def setToCounty(self, county, line):
        try:
            self.counties[county].append(line)
        except KeyError:
            self.counties[county] = []
            self.counties[county].append(line)

    def createTableArticle(self, countyName, countyTable):
        text = u"""Toto je '''seznam chráněných území v okrese """ + countyName + u"""''' aktuální k roku 2015, ve kterém jsou uvedena chráněná území v oblasti [[Okres """ + countyName + u"""|okresu """ + countyName + u"""]].
{| class="wikitable sortable"
|-
! Kód !! class="unsortable" style="width:100px;"|Obrázek!! Typ !! Název !! Rozloha ([[Hektar|ha]]) !! class="unsortable" | Galerie <br>Commons !! Popis území !! Souřadnice

        """
        for line in countyTable:
            line = u"""
|-
| {{Šzochčpsr|""" + self.getValue(line,'cislo') + u"""}} || <!--[[Soubor:???|100x100px|Popis obrazku]]--> || [[""" + self.translateCategory(line, False) + u"""|""" + self.getValue(line,'kategorie') +u"""]] || [[""" + self.getValue(line,'nazev') + u"""]] || {{nts|""" + self.getValue(line,'rozloha_hektary').replace(',','.') + u"""}} || <!--{{Commonscat v tabulce|???}}--> || <!-- -->
 || <!--{{Souřadnice|49|15}}-->"""
            text = text + line

        text = text + u"""
|}

== Reference ==

* Data v tabulce byla převzata z databáze [http://www.sopsr.sk/web/index.php?cl=114 ŠOPSR].

[[Kategorie:Seznamy chráněných území na Slovensku podle okresu|""" + countyName + u"""]]
[[Kategorie:Chráněná území v okrese """ + countyName + u"""|""" + countyName + u"""]]
        """
        site = pywikibot.Site('cs','wikipedia')
        try:
            page = pywikibot.Page(site, u'Seznam chráněných území v okrese ' + countyName)
            if not page.exists():
                page.text = text
                print u'Seznam chráněných území v okrese ' + countyName
                #page.save(u'Nový článek dle dat z http://www.sopsr.sk/web/index.php?cl=114', None, False, True)

        except pywikibot.NoPage, e:
            print e
        except Exception, e:
            print e

    def prepareCountiesTable(self, line):
        county = self.getValue(line, 'okres')
        moreCounties = re.search(',', county, flags=re.IGNORECASE)
        if (moreCounties):
            counties = county.split(',')
            for c in counties:
                self.setToCounty(c.strip(),line)
        else:
            self.setToCounty(county,line)

    def runTable(self):
        self.getCounties()
        for line in self.result:
            self.prepareCountiesTable(line)
        for countyName,countyTable in self.counties.items():
            self.createTableArticle(countyName, countyTable)




    def translateCategory(self, row, lower = True, second = False):
        categories = {}
        categories['NPR'] = u'Národní přírodní rezervace'
        categories['PR'] = u'Přírodní rezervace'
        categories['PP'] = u'Přírodní památka'
        categories['NPP'] = u'Národní přírodní památka'
        categories['CHA'] = u'Chráněný areál'
        categories['CHKP'] = u'Chráněný krajinný prvek'

        if (second):
            categories = {}
            categories['NPR'] = u'Národní přírodní rezervace'
            categories['PR'] = u'Přírodní rezervace'
            categories['PP'] = u'Přírodní památky'
            categories['NPP'] = u'Národní přírodní památky'
            categories['CHA'] = u'Chráněné areály'
            categories['CHKP'] = u'Chráněné krajinné prvky'

        if (row['kategorie'] in categories):
            if (lower):
                return categories[row['kategorie']].lower()
            else:
                return categories[row['kategorie']]

    def translateOffice(self, row):
        offices = {}
        offices['Ponitrie'] = u'Ponitří'
        offices['MaléKarpaty'] = u'Malé Karpaty'
        offices['BieleKarpaty'] = u'Bílé Karpaty'
        offices['VeľkáFatra'] = u'Velká Fatra'
        if (self.getValue(row,'pracoviste').replace(' ', '') in offices):
            return offices[self.getValue(row,'pracoviste').replace(' ', '')]
        else:
            return self.getValue(row, 'pracoviste')

    def getRegion(self, row, typ, link = True, append = ''):
        regions = {}
        regions['BB'] = {'nazev' : u'Banskobystrický', 'sesty' : u'Banskobystrickém', 'link' : u'Banskobystrický kraj'}
        regions['BA'] = {'nazev' : u'Bratislavský', 'sesty' : u'Bratislavském', 'link' : u'Bratislavský kraj'}
        regions['KE'] = {'nazev' : u'Košický', 'sesty' : u'Košickém', 'link' : u'Košický kraj'}
        regions['NR'] = {'nazev' : u'Nitranský', 'sesty' : u'Nitranském', 'link' : u'Nitranský kraj'}
        regions['PO'] = {'nazev' : u'Prešovský', 'sesty' : u'Prešovském', 'link' : u'Prešovský kraj'}
        regions['TN'] = {'nazev' : u'Trenčínský', 'sesty' : u'Trenčínském', 'link' : u'Trančínský kraj'}
        regions['TR'] = {'nazev' : u'Trenčínský', 'sesty' : u'Trenčínském', 'link' : u'Trenčínský kraj'}
        regions['TT'] = {'nazev' : u'Trnavský', 'sesty' : u'Trnavském', 'link' : u'Trnavský kraj'}
        regions['ZI'] = {'nazev' : u'Žilinský', 'sesty' : u'Žilinském', 'link' : u'Žilinský kraj'}



        if (row['kraj'] in regions):
            if (link):
                return u'[[' + regions[row['kraj']]['link'] + u'|' + regions[row['kraj']][typ] + append + u']]'
            else:
                return regions[row['kraj']][typ] + append
        else:
            moreRegion = re.search(u'-', row['kraj'], flags=re.IGNORECASE)
            if (moreRegion):
                list = row['kraj'].split('-')
                text = u''
                for reg in list:
                    if (link):
                        if (text == ''):
                            text = text + u'[[' + regions[reg]['link'] + u'|' + regions[reg][typ] + append + u']]'
                        else:
                            text = text + u', [[' + regions[reg]['link'] + u'|' + regions[reg][typ] + append + u']]'
                    else:
                        if (text == ''):
                            text = text + regions[reg][typ] + append
                        else:
                            text = text + ', ' + regions[reg][typ] + append

                return text

    def run(self):
        print "run"
        for row in self.result:
            self.prepareRow(row)

    def getOchrannePasmo(self, row):
        if (row['rozloha_ochranneho_pasma'] == 'x'):
            return u'Ochranné pásmo nebylo stanoveno.'
        else:
            return u'Rozloha ochranného pásma byla stanovena na ' + self.getValue(row,'rozloha_ochranneho_pasma') + u' ha.'

    def selfVyhlaseni(self, row):
        moreYears = re.search(',', row['rok_vyhlaseni_novelizace'], flags=re.IGNORECASE)
        if (moreYears):
            return u'Území bylo vyhlášeno či novelizováno v letech ' + self.getValue(row, 'rok_vyhlaseni_novelizace')
        else:
            return u'Území bylo vyhlášeno či novelizováno v roce ' + self.getValue(row, 'rok_vyhlaseni_novelizace')


    def prepareRow(self, row):
        self.translateCategory(row)
        #self.getRegion(row, 'nazev', append=' kraj')
        #self.createInfofox(row)
        #self.translateOffice(row)
        #self.createText(row)
        self.createArticle(row)

    def createInfofox(self, row):
        infobox = u'''
{{Infobox chráněné území na Slovensku
| typ = ''' + unicode(row['kategorie']) + u'''
| název = ''' + self.getCountyNameWithZavorka(row) + u'''
| foto =
| popis =
| vyhlášení = ''' + unicode(row['rok_vyhlaseni_novelizace'], encoding='Windows-1250') + u'''
| vyhlásil =
| kód = ''' + unicode(row['cislo'], encoding='Windows-1250') + u'''
| lokalita = ''' + unicode(row['pracoviste'], encoding='Windows-1250') + u'''
| výměra = ''' + unicode(row['rozloha_hektary'], encoding='Windows-1250') + u''' ha
| okres = ''' + unicode(row['okres'], encoding='Windows-1250') + u'''
| poznámky =
| commons =
| loc-map =
}}'''

        return infobox

    def getValue(self, row, key):
        return unicode(row[key],encoding='Windows-1250').strip()

    def createText(self, row):
        text = u"""
'''""" + self.getCountyNameWithZavorka(row) + u"""''' je """ + self.translateCategory(row) + u""" v oblasti """ + self.translateOffice(row) + u""".

Nachází se v [[okres """ + self.getValue(row,'okres') + u"""|okrese """ + self.getValue(row, 'okres') + u''']] v ''' + self.getRegion(row, 'sesty', append=u' kraji') + u'''. ''' + self.selfVyhlaseni(row) + u''' na rozloze ''' + self.getValue(row,'rozloha_hektary') + u''' ha. ''' + self.getOchrannePasmo(row) + u'''

== Externí odkazy ==
* [http://uzemia.enviroportal.sk/main/detail/cislo/''' + self.getValue(row, 'cislo') + u''' ''' + self.translateCategory(row,False) + u''' ''' + self.getCountyNameWithZavorka(row) + u'''], Štátny zoznam osobitne chránených častí prírody SR
* [http://www.sopsr.sk/web/index.php?cl=16 Chránené územia], Štátna ochrana prírody Slovenskej republiky
'''

        return text

    def addCategory(self):
        print "cat"

    def getCountyNameWithZavorka(self, row):
        countyName = self.getValue(row, 'nazev')
        regex = re.match('(.*) \((.*)\)', countyName, flags=re.IGNORECASE)
        if regex:
            return regex.group(1)
        else:
            return self.getValue(row, 'nazev')

    def createArticle(self, row):
        text = self.createInfofox(row) + u'''
        ''' + self.createText(row)

        #print text
        site = pywikibot.Site('cs','wikipedia')
        try:
            page = pywikibot.Page(site, self.getValue(row, 'nazev'))
            if not page.exists():
                newCatType = self.translateCategory(row,second=True) + u' na Slovensku'
                newCatCounty = u'Chráněná území v okrese ' + self.getValue(row, 'okres')
                newCatRegion = u'Chráněná území v kraji ' + self.getRegion(row,'sesty', False, u' kraji')

                cats = textlib.getCategoryLinks(text)
                cats.append(newCatType)
                cats.append(newCatCounty)
                if (not pywikibot.Category(site, newCatCounty).exists()):
                    self.createCategory(newCatCounty, newCatRegion)
                text = textlib.replaceCategoryLinks(text, cats,site=site)
                page.text = text
                countyName = self.getCountyNameWithZavorka(row)
                print countyName
                page.save(u'Nový článek dle dat z http://www.sopsr.sk/web/index.php?cl=114', None, False, True)

        except pywikibot.NoPage, e:
            print e
        except Exception, e:
            print e

    def createCategory(self, name, parent):
        site = pywikibot.Site('cs', 'wikipedia')
        page = pywikibot.Category(site, name)
        text = page.text
        cats = textlib.getCategoryLinks(text)
        cats.append(parent)
        text = textlib.replaceCategoryLinks(text, cats,site=site)
        page.text = text
        page.save(u'Nová kategorie pro CHU SK', None, False, True)

slovak = Slovak()
