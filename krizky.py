from math import radians, cos, sin, asin, sqrt

def haversine(lon1, lat1, lon2, lat2):
    """
    Calculate the great circle distance between two points
    on the earth (specified in decimal degrees)
    """
    # convert decimal degrees to radians
    lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])
    # haversine formula
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
    c = 2 * asin(sqrt(a))
    km = 6367 * c;
    return km

krizky = []
krizek = {'name' : 'u trebice', 'lat' : 15.920160555, 'lon' : 49.2208575}
krizky.append(krizek)
nove_krizky = []
novy_krizek = {'name' : 'u trebice novy', 'lat' : 15.930160555, 'lon' : 49.2308575}
nove_krizky.append(novy_krizek)
for krizek in krizky:
    distance = haversine(krizek['lon'], krizek['lat'], novy_krizek['lon'], novy_krizek['lat'])
    print(distance)