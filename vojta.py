#!/usr/bin/python
# -*- coding: utf-8 -*-
#importuje knihovnu collections (pro orderedDict)
import collections
#importuje knihovnu pro praci s CSV
import csv
#importuje knihovnu pro praci s casem a datem
import datetime

#importuje pywikibota
import pywikibot

#importuje logger třídu z knihovny Logger
from logger import Logger

#nazev classy - třídy
class HistoricLexicon:
    #základní fce - __init__ nutne pojmenovat takhle
    #self - drzi instanci tridy
    #logger_name - nazev loggeru
    #lze pridat i dalsi
    def __init__(self, logger_name):
        #do tridy da globalne pristupnou promennou errors - {} je dict tj. tam muzes ukladat jako klic slovo
        self.errors = {}

        #vypise "historicLexiconHouses"
        print('historicLexiconHouses')

        #zavede logger, to pouzivam navic, abych vedel, co probehlo a mohl taky preskocit to, co uz je hotovo
        self.logger = Logger(logger_name + u'Fill', 'saved')

        #zavolá metodu/funkci load_file (níže) s parametrem "nove-domy.csv" (což je file_name)
        self.load_file('nove-domy.csv', 'neco')



    def nejaka_funkce(self, prvni, druhy):
        print(prvni)
        print(druhy)

    #fce, která načte data a zpracuje je
    def load_file(self, file_name, druhy_parametr):
        # self.nejaka_funkce('jedna', 'dva')
        #'ukazka-dat-obyvatelstvo-2001.csv'
        # radek;entity;property;house; qualifier-time; time; qualifier-count; count; source; reference
        # 1;Q1001619;P527;Q3947;P585;+1869 - 01 - 01T00:00:00Z / 09;P1114;33;S248;Q28708818

        # 29676	Q56417401	P1082	357	P585	+2001-00-00T00:00:00Z/09	P459	Q56229410	P1013	Q56229498	S248	Q28708818	S813	+2017-07-18T00:00:00Z/11

        #načte wikidata - resp. řekne skriptu, že budeme pracovat s wikidaty
        wikidata = pywikibot.getSite('wikidata')

        #načte property P1082
        property = pywikibot.Claim(wikidata, 'P1082')

        #vytvoří základní pouze pro referenci s P248
        source = pywikibot.Claim(wikidata, 'P248')
        #řekne target tomu source - kdy my bereme stránku z wd "Q287..." https://www.wikidata.org/wiki/Q28708818 (historicky lexikon)
        #lze tam mít teoreticky i třeba čistý text nebo url reference, ...
        source.setTarget(pywikibot.ItemPage(wikidata, 'Q28708818'))

        ##+2017-07-18T00:00:00Z/11
        #čas uvedení
        source_time = pywikibot.Claim(wikidata, 'P813')
        #tvorba data a času ve WB formátu
        date_source = pywikibot.WbTime(year=2017, month=7, day=18, precision=11)
        source_time.setTarget(date_source)

        #předvytvoření vymezení čas/datum
        qualifier_time = pywikibot.Claim(wikidata, 'P585')
        #předvytgoření něčeho jiného
        qualifier_determ = pywikibot.Claim(wikidata, 'P459')
        qualifier_criterion = pywikibot.Claim(wikidata, 'P1013')

        # property.
        #try -> except - pokud v bloku try vyskoči nějaká výjimka (exception), tek ji můžu exceptem odchytit a nespadne tak běh skriptu a můžu s tím nějak pracovat
        #typicky třeba APIError (editační konflikt) nebo nějaká chyba v datech
        try:
            #pravuji se souborem z proměnné file_name (nove-domy.csv) - kdy rt je nastavení modu čtení, snaf jen "read"
            with open(file_name, 'rt') as csvfile:
                #reader - nějaká knihovna, která umí číst a zpracovat csvčka, přece to nebudeme dělat sami ...
                reader = csv.DictReader(csvfile, delimiter=';')
                #for řádek in reader
                #pro každý řádek v readeru udělej následující
                #např také for polozka_pole in pole, ...
                for line in reader:
                    #if - podmínka
                    #not - negace
                    #tohle zjistuje ze souboru zda už jsem někdy sahl na zaznam s obsahem sloupce "radek" v daným řádku (line)
                    if not self.logger.isCompleteFile(line['radek']):
                        #získání entity, kam budeme zapisovat v line entity je něco jako Q12345
                        entity = pywikibot.ItemPage(wikidata, line['entity'])
                        entity.get()
                        # +2001-01-01T00:00:00Z/09
                        #zásobník referenví
                        property.sources = []
                        #zásobník vymezení - zde jako orderedDict (seřazený dict, přídavek, šlo by to dělat jinak, ale takhle to je snažší)
                        property.qualifiers = collections.OrderedDict()
                        #datetime - lépe se s datem pracuje tak, že pak získám rok přímo
                        datetime_object = datetime.datetime.strptime(line['time'], '+%Y-%m-%dT00:00:00Z/09')

                        #datum ve wikidata formátu - čistě rok
                        date = pywikibot.WbTime(year=datetime_object.year)
                        qualifier_time.setTarget(date)

                        #do kvalifikátoru determ (P459, viz výše) vložím cíl z pole determ -> např Q3445674
                        qualifier_determ.setTarget(pywikibot.ItemPage(wikidata, line['determ']))
                        #obdobně
                        qualifier_criterion.setTarget(pywikibot.ItemPage(wikidata, line['criterion']))

                        #počet obyvatel - WBQuantity - typ počet
                        count = pywikibot.WbQuantity(line['count'], site=wikidata)

                        #property do P1082 vložím ten počet
                        property.setTarget(count)

                        #naší vybrané entitě přidám property P1082
                        entity.addClaim(property)

                        #tomu přidám vymezení čas, ...
                        property.addQualifier(qualifier_time)
                        property.addQualifier(qualifier_determ)
                        property.addQualifier(qualifier_criterion)

                        #pridám zdroj
                        property.addSources([source,source_time])

                        #zaloguju, že už máme hotovo a příště se tomu už vyhnu
                        self.logger.logComplete(line['radek'])
                        #try -> catch
                        #zkus vypsat label v češtině a mezeru a rok
                        #pokud chybí pole label v proměnné entity, tak vyskočí výjimka ValueError a tak spadnu do Value Error exceptu, napíšu tak "cyhba labelu"
                        try:
                            print(entity.labels[u'cs'] + u' ' + str(datetime_object.year))
                        except ValueError:
                            print(u"chyba labelu")
        except pywikibot.data.api.APIError as e:
            #spadne li to, tak to pustíme znovu - není nutné, ale taky to jde. tohle je trošku prasárná :)
            self.load_file('nove-domy.csv')

    #další fce, nakonec nevyužito
    def page_process(self, page):
        pass

    def run(self):
        pass

# načti třídu HistoricLexicon s loggername "lexiconHousesNew"
f = HistoricLexicon('lexiconHousesNew')
#spust fci run (ta je prazdna)
f.run()
