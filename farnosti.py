#!/usr/bin/python
# -*- coding: utf-8 -*-
import urllib
import json
import re
import csv
import cStringIO
import types
import sys
import codecs
import unicodecsv
from bs4 import BeautifulSoup
from logger import Logger


class UnicodeWriter:
    """
    A CSV writer which will write rows to CSV file "f",
    which is encoded in the given encoding.
    """

    def __init__(self, f, dialect=csv.excel, encoding="utf-8", **kwds):
        # Redirect output to a queue
        self.queue = cStringIO.StringIO()
        self.writer = csv.writer(self.queue, dialect=dialect, **kwds)
        self.stream = f
        self.encoder = codecs.getincrementalencoder(encoding)()

    def writerow(self, row):

        list = []

        self.writer.writerow(list)
        # Fetch UTF-8 output from the queue ...
        data = self.queue.getvalue()
        data = data.decode("utf-8")
        # ... and reencode it into the target encoding
        data = self.encoder.encode(data)
        # write to the target stream
        self.stream.write(data)
        # empty queue
        self.queue.truncate(0)

    def writerows(self, rows):
        for row in rows:
            self.writerow(row)

logger = Logger(u'farnosti','saved')
pages = 302

f = urllib.urlopen("http://www.biskupstvi.cz/katalog/telefony.php")
content = f.read()

# print(page)

contnew = content.decode('utf-8')

soup = BeautifulSoup(contnew, 'html.parser')
[x.extract() for x in soup.findAll('script')]
prett = soup.prettify()
for line in soup.select('table tr'):
    num = 0
    for cell in line.select('td'):

        num = num + 1
        if (num == 1):
            link = cell.select('a')
            href = link[0]['href']
            match = re.search(u'farnost\.php\?kod=([a-zA-Z]+[0-9]*)(.*)', href)
            kod = match.groups()[0]
            # print(unicode(num) + ' ' + unicode(link.string))
            print(kod)
            url = "http://www.biskupstvi.cz/katalog/farnost.php?kod=" + kod
            print(url)
            l = urllib.urlopen(url)
            content = l.read()
            # print(content)
            contnew = content.decode('utf-8')
            soup = BeautifulSoup(contnew, 'html.parser')
            [x.extract() for x in soup.findAll('script')]
            prett = soup.prettify()
            # print(prett)
            data = soup.select('div')
            print(data)
            sys.exit()
