#!/usr/bin/python
# -*- coding: utf-8 -*-
import pywikibot
import sys
import codecs
# import etree
import untangle
import httplib2

class Starter:
    def __init__(self):
        #obce = ['Babice (okres Třebíč)', 'Bačice', 'Bačkovice', 'Benetice', 'Biskupice-Pulkov', 'Blatnice (okres Třebíč)', 'Bohušice', 'Bochovice', 'Bransouze', 'Březník', 'Budišov', 'Budkov (okres Třebíč)', 'Cidlina (okres Třebíč)', 'Čáslavice', 'Častohostice', 'Čechočovice', 'Čechtín','Červená Lhota (okres Třebíč)','Číhalín','Číchov','Čikov','Číměř (okres Třebíč)','Dalešice (okres Třebíč)','Dědice','Dešov','Dolní Lažany','Dolní Vilémovice','Domamil','Dukovany','Hartvíkovice','Heraltice','Hluboké','Hodov','Horní Heřmanice (okres Třebíč)', 'Horní Smrčné', 'Horní Újezd (okres Třebíč)', 'Horní Vilémovice', 'Hornice', 'Hrotovice', 'Hroznatín', 'Hvězdoňovice', 'Chlístov (okres Třebíč)', 'Chlum (okres Třebíč)', 'Chotěbudice', 'Jakubov u Moravských Budějovic', 'Jaroměřice nad Rokytnou', 'Jasenice (okres Třebíč)','Jemnice', 'Jinošov', 'Jiratice', 'Kamenná (okres Třebíč)', 'Kdousov', 'Kladeruby nad Oslavou', 'Klučov (okres Třebíč)', 'Kojatice', 'Kojatín', 'Kojetice (okres Třebíč)', 'Komárovice (okres Třebíč)', 'Koněšín','Kostníky', 'Kouty (okres Třebíč)', 'Kozlany (okres Třebíč)', 'Kožichovice', 'Krahulov', 'Kralice nad Oslavou', 'Kramolín (okres Třebíč)', 'Krhov (okres Třebíč)', 'Krokočín', 'Kuroslepy', 'Láz (okres Třebíč)','Lesná (okres Třebíč)', 'Lesní Jakubov', 'Lesonice (okres Třebíč)', 'Lesůňky', 'Lhánice', 'Lhotice (okres Třebíč)', 'Lipník (okres Třebíč)', 'Litohoř', 'Litovany', 'Lomy (okres Třebíč)', 'Loukovice','Lovčovice', 'Lukov (okres Třebíč)', 'Markvartice (okres Třebíč)', 'Martínkov', 'Mastník (okres Třebíč)', 'Menhartice', 'Meziříčko (okres Třebíč)', 'Mikulovice (okres Třebíč)', 'Mladoňovice (okres Třebíč)', 'Mohelno','Moravské Budějovice', 'Myslibořice', 'Naloučany', 'Náměšť nad Oslavou', 'Nárameč', 'Nimpšov', 'Nová Ves (okres Třebíč)', 'Nové Syrovice', 'Nový Telečkov', 'Ocmanice', 'Odunec', 'Okarec','Okřešice', 'Okříšky', 'Opatov (okres Třebíč)', 'Oponešice', 'Ostašov', 'Pálovice', 'Petrovice (okres Třebíč)', 'Petrůvky', 'Pokojovice', 'Police (okres Třebíč)', 'Popůvky (okres Třebíč)','Pozďatín', 'Přeckov', 'Předín', 'Přešovice', 'Přibyslavice (okres Třebíč)', 'Příštpo', 'Pucov', 'Pyšel', 'Rácovice', 'Račice (okres Třebíč)', 'Radkovice u Budče', 'Radkovice u Hrotovic','Radonín', 'Radošov (okres Třebíč)', 'Radotice', 'Rapotice', 'Rohy (okres Třebíč)', 'Rokytnice nad Rokytnou', 'Rouchovany', 'Rudíkov', 'Římov (okres Třebíč)', 'Sedlec (okres Třebíč)','Slavětice', 'Slavičky', 'Slavíkovice (okres Třebíč)', 'Smrk (okres Třebíč)', 'Stařeč', 'Stropešín', 'Střítež (okres Třebíč)', 'Studenec (okres Třebíč)', 'Studnice (okres Třebíč)', 'Sudice (okres Třebíč)','Svatoslav (okres Třebíč)', 'Šebkovice', 'Štěměchy', 'Štěpkov', 'Trnava (okres Třebíč)', 'Třebelovice', 'Třebenice (okres Třebíč)', 'Třebíč', 'Třesov', 'Valdíkov', 'Valeč (okres Třebíč)', 'Vícenice','Vícenice u Náměště nad Oslavou', 'Vladislav (okres Třebíč)', 'Vlčatín', 'Výčapy', 'Zahrádka (okres Třebíč)', 'Zárubice', 'Zašovice', 'Zvěrkovice', 'Želetava',]
        try:
            osm = OsmRun()
            wikidata = WikiDataRun()
            wikidata.getCounties();
            
            okres = sys.argv[1]
            isOk = wikidata.isExistCounty(okres)
            #print wikidata.counties
            if (isOk):
                print "County exist"
                obce = wikidata.getVillages(okres);            
                for obec in obce:                
                    osm.parent = obec                
                    if (not wikidata.isCompleteObec(obec)):
                        continue_with_cycle = True
                        try:
                            osm.get_and_parse()
                        except KeyError:
                            wikidata.wikilink = obec
                            wikidata.logErr('key_err')
                            wikidata.logComplete()
                            continue_with_cycle = False
                        if (continue_with_cycle):                            
                            wikidata.parentInfo(obec)
                            wikidata.osm_object = osm.osm_object            
                            wikidata.process()
                            wikidata.logComplete()
                        else:
                            print "Err"
            else:
                print "End"
        except KeyboardInterrupt:
            sys.exit()

class OsmObject:
    def __init__(self):
        self.parent = False
        self.neighbours = list()
        self.parent_wikilink = ''
        self.wikilink = ""

class OsmRun:
    def __init__(self):
        print "osm run"
        self.prefix = "cs"
        self.parent = "";
        self.link = "";        
        self.post_data = dict()
        
        self.osm_object = ""
        
    def get_and_parse(self):
        h = Http()
                                    
        self.link = "(rel['wikipedia'='" + self.prefix + ":" + self.parent + "']['admin_level'='8']['boundary'='administrative'];way(r);rel(bw))->.c;(rel.c['admin_level'='8']['boundary'='administrative'];rel(br);)->.d;(rel.d['admin_level'=8])->.e;.e;out meta;"
        self.post_data['data'] = self.link.encode('utf-8')        
        print self.parent
        response = h.request('http://overpass-api.de/api/interpreter', "POST", urlencode(self.post_data))
        try:
            #print response[1]
            root = etree.XML(response[1])
            xmldict = xmltodict.parse(response[1])
            
            osm = OsmObject();
            for x in xmldict['osm']['relation']:
                #if (x['tag']['@k'] == 'wikipedia'):
                #    print x['tag']['@v']
                #print x['@id']
                for t in x['tag']:
                    wikilink = self.prefix + ":" + self.parent;
                    if (t['@k'] == 'wikipedia'):                        
                        if (t['@v'] == wikilink):
                            #print t['@v']
                            osm.parent_wikilink = wikilink
                            osm.parent = True
                            osm.wikilink = wikilink
                        else:
                            #print t['@v']
                            new_neighbour = {'name': t['@v'], 'property_code': 0, 'relation_id': x['@id']}
                            #print new_neighbour
                            osm.neighbours.append(new_neighbour)
                    
            #print vars(xmldict)            
            self.osm_object = osm        
        except etree.XMLSyntaxError:
            print 'XML parsing error.'
        except TypeError:
            print "typeerr"

            

class WikiDataRun:
    def __init__(self):
        print "wikidata run"
        self.parent_property_code = 0
        self.osm_object = ""
        self.parent_data = ""
        self.continue_with_adding = False
        self.wikilink = "";
        self.counties = list()
    
    def getVillages(self, county):
        site = pywikibot.getSite('cs','wikipedia')
        #napr radonin
        county_category_link = "Obce v okrese " + unicode(county,'utf-8')
        
        import catlib
        cat = catlib.Category(site, county_category_link)
        obce_list = catlib.Category.articlesList(cat)
        obce = list()
        for obec in obce_list:
            obce.append(obec.title())
        return obce
    
    def getCounties(self):
        site = pywikibot.getSite('cs','wikipedia')
        #napr radonin
        category_link = u"Okresy v Česku"
                
        import catlib
        cat = catlib.Category(site, category_link)
        articles_list = catlib.Category.articlesList(cat)
        counties = list()
        for county in articles_list:
            if (not county.title().find('Okres ')):
                counties.append(county.title())                
        self.counties = counties
    
    def isExistCounty(self,okres):
        okres_link = unicode('Okres ' + okres, 'utf-8')
        try:            
            self.counties.index(okres_link)
            return True
        except ValueError:
            return False
    
    def parentInfo(self, wikilink):    
        self.continue_with_adding = True
        self.wikilink = wikilink
        site = pywikibot.getSite('cs','wikipedia')
        #napr radonin
        page = pywikibot.Page(site, wikilink)
        #print pywikibot.DataPage(page).get()
        self.parent_data = pywikibot.DataPage(page)
        dictionary = self.parent_data.get()
        wikidata_id = self.parent_data.getID()
        
        for c in dictionary['claims']:            
            if (c['m'][1] == 47):
                self.continue_with_adding = False
                
        #print dictionary
        print wikidata_id
        print self.continue_with_adding
        self.parent_property_code = wikidata_id
    
    def getInfo(self, wikilink):    
        site = pywikibot.getSite('cs','wikipedia')
        #napr radonin
        page = pywikibot.Page(site, wikilink)
        data = pywikibot.DataPage(page)
        dictionary = data.get()
        return data.getID()
        
    def save(self, entity_add, osm_relation_id):
        refes = {'P402' : [{
                "snaktype": "value",
                "property":"P402",
                "datavalue":{'type': 'string', 'value': osm_relation_id}
                }]}
        self.parent_data.editclaim('P47', entity_add, refs=refes, override=False)
    
    def isCompleteObec(self, obec):
        try:
            file = codecs.open("log_complete.txt", "r", "utf-8")
            lines = file.readlines()
            list_obce = list()
            file.close()
            for it in lines:                
                list_obce.append(it.strip())
            
            #print list_obce                    
            try:            
                list_obce.index(obec)
                return True
            except ValueError:
                return False
        except IOError:
            print "Not exist file"
            codecs.open('log_complete.txt', 'w', 'utf-8')
        
    def process(self):
        if (self.continue_with_adding):
            for v in self.osm_object.neighbours:                
                property_code = self.getInfo(v['name'])
                self.save(property_code, v['relation_id'])
        else:
            self.logErr()
            
    def logComplete(self):
        file = codecs.open("log_complete.txt", "a", "utf-8")
        file.write(self.wikilink + '\n')
        file.close()
        
    def logErr(self, text=''):
        file = codecs.open("log_filled.txt", "a", "utf-8")
        file.write(self.wikilink + ' - ' + text + '\n')
        file.close()
          
class People:
        
    def __init__(self):
        print "People"
        self.people = list()
        self.wikilink = ""
        self.obec = ""
        self.link = ""
        self.wikidata = ""
        
    def getRelativeEntityId(self):
        site = pywikibot.getSite('cs', 'wikipedia')
        entity = pywikibot.Page(site, self.wikilink)
        self.wikidata = pywikibot.DataPage(entity)
        entity_add = self.wikidata.getID()        
        return entity_add
        
    def setNames(self, obec, wikilink):
        self.wikilink = wikilink
        self.obec = obec
        
    def setLink(self, link):
        self.link = link
        
    def addYear(self, year, countPeople, countHouse=0):
        append = {'year' : year, 'peoples' : countPeople, 'houses' : countHouse}
        self.people.append(append)
        
    def printPeople(self):
        #print self.obec
        #print self.wikilink
        #print self.people
        return True
        
    def save(self):        
        site = pywikibot.Site('cs','wikipedia') #  any site will work, this is just an example
        print self.wikilink
        page = pywikibot.Page(site, self.wikilink)
        #print page.get()
        item = pywikibot.ItemPage.fromPage(page) #  this can be used for any page object
        #you can also define an item like this
        repo = site.data_repository()  # this is a DataSite object
        
        sitelinks = item.sitelinks
        aliases = item.aliases
        if 'en' in item.labels:
            print 'The label in English is: ' + item.labels['en']                 
        
        from collections import OrderedDict
        predct = dict()
        
        for fir in self.people:
            predct[int(fir['year'])] = fir
            
        dct = OrderedDict(sorted(predct.items(), key=lambda t: t[0],reverse=True))        
        
        for peop in dct.items():
            try:        
                claim = pywikibot.Claim(repo, u'P1082')                
                quantity = pywikibot.WbQuantity(int(peop[1]['peoples']))
                claim.setTarget(quantity)
                item.addClaim(claim)
                
                qualifier = pywikibot.Claim(repo, u'P585')
                target = pywikibot.WbTime(int(peop[1]['year']))
                qualifier.setTarget(target)
                claim.addQualifier(qualifier)
                
                if (self.link == ""):
                    self.link = "http://www.csu.cz";
                
                statedin = pywikibot.Claim(repo, u'P854')
                statedin.setTarget(self.link)
                
                retrieved = pywikibot.Claim(repo, u'P813')
                date = pywikibot.WbTime(year=2014, month=9, day=12)
                retrieved.setTarget(date)
                claim.addSources([statedin, retrieved]) 
            except pywikibot.data.api.APIError:
                pass    
        
        
class preparePeople:
    
    def __init__(self):
        print "preparer";
                
        
        self.typeEntity = "people"
        self.getCountyLookup()
        
        parser = OptionParser()
        parser.add_option("-c", "--county")
        parser.add_option("-m", "--missing", action="store_true", default=False)
        parser.add_option("-s", "--seek", action="store_true", default=False)

        (options, args) = parser.parse_args()
        
        self.checkMissing = options.missing
        self.okres = options.county
        self.seek = options.seek
        
        if (options.seek):
            getPeople()
            sys.exit()
        #print self.lau_to_county
        #print self.county_to_lau
        
    def run(self):
        self.loadFile()
        self.loadCounty(self.okres)
        
        self.getVillages(self.okres)
        self.loadBoth()
        self.connectWithTable()
    
    def loadBoth(self):
        for obec in self.obce:
            
            res = re.split(' \(+', obec)
            #if (res[0] != u"Praha"):
            #    continue
            if (res[0] == u'Vojenský újezd Brdy'):
                res[0] = u'Brdy'
                
            if (res[0] == u'Lučiny'):
                continue           
            #print res[0]
            #print "-------"
            self.county[res[0]]['wikilink'] = obec
    
    def logComplete(self, title):
        file = codecs.open("log_" + self.typeEntity + ".txt", "a", "utf-8")
        file.write(title + '\n')
        file.close()
        
    def logError(self, title):
        file = codecs.open("log_err_" + self.typeEntity + ".txt", "a", "utf-8")
        file.write(title + '\n')
        file.close()
        
    def isCompleteFile(self, entity):
        try:
            file = codecs.open("log_" + self.typeEntity + ".txt", "r", "utf-8")
            lines = file.readlines()
            list_entity = list()
            file.close()
            for it in lines:                
                list_entity.append(it.strip())
            #TODO opravit!
                    
            try:            
                list_entity.index(entity)
                return True
            except ValueError:
                return False
        except IOError:
            print "Not exist file"
            codecs.open('log_' + self.typeEntity + '.txt', 'w', 'utf-8')
        
    def connectWithTable(self):
        #print self.county
        #print self.county[u'Třebíč']['obec_lau']
        countobec = 0
        for obec in self.county.items():
            try:
                if not (self.isCompleteFile(obec[1]['wikilink'])):
                    if not(self.checkMissing):
                        people = People()
                        people.setNames(obec[1]['obec'], obec[1]['wikilink'])
                        people.addYear(2014, obec[1]['obyv'])
                        self.getTable(obec[1]['obec_lau'], people)
                        people.printPeople()
                        people.save()
                        self.logComplete(obec[1]['wikilink'])
                    else:
                        print obec[1]['obec']
                        countobec = countobec + 1
            except KeyError:                
                print "Error"
                self.logError(obec[1]['obec'])
        
        if (self.checkMissing):            
            print "Počet chybějících obcí: " + str(countobec)
            
        
        
        
    def getVillages(self, county):
        site = pywikibot.getSite('cs','wikipedia')
        #napr radonin
        county_category_link = "Kategorie:Obce v okrese " + unicode(county,'utf-8')
        page = pywikibot.Page(site, county_category_link)
        cat = pywikibot.Category(page)
        if cat.exists():
            obce_list = cat.articles()
        else:
            obce_list = list()
        county_category_link = u"Kategorie:Města v okrese " + unicode(county,'utf-8')
        page = pywikibot.Page(site, county_category_link)
        cat = pywikibot.Category(page)
        if cat.exists():
            mesta_list = cat.articles()            
        else:
            mesta_list = list()

        if (county == 'Praha'):
            county_category_link = u"Kategorie:Praha"
            page = pywikibot.Page(site, county_category_link)
            cat = pywikibot.Category(page)
            mesta_list = cat.articles()

        self.obce = list()
        for obec in obce_list:
            self.obce.append(obec.title())
        for obec in mesta_list:
            self.obce.append(obec.title())
                    
        
    def getCountyLookup(self):
        
        self.lau_to_county = {
            u'CZ0100':u'Praha',
            u'CZ0201':u'Benešov',
            u'CZ0202':u'Beroun',
            u'CZ0203':u'Kladno',
            u'CZ0204':u'Kolín',
            u'CZ0205':u'Kutná Hora',
            u'CZ0206':u'Mělník',
            u'CZ0207':u'Mladá Boleslav',
            u'CZ0208':u'Nymburk',
            u'CZ0209':u'Praha-východ',
            u'CZ020A':u'Praha-západ',
            u'CZ020B':u'Příbram',
            u'CZ020C':u'Rakovník',
            u'CZ0311':u'České Budějovice',
            u'CZ0312':u'Český Krumlov',
            u'CZ0313':u'Jindřichův Hradec',
            u'CZ0314':u'Písek',
            u'CZ0315':u'Prachatice',
            u'CZ0316':u'Strakonice',
            u'CZ0317':u'Tábor',
            u'CZ0321':u'Domažlice',
            u'CZ0322':u'Klatovy',
            u'CZ0323':u'Plzeň-město',
            u'CZ0324':u'Plzeň-jih',
            u'CZ0325':u'Plzeň-sever',
            u'CZ0326':u'Rokycany',
            u'CZ0327':u'Tachov',
            u'CZ0411':u'Cheb',
            u'CZ0412':u'Karlovy Vary',
            u'CZ0413':u'Sokolov',
            u'CZ0421':u'Děčín',
            u'CZ0422':u'Chomutov',
            u'CZ0423':u'Litoměřice',
            u'CZ0424':u'Louny',
            u'CZ0425':u'Most',
            u'CZ0426':u'Teplice',
            u'CZ0427':u'Ústí nad Labem',
            u'CZ0511':u'Česká Lípa',
            u'CZ0512':u'Jablonec nad Nisou',
            u'CZ0513':u'Liberec',
            u'CZ0514':u'Semily',
            u'CZ0521':u'Hradec Králové',
            u'CZ0522':u'Jičín',
            u'CZ0523':u'Náchod',
            u'CZ0524':u'Rychnov nad Kněžnou',
            u'CZ0525':u'Trutnov',
            u'CZ0531':u'Chrudim',
            u'CZ0532':u'Pardubice',
            u'CZ0533':u'Svitavy',
            u'CZ0534':u'Ústí nad Orlicí',
            u'CZ0631':u'Havlíčkův Brod',
            u'CZ0632':u'Jihlava',
            u'CZ0633':u'Pelhřimov',
            u'CZ0634':u'Třebíč',
            u'CZ0635':u'Žďár nad Sázavou',
            u'CZ0641':u'Blansko',
            u'CZ0642':u'Brno-město',
            u'CZ0643':u'Brno-venkov',
            u'CZ0644':u'Břeclav',
            u'CZ0645':u'Hodonín',
            u'CZ0646':u'Vyškov',
            u'CZ0647':u'Znojmo',
            u'CZ0711':u'Jeseník',
            u'CZ0712':u'Olomouc',
            u'CZ0713':u'Prostějov',
            u'CZ0714':u'Přerov',
            u'CZ0715':u'Šumperk',
            u'CZ0721':u'Kroměříž',
            u'CZ0722':u'Uherské Hradiště',
            u'CZ0723':u'Vsetín',
            u'CZ0724':u'Zlín',
            u'CZ0801':u'Bruntál',
            u'CZ0802':u'Frýdek-Místek',
            u'CZ0803':u'Karviná',
            u'CZ0804':u'Nový Jičín',
            u'CZ0805':u'Opava',
            u'CZ0806':u'Ostrava-město',
        }
        self.county_to_lau = dict(zip(self.lau_to_county.values(),self.lau_to_county.keys()))

    def loadCounty(self,county):
        #try:
        #print self.county_to_lau[unicode(county,'utf-8')]
        self.county = dict()
        for lo in self.list_obce:
            if (lo['okres_lau'] == self.county_to_lau[unicode(county,'utf-8')]):
                #print lo['obec']
                self.county[lo['obec']] = lo
        #print self.county
        #print "------------"
    
    def loadFile(self):
        try:
            file = codecs.open("2014.csv", "r", "utf-8")
            lines = file.readlines()
            self.list_obce = list()            
            file.close()
            i = 0
            for it in lines:
                if (i>0):
                    line_explode = it.strip().split(';')
                    row = {'okres_lau' : line_explode[0], 'obec_lau' : line_explode[1], 'obec' : line_explode[2], 'obyv' : line_explode[3]}
                    self.list_obce.append(row)
                i=i+1
                
            
            #print self.list_obce                    
            
        except IOError:
            print "Not exist file"
            codecs.open('log_complete.txt', 'w', 'utf-8')
        
        
    def getTable(self, code, people):
        h = Http()
        #code = trebic = 590266
        #561959
        try:
            link = 'http://vdb.czso.cz/obsldb/tabulka.jsp?kob=' + str(code)
            response = h.request(link)
            #print response[1]
            #sys.exit();
            table_tmp = response[1]
            start_table = table_tmp.index('table')-1
            end_table = table_tmp.index('/table')+7
            table_str_tmp = table_tmp[start_table:end_table]
            table_str = table_str_tmp.replace(' align="right"', '')
            table_str_2 = table_str.replace(' ', '')
            #print table_str.decode("utf-8", "ignore")
            people.setLink(link)
            #people = People()
            table = etree.XML(table_str.decode("utf-8", "ignore"))
            rows = iter(table)
            headers = [col.text for col in next(rows)]        
            for row in rows:
                values = [col.text for col in row]
                people.addYear(values[0],values[1],values[2])
        except ValueError:
            self.logError(str(code) + " - problem - nenalezena tabulka na webu")
        
        
class getPeople:
    def __init__(self):
        print "getPeople"
        self.actual_county = ""
        parser = OptionParser()
        parser.add_option("-c", "--control", action="store_true", default=False)
        parser.add_option("-e", "--errors", action="store_true", default=False)
        parser.add_option("-s", "--seek", action="store_true", default=False)

        (options, args) = parser.parse_args()
        if (options.control):
            self.getCounties()
            self.control()
            
    def getCounties(self):
        self.counties = list()
        self.counties.append('Třebíč') #ok, Třebíč má víc
        self.counties.append('Praha') #ok, má něco navíc
        self.counties.append('Benešov') #ok
        self.counties.append('Beroun') #ok
        self.counties.append('Kladno') #ok
        self.counties.append('Kolín') #ok
        self.counties.append('Kutná Hora') #ok
        self.counties.append('Mělník') #ok
        self.counties.append('Mladá Boleslav') #ok
        self.counties.append('Nymburk') #ok
        self.counties.append('Praha-východ') #ok
        self.counties.append('Praha-západ') #ok
        self.counties.append('Příbram') #ok
        self.counties.append('Rakovník') #ok
        self.counties.append('České Budějovice') #ok vidov má víc
        self.counties.append('Český Krumlov') #ok
        self.counties.append('Jindřichův Hradec') #ok
        self.counties.append('Písek') #ok
        self.counties.append('Prachatice') #ok
        self.counties.append('Strakonice') #ok
        self.counties.append('Tábor') #ok
        self.counties.append('Domažlice') #ok
        self.counties.append('Klatovy') #ok
        self.counties.append('Plzeň-město') #ok
        self.counties.append('Plzeň-jih') #ok
        self.counties.append('Plzeň-sever') #ok
        self.counties.append('Rokycany') #ok
        self.counties.append('Tachov') #ok
        self.counties.append('Cheb') #ok aš má víc
        self.counties.append('Karlovy Vary') #ok Lučiny nemají, protože nemají ani infobox
        self.counties.append('Sokolov') #ok
        self.counties.append('Děčín') #ok
        self.counties.append('Chomutov') #ok
        self.counties.append('Litoměřice') #ok
        self.counties.append('Louny') #ok
        self.counties.append('Most') #ok
        self.counties.append('Teplice') #ok
        self.counties.append('Ústí nad Labem') #ok
        self.counties.append('Česká Lípa') #ok
        self.counties.append('Jablonec nad Nisou') #ok
        self.counties.append('Liberec') #ok (Hrádek nad Nisou má jednu navíc se zdrojem)
        self.counties.append('Semily') #ok
        self.counties.append('Hradec Králové') #ok
        self.counties.append('Jičín') #ok
        self.counties.append('Náchod') #ok
        self.counties.append('Rychnov nad Kněžnou') #ok
        self.counties.append('Trutnov') #ok
        self.counties.append('Chrudim') #ok
        self.counties.append('Pardubice') #ok
        self.counties.append('Svitavy') #ok
        self.counties.append('Ústí nad Orlicí') #ok
        self.counties.append('Havlíčkův Brod') #ok
        self.counties.append('Jihlava') #ok
        self.counties.append('Pelhřimov') #ok
        self.counties.append('Žďár nad Sázavou') #ok
        self.counties.append('Blansko') #ok
        self.counties.append('Brno-město') #ok
        self.counties.append('Brno-venkov') #ok
        self.counties.append('Břeclav') #ok
        self.counties.append('Hodonín') #ok
        self.counties.append('Vyškov') #ok (bžezina má míň)
        self.counties.append('Znojmo') #ok Velký Karlov založen pozdě
        self.counties.append('Jeseník') #ok
        self.counties.append('Olomouc') #ok
        self.counties.append('Prostějov') #ok
        self.counties.append('Přerov') #ok
        self.counties.append('Šumperk') #ok
        self.counties.append('Kroměříž') #ok
        self.counties.append('Uherské Hradiště') #ok
        self.counties.append('Vsetín') #ok
        self.counties.append('Zlín') #ok
        self.counties.append('Bruntál') #ok
        self.counties.append('Frýdek-Místek') #ok
        self.counties.append('Karviná') #ok
        self.counties.append('Nový Jičín') #ok
        self.counties.append('Opava') #ok
        self.counties.append('Ostrava-město') #ok
    
    def control(self):
        print "control"
        for county in self.counties:
            self.actual_county = county
            print county
            self.getVillages(county)
            for obec in self.obce:
                self.getVillage(obec)
            self.cleanVillages()
    
    def getVillages(self, county):
        site = pywikibot.getSite('cs','wikipedia')
        #napr radonin
        county_category_link = "Kategorie:Obce v okrese " + unicode(county,'utf-8')
        page = pywikibot.Page(site, county_category_link)
        cat = pywikibot.Category(page)
        obce_list = cat.articles()
        
        county_category_link = u"Kategorie:Města v okrese " + unicode(county,'utf-8')
        page = pywikibot.Page(site, county_category_link)
        cat = pywikibot.Category(page)
        mesta_list = cat.articles()
        self.obce = list()
        for obec in obce_list:
            self.obce.append(obec.title())
        for obec in mesta_list:
            self.obce.append(obec.title())

    def cleanVillages(self):
        self.obce = list()
        
    def getVillage(self, village):
        site = pywikibot.Site('cs','wikipedia') #  any site will work, this is just an example
        page = pywikibot.Page(site, village)
        item = pywikibot.ItemPage.fromPage(page) #  this can be used for any page object
        #you can also define an item like this
        repo = site.data_repository()  # this is a DataSite object
        sitelinks = item.sitelinks
        aliases = item.aliases
        #try:
        if u'P1082' in item.claims:
            count = 0
            for claim in item.claims[u'P1082']:
                #print claim.getTarget()
                count = count + 1
                for qualif in claim.qualifiers.items():
                    
                    time_entity = qualif[1][0].getTarget()
                    
                    if time_entity.year == 2014:
                        print village
                        print time_entity.year
                        
                        datasite = site.data_repository()
                        claim.rank = "preferred";
                        print datasite.changeClaimTarget(claim)
                        sys.exit()
                        #datasite.removeClaims(claim.qualifiers.items()[0][1])
                        
                        #qualifier = pywikibot.Claim(repo, u'P585')
                        target = pywikibot.WbTime(2014,1,1)
                        qualif[1][0].setTarget(target)                        
                        datasite.editQualifier(claim, qualif[1][0])
                        
                        pass
            if count <> 14:
                print u"podezdrely pocet zaznamu v obci " + village + " (" + unicode(self.actual_county,'utf-8') + ")"
        else:
            print u"Není přítomno obyvatelstvo v obci " + village + " (" + unicode(self.actual_county,'utf-8') + ")"
        #except Exception:
        #    pass

s = Starter()