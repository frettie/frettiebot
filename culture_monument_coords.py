#!/usr/bin/python
# -*- coding: utf-8 -*-
import unicodecsv

from logger import Logger
import untangle
import pywikibot
import re

from pywikibot import pagegenerators
from pywikibot.textlib import mwparserfromhell as parser

from math import radians, cos, sin, asin, sqrt

def haversine(lon1, lat1, lon2, lat2):
    """
    Calculate the great circle distance between two points
    on the earth (specified in decimal degrees)
    """
    # convert decimal degrees to radians
    lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])
    # haversine formula
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
    c = 2 * asin(sqrt(a))
    km = 6367 * c;
    return km

class culture_monument_coords:
    def __init__(self, logger_name):
        self.errors = {}
        print 'historicLexiconHouses'

        self.logger = Logger(logger_name + u'Fill', 'saved')
        self.actual_page = None
        self.wikidata = None
        self.language_wikipedia = 'cs'
        self.wikidata_family = 'wikidata'
        self.family = 'wikipedia'

        self.dataCoords = {}

        self.site = pywikibot.Site('cs', 'wikipedia')
        self.repo = self.site.data_repository()

        self.genFactory = pagegenerators.GeneratorFactory(site=self.site)
        self.genFactory.handleArg('-ns:0')
        self.generator = self.genFactory.getCombinedGenerator()
        if not self.generator:
            self.genFactory.handleArg(u'-ref:Template:Památky v Česku')
            self.generator = self.genFactory.getCombinedGenerator()
            # self.genFactory.handleArg(u'-page:' + u'Seznam kulturních památek v okrese Klatovy')
            # self.generator = self.genFactory.getCombinedGenerator()

        # self.genFactory.handleArg(u'-page:Seznam kulturních památek v Moravské Třebové')
        # self.generator = self.genFactory.getCombinedGenerator()

        # self.load_file()
        # monument_dictionary = self.load_dict()

        # self.import_to_templates(monument_dictionary)
        # self.links_in_templates()

        self.dict_rules()
        self.get_monuments()
        self.links_in_templates()

        # self.import_to_wikidata(monument_dictionary)
        # self.check_correct(monument_dictionary)

    def load_file(self):
        #'ukazka-dat-obyvatelstvo-2001.csv'
        # radek;entity;property;house; qualifier-time; time; qualifier-count; count; source; reference
        # 1;Q1001619;P527;Q3947;P585;+1869 - 01 - 01T00:00:00Z / 09;P1114;33;S248;Q28708818
        obj = untangle.parse('all.osm')
        dictfirst = {}
        for mon in obj.osm.node:
            assert isinstance(mon, untangle.Element)
            dictfirst[u'lon'] = u'lon'
            dictfirst[u'lat'] = u'lat'
            for info in mon.tag:
                assert isinstance(info, untangle.Element)
                dictfirst[info.get_attribute(u'k')] = info.get_attribute(u'k')

        f = unicodecsv.DictWriter(open("prahapam.csv", "ab+"), fieldnames=dictfirst)
        f.writeheader()
        for mon in obj.osm.node:
            line = {}
            assert isinstance(mon, untangle.Element)
            line[u'lat'] = mon.get_attribute(u'lat')
            line[u'lon'] = mon.get_attribute(u'lon')
            for info in mon.tag:
                assert isinstance(info, untangle.Element)
                val = info.get_attribute(u'v').replace('\n', '<br />')
                line[info.get_attribute(u'k')] = val

            f.writerow(line)


        pass

    def load_dict(self):
        obj = untangle.parse('third_ver.osm')
        # obj = untangle.parse('all2.osm')
        monument_dictionary = {}
        for mon in obj.osm.node:
            line = {}
            assert isinstance(mon, untangle.Element)
            line[u'lat'] = mon.get_attribute(u'lat')
            line[u'lon'] = mon.get_attribute(u'lon')
            line[u'url'] = mon.get_attribute(u'_urlExt_')

            for info in mon.tag:
                assert isinstance(info, untangle.Element)
                if info.get_attribute(u'k') == u'rejstrikov':
                    if (monument_dictionary.has_key(info.get_attribute(u'v'))):
                        print u"exist key " + info.get_attribute(u'v')
                    line[u'key'] = info.get_attribute(u'v')
                if info.get_attribute(u'k') == u'urlExt':
                    line[u'url'] = info.get_attribute(u'v')

            monument_dictionary[line[u'key']] = line
        return monument_dictionary

    def import_to_wikidata(self, monument_dictionary):
        for page in pagegenerators.PreloadingGenerator(self.generator):
            pywikibot.output(page)
            code = parser.parse(page.text)
            self.wd = pywikibot.Site('wikidata')

            source = pywikibot.Claim(self.wd, 'P248')
            source.setTarget(pywikibot.ItemPage(self.wd, 'Q41593640'))

            property = pywikibot.Claim(self.wd, 'P625')

            for template in code.ifilter_templates():
                if not template.name.matches(u'Památky v Česku'):
                    continue
                if template.has('Id_objektu', ignore_empty=True):
                    assert isinstance(template, parser.wikicode.Template)
                    ident = str(template.get('Id_objektu').value).strip()
                    try:
                        wikidata = template.get('Wikidata').value
                    except ValueError as e:
                        continue

                    if self.logger.isCompleteFile(str(wikidata)):
                        continue

                    # self.repo = self.site.data_repository()
                    entity = pywikibot.ItemPage(self.wd, wikidata)

                    # obec = unicode(template.get(u'Obec').value)
                    # lat = str(template.get(u'Zeměpisná_šířka').value).strip()
                    # lon = str(template.get(u'Zeměpisná_délka').value).strip()
                    # if lat:
                    #     continue  # todo: warning
                    # if lon:
                    #     continue  # todo: warning


                    sources = []
                    sources.append(source)

                    property.sources = []
                    try:
                        data = monument_dictionary[ident]
                        new_lat = data[u'lat']
                        new_lon = data[u'lon']
                        url_link = data[u'url']
                        claims = entity.get()

                        old_coord = claims[u'claims'].get(u'P625', False)
                        if not old_coord:
                            url = pywikibot.page.Claim(self.wd, 'P854', datatype='url')
                            url.setTarget(url_link)
                            sources.append(url)
                            new_coord = pywikibot.Coordinate(float(new_lat), float(new_lon), precision=1e-07, globe='earth', site=self.wd)
                            property.setTarget(new_coord)
                            entity.addClaim(property)
                            property.addSources(sources)
                            # property.addSource(url)





                        self.logger.logComplete(str(wikidata))
                    except KeyError as e:
                        # text = u"not in dictionary " + ident + u' obec: ' + obec
                        # print text
                        # self.logger.logError(text)
                        pass
                    except IOError as e:
                        print e
                        # self.logger.logError(text)
                        pass
                    except pywikibot.exceptions.IsRedirectPage as e:
                        pass
                    except AttributeError as e:
                        text = u"attribute err " + str(wikidata).strip()
                        print e
                        self.logger.logError(text)
                        pass

                    except KeyError as e:
                        text = u"not in dictionary " + ident + u' obec: ' + obec
                        # print text
                        self.logger.logError(text)
                        pass
                    except pywikibot.data.api.APIError as e:
                        print e
                        pass

            # if change:
            #     page.put_async(
            #         code, summary=u'doplnění koordinátů památek ze zdrojů NPÚ')

    def check_correct(self, monument_dictionary):
        for page in pagegenerators.PreloadingGenerator(self.generator):
            pywikibot.output(page)
            code = parser.parse(page.text)
            change = False
            self.wd = pywikibot.Site('wikidata')
            for template in code.ifilter_templates():
                if not template.name.matches(u'Památky v Česku'):
                    continue
                if template.has('Id_objektu', ignore_empty=True):
                    assert isinstance(template, parser.wikicode.Template)
                    ident = str(template.get('Id_objektu').value).strip()
                    try:
                        wikidata = template.get('Wikidata').value
                    except ValueError as e:
                        continue

                    # self.repo = self.site.data_repository()
                    entity = pywikibot.ItemPage(self.wd, wikidata)

                    # obec = unicode(template.get(u'Obec').value)
                    # lat = str(template.get(u'Zeměpisná_šířka').value).strip()
                    # lon = str(template.get(u'Zeměpisná_délka').value).strip()
                    # if lat:
                    #     continue  # todo: warning
                    # if lon:
                    #     continue  # todo: warning

                    try:
                        data = monument_dictionary[ident]
                        new_lat = data[u'lat']
                        new_lon = data[u'lon']
                        claims = entity.get()

                        old_coord = claims[u'claims'].get(u'P625',False)
                        if (old_coord):
                            old_lat = old_coord[0].target.lat
                            old_lon = old_coord[0].target.lon
                            distance = haversine(float(new_lon), float(new_lat), old_lon, old_lat)
                            if (distance > 0.1):
                                raise IOError

                        change = False
                    except KeyError as e:
                        # text = u"not in dictionary " + ident + u' obec: ' + obec
                        # print text
                        # self.logger.logError(text)
                        pass
                    except IOError as e:
                        text = u"distance higher than 100 m " + str(wikidata).strip() + u' distance ' + unicode(distance)
                        # print text
                        self.logger.logError(text)
                        pass
                    except pywikibot.exceptions.IsRedirectPage as e:
                        pass
                    except AttributeError as e:
                        text = u"attribute err " + str(wikidata).strip()
                        print e
                        self.logger.logError(text)
                        pass
            if change:
                page.put_async(
                    code, summary=u'doplnění koordinátů památek ze zdrojů NPÚ')

    def import_to_templates(self, monument_dictionary):
        for page in pagegenerators.PreloadingGenerator(self.generator):
            pywikibot.output(page)
            code = parser.parse(page.text)
            change = False
            for template in code.ifilter_templates():
                if not template.name.matches(u'Památky v Česku'):
                    continue
                if template.has('Id_objektu', ignore_empty=True):
                    assert isinstance(template, parser.wikicode.Template)
                    ident = str(template.get('Id_objektu').value).strip()
                    obec = unicode(template.get(u'Obec').value)
                    lat = str(template.get(u'Zeměpisná_šířka').value).strip()
                    lon = str(template.get(u'Zeměpisná_délka').value).strip()
                    if lat:
                        continue  # todo: warning
                    if lon:
                        continue  # todo: warning

                    try:
                        data = monument_dictionary[ident]
                        new_lat = data[u'lat']
                        new_lon = data[u'lon']
                        template.add(u'Zeměpisná_šířka', new_lat)
                        template.add(u'Zeměpisná_délka', new_lon)
                        template.add(u'Zdroj souřadnic', u'PK')
                        template.add(u'Zdroj souřadnic datum přístupu', u'2017-09-21')

                        change = True
                    except KeyError as e:
                        text = u"not in dictionary " + ident + u' obec: ' + obec
                        # print text
                        self.logger.logError(text)
                        pass
            if change:
                page.put_async(
                    code, summary=u'doplnění koordinátů památek ze zdrojů NPÚ')

    def get_rules(self):
        self.rules = {}
        self.logger = Logger(u'newStreets' + u'Fill', 'saved')
        cities = {}
        self.site = pywikibot.Site('cs', 'wikipedia')
        self.repo = self.site.data_repository()
        query = '''SELECT ?typLabel WHERE {
  ?item wdt:P1435 wd:Q385405 .
        ?item wdt:P31 ?typ .
        ?item wdt:P131 ?admin .
  SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],cs". }
} group by ?typLabel'''
        from pywikibot.data import sparql

        query_object = sparql.SparqlQuery()
        data = query_object.select(query, full_data=True)
        if data:
            for r in data:
                pywikibot.output(r['typLabel'])
                # cities[str(r['lau'])] = pywikibot.ItemPage(self.repo, r['mesto'].getID())

    def dict_rules(self):
        self.culture_rules = {}
        self.culture_rules[u'hrad'] = True
        self.culture_rules[u'zámek'] = True
        self.culture_rules[u'klášter'] = True
        self.culture_rules[u'zřícenina hradu'] = True
        self.culture_rules[u'synagoga'] = True
        self.culture_rules[u'hotel'] = True
        self.culture_rules[u'divadlo'] = True
        self.culture_rules[u'operní scéna'] = True
        self.culture_rules[u'letiště'] = True
        self.culture_rules[u'vodní kanál'] = True
        self.culture_rules[u'jezero'] = True
        self.culture_rules[u'radnice'] = True
        self.culture_rules[u'zaniklá stavba'] = False
        self.culture_rules[u'kostel'] = True
        self.culture_rules[u'muzeum'] = True
        self.culture_rules[u'stavba'] = True
        self.culture_rules[u'palác'] = True
        self.culture_rules[u'hřbitov'] = True
        self.culture_rules[u'koněspřežná dráha'] = True
        self.culture_rules[u'katedrála'] = True
        self.culture_rules[u'osada'] = True
        self.culture_rules[u'hradiště'] = True
        self.culture_rules[u'zřícenina'] = True
        self.culture_rules[u'rozhledna'] = True
        self.culture_rules[u'přehradní hráz'] = True
        # self.culture_rules[u'budova'] = True
        self.culture_rules[u'knihovna'] = True
        self.culture_rules[u'pivovar'] = True
        self.culture_rules[u'židovský hřbitov'] = True
        self.culture_rules[u'věž'] = True
        self.culture_rules[u'morový sloup'] = True
        self.culture_rules[u'jeskyně'] = True
        self.culture_rules[u'skalní hrad'] = True
        self.culture_rules[u'obloukový most'] = True
        self.culture_rules[u'kamenný most'] = True
        self.culture_rules[u'železniční most'] = True
        # self.culture_rules[u'usedlost'] = True
        self.culture_rules[u'pomník'] = True
        self.culture_rules[u'observatoř'] = True
        self.culture_rules[u'univerzita'] = True
        self.culture_rules[u'náměstí'] = True
        self.culture_rules[u'rotunda'] = True
        self.culture_rules[u'vila'] = True
        self.culture_rules[u'koncentrační tábor'] = True
        self.culture_rules[u'tvrz'] = True
        self.culture_rules[u'kolonáda'] = True
        self.culture_rules[u'koncertní síň'] = True
        self.culture_rules[u'stadion'] = True
        self.culture_rules[u'basilica minor'] = True
        self.culture_rules[u'lovecký zámeček'] = True
        self.culture_rules[u'archeologická lokalita'] = True
        self.culture_rules[u'pevnost'] = True
        self.culture_rules[u'zřícenina kláštera'] = True
        self.culture_rules[u'kavárna'] = True
        self.culture_rules[u'ocelárna'] = True
        self.culture_rules[u'mauzoleum'] = True
        self.culture_rules[u'Gloriet'] = True
        self.culture_rules[u'farní kostel'] = True
        self.culture_rules[u'malá vodní elektrárna'] = True
        self.culture_rules[u'obchodní dům'] = True
        self.culture_rules[u'obchodní centrum'] = True
        self.culture_rules[u'střelnice'] = True
        self.culture_rules[u'popraviště'] = True
        self.culture_rules[u'park'] = True
        self.culture_rules[u'vojenské muzeum'] = True
        self.culture_rules[u'větrný mlýn'] = True
        self.culture_rules[u'most'] = True
        self.culture_rules[u'Poutní cesta'] = True
        self.culture_rules[u'fara'] = True
        self.culture_rules[u'tvrziště'] = True
        self.culture_rules[u'nemocnice'] = True
        self.culture_rules[u'památník'] = True
        self.culture_rules[u'sloup Nejsvětější Trojice'] = True
        self.culture_rules[u'gymnázium'] = True
        self.culture_rules[u'obelisk'] = True
        self.culture_rules[u'městské hradby'] = True
        self.culture_rules[u'opevnění'] = True
        self.culture_rules[u'oppidum'] = True
        self.culture_rules[u'továrna'] = True
        self.culture_rules[u'skanzen'] = True
        self.culture_rules[u'křížová cesta'] = True
        self.culture_rules[u'kašna'] = True
        self.culture_rules[u'ozubnicová dráha'] = True
        self.culture_rules[u'hrobka'] = True
        self.culture_rules[u'vodojem'] = True
        self.culture_rules[u'městský park'] = True
        self.culture_rules[u'vodní mlýn'] = True
        self.culture_rules[u'dřevěný most'] = True
        self.culture_rules[u'krytý most'] = True
        self.culture_rules[u'bašta'] = True
        self.culture_rules[u'porodnice'] = True
        self.culture_rules[u'vodárna'] = True
        self.culture_rules[u'silniční most'] = True
        self.culture_rules[u'koupaliště'] = True
        self.culture_rules[u'pěchotní srub'] = True
        self.culture_rules[u'špitál'] = True
        self.culture_rules[u'brána'] = True
        self.culture_rules[u'hvězdárna'] = True
        self.culture_rules[u'zdymadlo'] = True
        self.culture_rules[u'letohrádek'] = True
        self.culture_rules[u'horská chata'] = True
        self.culture_rules[u'cukrovar'] = True
        self.culture_rules[u'vysoká pec'] = True
        self.culture_rules[u'sklárna'] = True
        self.culture_rules[u'kasárny'] = True
        self.culture_rules[u'vodní elektrárna'] = True
        self.culture_rules[u'lékárna'] = True
        self.culture_rules[u'elektrárna'] = True
        self.culture_rules[u'přístav'] = True
        self.culture_rules[u'arboretum'] = True
        self.culture_rules[u'spořitelna'] = True
        self.culture_rules[u'rodinný dům'] = True
        self.culture_rules[u'mariánský sloup'] = True
        self.culture_rules[u'kostnice'] = True
        self.culture_rules[u'cihelna'] = True
        self.culture_rules[u'zámecký park'] = True
        self.culture_rules[u'jez'] = True
        self.culture_rules[u'mincovna'] = True
        self.culture_rules[u'chrám'] = True

    def get_monuments(self):
        self.monuments = {}
        self.site = pywikibot.Site('cs', 'wikipedia')
        self.repo = self.site.data_repository()
        query = '''SELECT ?item ?itemLabel ?typLabel WHERE {
  ?item wdt:P1435 wd:Q385405 .
        ?item wdt:P31 ?typ .
#        ?item wdt:P131 ?admin .
  SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],cs". }
}  '''
        from pywikibot.data import sparql

        query_object = sparql.SparqlQuery()
        data = query_object.select(query, full_data=True)
        if data:
            for r in data:
                if (self.monuments.get(r['item'].getID(), False)):
                    self.monuments[r['item'].getID()].append(str(r['typLabel']))
                else:
                    self.monuments[r['item'].getID()] = []
                    self.monuments[r['item'].getID()] = [str(r['typLabel'])]
                # pywikibot.output(r['item'].getID())
                # pywikibot.output(r['typLabel'])
                # pywikibot.output(r['itemLabel'])
        return self.monuments

    def check_if_monument_write_link(self, monument_id):
        monument = self.monuments[monument_id]
        for mon in monument:
            if (self.culture_rules.get(mon, False)):
                return monument
        return False

    def links_in_templates(self):
        for page in pagegenerators.PreloadingGenerator(self.generator):
            pywikibot.output(page)
            code = parser.parse(page.text)
            change = False
            for template in code.ifilter_templates():
                if not template.name.matches(u'Památky v Česku'):
                    continue
                if template.has('Id_objektu', ignore_empty=True) and template.has('Wikidata', ignore_empty=True) and template.has(u'Článek'):
                    assert isinstance(template, parser.wikicode.Template)
                    wd = str(template.get('Wikidata').value).strip()
                    obec = unicode(template.get(u'Obec').value).strip()
                    clanek = unicode(template.get(u'Článek').value).strip()
                    nazev = unicode(template.get(u'Název').value).strip()

                    match = re.search(u'(.*) \((.*)\)', obec)
                    if not match:
                        name_of_obec = obec
                    else:
                        gr = match.groups()
                        name_of_obec = gr[0]

                    obec = name_of_obec

                    # lat = str(template.get(u'Zeměpisná_šířka').value).strip()
                    # lon = str(template.get(u'Zeměpisná_délka').value).strip()
                    if clanek:
                        continue  # todo: warning

                    try:
                        write_link = self.check_if_monument_write_link(wd)
                        if write_link != False:
                            if (len(write_link) == 1):
                                if 'kostel' in write_link:
                                    link = nazev + ' (' + obec + ')'
                                elif 'tvrz' in write_link:
                                    if nazev == 'Tvrz':
                                        link = nazev + ' ' + obec
                                    elif nazev == u'Bývalá tvrz':
                                        link = nazev + ' ' + obec
                                    else:
                                        link = nazev
                                elif 'hrad' in write_link:
                                    link = nazev
                                elif 'most' in write_link:
                                    link = nazev
                                elif 'vodojem' in write_link:
                                    link = nazev
                                elif 'jez' in write_link:
                                    link = nazev
                                elif 'cukrovar' in write_link:
                                    link = nazev
                                elif 'cihelna' in write_link:
                                    link = nazev
                                elif 'hrobka' in write_link:
                                    link = nazev
                                elif 'divadlo' in write_link:
                                    link = nazev
                                elif 'synagoga' in write_link:
                                    link = nazev
                                elif 'nemocnice' in write_link:
                                    if nazev == 'Poliklinika':
                                        link = nazev + ' (' + obec + ')'
                                    else:
                                        link = nazev
                                elif 'mincovna' in write_link:
                                    link = nazev
                                elif 'obelisk' in write_link:
                                    link = nazev
                                elif 'muzeum' in write_link:
                                    link = nazev
                                elif 'park' in write_link:
                                    if nazev == 'Park':
                                        link = nazev + ' (' + obec + ')'
                                    else:
                                        link = nazev
                                elif 'rozhledna' in write_link:
                                    link = nazev
                                elif 'mauzoleum' in write_link:
                                    link = nazev
                                elif 'hotel' in write_link:
                                    link = nazev
                                elif 'radnice' in write_link:
                                    if nazev == 'Radnice':
                                        link = nazev + ' (' + obec + ')'
                                    else:
                                        link = nazev
                                elif 'budova' in write_link:
                                    link = nazev + ' (' + obec + ')'
                                elif 'vila' in write_link:
                                    link = nazev + ' (' + obec + ')'
                                elif 'pivovar' in write_link:
                                    link = nazev + ' (' + obec + ')'
                                elif 'fara' in write_link:
                                    if nazev == 'Fara':
                                        link = nazev + ' ' + obec
                                    elif nazev == u'Děkanství a fara':
                                        link = nazev + ' ' + obec
                                    else:
                                        link = nazev
                                else:
                                    link = nazev
                            else:
                                if 'kostel' in write_link:
                                    link = nazev + ' (' + obec + ')'
                                elif 'hrad' in write_link:
                                    link = nazev
                                elif 'tvrz' in write_link:
                                    if nazev == 'Tvrz':
                                        link = nazev + ' ' + obec
                                    elif nazev == u'Bývalá tvrz':
                                        link = nazev + ' ' + obec
                                    else:
                                        link = nazev
                                else:
                                    link = nazev

                            pywikibot.output(link)
                            template.add(u'Článek', link)
                            # pywikibot.output(link)
                        change = True
                    except KeyError as e:
                        # text = u"not in dictionary " + ident + u' obec: ' + obec
                        # print text
                        # self.logger.logError(text)
                        pass
            if change:
                page.put_async(
                    code, summary=u'linky')

    def run(self):
        pass

f = culture_monument_coords('culturecoordscorrectionwikidata')
# f.import_to_templates()
# f.insert_to_redis()
f.run()
