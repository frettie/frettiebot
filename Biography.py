#!/usr/bin/python
# -*- coding: utf-8 -*-
import pywikibot
import re
import sys
import codecs
import datetime
import pagegenerators

class Biography:
    
    def __init__(self):
        print "biography"
        self.templateName = ''
        self.pages = list()
        self.generator
        self.months
        
    def loadArticlesByTemplate(self):
        site = pywikibot.getSite('cs', 'wikipedia')
        page = pywikibot.Page(site, u'Template:' + self.templateName)
        self.generator = pagegenerators.ReferringPageGenerator(page)        
        
    def getMonths(self):
        self.months = {
                'leden':1,
                'ledna':1,
                'únor':2,
                'února':2,
                'březen':3,
                'března':3,
                'duben':4,
                'dubna':4,
                'květen':5,
                'května':5,
                'červen':6,
                'června':6,
                'červenec':7,
                'červenec':7,
                'srpen':8,
                'sprna':8,
                'září':9,
                'říjen':10,
                'října':10,
                'listopad':11,
                'listopadu':11,
                'prosinec':12,
                'prosince':12,                
            }
        
    def start(self):
        print "nene"
        
    def processPage(self, page):        
        # and page.title() == u'Jevgenij Malkin'
        if (page.namespace() == 0):
            print page.title()
            templates = page.templatesWithParams()
            try:
                get_date_from_tpl = False
                ok = False
                for t in templates:
                    if (t[0] == u'Datum narození a věk' or t[0] == u'Datum narození' or t[0] == u'Věk' or t[0] == u'věkvletechadnech' or t[0] == u'věk v letech a dnech'):
                        #print t[1]                                
                        date_array = t[1]                                
                        get_date_from_tpl = True
                        ok = True
                        
                    if (t[0] == unicode(self.templateName)):
                        for param in t[1]:                            
                            result = re.split('\=+', param)                            
                            if (result[0].strip() == u'datum narození'):
                                ok = True                                
                                if (not get_date_from_tpl):
                                    date = result[1].strip().replace('[', '').replace(']','')
                                    
                                    if (re.search('\|', date)):                                        
                                        res = re.split('\|+', date)
                                        date = res[1].strip()
                                    else:
                                        date = date                                                                            
                                    for mesic,cislo in self.months.iteritems():                                        
                                        if (re.search(mesic, date)):                                            
                                            month = cislo
                                            date = date.replace(unicode(mesic), str(cislo) + '.').replace(' ', '')
                                            
                                            if (re.search('u', date)):
                                                #fix listopadu -> 11.u
                                                date = date.replace('u', '')
                                                
                                            if (re.search('nbsp', date)):
                                                #fix listopadu -> 11.u
                                                date = date.replace('&nbsp;', '')
                                                
                                            date = date.replace(',', '')
                                            print date    
                                            try:
                                                mat = re.match('(\d+).(\d+).(\d+)', date)
                                                date_array = mat.groups()
                                            except AttributeError:
                                                ok = False
                                                                                
                
                try:
                    if (ok):                            
                        if (get_date_from_tpl):
                            year = date_array[0]
                            month = date_array[2]
                            day = date_array[1]
                        else:
                            year = date_array[2]
                            month = date_array[1]
                            day = date_array[0]
                                                    
                        print page.title() + ' - ' + day + '. ' + month + '. ' + year
                        self.save(page, year, month, day)                        
                    else:
                        print page.title() + ' - not ok'
                        self.logError(page.title() + ' not ok')
                    self.logComplete(page.title())
                except UnboundLocalError:
                    pass
            except ValueError:
                print "err"
                print sys.exc_info()
        
    def save(self, page, year, month, day):
        refes = {'P143' : [{
                "snaktype": "value",
                "property":"P143",
                "datavalue":{'type': 'value', 'value': 'Q191168'}
                }]}
        
        refes = {('P143','Q191168')}
                         
        wikidata = pywikibot.DataPage(page)
        date = str(year) + '-' + str(month) + '-' + str(day)
        continue_with_adding = True
        dictionary = wikidata.get()
        
        for c in dictionary['claims']:
            if (c['m'][1] == 569):
                continue_with_adding = False
                  
        print continue_with_adding      
        if (continue_with_adding):
            my_date = '+0000000'+datetime.datetime(int(year), int(month), int(day)).isoformat()+'Z'
            wikidata.editclaim('P569', my_date, data_type="time", refs=refes, override=False)            
            
    def logComplete(self, title):
        file = codecs.open("log_" + self.typeEntity + ".txt", "a", "utf-8")
        file.write(title + '\n')
        file.close()
        
    def logError(self, title):
        file = codecs.open("log_error_" + self.typeEntity + ".txt", "a", "utf-8")
        file.write(title + '\n')
        file.close()
        
    def isCompleteFile(self, entity):
        try:
            file = codecs.open("log_" + self.typeEntity + ".txt", "r", "utf-8")
            lines = file.readlines()
            list_entity = list()
            file.close()
            for it in lines:                
                list_entity.append(it.strip())
            #TODO opravit!
                    
            try:            
                list_entity.index(entity)
                return True
            except ValueError:
                return False
        except IOError:
            print "Not exist file"
            codecs.open('log_' + self.typeEntity + '.txt', 'w', 'utf-8')
        
class BiographyHockey(Biography):
        def __init__(self):
            self.typeEntity = 'hockey'
            self.templateName = 'Infobox - hokejista'
            print self.templateName
        
        def start(self):
            self.loadArticlesByTemplate()
            
        def process(self):
            self.getMonths()
            for page in self.generator:
                if (not self.isCompleteFile(page.title())):
                    self.processPage(page)

class BiographyActor(Biography):
    def __init__(self):
        self.typeEntity = 'actor'
        self.templateName = 'Infobox - herec'
        print self.templateName
        
    def start(self):
        self.loadArticlesByTemplate()
        
    def processByList(self):
        file = codecs.open("log_error_" + self.typeEntity + ".txt", "r", "utf-8")
        lines = file.readlines()
        list_entity = list()
        file.close()
        for it in lines:                
            list_entity.append(it.strip())
        site = pywikibot.getSite('cs','wikipedia')
        for lp in list_entity:
            page = pywikibot.Page(site, lp)
            self.processPage(page)
        
        
    def process(self):
        self.getMonths()        
        for page in self.generator:
            if (not self.isCompleteFile(page.title())):
                self.processPage(page)
