#!/usr/bin/python
# -*- coding: utf-8 -*-
import re
from logger import Logger
import untangle
import pywikibot
import requests
import urllib.parse
from wikidatafun import *
import json

import unicodecsv
from pywikibot import pagegenerators, WikidataBot
from pywikibot.textlib import mwparserfromhell as parser


class excel_semicolon(unicodecsv.Dialect):
    """Describe the usual properties of Excel-generated CSV files."""
    delimiter = ';'
    quotechar = '"'
    doublequote = True
    skipinitialspace = False
    lineterminator = '\r\n'
    quoting = unicodecsv.QUOTE_MINIMAL


import sys


# reload(sys) # just to be sure
# sys.setdefaultencoding('utf-8')
# from math import radians, cos, sin, asin, sqrt

class streets():

    def __init__(self, logger_name, streetsDict, category, city_name, city_nameCs, city_nameEn, load_only=False):
        self.errors = {}
        self.translator = {}
        # print 'streets'
        # self.csv = []
        self.logger = Logger(logger_name + u'Fill', 'saved')

        self.actual_page = None
        self.wikidata = None
        self.language_wikipedia = 'cs'
        self.wikidata_family = 'wikidata'
        self.family = 'wikipedia'

        self.instance = 'square'
        self.pageOnly = True
        self.commonsCategory = category
        self.city_name = city_name
        self.city_nameCs = city_nameCs
        self.city_nameEn = city_nameEn
        # self.get_contribs()
        self.exclude = [
            u'Římské náměstí (Brno)',
            u'I/31 Road',
            u'Moskevská (Brno)',
            u'Koželužská (Tišnov)',
            u'Jirchářská (Tišnov)',
            u'Emigrantů',
            u'Farní (Příbor)',
            u'Farní (Sloup v Čechách)',
            u'Divadelní (Teplice)',
            u'Winterova (Teplice)',
            u'Streets in Kuří',
            u'Squares in Moravská Ostrava a Přívoz',
            u'Squares in Brno City',
            u'Squares in Masarykova čtvrť',
            u'Squares in Frýdek-Místek by name',
            u'Squares in Frýdek',
            u'Squares in Místek',
            u'Former squares in Karlovy Vary',
            u'Squares in Josefov (Jaroměř)',
            u'Katův plácek (Kadaň)',
            u'Squares in Kutná Hora by district',
            u'Squares in Kutná Hora by name',
            u'Tyršovo náměstí (Hořice)',
            u'Husovo náměstí (Kamenice nad Lipou)',
            u'Squares in Ostrava by district',
            u'Squares in Ostrava by name',
            u'Town Hall in Český Těšín',
            u'Street signs in Máslovice',
            u'Streets in Fojtka',
            u'Streets in Dolní Křečany',
            u'Löschnerovo náměstí',
            u'Náměstí 28. října (Nechanice)',
            u'Horní náměstí (Tachov)',
            u'House numbers in Brno',
            u'Bikeways in Brno',
            u'Streets in Brno City',
            u'Street furniture in Brno',
            u'Streets in Brno-Líšeň',
            u'Streets in Masarykova čtvrť',
            u'Roadworks in Brno',
            u'Embankments in Děčín',
            u'Streets in Frýdek-Místek by district',
            u'Streets in Frýdek-Místek by name',
            u'Street lights in Frýdek-Místek',
            u'Street lights in Frýdlant nad Ostravicí',
            u'Street signs in Havlíčkův Brod',
            u'Embankments in Hradec Králové',
            u'Streets in Josefov (Jaroměř)',
            u'Expressway R1 in Jesenice',
            u'Street signs in Jičín',
            u'Paths in Karlovy Vary',
            u'Former streets in Karlovy Vary',
            u'Streets in Kutná Hora by district',
            u'Cobblestones in Kutná Hora',
            u'Streets in Břuchotín',
            u'Streets in Křelov',
            u'Pavements in Litovel',
            u'Roundabouts in Louny',
            u'Road I/13 in Most',
            u'Street signs in Nepomuk',
            u'Street furniture in Nepomuk',
            u'Embankments in Olomouc',
            u'Street signs in Olomouc',
            u'Expressway R35 in Olomouc',
            u'Streets in Ostrava by district',
            u'Streets in Ostrava by name',
            u'Intersections in Ostrava',
            u'Embankments in Pardubice',
            u'Road I/36 in Pardubice',
            u'Road I/37 in Pardubice',
            u'Embankments in Písek',
            u'Passages in Písek',
            u'Highway D5 in Rudná',
            u'Street signs in Sloup v Čechách',
            u'Embankments in Sušice',
            u'Road I/8 in Teplice',
            u'Road I/13 in Teplice',
            u'Embankments in Trutnov',
            u'Road I/35 in Turnov',
            u'Squares in Třinec',
            u'Embankments in Uherské Hradiště',
            u'Embankments in Uherské Hradiště',
            u'Streets in Mařatice',
            u'Streets in Rybárny',
            u'Road I/4 in Volyně',
            u'Embankments in Zlín',
            u'Embankments in České Budějovice',
            u'Street signs in Český Krumlov',
            u'Street signs in Český Těšín',
            u'Streets in Kuří',
            u'Palackého náměstí (Vizovice)',
            u'Papírové náměstí (Liberec)',
            u'Wenceslas Square'
        ]

        # print (load_only)

        # if not load_only:
        #     self.streets = streetsDict
        #     self.add_data()
        # print ''
        # self.streets_from_csv = self.load_csv()
        # self.street_create_text()
        # self.load_translator()
        self.new_streets() #dulezite pro vkladani ulic novyhc
        # self.mestska_cast()
        # self.correct_coordinates()
        # self.genFactory.handleArg(u'-page:Seznam kulturních památek v Moravské Třebové')
        # self.generator = self.genFactory.getCombinedGenerator()
        # self.contribs()
        # self.load_file()
        # monument_dictionary = self.load_dict()

        # self.import_to_templates(monument_dictionary)
        # self.import_to_wikidata(monument_dictionary)
        # self.check_correct(monument_dictionary)

    def contribs(self):
        query = '''

select ?item ?itemLabel ?coord where {
{?item wdt:P31 wd:Q79007.
      ?item wdt:P17 wd:Q213.
      ?item wdt:P131 wd:Q1085.
  ?item wdt:P625 ?coord.
 OPTIONAL{?item wdt:P18 ?pic.}

}UNION{
  ?item wdt:P31 wd:Q174782.
      ?item wdt:P17 wd:Q213.
      ?item wdt:P131 wd:Q1085.
  ?item wdt:P625 ?coord.
   OPTIONAL{?item wdt:P18 ?pic.}
}
SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
}

            '''
        url = 'https://query.wikidata.org/bigdata/namespace/wdq/sparql?query=%s' % (urllib.parse.quote(query))
        url = '%s&format=json' % (url)
        print(url)
        site = pywikibot.Site('wikidata', 'wikidata')
        repo = site.data_repository()
        sparql = getURL(url)
        json1 = loadSPARQL(sparql=sparql)
        # print(json1)
        qlist = []
        arr = []
        for result in json1['results']['bindings']:
            try:
                arr = {}
                arr['item'] = result['item']['value'].split('/entity/')[1]
                arr['itemLabel'] = result['itemLabel']['value']
                regex = r"Point\(([0-9]+\.[0-9]+) ([0-9]+\.[0-9]+)\)"
                matches = re.search(regex, result['coord']['value'])
                grp = matches.groups()
                arr['lat'] = grp[0]
                arr['lon'] = grp[1]
                if arr not in qlist:
                    qlist.append(arr)
            except IndexError as e:
                pass
        newarr = []
        count = 0
        for q in qlist:
            try:
                count = count + 1
                # item = pywikibot.ItemPage(repo, q['item'])
                # revs = item.revisions()
                createdDict = {}
                createdDict['name'] = q['itemLabel']
                createdDict['lat'] = q['lat']
                createdDict['lon'] = q['lon']
                createdDict['weekYear'] = ""
                createdDict['date'] = ""
                createdDict['month'] = ""
                createdDict['year'] = ""
                createdDict['week'] = ""
                createdDict['user'] = ""
                # print(createdDict)
                print(count)
                newarr.append(createdDict)
                # for rev in revs:
                #     match = re.search(u'(.*)P18(.*)',rev.comment)
                #     if not match:
                #         pass
                #     else:
                #         # print(match.groups())
                #         # print(rev.timestamp.strftime("%Y%W"))
                #         createdDict['weekYear'] = rev.timestamp.strftime("%Y%W")
                #         createdDict['date'] = rev.timestamp.strftime("%Y-%m-%d")
                #         createdDict['month'] = rev.timestamp.strftime("%m")
                #         createdDict['year'] = rev.timestamp.strftime("%Y")
                #         createdDict['week'] = rev.timestamp.strftime("%W")
                #         createdDict['user'] = rev.user
                #         # print(createdDict)
                #         newarr.append(createdDict)
                #


            except Exception as e:
                pass
            with open('streetdataAll.json', 'w') as outfile:
                json.dump(newarr, outfile)

    def load_translator(self):
        self.translator[u'Rovensko pod Troskami'] = 'Rovensku pod Troskami'
        self.translator[u'Lipník nad Bečvou'] = 'Lipníku nad Bečvou'
        self.translator[u'Seč (okres Chrudim)'] = 'Seči'
        self.translator[u'Choltice'] = 'Cholticích'
        self.translator[u'Bystré (okres Svitavy)'] = 'Bystré'
        self.translator[u'Dobřany'] = 'Dobřanech'
        self.translator[u'Netolice'] = 'Netolicích'
        self.translator[u'Boskovice'] = 'Boskovicích'
        self.translator[u'Golčův Jeníkov'] = 'Golčově Jeníkově'
        self.translator[u'Uherský Ostroh'] = 'Uherském Ostrohu'
        self.translator[u'Brumov-Bylnice'] = 'Brumově'
        self.translator[u'Březno (okres Chomutov)'] = 'Březně'
        #
        self.translator[u'Soběslav'] = 'Soběslavi'
        self.translator[u'Studénka'] = 'Studénce'
        self.translator[u'Tachov'] = 'Tachově'
        self.translator[u'Trutnov'] = 'Trutnově'
        self.translator[u'Uherské Hradiště'] = 'Uherském Hradišti'
        self.translator[u'Znojmo'] = 'Znojmě'
        self.translator[u'Žďár nad Sázavou'] = 'Žďáru nad Sázavou'
        self.translator[u'Louny'] = 'Lounech'
        self.translator[u'Kadaň'] = 'Kadani'
        self.translator[u'Cheb'] = 'Chebu'
        self.translator[u'České Budějovice'] = 'Českých Budějovicích'
        self.translator[u'Vimperk'] = 'Vimperku'
        self.translator[u'Brno'] = 'Brně'
        self.translator[u'Praha'] = 'Praze'
        self.translator[u'Opava'] = 'Opavě'
        self.translator[u'Teplice'] = 'Teplicích'
        self.translator[u'Karlovy Vary'] = 'Karlových Varech'
        self.translator[u'Praha'] = 'Praze'
        self.translator[u'Teplice'] = 'Teplicích'
        self.translator[u'Praha'] = 'Praze'
        self.translator[u'Praha'] = 'Praze'
        self.translator[u'Brno'] = 'Brně'

        #
        self.translator[u'Louňovice pod Blaníkem'] = 'Louňovicích pod Blaníkem'
        self.translator[u'Pyšely'] = 'Pyšelech'
        #
        self.translator[u'Sázava (okres Benešov)'] = 'Sázavě'
        self.translator[u'Trhový Štěpánov'] = 'Trhovém Štěpánově'
        self.translator[u'Hostomice (okres Beroun)'] = 'Hostomicích'
        self.translator[u'Hořovice'] = 'Hořovicích'
        self.translator[u'Komárov (okres Beroun)'] = 'Komárově'
        self.translator[u'Králův Dvůr'] = 'Králově Dvoře'
        self.translator[u'Liteň'] = 'Litni'
        self.translator[u'Srbsko (okres Beroun)'] = 'Srbsku'
        self.translator[u'Zdice'] = 'Zdicích'
        self.translator[u'Buštěhrad'] = 'Buštěhradě'
        self.translator[u'Lány (okres Kladno)'] = 'Lány'
        self.translator[u'Stochov'] = 'Stochově'
        self.translator[u'Unhošť'] = 'Unhošti'
        self.translator[u'Velvary'] = 'Velvarech'
        self.translator[u'Zlonice'] = 'Zlonicích'
        self.translator[u'Zásmuky'] = 'Zásmukách'
        self.translator[u'Malešov'] = 'Malešově'
        self.translator[u'Nové Dvory (okres Kutná Hora)'] = 'Nových Dvorech'
        self.translator[u'Uhlířské Janovice'] = 'Uhlířských Janovicích'
        self.translator[u'Kostelec nad Labem'] = 'Kostelci nad Labem'
        self.translator[u'Mšeno'] = 'Mšeně'
        self.translator[u'Všetaty (okres Mělník)'] = 'Všetatech'
        self.translator[u'Veltrusy'] = 'Veltrusech'
        self.translator[u'Bakov nad Jizerou'] = 'Bakově nad Jizerou'
        self.translator[u'Benátky nad Jizerou'] = 'Benátkách nad Jizerou'
        self.translator[u'Katusice'] = 'Katusicích'
        self.translator[u'Mnichovo Hradiště'] = 'Mnichově Hradišti'
        self.translator[u'Rožďalovice'] = 'Rožďalovicích'
        self.translator[u'Sadská'] = 'Sadské'
        self.translator[u'Klecany'] = 'Klecanech'
        self.translator[u'Odolena Voda'] = 'Odolene Vodě'
        self.translator[u'Stříbrná Skalice'] = 'Stříbrné Skalici'
        self.translator[u'Dolní Břežany'] = 'Dolních Břežanech'
        self.translator[u'Hostivice'] = 'Hostivici'
        self.translator[u'Jeneč'] = 'Jenči'
        self.translator[u'Jílové u Prahy'] = 'Jílovém u Prahy'
        self.translator[u'Neratovice'] = 'Neratovicích'
        self.translator[u'Liběchov'] = 'Liběchově'
        self.translator[u'Březnice'] = 'Březnici'
        self.translator[u'Kosova Hora'] = 'Kosově Hoře'
        self.translator[u'Krásná Hora nad Vltavou'] = 'Krásné Hoře nad Vltavou'
        self.translator[u'Nový Knín'] = 'Novém Kníně'
        self.translator[u'Rožmitál pod Třemšínem'] = 'Rožmitále pod Třemšínem'
        self.translator[u'Nové Strašecí'] = 'Novém Strašecí'
        self.translator[u'Chlumec nad Cidlinou'] = 'Chlumci nad Cidlinou'
        self.translator[u'Třebechovice pod Orebem'] = 'Třebechovicích pod Orebem'
        self.translator[u'Kopidlno'] = 'Kopidlně'
        self.translator[u'Libáň'] = 'Libáni'
        self.translator[u'Lázně Bělohrad'] = 'Lázních Bělohrad'
        self.translator[u'Miletín'] = 'Miletíně'
        self.translator[u'Mlázovice'] = 'Mlázovicích'
        self.translator[u'Nová Paka'] = 'Nové Pace'
        self.translator[u'Pecka (okres Jičín)'] = 'Pecce'
        self.translator[u'Železnice (okres Jičín)'] = 'Železnice'
        self.translator[u'Machov'] = 'Machově'
        self.translator[u'Stárkov'] = 'Stárkově'
        self.translator[u'Červený Kostelec'] = 'Červeném Kostelci'
        self.translator[u'Česká Skalice'] = 'České Skalici'
        self.translator[u'Solnice (okres Rychnov nad Kněžnou)'] = 'Solnici'
        self.translator[u'Týniště nad Orlicí'] = 'Týništi nad Orlicí'
        self.translator[u'Malé Svatoňovice'] = 'Malých Svatoňovicích'
        self.translator[u'Pilníkov'] = 'Pilníkově'
        self.translator[u'Svoboda nad Úpou'] = 'Svobodě nad Úpou'
        self.translator[u'Úpice'] = 'Úpici'
        self.translator[u'Černý Důl'] = 'Černém Dole'
        self.translator[u'Žacléř'] = 'Žacléři'
        self.translator[u'Františkovy Lázně'] = 'Františkových Lázních'
        self.translator[u'Luby'] = 'Lubech'
        self.translator[u'Teplá'] = 'Teplé'
        self.translator[u'Bečov nad Teplou'] = 'Bečově nad Teplou'
        self.translator[u'Bochov'] = 'Bochově'
        self.translator[u'Chyše'] = 'Chyši'
        self.translator[u'Horní Blatná'] = 'Horní Blatné'
        self.translator[u'Hroznětín'] = 'Hroznětíně'
        self.translator[u'Jáchymov'] = 'Jáchymově'
        self.translator[u'Nejdek'] = 'Nejdku'
        self.translator[u'Vysoké nad Jizerou'] = 'Vysokém nad Jizerou'
        self.translator[u'Dubá'] = 'Dubé'
        self.translator[u'Stráž pod Ralskem'] = 'Stráži pod Ralskem'
        self.translator[u'Kravaře'] = 'Kravařích'
        self.translator[u'Chrastava'] = 'Chrastavě'
        self.translator[u'Hodkovice nad Mohelkou'] = 'Hodkovicích nad Mohelkou'
        self.translator[u'Hrádek nad Nisou'] = 'Hrádku nad Nisou'
        self.translator[u'Nové Město pod Smrkem'] = 'Novém Městě pod Smrkem'
        self.translator[u'Osečná'] = 'Osečné'
        self.translator[u'Dvorce (okres Bruntál)'] = 'Dvorcích'
        self.translator[u'Město Albrechtice'] = 'Městě Albrechtice'
        self.translator[u'Osoblaha'] = 'Osoblaze'
        self.translator[u'Ryžoviště (okres Bruntál)'] = 'Ryžovišti'
        self.translator[u'Vrbno pod Pradědem'] = 'Vrbně pod Pradědem'
        self.translator[u'Brušperk'] = 'Brušperku'
        self.translator[u'Frýdlant nad Ostravicí'] = 'Frýdlantě nad Ostravicí'
        self.translator[u'Orlová'] = 'Orlové'
        self.translator[u'Bílovec'] = 'Bílovci'
        self.translator[u'Frenštát pod Radhoštěm'] = 'Frenštátě pod Radhoštěm'
        self.translator[u'Fulnek'] = 'Fulneku'
        self.translator[u'Odry'] = 'Odrách'
        self.translator[u'Příbor (okres Nový Jičín)'] = 'Příboře'
        self.translator[u'Žacléř'] = 'Žacléři'
        self.translator[u'Studénka'] = 'Studénce'
        self.translator[u'Štramberk'] = 'Štramberku'
        self.translator[u'Hlučín'] = 'Hlučíně'
        self.translator[u'Vítkov'] = 'Vítkově'
        self.translator[u'Klimkovice'] = 'Klimkovicích'
        self.translator[u'Loštice'] = 'Lošticích'
        self.translator[u'Staré Město (okres Šumperk)'] = 'Starém Městě'
        self.translator[u'Štíty'] = 'Štítech'
        self.translator[u'Javorník (okres Jeseník)'] = 'Javorníku'
        self.translator[u'Vidnava'] = 'Vidnavě'
        self.translator[u'Zlaté Hory'] = 'Zlatých Horách'
        self.translator[u'Křelov-Břuchotín'] = 'Křelově'
        self.translator[u'Moravský Beroun'] = 'Moravském Berouně'
        self.translator[u'Velký Újezd'] = 'Velkém Újezdu'
        self.translator[u'Konice'] = 'Konicích'
        self.translator[u'Kostelec na Hané'] = 'Kostelci na Hané'
        self.translator[u'Brodek u Přerova'] = 'Brodku u Přerova'
        self.translator[u'Dřevohostice'] = 'Dřevohosticích'
        self.translator[u'Hustopeče nad Bečvou'] = 'Hustopečích nad Bečvou'
        self.translator[u'Tovačov'] = 'Tovačově'
        self.translator[u'Chroustovice'] = 'Chroustovicích'
        self.translator[u'Luže'] = 'Lužích'
        self.translator[u'Nasavrky'] = 'Nasavrkách'
        self.translator[u'Proseč'] = 'Proseči'
        self.translator[u'Ronov nad Doubravou'] = 'Ronově nad Doubravou'
        #
        self.translator[u'Trhová Kamenice'] = 'Trhové Kamenici'
        self.translator[u'Třemošnice'] = 'Třemošnici'
        self.translator[u'Žumberk'] = 'Žumberku'
        #
        self.translator[u'Dašice'] = 'Dašicích'
        self.translator[u'Holice'] = 'Holicích'
        self.translator[u'Horní Jelení'] = 'Horním Jelení'
        self.translator[u'Moravany (okres Pardubice)'] = 'Moravanech'
        self.translator[u'Sezemice (okres Pardubice)'] = 'Sezemicích'
        self.translator[u'Březová nad Svitavou'] = 'Březové nad Svitavou'
        #
        self.translator[u'Žacléř'] = 'Žacléři'
        self.translator[u'Svitavy'] = 'Svitavách'
        self.translator[u'Brandýs nad Orlicí'] = 'Brandýse nad Orlicí'
        self.translator[u'Choceň'] = 'Chocni'
        self.translator[u'Jablonné nad Orlicí'] = 'Jablonném nad Orlicí'
        self.translator[u'Bělá nad Radbuzou'] = 'Bělé nad Radbuzou'
        self.translator[u'Nýrsko'] = 'Nýrsku'
        self.translator[u'Plánice'] = 'Plánicích'
        self.translator[u'Rabí'] = 'Rabí'
        self.translator[u'Strážov'] = 'Strážově'
        self.translator[u'Velhartice'] = 'Velharticích'
        self.translator[u'Železná Ruda'] = 'Železné Rudě'
        self.translator[u'Starý Plzenec'] = 'Starém Plzenci'
        self.translator[u'Manětín'] = 'Manětíně'
        self.translator[u'Žacléř'] = 'Žacléři'
        self.translator[u'Blovice'] = 'Blovicích'
        #
        self.translator[u'Žacléř'] = 'Žacléři'
        self.translator[u'Přeštice'] = 'Přešticích'
        self.translator[u'Spálené Poříčí'] = 'Spáleném Poříčí'
        self.translator[u'Kladruby (okres Tachov)'] = 'Kladrubech'
        self.translator[u'Planá'] = 'Plané'
        self.translator[u'Přimda'] = 'Přimdě'
        self.translator[u'Černošín'] = 'Černošíně'
        self.translator[u'Chlum u Třeboně'] = 'Chlumu u Třeboně'
        self.translator[u'Kunžak'] = 'Kunžaku'
        self.translator[u'Husinec (okres Prachatice)'] = 'Husinci'
        #
        self.translator[u'Vimperk'] = 'Vimperku'
        self.translator[u'Vlachovo Březí'] = 'Vlachově Březí'
        self.translator[u'Volary'] = 'Volarech'
        self.translator[u'Mirotice'] = 'Miroticích'
        self.translator[u'Mirovice'] = 'Mirovicích'
        self.translator[u'Bavorov'] = 'Bavorově'
        self.translator[u'Bělčice'] = 'Bělčicích'
        self.translator[u'Vodňany'] = 'Vodňanech'
        self.translator[u'Bechyně'] = 'Bechyni'
        self.translator[u'Jistebnice'] = 'Jistebnici'
        self.translator[u'Borovany'] = 'Borovanech'
        self.translator[u'Nové Hrady'] = 'Nových Hradech'
        self.translator[u'Římov (okres České Budějovice)'] = 'Římově'
        self.translator[u'Žacléř'] = 'Žacléři'
        self.translator[u'Žacléř'] = 'Žacléři'
        self.translator[u'Žacléř'] = 'Žacléři'
        self.translator[u'Frymburk'] = 'Frymburku'
        self.translator[u'Žacléř'] = 'Žacléři'
        self.translator[u'Žacléř'] = 'Žacléři'
        self.translator[u'Kaplice'] = 'Kaplici'
        self.translator[u'Besednice'] = 'Besednici'
        self.translator[u'Žacléř'] = 'Žacléři'
        self.translator[u'Velešín'] = 'Velešíně'
        self.translator[u'Vyšší Brod'] = 'Vyšším Brodě'
        #
        self.translator[u'Kunštát'] = 'Kunštátě'
        self.translator[u'Olešnice (okres Blansko)'] = 'Olešnici'
        self.translator[u'Černá Hora (okres Blansko)'] = 'Černé Hoře'
        self.translator[u'Blučina'] = 'Blučině'
        self.translator[u'Kuřim'] = 'Kuřimi'
        self.translator[u'Modřice'] = 'Modřicích'
        self.translator[u'Pohořelice (okres Brno-venkov)'] = 'Pohořelicích'
        self.translator[u'Veverská Bítýška'] = 'Veverské Bítýšce'
        self.translator[u'Šlapanice'] = 'Šlapanicích'
        self.translator[u'Židlochovice'] = 'Židlochovicích'
        self.translator[u'Drnholec'] = 'Drnholci'
        self.translator[u'Klobouky u Brna'] = 'Kloboukách u Brna'
        self.translator[u'Lanžhot'] = 'Lanžhotě'
        self.translator[u'Lednice (okres Břeclav)'] = 'Lednici'
        self.translator[u'Valtice'] = 'Valticích'
        self.translator[u'Velké Pavlovice'] = 'Velkých Pavlovicích'
        self.translator[u'Bzenec'] = 'Bzenci'
        self.translator[u'Bučovice'] = 'Bučovicích'
        self.translator[u'Bučovice'] = 'Černčíně'
        self.translator[u'Rousínov'] = 'Rousínově'
        self.translator[u'Hnanice'] = 'Hnanicích'
        self.translator[u'Žacléř'] = 'Žacléři'
        self.translator[u'Hrušovany nad Jevišovkou'] = 'Hrušovanech nad Jevišovkou'
        self.translator[u'Jaroslavice'] = 'Jaroslavicích'
        self.translator[u'Miroslav (okres Znojmo)'] = 'Miroslavi'
        self.translator[u'Žacléř'] = 'Žacléři'
        self.translator[u'Vranov nad Dyjí'] = 'Vranově nad Dyjí'
        self.translator[u'Chotěboř'] = 'Chotěboři'
        #
        self.translator[u'Habry'] = 'Habrech'
        self.translator[u'Havlíčkova Borová'] = 'Havlíčkově Borové'
        self.translator[u'Havlíčkův Brod'] = 'Havlíčkově Brodě'
        self.translator[u'Žacléř'] = 'Žacléři'
        self.translator[u'Přibyslav'] = 'Přibyslavi'
        self.translator[u'Batelov'] = 'Batelově'
        self.translator[u'Nová Říše'] = 'Nové Říší'
        self.translator[u'Horní Cerekev'] = 'Horní Cerekvi'
        self.translator[u'Pacov'] = 'Pacově'
        self.translator[u'Žirovnice'] = 'Žirovnici'
        self.translator[u'Žacléř'] = 'Žacléři'
        self.translator[u'Hrotovice'] = 'Hrotovicích'
        self.translator[u'Jaroměřice nad Rokytnou'] = 'Jaroměřicích nad Rokytnou'
        self.translator[u'Žacléř'] = 'Žacléři'
        self.translator[u'Moravské Budějovice'] = 'Moravských Budějovicích'
        self.translator[u'Žacléř'] = 'Žacléři'
        self.translator[u'Želetava'] = 'Želetavě'
        self.translator[u'Bystřice nad Pernštejnem'] = 'Bystřici nad Pernštejnem'
        self.translator[u'Jimramov'] = 'Jimramově'
        self.translator[u'Křižanov (okres Žďár nad Sázavou)'] = 'Křižanově'
        self.translator[u'Velká Bíteš'] = 'Velké Bíteši'
        self.translator[u'Velké Meziříčí'] = 'Velkém Meziříčí'
        self.translator[u'Chropyně'] = 'Chropyni'
        self.translator[u'Hulín'] = 'Hulíně'
        self.translator[u'Žacléř'] = 'Žacléři'
        self.translator[u'Morkovice-Slížany'] = 'Morkovicích-Slížanech'
        self.translator[u'Buchlovice'] = 'Buchlovice'
        self.translator[u'Kunovice'] = 'Kunovicích'
        #
        self.translator[u'Žacléř'] = 'Žacléři'
        self.translator[u'Rožnov pod Radhoštěm'] = 'Rožnově pod Radhoštěm'
        self.translator[u'Valašské Meziříčí'] = 'Valašském Meziříčí'
        #
        self.translator[u'Fryšták'] = 'Fryštáku'
        self.translator[u'Napajedla'] = 'Napajedlech'
        self.translator[u'Otrokovice'] = 'Otrokovicích'
        self.translator[u'Slušovice'] = 'Slušovicích'
        self.translator[u'Tlumačov (okres Zlín)'] = 'Tlumačově'
        self.translator[u'Valašské Klobouky'] = 'Valašských Kloboukách'
        #
        self.translator[u'Jirkov'] = 'Jirkově'
        self.translator[u'Klášterec nad Ohří'] = 'Klášterci nad Ohří'
        self.translator[u'Mašťov'] = 'Mašťově'
        self.translator[u'Bohušovice nad Ohří'] = 'Bohušovicích nad Ohří'
        self.translator[u'Brozany nad Ohří'] = 'Brozanech nad Ohří'
        self.translator[u'Žacléř'] = 'Žacléři'
        self.translator[u'Lovosice'] = 'Lovosicích'
        self.translator[u'Terezín'] = 'Terezíně'
        self.translator[u'Úštěk'] = 'Úštěku'
        self.translator[u'Cítoliby'] = 'Cítolibech'
        self.translator[u'Postoloprty'] = 'Postoloprtech'
        self.translator[u'Vroutek'] = 'Vroutku'
        self.translator[u'Meziboří'] = 'Meziboří'
        self.translator[u'Lom (okres Most)'] = 'Lom'
        self.translator[u'Bílina (město)'] = 'Bílině'
        self.translator[u'Osek (okres Teplice)'] = 'Oseku'
        self.translator[u'Chabařovice'] = 'Chabařovicích'
        self.translator[u'Žacléř'] = 'Žacléři'
        self.translator[u'Blansko'] = 'Blansku'
        self.translator[u'Bor (okres Tachov)'] = 'Boru'
        self.translator[u'Brno'] = 'Brně'
        self.translator[u'Doksy'] = 'Doksech'
        self.translator[u'Duchcov'] = 'Duchcově'
        self.translator[u'Dvůr Králové nad Labem'] = 'Dvoře Králové nad Labem'
        self.translator[u'Havířov'] = 'Havířově'
        self.translator[u'Horní Slavkov'] = 'Horním Slavkově'
        self.translator[u'Horšovský Týn'] = 'Horšovském Týně'
        self.translator[u'Jáchymov'] = 'Jáchymově'
        self.translator[u'Karlovy Vary'] = 'Karlových Varech'
        self.translator[u'Kladruby (okres Tachov)'] = 'Kladrubech'
        self.translator[u'Kojetín'] = 'Kojetíně'
        self.translator[u'Lom (okres Most)'] = 'Lomu'
        self.translator[u'Mladá Boleslav'] = 'Mladé Boleslavi'
        self.translator[u'Neratovice'] = 'Neratovicích'
        self.translator[u'Nové Hrady'] = 'Nových Hradech'
        self.translator[u'Nové Město nad Metují'] = 'Novém Městě nad Metují'
        self.translator[u'Olomouc'] = 'Olomouci'
        self.translator[u'Opava'] = 'Opavě'
        self.translator[u'Oslavany'] = 'Oslavanech'
        self.translator[u'Ostrava'] = 'Ostravě'
        self.translator[u'Pardubice'] = 'Pardubicích'
        self.translator[u'Plzeň'] = 'Plzni'
        self.translator[u'Praha'] = 'Praze'

        #
        self.translator[u'Svratka (okres Žďár nad Sázavou)'] = 'Svratce'
        self.translator[u'Tábor'] = 'Táboře'
        self.translator[u'Vizovice'] = 'Vizovici'
        self.translator[u'Jilemnice'] = 'Jilemnici'
        self.translator[u'Chomutov'] = 'Chomutově'
        self.translator[u'Liberec'] = 'Liberci'
        self.translator[u'Brno'] = 'Brně'
        #
        self.translator[u'Český Dub'] = 'Českém Dubu'
        self.translator[u'Hrob (okres Teplice)'] = 'Hrobě'
        self.translator[u'Holešov'] = 'Holešově'
        self.translator[u'Počátky'] = 'Počátkách'
        self.translator[u'Moravský Krumlov'] = 'Moravském Krumlově'
        self.translator[u'Strážnice (okres Hodonín)'] = 'Strážnici'
        self.translator[u'Hustopeče'] = 'Hustopečích'
        self.translator[u'Týn nad Vltavou'] = 'Týně nad Vltavou'
        self.translator[u'Dačice'] = 'Dačicích'
        self.translator[u'Poběžovice'] = 'Poběžovicích'
        self.translator[u'Žamberk'] = 'Žamberku'
        self.translator[u'Horní Slavkov'] = 'Horním Slavkově'
        self.translator[u'Budišov nad Budišovkou'] = 'Budišově nad Budišovkou'
        #
        self.translator[u'Brno'] = 'Brně'
        # #
        self.translator[u'Frýdek-Místek'] = 'Frýdku'
        self.translator[u'Frýdek-Místek'] = 'Místku'
        self.translator[u'Frýdek-Místek'] = 'Frýdku-Místku'
        # #
        self.translator[u'Jaroměř'] = 'Josefově (Jaroměř)'
        self.translator[u'Kutná Hora'] = 'Kutné Hoře'
        self.translator[u'Ostrava'] = 'Ostravě'
        self.translator[u'Ostrava'] = 'Ostravě'
        self.translator[u'Ostrava'] = 'Ostravě'
        #
        self.translator[u'Adamov (okres Blansko)'] = 'Adamově'
        self.translator[u'Bělá pod Bezdězem'] = 'Bělé pod Bezdězem'
        self.translator[u'Benešov nad Ploučnicí'] = 'Benešově nad Ploučnicí'
        self.translator[u'Beroun'] = 'Berouně'
        self.translator[u'Blansko'] = 'Blansku'
        self.translator[u'Blatná'] = 'Blatné'
        self.translator[u'Bohumín'] = 'Bohumíně'
        self.translator[u'Bor (okres Tachov)'] = 'Boru'
        self.translator[u'Brandýs nad Labem-Stará Boleslav'] = 'Brandýse nad Labem-Staré Boleslavi'
        self.translator[u'Brno'] = 'Brně'
        self.translator[u'Broumov'] = 'Broumově'
        self.translator[u'Brtnice'] = 'Brtnici'
        self.translator[u'Bruntál'] = 'Bruntále'
        self.translator[u'Budyně nad Ohří'] = 'Budyni nad Ohří'
        self.translator[u'Bystřice pod Hostýnem'] = 'Bystřici pod Hostýnem'
        self.translator[u'Břeclav'] = 'Břeclavi'
        #
        self.translator[u'Cheb'] = 'Chebu'
        self.translator[u'Chrast'] = 'Chrasti'
        self.translator[u'Chrudim'] = 'Chrudimi'
        self.translator[u'Cvikov'] = 'Cvikově'
        #
        self.translator[u'Divišov'] = 'Divišově'
        self.translator[u'Dobruška'] = 'Dobrušce'
        self.translator[u'Doksy'] = 'Doksech'
        self.translator[u'Dolní Kounice'] = 'Dolních Kounicích'
        self.translator[u'Domažlice'] = 'Domažlicích'
        self.translator[u'Duchcov'] = 'Duchcově'
        self.translator[u'Dvůr Králové nad Labem'] = 'Dvoře Králové nad Labem'
        self.translator[u'Děčín'] = 'Děčíně'
        #
        self.translator[u'Frýdek-Místek'] = 'Frýdku-Místku'
        self.translator[u'Frýdlant'] = 'Frýdlantě'
        #
        self.translator[u'Havlíčkův Brod'] = 'Havlíčkově Brodě'
        self.translator[u'Havířov'] = 'Havířově'
        self.translator[u'Heřmanův Městec'] = 'Heřmanově Městci'
        self.translator[u'Hlinsko'] = 'Hlinsku'
        self.translator[u'Hodonín'] = 'Hodoníně'
        self.translator[u'Horažďovice'] = 'Horažďovicích'
        self.translator[u'Horšovský Týn'] = 'Horšovském Týně'
        self.translator[u'Hostinné'] = 'Hostinném'
        self.translator[u'Hořice'] = 'Hořicích'
        self.translator[u'Hradec Králové'] = 'Hradci Králové'
        self.translator[u'Hranice (okres Přerov)'] = 'Hranicích'
        self.translator[u'Hronov'] = 'Hronově'
        self.translator[u'Humpolec'] = 'Humpolci'
        #
        self.translator[u'Ivančice'] = 'Ivančicích'
        #
        self.translator[u'Jablonec nad Nisou'] = 'Jablonci nad Nisou'
        self.translator[u'Jablonné v Podještědí'] = 'Jablonném v Podještědí'
        self.translator[u'Jablunkov'] = 'Jablunkově'
        self.translator[u'Jaroměř'] = 'Jaroměři'
        self.translator[u'Jemnice'] = 'Jemnici'
        self.translator[u'Jeseník'] = 'Jeseníku'
        self.translator[u'Jevíčko'] = 'Jevíčku'
        self.translator[u'Jičín'] = 'Jičíně'
        self.translator[u'Jihlava'] = 'Jihlavě'
        #
        self.translator[u'Votice'] = 'Voticích'
        self.translator[u'Kladno'] = 'Kladně'
        self.translator[u'Kolín'] = 'Kolíně'
        self.translator[u'Dobříš'] = 'Dobříši'
        #
        self.translator[u'Jindřichův Hradec'] = 'Jindřichově Hradci'
        #
        self.translator[u'Kadaň'] = 'Kadani'
        self.translator[u'Kamenice nad Lipou'] = 'Kamenici nad Lipou'
        self.translator[u'Kamenický Šenov'] = 'Kamenickém Šenově'
        self.translator[u'Kardašova Řečice'] = 'Kardašově Řečici'
        self.translator[u'Karlovy Vary'] = 'Karlových Varech'
        self.translator[u'Karviná'] = 'Karviné'
        self.translator[u'Kašperské Hory'] = 'Kašperských Horách'
        self.translator[u'Klatovy'] = 'Klatovech'
        self.translator[u'Kojetín'] = 'Kojetíně'
        self.translator[u'Kostelec nad Orlicí'] = 'Kostelci nad Orlicí'
        self.translator[u'Kostelec nad Černými lesy'] = 'Kostelci nad Černými lesy'
        self.translator[u'Kouřim'] = 'Kouřimi'
        self.translator[u'Králíky'] = 'Králíkách'
        self.translator[u'Kralovice'] = 'Kralovicích'
        self.translator[u'Kralupy nad Vltavou'] = 'Kralupech nad Vltavou'
        self.translator[u'Krásná Lípa'] = 'Krásné Lípě'
        self.translator[u'Krnov'] = 'Krnově'
        self.translator[u'Kroměříž'] = 'Kroměříži'
        self.translator[u'Krupka'] = 'Krupce'
        self.translator[u'Krásno'] = 'Krásně'
        self.translator[u'Kutná Hora'] = 'Kutné Hoře'
        self.translator[u'Kyjov'] = 'Kyjově'
        #
        self.translator[u'Lanškroun'] = 'Lanškrouně'
        self.translator[u'Lázně Bohdaneč'] = 'Lázních Bohdaneč'
        self.translator[u'Ledeč nad Sázavou'] = 'Ledči nad Sázavou'
        self.translator[u'Letohrad'] = 'Letohradě'
        self.translator[u'Libochovice'] = 'Libochovicích'
        self.translator[u'Libčice nad Vltavou'] = 'Libčicích nad Vltavou'
        self.translator[u'Litomyšl'] = 'Litomyšli'
        self.translator[u'Litoměřice'] = 'Litoměřicích'
        self.translator[u'Litovel'] = 'Litovli'
        self.translator[u'Litvínov'] = 'Litvínově'
        self.translator[u'Loket (okres Sokolov)'] = 'Lokti'
        self.translator[u'Lomnice (okres Brno-venkov)'] = 'Lomnici'
        self.translator[u'Lomnice nad Lužnicí'] = 'Lomnici nad Lužnicí'
        self.translator[u'Lomnice nad Popelkou'] = 'Lomnici nad Popelkou'
        self.translator[u'Louny'] = 'Lounech'
        self.translator[u'Luhačovice'] = 'Luhačovicích'
        self.translator[u'Lysice'] = 'Lysicích'
        self.translator[u'Lysá nad Labem'] = 'Lysé nad Labem'
        #
        self.translator[u'Mariánské Lázně'] = 'Mariánských Lázních'
        self.translator[u'Mělník'] = 'Mělníku'
        self.translator[u'Mikulov'] = 'Mikulově'
        self.translator[u'Milevsko'] = 'Milevsku'
        self.translator[u'Mimoň'] = 'Mimoni'
        self.translator[u'Mladá Boleslav'] = 'Mladé Boleslavi'
        self.translator[u'Mníšek pod Brdy'] = 'Mníšku pod Brdy'
        self.translator[u'Mohelnice'] = 'Mohelnici'
        self.translator[u'Moravská Třebová'] = 'Moravské Třebové'
        self.translator[u'Most (město)'] = 'Mostě'
        self.translator[u'Město Touškov'] = 'Městě Touškově'
        #
        self.translator[u'Nechanice'] = 'Nechanicích'
        self.translator[u'Nepomuk'] = 'Nepomuku'
        self.translator[u'Neveklov'] = 'Neveklově'
        self.translator[u'Nová Bystřice'] = 'Nové Bystřici'
        self.translator[u'Nové Město na Moravě'] = 'Novém Městě na Moravě'
        self.translator[u'Nové Město nad Metují'] = 'Novém Městě nad Metují'
        self.translator[u'Nový Bor'] = 'Novém Boru'
        self.translator[u'Nový Jičín'] = 'Novém Jičíně'
        self.translator[u'Nymburk'] = 'Nymburku'
        self.translator[u'Náchod'] = 'Náchodě'
        self.translator[u'Náměšť nad Oslavou'] = 'Náměšti nad Oslavou'
        self.translator[u'Němčice nad Hanou'] = 'Němčicích nad Hanou'
        #
        self.translator[u'Olomouc'] = 'Olomouci'
        self.translator[u'Opava'] = 'Opavě'
        self.translator[u'Opočno'] = 'Opočně'
        self.translator[u'Oslavany'] = 'Oslavanech'
        self.translator[u'Ostrava'] = 'Ostravě'
        self.translator[u'Ostrov (okres Karlovy Vary)'] = 'Ostrově'
        #
        self.translator[u'Pardubice'] = 'Pardubicích'
        self.translator[u'Pelhřimov'] = 'Pelhřimově'
        self.translator[u'Peruc'] = 'Peruci'
        self.translator[u'Pečky'] = 'Pečkách'
        self.translator[u'Písek (město)'] = 'Písku'
        self.translator[u'Plumlov'] = 'Plumlově'
        self.translator[u'Plzeň'] = 'Plzni'
        self.translator[u'Poděbrady'] = 'Poděbradech'
        self.translator[u'Police nad Metují'] = 'Polici nad Metují'
        self.translator[u'Polička'] = 'Poličce'
        self.translator[u'Polná'] = 'Polné'
        self.translator[u'Prachatice'] = 'Prachaticích'
        self.translator[u'Praha'] = 'Praze'
        self.translator[u'Prostějov'] = 'Prostějově'
        self.translator[u'Protivín'] = 'Protivíně'
        self.translator[u'Přelouč'] = 'Přelouči'
        self.translator[u'Přerov'] = 'Přerově'
        self.translator[u'Příbram'] = 'Příbrami'
        #
        self.translator[u'Rakovník'] = 'Rakovníku'
        self.translator[u'Rokycany'] = 'Rokycanech'
        self.translator[u'Rokytnice v Orlických horách'] = 'Rokytnici v Orlických horách'
        self.translator[u'Rosice (okres Brno-venkov)'] = 'Rosicích'
        self.translator[u'Roudnice nad Labem'] = 'Roudnici nad Labem'
        self.translator[u'Roztoky (okres Praha-západ)'] = 'Roztokách'
        self.translator[u'Rudná (okres Praha-západ)'] = 'Rudné'
        self.translator[u'Rumburk'] = 'Rumburku'
        self.translator[u'Rychnov nad Kněžnou'] = 'Rychnově nad Kněžnou'
        self.translator[u'Rýmařov'] = 'Rýmařově'
        #
        self.translator[u'Sedlec-Prčice'] = 'Sedleci-Prčicích'
        self.translator[u'Sedlčany'] = 'Sedlčanech'
        self.translator[u'Semily'] = 'Semilech'
        self.translator[u'Sezimovo Ústí'] = 'Sezimově Ústí'
        self.translator[u'Skuteč'] = 'Skutči'
        self.translator[u'Slaný'] = 'Slaném'
        self.translator[u'Slavkov u Brna'] = 'Slavkově u Brna'
        self.translator[u'Slavonice'] = 'Slavonicích'
        self.translator[u'Smržovka'] = 'Smržovce'
        self.translator[u'Sobotka'] = 'Sobotce'
        self.translator[u'Soběslav'] = 'Soběslavi'
        self.translator[u'Sokolov'] = 'Sokolově'
        self.translator[u'Staré Město (okres Uherské Hradiště)'] = 'Starém Městě'
        self.translator[u'Stochov'] = 'Stochově'
        self.translator[u'Strakonice'] = 'Strakonicích'
        self.translator[u'Stráž nad Nežárkou'] = 'Stráži nad Nežárkou'
        self.translator[u'Stříbro (okres Tachov)'] = 'Stříbře'
        self.translator[u'Sušice'] = 'Sušici'
        self.translator[u'Svitavy'] = 'Svitavách'
        #
        #
        #
        self.translator[u'Tachov'] = 'Tachově'
        self.translator[u'Telč'] = 'Telči'
        self.translator[u'Teplice'] = 'Teplicích'
        self.translator[u'Tišnov'] = 'Tišnově'
        self.translator[u'Toužim'] = 'Toužimi'
        self.translator[u'Trhové Sviny'] = 'Trhových Svinech'
        self.translator[u'Trutnov'] = 'Trutnově'
        self.translator[u'Turnov'] = 'Turnově'
        self.translator[u'Třebenice (okres Litoměřice)'] = 'Třebenicích'
        self.translator[u'Třeboň'] = 'Třeboni'
        self.translator[u'Třebíč'] = 'Třebíči'
        self.translator[u'Třešť'] = 'Třešti'
        self.translator[u'Třinec'] = 'Třinci'
        #
        self.translator[u'Uherské Hradiště'] = 'Uherském Hradišti'
        self.translator[u'Uherský Brod'] = 'Uherském Brodě'
        self.translator[u'Uničov'] = 'Uničově'
        #
        self.translator[u'Vamberk'] = 'Vamberku'
        self.translator[u'Veselí nad Lužnicí'] = 'Veselí nad Lužnicí'
        self.translator[u'Veselí nad Moravou'] = 'Veselí nad Moravou'
        #
        self.translator[u'Vlašim'] = 'Vlašimi'
        self.translator[u'Volyně'] = 'Volyni'
        self.translator[u'Vrchlabí'] = 'Vrchlabí'
        self.translator[u'Vsetín'] = 'Vsetíně'
        self.translator[u'Vysoké Mýto'] = 'Vysokém Mýtě'
        self.translator[u'Vyškov'] = 'Vyškově'
        #
        self.translator[u'Zábřeh'] = 'Zábřehu'
        self.translator[u'Zákupy'] = 'Zákupech'
        self.translator[u'Zlín'] = 'Zlíně'
        self.translator[u'Znojmo'] = 'Znojmě'
        self.translator[u'Zruč nad Sázavou'] = 'Zruči nad Sázavou'
        #
        self.translator[u'Ústí nad Labem'] = 'Ústí nad Labem'
        self.translator[u'Ústí nad Orlicí'] = 'Ústí nad Orlicí'
        #
        self.translator[u'Česká Kamenice'] = 'České Kamenici'
        self.translator[u'Česká Lípa'] = 'České Lípě'
        self.translator[u'Česká Třebová'] = 'České Třebové'
        self.translator[u'České Budějovice'] = 'Českých Budějovicích'
        self.translator[u'Český Brod'] = 'Českém Brodě'
        self.translator[u'Český Krumlov'] = 'Českém Krumlově'
        self.translator[u'Český Těšín'] = 'Českém Těšíně'
        self.translator[u'Čáslav'] = 'Čáslavi'
        #
        self.translator[u'Řevnice'] = 'Řevnicích'
        self.translator[u'Říčany'] = 'Říčanech'
        #
        self.translator[u'Šluknov'] = 'Šluknově'
        self.translator[u'Šternberk'] = 'Šternberku'
        self.translator[u'Šumperk'] = 'Šumperku'
        #
        self.translator[u'Žatec'] = 'Žatci'
        self.translator[u'Železný Brod'] = 'Železném Brodě'
        self.translator[u'Žlutice'] = 'Žluticích'
        self.translator[u'Žulová'] = 'Žulové'
        self.translator[u'Žďár nad Sázavou'] = 'Žďáru nad Sázavou'
        #
        self.translator[u'Šestajovice (okres Praha-východ)'] = 'Šestajovicích'
        self.translator[u'Čečelice'] = 'Čečelicích'

        self.translator[u'Čečelice'] = 'Čečelicích'
        self.translator[u'Všetaty (okres Mělník)'] = 'Všetatech'
        self.translator[u'Byšice'] = 'Byšicích'
        self.translator[u'Nové Dvory (okres Kutná Hora)'] = 'Nových Dvorech'
        self.translator[u'Středokluky'] = 'Středoklukách'
        self.translator[u'Všenory'] = 'Všenorech'
        self.translator[u'Máslovice'] = 'Máslovicích'
        self.translator[u'Mšeno'] = 'Mšeně'
        self.translator[u'Liteň'] = 'Bělči (Liteň)'
        self.translator[u'Smržovka'] = 'Smržovce'
        self.translator[u'Tanvald'] = 'Tanvaldu'
        self.translator[u'Jeřmanice'] = 'Jeřmanicích'
        self.translator[u'Mníšek'] = 'Mníšku'
        self.translator[u'Hejnice'] = 'Hejnicích'
        self.translator[u'Mníšek'] = 'Fojtce'
        self.translator[u'Hodkovice nad Mohelkou'] = 'Hodkovicích nad Mohelkou'
        self.translator[u'Hrádek nad Nisou'] = 'Hrádku nad Nisou'
        self.translator[u'Doksy'] = 'Starých Splavech'
        self.translator[u'Stráž pod Ralskem'] = 'Stráži pod Ralskem'
        self.translator[u'Klatovy'] = 'Klatovech'
        self.translator[u'Kladruby (okres Tachov)'] = 'Kladrubech'
        self.translator[u'Stříbro (okres Tachov)'] = 'Stříbře'
        self.translator[u'Tachov'] = 'Tachově'
        self.translator[u'Vlachovo Březí'] = 'Vlachově Březí'
        self.translator[u'Tišnov'] = 'Tišnově'
        self.translator[u'Kunštát'] = 'Kunštátě'
        self.translator[u'Bílovice nad Svitavou'] = 'Bílovicích nad Svitavou'
        self.translator[u'Mariánské Radčice'] = 'Mariánských Radčicích'
        self.translator[u'Meziboří'] = 'Meziboří'
        self.translator[u'Rumburk'] = 'Rumburku'
        self.translator[u'Rumburk'] = 'Dolních Křečanech'
        #
        self.translator[u'Abertamy'] = 'Abertamech'
        self.translator[u'Rybitví'] = 'Rybitví'
        self.translator[u'Samotišky'] = 'Samotiškách'
        self.translator[u'Osek (okres Teplice)'] = 'Oseku'
        self.translator[u'Adamov (okres Blansko)'] = 'Adamově'
        #
        self.translator[u'Bechyně'] = 'Bechyni'
        self.translator[u'Benešov'] = 'Benešově'
        self.translator[u'Beroun'] = 'Berouně'
        self.translator[u'Bečov nad Teplou'] = 'Bečově nad Teplou'
        self.translator[u'Bílina (město)'] = 'Bílině'
        self.translator[u'Blansko'] = 'Blansku'
        self.translator[u'Bohumín'] = 'Bohumíně'
        self.translator[u'Boskovice'] = 'Boskovicích'
        self.translator[u'Brandýs nad Labem-Stará Boleslav'] = 'Brandýse nad Labem'
        self.translator[u'Brandýs nad Orlicí'] = 'Brandýse nad Orlicí'
        self.translator[u'Brno'] = 'Brně'
        self.translator[u'Bruntál'] = 'Bruntále'
        self.translator[u'Budyně nad Ohří'] = 'Budyni nad Ohří'
        self.translator[u'Břeclav'] = 'Břeclavi'
        #
        self.translator[u'Cheb'] = 'Chebu'
        self.translator[u'Chlumec (okres Ústí nad Labem)'] = 'Chlumci'
        self.translator[u'Choceň'] = 'Chocni'
        self.translator[u'Chomutov'] = 'Chomutově'
        self.translator[u'Chotěboř'] = 'Chotěboři'
        self.translator[u'Chrast'] = 'Chrasti'
        self.translator[u'Chrastava'] = 'Chrastavě'
        self.translator[u'Chrudim'] = 'Chrudimi'
        self.translator[u'Cvikov'] = 'Cvikově'
        #
        self.translator[u'Dačice'] = 'Dačicích'
        self.translator[u'Desná'] = 'Desné'
        self.translator[u'Dobřichovice'] = 'Dobřichovicích'
        self.translator[u'Dobříš'] = 'Dobříš'
        self.translator[u'Doksy'] = 'Doksech'
        self.translator[u'Doksy (okres Kladno)'] = 'Doksech'
        self.translator[u'Domažlice'] = 'Domažlicích'
        self.translator[u'Dubá'] = 'Dubé'
        self.translator[u'Duchcov'] = 'Duchcově'
        self.translator[u'Dvorce (okres Bruntál)'] = 'Dvorcích'
        self.translator[u'Dvůr Králové nad Labem'] = 'Dvoře Králové nad Labem'
        self.translator[u'Děčín'] = 'Děčíně'
        #
        self.translator[u'Františkovy Lázně'] = 'Františkových Lázních'
        self.translator[u'Frenštát pod Radhoštěm'] = 'Frenštátě pod Radhoštěm'
        self.translator[u'Frýdek-Místek'] = 'Frýdku-Místku'
        self.translator[u'Frýdlant'] = 'Frýdlantě'
        self.translator[u'Frýdlant nad Ostravicí'] = 'Frýdlantě nad Ostravicí'
        self.translator[u'Fulnek'] = 'Fulneku'
        #
        self.translator[u'Kutná Hora'] = 'Kutné Hoře'
        self.translator[u'Křelov-Břuchotín'] = 'Břuchotíně'
        self.translator[u'Křelov-Břuchotín'] = 'Křelově'
        self.translator[u'Ostrava'] = 'Ostravě'
        self.translator[u'Ostrava'] = 'Ostravě'
        self.translator[u'Ostrava'] = 'Ostravě'
        self.translator[u'Ostrava'] = 'Ostravě'
        self.translator[u'Ostrava'] = 'Ostravě'
        self.translator[u'Říčany'] = 'Kuří'
        self.translator[u'Frýdek-Místek'] = 'Místku'
        self.translator[u'Frýdek-Místek'] = 'Frýdku'
        self.translator[u'Frýdek-Místek'] = 'Frýdku-Místku'
        self.translator[u'Uherské Hradiště'] = 'Rybárnách'
        #
        self.translator[u'Havlíčkův Brod'] = 'Havlíčkově Brodě'
        self.translator[u'Havířov'] = 'Havířově'
        self.translator[u'Hlinsko'] = 'Hlinsku'
        self.translator[u'Hodonín'] = 'Hodoníně'
        self.translator[u'Holešov'] = 'Holešově'
        self.translator[u'Holice'] = 'Holicích'
        self.translator[u'Hora Svaté Kateřiny'] = 'Hoře Svaté Kateřiny'
        self.translator[u'Horní Blatná'] = 'Horní Blatné'
        self.translator[u'Horšovský Týn'] = 'Horšovském Týně'
        self.translator[u'Hostinné'] = 'Hostinném'
        self.translator[u'Hostivice'] = 'Hostivici'
        self.translator[u'Hořice'] = 'Hořicích'
        self.translator[u'Hradec Králové'] = 'Hradci Králové'
        self.translator[u'Hranice (okres Přerov)'] = 'Hranicích'
        self.translator[u'Hronov'] = 'Hronově'
        self.translator[u'Hustopeče'] = 'Hustopečích'
        #
        self.translator[u'Ivančice'] = 'Ivančicích'
        #
        self.translator[u'Jablonné v Podještědí'] = 'Jablonném v Podještědí'
        self.translator[u'Jablonec nad Nisou'] = 'Jablonci nad Nisou'
        self.translator[u'Jaroměř'] = 'Jaroměři'
        self.translator[u'Jesenice (okres Praha-západ)'] = 'Jesenici'
        self.translator[u'Jeseník'] = 'Jeseníku'
        self.translator[u'Jihlava'] = 'Jihlavě'
        self.translator[u'Jilemnice'] = 'Jilemnici'
        self.translator[u'Jílové u Prahy'] = 'Jílovém u Prahy'
        self.translator[u'Jimramov'] = 'Jimramově'
        self.translator[u'Jindřichův Hradec'] = 'Jindřichově Hradci'
        self.translator[u'Jirkov'] = 'Jirkově'
        self.translator[u'Jičín'] = 'Jičíně'
        self.translator[u'Jaroměř'] = 'Josefově (Jaroměř)'
        #
        self.translator[u'Kadaň'] = 'Kadani'
        self.translator[u'Kamenický Šenov'] = 'Kamenickém Šenově'
        self.translator[u'Karlovy Vary'] = 'Karlových Varech'
        self.translator[u'Karviná'] = 'Karviné'
        self.translator[u'Kašperské Hory'] = 'Kašperských Horách'
        self.translator[u'Kladno'] = 'Kladně'
        self.translator[u'Klobouky u Brna'] = 'Kloboukách u Brna'
        self.translator[u'Klášterec nad Ohří'] = 'Klášterci nad Ohří'
        self.translator[u'Kolín'] = 'Kolíně'
        self.translator[u'Kopidlno'] = 'Kopidlně'
        self.translator[u'Kostelec nad Labem'] = 'Kostelci nad Labem'
        self.translator[u'Kostelec nad Orlicí'] = 'Kostelci nad Orlicí'
        self.translator[u'Kostelec nad Černými lesy'] = 'Kostelci nad Černými lesy'
        self.translator[u'Kouřim'] = 'Kouřimi'
        self.translator[u'Kralovice'] = 'Kralovicích'
        self.translator[u'Kralupy nad Vltavou'] = 'Kralupech nad Vltavou'
        self.translator[u'Krásná Lípa'] = 'Krásné Lípě'
        self.translator[u'Krnov'] = 'Krnově'
        self.translator[u'Kroměříž'] = 'Kroměříži'
        self.translator[u'Králův Dvůr'] = 'Králově Dvoře'
        self.translator[u'Kutná Hora'] = 'Kutné Hoře'
        self.translator[u'Kyjov'] = 'Kyjově'
        self.translator[u'Křelov-Břuchotín'] = 'Křelově-Břuchotíně'
        #
        self.translator[u'Libčice nad Vltavou'] = 'Libčicích nad Vltavou'
        self.translator[u'Liběchov'] = 'Liběchově'
        self.translator[u'Lipník nad Bečvou'] = 'Lipníku nad Bečvou'
        self.translator[u'Liteň'] = 'Litni'
        self.translator[u'Litomyšl'] = 'Litomyšli'
        self.translator[u'Litoměřice'] = 'Litoměřicích'
        self.translator[u'Litovel'] = 'Litovli'
        self.translator[u'Litvínov'] = 'Litvínově'
        # f = streets('streets', streetsDict, u'Category:Streets in Loket', 'Loket (okres Sokolov)', 'Lokti', 'Loket',)
        self.translator[u'Lom (okres Most)'] = 'Lomu'
        self.translator[u'Lomnice nad Popelkou'] = 'Lomnici nad Popelkou'
        self.translator[u'Louny'] = 'Lounech'
        self.translator[u'Lovosice'] = 'Lovosicích'
        self.translator[u'Luhačovice'] = 'Luhačovicích'
        self.translator[u'Lysice'] = 'Lysicích'
        self.translator[u'Lysá nad Labem'] = 'Lysé nad Labem'
        #
        self.translator[u'Malešov'] = 'Malešově'
        self.translator[u'Mariánské Lázně'] = 'Mariánských Lázních'
        self.translator[u'Mělník'] = 'Mělníku'
        self.translator[u'Mikulov'] = 'Mikulově'
        self.translator[u'Milevsko'] = 'Milevsku'
        self.translator[u'Mimoň'] = 'Mimoni'
        self.translator[u'Miroslav (okres Znojmo)'] = 'Miroslavi'
        self.translator[u'Mladá Boleslav'] = 'Mladé Boleslavi'
        self.translator[u'Mnichovo Hradiště'] = 'Mnichově Hradišti'
        self.translator[u'Mohelnice'] = 'Mohelnici'
        self.translator[u'Moravská Třebová'] = 'Moravské Třebové'
        self.translator[u'Moravské Budějovice'] = 'Moravských Budějovicích'
        self.translator[u'Most (město)'] = 'Mostě'
        #
        self.translator[u'Napajedla'] = 'Napajedlech'
        self.translator[u'Nechanice'] = 'Nechanicích'
        self.translator[u'Nepomuk'] = 'Nepomuku'
        self.translator[u'Neratovice'] = 'Neratovicích'
        self.translator[u'Neveklov'] = 'Neveklově'
        #
        self.translator[u'Nové Město nad Metují'] = 'Novém Městě nad Metují'
        self.translator[u'Nový Bor'] = 'Novém Boru'
        self.translator[u'Nový Jičín'] = 'Novém Jičíně'
        self.translator[u'Nymburk'] = 'Nymburku'
        self.translator[u'Náchod'] = 'Náchodě'
        #
        self.translator[u'Olomouc'] = 'Olomouci'
        self.translator[u'Opava'] = 'Opavě'
        self.translator[u'Ostrava'] = 'Ostravě'
        self.translator[u'Otrokovice'] = 'Otrokovicích'
        #
        self.translator[u'Pardubice'] = 'Pardubicích'
        self.translator[u'Pelhřimov'] = 'Pelhřimově'
        self.translator[u'Pečky'] = 'Pečkách'
        self.translator[u'Písek (město)'] = 'Písku'
        self.translator[u'Plzeň'] = 'Plzni'
        self.translator[u'Poděbrady'] = 'Poděbradech'
        self.translator[u'Pohled (okres Havlíčkův Brod)'] = 'Pohledu'
        self.translator[u'Polička'] = 'Poličce'
        self.translator[u'Polná'] = 'Polné'
        self.translator[u'Prachatice'] = 'Prachaticích'
        self.translator[u'Příbram'] = 'Příbrami'
        self.translator[u'Prostějov'] = 'Prostějově'
        self.translator[u'Protivín'] = 'Protivíně'
        self.translator[u'Pyšely'] = 'Pyšelech'
        self.translator[u'Přerov'] = 'Přerově'
        self.translator[u'Přeštice'] = 'Přešticích'
        self.translator[u'Přibyslav'] = 'Přibyslavi'
        self.translator[u'Příbor (okres Nový Jičín)'] = 'Příboře'
        #
        self.translator[u'Rakovník'] = 'Rakovníku'
        self.translator[u'Raspenava'] = 'Raspenavě'
        self.translator[u'Rokycany'] = 'Rokycanech'
        self.translator[u'Rosice (okres Brno-venkov)'] = 'Rosicích'
        self.translator[u'Roudnice nad Labem'] = 'Roudnici nad Labem'
        self.translator[u'Roztoky (okres Praha-západ)'] = 'Roztokách'
        self.translator[u'Rožmitál pod Třemšínem'] = 'Rožmitále pod Třemšínem'
        self.translator[u'Rudná (okres Praha-západ)'] = 'Rudné'
        self.translator[u'Rychnov nad Kněžnou'] = 'Rychnově nad Kněžnou'
        self.translator[u'Rýmařov'] = 'Rýmařově'
        #
        self.translator[u'Semily'] = 'Semilech'
        self.translator[u'Sezimovo Ústí'] = 'Sezimově Ústí'
        self.translator[u'Skuteč'] = 'Skutči'
        self.translator[u'Slaný'] = 'Slaném'
        self.translator[u'Slavkov u Brna'] = 'Slavkově u Brna'
        self.translator[u'Slavonice'] = 'Slavonicích'
        self.translator[u'Sloup v Čechách'] = 'Sloupu v Čechách'
        self.translator[u'Sobotka'] = 'Sobotce'
        self.translator[u'Soběslav'] = 'Soběslavi'
        self.translator[u'Sokolov'] = 'Sokolově'
        self.translator[u'Srbsko (okres Beroun)'] = 'Srbsku'
        self.translator[u'Staré Město (okres Uherské Hradiště)'] = 'Starém Městě'
        self.translator[u'Strakonice'] = 'Strakonicích'
        self.translator[u'Stříbrná Skalice'] = 'Stříbrné Skalici'
        self.translator[u'Sušice'] = 'Sušici'
        self.translator[u'Svatava (okres Sokolov)'] = 'Svatavě'
        self.translator[u'Svitavy'] = 'Svitavách'
        self.translator[u'Svoboda nad Úpou'] = 'Svobodě nad Úpou'
        #
        self.translator[u'Tábor'] = 'Táboře'
        self.translator[u'Telč'] = 'Telči'
        self.translator[u'Teplice'] = 'Teplicích'
        self.translator[u'Trutnov'] = 'Trutnově'
        self.translator[u'Turnov'] = 'Turnově'
        self.translator[u'Týn nad Vltavou'] = 'Týně nad Vltavou'
        self.translator[u'Třeboň'] = 'Třeboni'
        self.translator[u'Třešť'] = 'Třešti'
        self.translator[u'Třinec'] = 'Třinci'
        #
        self.translator[u'Uherské Hradiště'] = 'Uherském Hradišti'
        self.translator[u'Uherský Brod'] = 'Uherském Brodě'
        self.translator[u'Uničov'] = 'Uničově'
        #
        self.translator[u'Valašské Meziříčí'] = 'Valašském Meziříčí'
        self.translator[u'Valtice'] = 'Valticích'
        self.translator[u'Vamberk'] = 'Vamberku'
        self.translator[u'Varnsdorf'] = 'Varnsdorfu'
        self.translator[u'Velké Popovice'] = 'Velkých Popovicích'
        self.translator[u'Velké Přílepy'] = 'Velkých Přílepech'
        self.translator[u'Veltrusy'] = 'Veltrusech'
        self.translator[u'Velvary'] = 'Velvarech'
        self.translator[u'Veselí nad Lužnicí'] = 'Veselí nad Lužnicí'
        self.translator[u'Vimperk'] = 'Vimperku'
        self.translator[u'Vítkov'] = 'Vítkově'
        self.translator[u'Volyně'] = 'Volyni'
        self.translator[u'Votice'] = 'Voticích'
        self.translator[u'Vrchlabí'] = 'Vrchlabí'
        self.translator[u'Vsetín'] = 'Vsetíně'
        self.translator[u'Vysoké Mýto'] = 'Vysokém Mýtě'
        self.translator[u'Vyškov'] = 'Vyškově'
        #
        self.translator[u'Zákupy'] = 'Zákupech'
        self.translator[u'Zdice'] = 'Zdici'
        self.translator[u'Zlín'] = 'Zlíně'
        self.translator[u'Znojmo'] = 'Znojmě'
        self.translator[u'Zruč nad Sázavou'] = 'Zruči nad Sázavou'
        self.translator[u'Zábřeh'] = 'Zábřehu'
        #
        self.translator[u'Ústí nad Labem'] = 'Ústí nad Labem'
        self.translator[u'Ústí nad Orlicí'] = 'Ústí nad Orlicí'
        self.translator[u'Úštěk'] = 'Úštěku'
        #
        self.translator[u'Čáslav'] = 'Čáslavi'
        self.translator[u'Černošice'] = 'Černošicích'
        self.translator[u'Česká Lípa'] = 'České Lípě'
        self.translator[u'Česká Třebová'] = 'České Třebové'
        self.translator[u'České Budějovice'] = 'Českých Budějovicích'
        self.translator[u'Český Brod'] = 'Českém Brodě'
        self.translator[u'Český Dub'] = 'Českém Dubu'
        self.translator[u'Český Krumlov'] = 'Českém Krumlově'
        self.translator[u'Český Těšín'] = 'Českém Těšíně'
        #
        self.translator[u'Řevnice'] = 'Řevnicích'
        self.translator[u'Říčany'] = 'Říčanech'
        #
        self.translator[u'Šluknov'] = 'Šluknově'
        self.translator[u'Šternberk'] = 'Šternberku'
        self.translator[u'Štramberk'] = 'Štramberku'
        self.translator[u'Šumperk'] = 'Šumperku'
        #
        self.translator[u'Žatec'] = 'Žatci'
        self.translator[u'Žďár nad Sázavou'] = 'Žďáru nad Sázavou'
        self.translator[u'Žatec'] = 'Žatci'

        self.translator[u'Abertamy'] = 'Abertamech'
        self.translator[u'Adamov'] = 'Adamově'
        self.translator[u'Albrechtice'] = 'Albrechticích'
        self.translator[u'Albrechtice nad Orlicí'] = 'Albrechticích nad Orlicí'
        self.translator[u'Aš'] = 'Aši'
        self.translator[u'Babice'] = 'Babicích'
        self.translator[u'Babice u Rosic'] = 'Babicích u Rosic'
        self.translator[u'Bakov nad Jizerou'] = 'Bakově nad Jizerou'
        self.translator[u'Bařice-Velké Těšany'] = 'Bařicích-Velkých Těšanech'
        self.translator[u'Bašť'] = 'Bašti'
        self.translator[u'Batelov'] = 'Batelově'
        self.translator[u'Bavorov'] = 'Bavorově'
        self.translator[u'Bavoryně'] = 'Bavoryni'
        self.translator[u'Bdeněves'] = 'Bdeněvsi'
        self.translator[u'Bečov nad Teplou'] = 'Bečově nad Teplou'
        self.translator[u'Bedihošť'] = 'Bedihoti'
        self.translator[u'Bechyně'] = 'Bechyni'
        self.translator[u'Bělá nad Radbuzou'] = 'Bělé nad Radbuzou'
        self.translator[u'Bělá pod Bezdězem'] = 'Bělé pod Bezdězem'
        self.translator[u'Bělčice'] = 'Bělčici'
        self.translator[u'Běleč'] = 'Bělči'
        self.translator[u'Benátky nad Jizerou'] = 'Benátkách nad Jizerou'
        self.translator[u'Benešov'] = 'Benešově'
        self.translator[u'Benešov nad Ploučnicí'] = 'Benešově nad Ploučnicí'
        self.translator[u'Bernartice'] = 'Bernartici'
        self.translator[u'Beroun'] = 'Berouně'
        self.translator[u'Běrunice'] = 'Běrunicích'
        self.translator[u'Besednice'] = 'Besednicích'
        self.translator[u'Bezdružice'] = 'Bezdružicích'
        self.translator[u'Bezno'] = 'Bezně'
        self.translator[u'Bílina'] = 'Bílině'
        self.translator[u'Bílovec'] = 'Bílovci'
        self.translator[u'Bílovice nad Svitavou'] = 'Bílovicích nad Svitavou'
        self.translator[u'Blansko'] = 'Blansku'
        self.translator[u'Blatná'] = 'Blatné'
        self.translator[u'Blažovice'] = 'Blažovicích'
        self.translator[u'Blešno'] = 'Blešně'
        self.translator[u'Blovice'] = 'Blovicích'
        self.translator[u'Blšany'] = 'Blšanech'
        self.translator[u'Blučina'] = 'Blučině'
        self.translator[u'Bludov'] = 'Bludově'
        self.translator[u'Bobnice'] = 'Bobnicích'
        self.translator[u'Bohumín'] = 'Bohumíně'
        self.translator[u'Bohuňovice'] = 'Bohuňovicích'
        self.translator[u'Bohuslavice'] = 'Bohuslavicích'
        self.translator[u'Bohušovice nad Ohří'] = 'Bohušovicích nad Ohří'
        self.translator[u'Bochoř'] = 'Bochoři'
        self.translator[u'Bochov'] = 'Bochově'
        self.translator[u'Bojkovice'] = 'Bojkovicích'
        self.translator[u'Bolatice'] = 'Bolaticích'
        self.translator[u'Bor'] = 'Boru'
        self.translator[u'Borek'] = 'Borku'
        self.translator[u'Borohrádek'] = 'Borohrádku'
        self.translator[u'Borovany'] = 'Borovanech'
        self.translator[u'Boršov nad Vltavou'] = 'Boršově nad Vltavou'
        self.translator[u'Bořanovice'] = 'Bořanovicích'
        self.translator[u'Bořitov'] = 'Bořitově'
        self.translator[u'Boskovice'] = 'Boskovicích'
        self.translator[u'Bošovice'] = 'Bošovicích'
        self.translator[u'Bradlec'] = 'Bradleci'
        self.translator[u'Braňany'] = 'Braňanech'
        self.translator[u'Brandov'] = 'Brandově'
        self.translator[u'Brandýs nad Labem-Stará Boleslav'] = 'Brandýsi nad Labem-Staré Boleslavi'
        self.translator[u'Brandýs nad Orlicí'] = 'Brandýse nad Orlicí'
        self.translator[u'Brandýsek'] = 'Brandýsku'
        self.translator[u'Branka u Opavy'] = 'Brance u Opavy'
        self.translator[u'Brankovice'] = 'Brankovicích'
        self.translator[u'Braškov'] = 'Braškově'
        self.translator[u'Brno'] = 'Brně'
        self.translator[u'Brodce'] = 'Brodci'
        self.translator[u'Brodek u Prostějova'] = 'Brodku u Prostějova'
        self.translator[u'Brodek u Přerova'] = 'Brodku u Přerova'
        self.translator[u'Broumov'] = 'Broumově'
        self.translator[u'Broumy'] = 'Broumách'
        self.translator[u'Brozany nad Ohří'] = 'Brozanech nad Ohří'
        self.translator[u'Brtnice'] = 'Brtnici'
        self.translator[u'Brťov-Jeneč'] = 'Brťově-Jenči'
        self.translator[u'Brumov-Bylnice'] = 'Brumově-Bylnici'
        self.translator[u'Brumovice'] = 'Brumovicích'
        self.translator[u'Bruntál'] = 'Bruntále'
        self.translator[u'Brušperk'] = 'Brušperku'
        self.translator[u'Břeclav'] = 'Břeclavi'
        self.translator[u'Břehy'] = 'Břehách'
        self.translator[u'Březí'] = 'Březím'
        self.translator[u'Březnice'] = 'Březnici'
        self.translator[u'Březno'] = 'Březnu'
        self.translator[u'Březová'] = 'Březové'
        self.translator[u'Březová nad Svitavou'] = 'Březové nad Svitavou'
        self.translator[u'Březová-Oleško'] = 'Březové-Olešku'
        self.translator[u'Břidličná'] = 'Břidličné'
        self.translator[u'Břvany'] = 'Břvanech'
        self.translator[u'Bubovice'] = 'Bubovicích'
        self.translator[u'Bučovice'] = 'Bučovicích'
        self.translator[u'Budišov nad Budišovkou'] = 'Budišově nad Budišovkou'
        self.translator[u'Budišovice'] = 'Budišovicích'
        self.translator[u'Budyně nad Ohří'] = 'Budyni nad Ohří'
        self.translator[u'Buchlovice'] = 'Buchlovicích'
        self.translator[u'Buštěhrad'] = 'Buštěhradě'
        self.translator[u'Bystré'] = 'Bystrém'
        self.translator[u'Bystrovany'] = 'Bystrovanech'
        self.translator[u'Bystřany'] = 'Bystřanech'
        self.translator[u'Bystřice'] = 'Bystřici'
        self.translator[u'Bystřice nad Pernštejnem'] = 'Bystřici nad Pernštejnem'
        self.translator[u'Bystřice pod Hostýnem'] = 'Bystřici pod Hostýnem'
        self.translator[u'Byšice'] = 'Byšici'
        self.translator[u'Bzenec'] = 'Bzenci'
        self.translator[u'Cerhenice'] = 'Cerhenicích'
        self.translator[u'Cerhovice'] = 'Cerhovicích'
        self.translator[u'Cetkovice'] = 'Cetkovicích'
        self.translator[u'Církvice'] = 'Církvicích'
        self.translator[u'Cítoliby'] = 'Cítolibech'
        self.translator[u'Cizkrajov'] = 'Cizkrajově'
        self.translator[u'Cvikov'] = 'Cvikově'
        self.translator[u'Cvrčovice'] = 'Cvrčovicích'
        self.translator[u'Čachovice'] = 'Čachovicích'
        self.translator[u'Čakovičky'] = 'Čakovičkách'
        self.translator[u'Čáslav'] = 'Čáslavi'
        self.translator[u'Častolovice'] = 'Častolovicích'
        self.translator[u'Čavisov'] = 'Čavisově'
        self.translator[u'Čečelice'] = 'Čečelicích'
        self.translator[u'Čechtice'] = 'Čechticích'
        self.translator[u'Čechy pod Kosířem'] = 'Čechách pod Kosířem'
        self.translator[u'Čejkovice'] = 'Čejkovicích'
        self.translator[u'Čelákovice'] = 'Čelákovicích'
        self.translator[u'Čelechovice na Hané'] = 'Čelechovice na Hané'
        self.translator[u'Čeperka'] = 'Čeperkách'
        self.translator[u'Čerčany'] = 'Čerčany'
        self.translator[u'Černá Hora'] = 'Černé Hoře'
        self.translator[u'Černčice'] = 'Černčicích'
        self.translator[u'Černé Voděrady'] = 'Černých Voděradech'
        self.translator[u'Černolice'] = 'Černolicích'
        self.translator[u'Černošice'] = 'Černošicích'
        self.translator[u'Černošín'] = 'Černošíně'
        self.translator[u'Černovice'] = 'Černovicích'
        self.translator[u'Černožice'] = 'Černožicích'
        self.translator[u'Červené Pečky'] = 'Červených Pečkách'
        self.translator[u'Červenka'] = 'Července'
        self.translator[u'Červený Kostelec'] = 'Červeném Kostelci'
        self.translator[u'Červený Újezd'] = 'Červeném Újezdě'
        self.translator[u'Česká Kamenice'] = 'České Kamenici'
        self.translator[u'Česká Lípa'] = 'České Lípě'
        self.translator[u'Česká Skalice'] = 'České Skalici'
        self.translator[u'Česká Třebová'] = 'České Třebové'
        self.translator[u'Česká Ves'] = 'České Vsi'
        self.translator[u'České Budějovice'] = 'Českých Budějovicích'
        self.translator[u'České Meziříčí'] = 'Českém Meziříčí'
        self.translator[u'České Velenice'] = 'Českých Velenicích'
        self.translator[u'Český Brod'] = 'Českém Brodě'
        self.translator[u'Český Dub'] = 'Českém Dubu'
        self.translator[u'Český Krumlov'] = 'Českém Krumlově'
        self.translator[u'Český Těšín'] = 'Českém Těšíně'
        self.translator[u'Čestlice'] = 'Čestlicích'
        self.translator[u'Číchov'] = 'Číchově'
        self.translator[u'Čistá'] = 'Čisté'
        self.translator[u'Čížkovice'] = 'Čížkovicích'
        self.translator[u'Dačice'] = 'Dačicích'
        self.translator[u'Dalovice'] = 'Dalovicích'
        self.translator[u'Dambořice'] = 'Dambořicích'
        self.translator[u'Darkovice'] = 'Darkovicích'
        self.translator[u'Dašice'] = 'Dašicích'
        self.translator[u'Davle'] = 'Davli'
        self.translator[u'Děčín'] = 'Děčíně'
        self.translator[u'Děhylov'] = 'Děhylově'
        self.translator[u'Desná'] = 'Desné'
        self.translator[u'Deštná'] = 'Deštné'
        self.translator[u'Divec'] = 'Divci'
        self.translator[u'Divišov'] = 'Divišově'
        self.translator[u'Dlouhá Loučka'] = 'Dlouhé Loučce'
        self.translator[u'Dlouhá Třebová'] = 'Dlouhé Třebové'
        self.translator[u'Dlouhoňovice'] = 'Dlouhoňovicích'
        self.translator[u'Dlouhopolsko'] = 'Dlouhopolsku'
        self.translator[u'Dobrá Voda u Českých Budějovic'] = 'Dobré Vodě u Českých Budějovic'
        self.translator[u'Dobroměřice'] = 'Dobroměřicích'
        self.translator[u'Dobromilice'] = 'Dobromilicích'
        self.translator[u'Dobronín'] = 'Dobroníně'
        self.translator[u'Dobroslavice'] = 'Dobroslavicích'
        self.translator[u'Dobrovice'] = 'Dobrovicích'
        self.translator[u'Dobrovíz'] = 'Dobrovízi'
        self.translator[u'Dobruška'] = 'Dobrušce'
        self.translator[u'Dobřany'] = 'Dobřanech'
        self.translator[u'Dobřejovice'] = 'Dobřejovicích'
        self.translator[u'Dobřichovice'] = 'Dobřichovicích'
        self.translator[u'Dobříň'] = 'Dobříni'
        self.translator[u'Dobříš'] = 'Dobříši'
        self.translator[u'Dobšice'] = 'Dobšicích'
        self.translator[u'Dobšice'] = 'Dobšicích'
        self.translator[u'Doksy'] = 'Doksech'
        self.translator[u'Dolany nad Vltavou'] = 'Dolanech nad Vltavou'
        self.translator[u'Dolní Benešov'] = 'Dolním Benešově'
        self.translator[u'Dolní Beřkovice'] = 'Dolních Beřkovicích'
        self.translator[u'Dolní Bojanovice'] = 'Dolních Bojanovicích'
        self.translator[u'Dolní Bousov'] = 'Dolním Bousově'
        self.translator[u'Dolní Břežany'] = 'Dolních Břežanech'
        self.translator[u'Dolní Bukovsko'] = 'Dolním Bukovsko'
        self.translator[u'Dolní Dunajovice'] = 'Dolních Dunajovicích'
        self.translator[u'Dolní Dvořiště'] = 'Dolním Dvořiště'
        self.translator[u'Dolní Kounice'] = 'Dolních Kounicích'
        self.translator[u'Dolní Kralovice'] = 'Dolních Kralovicích'
        self.translator[u'Dolní Lhota'] = 'Dolní Lhotě'
        self.translator[u'Dolní Lutyně'] = 'Dolní Lutyni'
        self.translator[u'Dolní Němčí'] = 'Dolním Němčí'
        self.translator[u'Dolní Poustevna'] = 'Dolní Poustevně'
        self.translator[u'Dolní Radechová'] = 'Dolní Radechové'
        self.translator[u'Dolní Rychnov'] = 'Dolním Rychnově'
        self.translator[u'Dolní Ředice'] = 'Dolních Ředicích'
        self.translator[u'Dolní Zálezly'] = 'Dolních Zálezlech'
        self.translator[u'Dolní Životice'] = 'Dolních Životicích'
        self.translator[u'Domašov'] = 'Domašově'
        self.translator[u'Domašov nad Bystřicí'] = 'Domašov nad Bystřicí'
        self.translator[u'Domažlice'] = 'Domažlicích'
        self.translator[u'Doubravčice'] = 'Doubravčicích'
        self.translator[u'Doubravice nad Svitavou'] = 'Doubravicích nad Svitavou'
        self.translator[u'Doudleby nad Orlicí'] = 'Doudlebech nad Orlicí'
        self.translator[u'Drahelčice'] = 'Drahelčicích'
        self.translator[u'Dražeň'] = 'Draženi'
        self.translator[u'Dražovice'] = 'Dražovicích'
        self.translator[u'Drmoul'] = 'Drmouli'
        self.translator[u'Drnholec'] = 'Drnholci'
        self.translator[u'Droužkovice'] = 'Droužkovicích'
        self.translator[u'Družec'] = 'Družci'
        self.translator[u'Držovice'] = 'Držovicích'
        self.translator[u'Dřevohostice'] = 'Dřevohosticích'
        self.translator[u'Dřísy'] = 'Dřísech'
        self.translator[u'Dub nad Moravou'] = 'Dubu nad Moravou'
        self.translator[u'Dubá'] = 'Dubé'
        self.translator[u'Dubí'] = 'Dubí'
        self.translator[u'Dubicko'] = 'Dubicku'
        self.translator[u'Dubňany'] = 'Dubňanech'
        self.translator[u'Dubno'] = 'Dubně'
        self.translator[u'Duchcov'] = 'Duchcově'
        self.translator[u'Dvorce'] = 'Dvorcích'
        self.translator[u'Dvůr Králové nad Labem'] = 'Dvoře Králové nad Labem'
        self.translator[u'Dyjice'] = 'Dyjicích'
        self.translator[u'Dymokury'] = 'Dymokurech'
        self.translator[u'Dýšina'] = 'Dýšině'
        self.translator[u'Ejpovice'] = 'Ejpovicích'
        self.translator[u'Františkovy Lázně'] = 'Františkových Lázních'
        self.translator[u'Frenštát pod Radhoštěm'] = 'Frenštátě pod Radhoštěm'
        self.translator[u'Frýdek-Místek'] = 'Frýdku-Místku'
        self.translator[u'Frýdlant'] = 'Frýdlantě'
        self.translator[u'Frýdlant nad Ostravicí'] = 'Frýdlantě nad Ostravicí'
        self.translator[u'Fryšták'] = 'Fryštáku'
        self.translator[u'Fulnek'] = 'Fulneku'
        self.translator[u'Golčův Jeníkov'] = 'Golčově Jeníkově'
        self.translator[u'Grygov'] = 'Grygově'

        self.translator[u'Habartov'] = 'Habartově'
        self.translator[u'Habry'] = 'Habrech'
        self.translator[u'Háj u Duchcova'] = 'Háji u Duchcova'
        self.translator[u'Háj ve Slezsku'] = 'Háji ve Slezsku'
        self.translator[u'Halže'] = 'Halži'
        self.translator[u'Hamr na Jezeře'] = 'Hamru na Jezeře'
        self.translator[u'Hanušovice'] = 'Hanušovicích'
        self.translator[u'Hať'] = 'Hati'
        self.translator[u'Havířov'] = 'Havířově'
        self.translator[u'Havlíčkova Borová'] = 'Havlíčkově Borové'
        self.translator[u'Havlíčkův Brod'] = 'Havlíčkově Brodě'
        self.translator[u'Hejnice'] = 'Hejnicích'
        self.translator[u'Herink'] = 'Herinku'
        self.translator[u'Heřmanova Huť'] = 'Heřmanově Huti'
        self.translator[u'Heřmanův Městec'] = 'Heřmanově Městci'
        self.translator[u'Hladké Životice'] = 'Hladkých Životicích'
        self.translator[u'Hlásná Třebaň'] = 'Hlásné Třebaňi'
        self.translator[u'Hlincová Hora'] = 'Hlincové Hoře'
        self.translator[u'Hlinsko'] = 'Hlinsku'
        self.translator[u'Hlohovec'] = 'Hlohovci'
        self.translator[u'Hlubočky'] = 'Hlubočkách'
        self.translator[u'Hluboká nad Vltavou'] = 'Hluboké nad Vltavou'
        self.translator[u'Hlučín'] = 'Hlučíně'
        self.translator[u'Hluk'] = 'Hluku'
        self.translator[u'Hlušovice'] = 'Hlušovicích'
        self.translator[u'Hnanice'] = 'Hnanicích'
        self.translator[u'Hněvošice'] = 'Hněvošicích'
        self.translator[u'Hodějice'] = 'Hodějicích'
        self.translator[u'Hodkovice nad Mohelkou'] = 'Hodkovicích nad Mohelkou'
        self.translator[u'Hodonice'] = 'Hodonicích'
        self.translator[u'Hodonín'] = 'Hodoníně'
        self.translator[u'Holasice'] = 'Holasicích'
        self.translator[u'Holedeč'] = 'Holedeč'
        self.translator[u'Holešov'] = 'Holešově'
        self.translator[u'Holice'] = 'Holicích'
        self.translator[u'Holohlavy'] = 'Holohlavech'
        self.translator[u'Holubice'] = 'Holubicích'
        self.translator[u'Holýšov'] = 'Holýšově'
        self.translator[u'Homole'] = 'Homoli'
        self.translator[u'Homole u Panny'] = 'Homoli u Panny'
        self.translator[u'Hora Svaté Kateřiny'] = 'Hoře Svaté Kateřiny'
        self.translator[u'Horažďovice'] = 'Horažďovicích'
        self.translator[u'Horka nad Moravou'] = 'Horce nad Moravou'
        self.translator[u'Horní Benešov'] = 'Horním Benešově'
        self.translator[u'Horní Beřkovice'] = 'Horních Beřkovicích'
        self.translator[u'Horní Bezděkov'] = 'Horním Bezděkově'
        self.translator[u'Horní Blatná'] = 'Horní Blatné'
        self.translator[u'Horní Bříza'] = 'Horní Bříze'
        self.translator[u'Horní Cerekev'] = 'Horní Cerekvi'
        self.translator[u'Horní Dubenky'] = 'Horních Dubenkách'
        self.translator[u'Horní Dvořiště'] = 'Horním Dvořišti'
        self.translator[u'Horní Jelení'] = 'Horním Jelení'
        self.translator[u'Horní Jiřetín'] = 'Horním Jiřetíně'
        self.translator[u'Horní Lhota'] = 'Horní Lhotě'
        self.translator[u'Horní Maršov'] = 'Horním Maršově'
        self.translator[u'Horní Moštěnice'] = 'Horních Moštěnicích'
        self.translator[u'Horní Planá'] = 'Horní Plané'
        self.translator[u'Horní Police'] = 'Horní Polici'
        self.translator[u'Horní Slavkov'] = 'Horním Slavkově'
        self.translator[u'Horní Suchá'] = 'Horní Suché'
        self.translator[u'Horní Věstonice'] = 'Horních Věstonicích'
        self.translator[u'Horoměřice'] = 'Horoměřicích'
        self.translator[u'Horoušany'] = 'Horoušanech'
        self.translator[u'Horšice'] = 'Horšicích'
        self.translator[u'Horšovský Týn'] = 'Horšovském Týně'
        self.translator[u'Hořepník'] = 'Hořepníku'
        self.translator[u'Hořice'] = 'Hořicích'
        self.translator[u'Hořín'] = 'Hoříně'
        self.translator[u'Hořovice'] = 'Hořovicích'
        self.translator[u'Hostinné'] = 'Hostinném'
        self.translator[u'Hostivice'] = 'Hostivici'
        self.translator[u'Hostomice'] = 'Hostomicích'
        self.translator[u'Hostomice'] = 'Hostomicích'
        self.translator[u'Hostouň'] = 'Hostouni'
        self.translator[u'Hostouň'] = 'Hostouni'
        self.translator[u'Hoštka'] = 'Hoštce'
        self.translator[u'Hovorčovice'] = 'Hovorčovicích'
        self.translator[u'Hrabětice'] = 'Hraběticích'
        self.translator[u'Hradčany'] = 'Hradčanech'
        self.translator[u'Hradec Králové'] = 'Hradci Králové'
        self.translator[u'Hradec nad Moravicí'] = 'Hradci nad Moravicí'
        self.translator[u'Hrádek'] = 'Hrádku'
        self.translator[u'Hrádek'] = 'Hrádku'
        self.translator[u'Hrádek nad Nisou'] = 'Hrádku nad Nisou'
        self.translator[u'Hradešín'] = 'Hradešíně'
        self.translator[u'Hradiště'] = 'Hradišti'
        self.translator[u'Hradištko'] = 'Hradištko'
        self.translator[u'Hradištko'] = 'Hradištko'
        self.translator[u'Hranice'] = 'Hranicích'
        self.translator[u'Hranice'] = 'Hranicích'
        self.translator[u'Hrdějovice'] = 'Hrdějovicích'
        self.translator[u'Hrob'] = 'Hrobě'
        self.translator[u'Hrobce'] = 'Hrobcích'
        self.translator[u'Hrochův Týnec'] = 'Hrochově Týnci'
        self.translator[u'Hronov'] = 'Hronově'
        self.translator[u'Hrotovice'] = 'Hrotovicích'
        self.translator[u'Hroznětín'] = 'Hroznětíně'
        self.translator[u'Hrušky'] = 'Hruškách'
        self.translator[u'Hrušovany nad Jevišovkou'] = 'Hrušovanech nad Jevišovkou'
        self.translator[u'Hrušovany u Brna'] = 'Hrušovanech u Brna'
        self.translator[u'Hřebeč'] = 'Hřebči'
        self.translator[u'Hudlice'] = 'Hudlicích'
        self.translator[u'Hulín'] = 'Hulíně'
        self.translator[u'Humpolec'] = 'Humpolci'
        self.translator[u'Hůry'] = 'Hůrech'
        self.translator[u'Husinec'] = 'Husinci'
        self.translator[u'Husinec'] = 'Husinci'
        self.translator[u'Hustopeče'] = 'Hustopečích'
        self.translator[u'Hustopeče nad Bečvou'] = 'Hustopečích nad Bečvou'
        self.translator[u'Hvozdná'] = 'Hvozdné'
        self.translator[u'Hýskov'] = 'Hýskově'
        self.translator[u'Chabařovice'] = 'Chabařovicích'
        self.translator[u'Cheb'] = 'Chebu'
        self.translator[u'Chlebičov'] = 'Chlebičově'
        self.translator[u'Chleby'] = 'Chlebech'
        self.translator[u'Chlum Svaté Maří'] = 'Chlumě Svaté Maří'
        self.translator[u'Chlum u Třeboně'] = 'Chlumě u Třeboně'
        self.translator[u'Chlumčany'] = 'Chlumčanech'
        self.translator[u'Chlumec'] = 'Chlumci'
        self.translator[u'Chlumec nad Cidlinou'] = 'Chlumci nad Cidlinou'
        self.translator[u'Choceň'] = 'Chocni'
        self.translator[u'Chodov'] = 'Chodově'
        self.translator[u'Chodová Planá'] = 'Chodové Plané'
        self.translator[u'Choltice'] = 'Cholticích'
        self.translator[u'Chomutov'] = 'Chomutově'
        self.translator[u'Chornice'] = 'Chornicích'
        self.translator[u'Chotěboř'] = 'Chotěboři'
        self.translator[u'Chotěbuz'] = 'Chotěbuzi'
        self.translator[u'Choteč'] = 'Chotči'
        self.translator[u'Chotěšov'] = 'Chotěšově'
        self.translator[u'Chotěšov'] = 'Chotěšově'
        self.translator[u'Chotětov'] = 'Chotětově'
        self.translator[u'Chotoviny'] = 'Chotovinech'
        self.translator[u'Chotutice'] = 'Chotuticích'
        self.translator[u'Chrast'] = 'Chrasti'
        self.translator[u'Chrást'] = 'Chrásti'
        self.translator[u'Chrastava'] = 'Chrastavě'
        self.translator[u'Chrášťany'] = 'Chrášťanech'
        self.translator[u'Chropyně'] = 'Chropyni'
        self.translator[u'Chroustovice'] = 'Chroustovicích'
        self.translator[u'Chrudim'] = 'Chrudimi'
        self.translator[u'Chudenice'] = 'Chudenicích'
        self.translator[u'Chuchelna'] = 'Chuchelně'
        self.translator[u'Chuchelná'] = 'Chuchelné'
        self.translator[u'Chvalčov'] = 'Chvalčově'
        self.translator[u'Chvaletice'] = 'Chvaleticích'
        self.translator[u'Chvalíkovice'] = 'Chvalíkovicích'
        self.translator[u'Chyňava'] = 'Chyňavě'
        self.translator[u'Chýně'] = 'Chýni'
        self.translator[u'Chýnice'] = 'Chýnicích'
        self.translator[u'Chýnov'] = 'Chýnově'
        self.translator[u'Chyše'] = 'Chyši'
        self.translator[u'Ivančice'] = 'Ivančicích'
        self.translator[u'Ivanovice na Hané'] = 'Ivanovicích na Hané'
        self.translator[u'Jablonec nad Nisou'] = 'Jablonci nad Nisou'
        self.translator[u'Jablonné nad Orlicí'] = 'Jablonném nad Orlicí'
        self.translator[u'Jablonné v Podještědí'] = 'Jablonném v Podještědí'
        self.translator[u'Jablunkov'] = 'Jablunkově'
        self.translator[u'Jáchymov'] = 'Jáchymově'
        self.translator[u'Jakubčovice nad Odrou'] = 'Jakubčovicích nad Odrou'
        self.translator[u'Jankov'] = 'Jankově'
        self.translator[u'Janovice nad Úhlavou'] = 'Janovicí nad Úhlavou'
        self.translator[u'Janské Lázně'] = 'Janských Lázních'
        self.translator[u'Jaroměř'] = 'Jaroměři'
        self.translator[u'Jaroměřice nad Rokytnou'] = 'Jaroměřicích nad Rokytnou'
        self.translator[u'Jaroslavice'] = 'Jaroslavicích'
        self.translator[u'Javorník'] = 'Javorníku'
        self.translator[u'Javůrek'] = 'Javůrku'
        self.translator[u'Jedlová'] = 'Jedlové'
        self.translator[u'Jedovnice'] = 'Jedovnicích'
        self.translator[u'Jemnice'] = 'Jemnici'
        self.translator[u'Jeneč'] = 'Jenči'
        self.translator[u'Jenišov'] = 'Jenišově'
        self.translator[u'Jenštejn'] = 'Jenštejně'
        self.translator[u'Jeřmanice'] = 'Jeřmanicích'
        self.translator[u'Jesenice'] = 'Jesenici'
        self.translator[u'Jesenice'] = 'Jesenici'
        self.translator[u'Jeseník'] = 'Jeseníku'
        self.translator[u'Jevany'] = 'Jevanech'
        self.translator[u'Jevíčko'] = 'Jevíčku'
        self.translator[u'Jevišovka'] = 'Jevišovce'
        self.translator[u'Jičín'] = 'Jičíně'
        self.translator[u'Jihlava'] = 'Jihlavě'
        self.translator[u'Jilemnice'] = 'Jilemnici'
        self.translator[u'Jílové'] = 'Jílovém'
        self.translator[u'Jílové u Prahy'] = 'Jílovém u Prahy'
        self.translator[u'Jíloviště'] = 'Jílovišti'
        self.translator[u'Jimramov'] = 'Jimramově'
        self.translator[u'Jince'] = 'Jincích'
        self.translator[u'Jindřichův Hradec'] = 'Jindřichově Hradci'
        self.translator[u'Jinočany'] = 'Jinočanech'
        self.translator[u'Jirkov'] = 'Jirkově'
        self.translator[u'Jirny'] = 'Jirnech'
        self.translator[u'Jiřetín pod Jedlovou'] = 'Jiřetíně pod Jedlovou'
        self.translator[u'Jiřice'] = 'Jiřicích'
        self.translator[u'Jiříkov'] = 'Jiříkově'
        self.translator[u'Jiříkovice'] = 'Jiříkovicích'
        self.translator[u'Jistebnice'] = 'Jistebnicích'
        self.translator[u'Josefov'] = 'Josefově'
        self.translator[u'Kácov'] = 'Kácově'
        self.translator[u'Kačice'] = 'Kačicích'
        self.translator[u'Kačlehy'] = 'Kačlehách'
        self.translator[u'Kadaň'] = 'Kadani'
        self.translator[u'Kájov'] = 'Kájově'
        self.translator[u'Kaliště'] = 'Kališti'
        self.translator[u'Kamenice'] = 'Kamenici'
        self.translator[u'Kamenice nad Lipou'] = 'Kamenici nad Lipou'
        self.translator[u'Kamenický Šenov'] = 'Kamenickém Šenově'
        self.translator[u'Kamenné Žehrovice'] = 'Kamenných Žehrovicích'
        self.translator[u'Kamenný Újezd'] = 'Kamenném Újezdě'
        self.translator[u'Kaplice'] = 'Kaplici'
        self.translator[u'Káraný'] = 'Káraném'
        self.translator[u'Kardašova Řečice'] = 'Kardašově Řečici'
        self.translator[u'Karlík'] = 'Karlíku'
        self.translator[u'Karlovy Vary'] = 'Karlových Varech'
        self.translator[u'Karolinka'] = 'Karolince'
        self.translator[u'Karviná'] = 'Karviné'
        self.translator[u'Kašperské Hory'] = 'Kašperských Horách'
        self.translator[u'Katovice'] = 'Katovicích'
        self.translator[u'Katusice'] = 'Katusicích'
        self.translator[u'Kaznějov'] = 'Kaznějově'
        self.translator[u'Kdyně'] = 'Kdyni'
        self.translator[u'Kejžlice'] = 'Kejžlicích'
        self.translator[u'Kladno'] = 'Kladně'
        self.translator[u'Kladruby'] = 'Kladrubech'
        self.translator[u'Klášterec nad Ohří'] = 'Klášterci nad Ohří'
        self.translator[u'Klatovy'] = 'Klatovech'
        self.translator[u'Klecany'] = 'Klecanech'
        self.translator[u'Kleneč'] = 'Klenči'
        self.translator[u'Klenovice'] = 'Klenovicích'
        self.translator[u'Klíčany'] = 'Klíčanech'
        self.translator[u'Klimkovice'] = 'Klimkovicích'
        self.translator[u'Klíny'] = 'Klínech'
        self.translator[u'Klobouky u Brna'] = 'Kloboukách u Brna'
        self.translator[u'Klobuky'] = 'Klobukách'
        self.translator[u'Kněževes'] = 'Kněževsi'
        self.translator[u'Kněževes'] = 'Kněževsi'
        self.translator[u'Kněžmost'] = 'Kněžmostu'
        self.translator[u'Knyk'] = 'Knyku'
        self.translator[u'Kobeřice'] = 'Kobeřicích'
        self.translator[u'Kobeřice u Brna'] = 'Kobeřicích u Brna'
        self.translator[u'Kobylnice'] = 'Kobylnicích'
        self.translator[u'Kojetice'] = 'Kojeticích'
        self.translator[u'Kojetín'] = 'Kojetíně'
        self.translator[u'Kolín'] = 'Kolíně'
        self.translator[u'Koloveč'] = 'Kolovči'
        self.translator[u'Komárov'] = 'Komárově'
        self.translator[u'Komorovice'] = 'Komorovicích'
        self.translator[u'Konárovice'] = 'Konárovicích'
        self.translator[u'Konice'] = 'Konicích'
        self.translator[u'Konstantinovy Lázně'] = 'Konstantinových Lázních'
        self.translator[u'Kopidlno'] = 'Kopidlně'
        self.translator[u'Kopřivnice'] = 'Kopřivnici'
        self.translator[u'Koryčany'] = 'Koryčanech'
        self.translator[u'Kosmonosy'] = 'Kosmonosech'
        self.translator[u'Kosoř'] = 'Kosoři'
        self.translator[u'Kostelec'] = 'Kostelci'
        self.translator[u'Kostelec na Hané'] = 'Kostelci na Hané'
        self.translator[u'Kostelec nad Černými lesy'] = 'Kostelci nad Černými lesy'
        self.translator[u'Kostelec nad Labem'] = 'Kostelci nad Labem'
        self.translator[u'Kostelec nad Orlicí'] = 'Kostelci nad Orlicí'
        self.translator[u'Kostěnice'] = 'Kostěnicích'
        self.translator[u'Kostice'] = 'Kosticích'
        self.translator[u'Kostomlátky'] = 'Kostomlátkách'
        self.translator[u'Kostomlaty nad Labem'] = 'Kostomlatech nad Labem'
        self.translator[u'Kostomlaty pod Milešovkou'] = 'Kostomlatech pod Milešovkou'
        self.translator[u'Košetice'] = 'Košeticích'
        self.translator[u'Košťany'] = 'Košťanech'
        self.translator[u'Kouřim'] = 'Kouřimi'
        self.translator[u'Kovářská'] = 'Kovářské'
        self.translator[u'Kozmice'] = 'Kozmicích'
        self.translator[u'Kozojedy'] = 'Kozojedech'
        self.translator[u'Kožichovice'] = 'Kožichovicích'
        self.translator[u'Kožlany'] = 'Kožlanech'
        self.translator[u'Kralice na Hané'] = 'Kralicích na Hané'
        self.translator[u'Kralice nad Oslavou'] = 'Kralicích nad Oslavou'
        self.translator[u'Králíky'] = 'Králíkách'
        self.translator[u'Kralovice'] = 'Kralovicích'
        self.translator[u'Královské Poříčí'] = 'Královském Poříčí'
        self.translator[u'Kralupy nad Vltavou'] = 'Kralupech nad Vltavou'
        self.translator[u'Králův Dvůr'] = 'Králově Dvoře'
        self.translator[u'Kraslice'] = 'Kraslicích'
        self.translator[u'Krásná Hora'] = 'Krásné Hoře'
        self.translator[u'Krásná Lípa'] = 'Krásné Lípě'
        self.translator[u'Krásno'] = 'Krásně'
        self.translator[u'Kravaře'] = 'Kravařích'
        self.translator[u'Kravaře'] = 'Kravařích'
        self.translator[u'Krčmaň'] = 'Krčmani'
        self.translator[u'Krhanice'] = 'Krhanicích'
        self.translator[u'Krhová'] = 'Krhové'
        self.translator[u'Krchleby'] = 'Krchlebech'
        self.translator[u'Krmelín'] = 'Krmelíně'
        self.translator[u'Krnov'] = 'Krnově'
        self.translator[u'Kroměříž'] = 'Kroměříži'
        self.translator[u'Krucemburk'] = 'Krucemburku'
        self.translator[u'Krupka'] = 'Krupce'
        self.translator[u'Krušovice'] = 'Krušovicích'
        self.translator[u'Kryry'] = 'Kryrech'
        self.translator[u'Křelov-Břuchotín'] = 'Křelově-Břuchotíně'
        self.translator[u'Křemže'] = 'Křemži'
        self.translator[u'Křenice'] = 'Křenicích'
        self.translator[u'Křenovice'] = 'Křenovicích'
        self.translator[u'Křešice'] = 'Křešicích'
        self.translator[u'Křinec'] = 'Křinci'
        self.translator[u'Křižanov'] = 'Křižanově'
        self.translator[u'Kuchařovice'] = 'Kuchařovicích'
        self.translator[u'Kunice'] = 'Kunicích'
        self.translator[u'Kunovice'] = 'Kunovicích'
        self.translator[u'Kunštát'] = 'Kunštátě'
        self.translator[u'Kunžak'] = 'Kunžaku'
        self.translator[u'Kuřim'] = 'Kuřimi'
        self.translator[u'Kutná Hora'] = 'Kutné Hoře'
        self.translator[u'Kvasice'] = 'Kvasicích'
        self.translator[u'Květnice'] = 'Květnicích'
        self.translator[u'Kyjov'] = 'Kyjově'
        self.translator[u'Kyjov'] = 'Kyjově'
        self.translator[u'Kynšperk nad Ohří'] = 'Kynšperku nad Ohří'
        self.translator[u'Kyšice'] = 'Kyšicích'
        self.translator[u'Kyšice'] = 'Kyšicích'
        self.translator[u'Kyškovice'] = 'Kyškovicích'
        self.translator[u'Ladná'] = 'Ladné'
        self.translator[u'Lahošť'] = 'Lahošti'
        self.translator[u'Lanškroun'] = 'Lanškrouně'
        self.translator[u'Lány'] = 'Lánech'
        self.translator[u'Lány'] = 'Lánech'
        self.translator[u'Lanžhot'] = 'Lanžhotě'
        self.translator[u'Lanžov'] = 'Lanžově'
        self.translator[u'Lázně Bělohrad'] = 'Lázních Bělohrad'
        self.translator[u'Lázně Bohdaneč'] = 'Lázních Bohdaneč'
        self.translator[u'Lázně Kynžvart'] = 'Lázních Kynžvart'
        self.translator[u'Lázně Toušeň'] = 'Lázních Toušeň'
        self.translator[u'Ledeč nad Sázavou'] = 'Ledči nad Sázavou'
        self.translator[u'Ledenice'] = 'Ledenicích'
        self.translator[u'Lednice'] = 'Lednici'
        self.translator[u'Ledvice'] = 'Ledvicích'
        self.translator[u'Lelekovice'] = 'Lelekovicích'
        self.translator[u'Lenešice'] = 'Lenešicích'
        self.translator[u'Leština'] = 'Leštině'
        self.translator[u'Letkov'] = 'Letkově'
        self.translator[u'Letohrad'] = 'Letohradě'
        self.translator[u'Letonice'] = 'Letonicích'
        self.translator[u'Letovice'] = 'Letovicích'
        self.translator[u'Lety'] = 'Letech'
        self.translator[u'Lhenice'] = 'Lhenicích'
        self.translator[u'Lhota'] = 'Lhotě'
        self.translator[u'Lhota'] = 'Lhotě'
        self.translator[u'Lhotka'] = 'Lhotce'
        self.translator[u'Libáň'] = 'Libáni'
        self.translator[u'Libčeves'] = 'Libčevsi'
        self.translator[u'Libčice nad Vltavou'] = 'Libčice nad Vltavou'
        self.translator[u'Liběchov'] = 'Liběchově'
        self.translator[u'Liberec'] = 'Liberci'
        self.translator[u'Líbeznice'] = 'Líbeznicích'
        self.translator[u'Libice nad Cidlinou'] = 'Libici nad Cidlinou'
        self.translator[u'Libice nad Doubravou'] = 'Libici nad Doubravou'
        self.translator[u'Libiš'] = 'Libiši'
        self.translator[u'Libníč'] = 'Libníči'
        self.translator[u'Libochovice'] = 'Libochovicích'
        self.translator[u'Liboš'] = 'Liboši'
        self.translator[u'Libušín'] = 'Libušíně'
        self.translator[u'Lidice'] = 'Lidicích'
        self.translator[u'Líně'] = 'Líni'
        self.translator[u'Lipník nad Bečvou'] = 'Lipníku nad Bečvou'
        self.translator[u'Lipová'] = 'Lipové'
        self.translator[u'Lišany'] = 'Lišanech'
        self.translator[u'Líšnice'] = 'Líšnicích'
        self.translator[u'Lišov'] = 'Lišově'
        self.translator[u'Líšťany'] = 'Líšťanech'
        self.translator[u'Liteň'] = 'Litni'
        self.translator[u'Litoměřice'] = 'Litoměřicích'
        self.translator[u'Litomyšl'] = 'Litomyšli'
        self.translator[u'Litovel'] = 'Litoveli'
        self.translator[u'Litvínov'] = 'Litvínově'
        self.translator[u'Litvínovice'] = 'Litvínovicích'
        self.translator[u'Loděnice'] = 'Loděnicích'
        self.translator[u'Lochovice'] = 'Lochovicích'
        self.translator[u'Loket'] = 'Lokti'
        self.translator[u'Lom'] = 'Lomu'
        self.translator[u'Lomnice'] = 'Lomnici'
        self.translator[u'Lomnice'] = 'Lomnici'
        self.translator[u'Lomnice nad Lužnicí'] = 'Lomnici nad Lužnicí'
        self.translator[u'Lomnice nad Popelkou'] = 'Lomnici nad Popelkou'
        self.translator[u'Loštice'] = 'Lošticích'
        self.translator[u'Loučeň'] = 'Loučeni'
        self.translator[u'Louka u Litvínova'] = 'Louce u Litvínova'
        self.translator[u'Louňovice'] = 'Louňovicích'
        self.translator[u'Louňovice pod Blaníkem'] = 'Louňovicích pod Blaníkem'
        self.translator[u'Louny'] = 'Lounech'
        self.translator[u'Lovosice'] = 'Lovosicích'
        self.translator[u'Lštění'] = 'Lštění'
        self.translator[u'Lubenec'] = 'Lubenci'
        self.translator[u'Luby'] = 'Lubech'
        self.translator[u'Ludgeřovice'] = 'Ludgeřovicích'
        self.translator[u'Luhačovice'] = 'Luhačovicích'
        self.translator[u'Luka nad Jihlavou'] = 'Lukách nad Jihlavou'
        self.translator[u'Lukavec'] = 'Lukavci'
        self.translator[u'Lukov'] = 'Lukově'
        self.translator[u'Lukoveček'] = 'Lukovečku'
        self.translator[u'Luštěnice'] = 'Luštěnicích'
        self.translator[u'Lutín'] = 'Lutíně'
        self.translator[u'Luže'] = 'Luži'
        self.translator[u'Lužec nad Vltavou'] = 'Lužci nad Vltavou'
        self.translator[u'Lužice'] = 'Lužicích'
        self.translator[u'Lužná'] = 'Lužné'
        self.translator[u'Lysá nad Labem'] = 'Lysé nad Labem'
        self.translator[u'Lysice'] = 'Lysicích'
        self.translator[u'Majetín'] = 'Majetíně'
        self.translator[u'Malá Skála'] = 'Malé Skále'
        self.translator[u'Malé Kyšice'] = 'Malých Kyšicích'
        self.translator[u'Malé Přítočno'] = 'Malém Přítočně'
        self.translator[u'Malé Svatoňovice'] = 'Malých Svatoňovicích'
        self.translator[u'Malé Žernoseky'] = 'Malých Žernosekách'
        self.translator[u'Malenice'] = 'Malenicích'
        self.translator[u'Malešov'] = 'Malešově'
        self.translator[u'Malíč'] = 'Malíči'
        self.translator[u'Malovice'] = 'Malovicích'
        self.translator[u'Malšice'] = 'Malšicích'
        self.translator[u'Mariánské Lázně'] = 'Mariánských Lázních'
        self.translator[u'Mariánské Radčice'] = 'Mariánských Radčicích'
        self.translator[u'Markvartovice'] = 'Markvartovicích'
        self.translator[u'Martínkovice'] = 'Martínkovicích'
        self.translator[u'Máslovice'] = 'Máslovicích'
        self.translator[u'Mašťov'] = 'Mašťově'
        self.translator[u'Měčín'] = 'Měčíně'
        self.translator[u'Měděnec'] = 'Měděnci'
        self.translator[u'Měchenice'] = 'Měchenicích'
        self.translator[u'Mělnické Vtelno'] = 'Mělnickém Vtelně'
        self.translator[u'Mělník'] = 'Mělníku'
        self.translator[u'Merklín'] = 'Merklíně'
        self.translator[u'Měřín'] = 'Měříně'
        self.translator[u'Městec Králové'] = 'Městci Králové'
        self.translator[u'Městečko'] = 'Městečku'
        self.translator[u'Město Albrechtice'] = 'Městě Albrechtice'
        self.translator[u'Město Libavá'] = 'Městě Libavá'
        self.translator[u'Město Touškov'] = 'Městě Touškově'
        self.translator[u'Měšice'] = 'Měšicích'
        self.translator[u'Meziboří'] = 'Meziboří'
        self.translator[u'Meziměstí'] = 'Meziměstí'
        self.translator[u'Mikulov'] = 'Mikulově'
        self.translator[u'Mikulov'] = 'Mikulově'
        self.translator[u'Mikulovice'] = 'Mikulovicích'
        self.translator[u'Mikulovice'] = 'Mikulovicích'
        self.translator[u'Miletín'] = 'Miletíně'
        self.translator[u'Milevsko'] = 'Milevsku'
        self.translator[u'Milín'] = 'Milíně'
        self.translator[u'Milotice'] = 'Miloticích'
        self.translator[u'Milovice'] = 'Milovicích'
        self.translator[u'Mimoň'] = 'Mimoni'
        self.translator[u'Miroslav'] = 'Miroslavi'
        self.translator[u'Mirošov'] = 'Mirošově'
        self.translator[u'Mirošovice'] = 'Mirošovicích'
        self.translator[u'Mirotice'] = 'Miroticích'
        self.translator[u'Mírová pod Kozákovem'] = 'Mírové pod Kozákovem'
        self.translator[u'Mirovice'] = 'Mirovicích'
        self.translator[u'Miřejovice'] = 'Miřejovicích'
        self.translator[u'Mířkov'] = 'Mířkově'
        self.translator[u'Mladá Boleslav'] = 'Mladé Boleslavi'
        self.translator[u'Mladá Vožice'] = 'Mladé Vožici'
        self.translator[u'Mlázovice'] = 'Mlázovicích'
        self.translator[u'Mnichovice'] = 'Mnichovicích'
        self.translator[u'Mnichovo Hradiště'] = 'Mnichově Hradišti'
        self.translator[u'Mníšek'] = 'Mníšku'
        self.translator[u'Mníšek pod Brdy'] = 'Mníšku pod Brdy'
        self.translator[u'Modletice'] = 'Modleticích'
        self.translator[u'Modřice'] = 'Modřicích'
        self.translator[u'Mohelnice'] = 'Mohelnicích'
        self.translator[u'Mochov'] = 'Mochově'
        self.translator[u'Mokré'] = 'Mokré'
        self.translator[u'Mokré Lazce'] = 'Mokrých Lazcích'
        self.translator[u'Moravany'] = 'Moravanech'
        self.translator[u'Moravany'] = 'Moravanech'
        self.translator[u'Moravská Nová Ves'] = 'Moravské Nové Vsi'
        self.translator[u'Moravská Třebová'] = 'Moravské Třebové'
        self.translator[u'Moravské Budějovice'] = 'Moravských Budějovicích'
        self.translator[u'Moravské Knínice'] = 'Moravských Knínicích'
        self.translator[u'Moravský Beroun'] = 'Moravském Berouně'
        self.translator[u'Moravský Krumlov'] = 'Moravském Krumlově'
        self.translator[u'Moravský Písek'] = 'Moravském Písku'
        self.translator[u'Moravský Žižkov'] = 'Moravském Žižkově'
        self.translator[u'Morkovice-Slížany'] = 'Morkovicích-Slížanech'
        self.translator[u'Mořkov'] = 'Mořkově'
        self.translator[u'Most'] = 'Mostě'
        self.translator[u'Mostkovice'] = 'Mostkovicích'
        self.translator[u'Mošnov'] = 'Mošnově'
        self.translator[u'Mouchnice'] = 'Mouchnicích'
        self.translator[u'Mrač'] = 'Mrači'
        self.translator[u'Mratín'] = 'Mratíně'
        self.translator[u'Mšené-lázně'] = 'Mšené-lázních'
        self.translator[u'Mšeno'] = 'Mšeně'
        self.translator[u'Mukařov'] = 'Mukařově'
        self.translator[u'Mutějovice'] = 'Mutějovicích'
        self.translator[u'Mutěnice'] = 'Mutěnicích'
        self.translator[u'Mýto'] = 'Mýtě'
        self.translator[u'Načeradec'] = 'Načeradci'
        self.translator[u'Nadějkov'] = 'Nadějkově'
        self.translator[u'Náchod'] = 'Náchodě'
        self.translator[u'Náměšť na Hané'] = 'Náměšti na Hané'
        self.translator[u'Náměšť nad Oslavou'] = 'Náměšti nad Oslavou'
        self.translator[u'Napajedla'] = 'Napajedlech'
        self.translator[u'Nasavrky'] = 'Nasavrkách'
        self.translator[u'Návsí'] = 'Návsí'
        self.translator[u'Neděliště'] = 'Nedělišti'
        self.translator[u'Nehvizdy'] = 'Nehvizdech'
        self.translator[u'Nechanice'] = 'Nechanicích'
        self.translator[u'Nejdek'] = 'Nejdku'
        self.translator[u'Nelahozeves'] = 'Nelahozevsi'
        self.translator[u'Němčice nad Hanou'] = 'Němčicích nad Hanou'
        self.translator[u'Neplachovice'] = 'Neplachovicích'
        self.translator[u'Nepomuk'] = 'Nepomuku'
        self.translator[u'Neratov'] = 'Neratově'
        self.translator[u'Neratovice'] = 'Neratovicích'
        self.translator[u'Neslovice'] = 'Neslovicích'
        self.translator[u'Nespeky'] = 'Nespekách'
        self.translator[u'Netolice'] = 'Netolicích'
        self.translator[u'Netvořice'] = 'Netvořicích'
        self.translator[u'Neuměř'] = 'Neuměři'
        self.translator[u'Neumětely'] = 'Neumětelech'
        self.translator[u'Neveklov'] = 'Neveklově'
        self.translator[u'Nezamyslice'] = 'Nezamyslicích'
        self.translator[u'Nivnice'] = 'Nivnicích'
        self.translator[u'Nižbor'] = 'Nižboru'
        self.translator[u'Nižní Lhoty'] = 'Nižních Lhotách'
        self.translator[u'Nosislav'] = 'Nosislavi'
        self.translator[u'Nošovice'] = 'Nošovicích'
        self.translator[u'Nová Bystřice'] = 'Nové Bystřici'
        self.translator[u'Nová Paka'] = 'Nové Pace'
        self.translator[u'Nová Role'] = 'Nové Roli'
        self.translator[u'Nová Říše'] = 'Nové Říši'
        self.translator[u'Nová Včelnice'] = 'Nové Včelnici'
        self.translator[u'Nová Ves'] = 'Nové Vsi'
        self.translator[u'Nová Ves I'] = 'Nové Vsi I'
        self.translator[u'Nová Ves pod Pleší'] = 'Nové Vsi pod Pleší'
        self.translator[u'Nové Dvory'] = 'Nových Dvorech'
        self.translator[u'Nové Hrady'] = 'Nových Hradech'
        self.translator[u'Nové Město na Moravě'] = 'Novém Městě na Moravě'
        self.translator[u'Nové Město nad Metují'] = 'Novém Městě nad Metují'
        self.translator[u'Nové Město pod Smrkem'] = 'Novém Městě pod Smrkem'
        self.translator[u'Nové Sedlice'] = 'Nových Sedlicích'
        self.translator[u'Nové Sedlo'] = 'Novém Sedle'
        self.translator[u'Nové Sedlo'] = 'Novém Sedle'
        self.translator[u'Nové Strašecí'] = 'Novém Strašecí'
        self.translator[u'Nové Veselí'] = 'Novém Veselí'
        self.translator[u'Novosedlice'] = 'Novosedlicích'
        self.translator[u'Nový Bor'] = 'Novém Boru'
        self.translator[u'Nový Bydžov'] = 'Novém Bydžově'
        self.translator[u'Nový Hrádek'] = 'Novém Hrádku'
        self.translator[u'Nový Jáchymov'] = 'Novém Jáchymově'
        self.translator[u'Nový Jičín'] = 'Novém Jičíně'
        self.translator[u'Nový Knín'] = 'Novém Kníně'
        self.translator[u'Nový Vestec'] = 'Novém Vestci'
        self.translator[u'Nučice'] = 'Nučicích'
        self.translator[u'Nupaky'] = 'Nupakách'
        self.translator[u'Nymburk'] = 'Nymburku'
        self.translator[u'Nýrsko'] = 'Nýrsku'
        self.translator[u'Nýřany'] = 'Nýřanech'
        self.translator[u'Obořiště'] = 'Obořišti'
        self.translator[u'Obrnice'] = 'Obrnicích'
        self.translator[u'Obříství'] = 'Obříství'
        self.translator[u'Odolena Voda'] = 'Odolene Vodě'
        self.translator[u'Odry'] = 'Odrách'
        self.translator[u'Ohrobec'] = 'Ohrobci'
        self.translator[u'Ochoz u Brna'] = 'Ochozu u Brna'
        self.translator[u'Okrouhlice'] = 'Okrouhlicích'
        self.translator[u'Okříšky'] = 'Okříškách'
        self.translator[u'Olbramice'] = 'Olbramicích'
        self.translator[u'Oldřišov'] = 'Oldřišově'
        self.translator[u'Olešnice'] = 'Olešnici'
        self.translator[u'Olešnice'] = 'Olešnici'
        self.translator[u'Olomouc'] = 'Olomouci'
        self.translator[u'Oloví'] = 'Oloví'
        self.translator[u'Olovnice'] = 'Olovnici'
        self.translator[u'Omice'] = 'Omicích'
        self.translator[u'Ondřejov'] = 'Ondřejově'
        self.translator[u'Opatov'] = 'Opatově'
        self.translator[u'Opatovice'] = 'Opatovicích'
        self.translator[u'Opatovice'] = 'Opatovicích'
        self.translator[u'Opatovice nad Labem'] = 'Opatovicívh nad Labem'
        self.translator[u'Opava'] = 'Opavě'
        self.translator[u'Opočno'] = 'Opočně'
        self.translator[u'Oráčov'] = 'Oráčově'
        self.translator[u'Orlová'] = 'Orlové'
        self.translator[u'Ořech'] = 'Ořechu'
        self.translator[u'Ořechov'] = 'Ořechově'
        self.translator[u'Osečná'] = 'Osečné'
        self.translator[u'Osek'] = 'Oseku'
        self.translator[u'Oskořínek'] = 'Oskořínku'
        self.translator[u'Oslavany'] = 'Oslavanech'
        self.translator[u'Osoblaha'] = 'Osoblaze'
        self.translator[u'Ostopovice'] = 'Ostopovicích'
        self.translator[u'Ostrá'] = 'Ostré'
        self.translator[u'Ostrava'] = 'Ostravě'
        self.translator[u'Ostroměř'] = 'Ostroměři'
        self.translator[u'Ostrov'] = 'Ostrově'
        self.translator[u'Ostrovačice'] = 'Ostrovačicích'
        self.translator[u'Ostrožská Nová Ves'] = 'Ostrožské Nové Vsi'
        self.translator[u'Ostřešany'] = 'Ostřešanech'
        self.translator[u'Otice'] = 'Oticích'
        self.translator[u'Otnice'] = 'Otnicích'
        self.translator[u'Otovice'] = 'Otovicích'
        self.translator[u'Otrokovice'] = 'Otrokovicích'
        self.translator[u'Otvice'] = 'Otvicích'
        self.translator[u'Ovčáry'] = 'Ovčárech'
        self.translator[u'Pacov'] = 'Pacově'
        self.translator[u'Panenské Břežany'] = 'Panenských Břežanech'
        self.translator[u'Pardubice'] = 'Pardubicích'
        self.translator[u'Paskov'] = 'Paskově'
        self.translator[u'Pastuchovice'] = 'Pastuchovicích'
        self.translator[u'Pátek'] = 'Pátku'
        self.translator[u'Pavlov'] = 'Pavlově'
        self.translator[u'Pavlov'] = 'Pavlově'
        self.translator[u'Pečky'] = 'Pečkách'
        self.translator[u'Pelhřimov'] = 'Pelhřimově'
        self.translator[u'Pernink'] = 'Perninku'
        self.translator[u'Perštejn'] = 'Perštejnu'
        self.translator[u'Peruc'] = 'Peruci'
        self.translator[u'Petrov'] = 'Petrově'
        self.translator[u'Petrovice'] = 'Petrovicích'
        self.translator[u'Petříkov'] = 'Petříkově'
        self.translator[u'Petřvald'] = 'Petřvaldu'
        self.translator[u'Pchery'] = 'Pcherech'
        self.translator[u'Pilníkov'] = 'Pilníkově'
        self.translator[u'Písek'] = 'Písku'
        self.translator[u'Písková Lhota'] = 'Pískové Lhotě'
        self.translator[u'Píšť'] = 'Píšti'
        self.translator[u'Planá'] = 'Plané'
        self.translator[u'Planá nad Lužnicí'] = 'Plané nad Lužnicí'
        self.translator[u'Plaňany'] = 'Plaňanech'
        self.translator[u'Plánice'] = 'Plánicích'
        self.translator[u'Plasy'] = 'Plasech'
        self.translator[u'Plaveč'] = 'Plavči'
        self.translator[u'Plesná'] = 'Plesné'
        self.translator[u'Pletený Újezd'] = 'Pleteném Újezdu'
        self.translator[u'Plumlov'] = 'Plumlově'
        self.translator[u'Plzeň'] = 'Plzni'
        self.translator[u'Pňov-Předhradí'] = 'Pňovi-Předhradí'
        self.translator[u'Poběžovice'] = 'Poběžovicích'
        self.translator[u'Počátky'] = 'Počátkách'
        self.translator[u'Podbořany'] = 'Podbořanech'
        self.translator[u'Poděbrady'] = 'Poděbradech'
        self.translator[u'Podivín'] = 'Podivíně'
        self.translator[u'Podolanka'] = 'Podolance'
        self.translator[u'Pohled'] = 'Pohledu'
        self.translator[u'Pohořelice'] = 'Pohořelicích'
        self.translator[u'Pohořelice'] = 'Pohořelicích'
        self.translator[u'Police nad Metují'] = 'Polici nad Metují'
        self.translator[u'Polička'] = 'Poličce'
        self.translator[u'Polná'] = 'Polné'
        self.translator[u'Polom'] = 'Polomi'
        self.translator[u'Pomezí nad Ohří'] = 'Pomezí nad Ohří'
        self.translator[u'Popice'] = 'Popicích'
        self.translator[u'Popovičky'] = 'Popovičkách'
        self.translator[u'Popůvky'] = 'Popůvkách'
        self.translator[u'Poříčany'] = 'Poříčanech'
        self.translator[u'Poříčí nad Sázavou'] = 'Poříčí nad Sázavou'
        self.translator[u'Postoloprty'] = 'Postoloprtech'
        self.translator[u'Postřelmov'] = 'Postřelmově'
        self.translator[u'Postřižín'] = 'Postřižíně'
        self.translator[u'Postupice'] = 'Postupicích'
        self.translator[u'Potštát'] = 'Potštátě'
        self.translator[u'Potštejn'] = 'Potštejně'
        self.translator[u'Pouzdřany'] = 'Pouzdřanech'
        self.translator[u'Povrly'] = 'Povrlech'
        self.translator[u'Pozlovice'] = 'Pozlovicích'
        self.translator[u'Pozořice'] = 'Pozořicích'
        self.translator[u'Prace'] = 'Pracích'
        self.translator[u'Prackovice nad Labem'] = 'Prackovicích nad Labem'
        self.translator[u'Praha'] = 'Praze'
        self.translator[u'Prachatice'] = 'Prachaticích'
        self.translator[u'Prachovice'] = 'Prachovicích'
        self.translator[u'Proboštov'] = 'Proboštově'
        self.translator[u'Proseč'] = 'Proseči'
        self.translator[u'Prosenice'] = 'Prosenicích'
        self.translator[u'Prostějov'] = 'Prostějově'
        self.translator[u'Protivanov'] = 'Protivanově'
        self.translator[u'Protivín'] = 'Protivíně'
        self.translator[u'Prštice'] = 'Pršticích'
        self.translator[u'Průhonice'] = 'Průhonicích'
        self.translator[u'Prusinovice'] = 'Prusinovicích'
        self.translator[u'Předboj'] = 'Předboji'
        self.translator[u'Předhradí'] = 'Předhradí'
        self.translator[u'Předklášteří'] = 'Předklášteří'
        self.translator[u'Předměřice nad Labem'] = 'Předměřicích nad Labem'
        self.translator[u'Přední Výtoň'] = 'Přední Výtoni'
        self.translator[u'Přelouč'] = 'Přelouči'
        self.translator[u'Přerov'] = 'Přerově'
        self.translator[u'Přeštice'] = 'Přešticích'
        self.translator[u'Přezletice'] = 'Přezleticích'
        self.translator[u'Příbor'] = 'Příboře'
        self.translator[u'Příbram'] = 'Příbrami'
        self.translator[u'Přibyslav'] = 'Přibyslavi'
        self.translator[u'Přibyslavice'] = 'Přibyslavicích'
        self.translator[u'Přibyslavice'] = 'Přibyslavicích'
        self.translator[u'Přimda'] = 'Přimdě'
        self.translator[u'Přísnotice'] = 'Přísnoticích'
        self.translator[u'Přítluky'] = 'Přítlukách'
        self.translator[u'Psáry'] = 'Psárech'
        self.translator[u'Ptice'] = 'Pticích'
        self.translator[u'Pustá Polom'] = 'Pusté Polomi'
        self.translator[u'Pustiměř'] = 'Pustiměři'
        self.translator[u'Pyšely'] = 'Pyšelech'
        self.translator[u'Radějovice'] = 'Radějovicích'
        self.translator[u'Radnice'] = 'Radnicích'
        self.translator[u'Radomyšl'] = 'Radomyšli'
        self.translator[u'Radonice'] = 'Radonicích'
        self.translator[u'Radostice'] = 'Radosticích'
        self.translator[u'Radovesnice I'] = 'Radovesnici I'
        self.translator[u'Radslavice'] = 'Radslavicích'
        self.translator[u'Raduň'] = 'Raduni'
        self.translator[u'Rájec-Jestřebí'] = 'Rájci-Jestřebí'
        self.translator[u'Ráječko'] = 'Ráječku'
        self.translator[u'Rajhrad'] = 'Rajhradě'
        self.translator[u'Rajhradice'] = 'Rajhradicích'
        self.translator[u'Rakovník'] = 'Rakovníku'
        self.translator[u'Rakvice'] = 'Rakvicích'
        self.translator[u'Ralsko'] = 'Ralsku'
        self.translator[u'Rapotice'] = 'Rapoticích'
        self.translator[u'Rapotín'] = 'Rapotíně'
        self.translator[u'Rapšach'] = 'Rapšachu'
        self.translator[u'Raspenava'] = 'Raspenavě'
        self.translator[u'Rataje nad Sázavou'] = 'Ratajích nad Sázavou'
        self.translator[u'Ratboř'] = 'Ratboři'
        self.translator[u'Ratíškovice'] = 'Ratíškovicích'
        self.translator[u'Rebešovice'] = 'Rebešovicích'
        self.translator[u'Rejštejn'] = 'Rejštejně'
        self.translator[u'Roblín'] = 'Roblíně'
        self.translator[u'Rohatec'] = 'Rohatci'
        self.translator[u'Rohov'] = 'Rohově'
        self.translator[u'Rokycany'] = 'Rokycanech'
        self.translator[u'Rokytnice v Orlických horách'] = 'Rokytnici v Orlických horách'
        self.translator[u'Rokytno'] = 'Rokytně'
        self.translator[u'Ronov nad Doubravou'] = 'Ronově nad Doubravou'
        self.translator[u'Rosice'] = 'Rosicích'
        self.translator[u'Rostoklaty'] = 'Rostoklatech'
        self.translator[u'Rotava'] = 'Rotavě'
        self.translator[u'Roudné'] = 'Roudném'
        self.translator[u'Roudnice nad Labem'] = 'Roudnici nad Labem'
        self.translator[u'Rousínov'] = 'Rousínově'
        self.translator[u'Rovensko pod Troskami'] = 'Rovensku pod Troskami'
        self.translator[u'Rozdrojovice'] = 'Rozdrojovicích'
        self.translator[u'Roztoky'] = 'Roztokách'
        self.translator[u'Roztoky'] = 'Roztokách'
        self.translator[u'Rožďalovice'] = 'Rožďalovicích'
        self.translator[u'Rožmitál na Šumavě'] = 'Rožmitále na Šumavě'
        self.translator[u'Rožmitál pod Třemšínem'] = 'Rožmitále pod Třemšínem'
        self.translator[u'Rožnov pod Radhoštěm'] = 'Rožnově pod Radhoštěm'
        self.translator[u'Rtyně v Podkrkonoší'] = 'Rtyni v Podkrkonoší'
        self.translator[u'Ruda'] = 'Rudě'
        self.translator[u'Ruda nad Moravou'] = 'Rudě nad Moravou'
        self.translator[u'Rudná'] = 'Rudné'
        self.translator[u'Rudolfov'] = 'Rudolfově'
        self.translator[u'Rumburk'] = 'Rumburku'
        self.translator[u'Rybitví'] = 'Rybitví'
        self.translator[u'Rychnov nad Kněžnou'] = 'Rychnově nad Kněžnou'
        self.translator[u'Rychnov u Jablonce nad Nisou'] = 'Rychnově u Jablonce nad Nisou'
        self.translator[u'Rychvald'] = 'Rychvaldu'
        self.translator[u'Rýmařov'] = 'Rýmařově'
        self.translator[u'Rynholec'] = 'Rynholci'
        self.translator[u'Ryžoviště'] = 'Ryžovišti'
        self.translator[u'Řečany nad Labem'] = 'Řečanech nad Labem'
        self.translator[u'Řehenice'] = 'Řehenicích'
        self.translator[u'Řemíčov'] = 'Řemíčově'
        self.translator[u'Řepín'] = 'Řepíně'
        self.translator[u'Řepiště'] = 'Řepišti'
        self.translator[u'Řevnice'] = 'Řevnicích'
        self.translator[u'Řevničov'] = 'Řevničově'
        self.translator[u'Řícmanice'] = 'Řícmanicích'
        self.translator[u'Říčany'] = 'Říčanech'
        self.translator[u'Říčany'] = 'Říčanech'
        self.translator[u'Římov'] = 'Římově'
        self.translator[u'Řitka'] = 'Řitce'
        self.translator[u'Sadská'] = 'Sadské'
        self.translator[u'Samotišky'] = 'Samotiškách'
        self.translator[u'Sány'] = 'Sánech'
        self.translator[u'Sázava'] = 'Sázavě'
        self.translator[u'Seč'] = 'Seči'
        self.translator[u'Sedlčany'] = 'Sedlčanech'
        self.translator[u'Sedlec-Prčice'] = 'Sedleci-Prčicích'
        self.translator[u'Sedlice'] = 'Sedlicích'
        self.translator[u'Semily'] = 'Semilech'
        self.translator[u'Senice'] = 'Senicích'
        self.translator[u'Senice na Hané'] = 'Senici na Hané'
        self.translator[u'Senohraby'] = 'Senohrabech'
        self.translator[u'Senomaty'] = 'Senomatech'
        self.translator[u'Sezemice'] = 'Sezemicích'
        self.translator[u'Sezimovo Ústí'] = 'Sezimově Ústí'
        self.translator[u'Sibřina'] = 'Sibřině'
        self.translator[u'Silůvky'] = 'Silůvkách'
        self.translator[u'Skalná'] = 'Skalné'
        self.translator[u'Skorošice'] = 'Skorošicích'
        self.translator[u'Skrbeň'] = 'Skrbni'
        self.translator[u'Skřivany'] = 'Skřivanech'
        self.translator[u'Skuteč'] = 'Skutči'
        self.translator[u'Slaný'] = 'Slaném'
        self.translator[u'Slatiňany'] = 'Slatiňanech'
        self.translator[u'Slavětín'] = 'Slavětíně'
        self.translator[u'Slavičín'] = 'Slavičíně'
        self.translator[u'Slavkov'] = 'Slavkově'
        self.translator[u'Slavkov u Brna'] = 'Slavkově u Brna'
        self.translator[u'Slavonice'] = 'Slavonicích'
        self.translator[u'Slepotice'] = 'Slepoticích'
        self.translator[u'Sloup v Čechách'] = 'Sloupu v Čechách'
        self.translator[u'Sloveč'] = 'Slovči'
        self.translator[u'Slušovice'] = 'Slušovicích'
        self.translator[u'Smečno'] = 'Smečně'
        self.translator[u'Smidary'] = 'Smidarech'
        self.translator[u'Smiřice'] = 'Smiřicích'
        self.translator[u'Smržice'] = 'Smržicích'
        self.translator[u'Smržovka'] = 'Smržovce'
        self.translator[u'Soběslav'] = 'Soběslavi'
        self.translator[u'Sobotka'] = 'Sobotce'
        self.translator[u'Sokoleč'] = 'Sokolči'
        self.translator[u'Sokolnice'] = 'Sokolnicích'
        self.translator[u'Sokolov'] = 'Sokolově'
        self.translator[u'Solnice'] = 'Solnicích'
        self.translator[u'Sovínky'] = 'Sovínkávh'
        self.translator[u'Spálené Poříčí'] = 'Spálené Poříčí'
        self.translator[u'Spojil'] = 'Spojili'
        self.translator[u'Spořice'] = 'Spořicích'
        self.translator[u'Srbsko'] = 'Srbsku'
        self.translator[u'Srch'] = 'Srchu'
        self.translator[u'Srnojedy'] = 'Srnojedech'
        self.translator[u'Srubec'] = 'Srubci'
        self.translator[u'Stachy'] = 'Stachách'
        self.translator[u'Staňkov'] = 'Staňkově'
        self.translator[u'Staňkovice'] = 'Staňkovicích'
        self.translator[u'Stará Huť'] = 'Staré Huti'
        self.translator[u'Stará Paka'] = 'Staré Pace'
        self.translator[u'Stará Ves'] = 'Staré Vsi'
        self.translator[u'Stará Ves nad Ondřejnicí'] = 'Staré Vsi nad Ondřejnicí'
        self.translator[u'Staré Hodějovice'] = 'Starých Hodějovicích'
        self.translator[u'Staré Hradiště'] = 'Starém Hradišti'
        self.translator[u'Staré Město'] = 'Starém Městě'
        self.translator[u'Staré Město'] = 'Starém Městě'
        self.translator[u'Staré Město'] = 'Starém Městě'
        self.translator[u'Staré Sedlo'] = 'Starém Sedle'
        self.translator[u'Staré Ždánice'] = 'Starých Ždánicích'
        self.translator[u'Starý Jičín'] = 'Starém Jičíně'
        self.translator[u'Starý Kolín'] = 'Starém Kolíně'
        self.translator[u'Starý Plzenec'] = 'Starém Plzenci'
        self.translator[u'Stařeč'] = 'Starči'
        self.translator[u'Staříč'] = 'Staříči'
        self.translator[u'Statenice'] = 'Statenicích'
        self.translator[u'Stehelčeves'] = 'Stehelčevsi'
        self.translator[u'Stěžery'] = 'Stěžerech'
        self.translator[u'Stod'] = 'Stodě'
        self.translator[u'Stochov'] = 'Stochově'
        self.translator[u'Strachotín'] = 'Strachotíně'
        self.translator[u'Strakonice'] = 'Strakonicích'
        self.translator[u'Strančice'] = 'Strančicích'
        self.translator[u'Strání'] = 'Strání'
        self.translator[u'Strašín'] = 'Strašíně'
        self.translator[u'Stráž nad Nežárkou'] = 'Stráiž nad Nežárkou'
        self.translator[u'Stráž nad Nisou'] = 'Stráži nad Nisou'
        self.translator[u'Stráž pod Ralskem'] = 'Stráži pod Ralskem'
        self.translator[u'Strážnice'] = 'Strážnici'
        self.translator[u'Strmilov'] = 'Strmilově'
        self.translator[u'Struhařov'] = 'Struhařově'
        self.translator[u'Strunkovice nad Blanicí'] = 'Strunkovicích nad Blanicí'
        self.translator[u'Středokluky'] = 'Středoklukách'
        self.translator[u'Střelice'] = 'Střelicích'
        self.translator[u'Střelské Hoštice'] = 'Střelských Hošticích'
        self.translator[u'Stříbrná Skalice'] = 'Stříbrné Skalici'
        self.translator[u'Stříbro'] = 'Stříbře'
        self.translator[u'Střílky'] = 'Střílkách'
        self.translator[u'Střítež'] = 'Stříteži'
        self.translator[u'Studená'] = 'Studené'
        self.translator[u'Studénka'] = 'Studénce'
        self.translator[u'Sudice'] = 'Sudicích'
        self.translator[u'Suchdol'] = 'Suchdole'
        self.translator[u'Suchdol nad Lužnicí'] = 'Suchdole nad Lužnicí'
        self.translator[u'Suchdol nad Odrou'] = 'Suchdole nad Odrou'
        self.translator[u'Suchohrdly'] = 'Suchohrdlech'
        self.translator[u'Sulejovice'] = 'Sulejovicích'
        self.translator[u'Sulice'] = 'Sulicích'
        self.translator[u'Sušice'] = 'Sušici'
        self.translator[u'Svárov'] = 'Svárově'
        self.translator[u'Svatava'] = 'Svatavě'
        self.translator[u'Svatobořice-Mistřín'] = 'Svatobořicích-Mistříně'
        self.translator[u'Svatý Jan pod Skalou'] = 'Svatém Janu pod Skalou'
        self.translator[u'Svatý Mikuláš'] = 'Svatém Mikuláši'
        self.translator[u'Svémyslice'] = 'Svémyslicích'
        self.translator[u'Světec'] = 'Světci'
        self.translator[u'Světice'] = 'Světicích'
        self.translator[u'Světlá nad Sázavou'] = 'Světlé nad Sázavou'
        self.translator[u'Sviadnov'] = 'Sviadnově'
        self.translator[u'Svinaře'] = 'Svinařích'
        self.translator[u'Svinařov'] = 'Svinařově'
        self.translator[u'Svitávka'] = 'Svitávce'
        self.translator[u'Svitavy'] = 'Svitavách'
        self.translator[u'Svoboda nad Úpou'] = 'Svobodě nad Úpou'
        self.translator[u'Svobodné Heřmanice'] = 'Svobodných Heřmanicích'
        self.translator[u'Svojetice'] = 'Svojeticích'
        self.translator[u'Svratka'] = 'Svratce'
        self.translator[u'Sýkořice'] = 'Sýkořicích'
        self.translator[u'Šakvice'] = 'Šakvicích'
        self.translator[u'Šanov'] = 'Šanově'
        self.translator[u'Šaratice'] = 'Šaraticích'
        self.translator[u'Šenov'] = 'Šenově'
        self.translator[u'Šenov u Nového Jičína'] = 'Šenově u Nového Jičína'
        self.translator[u'Šestajovice'] = 'Šestajovicích'
        self.translator[u'Ševětín'] = 'Ševětíně'
        self.translator[u'Šilheřovice'] = 'Šilheřovicích'
        self.translator[u'Šimonovice'] = 'Šimonovicích'
        self.translator[u'Šitbořice'] = 'Šitbořicích'
        self.translator[u'Škvorec'] = 'Škvorci'
        self.translator[u'Šlapanice'] = 'Šlapanicích'
        self.translator[u'Šluknov'] = 'Šluknově'
        self.translator[u'Špičky'] = 'Špičkách'
        self.translator[u'Šťáhlavy'] = 'Šťáhlavech'
        self.translator[u'Štěchovice'] = 'Štěchovicích'
        self.translator[u'Štěkeň'] = 'Štěkeni'
        self.translator[u'Štěnovice'] = 'Štěnovicích'
        self.translator[u'Štěpánkovice'] = 'Štěpánkovicích'
        self.translator[u'Štěpánov'] = 'Štěpánově'
        self.translator[u'Štěpánovice'] = 'Štěpánovicích'
        self.translator[u'Šternberk'] = 'Šternberku'
        self.translator[u'Štětí'] = 'Štětí'
        self.translator[u'Štítina'] = 'Štítina'
        self.translator[u'Štíty'] = 'Štítech'
        self.translator[u'Štoky'] = 'Štokách'
        self.translator[u'Štramberk'] = 'Štramberku'
        self.translator[u'Šumperk'] = 'Šumperku'
        self.translator[u'Švihov'] = 'Švihově'
        self.translator[u'Tábor'] = 'Táboře'
        self.translator[u'Tachlovice'] = 'Tachlovicích'
        self.translator[u'Tachov'] = 'Tachově'
        self.translator[u'Tanvald'] = 'Tanvaldě'
        self.translator[u'Tatce'] = 'Tatcích'
        self.translator[u'Tehov'] = 'Tehově'
        self.translator[u'Tehov'] = 'Tehově'
        self.translator[u'Tehovec'] = 'Tehovci'
        self.translator[u'Telč'] = 'Telči'
        self.translator[u'Telnice'] = 'Telnici'
        self.translator[u'Teplá'] = 'Teplé'
        self.translator[u'Teplice'] = 'Teplicích'
        self.translator[u'Teplice nad Metují'] = 'Teplicích nad Metují'
        self.translator[u'Terezín'] = 'Terezíně'
        self.translator[u'Těrlicko'] = 'Těrlicku'
        self.translator[u'Tetčice'] = 'Tetčicích'
        self.translator[u'Tetín'] = 'Tetíně'
        self.translator[u'Tisá'] = 'Tisé'
        self.translator[u'Tišice'] = 'Tišicích'
        self.translator[u'Tišnov'] = 'Tišnově'
        self.translator[u'Tlučná'] = 'Tlučné'
        self.translator[u'Tlumačov'] = 'Tlumačově'
        self.translator[u'Tmaň'] = 'Tmani'
        self.translator[u'Toužim'] = 'Toužimi'
        self.translator[u'Tovačov'] = 'Tovačově'
        self.translator[u'Trhová Kamenice'] = 'Trhové Kamenici'
        self.translator[u'Trhové Sviny'] = 'Trhových Svinech'
        self.translator[u'Trhový Štěpánov'] = 'Trhovém Štěpánově'
        self.translator[u'Trmice'] = 'Trmicích'
        self.translator[u'Trnávka'] = 'Trnávce'
        self.translator[u'Troubky'] = 'Troubkách'
        self.translator[u'Troubsko'] = 'Troubsku'
        self.translator[u'Trubín'] = 'Trubíně'
        self.translator[u'Trutnov'] = 'Trutnově'
        self.translator[u'Třebechovice pod Orebem'] = 'Třebechovicích pod Orebem'
        self.translator[u'Třebenice'] = 'Třebenicích'
        self.translator[u'Třebestovice'] = 'Třebestovicích'
        self.translator[u'Třebíč'] = 'Třebíči'
        self.translator[u'Třebívlice'] = 'Třebívlicích'
        self.translator[u'Třeboň'] = 'Třeboni'
        self.translator[u'Třebotov'] = 'Třebotově'
        self.translator[u'Třemošná'] = 'Třemošné'
        self.translator[u'Třemošnice'] = 'Třemošnici'
        self.translator[u'Třešť'] = 'Třešťi'
        self.translator[u'Třinec'] = 'Třinci'
        self.translator[u'Tuchlovice'] = 'Tuchlovicích'
        self.translator[u'Tuchoměřice'] = 'Tuchoměřicích'
        self.translator[u'Tuklaty'] = 'Tuklatech'
        self.translator[u'Turnov'] = 'Turnově'
        self.translator[u'Tursko'] = 'Tursku'
        self.translator[u'Tvrdonice'] = 'Tvrdonicích'
        self.translator[u'Týn nad Bečvou'] = 'Týně nad Bečvou'
        self.translator[u'Týn nad Vltavou'] = 'Týně nad Vltavou'
        self.translator[u'Týnec'] = 'Týnci'
        self.translator[u'Týnec nad Labem'] = 'Týnci nad Labem'
        self.translator[u'Týnec nad Sázavou'] = 'Týnci nad Sázavou'
        self.translator[u'Týniště nad Orlicí'] = 'Týništi nad Orlicí'
        self.translator[u'Údlice'] = 'Údlicích'
        self.translator[u'Úherce'] = 'Úhercích'
        self.translator[u'Uherské Hradiště'] = 'Uherském Hradišti'
        self.translator[u'Uherský Brod'] = 'Uherském Brodě'
        self.translator[u'Uherský Ostroh'] = 'Uherském Ostrohu'
        self.translator[u'Uhlířské Janovice'] = 'Uhlířských Janovicích'
        self.translator[u'Úholičky'] = 'Úholičkách'
        self.translator[u'Úhonice'] = 'Úhonicích'
        self.translator[u'Újezd u Brna'] = 'Újezdě u Brna'
        self.translator[u'Újezdeček'] = 'Újezdečku'
        self.translator[u'Únětice'] = 'Úněticích'
        self.translator[u'Unhošť'] = 'Unhošť'
        self.translator[u'Uničov'] = 'Uničově'
        self.translator[u'Úpice'] = 'Úpicích'
        self.translator[u'Úsov'] = 'Úsově'
        self.translator[u'Ústí nad Labem'] = 'Ústí nad Labem'
        self.translator[u'Ústí nad Orlicí'] = 'Ústí nad Orlicí'
        self.translator[u'Úštěk'] = 'Úštěku'
        self.translator[u'Úterý'] = 'Úterý'
        self.translator[u'Úvaly'] = 'Úvalech'
        self.translator[u'Úžice'] = 'Úžicích'
        self.translator[u'Vacenovice'] = 'Vacenovicích'
        self.translator[u'Václavovice'] = 'Václavovicích'
        self.translator[u'Valašské Klobouky'] = 'Valašských Kloboukách'
        self.translator[u'Valašské Meziříčí'] = 'Valašském Meziříčí'
        self.translator[u'Valdice'] = 'Valdicích'
        self.translator[u'Valeč'] = 'Valči'
        self.translator[u'Valtice'] = 'Valticích'
        self.translator[u'Valy'] = 'Valech'
        self.translator[u'Valy'] = 'Valech'
        self.translator[u'Vamberk'] = 'Vamberku'
        self.translator[u'Varnsdorf'] = 'Varnsdorfu'
        self.translator[u'Varvažov'] = 'Varvažově'
        self.translator[u'Včelákov'] = 'Včelákově'
        self.translator[u'Včelná'] = 'Včelná'
        self.translator[u'Vědomice'] = 'Vědomicích'
        self.translator[u'Vejprnice'] = 'Vejprnicích'
        self.translator[u'Vejprty'] = 'Vejprtech'
        self.translator[u'Velehrad'] = 'Velehradě'
        self.translator[u'Velemín'] = 'Velemíně'
        self.translator[u'Veleň'] = 'Veleni'
        self.translator[u'Velešín'] = 'Velešíně'
        self.translator[u'Velichovky'] = 'Velichovkách'
        self.translator[u'Veliká Ves'] = 'Veliké Vsi'
        self.translator[u'Velim'] = 'Velimi'
        self.translator[u'Veliny'] = 'Velinech'
        self.translator[u'Velká Bíteš'] = 'Velké Bíteši'
        self.translator[u'Velká Bystřice'] = 'Velké Bystřici'
        self.translator[u'Velká Dobrá'] = 'Velké Dobré'
        self.translator[u'Velká Hleďsebe'] = 'Velké Hleďsebi'
        self.translator[u'Velká Polom'] = 'Velké Polomi'
        self.translator[u'Velké Bílovice'] = 'Velkých Bílovicích'
        self.translator[u'Velké Březno'] = 'Velkém Březně'
        self.translator[u'Velké Heraltice'] = 'Velkých Heralticích'
        self.translator[u'Velké Hoštice'] = 'Velkých Hošticích'
        self.translator[u'Velké Losiny'] = 'Velké Losiny'
        self.translator[u'Velké Meziříčí'] = 'Velkém Meziříčí'
        self.translator[u'Velké Němčice'] = 'Velkých Němčicích'
        self.translator[u'Velké Opatovice'] = 'Velkých Opatovicích'
        self.translator[u'Velké Pavlovice'] = 'Velkých Pavlovicích'
        self.translator[u'Velké Popovice'] = 'Velkých Popovicích'
        self.translator[u'Velké Poříčí'] = 'Velkém Poříčí'
        self.translator[u'Velké Přílepy'] = 'Velkých Přílepech'
        self.translator[u'Velké Přítočno'] = 'Velkém Přítočně'
        self.translator[u'Velký Borek'] = 'Velkém Borku'
        self.translator[u'Velký Osek'] = 'Velkém Oseku'
        self.translator[u'Velký Šenov'] = 'Velkém Šenově'
        self.translator[u'Velký Týnec'] = 'Velkém Týnci'
        self.translator[u'Velký Újezd'] = 'Velkém Újezdě'
        self.translator[u'Veltěže'] = 'Veltěži'
        self.translator[u'Veltruby'] = 'Veltrubech'
        self.translator[u'Veltrusy'] = 'Veltrusech'
        self.translator[u'Velvary'] = 'Velvarech'
        self.translator[u'Verměřovice'] = 'Verměřovicích'
        self.translator[u'Verneřice'] = 'Verneřicích'
        self.translator[u'Veselí nad Lužnicí'] = 'Veselí nad Lužnicí'
        self.translator[u'Veselí nad Moravou'] = 'Veselí nad Moravou'
        self.translator[u'Vestec'] = 'Vestci'
        self.translator[u'Větrušice'] = 'Větrušicích'
        self.translator[u'Větřní'] = 'Větřní'
        self.translator[u'Veverská Bítýška'] = 'Veverské Bítýšce'
        self.translator[u'Vidnava'] = 'Vidnavě'
        self.translator[u'Vikýřovice'] = 'Vikýřovicích'
        self.translator[u'Vilémov'] = 'Vilémově'
        self.translator[u'Vimperk'] = 'Vimperku'
        self.translator[u'Vinařice'] = 'Vinařicích'
        self.translator[u'Vitějovice'] = 'Vitějovicích'
        self.translator[u'Vítkov'] = 'Vítkově'
        self.translator[u'Vizovice'] = 'Vizovicích'
        self.translator[u'Vlachovo Březí'] = 'Vlachově Březí'
        self.translator[u'Vlašim'] = 'Vlašimi'
        self.translator[u'Vlkava'] = 'Vlkavě'
        self.translator[u'Vlkoš'] = 'Vlkoši'
        self.translator[u'Vnorovy'] = 'Vnorovech'
        self.translator[u'Vodňany'] = 'Vodňanech'
        self.translator[u'Vodochody'] = 'Vodochodech'
        self.translator[u'Vojenský újezd Boletice'] = 'Vojenském újezdě Boletice'
        self.translator[u'Vojenský újezd Libavá'] = 'Vojenském újezdě Libavá'
        self.translator[u'Vojkovice'] = 'Vojkovicích'
        self.translator[u'Vojtanov'] = 'Vojtanově'
        self.translator[u'Volary'] = 'Volarech'
        self.translator[u'Volyně'] = 'Volyni'
        self.translator[u'Vonoklasy'] = 'Vonoklasech'
        self.translator[u'Votice'] = 'Voticích'
        self.translator[u'Vrábče'] = 'Vrábči'
        self.translator[u'Vracov'] = 'Vracově'
        self.translator[u'Vrané nad Vltavou'] = 'Vraném nad Vltavou'
        self.translator[u'Vranov nad Dyjí'] = 'Vranově nad Dyjí'
        self.translator[u'Vranovice'] = 'Vranovicích'
        self.translator[u'Vratimov'] = 'Vratimově'
        self.translator[u'Vráto'] = 'Vrátu'
        self.translator[u'Vráž'] = 'Vráži'
        self.translator[u'Vrbice'] = 'Vrbicích'
        self.translator[u'Vrbice'] = 'Vrbicích'
        self.translator[u'Vrbno pod Pradědem'] = 'Vrbnu pod Pradědem'
        self.translator[u'Vrbová Lhota'] = 'Vrbové Lhotě'
        self.translator[u'Vrdy'] = 'Vrdech'
        self.translator[u'Vrchlabí'] = 'Vrchlabí'
        self.translator[u'Vroutek'] = 'Vroutku'
        self.translator[u'Vřesina'] = 'Vřesině'
        self.translator[u'Vřesina'] = 'Vřesině'
        self.translator[u'Vsetín'] = 'Vsetíně'
        self.translator[u'Všejany'] = 'Všejanech'
        self.translator[u'Všemyslice'] = 'Všemyslicích'
        self.translator[u'Všenory'] = 'Všenorech'
        self.translator[u'Všestary'] = 'Všestarech'
        self.translator[u'Všetaty'] = 'Všetatech'
        self.translator[u'Vysoká'] = 'Vysoké'
        self.translator[u'Vysoká nad Labem'] = 'Vysoké nad Labem'
        self.translator[u'Vysoká Pec'] = 'Vysoké Peci'
        self.translator[u'Vysoké Chvojno'] = 'Vysokém Chvojně'
        self.translator[u'Vysoké Mýto'] = 'Vysokém Mýtě'
        self.translator[u'Vysoké nad Jizerou'] = 'Vysokém nad Jizerou'
        self.translator[u'Vysoké Veselí'] = 'Vysokém Veselí'
        self.translator[u'Vysoký Újezd'] = 'Vysokém Újezdě'
        self.translator[u'Vyškov'] = 'Vyškově'
        self.translator[u'Vyšší Brod'] = 'Vyšším Brodě'
        self.translator[u'Vyžlovka'] = 'Vyžlovce'
        self.translator[u'Záboří nad Labem'] = 'Záboří nad Labem'
        self.translator[u'Zábřeh'] = 'Zábřehu'
        self.translator[u'Zadní Třebaň'] = 'Zadní Třebani'
        self.translator[u'Zahnašovice'] = 'Zahnašovicích'
        self.translator[u'Záhornice'] = 'Záhornicích'
        self.translator[u'Zaječí'] = 'Zaječí'
        self.translator[u'Zákupy'] = 'Zákupech'
        self.translator[u'Zápy'] = 'Zápech'
        self.translator[u'Zásmuky'] = 'Zásmukách'
        self.translator[u'Zastávka'] = 'Zastávce'
        self.translator[u'Zbiroh'] = 'Zbirohu'
        self.translator[u'Zborovice'] = 'Zborovicích'
        self.translator[u'Zbraslav'] = 'Zbraslavi'
        self.translator[u'Zbůch'] = 'Zbůchu'
        self.translator[u'Zbuzany'] = 'Zbuzanech'
        self.translator[u'Zbyslavice'] = 'Zbyslavicích'
        self.translator[u'Zbýšov'] = 'Zbýšově'
        self.translator[u'Zdechovice'] = 'Zdechovicích'
        self.translator[u'Zdiby'] = 'Zdibech'
        self.translator[u'Zdice'] = 'Zdicích'
        self.translator[u'Zdobnice'] = 'Zdobnicích'
        self.translator[u'Zdounky'] = 'Zdounkách'
        self.translator[u'Zeleneč'] = 'Zelenči'
        self.translator[u'Zlaté Hory'] = 'Zlatých Horách'
        self.translator[u'Zlatníky-Hodkovice'] = 'Zlatníkách-Hodkovicích'
        self.translator[u'Zlín'] = 'Zlíně'
        self.translator[u'Zliv'] = 'Zlivi'
        self.translator[u'Zlonice'] = 'Zlonicích'
        self.translator[u'Znojmo'] = 'Znojmě'
        self.translator[u'Zruč nad Sázavou'] = 'Zruči nad Sázavou'
        self.translator[u'Zruč-Senec'] = 'Zruči-Senci'
        self.translator[u'Zubří'] = 'Zubří'
        self.translator[u'Zvánovice'] = 'Zvánovicích'
        self.translator[u'Zvěřínek'] = 'Zvěřínku'
        self.translator[u'Zvíkov'] = 'Zvíkově'
        self.translator[u'Zvole'] = 'Zvoli'
        self.translator[u'Žabčice'] = 'Žabčicích'
        self.translator[u'Žabovřesky nad Ohří'] = 'Žabovřeskách nad Ohří'
        self.translator[u'Žacléř'] = 'Žacléři'
        self.translator[u'Žalany'] = 'Žalanech'
        self.translator[u'Žamberk'] = 'Žamberku'
        self.translator[u'Žandov'] = 'Žandově'
        self.translator[u'Žatec'] = 'Žatci'
        self.translator[u'Ždánice'] = 'Ždánicích'
        self.translator[u'Žďár nad Sázavou'] = 'Žďáru nad Sázavou'
        self.translator[u'Ždírec nad Doubravou'] = 'Ždírci nad Doubravou'
        self.translator[u'Žebrák'] = 'Žebráku'
        self.translator[u'Žehušice'] = 'Žehušicích'
        self.translator[u'Želechovice nad Dřevnicí'] = 'Želechovicích nad Dřevnicí'
        self.translator[u'Želenice'] = 'Želenicích'
        self.translator[u'Želenice'] = 'Želenicích'
        self.translator[u'Želešice'] = 'Želešicích'
        self.translator[u'Želetava'] = 'Želetavě'
        self.translator[u'Železná Ruda'] = 'Železné Rudě'
        self.translator[u'Železnice'] = 'Železnicích'
        self.translator[u'Železný Brod'] = 'Železném Brodě'
        self.translator[u'Židlochovice'] = 'Židlochovicích'
        self.translator[u'Žilina'] = 'Žilině'
        self.translator[u'Žirovnice'] = 'Žirovnicích'
        self.translator[u'Žitenice'] = 'Žitenicích'
        self.translator[u'Žiželice'] = 'Žiželicích'
        self.translator[u'Žižkovo Pole'] = 'Žižkově Poli'
        self.translator[u'Žleby'] = 'Žlebech'
        self.translator[u'Žlutice'] = 'Žluticích'
        self.translator[u'Žulová'] = 'Žulové'

    def load_csv(self, list_only=False):
        #https://www.cuzk.cz/Uvod/Produkty-a-sluzby/RUIAN/2-Poskytovani-udaju-RUIAN-ISUI-VDP/Ciselniky-ISUI/Nizsi-uzemni-prvky-a-uzemne-evidencni-jednotky.aspx#UI_ULICE
        with open('UI_ULICE.csv', 'rb') as f:
            unicodecsv.register_dialect('excel_semicolon', excel_semicolon)
            reader = unicodecsv.reader(f, encoding='cp1250', dialect=excel_semicolon)
            # headers = next(reader)
            self.csv = list(reader)

        if list_only:
            return self.csv

        cities = {}
        # try:
        text = ''
        for line in self.csv:
            if (len(line) < 2):
                continue
            if (cities.get(line[2], False)):
                name = line[1][0].upper() + line[1][1:]
                cities[line[2]][name] = line[0]
            else:
                text = text + ",'" + line[2] + "'"
                name = line[1][0].upper() + line[1][1:]
                cities[line[2]] = {}
                cities[line[2]][name] = line[0]
        # except Exception as e:
        # print e
        # print text
        # sys.exit()
        return cities

    def get_contribs(self):

        self.site = pywikibot.Site('cs', 'wikipedia')
        self.commons = self.site.image_repository()
        # self.repo = self.site.data_repository()
        self.genFactory = pagegenerators.GeneratorFactory(site=self.commons)
        self.genFactory.handleArg('-limit:2500')
        self.generator = self.genFactory.getCombinedGenerator()
        if not self.generator:
            self.genFactory.handleArg(u'-usercontribs:' + u'Frettie')
            self.generator = self.genFactory.getCombinedGenerator()
        logger = Logger(u'magic' + u'Fill', 'process')
        pages = {}
        iter = 0
        for page in pagegenerators.PreloadingGenerator(self.generator):
            iter = iter + 1
            match = re.search(u'Frettie moved page \[\[Category:(.*)\]\] to \[\[Category:(.*)\]\]: (.*)',
                              page.latest_revision.comment)
            if not match:
                pass
                # not for me
            else:
                groups = match.groups()
                street_name = groups[0]
                print(page.latest_revision.comment)

                match3 = re.search(u'Category:(.*)', page.title())
                gr = match3.groups()
                name = gr[0]

                if (groups[1] == name):
                    pages[page.title()] = {}
                    pages[page.title()]['name'] = page.title()
                    pages[page.title()]['from'] = groups[0]
                    pages[page.title()]['to'] = groups[1]
                    pages[page.title()]['from_text'] = page.text
                    pages[page.title()]['comment'] = groups[2]
                    pages[page.title()]['whole_comment'] = page.latest_revision.comment

        for key, page in pages.iteritems():
            pywikibot.output('Page %s ...' % page['to'] + ' -> ' + page['from'])
            choice = pywikibot.input_choice('Move?',
                                            [('Yes', 'y'),
                                             ('No', 'N')],
                                            default='Y',
                                            automatic_quit=False)

            if choice == 'n':
                pass

            if choice == 'y':
                to_old_text = "{{Category redirect|Category:" + page['from'] + "}}"
                to_new_text = page['from_text']
                page_old = pywikibot.Category(self.commons, u'Category:' + page['to'])
                page_new = pywikibot.Category(self.commons, u'Category:' + page['from'])
                page_old.text = to_old_text
                page_new.text = to_new_text
                page_old.save(summary=u'undo moving')
                page_new.save(summary=u'undo moving')

                images = page_old.articles()
                for image in images:
                    assert isinstance(image, pywikibot.FilePage)
                    image.change_category(page_old, page_new, summary=u'undo moving')
                subcategories = page_old.subcategories()
                for subcat in subcategories:
                    assert isinstance(subcat, pywikibot.Category)
                    subcat.change_category(page_old, page_new, summary=u'undo moving')

    def street_create_text(self):
        self.logger = Logger(u'newStreetssquare' + u'Fill', 'saved')
        cities = {}
        self.site = pywikibot.Site('cs', 'wikipedia')
        self.repo = self.site.data_repository()
        query = '''SELECT * WHERE {
          ?ulice wdt:P31 wd:Q174782.
          ?ulice wdt:P17 wd:Q213.
          ?ulice wdt:P4533 ?ruian
        }'''
        from pywikibot.data import sparql
        query_object = sparql.SparqlQuery()
        data = query_object.select(query, full_data=True)
        if data:
            for r in data:
                ruian = str(r['ruian'])
                self.logger.logComplete(ruian)
        sys.exit()

    def mestska_cast(self):
        import mysql.connector
        logger_name = 'streets_casti'
        logger = Logger(logger_name + u'Fill', 'saved')
        cnx = mysql.connector.connect(user='root', database='ruian', password='root',
                                      unix_socket='/Applications/MAMP/tmp/mysql/mysql.sock')
        cursor = cnx.cursor()
        casti = {}

        self.site = pywikibot.Site('cs', 'wikipedia')
        self.repo = self.site.data_repository()

        query = '''SELECT * WHERE {
                                        ?castobce wdt:P2788 [].
                                        ?castobce wdt:P2788 ?castobceid.
                                        #?castobce wdt:P131 wd:Q128752
                                    }'''
        from pywikibot.data import sparql

        query_object = sparql.SparqlQuery()
        data = query_object.select(query, full_data=True)
        if data:
            for r in data:
                casti[str(r['castobceid'])] = pywikibot.ItemPage(self.repo, r['castobce'].getID())




        query = '''SELECT * WHERE {                  
                  ?ulice wdt:P4533 [].
                  ?ulice wdt:P4533 ?ruian.
                 # ?ulice wdt:P131 wd:Q128752
                }'''
        query_object = sparql.SparqlQuery()
        data = query_object.select(query, full_data=True)
        if data:
            for r in data:

                if not logger.isCompleteFile(str(r['ruian'])):


                    ruian = str(r['ruian'])
                    query = ('''
                    SELECT u.id,u.casti_obce_id, u.obec_id, nazev_ulice, c.nazev as nazev_casti, momc_id, nazev_momc, mop_id, nazev_mop, o.nazev as nazev_obce FROM ruian_ulice as u
    inner join ruian_casti_obce as c on c.id = u.casti_obce_id
    inner join ruian_obce as o on o.id = u.obec_id
    where u.id = %s
                    ''')
                    street_id = ruian
                    try:
                        cursor.execute(query, (street_id,))
                    except Exception:
                        # cnx = mysql.connector.connect(user='root', database='ruian', password='root',
                        #                               unix_socket='/Applications/MAMP/tmp/mysql/mysql.sock')
                        # cursor = cnx.cursor()
                        sys.exit()
                    item_id = r['ulice'].getID()
                    item = pywikibot.ItemPage(self.repo, item_id)
                    datas = item.get()
                    try:
                        admins = datas['claims']['P131']
                    except KeyError as e:
                        print('key err ' + str(street_id))
                        admins = []
                    admins_list = []
                    for admin in admins:
                        admin_id = admin.getTarget().getID()
                        admins_list.append(admin_id)
                    # print(admins_list)
                    for (ulice_id,casti_obce_id,obec_id,nazev_ulice,nazev_casti,momc_id,nazev_momc,mop_id,nazev_mop,nazev_obce) in cursor:

                        # print(ulice_id)
                        # print(casti_obce_id)
                        # print(obec_id)
                        print(nazev_ulice)
                        print(nazev_casti)
                        # print(momc_id)
                        # print(nazev_momc)
                        # print(mop_id)
                        # print(nazev_mop)
                        # print(nazev_obce)
                        castqid = casti[str(casti_obce_id)].getID()
                        if (castqid not in admins_list):
                            sources = []

                            source = pywikibot.Claim(self.repo, 'P248')
                            source.setTarget(pywikibot.ItemPage(self.repo, 'Q12049125'))

                            castClaim = pywikibot.Claim(self.repo, 'P131')
                            castClaim.setTarget(casti[str(casti_obce_id)])

                            item.addClaim(castClaim)
                            sources.append(source)
                            castClaim.addSources(sources)
                    logger.logComplete(str(r['ruian']))






        sys.exit()

        cursor.close()




        cnx.close()

    def new_streets(self):
        self.logger = Logger(u'newStreets' + u'Fill', 'saved')
        cities = {}
        self.site = pywikibot.Site('cs', 'wikipedia')
        self.repo = self.site.data_repository()
        query = '''SELECT * WHERE {
                        ?mesto wdt:P31 wd:Q5153359.
                        ?mesto wdt:P782 ?lau
                    }'''
        from pywikibot.data import sparql

        query_object = sparql.SparqlQuery()
        data = query_object.select(query, full_data=True)
        if data:
            for r in data:
                cities[str(r['lau'])] = pywikibot.ItemPage(self.repo, r['mesto'].getID())

        self.streets_from_csv = self.load_csv(list_only=True)

        iternum = 0
        for street in self.streets_from_csv:
            # Header
            if iternum == 0:
                iternum = iternum + 1
                continue
            # faster skip lines
            if iternum < 1:
                iternum = iternum + 1
                continue
            ruian = street[0]  # ruian
            if (not self.logger.isCompleteFile(ruian)):
                # print street[1] #name
                # print street[2] #city_lau
                city_lau = street[2]
                city_item = cities['CZ' + city_lau]

                city_name = city_item.text[u'labels'][u'cs']

                street_name = street[1]
                plati_do = street[4]

                regex = u"náměstí"

                match = re.search(regex, street_name, re.IGNORECASE)
                # match = re.search('[Nn]áměstí', street_name)
                if not match:
                    regex = u"nám."

                    match = re.search(regex, street_name, re.IGNORECASE)
                    if not match:
                        typeOfInstance = 'street'
                    else:
                        typeOfInstance = 'square'
                else:
                    typeOfInstance = 'square'

                sources = []

                source = pywikibot.Claim(self.repo, 'P248')
                source.setTarget(pywikibot.ItemPage(self.repo, 'Q12049125'))

                instance = pywikibot.Claim(self.repo, 'P31')
                if typeOfInstance == 'street':
                    instance.setTarget(pywikibot.ItemPage(self.repo, 'Q79007'))
                else:
                    instance.setTarget(pywikibot.ItemPage(self.repo, 'Q174782'))

                if plati_do != '':
                    instance.setTarget(pywikibot.ItemPage(self.repo, 'Q15893266'))

                country = pywikibot.Claim(self.repo, 'P17')
                country.setTarget(pywikibot.ItemPage(self.repo, 'Q213'))

                admin = pywikibot.Claim(self.repo, 'P131')
                admin.setTarget(city_item)

                try:
                    coords = pywikibot.Claim(self.repo, 'P625')
                    payload = {'query': street_name + ', ' + city_name}
                    r = requests.get(u'https://api.mapy.cz/geocode', params=payload)
                    xml = r.text
                    data = untangle.parse(xml)

                    # print xml
                    # print data
                    # sys.exit()
                    if isinstance(data.result.point.item, list):
                        coordsElem = data.result.point.item[0]
                    else:
                        coordsElem = data.result.point.item
                    assert isinstance(coordsElem, untangle.Element)
                    line = {}
                    lat = coordsElem.get_attribute(u'y')
                    lon = coordsElem.get_attribute(u'x')
                    coord = pywikibot.Coordinate(float(lat), float(lon), precision=1e-07, globe='earth', site=self.repo)
                    coords.setTarget(coord)
                except AttributeError as e:
                    self.logger.logError(street_name)
                    self.logger.logError(u'coord problems')
                    coords = None

                try:
                    code = pywikibot.Claim(self.repo, 'P4533')
                    code.setTarget(ruian)

                    sources.append(source)
                except:
                    self.logger.logError(street_name)
                    self.logger.logError(self.city_name)
                    self.logger.logError(u'street code problem')
                    continue

                data = {}

                data.setdefault('labels', {}).update({
                    'en': {
                        'language': 'en',
                        'value': street_name
                    },
                    'cs': {
                        'language': 'cs',
                        'value': street_name
                    },
                })

                city_name_cs = self.translator.get(city_name, city_name)

                if typeOfInstance == 'street':

                    data.setdefault('descriptions', {}).update({
                        'en': {
                            'language': 'en',
                            'value': 'street in ' + city_name
                        },
                        'cs': {
                            'language': 'cs',
                            'value': 'ulice v ' + city_name_cs
                        },
                    })
                else:
                    data.setdefault('descriptions', {}).update({
                        'en': {
                            'language': 'en',
                            'value': 'square in ' + city_name
                        },
                        'cs': {
                            'language': 'cs',
                            'value': 'náměstí v ' + city_name_cs
                        },
                    })
                try:
                    item = self.create_item_for_page(page=None, data=data, repo=self.repo,
                                                     summary=u'Creating czech street item')
                except pywikibot.OtherPageSaveError as e:
                    self.logger.logError(street_name)

                claimsStreet = item.get()
                if (not claimsStreet[u'claims'].get(u'P31', False)):
                    item.addClaim(instance)
                if (not claimsStreet[u'claims'].get(u'P17', False)):
                    item.addClaim(country)
                if (not claimsStreet[u'claims'].get(u'P131', False)):
                    item.addClaim(admin)
                if (not claimsStreet[u'claims'].get(u'P625', False)):
                    if coords:
                        item.addClaim(coords)
                if (not claimsStreet[u'claims'].get(u'P4533', False)):
                    if code:
                        item.addClaim(code)
                        code.addSources(sources)

                        source = pywikibot.Claim(self.repo, 'P248')
                        source.setTarget(pywikibot.ItemPage(self.repo, 'Q12049125'))
                        sources.append(source)
                        # item = pywikibot.ItemPage(repo, 'Q12345')
                        # instance = pywikibot.Claim(repo, 'P31')
                        # item.addClaim(instance)
                        # instance.addSources(sources)


                self.logger.logComplete(ruian)

    def correct_coordinates(self):

        self.logger = Logger(u'newStreetssquare' + u'Fill', 'saved')
        self.site = pywikibot.Site('cs', 'wikipedia')
        self.repo = self.site.data_repository()
        #         query = '''
        #                 SELECT ?item ?itemLabel ?adminLabel ?okrLabel ?item2 ?item2Label ?admin2Label ?coord {
        #   ?item wdt:P4533 []; wdt:P625 ?coord; wdt:P131 ?above1 .
        #   ?item wdt:P131 ?admin .
        #
        #   ?coord ^wdt:P625 ?item2 .
        #   ?item2 wdt:P131 ?admin2 .
        #   ?admin2 wdt:P131 ?okr .
        #   FILTER( STR( ?item ) < STR( ?item2 ) ) .
        #   ?item2 wdt:P131 ?above2 .
        #   FILTER( ?above1 != ?above2 ) .
        #   SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],cs". }
        # }
        #                 '''

        # query = '''
        # SELECT distinct ?item ?item2 ?item2Label ?adminLabel ?okrLabel where {
        #   ?item wdt:P4533 []; wdt:P625/^wdt:P625 ?item2 .
        #   FILTER( ?item != ?item2 ) .
        #   ?item2 wdt:P131 ?admin .
        #             ?admin wdt:P131 ?okr .
        #         SERVICE wikibase:label { bd:serviceParam wikibase:language "cs". }
        # }
        #
        # order by ?adminLabel
        #
        #
        #
        #         '''

        query = '''
        SELECT ?item ?itemLabel ?parent ?okrLabel ?parentLabel ?dist {
  ?item wdt:P4533 [];
        wdt:P625 ?coord1;
        wdt:P131 ?parent .
  ?parent wdt:P625 ?coord .
  ?parent wdt:P131 ?okr .
  BIND( geof:distance( ?coord1, ?coord ) AS ?dist ) .
  FILTER( ?dist > 10 ) .
      FILTER (?parent not in (wd:Q1085,wd:Q8385)).

  SERVICE wikibase:label { bd:serviceParam wikibase:language "cs" }.
} ORDER BY DESC(?dist)

        '''

        from pywikibot.data import sparql
        query_object = sparql.SparqlQuery()
        data = query_object.select(query, full_data=True)
        if data:
            for r in data:
                item_id = r['item'].getID()
                streetName = r['itemLabel']
                cityName = r['parentLabel']
                okrName = r['okrLabel']
                ulice = 'ulice'

                item = pywikibot.ItemPage(self.repo, item_id)
                datas = item.get()
                claims = datas['claims']
                try:
                    claim = claims['P625'][0]
                except KeyError as k:
                    continue
                try:
                    # coords = pywikibot.Claim(self.repo, 'P625')

                    payload = {'query': str(streetName) + ', ' + str(cityName) + ', ' + str(okrName)}
                    r = requests.get(u'https://api.mapy.cz/geocode', params=payload)
                    xml = r.text
                    data = untangle.parse(xml)

                    # print xml
                    # print data
                    # sys.exit()
                    chosen = False
                    if isinstance(data.result.point.item, list):
                        coordsElements = data.result.point.item
                        for coel in coordsElements:
                            if chosen:
                                continue

                            if (coel.get_attribute(u'type') == u'stre'):
                                titulek = coel.get_attribute(u'title')
                                match = re.search(u'(.*), (.*), (.*), (.*), (.*)', titulek)
                                gr = match.groups()
                                if (match):
                                    street = gr[0]
                                    city = gr[1]

                                okStreet = False
                                okCity = False
                                if street == str(streetName).strip():
                                    okStreet = True
                                if city == str(cityName):
                                    okCity = True
                                if (okStreet and okCity):
                                    coordsElem = coel
                                    chosen = True
                                else:
                                    pywikibot.output(
                                        'WD page %s ' % str(streetName).strip() + ', ' + str(cityName) + ', ' + str(
                                            okrName))
                                    pywikibot.output('Souradky %s ' % titulek)
                                    choice = pywikibot.input_choice('Pouzit?',
                                                                    [('Yes', 'y'),
                                                                     ('No', 'n')],
                                                                    default='Y',
                                                                    automatic_quit=False)

                                    if choice == 'n':
                                        pass

                                    if choice == 'y':
                                        coordsElem = coel
                                        chosen = True

                    else:
                        coel = data.result.point.item
                        titulek = coel.get_attribute(u'title')

                        match = re.search(u'(.*), (.*), (.*), (.*), (.*)', titulek)
                        gr = match.groups()
                        if (match):
                            street = gr[0]
                            city = gr[1]

                        okStreet = False
                        okCity = False
                        if street == str(streetName).strip():
                            okStreet = True
                        if city == str(cityName):
                            okCity = True

                        if (okCity and okStreet):
                            coordsElem = coel
                        else:
                            pywikibot.output(
                                'WD page %s ' % str(streetName).strip() + ', ' + str(cityName) + ', ' + str(okrName))
                            pywikibot.output('Souradky %s ' % titulek)

                            choice = pywikibot.input_choice('Pouzit?',
                                                            [('Yes', 'y'),
                                                             ('No', 'n')],
                                                            default='Y',
                                                            automatic_quit=False)

                            if choice == 'n':
                                pass

                            if choice == 'y':
                                # coordsElem = coel
                                # chosen = True
                                coordsElem = coel
                    assert isinstance(coordsElem, untangle.Element)
                    line = {}
                    lat = coordsElem.get_attribute(u'y')
                    lon = coordsElem.get_attribute(u'x')
                    coord = pywikibot.Coordinate(float(lat), float(lon), precision=1e-07, globe='earth', site=self.repo)
                    # coords.setTarget(coord)
                    claim.setTarget(coord)
                    item.editEntity()
                    # item.addClaim(coords)
                except AttributeError as e:
                    # self.logger.logError(street_name)
                    # self.logger.logError(u'coord problems')
                    coords = None
                except UnboundLocalError as e:
                    # self.logger.logError(street_name)
                    # self.logger.logError(u'coord problems')
                    coords = None

                pass

                # self.logger.logComplete(ruian)
        sys.exit()

    def add_data(self):
        self.dataCoords = {}

        self.site = pywikibot.Site('cs', 'wikipedia')
        self.commons = self.site.image_repository()
        self.repo = self.site.data_repository()
        # self.commonsCategory = u'Category:Streets in Břeclav'
        self.genFactory = pagegenerators.GeneratorFactory(site=self.commons)
        # self.genFactory.handleArg('-ns:0')
        self.generator = self.genFactory.getCombinedGenerator()
        if not self.generator:
            if (self.pageOnly):
                self.genFactory.handleArg(u'-page:' + self.commonsCategory)
                self.generator = self.genFactory.getCombinedGenerator()
            else:
                self.genFactory.handleArg(u'-subcats:' + self.commonsCategory)
                self.generator = self.genFactory.getCombinedGenerator()

        # self.city_name = u'Břeclav'
        # self.city_nameCs = 'Břeclavi'
        # self.city_nameEn = 'Břeclav'
        city = pywikibot.Page(self.site, self.city_name)
        city_item = city.data_item()
        claims = city_item.get()
        lau_field = claims[u'claims'].get(u'P782', False)
        if (lau_field):
            lau = lau_field[0].target

        city_codes = self.streets.get(lau, False)
        for page in pagegenerators.PreloadingGenerator(self.generator):
            match3 = re.search(u'Category:(.*)', page.title())
            gr = match3.groups()
            prepstreetname = gr[0]
            if (prepstreetname in self.exclude):
                pywikibot.output('Excluded %s...' % page)
                continue
            match = re.search(u'(.*) (\((.*)\))', page.title())
            if not match:
                street_name = page.title()
            else:
                groups = match.groups()
                street_name = groups[0]

            match2 = re.search(u'Category:(.*)', street_name)
            gr = match2.groups()
            street_name = gr[0]

            street_code = city_codes.get(street_name, False)

            if (street_code == False):
                # pywikibot.output('Name náměstí -> nám. u %s' %page)
                # print street_name
                # street_name = street_name.replace(u'náměstí', u'náměstí')
                street_name = street_name.replace(u'náměstí', u'Náměstí')
                # print street_name

                street_code = city_codes.get(street_name, False)

            street_name = street_name[0].upper() + street_name[1:]

            if (street_code == False):
                # pywikibot.output('Name náměstí -> nám. u %s' %page)
                # print street_name
                street_name = street_name.replace(u'náměstí', u'nám.')
                street_name = street_name.replace(u'Náměstí', u'Nám.')
                # print street_name

                street_code = city_codes.get(street_name, False)

            if (street_code == False):
                # pywikibot.output('Name svaté -> sv. u %s' % page)
                # print street_name
                street_name = street_name.replace(u'Svatého', u'Sv.')
                street_name = street_name.replace(u'svatého', u'sv.')
                # print street_name
                street_code = city_codes.get(street_name, False)

            if (street_code == False):
                # pywikibot.output('Name svaté -> sv. u %s' % page)
                # print street_name
                street_name = street_name.replace(u'Svaté', u'Sv.')
                street_name = street_name.replace(u'svaté', u'sv.')
                # print street_name
                street_code = city_codes.get(street_name, False)
                if (street_code == False):
                    # pywikibot.output('Name nám. -> náměstí u %s' %page)
                    # print street_name
                    street_name_support = street_name
                    street_name_support = street_name_support.replace(u'nám.', u'náměstí')
                    street_name_support = street_name_support.replace(u'Nám.', u'Náměstí')
                    # print street_name

                    street_code = city_codes.get(street_name_support, False)

            if (street_code == False):
                # pywikibot.output('Name svaté -> sv. u %s' % page)
                # print street_name
                street_name = street_name.replace(u' Sv. ', u'Sv.')
                street_name = street_name.replace(u' sv. ', u'sv.')
                # print street_name
                street_code = city_codes.get(street_name, False)
                if (street_code == False):
                    # pywikibot.output('Name nám. -> náměstí u %s' %page)
                    # print street_name
                    street_name_support = street_name
                    street_name_support = street_name_support.replace(u'nám.', u'náměstí')
                    street_name_support = street_name_support.replace(u'Nám.', u'Náměstí')
                    # print street_name

                    street_code = city_codes.get(street_name_support, False)

            if (street_code == False):
                # pywikibot.output('Name marie -> M. u %s' % page)
                # print street_name
                street_name = street_name.replace(u'Marie ', u'M.')
                street_name = street_name.replace(u'marie ', u'm.')
                # print street_name
                street_code = city_codes.get(street_name, False)

            if (street_code == False):
                # pywikibot.output('Name marie -> M. u %s' % page)
                # print street_name
                street_name = street_name.replace(u'Profesora', u'Prof.')
                street_name = street_name.replace(u'profesora', u'prof.')
                # print street_name
                street_code = city_codes.get(street_name, False)
                if (street_code == False):
                    # pywikibot.output('Name nám. -> náměstí u %s' %page)
                    # print street_name
                    street_name_support = street_name
                    street_name_support = street_name_support.replace(u'nám.', u'náměstí')
                    street_name_support = street_name_support.replace(u'Nám.', u'Náměstí')
                    # print street_name

                    street_code = city_codes.get(street_name_support, False)

            if (street_code == False):
                # pywikibot.output('Name máje -> Máje u %s' % page)
                # print street_name
                street_name = street_name.replace(u'máje', u'Máje')
                # print street_name
                street_code = city_codes.get(street_name, False)

                if (street_code == False):
                    # pywikibot.output('Name nám. -> náměstí u %s' %page)
                    # print street_name
                    street_name_support = street_name
                    street_name_support = street_name_support.replace(u'nám.', u'náměstí')
                    street_name_support = street_name_support.replace(u'Nám.', u'Náměstí')
                    # print street_name

                    street_code = city_codes.get(street_name_support, False)

            if (street_code == False):
                # pywikibot.output('Name máje -> Máje u %s' % page)
                # print street_name
                street_name = street_name.replace(u'května', u'Května')
                # print street_name
                street_code = city_codes.get(street_name, False)

                if (street_code == False):
                    # pywikibot.output('Name nám. -> náměstí u %s' %page)
                    # print street_name
                    street_name_support = street_name
                    street_name_support = street_name_support.replace(u'nám.', u'náměstí')
                    street_name_support = street_name_support.replace(u'Nám.', u'Náměstí')
                    # print street_name

                    street_code = city_codes.get(street_name_support, False)

            if (street_code == False):
                # pywikibot.output('Name Jaromíra -> J. u %s' %page)
                # print street_name
                street_name = street_name.replace(u'Čeňka', u'Č.')
                # print street_name

                street_code = city_codes.get(street_name, False)
                if (street_code == False):
                    # pywikibot.output('Name nám. -> náměstí u %s' %page)
                    # print street_name
                    street_name_support = street_name
                    street_name_support = street_name_support.replace(u'nám.', u'náměstí')
                    street_name_support = street_name_support.replace(u'Nám.', u'Náměstí')
                    # print street_name

                    street_code = city_codes.get(street_name_support, False)

            if (street_code == False):
                # pywikibot.output('Name května -> Května u %s' % page)
                # print street_name
                street_name = street_name.replace(u'května', u'Května')
                # print street_name
                street_code = city_codes.get(street_name, False)

            if (street_code == False):
                # pywikibot.output('Name Jaromíra -> J. u %s' %page)
                # print street_name
                street_name = street_name.replace(u'Jaromíra', u'J.')
                # print street_name

                street_code = city_codes.get(street_name, False)

            if (street_code == False):
                # pywikibot.output('Name Jaromíra -> J. u %s' %page)
                # print street_name
                street_name = street_name.replace(u'Aloise', u'A.')
                # print street_name

                street_code = city_codes.get(street_name, False)

            if (street_code == False):
                # pywikibot.output('Name Jaromíra -> J. u %s' %page)
                # print street_name
                street_name = street_name.replace(u'Přemysla', u'Př.')
                # print street_name

                street_code = city_codes.get(street_name, False)

            if (street_code == False):
                # pywikibot.output('Name Jaromíra -> J. u %s' %page)
                # print street_name
                street_name = street_name.replace(u'Československých', u'Čs.')
                # print street_name

                street_code = city_codes.get(street_name, False)

            if (street_code == False):
                # pywikibot.output('Name Jaromíra -> J. u %s' %page)
                # print street_name
                street_name = street_name.replace(u'Emila', u'E.')
                # print street_name

                street_code = city_codes.get(street_name, False)

            if (street_code == False):
                # pywikibot.output('Name Jaromíra -> J. u %s' %page)
                # print street_name
                street_name = street_name.replace(u'Svatopluka', u'Svat.')
                # print street_name

                street_code = city_codes.get(street_name, False)

            if (street_code == False):
                # pywikibot.output('Name Jaromíra -> J. u %s' %page)
                # print street_name
                street_name = street_name.replace(u'Františka', u'F.')
                # print street_name

                street_code = city_codes.get(street_name, False)

            if (street_code == False):
                # pywikibot.output('Name Jaromíra -> J. u %s' %page)
                # print street_name
                street_name = street_name.replace(u'Hildy', u'H.')
                # print street_name

                street_code = city_codes.get(street_name, False)

            if (street_code == False):
                # pywikibot.output('Name Jaromíra -> J. u %s' %page)
                # print street_name
                street_name = street_name.replace(u'Františka', u'Fr.')
                # print street_name

                street_code = city_codes.get(street_name, False)

            if (street_code == False):
                # pywikibot.output('Name Jaromíra -> J. u %s' %page)
                # print street_name
                street_name = street_name.replace(u'Jana', u'J.')
                # print street_name

                street_code = city_codes.get(street_name, False)

            if (street_code == False):
                # pywikibot.output('Name Jaromíra -> J. u %s' %page)
                # print street_name
                street_name = street_name.replace(u'Karla', u'K.')
                # print street_name

                street_code = city_codes.get(street_name, False)
                if (street_code == False):
                    # pywikibot.output('Name nám. -> náměstí u %s' %page)
                    # print street_name
                    street_name_support = street_name
                    street_name_support = street_name_support.replace(u'nám.', u'náměstí')
                    street_name_support = street_name_support.replace(u'Nám.', u'Náměstí')
                    # print street_name

                    street_code = city_codes.get(street_name_support, False)

            if (street_code == False):
                # pywikibot.output('Name Jaromíra -> J. u %s' %page)
                # print street_name
                street_name = street_name.replace(u'Masaryka', u'M.')
                # print street_name

                street_code = city_codes.get(street_name, False)
                if (street_code == False):
                    # pywikibot.output('Name nám. -> náměstí u %s' %page)
                    # print street_name
                    street_name_support = street_name
                    street_name_support = street_name_support.replace(u'nám.', u'náměstí')
                    street_name_support = street_name_support.replace(u'Nám.', u'Náměstí')
                    # print street_name

                    street_code = city_codes.get(street_name_support, False)

            if (street_code == False):
                # pywikibot.output('Name Jaromíra -> J. u %s' %page)
                # print street_name
                street_name = street_name.replace(u'Národního', u'Nár.')
                # print street_name

                street_code = city_codes.get(street_name, False)

            if (street_code == False):
                # pywikibot.output('Name Jaromíra -> J. u %s' %page)
                # print street_name
                street_name = street_name.replace(u'povstání', u'povst.')
                # print street_name

                street_code = city_codes.get(street_name, False)

            if (street_code == False):
                # pywikibot.output('Name Jaromíra -> J. u %s' %page)
                # print street_name
                street_name = street_name.replace(u'T. G. ', u'T.G.')
                # print street_name

                street_code = city_codes.get(street_name, False)

            if (street_code == False):
                # pywikibot.output('Name Jaromíra -> J. u %s' %page)
                # print street_name
                groupsWords = street_name.split()
                if (len(groupsWords) == 2):
                    street_name = groupsWords[1][0].upper() + groupsWords[1][1:] + ' ' + groupsWords[0]

                # print street_name

                street_code = city_codes.get(street_name, False)

            if (street_code == False):
                # pywikibot.output('Name nám. -> náměstí u %s' %page)
                # print street_name
                street_name = street_name.replace(u'nám.', u'náměstí')
                street_name = street_name.replace(u'Nám.', u'Náměstí')
                # print street_name

                street_code = city_codes.get(street_name, False)

            if (street_code == False):
                # pywikibot.output('Name náměstí -> Náměstí u %s' %page)
                # print street_name
                street_name = street_name.replace(u'náměstí', u'Náměstí')
                # print street_name

                street_code = city_codes.get(street_name, False)

            if (not self.logger.isCompleteFile(street_code)):

                sources = []

                source = pywikibot.Claim(self.repo, 'P248')
                source.setTarget(pywikibot.ItemPage(self.repo, 'Q12049125'))

                instance = pywikibot.Claim(self.repo, 'P31')
                if (self.instance == 'street'):
                    instance.setTarget(pywikibot.ItemPage(self.repo, 'Q79007'))
                else:
                    instance.setTarget(pywikibot.ItemPage(self.repo, 'Q174782'))

                country = pywikibot.Claim(self.repo, 'P17')
                country.setTarget(pywikibot.ItemPage(self.repo, 'Q213'))

                admin = pywikibot.Claim(self.repo, 'P131')
                admin.setTarget(city_item)

                try:
                    coords = pywikibot.Claim(self.repo, 'P625')

                    payload = {'query': street_name + ', ' + self.city_name}
                    r = requests.get(u'https://api.mapy.cz/geocode', params=payload)
                    xml = r.text
                    data = untangle.parse(xml)

                    # print xml
                    # print data
                    # sys.exit()
                    if isinstance(data.result.point.item, list):
                        coordsElem = data.result.point.item[0]
                    else:
                        coordsElem = data.result.point.item
                    assert isinstance(coordsElem, untangle.Element)
                    line = {}
                    lat = coordsElem.get_attribute(u'y')
                    lon = coordsElem.get_attribute(u'x')
                    coord = pywikibot.Coordinate(float(lat), float(lon), precision=1e-07, globe='earth', site=self.repo)
                    coords.setTarget(coord)
                except AttributeError as e:
                    self.logger.logError(street_name)
                    self.logger.logError(u'coord problems')
                    coords = None

                try:
                    code = pywikibot.Claim(self.repo, 'P4533')
                    code.setTarget(street_code)

                    sources.append(source)
                except:
                    self.logger.logError(street_name)
                    self.logger.logError(self.city_name)
                    self.logger.logError(u'street code problem')
                    continue

                try:
                    item = page.data_item()
                    print(item.title())


                except pywikibot.exceptions.NoPage as e:
                    data = {}
                    data.setdefault('sitelinks', {}).update({
                        page.site.dbName(): {
                            'site': page.site.dbName(),
                            'title': page.title()
                        }
                    })

                    if (self.instance == 'street'):
                        data.setdefault('descriptions', {}).update({
                            page.site.lang: {
                                'language': 'en',
                                'value': 'street in ' + self.city_nameEn
                            },
                            'cs': {
                                'language': 'cs',
                                'value': 'ulice v ' + self.city_nameCs
                            },
                        })
                    else:
                        data.setdefault('descriptions', {}).update({
                            page.site.lang: {
                                'language': 'en',
                                'value': 'square in ' + self.city_nameEn
                            },
                            'cs': {
                                'language': 'cs',
                                'value': 'náměstí v ' + self.city_nameCs
                            },
                        })
                    try:
                        item = self.create_item_for_page(page, data=data)
                    except pywikibot.OtherPageSaveError as e:
                        self.logger.logError(street_name)

                claimsStreet = item.get()
                if (not claimsStreet[u'claims'].get(u'P31', False)):
                    item.addClaim(instance)
                if (not claimsStreet[u'claims'].get(u'P17', False)):
                    item.addClaim(country)
                if (not claimsStreet[u'claims'].get(u'P131', False)):
                    item.addClaim(admin)
                if (not claimsStreet[u'claims'].get(u'P625', False)):
                    if coords:
                        item.addClaim(coords)
                if (not claimsStreet[u'claims'].get(u'P4533', False)):
                    if code:
                        item.addClaim(code)
                        code.addSources(sources)

                self.logger.logComplete(street_code)

    def create_item_for_page(self, page=None, data=None, summary=None, repo=None):
        if not summary:
            summary = (u'Bot: New item with sitelink from %s'
                       % page.title(asLink=True, insite=self.repo))

        if data is None:
            data = {}
            data.setdefault('sitelinks', {}).update({
                page.site.dbName(): {
                    'site': page.site.dbName(),
                    'title': page.title()
                }
            })
            data.setdefault('labels', {}).update({
                page.site.lang: {
                    'language': page.site.lang,
                    'value': page.title()
                }
            })

        if (not page):
            pywikibot.output('Creating item')
            item = pywikibot.ItemPage(repo)
        else:
            pywikibot.output('Creating item for %s...' % page)
            item = pywikibot.ItemPage(page.site.data_repository())

        result = self.user_edit_entity(item, data, summary=summary)
        if result:
            return item
        else:
            return None

    def user_edit_entity(self, item, data=None, summary=None):
        """
        Edit entity with data provided, with user confirmation as required.

        @param item: page to be edited
        @type item: ItemPage
        @param data: data to be saved, or None if the diff should be created
          automatically
        @kwarg summary: revision comment, passed to ItemPage.editEntity
        @type summary: str
        @kwarg show_diff: show changes between oldtext and newtext (default:
          True)
        @type show_diff: bool
        @kwarg ignore_server_errors: if True, server errors will be reported
          and ignored (default: False)
        @type ignore_server_errors: bool
        @kwarg ignore_save_related_errors: if True, errors related to
          page save will be reported and ignored (default: False)
        @type ignore_save_related_errors: bool
        @return: whether the item was saved successfully
        @rtype: bool
        """
        return self._save_page(item, item.editEntity, data)

    def _save_page(self, page, func, *args):
        """
        Helper function to handle page save-related option error handling.

        @param page: currently edited page
        @param func: the function to call
        @param args: passed to the function
        @param kwargs: passed to the function
        @kwarg ignore_server_errors: if True, server errors will be reported
          and ignored (default: False)
        @kwtype ignore_server_errors: bool
        @kwarg ignore_save_related_errors: if True, errors related to
        page save will be reported and ignored (default: False)
        @kwtype ignore_save_related_errors: bool
        @return: whether the page was saved successfully
        @rtype: bool
        """
        func(*args)
        return True

    def run(self):
        pass


# def __init__(self, logger_name, category, city_name, city_nameCs, city_nameEn):

# a = streets('streets', None, '', '', '', '', True)
# streetsDict = a.load_csv()
# pywikibot.output('loaded CSV')
#

a = streets('streets', None, '', '', '', '', True)

# f = streets('squares', streetsDict, u'Category:Náměstí Profesora Drahoňovského', 'Rovensko pod Troskami', 'Rovensku pod Troskami', 'Rovensko pod Troskami')
# f = streets('squares', streetsDict, u'Category:Náměstí T. G. Masaryka (Lipník nad Bečvou)', 'Lipník nad Bečvou', 'Lipníku nad Bečvou', 'Lipník nad Bečvou')
# f = streets('squares', streetsDict, u'Category:Náměstí Profesora Čeňka Strouhala', 'Seč (okres Chrudim)', 'Seči', 'Seč')
# f = streets('squares', streetsDict, u'Category:Náměstí Sv. Trojice (Choltice)', 'Choltice', 'Cholticích', 'Choltice')
# f = streets('squares', streetsDict, u'Category:Náměstí Na podkově', 'Bystré (okres Svitavy)', 'Bystré', 'Bystré')
# f = streets('squares', streetsDict, u'Category:Náměstí T. G. Masaryka (Dobřany)', 'Dobřany', 'Dobřanech', 'Dobřany')
# f = streets('squares', streetsDict, u'Category:Mírové náměstí (Netolice)', 'Netolice', 'Netolicích', 'Netolice')
# f = streets('squares', streetsDict, u'Category:Masarykovo náměstí (Boskovice)', 'Boskovice', 'Boskovicích', 'Boskovice')
# f = streets('squares', streetsDict, u'Category:Náměstí T. G. Masaryka (Golčův Jeníkov)', 'Golčův Jeníkov', 'Golčově Jeníkově', 'Golčův Jeníkov')
# f = streets('squares', streetsDict, u'Category:Náměstí svatého Ondřeje', 'Uherský Ostroh', 'Uherském Ostrohu', 'Uherský Ostroh')
# f = streets('squares', streetsDict, u'Category:Hildy Synkové', 'Brumov-Bylnice', 'Brumově', 'Brumov')
# f = streets('squares', streetsDict, u'Category:Náměstí míru (Březno)', 'Březno (okres Chomutov)', 'Březně', 'Březno')
#
# f = streets('squares', streetsDict, u'Category:Náměstí Republiky (Soběslav)', 'Soběslav', 'Soběslavi', 'Soběslav')
# f = streets('squares', streetsDict, u'Category:Náměstí Republiky (Studénka)', 'Studénka', 'Studénce', 'Studénka')
# f = streets('squares', streetsDict, u'Category:Náměstí Republiky (Tachov)', 'Tachov', 'Tachově', 'Tachov')
# f = streets('squares', streetsDict, u'Category:Náměstí Republiky (Trutnov)', 'Trutnov', 'Trutnově', 'Trutnov')
# f = streets('squares', streetsDict, u'Category:Náměstí Republiky (Uherské Hradiště)', 'Uherské Hradiště', 'Uherském Hradišti', 'Uherské Hradiště')
# f = streets('squares', streetsDict, u'Category:Náměstí Republiky (Znojmo)', 'Znojmo', 'Znojmě', 'Znojmo')
# f = streets('squares', streetsDict, u'Category:Náměstí Republiky (Žďár nad Sázavou)', 'Žďár nad Sázavou', 'Žďáru nad Sázavou', 'Žďár nad Sázavou')
# f = streets('squares', streetsDict, u'Category:Mírové náměstí (Louny)', 'Louny', 'Lounech', 'Louny')
# f = streets('squares', streetsDict, u'Category:Mírové náměstí (Kadaň)', 'Kadaň', 'Kadani', 'Kadaň')
# f = streets('squares', streetsDict, u'Category:Náměstí Krále Jiřího z Poděbrad (Cheb)', 'Cheb', 'Chebu', 'Cheb')
# f = streets('squares', streetsDict, u'Category:Náměstí Přemysla Otakara II. (České Budějovice)', 'České Budějovice', 'Českých Budějovicích', 'České Budějovice')
# f = streets('squares', streetsDict, u'Category:Náměstí Svobody (Vimperk)', 'Vimperk', 'Vimperku', 'Vimperk')
# f = streets('squares', streetsDict, u'Category:Obilní trh', 'Brno', 'Brně', 'Brno')
# # f = streets('squares', streetsDict, u'Category:Ovocný trh (Prague)', 'Praha', 'Praze', 'Praha')
# f = streets('squares', streetsDict, u'Category:Rybí trh', 'Opava', 'Opavě', 'Opava')
# f = streets('squares', streetsDict, u'Category:Náměstí Svobody (Teplice)', 'Teplice', 'Teplicích', 'Teplice')
# f = streets('squares', streetsDict, u'Category:Tržiště (Karlovy Vary)', 'Karlovy Vary', 'Karlových Varech', 'Karlovy Vary')
# f = streets('squares', streetsDict, u'Category:Tržiště (Prague)', 'Praha', 'Praze', 'Praha')
# f = streets('squares', streetsDict, u'Category:Tržní náměstí (Teplice)', 'Teplice', 'Teplicích', 'Teplice')
# # f = streets('squares', streetsDict, u'Category:Uhelný trh', 'Praha', 'Praze', 'Praha')
# f = streets('squares', streetsDict, u'Category:Wenceslas Square', 'Praha', 'Praze', 'Praha')
# f = streets('squares', streetsDict, u'Category:Zelný trh (Brno)', 'Brno', 'Brně', 'Brno')

#
# f = streets('squares', streetsDict, u'Category:Náměstí Jana Žižky (Louňovice pod Blaníkem)', 'Louňovice pod Blaníkem', 'Louňovicích pod Blaníkem', 'Louňovice pod Blaníkem')
# f = streets('squares', streetsDict, u'Category:Náměstí T. G. Masaryka (Pyšely) ', 'Pyšely', 'Pyšelech', 'Pyšelech')
#
# f = streets('squares', streetsDict, u'Category:Náměstí Voskovce a Wericha', 'Sázava (okres Benešov)', 'Sázavě', 'Sázava')
# f = streets('squares', streetsDict, u'Category:Náměstí (Trhový Štěpánov)', 'Trhový Štěpánov', 'Trhovém Štěpánově', 'Trhový Štěpánov')
# f = streets('squares', streetsDict, u'Category:Tyršovo náměstí (Hostomice)', 'Hostomice (okres Beroun)', 'Hostomicích', 'Hostomice')
# f = streets('squares', streetsDict, u'Category:Palackého náměstí (Hořovice)', 'Hořovice', 'Hořovicích', 'Hořovice')
# f = streets('squares', streetsDict, u'Category:Náměstí Míru (Komárov)', 'Komárov (okres Beroun)', 'Komárově', 'Komárov')
# f = streets('squares', streetsDict, u'Category:Náměstí Míru (Králův Dvůr)', 'Králův Dvůr', 'Králově Dvoře', 'Králův Dvůr')
# f = streets('squares', streetsDict, u'Category:Náměstí (Liteň)', 'Liteň', 'Litni', 'Liteň')
# f = streets('squares', streetsDict, u'Category:Náves U Lípy', 'Srbsko (okres Beroun)', 'Srbsku', 'Srbsku')
# f = streets('squares', streetsDict, u'Category:Palackého náměstí (Zdice)', 'Zdice', 'Zdicích', 'Zdice')
# f = streets('squares', streetsDict, u'Category:Náměstí (Buštěhrad)', 'Buštěhrad', 'Buštěhradě', 'Buštěhrad')
# f = streets('squares', streetsDict, u'Category:Masarykovo náměstí (Lány)', 'Lány (okres Kladno)', 'Lány', 'Lány')
# f = streets('squares', streetsDict, u'Category:Mírové náměstí (Stochov)', 'Stochov', 'Stochově', 'Stochov')
# f = streets('squares', streetsDict, u'Category:Václavské náměstí (Unhošť)', 'Unhošť', 'Unhošti', 'Unhošť')
# f = streets('squares', streetsDict, u'Category:Náměstí Krále Vladislava', 'Velvary', 'Velvarech', 'Velvary')
# f = streets('squares', streetsDict, u'Category:Náměstí Pod Lipami', 'Zlonice', 'Zlonicích', 'Zlonice')
# f = streets('squares', streetsDict, u'Category:Komenského náměstí (Zásmuky)', 'Zásmuky', 'Zásmukách', 'Zásmuky')
# f = streets('squares', streetsDict, u'Category:Žižkovo náměstí (Malešov)', 'Malešov', 'Malešově', 'Malešov')
# f = streets('squares', streetsDict, u'Category:Masarykovo náměstí (Nové Dvory)', 'Nové Dvory (okres Kutná Hora)', 'Nových Dvorech', 'Nové Dvory')
# f = streets('squares', streetsDict, u'Category:Václavské náměstí (Uhlířské Janovice)', 'Uhlířské Janovice', 'Uhlířských Janovicích', 'Uhlířské Janovice')
# f = streets('squares', streetsDict, u'Category:Komenského náměstí (Kostelec nad Labem)', 'Kostelec nad Labem', 'Kostelci nad Labem', 'Kostelec nad Labem')
# f = streets('squares', streetsDict, u'Category:Náměstí Míru (Mšeno)', 'Mšeno', 'Mšeně', 'Mšeno')
# f = streets('squares', streetsDict, u'Category:Na Náměstí (Všetaty)', 'Všetaty (okres Mělník)', 'Všetatech', 'Všetaty')
# f = streets('squares', streetsDict, u'Category:Náměstí A. Dvořáka (Veltrusy)', 'Veltrusy', 'Veltrusech', 'Veltrusy')
# f = streets('squares', streetsDict, u'Category:Mírové náměstí (Bakov nad Jizerou)', 'Bakov nad Jizerou', 'Bakově nad Jizerou', 'Bakov nad Jizerou')
# f = streets('squares', streetsDict, u'Category:Husovo náměstí (Benátky nad Jizerou)', 'Benátky nad Jizerou', 'Benátkách nad Jizerou', 'Benátky nad Jizerou')
# f = streets('squares', streetsDict, u'Category:Náměstí Budovatelů (Katusice)', 'Katusice', 'Katusicích', 'Katusice')
# f = streets('squares', streetsDict, u'Category:Masarykovo náměstí (Mnichovo Hradiště)', 'Mnichovo Hradiště', 'Mnichově Hradišti', 'Mnichovo Hradiště')
# f = streets('squares', streetsDict, u'Category:Náměstí (Rožďalovice)', 'Rožďalovice', 'Rožďalovicích', 'Rožďalovice')
# f = streets('squares', streetsDict, u'Category:Palackého náměstí (Sadská)', 'Sadská', 'Sadské', 'Sadská')
# f = streets('squares', streetsDict, u'Category:Třebízského náměstí (Klecany)', 'Klecany', 'Klecanech', 'Klecany')
# f = streets('squares', streetsDict, u'Category:Dolní náměstí (Odolena Voda)', 'Odolena Voda', 'Odolene Vodě', 'Odolena Voda')
# f = streets('squares', streetsDict, u'Category:Na městečku (Stříbrná Skalice)', 'Stříbrná Skalice', 'Stříbrné Skalici', 'Stříbrná Skalice')
# f = streets('squares', streetsDict, u'Category:Náměstí Na Sádkách', 'Dolní Břežany', 'Dolních Břežanech', 'Dolní Břežany')
# f = streets('squares', streetsDict, u'Category:Husovo náměstí (Hostivice)', 'Hostivice', 'Hostivici', 'Hostivice')
# f = streets('squares', streetsDict, u'Category:Malé náměstí (Jeneč)', 'Jeneč', 'Jenči', 'Jeneč')
# f = streets('squares', streetsDict, u'Category:Masarykovo náměstí (Jílové u Prahy)', 'Jílové u Prahy', 'Jílovém u Prahy', 'Jílové u Prahy')
# f = streets('squares', streetsDict, u'Category:Náměstí Republiky (Neratovice)', 'Neratovice', 'Neratovicích', 'Neratovice')
# f = streets('squares', streetsDict, u'Category:Náměstí V. Levého (Liběchov)', 'Liběchov', 'Liběchově', 'Liběchov')
# f = streets('squares', streetsDict, u'Category:Náměstí (Březnice)', 'Březnice', 'Březnici', 'Březnice')
# # f = streets('squares', streetsDict, u'Category:Square in Kosova Hora', 'Kosova Hora', 'Kosově Hoře', 'Kosova Hora')
# # f = streets('squares', streetsDict, u'Category:Square in Krásná Hora nad Vltavou', 'Krásná Hora nad Vltavou', 'Krásné Hoře nad Vltavou', 'Krásná Hora nad Vltavou')
# f = streets('squares', streetsDict, u'Category:Náměstí Jiřího z Poděbrad (Nový Knín)', 'Nový Knín', 'Novém Kníně', 'Nový Knín')
# f = streets('squares', streetsDict, u'Category:Náměstí (Rožmitál pod Třemšínem)', 'Rožmitál pod Třemšínem', 'Rožmitále pod Třemšínem', 'Rožmitál pod Třemšínem')
# f = streets('squares', streetsDict, u'Category:Komenského náměstí (Nové Strašecí)', 'Nové Strašecí', 'Novém Strašecí', 'Nové Strašecí')
# f = streets('squares', streetsDict, u'Category:Klicperovo náměstí', 'Chlumec nad Cidlinou', 'Chlumci nad Cidlinou', 'Chlumec nad Cidlinou')
# f = streets('squares', streetsDict, u'Category:Masarykovo náměstí (Třebechovice pod Orebem)', 'Třebechovice pod Orebem', 'Třebechovicích pod Orebem', 'Třebechovice pod Orebem')
# f = streets('squares', streetsDict, u'Category:Hilmarovo náměstí', 'Kopidlno', 'Kopidlně', 'Kopidlno')
# f = streets('squares', streetsDict, u'Category:Náměstí Svobody (Libáň)', 'Libáň', 'Libáni', 'Libáň')
# f = streets('squares', streetsDict, u'Category:Náměstí K. V. Raise', 'Lázně Bělohrad', 'Lázních Bělohrad', 'Lázně Bělohrad')
# f = streets('squares', streetsDict, u'Category:Náměstí K. J. Erbena', 'Miletín', 'Miletíně', 'Miletín')
# f = streets('squares', streetsDict, u'Category:Náměstí (Mlázovice)', 'Mlázovice', 'Mlázovicích', 'Mlázovice')
# f = streets('squares', streetsDict, u'Category:Masarykovo náměstí (Nová Paka)', 'Nová Paka', 'Nové Pace', 'Nová Paka')
# # f = streets('squares', streetsDict, u'Category:Square in Pecka', 'Pecka (okres Jičín)', 'Pecce', 'Pecka')
# f = streets('squares', streetsDict, u'Category:Náměstí Svobody (Železnice)', 'Železnice (okres Jičín)', 'Železnice', 'Železnice')
# # f = streets('squares', streetsDict, u'Category:Square in Machov', 'Machov', 'Machově', 'Machov')
# # f = streets('squares', streetsDict, u'Category:Square in Stárkov', 'Stárkov', 'Stárkově', 'Stárkov')
# f = streets('squares', streetsDict, u'Category:Náměstí T. G. Masaryka (Červený Kostelec)', 'Červený Kostelec', 'Červeném Kostelci', 'Červený Kostelec')
# f = streets('squares', streetsDict, u'Category:Husovo náměstí (Česká Skalice)', 'Česká Skalice', 'České Skalici', 'Česká Skalice')
# f = streets('squares', streetsDict, u'Category:Masarykovo náměstí (Solnice)', 'Solnice (okres Rychnov nad Kněžnou)', 'Solnici', 'Solnice')
# f = streets('squares', streetsDict, u'Category:Mírové náměstí (Týniště nad Orlicí)', 'Týniště nad Orlicí', 'Týništi nad Orlicí', 'Týniště nad Orlicí')
# f = streets('squares', streetsDict, u'Category:Náměstí Karla Čapka', 'Malé Svatoňovice', 'Malých Svatoňovicích', 'Malé Svatoňovice')
# f = streets('squares', streetsDict, u'Category:Náměstí (Pilníkov)', 'Pilníkov', 'Pilníkově', 'Pilníkov')
# f = streets('squares', streetsDict, u'Category:Náměstí Svornosti (Svoboda nad Úpou)', 'Svoboda nad Úpou', 'Svobodě nad Úpou', 'Svoboda nad Úpou')
# f = streets('squares', streetsDict, u'Category:Náměstí T. G. Masaryka (Úpice)', 'Úpice', 'Úpici', 'Úpice')
# # f = streets('squares', streetsDict, u'Category:Square in Černý Důl', 'Černý Důl', 'Černém Dole', 'Černý Důl')
# f = streets('squares', streetsDict, u'Category:Rýchorské náměstí', 'Žacléř', 'Žacléři', 'Žacléř')
# f = streets('squares', streetsDict, u'Category:Náměstí míru (Františkovy Lázně)', 'Františkovy Lázně', 'Františkových Lázních', 'Františkovy Lázně')
# f = streets('squares', streetsDict, u'Category:Náměstí 5. května (Luby)', 'Luby', 'Lubech', 'Luby')
# f = streets('squares', streetsDict, u'Category:Masarykovo náměstí (Teplá)', 'Teplá', 'Teplé', 'Teplá')
# f = streets('squares', streetsDict, u'Category:Náměstí 5. května (Bečov nad Teplou)', 'Bečov nad Teplou', 'Bečově nad Teplou', 'Bečov nad Teplou')
# f = streets('squares', streetsDict, u'Category:Náměstí Míru (Bochov)', 'Bochov', 'Bochově', 'Bochov')
# f = streets('squares', streetsDict, u'Category:Žižkovo náměstí (Chyše)', 'Chyše', 'Chyši', 'Chyše')
# f = streets('squares', streetsDict, u'Category:Náměstí Svatého Vavřince', 'Horní Blatná', 'Horní Blatné', 'Horní Blatná')
# f = streets('squares', streetsDict, u'Category:Krušnohorské náměstí', 'Hroznětín', 'Hroznětíně', 'Hroznětín')
# f = streets('squares', streetsDict, u'Category:Náměstí Republiky (Jáchymov)', 'Jáchymov', 'Jáchymově', 'Jáchymov')
# f = streets('squares', streetsDict, u'Category:Náměstí Karla IV. (Nejdek)', 'Nejdek', 'Nejdku', 'Nejdek')
# f = streets('squares', streetsDict, u'Category:Náměstí Dr. Karla Kramáře', 'Vysoké nad Jizerou', 'Vysokém nad Jizerou', 'Vysoké nad Jizerou')
# f = streets('squares', streetsDict, u'Category:Masarykovo náměstí (Dubá)', 'Dubá', 'Dubé', 'Dubá')
# f = streets('squares', streetsDict, u'Category:Náměstí 5. května (Stráž pod Ralskem)', 'Stráž pod Ralskem', 'Stráži pod Ralskem', 'Stráž pod Ralskem')
# f = streets('squares', streetsDict, u'Category:Náměstí (Kravaře)', 'Kravaře', 'Kravařích', 'Kravaře')
# f = streets('squares', streetsDict, u'Category:Náměstí 1. máje (Chrastava)', 'Chrastava', 'Chrastavě', 'Chrastava')
# f = streets('squares', streetsDict, u'Category:Náměstí T. G. Masaryka (Hodkovice nad Mohelkou)', 'Hodkovice nad Mohelkou', 'Hodkovicích nad Mohelkou', 'Hodkovice nad Mohelkou')
# f = streets('squares', streetsDict, u'Category:Horní náměstí (Hrádek nad Nisou)', 'Hrádek nad Nisou', 'Hrádku nad Nisou', 'Hrádek nad Nisou')
# f = streets('squares', streetsDict, u'Category:Mírové náměstí (Nové Město pod Smrkem)', 'Nové Město pod Smrkem', 'Novém Městě pod Smrkem', 'Nové Město pod Smrkem')
# f = streets('squares', streetsDict, u'Category:Svatovítské náměstí (Osečná)', 'Osečná', 'Osečné', 'Osečná')
# f = streets('squares', streetsDict, u'Category:Náměstí (Dvorce)', 'Dvorce (okres Bruntál)', 'Dvorcích', 'Dvorce')
# f = streets('squares', streetsDict, u'Category:Náměstí ČSA (Město Albrechtice)', 'Město Albrechtice', 'Městě Albrechtice', 'Město Albrechtice')
# f = streets('squares', streetsDict, u'Category:Na Náměstí (Osoblaha)', 'Osoblaha', 'Osoblaze', 'Osoblaha')
# f = streets('squares', streetsDict, u'Category:Náměstí Míru (Ryžoviště)', 'Ryžoviště (okres Bruntál)', 'Ryžovišti', 'Ryžoviště')
# f = streets('squares', streetsDict, u'Category:Náměstí Svatého Michala', 'Vrbno pod Pradědem', 'Vrbně pod Pradědem', 'Vrbno pod Pradědem')
# f = streets('squares', streetsDict, u'Category:Náměstí J. A. Komenského (Brušperk)', 'Brušperk', 'Brušperku', 'Brušperk')
# f = streets('squares', streetsDict, u'Category:Náměstí (Frýdlant nad Ostravicí)', 'Frýdlant nad Ostravicí', 'Frýdlantě nad Ostravicí', 'Frýdlant nad Ostravicí')
# f = streets('squares', streetsDict, u'Category:Staré náměstí (Orlová)', 'Orlová', 'Orlové', 'Orlová')
# f = streets('squares', streetsDict, u'Category:Slezské náměstí', 'Bílovec', 'Bílovci', 'Bílovec')
# f = streets('squares', streetsDict, u'Category:Náměstí Míru (Frenštát pod Radhoštěm)', 'Frenštát pod Radhoštěm', 'Frenštátě pod Radhoštěm', 'Frenštát pod Radhoštěm')
# f = streets('squares', streetsDict, u'Category:Náměstí Komenského (Fulnek)', 'Fulnek', 'Fulneku', 'Fulnek')
# f = streets('squares', streetsDict, u'Category:Masarykovo náměstí (Odry)', 'Odry', 'Odrách', 'Odry')
# f = streets('squares', streetsDict, u'Category:Náměstí Sigmunda Freuda (Příbor)', 'Příbor (okres Nový Jičín)', 'Příboře', 'Příbor')
# # f = streets('squares', streetsDict, u'Category:Square in Starý Jičín', 'Žacléř', 'Žacléři', 'Žacléř')
# f = streets('squares', streetsDict, u'Category:Náměstí Republiky (Studénka)', 'Studénka', 'Studénce', 'Studénka')
# f = streets('squares', streetsDict, u'Category:Náměstí (Štramberk)', 'Štramberk', 'Štramberku', 'Štramberk')
# f = streets('squares', streetsDict, u'Category:Mírové náměstí (Hlučín)', 'Hlučín', 'Hlučíně', 'Hlučín')
# f = streets('squares', streetsDict, u'Category:Náměstí Jana Zajíce', 'Vítkov', 'Vítkově', 'Vítkov')
# f = streets('squares', streetsDict, u'Category:Náměstí (Klimkovice)', 'Klimkovice', 'Klimkovicích', 'Klimkovice')
# f = streets('squares', streetsDict, u'Category:Náměstí Míru (Loštice)', 'Loštice', 'Lošticích', 'Loštice')
# f = streets('squares', streetsDict, u'Category:Náměstí Osvobození (Staré Město)', 'Staré Město (okres Šumperk)', 'Starém Městě', 'Staré Město')
# f = streets('squares', streetsDict, u'Category:Náměstí Míru (Štíty)', 'Štíty', 'Štítech', 'Štíty')
# f = streets('squares', streetsDict, u'Category:Náměstí Svobody (Javorník)', 'Javorník (okres Jeseník)', 'Javorníku', 'Javorník')
# f = streets('squares', streetsDict, u'Category:Mírové náměstí (Vidnava)', 'Vidnava', 'Vidnavě', 'Vidnava')
# f = streets('squares', streetsDict, u'Category:Náměstí Svobody (Zlaté Hory)', 'Zlaté Hory', 'Zlatých Horách', 'Zlaté Hory')
# f = streets('squares', streetsDict, u'Category:Lipové náměstí (Křelov)', 'Křelov-Břuchotín', 'Křelově', 'Křelov')
# f = streets('squares', streetsDict, u'Category:Náměstí 9. května (Moravský Beroun)', 'Moravský Beroun', 'Moravském Berouně', 'Moravský Beroun')
# f = streets('squares', streetsDict, u'Category:Náměstí (Velký Újezd)', 'Velký Újezd', 'Velkém Újezdu', 'Velký Újezd')
# f = streets('squares', streetsDict, u'Category:Masarykovo náměstí (Konice)', 'Konice', 'Konicích', 'Konice')
# f = streets('squares', streetsDict, u'Category:Jakubské náměstí (Kostelec na Hané)', 'Kostelec na Hané', 'Kostelci na Hané', 'Kostelec na Hané')
# f = streets('squares', streetsDict, u'Category:Masarykovo náměstí (Brodek u Přerova)', 'Brodek u Přerova', 'Brodku u Přerova', 'Brodek u Přerova')
# f = streets('squares', streetsDict, u'Category:Náměstí (Dřevohostice)', 'Dřevohostice', 'Dřevohosticích', 'Dřevohostice')
# f = streets('squares', streetsDict, u'Category:Náměstí Míru (Hustopeče nad Bečvou)', 'Hustopeče nad Bečvou', 'Hustopečích nad Bečvou', 'Hustopeče nad Bečvou')
# f = streets('squares', streetsDict, u'Category:Náměstí (Tovačov)', 'Tovačov', 'Tovačově', 'Tovačov')
# f = streets('squares', streetsDict, u'Category:Náměstí Josefa Haška', 'Chroustovice', 'Chroustovicích', 'Chroustovice')
# f = streets('squares', streetsDict, u'Category:Náměstí Plk. Josefa Koukala', 'Luže', 'Lužích', 'Luže')
# f = streets('squares', streetsDict, u'Category:Náměstí (Nasavrky)', 'Nasavrky', 'Nasavrkách', 'Nasavrky')
# f = streets('squares', streetsDict, u'Category:Náměstí Dr. Tošovského', 'Proseč', 'Proseči', 'Proseč')
# f = streets('squares', streetsDict, u'Category:Chittussiho náměstí', 'Ronov nad Doubravou', 'Ronově nad Doubravou', 'Ronov nad Doubravou')
#
# f = streets('squares', streetsDict, u'Category:Raisovo náměstí', 'Trhová Kamenice', 'Trhové Kamenici', 'Trhová Kamenice')
# f = streets('squares', streetsDict, u'Category:Náměstí Míru (Třemošnice)', 'Třemošnice', 'Třemošnici', 'Třemošnice')
# # f = streets('squares', streetsDict, u'Category:Laušmanovo náměstí', 'Žumberk', 'Žumberku', 'Žumberk')
#
# f = streets('squares', streetsDict, u'Category:Náměstí T. G. Masaryka (Dašice)', 'Dašice', 'Dašicích', 'Dašice')
# f = streets('squares', streetsDict, u'Category:Náměstí T. G. Masaryka (Holice)', 'Holice', 'Holicích', 'Holice')
# f = streets('squares', streetsDict, u'Category:Komenského náměstí (Horní Jelení)', 'Horní Jelení', 'Horním Jelení', 'Horní Jelení')
# f = streets('squares', streetsDict, u'Category:Náměstí Hrdinů (Moravany)', 'Moravany (okres Pardubice)', 'Moravanech', 'Moravany')
# f = streets('squares', streetsDict, u'Category:Husovo náměstí (Sezemice)', 'Sezemice (okres Pardubice)', 'Sezemicích', 'Sezemice')
# f = streets('squares', streetsDict, u'Category:Moravské náměstí (Březová nad Svitavou)', 'Březová nad Svitavou', 'Březové nad Svitavou', 'Březová nad Svitavou')
#
# # f = streets('squares', streetsDict, u'Category:Square in Městečko Trnávka', 'Žacléř', 'Žacléři', 'Žacléř')
# f = streets('squares', streetsDict, u'Category:Náměstí Míru (Svitavy)', 'Svitavy', 'Svitavách', 'Svitavy')
# f = streets('squares', streetsDict, u'Category:Náměstí Komenského (Brandýs nad Orlicí)', 'Brandýs nad Orlicí', 'Brandýse nad Orlicí', 'Brandýs nad Orlicí')
# f = streets('squares', streetsDict, u'Category:Náměstí Tyršovo (Choceň)', 'Choceň', 'Chocni', 'Choceň')
# f = streets('squares', streetsDict, u'Category:Náměstí 5. května (Jablonné nad Orlicí)', 'Jablonné nad Orlicí', 'Jablonném nad Orlicí', 'Jablonné nad Orlicí')
# f = streets('squares', streetsDict, u'Category:Náměstí (Bělá nad Radbuzou)', 'Bělá nad Radbuzou', 'Bělé nad Radbuzou', 'Bělá nad Radbuzou')
# f = streets('squares', streetsDict, u'Category:Náměstí (Nýrsko)', 'Nýrsko', 'Nýrsku', 'Nýrsko')
# f = streets('squares', streetsDict, u'Category:Náměstí (Plánice)', 'Plánice', 'Plánicích', 'Plánice')
# # f = streets('squares', streetsDict, u'Category:Square in Rabí', 'Rabí', 'Rabí', 'Rabí')
# # f = streets('squares', streetsDict, u'Category:Square in Strážov', 'Strážov', 'Strážově', 'Strážov')
# # f = streets('squares', streetsDict, u'Category:Square in Velhartice', 'Velhartice', 'Velharticích', 'Velhartice')
# f = streets('squares', streetsDict, u'Category:Klostermannovo náměstí', 'Železná Ruda', 'Železné Rudě', 'Železná Ruda')
# f = streets('squares', streetsDict, u'Category:Masarykovo náměstí (Starý Plzenec)', 'Starý Plzenec', 'Starém Plzenci', 'Starý Plzenec')
# # f = streets('squares', streetsDict, u'Category:Square in Manětín', 'Manětín', 'Manětíně', 'Manětín')
# # f = streets('squares', streetsDict, u'Category:Square in Úterý', 'Žacléř', 'Žacléři', 'Žacléř')
# f = streets('squares', streetsDict, u'Category:Masarykovo náměstí (Blovice)', 'Blovice', 'Blovicích', 'Blovice')
#
# # f = streets('squares', streetsDict, u'Category:Square in Kasejovice', 'Žacléř', 'Žacléři', 'Žacléř')
# f = streets('squares', streetsDict, u'Category:Masarykovo náměstí (Přeštice)', 'Přeštice', 'Přešticích', 'Přeštice')
# f = streets('squares', streetsDict, u'Category:Náměstí Svobody (Spálené Poříčí)', 'Spálené Poříčí', 'Spáleném Poříčí', 'Spálené Poříčí')
# f = streets('squares', streetsDict, u'Category:Náměstí Republiky (Kladruby)', 'Kladruby (okres Tachov)', 'Kladrubech', 'Kladruby')
# f = streets('squares', streetsDict, u'Category:Náměstí Svobody (Planá)', 'Planá', 'Plané', 'Planá')
# f = streets('squares', streetsDict, u'Category:Náměstí Republiky (Přimda)', 'Přimda', 'Přimdě', 'Přimda')
# f = streets('squares', streetsDict, u'Category:Náměstí 1. máje (Černošín)', 'Černošín', 'Černošíně', 'Černošín')
# f = streets('squares', streetsDict, u'Category:Náměstí (Chlum u Třeboně)', 'Chlum u Třeboně', 'Chlumu u Třeboně', 'Chlum u Třeboně')
# f = streets('squares', streetsDict, u'Category:Náměstí Komenského (Kunžak)', 'Kunžak', 'Kunžaku', 'Kunžak')
# f = streets('squares', streetsDict, u'Category:Prokopovo náměstí (Husinec)', 'Husinec (okres Prachatice)', 'Husinci', 'Husinec')
#
# f = streets('squares', streetsDict, u'Category:Náměstí Svobody (Vimperk)', 'Vimperk', 'Vimperku', 'Vimperk')
# f = streets('squares', streetsDict, u'Category:Náměstí Svobody (Vlachovo Březí)', 'Vlachovo Březí', 'Vlachově Březí', 'Vlachovo Březí')
# f = streets('squares', streetsDict, u'Category:Náměstí (Volary)', 'Volary', 'Volarech', 'Volary')
# f = streets('squares', streetsDict, u'Category:Náměstí Mikoláše Alše (Mirotice)', 'Mirotice', 'Miroticích', 'Mirotice')
# f = streets('squares', streetsDict, u'Category:Masarykovo náměstí (Mirovice)', 'Mirovice', 'Mirovicích', 'Mirovice')
# f = streets('squares', streetsDict, u'Category:Náměstí Míru (Bavorov)', 'Bavorov', 'Bavorově', 'Bavorov')
# f = streets('squares', streetsDict, u'Category:Náměstí Jana Kučery', 'Bělčice', 'Bělčicích', 'Bělčice')
# f = streets('squares', streetsDict, u'Category:Náměstí Svobody (Vodňany)', 'Vodňany', 'Vodňanech', 'Vodňany')
# f = streets('squares', streetsDict, u'Category:Náměstí T. G. Masaryka (Bechyně)', 'Bechyně', 'Bechyni', 'Bechyně')
# f = streets('squares', streetsDict, u'Category:Náměstí (Jistebnice)', 'Jistebnice', 'Jistebnici', 'Jistebnice')
# f = streets('squares', streetsDict, u'Category:Žižkovo náměstí (Borovany)', 'Borovany', 'Borovanech', 'Borovany')
# f = streets('squares', streetsDict, u'Category:Náměstí Republiky (Nové Hrady)', 'Nové Hrady', 'Nových Hradech', 'Nové Hrady')
# f = streets('squares', streetsDict, u'Category:Náměstí J. Gurreho', 'Římov (okres České Budějovice)', 'Římově', 'Římov')
# # f = streets('squares', streetsDict, u'Category:Square in Benešov nad Černou', 'Žacléř', 'Žacléři', 'Žacléř')
# # f = streets('squares', streetsDict, u'Category:Square in Chvalšiny', 'Žacléř', 'Žacléři', 'Žacléř')
# # f = streets('squares', streetsDict, u'Category:Square in Dolní Dvořiště', 'Žacléř', 'Žacléři', 'Žacléř')
# # f = streets('squares', streetsDict, u'Category:Náměstí (Frymburk)', 'Frymburk', 'Frymburku', 'Frymburk')
# # f = streets('squares', streetsDict, u'Category:Square in Horní Dvořiště', 'Žacléř', 'Žacléři', 'Žacléř')
# # f = streets('squares', streetsDict, u'Category:Square in Hořice na Šumavě', 'Žacléř', 'Žacléři', 'Žacléř')
# f = streets('squares', streetsDict, u'Category:Náměstí (Kaplice)', 'Kaplice', 'Kaplici', 'Kaplice')
# f = streets('squares', streetsDict, u'Category:Náměstí (Besednice)', 'Besednice', 'Besednici', 'Besednice')
# # f = streets('squares', streetsDict, u'Category:Square in Rožmberk nad Vltavou', 'Žacléř', 'Žacléři', 'Žacléř')
# f = streets('squares', streetsDict, u'Category:Náměstí J. V. Kamarýta', 'Velešín', 'Velešíně', 'Velešín')
# f = streets('squares', streetsDict, u'Category:Náměstí (Vyšší Brod)', 'Vyšší Brod', 'Vyšším Brodě', 'Vyšší Brod')
#
# f = streets('squares', streetsDict, u'Category:Náměstí Krále Jiřího (Kunštát)', 'Kunštát', 'Kunštátě', 'Kunštát')
# f = streets('squares', streetsDict, u'Category:Náměstí Míru (Olešnice)', 'Olešnice (okres Blansko)', 'Olešnici', 'Olešnice')
# f = streets('squares', streetsDict, u'Category:Náměstí Míru (Černá Hora)', 'Černá Hora (okres Blansko)', 'Černé Hoře', 'Černá Hora')
# f = streets('squares', streetsDict, u'Category:Náměstí Svobody (Blučina)', 'Blučina', 'Blučině', 'Blučina')
# f = streets('squares', streetsDict, u'Category:Náměstí 1. května (Kuřim)', 'Kuřim', 'Kuřimi', 'Kuřim')
# f = streets('squares', streetsDict, u'Category:Náměstí Svobody (Modřice)', 'Modřice', 'Modřicích', 'Modřice')
# f = streets('squares', streetsDict, u'Category:Náměstí Svobody (Pohořelice)', 'Pohořelice (okres Brno-venkov)', 'Pohořelicích', 'Pohořelice')
# f = streets('squares', streetsDict, u'Category:Náměstí Na Městečku (Veverská Bítýška)', 'Veverská Bítýška', 'Veverské Bítýšce', 'Veverská Bítýška')
# f = streets('squares', streetsDict, u'Category:Masarykovo náměstí (Šlapanice)', 'Šlapanice', 'Šlapanicích', 'Šlapanice')
# f = streets('squares', streetsDict, u'Category:Náměstí Míru (Židlochovice)', 'Židlochovice', 'Židlochovicích', 'Židlochovice')
# f = streets('squares', streetsDict, u'Category:Náměstí Svobody (Drnholec)', 'Drnholec', 'Drnholci', 'Drnholec')
# f = streets('squares', streetsDict, u'Category:Náměstí Míru (Klobouky u Brna)', 'Klobouky u Brna', 'Kloboukách u Brna', 'Klobouky u Brna')
# f = streets('squares', streetsDict, u'Category:Náměstí (Lanžhot)', 'Lanžhot', 'Lanžhotě', 'Lanžhot')
# f = streets('squares', streetsDict, u'Category:Zámecké náměstí (Lednice)', 'Lednice (okres Břeclav)', 'Lednici', 'Lednice')
# f = streets('squares', streetsDict, u'Category:Náměstí Svobody (Valtice)', 'Valtice', 'Valticích', 'Valtice')
# f = streets('squares', streetsDict, u'Category:Náměstí 9. května (Velké Pavlovice)', 'Velké Pavlovice', 'Velkých Pavlovicích', 'Velké Pavlovice')
# f = streets('squares', streetsDict, u'Category:Náměstí Svobody (Bzenec)', 'Bzenec', 'Bzenci', 'Bzenec')
# f = streets('squares', streetsDict, u'Category:Náměstí Svobody (Bučovice)', 'Bučovice', 'Bučovicích', 'Bučovice')
# f = streets('squares', streetsDict, u'Category:Marečkovo náměstí', 'Bučovice', 'Černčíně', 'Černčín')
# f = streets('squares', streetsDict, u'Category:Sušilovo náměstí (Rousínov)', 'Rousínov', 'Rousínově', 'Rousínov')
# f = streets('squares', streetsDict, u'Category:Náměstí svatého Wolfganga', 'Hnanice', 'Hnanicích', 'Hnanice')
# # f = streets('squares', streetsDict, u'Category:Square in Hostěradice', 'Žacléř', 'Žacléři', 'Žacléř')
# f = streets('squares', streetsDict, u'Category:Náměstí Míru (Hrušovany nad Jevišovkou)', 'Hrušovany nad Jevišovkou', 'Hrušovanech nad Jevišovkou', 'Hrušovany nad Jevišovkou')
# f = streets('squares', streetsDict, u'Category:Náměstí (Jaroslavice)', 'Jaroslavice', 'Jaroslavicích', 'Jaroslavice')
# f = streets('squares', streetsDict, u'Category:Náměstí Svobody (Miroslav)', 'Miroslav (okres Znojmo)', 'Miroslavi', 'Miroslav')
# # f = streets('squares', streetsDict, u'Category:Square in Olbramovice (Znojmo District)', 'Žacléř', 'Žacléři', 'Žacléř')
# f = streets('squares', streetsDict, u'Category:Náměstí (Vranov nad Dyjí)', 'Vranov nad Dyjí', 'Vranově nad Dyjí', 'Vranov nad Dyjí')
# f = streets('squares', streetsDict, u'Category:Náměstí T. G. Masaryka (Chotěboř)', 'Chotěboř', 'Chotěboři', 'Chotěboř')
#
# f = streets('squares', streetsDict, u'Category:Žižkovo náměstí (Habry)', 'Habry', 'Habrech', 'Habry')
# f = streets('squares', streetsDict, u'Category:Náměstí (Havlíčkova Borová)', 'Havlíčkova Borová', 'Havlíčkově Borové', 'Havlíčkova Borová')
# f = streets('squares', streetsDict, u'Category:Smetanovo náměstí (Havlíčkův Brod)', 'Havlíčkův Brod', 'Havlíčkově Brodě', 'Havlíčkův Brod')
# # f = streets('squares', streetsDict, u'Category:Square in Lipnice nad Sázavou', 'Žacléř', 'Žacléři', 'Žacléř')
# f = streets('squares', streetsDict, u'Category:Bechyňovo náměstí (Přibyslav)', 'Přibyslav', 'Přibyslavi', 'Přibyslav')
# f = streets('squares', streetsDict, u'Category:Náměstí Míru (Batelov)', 'Batelov', 'Batelově', 'Batelov')
# f = streets('squares', streetsDict, u'Category:Náměstí (Nová Říše)', 'Nová Říše', 'Nové Říší', 'Nová Říše')
# f = streets('squares', streetsDict, u'Category:Náměstí T. G. Masaryka (Horní Cerekev)', 'Horní Cerekev', 'Horní Cerekvi', 'Horní Cerekev')
# f = streets('squares', streetsDict, u'Category:Náměstí Svobody (Pacov)', 'Pacov', 'Pacově', 'Pacov')
# f = streets('squares', streetsDict, u'Category:Havlíčkovo náměstí (Žirovnice)', 'Žirovnice', 'Žirovnici', 'Žirovnice')
# # f = streets('squares', streetsDict, u'Category:Square in Dalešice (Třebíč District)', 'Žacléř', 'Žacléři', 'Žacléř')
# f = streets('squares', streetsDict, u'Category:Náměstí 8. května (Hrotovice)', 'Hrotovice', 'Hrotovicích', 'Hrotovice')
# f = streets('squares', streetsDict, u'Category:Náměstí Míru (Jaroměřice nad Rokytnou)', 'Jaroměřice nad Rokytnou', 'Jaroměřicích nad Rokytnou', 'Jaroměřice nad Rokytnou')
# # f = streets('squares', streetsDict, u'Category:Square in Mohelno', 'Žacléř', 'Žacléři', 'Žacléř')
# f = streets('squares', streetsDict, u'Category:Náměstí Míru (Moravské Budějovice)', 'Moravské Budějovice', 'Moravských Budějovicích', 'Moravské Budějovice')
# # f = streets('squares', streetsDict, u'Category:Square in Rouchovany', 'Žacléř', 'Žacléři', 'Žacléř')
# f = streets('squares', streetsDict, u'Category:Náměstí Míru (Želetava)', 'Želetava', 'Želetavě', 'Želetava')
# f = streets('squares', streetsDict, u'Category:Masarykovo náměstí (Bystřice nad Pernštejnem)', 'Bystřice nad Pernštejnem', 'Bystřici nad Pernštejnem', 'Bystřice nad Pernštejnem')
# f = streets('squares', streetsDict, u'Category:Náměstí Jana Karafiáta', 'Jimramov', 'Jimramově', 'Jimramov')
# f = streets('squares', streetsDict, u'Category:Benešovo náměstí (Křižanov)', 'Křižanov (okres Žďár nad Sázavou)', 'Křižanově', 'Křižanov')
# f = streets('squares', streetsDict, u'Category:Masarykovo náměstí (Velká Bíteš)', 'Velká Bíteš', 'Velké Bíteši', 'Velká Bíteš')
# f = streets('squares', streetsDict, u'Category:Náměstí (Velké Meziříčí)', 'Velké Meziříčí', 'Velkém Meziříčí', 'Velké Meziříčí')
# f = streets('squares', streetsDict, u'Category:Náměstí Svobody (Chropyně)', 'Chropyně', 'Chropyni', 'Chropyně')
# f = streets('squares', streetsDict, u'Category:Náměstí Míru (Hulín)', 'Hulín', 'Hulíně', 'Hulín')
# # f = streets('squares', streetsDict, u'Category:Square in Koryčany', 'Žacléř', 'Žacléři', 'Žacléř')
# f = streets('squares', streetsDict, u'Category:Náměstí (Morkovice-Slížany)', 'Morkovice-Slížany', 'Morkovicích-Slížanech', 'Morkovice-Slížany')
# f = streets('squares', streetsDict, u'Category:Náměstí Svobody (Buchlovice)', 'Buchlovice', 'Buchlovice', 'Buchlovice')
# f = streets('squares', streetsDict, u'Category:Náměstí Svobody (Kunovice)', 'Kunovice', 'Kunovicích', 'Kunovice')
#
# # f = streets('squares', streetsDict, u'Category:Square in Kelč', 'Žacléř', 'Žacléři', 'Žacléř')
# f = streets('squares', streetsDict, u'Category:Masarykovo náměstí (Rožnov pod Radhoštěm)', 'Rožnov pod Radhoštěm', 'Rožnově pod Radhoštěm', 'Rožnov pod Radhoštěm')
# f = streets('squares', streetsDict, u'Category:Náměstí (Valašské Meziříčí)', 'Valašské Meziříčí', 'Valašském Meziříčí', 'Valašské Meziříčí')
#
# f = streets('squares', streetsDict, u'Category:Náměstí Míru (Fryšták)', 'Fryšták', 'Fryštáku', 'Fryšták')
# f = streets('squares', streetsDict, u'Category:Masarykovo náměstí (Napajedla)', 'Napajedla', 'Napajedlech', 'Napajedla')
# f = streets('squares', streetsDict, u'Category:Náměstí 3. května (Otrokovice)', 'Otrokovice', 'Otrokovicích', 'Otrokovice')
# f = streets('squares', streetsDict, u'Category:Náměstí Svobody (Slušovice)', 'Slušovice', 'Slušovicích', 'Slušovice')
# f = streets('squares', streetsDict, u'Category:Náměstí Komenského (Tlumačov)', 'Tlumačov (okres Zlín)', 'Tlumačově', 'Tlumačov')
# f = streets('squares', streetsDict, u'Category:Masarykovo náměstí (Valašské Klobouky)', 'Valašské Klobouky', 'Valašských Kloboukách', 'Valašské Klobouky')
#
# f = streets('squares', streetsDict, u'Category:Náměstí Dr. E. Beneše (Jirkov)', 'Jirkov', 'Jirkově', 'Jirkov')
# f = streets('squares', streetsDict, u'Category:Náměstí Dr. Eduarda Beneše (Klášterec nad Ohří)', 'Klášterec nad Ohří', 'Klášterci nad Ohří', 'Klášterec nad Ohří')
# f = streets('squares', streetsDict, u'Category:Náměstí (Mašťov)', 'Mašťov', 'Mašťově', 'Mašťov')
# f = streets('squares', streetsDict, u'Category:Husovo náměstí (Bohušovice nad Ohří)', 'Bohušovice nad Ohří', 'Bohušovicích nad Ohří', 'Bohušovice nad Ohří')
# f = streets('squares', streetsDict, u'Category:Palackého náměstí (Brozany nad Ohří)', 'Brozany nad Ohří', 'Brozanech nad Ohří', 'Brozany nad Ohří')
# # f = streets('squares', streetsDict, u'Category:Square in Levín', 'Žacléř', 'Žacléři', 'Žacléř')
# f = streets('squares', streetsDict, u'Category:Václavské náměstí (Lovosice)', 'Lovosice', 'Lovosicích', 'Lovosice')
# f = streets('squares', streetsDict, u'Category:Náměstí ČSA (Terezín)', 'Terezín', 'Terezíně', 'Terezín')
# f = streets('squares', streetsDict, u'Category:Mírové náměstí (Úštěk)', 'Úštěk', 'Úštěku', 'Úštěk')
# f = streets('squares', streetsDict, u'Category:Tyršovo náměstí (Cítoliby)', 'Cítoliby', 'Cítolibech', 'Cítoliby')
# f = streets('squares', streetsDict, u'Category:Mírové náměstí (Postoloprty)', 'Postoloprty', 'Postoloprtech', 'Postoloprty')
# f = streets('squares', streetsDict, u'Category:Náměstí Míru (Vroutek)', 'Vroutek', 'Vroutku', 'Vroutek')
# f = streets('squares', streetsDict, u'Category:Náměstí 8. května (Meziboří)', 'Meziboří', 'Meziboří', 'Meziboří')
# f = streets('squares', streetsDict, u'Category:Náměstí Republiky (Lom)', 'Lom (okres Most)', 'Lom', 'Lom')
# f = streets('squares', streetsDict, u'Category:Mírové náměstí (Bílina)', 'Bílina (město)', 'Bílině', 'Bílina')
# f = streets('squares', streetsDict, u'Category:Klášterní náměstí (Osek)', 'Osek (okres Teplice)', 'Oseku', 'Osek')
# f = streets('squares', streetsDict, u'Category:Husovo náměstí (Chabařovice)', 'Chabařovice', 'Chabařovicích', 'Chabařovice')
# # f = streets('squares', streetsDict, u'Category:Square in Chlumec (Ústí nad Labem District)', 'Žacléř', 'Žacléři', 'Žacléř')
# f = streets('squares', streetsDict, u'Category:Náměstí Republiky (Blansko)', 'Blansko', 'Blansku', 'Blansko')
# f = streets('squares', streetsDict, u'Category:Náměstí Republiky (Bor)', 'Bor (okres Tachov)', 'Boru', 'Bor')
# f = streets('squares', streetsDict, u'Category:Náměstí Republiky (Brno)', 'Brno', 'Brně', 'Brno')
# f = streets('squares', streetsDict, u'Category:Náměstí Republiky (Doksy)', 'Doksy', 'Doksech', 'Doksy')
# f = streets('squares', streetsDict, u'Category:Náměstí Republiky (Duchcov)', 'Duchcov', 'Duchcově', 'Duchcov')
# f = streets('squares', streetsDict, u'Category:Náměstí Republiky (Dvůr Králové nad Labem)', 'Dvůr Králové nad Labem', 'Dvoře Králové nad Labem', 'Dvůr Králové nad Labem')
# f = streets('squares', streetsDict, u'Category:Náměstí Republiky (Havířov)', 'Havířov', 'Havířově', 'Havířov')
# f = streets('squares', streetsDict, u'Category:Náměstí Republiky (Horní Slavkov)', 'Horní Slavkov', 'Horním Slavkově', 'Horní Slavkov')
# f = streets('squares', streetsDict, u'Category:Náměstí Republiky (Horšovský Týn)', 'Horšovský Týn', 'Horšovském Týně', 'Horšovský Týn')
# f = streets('squares', streetsDict, u'Category:Náměstí Republiky (Jáchymov)', 'Jáchymov', 'Jáchymově', 'Jáchymov')
# f = streets('squares', streetsDict, u'Category:Náměstí Republiky (Karlovy Vary)', 'Karlovy Vary', 'Karlových Varech', 'Karlovy Vary')
# f = streets('squares', streetsDict, u'Category:Náměstí Republiky (Kladruby)', 'Kladruby (okres Tachov)', 'Kladrubech', 'Kladruby')
# f = streets('squares', streetsDict, u'Category:Náměstí Republiky (Kojetín)', 'Kojetín', 'Kojetíně', 'Kojetín')
# f = streets('squares', streetsDict, u'Category:Náměstí Republiky (Lom)', 'Lom (okres Most)', 'Lomu', 'Lom')
# f = streets('squares', streetsDict, u'Category:Náměstí Republiky (Mladá Boleslav)', 'Mladá Boleslav', 'Mladé Boleslavi', 'Mladá Boleslav')
# f = streets('squares', streetsDict, u'Category:Náměstí Republiky (Neratovice)', 'Neratovice', 'Neratovicích', 'Neratovice')
# f = streets('squares', streetsDict, u'Category:Náměstí Republiky (Nové Hrady)', 'Nové Hrady', 'Nových Hradech', 'Nové Hrady')
# f = streets('squares', streetsDict, u'Category:Náměstí Republiky (Nové Město nad Metují)', 'Nové Město nad Metují', 'Novém Městě nad Metují', 'Nové Město nad Metují')
# f = streets('squares', streetsDict, u'Category:Náměstí Republiky (Olomouc)', 'Olomouc', 'Olomouci', 'Olomouc')
# f = streets('squares', streetsDict, u'Category:Náměstí Republiky (Opava)', 'Opava', 'Opavě', 'Opava')
# f = streets('squares', streetsDict, u'Category:Náměstí Republiky (Oslavany)', 'Oslavany', 'Oslavanech', 'Oslavany')
# f = streets('squares', streetsDict, u'Category:Náměstí Republiky (Ostrava)', 'Ostrava', 'Ostravě', 'Ostrava')
# f = streets('squares', streetsDict, u'Category:Náměstí Republiky (Pardubice)', 'Pardubice', 'Pardubicích', 'Pardubice')
# f = streets('squares', streetsDict, u'Category:Náměstí Republiky (Plzeň)', 'Plzeň', 'Plzni', 'Plzeň')
# f = streets('squares', streetsDict, u'Category:Náměstí Republiky (Prague)', 'Praha', 'Praze', 'Praha')

#
# f = streets('squares', streetsDict, u'Category:Squares in Svratka', 'Svratka (okres Žďár nad Sázavou)', 'Svratce', 'Svratka')
# f = streets('squares', streetsDict, u'Category:Squares in Tábor', 'Tábor', 'Táboře', 'Tábor')
# f = streets('squares', streetsDict, u'Category:Squares in Vizovice', 'Vizovice', 'Vizovici', 'Vizovice')
# f = streets('squares', streetsDict, u'Category:Squares in Jilemnice', 'Jilemnice', 'Jilemnici', 'Jilemnice')
# f = streets('squares', streetsDict, u'Category:Squares in Chomutov', 'Chomutov', 'Chomutově', 'Chomutov')
# f = streets('squares', streetsDict, u'Category:Squares in Liberec', 'Liberec', 'Liberci', 'Liberec')
# f = streets('squares', streetsDict, u'Category:Squares in Brno City', 'Brno', 'Brně', 'Brno')
#
# f = streets('squares', streetsDict, u'Category:Squares in Český Dub', 'Český Dub', 'Českém Dubu', 'Český Dub')
# f = streets('squares', streetsDict, u'Category:Squares in Hrob', 'Hrob (okres Teplice)', 'Hrobě', 'Hrob')
# f = streets('squares', streetsDict, u'Category:Squares in Holešov', 'Holešov', 'Holešově', 'Holešov')
# f = streets('squares', streetsDict, u'Category:Squares in Počátky', 'Počátky', 'Počátkách', 'Počátky')
# f = streets('squares', streetsDict, u'Category:Squares in Moravský Krumlov', 'Moravský Krumlov', 'Moravském Krumlově', 'Moravský Krumlov')
# f = streets('squares', streetsDict, u'Category:Squares in Strážnice', 'Strážnice (okres Hodonín)', 'Strážnici', 'Strážnice')
# f = streets('squares', streetsDict, u'Category:Squares in Hustopeče', 'Hustopeče', 'Hustopečích', 'Hustopeče')
# f = streets('squares', streetsDict, u'Category:Squares in Týn nad Vltavou', 'Týn nad Vltavou', 'Týně nad Vltavou', 'Týn nad Vltavou')
# f = streets('squares', streetsDict, u'Category:Squares in Dačice', 'Dačice', 'Dačicích', 'Dačice')
# f = streets('squares', streetsDict, u'Category:Squares in Poběžovice', 'Poběžovice', 'Poběžovicích', 'Poběžovice')
# f = streets('squares', streetsDict, u'Category:Squares in Žamberk', 'Žamberk', 'Žamberku', 'Žamberk')
# f = streets('squares', streetsDict, u'Category:Squares in Horní Slavkov', 'Horní Slavkov', 'Horním Slavkově', 'Horní Slavkov')
# f = streets('squares', streetsDict, u'Category:Squares in Budišov nad Budišovkou', 'Budišov nad Budišovkou', 'Budišově nad Budišovkou', 'Budišov nad Budišovkou')
#
# f = streets('squares', streetsDict, u'Category:Squares in Masarykova čtvrť', 'Brno', 'Brně', 'Brno')
# #
# f = streets('squares', streetsDict, u'Category:Squares in Frýdek', 'Frýdek-Místek', 'Frýdku', 'Frýdek')
# f = streets('squares', streetsDict, u'Category:Squares in Místek', 'Frýdek-Místek', 'Místku', 'Místek')
# f = streets('squares', streetsDict, u'Category:Squares in Frýdek-Místek by name', 'Frýdek-Místek', 'Frýdku-Místku', 'Frýdek-Místek')
# #
# f = streets('squares', streetsDict, u'Category:Squares in Josefov', 'Jaroměř', 'Josefově (Jaroměř)', 'Josefov (Jaroměř)')
# f = streets('squares', streetsDict, u'Category:Squares in Kutná Hora by name', 'Kutná Hora', 'Kutné Hoře', 'Kutná Hora')
# f = streets('squares', streetsDict, u'Category:Squares in Ostrava by name', 'Ostrava', 'Ostravě', 'Ostrava')
# f = streets('squares', streetsDict, u'Category:Squares in Ostrava by district', 'Ostrava', 'Ostravě', 'Ostrava')
# f = streets('squares', streetsDict, u'Category:Squares in Moravská Ostrava a Přívoz', 'Ostrava', 'Ostravě', 'Ostrava')
#
# f = streets('squares', streetsDict, u'Category:Squares in Adamov', 'Adamov (okres Blansko)', 'Adamově', 'Adamov')
# f = streets('squares', streetsDict, u'Category:Squares in Bělá pod Bezdězem', 'Bělá pod Bezdězem', 'Bělé pod Bezdězem', 'Bělá pod Bezdězem')
# f = streets('squares', streetsDict, u'Category:Squares in Benešov nad Ploučnicí', 'Benešov nad Ploučnicí', 'Benešově nad Ploučnicí', 'Benešov nad Ploučnicí')
# f = streets('squares', streetsDict, u'Category:Squares in Beroun', 'Beroun', 'Berouně', 'Beroun')
# f = streets('squares', streetsDict, u'Category:Squares in Blansko', 'Blansko', 'Blansku', 'Blansko')
# f = streets('squares', streetsDict, u'Category:Squares in Blatná', 'Blatná', 'Blatné', 'Blatná')
# f = streets('squares', streetsDict, u'Category:Squares in Bohumín', 'Bohumín', 'Bohumíně', 'Bohumín')
# f = streets('squares', streetsDict, u'Category:Squares in Bor (Tachov District)', 'Bor (okres Tachov)', 'Boru', 'Bor')
# f = streets('squares', streetsDict, u'Category:Squares in Brandýs nad Labem-Stará Boleslav', 'Brandýs nad Labem-Stará Boleslav', 'Brandýse nad Labem-Staré Boleslavi', 'Brandýs nad Labem-Stará Boleslav')
# f = streets('squares', streetsDict, u'Category:Squares in Brno', 'Brno', 'Brně', 'Brno')
# f = streets('squares', streetsDict, u'Category:Squares in Broumov', 'Broumov', 'Broumově', 'Broumov')
# f = streets('squares', streetsDict, u'Category:Squares in Brtnice', 'Brtnice', 'Brtnici', 'Brtnice')
# f = streets('squares', streetsDict, u'Category:Squares in Bruntál', 'Bruntál', 'Bruntále', 'Bruntál')
# f = streets('squares', streetsDict, u'Category:Squares in Budyně nad Ohří', 'Budyně nad Ohří', 'Budyni nad Ohří', 'Budyně nad Ohří')
# f = streets('squares', streetsDict, u'Category:Squares in Bystřice pod Hostýnem', 'Bystřice pod Hostýnem', 'Bystřici pod Hostýnem', 'Bystřice pod Hostýnem')
# f = streets('squares', streetsDict, u'Category:Squares in Břeclav', 'Břeclav', 'Břeclavi', 'Břeclav')
#
# f = streets('squares', streetsDict, u'Category:Squares in Cheb', 'Cheb', 'Chebu', 'Cheb')
# f = streets('squares', streetsDict, u'Category:Squares in Chrast', 'Chrast', 'Chrasti', 'Chrast')
# f = streets('squares', streetsDict, u'Category:Squares in Chrudim', 'Chrudim', 'Chrudimi', 'Chrudim')
# f = streets('squares', streetsDict, u'Category:Squares in Cvikov', 'Cvikov', 'Cvikově', 'Cvikov')
#
# f = streets('squares', streetsDict, u'Category:Squares in Divišov', 'Divišov', 'Divišově', 'Divišov')
# f = streets('squares', streetsDict, u'Category:Squares in Dobruška', 'Dobruška', 'Dobrušce', 'Dobruška')
# f = streets('squares', streetsDict, u'Category:Squares in Doksy', 'Doksy', 'Doksech', 'Doksy')
# f = streets('squares', streetsDict, u'Category:Squares in Dolní Kounice', 'Dolní Kounice', 'Dolních Kounicích', 'Dolní Kounice')
# f = streets('squares', streetsDict, u'Category:Squares in Domažlice', 'Domažlice', 'Domažlicích', 'Domažlice')
# f = streets('squares', streetsDict, u'Category:Squares in Duchcov', 'Duchcov', 'Duchcově', 'Duchcov')
# f = streets('squares', streetsDict, u'Category:Squares in Dvůr Králové nad Labem', 'Dvůr Králové nad Labem', 'Dvoře Králové nad Labem', 'Dvůr Králové nad Labem')
# f = streets('squares', streetsDict, u'Category:Squares in Děčín', 'Děčín', 'Děčíně', 'Děčín')
#
# f = streets('squares', streetsDict, u'Category:Squares in Frýdek-Místek', 'Frýdek-Místek', 'Frýdku-Místku', 'Frýdek-Místek')
# f = streets('squares', streetsDict, u'Category:Squares in Frýdlant', 'Frýdlant', 'Frýdlantě', 'Frýdlant')
#
# f = streets('squares', streetsDict, u'Category:Squares in Havlíčkův Brod', 'Havlíčkův Brod', 'Havlíčkově Brodě', 'Havlíčkův Brod')
# f = streets('squares', streetsDict, u'Category:Squares in Havířov', 'Havířov', 'Havířově', 'Havířov')
# f = streets('squares', streetsDict, u'Category:Squares in Heřmanův Městec', 'Heřmanův Městec', 'Heřmanově Městci', 'Heřmanův Městec')
# f = streets('squares', streetsDict, u'Category:Squares in Hlinsko', 'Hlinsko', 'Hlinsku', 'Hlinsko')
# f = streets('squares', streetsDict, u'Category:Squares in Hodonín', 'Hodonín', 'Hodoníně', 'Hodonín')
# f = streets('squares', streetsDict, u'Category:Squares in Horažďovice', 'Horažďovice', 'Horažďovicích', 'Horažďovice')
# f = streets('squares', streetsDict, u'Category:Squares in Horšovský Týn', 'Horšovský Týn', 'Horšovském Týně', 'Horšovský Týn')
# f = streets('squares', streetsDict, u'Category:Squares in Hostinné', 'Hostinné', 'Hostinném', 'Hostinné')
# f = streets('squares', streetsDict, u'Category:Squares in Hořice', 'Hořice', 'Hořicích', 'Hořice')
# f = streets('squares', streetsDict, u'Category:Squares in Hradec Králové', 'Hradec Králové', 'Hradci Králové', 'Hradec Králové')
# f = streets('squares', streetsDict, u'Category:Squares in Hranice', 'Hranice (okres Přerov)', 'Hranicích', 'Hranice')
# f = streets('squares', streetsDict, u'Category:Squares in Hronov', 'Hronov', 'Hronově', 'Hronov')
# f = streets('squares', streetsDict, u'Category:Squares in Humpolec', 'Humpolec', 'Humpolci', 'Humpolec')
#
# f = streets('squares', streetsDict, u'Category:Squares in Ivančice', 'Ivančice', 'Ivančicích', 'Ivančice')
#
# f = streets('squares', streetsDict, u'Category:Squares in Jablonec nad Nisou', 'Jablonec nad Nisou', 'Jablonci nad Nisou', 'Jablonec nad Nisou')
# f = streets('squares', streetsDict, u'Category:Squares in Jablonné v Podještědí', 'Jablonné v Podještědí', 'Jablonném v Podještědí', 'Jablonné v Podještědí')
# f = streets('squares', streetsDict, u'Category:Squares in Jablunkov', 'Jablunkov', 'Jablunkově', 'Jablunkov')
# f = streets('squares', streetsDict, u'Category:Squares in Jaroměř', 'Jaroměř', 'Jaroměři', 'Jaroměř')
# f = streets('squares', streetsDict, u'Category:Squares in Jemnice', 'Jemnice', 'Jemnici', 'Jemnice')
# f = streets('squares', streetsDict, u'Category:Squares in Jeseník', 'Jeseník', 'Jeseníku', 'Jeseník')
# f = streets('squares', streetsDict, u'Category:Squares in Jevíčko', 'Jevíčko', 'Jevíčku', 'Jevíčko')
# f = streets('squares', streetsDict, u'Category:Squares in Jičín', 'Jičín', 'Jičíně', 'Jičín')
# f = streets('squares', streetsDict, u'Category:Squares in Jihlava', 'Jihlava', 'Jihlavě', 'Jihlava')
#
# f = streets('squares', streetsDict, u'Category:Squares in Votice', 'Votice', 'Voticích', 'Votice')
# f = streets('squares', streetsDict, u'Category:Squares in Kladno', 'Kladno', 'Kladně', 'Kladno')
# f = streets('squares', streetsDict, u'Category:Squares in Kolín', 'Kolín', 'Kolíně', 'Kolín')
# f = streets('squares', streetsDict, u'Category:Squares in Dobříš', 'Dobříš', 'Dobříši', 'Dobříš')
#
# f = streets('squares', streetsDict, u'Category:Squares in Jindřichův Hradec', 'Jindřichův Hradec', 'Jindřichově Hradci', 'Jindřichův Hradec')
#
# f = streets('squares', streetsDict, u'Category:Squares in Kadaň', 'Kadaň', 'Kadani', 'Kadaň')
# f = streets('squares', streetsDict, u'Category:Squares in Kamenice nad Lipou', 'Kamenice nad Lipou', 'Kamenici nad Lipou', 'Kamenice nad Lipou')
# f = streets('squares', streetsDict, u'Category:Squares in Kamenický Šenov', 'Kamenický Šenov', 'Kamenickém Šenově', 'Kamenický Šenov')
# f = streets('squares', streetsDict, u'Category:Squares in Kardašova Řečice', 'Kardašova Řečice', 'Kardašově Řečici', 'Kardašova Řečice')
# f = streets('squares', streetsDict, u'Category:Squares in Karlovy Vary', 'Karlovy Vary', 'Karlových Varech', 'Karlovy Vary')
# f = streets('squares', streetsDict, u'Category:Squares in Karviná', 'Karviná', 'Karviné', 'Karviná')
# f = streets('squares', streetsDict, u'Category:Squares in Kašperské Hory', 'Kašperské Hory', 'Kašperských Horách', 'Kašperské Hory')
# f = streets('squares', streetsDict, u'Category:Squares in Klatovy', 'Klatovy', 'Klatovech', 'Klatovy')
# f = streets('squares', streetsDict, u'Category:Squares in Kojetín', 'Kojetín', 'Kojetíně', 'Kojetín')
# f = streets('squares', streetsDict, u'Category:Squares in Kostelec nad Orlicí', 'Kostelec nad Orlicí', 'Kostelci nad Orlicí', 'Kostelec nad Orlicí')
# f = streets('squares', streetsDict, u'Category:Squares in Kostelec nad Černými lesy', 'Kostelec nad Černými lesy', 'Kostelci nad Černými lesy', 'Kostelec nad Černými lesy')
# f = streets('squares', streetsDict, u'Category:Squares in Kouřim', 'Kouřim', 'Kouřimi', 'Kouřim')
# f = streets('squares', streetsDict, u'Category:Squares in Králíky', 'Králíky', 'Králíkách', 'Králíky')
# f = streets('squares', streetsDict, u'Category:Squares in Kralovice', 'Kralovice', 'Kralovicích', 'Kralovice')
# f = streets('squares', streetsDict, u'Category:Squares in Kralupy nad Vltavou', 'Kralupy nad Vltavou', 'Kralupech nad Vltavou', 'Kralupy nad Vltavou')
# f = streets('squares', streetsDict, u'Category:Squares in Krásná Lípa', 'Krásná Lípa', 'Krásné Lípě', 'Krásná Lípa')
# f = streets('squares', streetsDict, u'Category:Squares in Krnov', 'Krnov', 'Krnově', 'Krnov')
# f = streets('squares', streetsDict, u'Category:Squares in Kroměříž', 'Kroměříž', 'Kroměříži', 'Kroměříž')
# f = streets('squares', streetsDict, u'Category:Squares in Krupka', 'Krupka', 'Krupce', 'Krupka')
# f = streets('squares', streetsDict, u'Category:Squares in Krásno', 'Krásno', 'Krásně', 'Krásno')
# f = streets('squares', streetsDict, u'Category:Squares in Kutná Hora', 'Kutná Hora', 'Kutné Hoře', 'Kutná Hora')
# f = streets('squares', streetsDict, u'Category:Squares in Kyjov', 'Kyjov', 'Kyjově', 'Kyjov')
#
# f = streets('squares', streetsDict, u'Category:Squares in Lanškroun', 'Lanškroun', 'Lanškrouně', 'Lanškroun')
# f = streets('squares', streetsDict, u'Category:Squares in Lázně Bohdaneč', 'Lázně Bohdaneč', 'Lázních Bohdaneč', 'Lázně Bohdaneč')
# f = streets('squares', streetsDict, u'Category:Squares in Ledeč nad Sázavou', 'Ledeč nad Sázavou', 'Ledči nad Sázavou', 'Ledeč nad Sázavou')
# f = streets('squares', streetsDict, u'Category:Squares in Letohrad', 'Letohrad', 'Letohradě', 'Letohrad')
# f = streets('squares', streetsDict, u'Category:Squares in Libochovice', 'Libochovice', 'Libochovicích', 'Libochovice')
# f = streets('squares', streetsDict, u'Category:Squares in Libčice nad Vltavou', 'Libčice nad Vltavou', 'Libčicích nad Vltavou', 'Libčice nad Vltavou')
# f = streets('squares', streetsDict, u'Category:Squares in Litomyšl', 'Litomyšl', 'Litomyšli', 'Litomyšl')
# f = streets('squares', streetsDict, u'Category:Squares in Litoměřice', 'Litoměřice', 'Litoměřicích', 'Litoměřice')
# f = streets('squares', streetsDict, u'Category:Squares in Litovel', 'Litovel', 'Litovli', 'Litovel')
# f = streets('squares', streetsDict, u'Category:Squares in Litvínov', 'Litvínov', 'Litvínově', 'Litvínov')
# f = streets('squares', streetsDict, u'Category:Squares in Loket', 'Loket (okres Sokolov)', 'Lokti', 'Loket')
# f = streets('squares', streetsDict, u'Category:Squares in Lomnice (Brno-Country District)', 'Lomnice (okres Brno-venkov)', 'Lomnici', 'Lomnice')
# f = streets('squares', streetsDict, u'Category:Squares in Lomnice nad Lužnicí', 'Lomnice nad Lužnicí', 'Lomnici nad Lužnicí', 'Lomnice nad Lužnicí')
# f = streets('squares', streetsDict, u'Category:Squares in Lomnice nad Popelkou', 'Lomnice nad Popelkou', 'Lomnici nad Popelkou', 'Lomnice nad Popelkou')
# f = streets('squares', streetsDict, u'Category:Squares in Louny', 'Louny', 'Lounech', 'Louny')
# f = streets('squares', streetsDict, u'Category:Squares in Luhačovice', 'Luhačovice', 'Luhačovicích', 'Luhačovice')
# f = streets('squares', streetsDict, u'Category:Squares in Lysice', 'Lysice', 'Lysicích', 'Lysice')
# f = streets('squares', streetsDict, u'Category:Squares in Lysá nad Labem', 'Lysá nad Labem', 'Lysé nad Labem', 'Lysá nad Labem')
#
# f = streets('squares', streetsDict, u'Category:Squares in Mariánské Lázně', 'Mariánské Lázně', 'Mariánských Lázních', 'Mariánské Lázně')
# f = streets('squares', streetsDict, u'Category:Squares in Mělník', 'Mělník', 'Mělníku', 'Mělník')
# f = streets('squares', streetsDict, u'Category:Squares in Mikulov', 'Mikulov', 'Mikulově', 'Mikulov')
# f = streets('squares', streetsDict, u'Category:Squares in Milevsko', 'Milevsko', 'Milevsku', 'Milevsko')
# f = streets('squares', streetsDict, u'Category:Squares in Mimoň', 'Mimoň', 'Mimoni', 'Mimoň')
# f = streets('squares', streetsDict, u'Category:Squares in Mladá Boleslav', 'Mladá Boleslav', 'Mladé Boleslavi', 'Mladá Boleslav')
# f = streets('squares', streetsDict, u'Category:Squares in Mníšek pod Brdy', 'Mníšek pod Brdy', 'Mníšku pod Brdy', 'Mníšek pod Brdy')
# f = streets('squares', streetsDict, u'Category:Squares in Mohelnice', 'Mohelnice', 'Mohelnici', 'Mohelnice')
# f = streets('squares', streetsDict, u'Category:Squares in Moravská Třebová', 'Moravská Třebová', 'Moravské Třebové', 'Moravská Třebová')
# f = streets('squares', streetsDict, u'Category:Squares in Most', 'Most (město)', 'Mostě', 'Most')
# f = streets('squares', streetsDict, u'Category:Squares in Město Touškov', 'Město Touškov', 'Městě Touškov', 'Město Touškov')
#
# f = streets('squares', streetsDict, u'Category:Squares in Nechanice', 'Nechanice', 'Nechanicích', 'Nechanice')
# f = streets('squares', streetsDict, u'Category:Squares in Nepomuk', 'Nepomuk', 'Nepomuku', 'Nepomuk')
# f = streets('squares', streetsDict, u'Category:Squares in Neveklov', 'Neveklov', 'Neveklově', 'Neveklov')
# f = streets('squares', streetsDict, u'Category:Squares in Nová Bystřice', 'Nová Bystřice', 'Nové Bystřici', 'Nová Bystřice')
# f = streets('squares', streetsDict, u'Category:Squares in Nové Město na Moravě', 'Nové Město na Moravě', 'Novém Městě na Moravě', 'Nové Město na Moravě')
# f = streets('squares', streetsDict, u'Category:Squares in Nové Město nad Metují', 'Nové Město nad Metují', 'Novém Městě nad Metují', 'Nové Město nad Metují')
# f = streets('squares', streetsDict, u'Category:Squares in Nový Bor', 'Nový Bor', 'Novém Boru', 'Nový Bor')
# f = streets('squares', streetsDict, u'Category:Squares in Nový Jičín', 'Nový Jičín', 'Novém Jičíně', 'Nový Jičín')
# f = streets('squares', streetsDict, u'Category:Squares in Nymburk', 'Nymburk', 'Nymburku', 'Nymburk')
# f = streets('squares', streetsDict, u'Category:Squares in Náchod', 'Náchod', 'Náchodě', 'Náchod')
# f = streets('squares', streetsDict, u'Category:Squares in Náměšť nad Oslavou', 'Náměšť nad Oslavou', 'Náměšti nad Oslavou', 'Náměšť nad Oslavou')
# f = streets('squares', streetsDict, u'Category:Squares in Němčice nad Hanou', 'Němčice nad Hanou', 'Němčicích nad Hanou', 'Němčice nad Hanou')
#
# f = streets('squares', streetsDict, u'Category:Squares in Olomouc', 'Olomouc', 'Olomouci', 'Olomouc')
# f = streets('squares', streetsDict, u'Category:Squares in Opava', 'Opava', 'Opavě', 'Opava')
# f = streets('squares', streetsDict, u'Category:Squares in Opočno', 'Opočno', 'Opočně', 'Opočno')
# f = streets('squares', streetsDict, u'Category:Squares in Oslavany', 'Oslavany', 'Oslavanech', 'Oslavany')
# f = streets('squares', streetsDict, u'Category:Squares in Ostrava', 'Ostrava', 'Ostravě', 'Ostrava')
# f = streets('squares', streetsDict, u'Category:Squares in Ostrov', 'Ostrov (okres Karlovy Vary)', 'Ostrově', 'Ostrov')
#
# f = streets('squares', streetsDict, u'Category:Squares in Pardubice', 'Pardubice', 'Pardubicích', 'Pardubice')
# f = streets('squares', streetsDict, u'Category:Squares in Pelhřimov', 'Pelhřimov', 'Pelhřimově', 'Pelhřimov')
# f = streets('squares', streetsDict, u'Category:Squares in Peruc', 'Peruc', 'Peruci', 'Peruc')
# f = streets('squares', streetsDict, u'Category:Squares in Pečky', 'Pečky', 'Pečkách', 'Pečky')
# f = streets('squares', streetsDict, u'Category:Squares in Písek', 'Písek (město)', 'Písku', 'Písek')
# f = streets('squares', streetsDict, u'Category:Squares in Plumlov', 'Plumlov', 'Plumlově', 'Plumlov')
# f = streets('squares', streetsDict, u'Category:Squares in Plzeň', 'Plzeň', 'Plzni', 'Plzeň')
# f = streets('squares', streetsDict, u'Category:Squares in Poděbrady', 'Poděbrady', 'Poděbradech', 'Poděbrady')
# f = streets('squares', streetsDict, u'Category:Squares in Police nad Metují', 'Police nad Metují', 'Polici nad Metují', 'Police nad Metují')
# f = streets('squares', streetsDict, u'Category:Squares in Polička', 'Polička', 'Poličce', 'Polička')
# f = streets('squares', streetsDict, u'Category:Squares in Polná', 'Polná', 'Polné', 'Polná')
# f = streets('squares', streetsDict, u'Category:Squares in Prachatice', 'Prachatice', 'Prachaticích', 'Prachatice')
# f = streets('squares', streetsDict, u'Category:Squares in Prague by name', 'Praha', 'Praze', 'Praha')
# f = streets('squares', streetsDict, u'Category:Urban squares in Prague by name', 'Praha', 'Praze', 'Praha')
# f = streets('squares', streetsDict, u'Category:Squares in Prostějov', 'Prostějov', 'Prostějově', 'Prostějov')
# f = streets('squares', streetsDict, u'Category:Squares in Protivín', 'Protivín', 'Protivíně', 'Protivín')
# f = streets('squares', streetsDict, u'Category:Squares in Přelouč', 'Přelouč', 'Přelouči', 'Přelouč')
# f = streets('squares', streetsDict, u'Category:Squares in Přerov', 'Přerov', 'Přerově', 'Přerov')
# f = streets('squares', streetsDict, u'Category:Squares in Příbram', 'Příbram', 'Příbrami', 'Příbram')
#
# f = streets('squares', streetsDict, u'Category:Squares in Rakovník', 'Rakovník', 'Rakovníku', 'Rakovník')
# f = streets('squares', streetsDict, u'Category:Squares in Rokycany', 'Rokycany', 'Rokycanech', 'Rokycany')
# f = streets('squares', streetsDict, u'Category:Squares in Rokytnice v Orlických horách', 'Rokytnice v Orlických horách', 'Rokytnici v Orlických horách', 'Rokytnice v Orlických horách')
# f = streets('squares', streetsDict, u'Category:Squares in Rosice (Brno-Country District)', 'Rosice (okres Brno-venkov)', 'Rosicích', 'Rosice')
# f = streets('squares', streetsDict, u'Category:Squares in Roudnice nad Labem', 'Roudnice nad Labem', 'Roudnici nad Labem', 'Roudnice nad Labem')
# f = streets('squares', streetsDict, u'Category:Squares in Roztoky (Prague-West District)', 'Roztoky (okres Praha-západ)', 'Roztokách', 'Roztoky')
# f = streets('squares', streetsDict, u'Category:Squares in Rudná', 'Rudná (okres Praha-západ)', 'Rudné', 'Rudná')
# f = streets('squares', streetsDict, u'Category:Squares in Rumburk', 'Rumburk', 'Rumburku', 'Rumburk')
# f = streets('squares', streetsDict, u'Category:Squares in Rychnov nad Kněžnou', 'Rychnov nad Kněžnou', 'Rychnově nad Kněžnou', 'Rychnov nad Kněžnou')
# f = streets('squares', streetsDict, u'Category:Squares in Rýmařov', 'Rýmařov', 'Rýmařově', 'Rýmařov')
#
# f = streets('squares', streetsDict, u'Category:Squares in Sedlec-Prčice', 'Sedlec-Prčice', 'Sedleci-Prčicích', 'Sedlec-Prčice')
# f = streets('squares', streetsDict, u'Category:Squares in Sedlčany', 'Sedlčany', 'Sedlčanech', 'Sedlčany')
# f = streets('squares', streetsDict, u'Category:Squares in Semily', 'Semily', 'Semilech', 'Semily')
# f = streets('squares', streetsDict, u'Category:Squares in Sezimovo Ústí', 'Sezimovo Ústí', 'Sezimově Ústí', 'Sezimovo Ústí')
# f = streets('squares', streetsDict, u'Category:Squares in Skuteč', 'Skuteč', 'Skutči', 'Skuteč')
# f = streets('squares', streetsDict, u'Category:Squares in Slaný', 'Slaný', 'Slaném', 'Slaný')
# f = streets('squares', streetsDict, u'Category:Squares in Slavkov u Brna', 'Slavkov u Brna', 'Slavkově u Brna', 'Slavkov u Brna')
# f = streets('squares', streetsDict, u'Category:Squares in Slavonice', 'Slavonice', 'Slavonicích', 'Slavonice')
# f = streets('squares', streetsDict, u'Category:Squares in Smržovka', 'Smržovka', 'Smržovce', 'Smržovka')
# f = streets('squares', streetsDict, u'Category:Squares in Sobotka', 'Sobotka', 'Sobotce', 'Sobotka')
# f = streets('squares', streetsDict, u'Category:Squares in Soběslav', 'Soběslav', 'Soběslavi', 'Soběslav')
# f = streets('squares', streetsDict, u'Category:Squares in Sokolov', 'Sokolov', 'Sokolově', 'Sokolov')
# f = streets('squares', streetsDict, u'Category:Squares in Staré Město (Uherské Hradiště District)', 'Staré Město (okres Uherské Hradiště)', 'Starém Městě', 'Staré Město')
# f = streets('squares', streetsDict, u'Category:Squares in Stochov', 'Stochov', 'Stochově', 'Stochov')
# f = streets('squares', streetsDict, u'Category:Squares in Strakonice', 'Strakonice', 'Strakonicích', 'Strakonice')
# f = streets('squares', streetsDict, u'Category:Squares in Stráž nad Nežárkou', 'Stráž nad Nežárkou', 'Stráži nad Nežárkou', 'Stráž nad Nežárkou')
# f = streets('squares', streetsDict, u'Category:Squares in Stříbro', 'Stříbro (okres Tachov)', 'Stříbře', 'Stříbro')
# f = streets('squares', streetsDict, u'Category:Squares in Sušice', 'Sušice', 'Sušici', 'Sušice')
# f = streets('squares', streetsDict, u'Category:Squares in Svitavy', 'Svitavy', 'Svitavách', 'Svitavy')
#
#
#
# f = streets('squares', streetsDict, u'Category:Squares in Tachov', 'Tachov', 'Tachově', 'Tachov')
# f = streets('squares', streetsDict, u'Category:Squares in Telč', 'Telč', 'Telči', 'Telč')
# f = streets('squares', streetsDict, u'Category:Squares in Teplice', 'Teplice', 'Teplicích', 'Teplice')
# f = streets('squares', streetsDict, u'Category:Squares in Tišnov', 'Tišnov', 'Tišnově', 'Tišnov')
# f = streets('squares', streetsDict, u'Category:Squares in Toužim', 'Toužim', 'Toužimi', 'Toužim')
# f = streets('squares', streetsDict, u'Category:Squares in Trhové Sviny', 'Trhové Sviny', 'Trhových Svinech', 'Trhové Sviny')
# f = streets('squares', streetsDict, u'Category:Squares in Trutnov', 'Trutnov', 'Trutnově', 'Trutnov')
# f = streets('squares', streetsDict, u'Category:Squares in Turnov', 'Turnov', 'Turnově', 'Turnov')
# f = streets('squares', streetsDict, u'Category:Squares in Třebenice', 'Třebenice (okres Litoměřice)', 'Třebenicích', 'Třebenice')
# f = streets('squares', streetsDict, u'Category:Squares in Třeboň', 'Třeboň', 'Třeboni', 'Třeboň')
# f = streets('squares', streetsDict, u'Category:Squares in Třebíč', 'Třebíč', 'Třebíči', 'Třebíč')
# f = streets('squares', streetsDict, u'Category:Squares in Třešť', 'Třešť', 'Třešti', 'Třešť')
# f = streets('squares', streetsDict, u'Category:Squares in Třinec', 'Třinec', 'Třinci', 'Třinec')
#
# f = streets('squares', streetsDict, u'Category:Squares in Uherské Hradiště', 'Uherské Hradiště', 'Uherském Hradišti', 'Uherské Hradiště')
# f = streets('squares', streetsDict, u'Category:Squares in Uherský Brod', 'Uherský Brod', 'Uherském Brodě', 'Uherský Brod')
# f = streets('squares', streetsDict, u'Category:Squares in Uničov', 'Uničov', 'Uničově', 'Uničov')
#
# f = streets('squares', streetsDict, u'Category:Squares in Vamberk', 'Vamberk', 'Vamberku', 'Vamberk')
# f = streets('squares', streetsDict, u'Category:Squares in Veselí nad Lužnicí', 'Veselí nad Lužnicí', 'Veselí nad Lužnicí', 'Veselí nad Lužnicí')
# f = streets('squares', streetsDict, u'Category:Squares in Veselí nad Moravou', 'Veselí nad Moravou', 'Veselí nad Moravou', 'Veselí nad Moravou')
#
# f = streets('squares', streetsDict, u'Category:Squares in Vlašim', 'Vlašim', 'Vlašimi', 'Vlašim')
# f = streets('squares', streetsDict, u'Category:Squares in Volyně', 'Volyně', 'Volyni', 'Volyně')
# f = streets('squares', streetsDict, u'Category:Squares in Vrchlabí', 'Vrchlabí', 'Vrchlabí', 'Vrchlabí')
# f = streets('squares', streetsDict, u'Category:Squares in Vsetín', 'Vsetín', 'Vsetíně', 'Vsetín')
# f = streets('squares', streetsDict, u'Category:Squares in Vysoké Mýto', 'Vysoké Mýto', 'Vysokém Mýtě', 'Vysoké Mýto')
# f = streets('squares', streetsDict, u'Category:Squares in Vyškov', 'Vyškov', 'Vyškově', 'Vyškov')
#
# f = streets('squares', streetsDict, u'Category:Squares in Zábřeh', 'Zábřeh', 'Zábřehu', 'Zábřeh')
# f = streets('squares', streetsDict, u'Category:Squares in Zákupy', 'Zákupy', 'Zákupech', 'Zákupy')
# f = streets('squares', streetsDict, u'Category:Squares in Zlín', 'Zlín', 'Zlíně', 'Zlín')
# f = streets('squares', streetsDict, u'Category:Squares in Znojmo', 'Znojmo', 'Znojmě', 'Znojmo')
# f = streets('squares', streetsDict, u'Category:Squares in Zruč nad Sázavou', 'Zruč nad Sázavou', 'Zruči nad Sázavou', 'Zruč nad Sázavou')
#
# f = streets('squares', streetsDict, u'Category:Squares in Ústí nad Labem', 'Ústí nad Labem', 'Ústí nad Labem', 'Ústí nad Labem')
# f = streets('squares', streetsDict, u'Category:Squares in Ústí nad Orlicí', 'Ústí nad Orlicí', 'Ústí nad Orlicí', 'Ústí nad Orlicí')
#
# f = streets('squares', streetsDict, u'Category:Squares in Česká Kamenice', 'Česká Kamenice', 'České Kamenici', 'Česká Kamenice')
# f = streets('squares', streetsDict, u'Category:Squares in Česká Lípa', 'Česká Lípa', 'České Lípě', 'Česká Lípa')
# f = streets('squares', streetsDict, u'Category:Squares in Česká Třebová', 'Česká Třebová', 'České Třebové', 'Česká Třebová')
# f = streets('squares', streetsDict, u'Category:Squares in České Budějovice', 'České Budějovice', 'Českých Budějovicích', 'České Budějovice')
# f = streets('squares', streetsDict, u'Category:Squares in Český Brod', 'Český Brod', 'Českém Brodě', 'Český Brod')
# f = streets('squares', streetsDict, u'Category:Squares in Český Krumlov', 'Český Krumlov', 'Českém Krumlově', 'Český Krumlov')
# f = streets('squares', streetsDict, u'Category:Squares in Český Těšín', 'Český Těšín', 'Českém Těšíně', 'Český Těšín')
# f = streets('squares', streetsDict, u'Category:Squares in Čáslav', 'Čáslav', 'Čáslavi', 'Čáslav')
#
# f = streets('squares', streetsDict, u'Category:Squares in Řevnice', 'Řevnice', 'Řevnicích', 'Řevnice')
# f = streets('squares', streetsDict, u'Category:Squares in Říčany (Prague-East District)', 'Říčany', 'Říčanech', 'Říčany')
#
# f = streets('squares', streetsDict, u'Category:Squares in Šluknov', 'Šluknov', 'Šluknově', 'Šluknov')
# f = streets('squares', streetsDict, u'Category:Squares in Šternberk', 'Šternberk', 'Šternberku', 'Šternberk')
# f = streets('squares', streetsDict, u'Category:Squares in Šumperk', 'Šumperk', 'Šumperku', 'Šumperk')
#
# f = streets('squares', streetsDict, u'Category:Squares in Žatec', 'Žatec', 'Žatci', 'Žatec')
# f = streets('squares', streetsDict, u'Category:Squares in Železný Brod', 'Železný Brod', 'Železném Brodě', 'Železný Brod')
# f = streets('squares', streetsDict, u'Category:Squares in Žlutice', 'Žlutice', 'Žluticích', 'Žlutice')
# f = streets('squares', streetsDict, u'Category:Squares in Žulová', 'Žulová', 'Žulové', 'Žulová')
# f = streets('squares', streetsDict, u'Category:Squares in Žďár nad Sázavou', 'Žďár nad Sázavou', 'Žďáru nad Sázavou', 'Žďár nad Sázavou')
#
# f = streets('streets', streetsDict, u'Category:Streets in Šestajovice (Prague-East District)', 'Šestajovice (okres Praha-východ)', 'Šestajovicích', 'Šestajovice')
# f = streets('streets', streetsDict, u'Category:Streets in Čečelice', 'Čečelice', 'Čečelicích', 'Čečelice')


# f = streets('streets', streetsDict, u'Category:Streets in Čečelice', 'Čečelice', 'Čečelicích', 'Čečelice')
# f = streets('streets', streetsDict, u'Category:Streets in Všetaty (Mělník District)', 'Všetaty (okres Mělník)', 'Všetatech', 'Všetaty')
# f = streets('streets', streetsDict, u'Category:Streets in Byšice', 'Byšice', 'Byšicích', 'Byšice')
# f = streets('streets', streetsDict, u'Category:Streets in Nové Dvory (Kutná Hora District)', 'Nové Dvory (okres Kutná Hora)', 'Nových Dvorech', 'Nové Dvory')
# f = streets('streets', streetsDict, u'Category:Streets in Středokluky', 'Středokluky', 'Středoklukách', 'Středokluky')
# f = streets('streets', streetsDict, u'Category:Streets in Všenory', 'Všenory', 'Všenorech', 'Všenory')
# f = streets('streets', streetsDict, u'Category:Streets in Máslovice', 'Máslovice', 'Máslovicích', 'Máslovice')
# f = streets('streets', streetsDict, u'Category:Streets in Mšeno', 'Mšeno', 'Mšeně', 'Mšeno')
# f = streets('streets', streetsDict, u'Category:Streets in Běleč (Liteň)', 'Liteň', 'Bělči (Liteň)', 'Běleč (Liteň)')
# f = streets('streets', streetsDict, u'Category:Streets in Smržovka', 'Smržovka', 'Smržovce', 'Smržovka')
# f = streets('streets', streetsDict, u'Category:Streets in Tanvald', 'Tanvald', 'Tanvaldu', 'Tanvald')
# f = streets('streets', streetsDict, u'Category:Streets in Jeřmanice', 'Jeřmanice', 'Jeřmanicích', 'Jeřmanice')
# f = streets('streets', streetsDict, u'Category:Streets in Mníšek (Liberec District)', 'Mníšek', 'Mníšku', 'Mníšek')
# f = streets('streets', streetsDict, u'Category:Streets in Hejnice (Liberec District)', 'Hejnice', 'Hejnicích', 'Hejnice')
# f = streets('streets', streetsDict, u'Category:Streets in Fojtka', 'Mníšek', 'Fojtce', 'Fojtka')
# f = streets('streets', streetsDict, u'Category:Streets in Hodkovice nad Mohelkou', 'Hodkovice nad Mohelkou', 'Hodkovicích nad Mohelkou', 'Hodkovice nad Mohelkou')
# f = streets('streets', streetsDict, u'Category:Streets in Hrádek nad Nisou', 'Hrádek nad Nisou', 'Hrádku nad Nisou', 'Hrádek nad Nisou')
# f = streets('streets', streetsDict, u'Category:Streets in Staré Splavy', 'Doksy', 'Starých Splavech', 'Staré Splavy')
# f = streets('streets', streetsDict, u'Category:Streets in Stráž pod Ralskem', 'Stráž pod Ralskem', 'Stráži pod Ralskem', 'Stráž pod Ralskem')
# f = streets('streets', streetsDict, u'Category:Streets in Klatovy', 'Klatovy', 'Klatovech', 'Klatovy')
# f = streets('streets', streetsDict, u'Category:Streets in Kladruby (Tachov District)', 'Kladruby (okres Tachov)', 'Kladrubech', 'Kladruby')
# f = streets('streets', streetsDict, u'Category:Streets in Stříbro', 'Stříbro (okres Tachov)', 'Stříbře', 'Stříbro')
# f = streets('streets', streetsDict, u'Category:Streets in Tachov', 'Tachov', 'Tachově', 'Tachov')
# f = streets('streets', streetsDict, u'Category:Streets in Vlachovo Březí', 'Vlachovo Březí', 'Vlachově Březí', 'Vlachovo Březí')
# f = streets('streets', streetsDict, u'Category:Streets in Tišnov', 'Tišnov', 'Tišnově', 'Tišnov')
# f = streets('streets', streetsDict, u'Category:Streets in Kunštát', 'Kunštát', 'Kunštátě', 'Kunštát')
# f = streets('streets', streetsDict, u'Category:Streets in Bílovice nad Svitavou', 'Bílovice nad Svitavou', 'Bílovicích nad Svitavou', 'Bílovice nad Svitavou')
# f = streets('streets', streetsDict, u'Category:Streets in Mariánské Radčice', 'Mariánské Radčice', 'Mariánských Radčicích', 'Mariánské Radčice')
# f = streets('streets', streetsDict, u'Category:Streets in Meziboří', 'Meziboří', 'Meziboří', 'Meziboří')
# f = streets('streets', streetsDict, u'Category:Streets in Rumburk', 'Rumburk', 'Rumburku', 'Rumburk')
# f = streets('streets', streetsDict, u'Category:Streets in Dolní Křečany', 'Rumburk', 'Dolních Křečanech', 'Dolní Křečany')
#
# f = streets('streets', streetsDict, u'Category:Streets in Abertamy', 'Abertamy', 'Abertamech', 'Abertamy')
# f = streets('streets', streetsDict, u'Category:Streets in Rybitví', 'Rybitví', 'Rybitví', 'Rybitví')
# f = streets('streets', streetsDict, u'Category:Streets in Samotišky', 'Samotišky', 'Samotiškách', 'Samotišky')
# f = streets('streets', streetsDict, u'Category:Streets in Osek', 'Osek (okres Teplice)', 'Oseku', 'Osek')
# f = streets('streets', streetsDict, u'Category:Streets in Adamov', 'Adamov (okres Blansko)', 'Adamově', 'Adamov')
#
# f = streets('streets', streetsDict, u'Category:Streets in Bechyně', 'Bechyně', 'Bechyni', 'Bechyně')
# f = streets('streets', streetsDict, u'Category:Streets in Benešov', 'Benešov', 'Benešově', 'Benešov')
# f = streets('streets', streetsDict, u'Category:Streets in Beroun', 'Beroun', 'Berouně', 'Beroun')
# f = streets('streets', streetsDict, u'Category:Streets in Bečov nad Teplou', 'Bečov nad Teplou', 'Bečově nad Teplou', 'Bečov nad Teplou')
# f = streets('streets', streetsDict, u'Category:Streets in Bílina', 'Bílina (město)', 'Bílině', 'Bílina')
# f = streets('streets', streetsDict, u'Category:Streets in Blansko', 'Blansko', 'Blansku', 'Blansko')
# f = streets('streets', streetsDict, u'Category:Streets in Bohumín', 'Bohumín', 'Bohumíně', 'Bohumín')
# f = streets('streets', streetsDict, u'Category:Streets in Boskovice', 'Boskovice', 'Boskovicích', 'Boskovice')
# f = streets('streets', streetsDict, u'Category:Streets in Brandýs nad Labem', 'Brandýs nad Labem-Stará Boleslav', 'Brandýse nad Labem', 'Brandýs nad Labem')
# f = streets('streets', streetsDict, u'Category:Streets in Brandýs nad Orlicí', 'Brandýs nad Orlicí', 'Brandýse nad Orlicí', 'Brandýs nad Orlicí')
# f = streets('streets', streetsDict, u'Category:Streets in Brno', 'Brno', 'Brně', 'Brno')
# f = streets('streets', streetsDict, u'Category:Streets in Bruntál', 'Bruntál', 'Bruntále', 'Bruntál')
# f = streets('streets', streetsDict, u'Category:Streets in Budyně nad Ohří', 'Budyně nad Ohří', 'Budyni nad Ohří', 'Budyně nad Ohří')
# f = streets('streets', streetsDict, u'Category:Streets in Břeclav', 'Břeclav', 'Břeclavi', 'Břeclav')
#
# f = streets('streets', streetsDict, u'Category:Streets in Cheb', 'Cheb', 'Chebu', 'Cheb')
# f = streets('streets', streetsDict, u'Category:Streets in Chlumec', 'Chlumec (okres Ústí nad Labem)', 'Chlumci', 'Chlumec')
# f = streets('streets', streetsDict, u'Category:Streets in Choceň', 'Choceň', 'Chocni', 'Choceň')
# f = streets('streets', streetsDict, u'Category:Streets in Chomutov', 'Chomutov', 'Chomutově', 'Chomutov')
# f = streets('streets', streetsDict, u'Category:Streets in Chotěboř', 'Chotěboř', 'Chotěboři', 'Chotěboř')
# f = streets('streets', streetsDict, u'Category:Streets in Chrast', 'Chrast', 'Chrasti', 'Chrast')
# f = streets('streets', streetsDict, u'Category:Streets in Chrastava', 'Chrastava', 'Chrastavě', 'Chrastava')
# f = streets('streets', streetsDict, u'Category:Streets in Chrudim', 'Chrudim', 'Chrudimi', 'Chrudim')
# f = streets('streets', streetsDict, u'Category:Streets in Cvikov', 'Cvikov', 'Cvikově', 'Cvikov')
#
# f = streets('streets', streetsDict, u'Category:Streets in Dačice', 'Dačice', 'Dačicích', 'Dačice')
# f = streets('streets', streetsDict, u'Category:Streets in Desná (Jablonec nad Nisou District)', 'Desná', 'Desné', 'Desná')
# f = streets('streets', streetsDict, u'Category:Streets in Dobřichovice', 'Dobřichovice', 'Dobřichovicích', 'Dobřichovice')
# f = streets('streets', streetsDict, u'Category:Streets in Dobříš', 'Dobříš', 'Dobříš', 'Dobříši')
# f = streets('streets', streetsDict, u'Category:Streets in Doksy', 'Doksy', 'Doksech', 'Doksy')
# f = streets('streets', streetsDict, u'Category:Streets in Doksy (Kladno District)', 'Doksy (okres Kladno)', 'Doksech', 'Doksy')
# f = streets('streets', streetsDict, u'Category:Streets in Domažlice', 'Domažlice', 'Domažlicích', 'Domažlice')
# f = streets('streets', streetsDict, u'Category:Streets in Dubá', 'Dubá', 'Dubé', 'Dubá')
# f = streets('streets', streetsDict, u'Category:Streets in Duchcov', 'Duchcov', 'Duchcově', 'Duchcov')
# f = streets('streets', streetsDict, u'Category:Streets in Dvorce (Bruntál District)', 'Dvorce (okres Bruntál)', 'Dvorcích', 'Dvorce')
# f = streets('streets', streetsDict, u'Category:Streets in Dvůr Králové nad Labem', 'Dvůr Králové nad Labem', 'Dvoře Králové nad Labem', 'Dvůr Králové nad Labem')
# f = streets('streets', streetsDict, u'Category:Streets in Děčín', 'Děčín', 'Děčíně', 'Děčín')
#
# f = streets('streets', streetsDict, u'Category:Streets in Františkovy Lázně', 'Františkovy Lázně', 'Františkových Lázních', 'Františkovy Lázně')
# f = streets('streets', streetsDict, u'Category:Streets in Frenštát pod Radhoštěm', 'Frenštát pod Radhoštěm', 'Frenštátě pod Radhoštěm', 'Frenštát pod Radhoštěm')
# f = streets('streets', streetsDict, u'Category:Streets in Frýdek-Místek', 'Frýdek-Místek', 'Frýdku-Místku', 'Frýdek-Místek')
# f = streets('streets', streetsDict, u'Category:Streets in Frýdlant', 'Frýdlant', 'Frýdlantě', 'Frýdlant')
# f = streets('streets', streetsDict, u'Category:Streets in Frýdlant nad Ostravicí', 'Frýdlant nad Ostravicí', 'Frýdlantě nad Ostravicí', 'Frýdlant nad Ostravicí')
# f = streets('streets', streetsDict, u'Category:Streets in Fulnek', 'Fulnek', 'Fulneku', 'Fulnek')
#
# f = streets('streets', streetsDict, u'Category:Streets in Kutná Hora-Vnitřní Město', 'Kutná Hora', 'Kutné Hoře', 'Kutná Hora')
# f = streets('streets', streetsDict, u'Category:Streets in Břuchotín', 'Křelov-Břuchotín', 'Břuchotíně', 'Břuchotín')
# f = streets('streets', streetsDict, u'Category:Streets in Křelov', 'Křelov-Břuchotín', 'Křelově', 'Křelov')
# f = streets('streets', streetsDict, u'Category:Streets in Ostrava by name', 'Ostrava', 'Ostravě', 'Ostrava')
# f = streets('streets', streetsDict, u'Category:Streets in Mariánské Hory', 'Ostrava', 'Ostravě', 'Ostrava')
# f = streets('streets', streetsDict, u'Category:Streets in Moravská Ostrava a Přívoz', 'Ostrava', 'Ostravě', 'Ostrava')
# f = streets('streets', streetsDict, u'Category:Streets in Ostrava-Jih', 'Ostrava', 'Ostravě', 'Ostrava')
# f = streets('streets', streetsDict, u'Category:Streets in Poruba (Ostrava)', 'Ostrava', 'Ostravě', 'Ostrava')
# f = streets('streets', streetsDict, u'Category:Streets in Kuří (Říčany)', 'Říčany', 'Kuří', 'Kuří')
# f = streets('streets', streetsDict, u'Category:Streets in Místek', 'Frýdek-Místek', 'Místku', 'Místek')
# f = streets('streets', streetsDict, u'Category:Streets in Frýdek', 'Frýdek-Místek', 'Frýdku', 'Frýdek')
# f = streets('streets', streetsDict, u'Category:Streets in Frýdek-Místek by name', 'Frýdek-Místek', 'Frýdku-Místku', 'Frýdek-Místek')
# f = streets('streets', streetsDict, u'Category:Streets in Rybárny', 'Uherské Hradiště', 'Rybárnách', 'Rybárny')
#
# f = streets('streets', streetsDict, u'Category:Streets in Havlíčkův Brod', 'Havlíčkův Brod', 'Havlíčkově Brodě', 'Havlíčkův Brod')
# f = streets('streets', streetsDict, u'Category:Streets in Havířov', 'Havířov', 'Havířově', 'Havířov')
# f = streets('streets', streetsDict, u'Category:Streets in Hlinsko', 'Hlinsko', 'Hlinsku', 'Hlinsko')
# f = streets('streets', streetsDict, u'Category:Streets in Hodonín', 'Hodonín', 'Hodoníně', 'Hodonín')
# f = streets('streets', streetsDict, u'Category:Streets in Holešov', 'Holešov', 'Holešově', 'Holešov')
# f = streets('streets', streetsDict, u'Category:Streets in Holice', 'Holice', 'Holicích', 'Holice')
# f = streets('streets', streetsDict, u'Category:Streets in Hora Svaté Kateřiny', 'Hora Svaté Kateřiny', 'Hoře Svaté Kateřiny', 'Hora Svaté Kateřiny')
# f = streets('streets', streetsDict, u'Category:Streets in Horní Blatná', 'Horní Blatná', 'Horní Blatné', 'Horní Blatná')
# f = streets('streets', streetsDict, u'Category:Streets in Horšovský Týn', 'Horšovský Týn', 'Horšovském Týně', 'Horšovský Týn')
# f = streets('streets', streetsDict, u'Category:Streets in Hostinné', 'Hostinné', 'Hostinném', 'Hostinné')
# f = streets('streets', streetsDict, u'Category:Streets in Hostivice', 'Hostivice', 'Hostivici', 'Hostivice')
# f = streets('streets', streetsDict, u'Category:Streets in Hořice', 'Hořice', 'Hořicích', 'Hořice')
# f = streets('streets', streetsDict, u'Category:Streets in Hradec Králové', 'Hradec Králové', 'Hradci Králové', 'Hradec Králové')
# f = streets('streets', streetsDict, u'Category:Streets in Hranice', 'Hranice (okres Přerov)', 'Hranicích', 'Hranice')
# f = streets('streets', streetsDict, u'Category:Streets in Hronov', 'Hronov', 'Hronově', 'Hronov')
# f = streets('streets', streetsDict, u'Category:Streets in Hustopeče', 'Hustopeče', 'Hustopečích', 'Hustopeče')
#
# f = streets('streets', streetsDict, u'Category:Streets in Ivančice', 'Ivančice', 'Ivančicích', 'Ivančice')
#
# f = streets('streets', streetsDict, u'Category:Streets in Jablonné v Podještědí', 'Jablonné v Podještědí', 'Jablonném v Podještědí', 'Jablonné v Podještědí')
# f = streets('streets', streetsDict, u'Category:Streets in Jablonec nad Nisou', 'Jablonec nad Nisou', 'Jablonci nad Nisou', 'Jablonec nad Nisou')
# f = streets('streets', streetsDict, u'Category:Streets in Jaroměř', 'Jaroměř', 'Jaroměři', 'Jaroměř')
# f = streets('streets', streetsDict, u'Category:Streets in Jesenice (Prague-West District)', 'Jesenice (okres Praha-západ)', 'Jesenici', 'Jesenice')
# f = streets('streets', streetsDict, u'Category:Streets in Jeseník', 'Jeseník', 'Jeseníku', 'Jeseník')
# f = streets('streets', streetsDict, u'Category:Streets in Jihlava', 'Jihlava', 'Jihlavě', 'Jihlava')
# f = streets('streets', streetsDict, u'Category:Streets in Jilemnice', 'Jilemnice', 'Jilemnici', 'Jilemnice')
# f = streets('streets', streetsDict, u'Category:Streets in Jílové u Prahy', 'Jílové u Prahy', 'Jílovém u Prahy', 'Jílové u Prahy')
# f = streets('streets', streetsDict, u'Category:Streets in Jimramov', 'Jimramov', 'Jimramově', 'Jimramov')
# f = streets('streets', streetsDict, u'Category:Streets in Jindřichův Hradec', 'Jindřichův Hradec', 'Jindřichově Hradci', 'Jindřichův Hradec')
# f = streets('streets', streetsDict, u'Category:Streets in Jirkov', 'Jirkov', 'Jirkově', 'Jirkov')
# f = streets('streets', streetsDict, u'Category:Streets in Jičín', 'Jičín', 'Jičíně', 'Jičín')
# f = streets('streets', streetsDict, u'Category:Streets in Josefov (Jaroměř)', 'Jaroměř', 'Josefově (Jaroměř)', 'Josefov (Jaroměř)')
#
# f = streets('streets', streetsDict, u'Category:Streets in Kadaň', 'Kadaň', 'Kadani', 'Kadaň')
# f = streets('streets', streetsDict, u'Category:Streets in Kamenický Šenov', 'Kamenický Šenov', 'Kamenickém Šenově', 'Kamenický Šenov')
# f = streets('streets', streetsDict, u'Category:Streets in Karlovy Vary', 'Karlovy Vary', 'Karlových Varech', 'Karlovy Vary')
# f = streets('streets', streetsDict, u'Category:Streets in Karviná', 'Karviná', 'Karviné', 'Karviná')
# f = streets('streets', streetsDict, u'Category:Streets in Kašperské Hory', 'Kašperské Hory', 'Kašperských Horách', 'Kašperské Hory')
# f = streets('streets', streetsDict, u'Category:Streets in Kladno', 'Kladno', 'Kladně', 'Kladno')
# f = streets('streets', streetsDict, u'Category:Streets in Klobouky u Brna', 'Klobouky u Brna', 'Kloboukách u Brna', 'Klobouky u Brna')
# f = streets('streets', streetsDict, u'Category:Streets in Klášterec nad Ohří', 'Klášterec nad Ohří', 'Klášterci nad Ohří', 'Klášterec nad Ohří')
# f = streets('streets', streetsDict, u'Category:Streets in Kolín', 'Kolín', 'Kolíně', 'Kolín')
# f = streets('streets', streetsDict, u'Category:Streets in Kopidlno', 'Kopidlno', 'Kopidlně', 'Kopidlno')
# f = streets('streets', streetsDict, u'Category:Streets in Kostelec nad Labem', 'Kostelec nad Labem', 'Kostelci nad Labem', 'Kostelec nad Labem')
# f = streets('streets', streetsDict, u'Category:Streets in Kostelec nad Orlicí', 'Kostelec nad Orlicí', 'Kostelci nad Orlicí', 'Kostelec nad Orlicí')
# f = streets('streets', streetsDict, u'Category:Streets in Kostelec nad Černými lesy', 'Kostelec nad Černými lesy', 'Kostelci nad Černými lesy', 'Kostelec nad Černými lesy')
# f = streets('streets', streetsDict, u'Category:Streets in Kouřim', 'Kouřim', 'Kouřimi', 'Kouřim')
# f = streets('streets', streetsDict, u'Category:Streets in Kralovice', 'Kralovice', 'Kralovicích', 'Kralovice')
# f = streets('streets', streetsDict, u'Category:Streets in Kralupy nad Vltavou', 'Kralupy nad Vltavou', 'Kralupech nad Vltavou', 'Kralupy nad Vltavou')
# f = streets('streets', streetsDict, u'Category:Streets in Krásná Lípa', 'Krásná Lípa', 'Krásné Lípě', 'Krásná Lípa')
# f = streets('streets', streetsDict, u'Category:Streets in Krnov', 'Krnov', 'Krnově', 'Krnov')
# f = streets('streets', streetsDict, u'Category:Streets in Kroměříž', 'Kroměříž', 'Kroměříži', 'Kroměříž')
# f = streets('streets', streetsDict, u'Category:Streets in Králův Dvůr', 'Králův Dvůr', 'Králově Dvoře', 'Králův Dvůr')
# f = streets('streets', streetsDict, u'Category:Streets in Kutná Hora', 'Kutná Hora', 'Kutné Hoře', 'Kutná Hora')
# f = streets('streets', streetsDict, u'Category:Streets in Kyjov', 'Kyjov', 'Kyjově', 'Kyjov')
# f = streets('streets', streetsDict, u'Category:Streets in Křelov-Břuchotín', 'Křelov-Břuchotín', 'Křelově-Břuchotíně', 'Křelov-Břuchotín')
#
# f = streets('streets', streetsDict, u'Category:Streets in Libčice nad Vltavou', 'Libčice nad Vltavou', 'Libčicích nad Vltavou', 'Libčice nad Vltavou')
# f = streets('streets', streetsDict, u'Category:Streets in Liběchov', 'Liběchov', 'Liběchově', 'Liběchov')
# f = streets('streets', streetsDict, u'Category:Streets in Lipník nad Bečvou', 'Lipník nad Bečvou', 'Lipníku nad Bečvou', 'Lipník nad Bečvou')
# f = streets('streets', streetsDict, u'Category:Streets in Liteň', 'Liteň', 'Litni', 'Liteň')
# f = streets('streets', streetsDict, u'Category:Streets in Litomyšl', 'Litomyšl', 'Litomyšli', 'Litomyšl')
# f = streets('streets', streetsDict, u'Category:Streets in Litoměřice', 'Litoměřice', 'Litoměřicích', 'Litoměřice')
# f = streets('streets', streetsDict, u'Category:Streets in Litovel', 'Litovel', 'Litovli', 'Litovel')
# f = streets('streets', streetsDict, u'Category:Streets in Litvínov', 'Litvínov', 'Litvínově', 'Litvínov')
# f = streets('streets', streetsDict, u'Category:Streets in Loket', 'Loket (okres Sokolov)', 'Lokti', 'Loket',)
# f = streets('streets', streetsDict, u'Category:Streets in Lom (Most District)', 'Lom (okres Most)', 'Lomu', 'Lom')
# f = streets('streets', streetsDict, u'Category:Streets in Lomnice nad Popelkou', 'Lomnice nad Popelkou', 'Lomnici nad Popelkou', 'Lomnice nad Popelkou')
# f = streets('streets', streetsDict, u'Category:Streets in Louny', 'Louny', 'Lounech', 'Louny')
# f = streets('streets', streetsDict, u'Category:Streets in Lovosice', 'Lovosice', 'Lovosicích', 'Lovosice')
# f = streets('streets', streetsDict, u'Category:Streets in Luhačovice', 'Luhačovice', 'Luhačovicích', 'Luhačovice')
# f = streets('streets', streetsDict, u'Category:Streets in Lysice', 'Lysice', 'Lysicích', 'Lysice')
# f = streets('streets', streetsDict, u'Category:Streets in Lysá nad Labem', 'Lysá nad Labem', 'Lysé nad Labem', 'Lysá nad Labem')
#
# f = streets('streets', streetsDict, u'Category:Streets in Malešov', 'Malešov', 'Malešově', 'Malešov')
# f = streets('streets', streetsDict, u'Category:Streets in Mariánské Lázně', 'Mariánské Lázně', 'Mariánských Lázních', 'Mariánské Lázně')
# f = streets('streets', streetsDict, u'Category:Streets in Mělník', 'Mělník', 'Mělníku', 'Mělník')
# f = streets('streets', streetsDict, u'Category:Streets in Mikulov', 'Mikulov', 'Mikulově', 'Mikulov')
# f = streets('streets', streetsDict, u'Category:Streets in Milevsko', 'Milevsko', 'Milevsku', 'Milevsko')
# f = streets('streets', streetsDict, u'Category:Streets in Mimoň', 'Mimoň', 'Mimoni', 'Mimoň')
# f = streets('streets', streetsDict, u'Category:Streets in Miroslav', 'Miroslav (okres Znojmo)', 'Miroslavi', 'Miroslav')
# f = streets('streets', streetsDict, u'Category:Streets in Mladá Boleslav', 'Mladá Boleslav', 'Mladé Boleslavi', 'Mladá Boleslav')
# f = streets('streets', streetsDict, u'Category:Streets in Mnichovo Hradiště', 'Mnichovo Hradiště', 'Mnichově Hradišti', 'Mnichovo Hradiště')
# f = streets('streets', streetsDict, u'Category:Streets in Mohelnice', 'Mohelnice', 'Mohelnici', 'Mohelnice')
# f = streets('streets', streetsDict, u'Category:Streets in Moravská Třebová', 'Moravská Třebová', 'Moravské Třebové', 'Moravská Třebová')
# f = streets('streets', streetsDict, u'Category:Streets in Moravské Budějovice', 'Moravské Budějovice', 'Moravských Budějovicích', 'Moravské Budějovice')
# f = streets('streets', streetsDict, u'Category:Streets in Most', 'Most (město)', 'Mostě', 'Most')
#
# f = streets('streets', streetsDict, u'Category:Streets in Napajedla', 'Napajedla', 'Napajedlech', 'Napajedla')
# f = streets('streets', streetsDict, u'Category:Streets in Nechanice', 'Nechanice', 'Nechanicích', 'Nechanice')
# f = streets('streets', streetsDict, u'Category:Streets in Nepomuk', 'Nepomuk', 'Nepomuku', 'Nepomuk')
# f = streets('streets', streetsDict, u'Category:Streets in Neratovice', 'Neratovice', 'Neratovicích', 'Neratovice')
# f = streets('streets', streetsDict, u'Category:Streets in Neveklov', 'Neveklov', 'Neveklově', 'Neveklov')
#
# f = streets('streets', streetsDict, u'Category:Streets in Nové Město nad Metují', 'Nové Město nad Metují', 'Novém Městě nad Metují', 'Nové Město nad Metují')
# f = streets('streets', streetsDict, u'Category:Streets in Nový Bor', 'Nový Bor', 'Novém Boru', 'Nový Bor')
# f = streets('streets', streetsDict, u'Category:Streets in Nový Jičín', 'Nový Jičín', 'Novém Jičíně', 'Nový Jičín')
# f = streets('streets', streetsDict, u'Category:Streets in Nymburk', 'Nymburk', 'Nymburku', 'Nymburk')
# f = streets('streets', streetsDict, u'Category:Streets in Náchod', 'Náchod', 'Náchodě', 'Náchod')
#
# f = streets('streets', streetsDict, u'Category:Streets in Olomouc', 'Olomouc', 'Olomouci', 'Olomouc')
# f = streets('streets', streetsDict, u'Category:Streets in Opava', 'Opava', 'Opavě', 'Opava')
# f = streets('streets', streetsDict, u'Category:Streets in Ostrava', 'Ostrava', 'Ostravě', 'Ostrava')
# f = streets('streets', streetsDict, u'Category:Streets in Otrokovice', 'Otrokovice', 'Otrokovicích', 'Otrokovice')
#
# f = streets('streets', streetsDict, u'Category:Streets in Pardubice', 'Pardubice', 'Pardubicích', 'Pardubice')
# f = streets('streets', streetsDict, u'Category:Streets in Pelhřimov', 'Pelhřimov', 'Pelhřimově', 'Pelhřimov')
# f = streets('streets', streetsDict, u'Category:Streets in Pečky', 'Pečky', 'Pečkách', 'Pečky')
# f = streets('streets', streetsDict, u'Category:Streets in Písek', 'Písek (město)', 'Písku', 'Písek')
# f = streets('streets', streetsDict, u'Category:Streets in Plzeň', 'Plzeň', 'Plzni', 'Plzeň')
# f = streets('streets', streetsDict, u'Category:Streets in Poděbrady', 'Poděbrady', 'Poděbradech', 'Poděbrady')
# f = streets('streets', streetsDict, u'Category:Streets in Pohled', 'Pohled (okres Havlíčkův Brod)', 'Pohledu', 'Pohled')
# f = streets('streets', streetsDict, u'Category:Streets in Polička', 'Polička', 'Poličce', 'Polička')
# f = streets('streets', streetsDict, u'Category:Streets in Polná', 'Polná', 'Polné', 'Polná')
# f = streets('streets', streetsDict, u'Category:Streets in Prachatice', 'Prachatice', 'Prachaticích', 'Prachatice')
# f = streets('streets', streetsDict, u'Category:Streets in Příbram', 'Příbram', 'Příbrami', 'Příbram')
# f = streets('streets', streetsDict, u'Category:Streets in Prostějov', 'Prostějov', 'Prostějově', 'Prostějov')
# f = streets('streets', streetsDict, u'Category:Streets in Protivín', 'Protivín', 'Protivíně', 'Protivín')
# f = streets('streets', streetsDict, u'Category:Streets in Pyšely', 'Pyšely', 'Pyšelech', 'Pyšely')
# f = streets('streets', streetsDict, u'Category:Streets in Přerov', 'Přerov', 'Přerově', 'Přerov')
# f = streets('streets', streetsDict, u'Category:Streets in Přeštice', 'Přeštice', 'Přešticích', 'Přeštice')
# f = streets('streets', streetsDict, u'Category:Streets in Přibyslav', 'Přibyslav', 'Přibyslavi', 'Přibyslav')
# f = streets('streets', streetsDict, u'Category:Streets in Příbor', 'Příbor (okres Nový Jičín)', 'Příboře', 'Příbor')
#
# f = streets('streets', streetsDict, u'Category:Streets in Rakovník', 'Rakovník', 'Rakovníku', 'Rakovník')
# f = streets('streets', streetsDict, u'Category:Streets in Raspenava', 'Raspenava', 'Raspenavě', 'Raspenava')
# f = streets('streets', streetsDict, u'Category:Streets in Rokycany', 'Rokycany', 'Rokycanech', 'Rokycany')
# f = streets('streets', streetsDict, u'Category:Streets in Rosice (Brno-Country District)', 'Rosice (okres Brno-venkov)', 'Rosicích', 'Rosice')
# f = streets('streets', streetsDict, u'Category:Streets in Roudnice nad Labem', 'Roudnice nad Labem', 'Roudnici nad Labem', 'Roudnice nad Labem')
# f = streets('streets', streetsDict, u'Category:Streets in Roztoky (Prague-West District)', 'Roztoky (okres Praha-západ)', 'Roztokách', 'Roztoky')
# f = streets('streets', streetsDict, u'Category:Streets in Rožmitál pod Třemšínem', 'Rožmitál pod Třemšínem', 'Rožmitále pod Třemšínem', 'Rožmitál pod Třemšínem')
# f = streets('streets', streetsDict, u'Category:Streets in Rudná', 'Rudná (okres Praha-západ)', 'Rudné', 'Rudná')
# f = streets('streets', streetsDict, u'Category:Streets in Rychnov nad Kněžnou', 'Rychnov nad Kněžnou', 'Rychnově nad Kněžnou', 'Rychnov nad Kněžnou')
# f = streets('streets', streetsDict, u'Category:Streets in Rýmařov', 'Rýmařov', 'Rýmařově', 'Rýmařov')
#
# f = streets('streets', streetsDict, u'Category:Streets in Semily', 'Semily', 'Semilech', 'Semily')
# f = streets('streets', streetsDict, u'Category:Streets in Sezimovo Ústí', 'Sezimovo Ústí', 'Sezimově Ústí', 'Sezimovo Ústí')
# f = streets('streets', streetsDict, u'Category:Streets in Skuteč', 'Skuteč', 'Skutči', 'Skuteč')
# f = streets('streets', streetsDict, u'Category:Streets in Slaný', 'Slaný', 'Slaném', 'Slaný')
# f = streets('streets', streetsDict, u'Category:Streets in Slavkov u Brna', 'Slavkov u Brna', 'Slavkově u Brna', 'Slavkov u Brna')
# f = streets('streets', streetsDict, u'Category:Streets in Slavonice', 'Slavonice', 'Slavonicích', 'Slavonice')
# f = streets('streets', streetsDict, u'Category:Streets in Sloup v Čechách', 'Sloup v Čechách', 'Sloupu v Čechách', 'Sloup v Čechách')
# f = streets('streets', streetsDict, u'Category:Streets in Sobotka', 'Sobotka', 'Sobotce', 'Sobotka')
# f = streets('streets', streetsDict, u'Category:Streets in Soběslav', 'Soběslav', 'Soběslavi', 'Soběslav')
# f = streets('streets', streetsDict, u'Category:Streets in Sokolov', 'Sokolov', 'Sokolově', 'Sokolov')
# f = streets('streets', streetsDict, u'Category:Streets in Srbsko (Beroun District)', 'Srbsko (okres Beroun)', 'Srbsku', 'Srbsko')
# f = streets('streets', streetsDict, u'Category:Streets in Staré Město (Uherské Hradiště District)', 'Staré Město (okres Uherské Hradiště)', 'Starém Městě', 'Staré Město')
# f = streets('streets', streetsDict, u'Category:Streets in Strakonice', 'Strakonice', 'Strakonicích', 'Strakonice')
# f = streets('streets', streetsDict, u'Category:Streets in Stříbrná Skalice', 'Stříbrná Skalice', 'Stříbrné Skalici', 'Stříbrná Skalice')
# f = streets('streets', streetsDict, u'Category:Streets in Sušice', 'Sušice', 'Sušici', 'Sušice')
# f = streets('streets', streetsDict, u'Category:Streets in Svatava', 'Svatava (okres Sokolov)', 'Svatavě', 'Svatava')
# f = streets('streets', streetsDict, u'Category:Streets in Svitavy', 'Svitavy', 'Svitavách', 'Svitavy')
# f = streets('streets', streetsDict, u'Category:Streets in Svoboda nad Úpou', 'Svoboda nad Úpou', 'Svobodě nad Úpou', 'Svoboda nad Úpou')
#
# f = streets('streets', streetsDict, u'Category:Streets in Tábor', 'Tábor', 'Táboře', 'Tábor')
# f = streets('streets', streetsDict, u'Category:Streets in Telč', 'Telč', 'Telči', 'Telč')
# f = streets('streets', streetsDict, u'Category:Streets in Teplice', 'Teplice', 'Teplicích', 'Teplice')
# f = streets('streets', streetsDict, u'Category:Streets in Trutnov', 'Trutnov', 'Trutnově', 'Trutnov')
# f = streets('streets', streetsDict, u'Category:Streets in Turnov', 'Turnov', 'Turnově', 'Turnov')
# f = streets('streets', streetsDict, u'Category:Streets in Týn nad Vltavou', 'Týn nad Vltavou', 'Týně nad Vltavou', 'Týn nad Vltavou')
# f = streets('streets', streetsDict, u'Category:Streets in Třeboň', 'Třeboň', 'Třeboni', 'Třeboň')
# f = streets('streets', streetsDict, u'Category:Streets in Třešť', 'Třešť', 'Třešti', 'Třešť')
# f = streets('streets', streetsDict, u'Category:Streets in Třinec', 'Třinec', 'Třinci', 'Třinec')
#
# f = streets('streets', streetsDict, u'Category:Streets in Uherské Hradiště', 'Uherské Hradiště', 'Uherském Hradišti', 'Uherské Hradiště')
# f = streets('streets', streetsDict, u'Category:Streets in Uherský Brod', 'Uherský Brod', 'Uherském Brodě', 'Uherský Brod')
# f = streets('streets', streetsDict, u'Category:Streets in Uničov', 'Uničov', 'Uničově', 'Uničov')
#
# f = streets('streets', streetsDict, u'Category:Streets in Valašské Meziříčí', 'Valašské Meziříčí', 'Valašském Meziříčí', 'Valašské Meziříčí')
# f = streets('streets', streetsDict, u'Category:Streets in Valtice', 'Valtice', 'Valticích', 'Valtice')
# f = streets('streets', streetsDict, u'Category:Streets in Vamberk', 'Vamberk', 'Vamberku', 'Vamberk')
# f = streets('streets', streetsDict, u'Category:Streets in Varnsdorf', 'Varnsdorf', 'Varnsdorfu', 'Varnsdorf')
# f = streets('streets', streetsDict, u'Category:Streets in Velké Popovice', 'Velké Popovice', 'Velkých Popovicích', 'Velké Popovice')
# f = streets('streets', streetsDict, u'Category:Streets in Velké Přílepy', 'Velké Přílepy', 'Velkých Přílepech', 'Velké Přílepy')
# f = streets('streets', streetsDict, u'Category:Streets in Veltrusy', 'Veltrusy', 'Veltrusech', 'Veltrusy')
# f = streets('streets', streetsDict, u'Category:Streets in Velvary', 'Velvary', 'Velvarech', 'Velvary')
# f = streets('streets', streetsDict, u'Category:Streets in Veselí nad Lužnicí', 'Veselí nad Lužnicí', 'Veselí nad Lužnicí', 'Veselí nad Lužnicí')
# f = streets('streets', streetsDict, u'Category:Streets in Vimperk', 'Vimperk', 'Vimperku', 'Vimperk')
# f = streets('streets', streetsDict, u'Category:Streets in Vítkov', 'Vítkov', 'Vítkově', 'Vítkov')
# f = streets('streets', streetsDict, u'Category:Streets in Volyně', 'Volyně', 'Volyni', 'Volyně')
# f = streets('streets', streetsDict, u'Category:Streets in Votice', 'Votice', 'Voticích', 'Votice')
# f = streets('streets', streetsDict, u'Category:Streets in Vrchlabí', 'Vrchlabí', 'Vrchlabí', 'Vrchlabí')
# f = streets('streets', streetsDict, u'Category:Streets in Vsetín', 'Vsetín', 'Vsetíně', 'Vsetín')
# f = streets('streets', streetsDict, u'Category:Streets in Vysoké Mýto', 'Vysoké Mýto', 'Vysokém Mýtě', 'Vysoké Mýto')
# f = streets('streets', streetsDict, u'Category:Streets in Vyškov', 'Vyškov', 'Vyškově', 'Vyškov')
#
# f = streets('streets', streetsDict, u'Category:Streets in Zákupy', 'Zákupy', 'Zákupech', 'Zákupy')
# f = streets('streets', streetsDict, u'Category:Streets in Zdice', 'Zdice', 'Zdici', 'Zdice')
# f = streets('streets', streetsDict, u'Category:Streets in Zlín', 'Zlín', 'Zlíně', 'Zlín')
# f = streets('streets', streetsDict, u'Category:Streets in Znojmo', 'Znojmo', 'Znojmě', 'Znojmo')
# f = streets('streets', streetsDict, u'Category:Streets in Zruč nad Sázavou', 'Zruč nad Sázavou', 'Zruči nad Sázavou', 'Zruč nad Sázavou')
# f = streets('streets', streetsDict, u'Category:Streets in Zábřeh', 'Zábřeh', 'Zábřehu', 'Zábřeh')
#
# f = streets('streets', streetsDict, u'Category:Streets in Ústí nad Labem', 'Ústí nad Labem', 'Ústí nad Labem', 'Ústí nad Labem')
# f = streets('streets', streetsDict, u'Category:Streets in Ústí nad Orlicí', 'Ústí nad Orlicí', 'Ústí nad Orlicí', 'Ústí nad Orlicí')
# f = streets('streets', streetsDict, u'Category:Streets in Úštěk', 'Úštěk', 'Úštěku', 'Úštěk')
#
# f = streets('streets', streetsDict, u'Category:Streets in Čáslav', 'Čáslav', 'Čáslavi', 'Čáslav')
# f = streets('streets', streetsDict, u'Category:Streets in Černošice', 'Černošice', 'Černošicích', 'Černošice')
# f = streets('streets', streetsDict, u'Category:Streets in Česká Lípa', 'Česká Lípa', 'České Lípě', 'Česká Lípa')
# f = streets('streets', streetsDict, u'Category:Streets in Česká Třebová', 'Česká Třebová', 'České Třebové', 'Česká Třebová')
# f = streets('streets', streetsDict, u'Category:Streets in České Budějovice', 'České Budějovice', 'Českých Budějovicích', 'České Budějovice')
# f = streets('streets', streetsDict, u'Category:Streets in Český Brod', 'Český Brod', 'Českém Brodě', 'Český Brod')
# f = streets('streets', streetsDict, u'Category:Streets in Český Dub', 'Český Dub', 'Českém Dubu', 'Český Dub')
# f = streets('streets', streetsDict, u'Category:Streets in Český Krumlov', 'Český Krumlov', 'Českém Krumlově', 'Český Krumlov')
# f = streets('streets', streetsDict, u'Category:Streets in Český Těšín', 'Český Těšín', 'Českém Těšíně', 'Český Těšín')
#
# f = streets('streets', streetsDict, u'Category:Streets in Řevnice', 'Řevnice', 'Řevnicích', 'Řevnice')
# f = streets('streets', streetsDict, u'Category:Streets in Říčany (Prague-East District)', 'Říčany', 'Říčanech', 'Říčany')
#
# f = streets('streets', streetsDict, u'Category:Streets in Šluknov', 'Šluknov', 'Šluknově', 'Šluknov')
# f = streets('streets', streetsDict, u'Category:Streets in Šternberk', 'Šternberk', 'Šternberku', 'Šternberk')
# f = streets('streets', streetsDict, u'Category:Streets in Štramberk', 'Štramberk', 'Štramberku', 'Štramberk')
# f = streets('streets', streetsDict, u'Category:Streets in Šumperk', 'Šumperk', 'Šumperku', 'Šumperk')
#
# f = streets('streets', streetsDict, u'Category:Streets in Žatec', 'Žatec', 'Žatci', 'Žatec')
# f = streets('streets', streetsDict, u'Category:Streets in Žďár nad Sázavou', 'Žďár nad Sázavou', 'Žďáru nad Sázavou', 'Žďár nad Sázavou')
