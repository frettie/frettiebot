#!/usr/bin/python
# -*- coding: utf-8 -*-
import pywikibot

class OpenDataDay():

    def __init__(self):
        print "Prezentace International Open Data Day"
        self.site = pywikibot.Site('cs', 'wikipedia')

    def insertDescription(self):
        descriptions = {}
        descriptions['cs'] = 'Železniční trať v České republice'
        descriptions['en'] = 'Railroad track in the Czech Republic'
        self.wikidataItem.editDescriptions(descriptions)

    def insertAlias(self):
        aliases = {}
        aliases['cs'] = ['Trať 240']
        self.wikidataItem.editAliases(aliases)

    def insertClaim(self):
        okresTrebic = pywikibot.Page(self.site, u'Okres Třebíč')
        okresTrebicItem = pywikibot.ItemPage.fromPage(okresTrebic)

        claim = pywikibot.Claim(okresTrebicItem.site, u'P131')
        claim.setTarget(okresTrebicItem)

        self.wikidataItem.addClaim(claim)

    def run(self):

        page = pywikibot.Page(self.site, u'Železniční trať Brno–Jihlava')
        #print page.get()
        self.wikidataItem = pywikibot.ItemPage.fromPage(page)
        #print self.wikidataItem.labels







open = OpenDataDay()
open.run()
open.insertAlias()
open.insertDescription()
open.insertClaim()
