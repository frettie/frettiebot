#!/usr/bin/env python
#-*- coding: utf-8 -*-

from fdfgen import forge_fdf
import io

import pdfrw
# from reportlab.pdfgen import canvas

data_file = open('data.tsv', 'r')
data = data_file.readlines()
data.pop(0)
data_file.close()

fields = []
for i in range(0, len(data)):
	line = data[i].split('\t')
	displayrealname = 'občanské jméno' in line[4]
	displayusername = 'uživatelské jméno' in line[4]
	if displayrealname:
		first_name_data = line[1]
		last_name_data = line[2]
	else:
		first_name_data = ''
		last_name_data = ''
	if displayusername:
		username_data = 'Wikipedista:' + line[3]
	else:
		username_data = ''
	organization_data = ''

	first_name_field = u'Jméno'
	last_name_field = u'Příjmení'
	username_field = u'Už. jméno'
	organization_field = 'Organizace'

	if i>0:
		first_name_field += str(i)
		last_name_field += str(i)
		username_field += str(i)
		organization_field += str(i)
	fields.append((first_name_field, first_name_data))
	fields.append((last_name_field, last_name_data))
	fields.append((username_field, username_data))
	fields.append((organization_field, organization_data))
	if i>15:
		break

print(fields)
fdf = forge_fdf(None, fields, [], [], [])
fdf_file = open('form.fdf', 'wb')
fdf_file.write(fdf)
fdf_file.close()

template = pdfrw.PdfReader('form.pdf')
for page in template.Root.Pages.Kids:
    for field in page.Annots:
        label = field.T
        sides_positions = field.Rect