#!/usr/bin/python
# -*- coding: utf-8 -*-
# import unicodecsv

# from logger import Logger
import untangle
# import pywikibot
# import re

# from pywikibot import pagegenerators
# from pywikibot.textlib import mwparserfromhell as parser

# from math import radians, cos, sin, asin, sqrt

class volby():

    def __init__(self):
        self.candidates = {}
        self.cities = {}
        self.obce = {}
        self.load_obce()
        self.okrsky_v_obcich = {}
        self.obce_dva = {}
        self.vitez_dle_okrsku = {}
        self.pocet_okrsku = {}

        self.pocty_dle_okrsku = {}

        # self.load_dict('volby_prez_2013')
        # self.load_dict('volby_prez_2018')

        self.load_dict('volby_prez_2013')
        self.load_dict('volby_prez_2018')

        self.kandidati('volby_prez_2013')
        self.kandidati('volby_prez_2018')

        self.get_vitez('volby_prez_2013', 1)
        self.get_vitez('volby_prez_2018', 1)

        self.who_win(1, 'volby_prez_2013', 'volby_prez_2018')

    def kandidati(self, file_name):

        # if (type == 'volby_prez_2013'):
        if ('volby_prez_2013' == file_name or 'volby_test_2013' == file_name):
            self.candidates[file_name] = {}
            self.candidates[file_name][1] =  'Zuzana Roithová'
            self.candidates[file_name][2] = 'Jan Fischer'
            self.candidates[file_name][3] = 'Jana Bobošíková'
            self.candidates[file_name][4] = 'Taťana Fischerová'
            self.candidates[file_name][5] = 'Přemysl Sobotka'
            self.candidates[file_name][6] = 'Miloš Zeman'
            self.candidates[file_name][7] = 'Vladimír Franz'
            self.candidates[file_name][8] = 'Jiří Dienstbier'
            self.candidates[file_name][9] = 'Karel Schwarzenberg'

        if ('volby_prez_2018' == file_name or 'volby_test_2018' == file_name):
            self.candidates[file_name] = {}
            self.candidates[file_name][1] = 'Mirek Topolánek'
            self.candidates[file_name][2] = 'Michal Horáček'
            self.candidates[file_name][3] = 'Pavel Fischer'
            self.candidates[file_name][4] = 'Jiří Hynek'
            self.candidates[file_name][5] = 'Petr Hannig'
            self.candidates[file_name][6] = 'Vratislav Kulhánek'
            self.candidates[file_name][7] = 'Miloš Zeman'
            self.candidates[file_name][8] = 'Marek Hilšer'
            self.candidates[file_name][9] = 'Jiří Drahoš'

    def load_obce(self):
        obj = untangle.parse('cisob.xml')
        for line in obj.CISOB.CISOB_ROW:
            pass
            hlasy = {}
            cislo = line.get_elements('OBEC_PREZ')[0].cdata
            nazev = line.get_elements('NAZEVOBCE')[0].cdata
            self.obce[cislo] = nazev

    def who_win(self, kolo, file_name_first, file_name_second):
        print 'fds'
        self.pocty_dle_okrsku['drahos_schwarzenberg'] = 0
        self.pocty_dle_okrsku['zeman_zeman'] = 0
        self.pocty_dle_okrsku['zeman_schwarzenberg'] = 0
        self.pocty_dle_okrsku['drahos_zeman'] = 0
        self.pocty_dle_okrsku['ostatni'] = 0
        # for obce_cislo, nazev in self.obce.items():
        # for obce_cislo in self.obce_dva:
        for obce_cislo,obce in self.vitez_dle_okrsku[file_name_first][kolo].items():
            try:
                for okrsek_cislo,okrsek in obce.items():
                    # print '2013: ' + self.candidates[file_name_first][self.vitez_dle_okrsku[file_name_first][kolo][obce_cislo][okrsek_cislo]]
                    # print '2018: ' + self.candidates[file_name_second][self.vitez_dle_okrsku[file_name_second][kolo][obce_cislo][okrsek_cislo]]
                    # print self.vitez_dle_okrsku[file_name_second][kolo][obce_cislo][okrsek_cislo]
                    if (self.vitez_dle_okrsku[file_name_first][kolo][obce_cislo][okrsek_cislo] == 9 and self.vitez_dle_okrsku[file_name_second][kolo][obce_cislo][okrsek_cislo] == 9):
                        #vyhral drahos a predtim schwarzenberg
                        self.pocty_dle_okrsku['drahos_schwarzenberg'] = self.pocty_dle_okrsku['drahos_schwarzenberg'] + 1
                    elif (self.vitez_dle_okrsku[file_name_first][kolo][obce_cislo][okrsek_cislo] == 6 and self.vitez_dle_okrsku[file_name_second][kolo][obce_cislo][okrsek_cislo] == 7):
                        # vyhral zeman a predtim zeman
                        self.pocty_dle_okrsku['zeman_zeman'] = self.pocty_dle_okrsku['zeman_zeman'] + 1
                    elif (self.vitez_dle_okrsku[file_name_first][kolo][obce_cislo][okrsek_cislo] == 9 and self.vitez_dle_okrsku[file_name_second][kolo][obce_cislo][okrsek_cislo] == 7):
                        #vyhral zeman a predtim schwarzenberg
                        self.pocty_dle_okrsku['zeman_schwarzenberg'] = self.pocty_dle_okrsku['zeman_schwarzenberg'] + 1
                    elif (self.vitez_dle_okrsku[file_name_first][kolo][obce_cislo][okrsek_cislo] == 6 and self.vitez_dle_okrsku[file_name_second][kolo][obce_cislo][okrsek_cislo] == 9):
                        # vyhral drahos a predtim zeman
                        self.pocty_dle_okrsku['drahos_zeman'] = self.pocty_dle_okrsku['drahos_zeman'] + 1
                    else:
                        #ostatni
                        self.pocty_dle_okrsku['ostatni'] = self.pocty_dle_okrsku['ostatni'] + 1

            except KeyError as e:
                print
                pass

        print 'Pocet okrsku, kde v roce 2018 vyhral Drahos a v roce 2013 Schwarzenberg: ' + str(self.pocty_dle_okrsku['drahos_schwarzenberg'])
        print 'Pocet okrsku, kde v roce 2018 vyhral Zeman a v roce 2013 Zeman: ' + str(self.pocty_dle_okrsku['zeman_zeman'])
        print 'Pocet okrsku, kde v roce 2018 vyhral Zeman a v roce 2013 Schwarzenberg: ' + str(self.pocty_dle_okrsku['zeman_schwarzenberg'])
        print 'Pocet okrsku, kde v roce 2018 vyhral Drahos a v roce 2013 Zeman: ' + str(self.pocty_dle_okrsku['drahos_zeman'])
        print 'Pocet okrsku, jinych kombinaci: ' + str(self.pocty_dle_okrsku['ostatni'])



    def get_vitez(self, file_name, kolo):
        self.vitez_dle_okrsku[file_name] = {}
        self.vitez_dle_okrsku[file_name][kolo] = {}
        self.obce_dva = []
        for k, obec in self.cities[file_name].items():
            # print obec
            self.obce_dva.append(k)
            self.vitez_dle_okrsku[file_name][kolo][k] = {}
            self.okrsky_v_obcich[k] = []
            for k2, okrsek in obec.items():
                # print okrsek
                self.okrsky_v_obcich[k].append(k2)
                hlasy = okrsek[unicode(kolo)]
                winner = 0
                max_hlas = 0
                for kandidat, hlas in hlasy.items():
                    if int(hlas) > max_hlas:
                        winner = int(kandidat)
                        max_hlas = int(hlas)
                self.vitez_dle_okrsku[file_name][kolo][k][k2] = winner


    def load_dict(self, file_name):
        # obj = untangle.parse('volby_prez_2013.xml')
        obj = untangle.parse(file_name + '.xml')
        self.cities[file_name] = {}

        self.pocet_okrsku[file_name] = {}
        self.pocet_okrsku[file_name][1] = 0
        self.pocet_okrsku[file_name][2] = 0
        for line in obj.PE_T1.PE_T1_ROW:
            pass
            hlasy = {}

            hlasy[1] = line.get_elements('HLASY_01')[0].cdata
            hlasy[2] = line.get_elements('HLASY_02')[0].cdata
            hlasy[3] = line.get_elements('HLASY_03')[0].cdata
            hlasy[4] = line.get_elements('HLASY_04')[0].cdata
            hlasy[5] = line.get_elements('HLASY_05')[0].cdata
            hlasy[6] = line.get_elements('HLASY_06')[0].cdata
            hlasy[7] = line.get_elements('HLASY_07')[0].cdata
            hlasy[8] = line.get_elements('HLASY_08')[0].cdata
            hlasy[9] = line.get_elements('HLASY_09')[0].cdata
            assert isinstance(line, untangle.Element)
            obec = line.get_elements('OBEC')[0].cdata
            okrsek = line.get_elements('OKRSEK')[0].cdata
            kolo = line.get_elements('KOLO')[0].cdata
            self.pocet_okrsku[file_name][int(kolo)] = self.pocet_okrsku[file_name][int(kolo)] + 1
            if (self.cities[file_name].get(obec, False)):
                if (self.cities[file_name][obec].get(okrsek, False)):
                    self.cities[file_name][obec][okrsek][kolo] = hlasy
                else:
                    self.cities[file_name][obec][okrsek] = {}
                    self.cities[file_name][obec][okrsek][kolo] = hlasy

            else:
                self.cities[file_name][obec] = {}
                self.cities[file_name][obec][okrsek] = {}

                self.cities[file_name][obec][okrsek][kolo] = hlasy


        print self.pocet_okrsku[file_name]

        return self.cities[file_name]


v = volby()