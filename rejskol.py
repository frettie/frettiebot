#!/usr/bin/python
# -*- coding: utf-8 -*-
import dateutil.parser
import re
from logger import Logger
import untangle
import pywikibot
import requests
import urllib.parse
from wikidatafun import *
import json

import unicodecsv
from pywikibot import pagegenerators, WikidataBot
from pywikibot.textlib import mwparserfromhell as parser


class excel_semicolon(unicodecsv.Dialect):
    """Describe the usual properties of Excel-generated CSV files."""
    delimiter = ';'
    quotechar = '"'
    doublequote = True
    skipinitialspace = False
    lineterminator = '\r\n'
    quoting = unicodecsv.QUOTE_MINIMAL


import sys


# reload(sys) # just to be sure
# sys.setdefaultencoding('utf-8')
# from math import radians, cos, sin, asin, sqrt

class rejskol():

    def __init__(self):
        self.errors = {}
        # self.csv = []
        # self.logger = Logger(u'rejskol' + u'Fill', 'saved')
        # self.loadXml()

        self.logger = Logger(u'rejskol' + u'Fill', 'saved')

        self.translator = {}

        self.ruian = {}
        self.streets = {}
        self.castiobce = {}
        self.mesta = {}
        self.skoly = {}

        self.icoobce = {}
        self.final = []

        self.site = pywikibot.Site('cs', 'wikipedia')
        self.repo = self.site.data_repository()
        self.load_translator()
        self.loadObce()
        self.loadCastiObce()
        self.loadUlice()
        self.loadSkoly()
        self.loadCSV()
        self.loadXml()

        self.processSchools()

        # self.checkExistZrizovatel()

        # self.loadIcoObce()
        # self.fillCityIco()


    def fillCityIco(self):
        query = '''
            select ?city ?lau where {
        ?city wdt:P782 [].
        
        ?city wdt:P17 wd:Q213.
        ?city wdt:P782 ?lau
                MINUS{?city wdt:P4156 []}

        }
            '''
        from pywikibot.data import sparql
        query_object = sparql.SparqlQuery()
        data = query_object.select(query, full_data=True)
        site = pywikibot.Site('wikidata', 'wikidata')
        repo = site.data_repository()
        source = pywikibot.Claim(repo, 'P248')
        source.setTarget(pywikibot.ItemPage(repo, 'Q8182488'))
        for lau in data:
            cityWd = lau['city'].getID()
            match = re.search(u'CZ([0-9]+)', str(lau['lau']))
            city = pywikibot.ItemPage(self.repo, cityWd)
            if (match):
                gr = match.groups()
                lauFinal = gr[0]
                try:
                    print(lauFinal)
                    ico = self.icoobce[lauFinal]
                    print(cityWd)



                    icwd = pywikibot.Claim(repo, 'P4156')
                    icwd.setTarget(ico)

                    city.addClaim(icwd)
                    icwd.addSource(source)
                except KeyError as k:
                    pass


    def checkExistZrizovatel(self):
        file = "zriz.csv"
        f = open(file, 'r')
        count = 0
        for line in f:
            splitted = line.split(';')
            url = "https://tools.wmflabs.org/hub/P4156:" + splitted[0] + "?lang=cs&format=json"
            response = requests.get(url)
            jsontext = response.text
            data = json.loads(jsontext)
            try:
                mess = data['message']
                print('neexistuje ic: ' + splitted[0] + ' nazev: ' + splitted[1])
            except pywikibot.exceptions.OtherPageSaveError:
                pass
            except KeyError:
                pass
            except json.decoder.JSONDecodeError:
                pass
            except BaseException:
                pass

    def loadIcoObce(self):
        file = "icoobce.csv"
        f = open(file, 'r')
        count = 0
        for line in f:
            splitted = line.split(';')
            self.icoobce[splitted[1]] = splitted[2].strip()

        print(self.icoobce)



    def loadUlice(self):
        file = "ulice.csv"
        f = open(file, 'r')
        count = 0
        for line in f:
            splitted = line.split(',')
            match = re.search(u'http://www.wikidata.org/entity/(Q[0-9]+)', splitted[0])
            if (match):
                gr = match.groups()
                streetWd = gr[0]
                street = pywikibot.ItemPage(self.repo, streetWd)
                ruian = int(splitted[1].strip())
                self.streets[ruian] = street

    def loadSkoly(self):
        file = "skoly.csv"
        f = open(file, 'r')
        count = 0
        for line in f:
            splitted = line.split(',')
            match = re.search(u'http://www.wikidata.org/entity/(Q[0-9]+)', splitted[0])
            if (match):
                gr = match.groups()
                skolyWd = gr[0]
                skoly = pywikibot.ItemPage(self.repo, skolyWd)
                ruian = int(splitted[1].strip())
                self.skoly[ruian] = skoly

    def loadCastiObce(self):
        file = "castiobce.csv"
        f = open(file, 'r')
        count = 0
        for line in f:
            splitted = line.split(',')
            match = re.search(u'http://www.wikidata.org/entity/(Q[0-9]+)', splitted[0])
            if (match):
                gr = match.groups()
                castobceWd = gr[0]
                castobce = pywikibot.ItemPage(self.repo, castobceWd)
                ruian = int(splitted[1].strip())
                self.castiobce[ruian] = castobce

    def loadObce(self):
        file = "mesta.csv"
        f = open(file, 'r')
        count = 0
        for line in f:
            splitted = line.split(',')
            match = re.search(u'http://www.wikidata.org/entity/(Q[0-9]+)', splitted[0])
            if (match):
                gr = match.groups()
                mestoWd = gr[0]
                mesto = pywikibot.ItemPage(self.repo, mestoWd)


                match2 = re.search(u'CZ([0-9]+)', splitted[1])
                if match2:
                    gr = match2.groups()
                    ruian = int(gr[0])

                    self.mesta[ruian] = mesto

    def loadCSV(self):
        file = "finalclean.csv"
        f = open(file, 'r')
        count = 0
        for line in f:
            splitted = line.split(';')
            arr = {}
            if (count > 0):
                if (splitted[0] == ''):
                    arr['KodAdm'] = 0
                else:
                    arr['KodAdm'] = int(splitted[0])

                if (splitted[1] == ''):
                    arr['KodObce'] = 0
                else:
                    arr['KodObce'] = int(splitted[1])

                if (splitted[2] == ''):
                    arr['KodMOMC'] = 0
                else:
                    arr['KodMOMC'] = int(splitted[2])

                if (splitted[3] == ''):
                    arr['KodCastiObce'] = 0
                else:
                    arr['KodCastiObce'] = int(splitted[3])

                if (splitted[4] == ''):
                    arr['KodUlice'] = 0
                else:
                    arr['KodUlice'] = int(splitted[4])

                if (splitted[5] == ''):
                    arr['CisloDomovni'] = 0
                else:
                    arr['CisloDomovni'] = int(splitted[5])

                if (splitted[6] == ''):
                    arr['CisloOrientacni'] = 0
                else:
                    arr['CisloOrientacni'] = int(splitted[6])

                if (splitted[7] == ''):
                    arr['PSC'] = 0
                else:
                    arr['PSC'] = int(splitted[7].strip())


                self.ruian[int(splitted[0])] = arr

            count = count + 1


        pass

    def loadXml(self):
        file = untangle.parse('vrejcelk.xml')
        skolaPlnyNazev = []

        materskaSkola = pywikibot.ItemPage(self.repo, 'Q126807')
        zakladniSkola = pywikibot.ItemPage(self.repo, 'Q9842')
        zus = pywikibot.ItemPage(self.repo, 'Q10861807')
        stredniskola = pywikibot.ItemPage(self.repo, 'Q159334')
        konzervator = pywikibot.ItemPage(self.repo, 'Q184644')
        vos = pywikibot.ItemPage(self.repo, 'Q9369203')
        jazykovaskola = pywikibot.ItemPage(self.repo, 'Q61439966')
        strediskopraktickehovycviku = pywikibot.ItemPage(self.repo, 'Q61439974')
        plaveckaskola = pywikibot.ItemPage(self.repo, 'Q61439978')
        skolavprirode = pywikibot.ItemPage(self.repo, 'Q19794100')

        for x in file.ExportDat.PravniSubjekt:
            skola = {}
            skola['redizo'] = x.RedIzo.cdata
            skola['ico'] = x.ICO.cdata
            skola['plnyNazev'] = x.Reditelstvi.RedPlnyNazev.cdata
            skola['nazev'] = x.Reditelstvi.RedZkracenyNazev.cdata
            skola['RUIANKOD'] = x.Reditelstvi.RedRUAINKod.cdata

            if (skola['RUIANKOD'] != ''):
                if (int(skola['RUIANKOD']) in self.ruian):

                    skola['kodUlice'] = self.ruian[int(skola['RUIANKOD'])]['KodUlice']
                    skola['cisloDomovni'] = self.ruian[int(skola['RUIANKOD'])]['CisloDomovni']
                    skola['cisloOrientacni'] = self.ruian[int(skola['RUIANKOD'])]['CisloOrientacni']
                    skola['PSCRuian'] = self.ruian[int(skola['RUIANKOD'])]['PSC']
                    skola['kodCastiObce'] = self.ruian[int(skola['RUIANKOD'])]['KodCastiObce']
                    skola['kodMOMC'] = self.ruian[int(skola['RUIANKOD'])]['KodMOMC']
                    skola['kodObce'] = self.ruian[int(skola['RUIANKOD'])]['KodObce']

                    if (skola['kodObce'] == 0):
                        skola['obec'] = None
                    else:
                        skola['obec'] = self.mesta[skola['kodObce']]

                    if skola['kodCastiObce'] == 0:
                        skola['castObce'] = None
                    else:
                        skola['castObce'] = self.castiobce[skola['kodCastiObce']]

                    if (skola['kodUlice'] == 0):
                        skola['ulice'] = None
                    else:
                        skola['ulice'] = self.streets[skola['kodUlice']]
                else:
                    skola['kodUlice'] = 0
                    skola['cisloDomovni'] = 0
                    skola['cisloOrientacni'] = 0
                    skola['PSCRuian'] = 0
                    skola['PSCRuian'] = 0
                    skola['kodCastiObce'] = 0
                    skola['kodMOMC'] = 0
                    skola['kodObce'] = 0
            skola['adresa'] = x.Reditelstvi.RedAdresa1.cdata
            skola['mesto'] = x.Reditelstvi.RedAdresa2.cdata
            skola['psc'] = x.Reditelstvi.RedAdresa3.cdata
            skola['pravniForma'] = x.Reditelstvi.PravniForma.cdata
            skola['druhZrizovatele'] = x.Reditelstvi.DruhZrizovatele.cdata
            skola['email'] = x.Reditelstvi.Email1.cdata
            skola['email2'] = x.Reditelstvi.Email2.cdata

            instances = []
            dates = {}
            for skolyzarizeni in x.SkolyZarizeni.SkolaZarizeni:

                if hasattr(skolyzarizeni, 'SkolaPlnyNazev'):
                    plnynazev = skolyzarizeni.SkolaPlnyNazev.cdata
                    datumZalozeni = skolyzarizeni.SkolaDatumZahajeni.cdata
                    dt = dateutil.parser.parse(datumZalozeni)
                    wbdt = pywikibot.WbTime(dt.year, dt.month, dt.day)
                    if plnynazev == 'Mateřská škola':
                        #1996-05-23T00:00:00
                        date = {
                            'instance' : materskaSkola,
                            'date' : wbdt
                        }
                        instances.append(materskaSkola)
                        dates[plnynazev] = date
                    if plnynazev == 'Základní škola':
                        date = {
                            'instance': zakladniSkola,
                            'date': wbdt
                        }
                        instances.append(zakladniSkola)
                        dates[plnynazev] = date
                    if plnynazev == 'Základní umělecká škola':
                        date = {
                            'instance': zus,
                            'date': wbdt
                        }
                        instances.append(zus)
                        dates[plnynazev] = date
                    if plnynazev == 'Střední škola':
                        date = {
                            'instance': stredniskola,
                            'date': wbdt
                        }
                        instances.append(stredniskola)
                        dates[plnynazev] = date
                    if plnynazev == 'Konzervatoř':
                        date = {
                            'instance': konzervator,
                            'date': wbdt
                        }
                        instances.append(konzervator)
                        dates[plnynazev] = date
                    if plnynazev == 'Vyšší odborná škola':
                        date = {
                            'instance': vos,
                            'date': wbdt
                        }
                        instances.append(vos)
                        dates[plnynazev] = date
                    if plnynazev == 'Jazyková škola s právem státní jazykové zkoušky':
                        date = {
                            'instance': jazykovaskola,
                            'date': wbdt
                        }
                        instances.append(jazykovaskola)
                        dates[plnynazev] = date
                    if plnynazev == 'Středisko praktického vyučování':
                        date = {
                            'instance': strediskopraktickehovycviku,
                            'date': wbdt
                        }
                        instances.append(strediskopraktickehovycviku)
                        dates[plnynazev] = date
                    if plnynazev == 'Plavecká škola':
                        date = {
                            'instance': plaveckaskola,
                            'date': wbdt
                        }
                        instances.append(plaveckaskola)
                        dates[plnynazev] = date
                    if plnynazev == 'Škola v přírodě':
                        date = {
                            'instance': skolavprirode,
                            'date': wbdt
                        }
                        instances.append(skolavprirode)
                        dates[plnynazev] = date
                    if plnynazev == 'Mateřská škola (lesní mateřská škola)':
                        date = {
                            'instance': materskaSkola,
                            'date': wbdt
                        }
                        instances.append(materskaSkola)
                        dates[plnynazev] = date
                    if plnynazev == 'Mateřská škola - Przedszkole, Vendryně 1':
                        date = {
                            'instance': materskaSkola,
                            'date': wbdt
                        }
                        instances.append(materskaSkola)
                        dates[plnynazev] = date
                    if plnynazev == 'Mateřská škola - Przedszkole':
                        date = {
                            'instance': materskaSkola,
                            'date': wbdt
                        }
                        instances.append(materskaSkola)
                        dates[plnynazev] = date


                    skolaPlnyNazev.append(plnynazev)
            skola['instances'] = instances
            skola['dates'] = dates


            kod_forma = skola['pravniForma']
            forma_wd = ''
            forma_wd = None
            if kod_forma == '121':
                # akciova
                forma_wd = 'Q3742494'

            if kod_forma == '112':
                # Sro
                forma_wd = 'Q15646299'

            if kod_forma == '113':
                # komanditni
                forma_wd = 'Q43751707'

            if kod_forma == '111':
                # vos
                forma_wd = 'Q646164'

            if kod_forma == '706':
                # spolek
                forma_wd = 'Q48204'

            if kod_forma == '932':
                # evropska spol
                forma_wd = 'Q279014'

            if kod_forma == '801':
                forma_wd = 'Q56440793'

            if kod_forma == '804':
                forma_wd = 'Q56440892'

            if kod_forma == '741':
                forma_wd = 'Q56440908'

            if kod_forma == '331':
                forma_wd = 'Q7257282'

            if kod_forma == '641':
                forma_wd = 'Q56447357'

            if kod_forma == '141':
                forma_wd = 'Q12041908'

            if kod_forma == '601':
                forma_wd = 'Q56447581'

            if kod_forma == '161':
                forma_wd = 'Q56457912'

            if kod_forma == '325':
                forma_wd = 'Q12042877'

            if kod_forma == '707':
                forma_wd = 'Q56458713'

            if kod_forma == '921':
                forma_wd = 'Q56466055'

            if kod_forma == '736':
                forma_wd = 'Q56466930'

            if kod_forma == '703':
                forma_wd = 'Q56467825'

            if kod_forma == '723':
                forma_wd = 'Q56469198'

            if kod_forma == '205':
                forma_wd = 'Q4539'

            if kod_forma == '101':
                forma_wd = 'Q56475875'

            if kod_forma == '102':
                forma_wd = 'Q58517666'

            if kod_forma == '751':
                forma_wd = 'Q10861788'

            if kod_forma == '301':
                forma_wd = 'Q16917889'

            if kod_forma == '722':
                forma_wd = 'Q56487597'

            if kod_forma == '721':
                forma_wd = 'Q58515694'

            if kod_forma == '421':
                forma_wd = 'Q56517350'

            if kod_forma == '661':
                forma_wd = 'Q11863217'

            if kod_forma == '117':
                forma_wd = 'Q157031'

            if kod_forma == '999':
                forma_wd = 'Q56538688'

            if kod_forma == '391':
                forma_wd = 'Q563787'

            if kod_forma == '381':
                forma_wd = 'Q58219154'

            if kod_forma == '805':
                forma_wd = 'Q58219166'

            if kod_forma == '771':
                forma_wd = 'Q12057479'

            if forma_wd is not None:
                skola['pravniFormaItem'] = pywikibot.ItemPage(self.repo, forma_wd)
            else:
                skola['pravniFormaItem'] = None

            zrizovatele = []
            for zrizovatel in x.Reditelstvi.Zrizovatele:
                zrizovatelData = {}
                if hasattr(zrizovatel, 'Zrizovatel'):


                    # if 'ZrizOsoba' in zrizovatel.Zrizovatel:
                    #     zrizovatelData['typOsoby'] = zrizovatel.Zrizovatel.ZrizOsoba.cdata
                    # else:
                    #     zrizovatelData['typOsoby'] = 'P'
                    # zrizovatelData['pravniForma'] = zrizovatel.Zrizovatel.ZrizPravniForma.cdata
                    if hasattr(zrizovatel.Zrizovatel, 'ZrizICO'):
                        zrizovatelData['ico'] = zrizovatel.Zrizovatel.ZrizICO.cdata
                    else:
                        zrizovatelData['ico'] = ''

                    if hasattr(zrizovatel.Zrizovatel, 'ZrizNazev'):
                        zrizovatelData['nazev'] = zrizovatel.Zrizovatel.ZrizNazev.cdata
                    else:
                        zrizovatelData['nazev'] = ''

                    # zrizovatelData['mesto'] = zrizovatel.Zrizovatel.ZrizAdresa2.cdata
                    # zrizovatelData['psc'] = zrizovatel.Zrizovatel.ZrizAdresa3.cdata
                    # zrizovatelData['email'] = zrizovatel.Zrizovatel.ZrizEmail1.cdata
                    zrizovatele.append(zrizovatelData)
            skola['zrizovatele'] = zrizovatele
            # print(skola)
            self.final.append(skola)
        print(skolaPlnyNazev)

    def processSchools(self):
        print('eano')
        zrizovatele = {}

        sources = []
        source = pywikibot.Claim(self.repo, 'P248')
        source.setTarget(pywikibot.ItemPage(self.repo, 'Q61376331'))

        import datetime
        # dt = datetime.datetime(2019, 1, 31, 0, 0, 0)
        dt = pywikibot.WbTime(2019, 1, 31)

        source2 = pywikibot.Claim(self.repo, 'P813')
        source2.setTarget(dt)

        sources.append(source)
        sources.append(source2)

        for skola in self.final:

            if not self.logger.isCompleteFile(skola['ico']):

                if int(skola['ico']) in self.skoly:
                    # print(skola['zrizovatele'])
                    # skolaItem = self.skoly[int(skola['ico'])]

                    # redizoClaim = pywikibot.Claim(self.repo, 'P6370')
                    # redizoClaim.setTarget(skola['redizo'])




                    # skolaItem.addClaim(redizoClaim)

                    # redizoClaim.addSources(sources)

                    # vroli = pywikibot.Claim(self.repo, 'P2868')
                    # zrizovatelrole = pywikibot.ItemPage(self.repo, 'Q61268336')
                    # vroli.setTarget(zrizovatelrole)


                    for zriz in skola['zrizovatele']:
                        if (zriz['ico'] != ''):
                            pass
                            # zrizovatele[zriz['ico']] = zriz['nazev']
                            #
                            # url = "https://tools.wmflabs.org/hub/P4156:" + zriz['ico'] + "?lang=cs&format=json"
                            # response = requests.get(url)
                            # jsontext = response.text
                            # data = json.loads(jsontext)
                            # # data['origin']['qid']
                            # try:
                            #     qidzriz = data['origin']['qid']
                            #     zrizitem = pywikibot.ItemPage(self.repo, qidzriz)
                            #     zrizovatelClaim = pywikibot.Claim(self.repo, 'P749')
                            #     zrizovatelClaim.setTarget(zrizitem)
                            #
                            #
                            #
                            #     skolaItem.addClaim(zrizovatelClaim)
                            #     zrizovatelClaim.addSources(sources)
                            #     zrizovatelClaim.addQualifier(qualifier=vroli)
                            #     # print('neexistuje ic: ' + splitted[0] + ' nazev: ' + splitted[1])
                            # except pywikibot.exceptions.OtherPageSaveError:
                            #     pass
                            # except KeyError:
                            #     pass
                            # except json.decoder.JSONDecodeError:
                            #     pass
                            # except BaseException as e:
                            #     print(e)
                            #     pass
                        else:
                            pass

                    # else:
                    #     print(skola['zrizovatele'])
                else:
                    # print(skola['zrizovatele'])
                    # skolaItem = self.skoly[int(skola['ico'])]

                    labelsAndDescriptions = {}

                    labelsAndDescriptions.setdefault('labels', {}).update({
                        'en': {
                            'language': 'en',
                            'value': skola['nazev']
                        },
                        'cs': {
                            'language': 'cs',
                            'value': skola['nazev']
                        },
                    })

                    try:
                        skola['obec'].get()
                        city_name = skola['obec'].labels['cs']
                        city_name_cs = self.translator.get(city_name, city_name)
                    except KeyError as e:
                        city_name = 'Czech Republic'
                        city_name_cs = 'Česku'

                    labelsAndDescriptions.setdefault('descriptions', {}).update({
                        'en': {
                            'language': 'en',
                            'value': 'school in ' + city_name
                        },
                        'cs': {
                            'language': 'cs',
                            'value': 'škola v ' + city_name_cs
                        },
                    })

                    labelsAndDescriptions.setdefault('aliases', {}).update({
                        'en': [{
                            'language': 'en',
                            'value': skola['plnyNazev']
                        }],
                        'cs': [{
                            'language': 'cs',
                            'value': skola['plnyNazev']
                        }],
                    })

                    try:
                        skolaItem = self.create_item_for_page(page=None, data=labelsAndDescriptions, repo=self.repo,
                                                         summary=u'Creating Czech schools')
                    except pywikibot.OtherPageSaveError as e:
                        # idtext = e.reason.other['messages'][0]['parameters'][2]
                        # match = re.search(u'\[\[(Q[0-9]+)\|Q[0-9]+\]\]', idtext)
                        # if (match):
                        #     gr = match.groups()
                        #     idstranky = gr[0]
                        # skolaItem = pywikibot.ItemPage(self.repo, idstranky)

                        labelsAndDescriptions = {}

                        labelsAndDescriptions.setdefault('labels', {}).update({
                            'en': {
                                'language': 'en',
                                'value': skola['plnyNazev']
                            },
                            'cs': {
                                'language': 'cs',
                                'value': skola['plnyNazev']
                            },
                        })

                        try:
                            skola['obec'].get()
                            city_name = skola['obec'].labels['cs']
                            city_name_cs = self.translator.get(city_name, city_name)
                        except KeyError as e:
                            city_name = 'Czech Republic'
                            city_name_cs = 'Česku'

                        labelsAndDescriptions.setdefault('descriptions', {}).update({
                            'en': {
                                'language': 'en',
                                'value': 'school in ' + city_name
                            },
                            'cs': {
                                'language': 'cs',
                                'value': 'škola v ' + city_name_cs
                            },
                        })

                        labelsAndDescriptions.setdefault('aliases', {}).update({
                            'en': [{
                                'language': 'en',
                                'value': skola['plnyNazev']
                            }],
                            'cs': [{
                                'language': 'cs',
                                'value': skola['plnyNazev']
                            }],
                        })
                        skolaItem = self.create_item_for_page(page=None, data=labelsAndDescriptions, repo=self.repo,
                                                              summary=u'Creating Czech schools')
                        self.logger.logError(skola['ico'])



                    instanceClaim = pywikibot.Claim(self.repo, 'P31')
                    for instance in skola['instances']:
                        instanceClaim.setTarget(instance)
                        skolaItem.addClaim(instanceClaim)
                        instanceClaim.addSources(sources)

                    datumZalozeniClaim = pywikibot.Claim(self.repo, 'P571')
                    for datezalozeni in skola['dates'].items():
                        datumTarget = datezalozeni[1]['date']
                        datumZalozeniClaim.setTarget(datumTarget)
                        skolaItem.addClaim(datumZalozeniClaim)
                        qualifier = pywikibot.Claim(self.repo, 'P518')
                        qualifier.setTarget(datezalozeni[1]['instance'])
                        datumZalozeniClaim.addQualifier(qualifier)
                        datumZalozeniClaim.addSources(sources)

                    redizoClaim = pywikibot.Claim(self.repo, 'P6370')
                    redizoClaim.setTarget(skola['redizo'])



                    icoClaim = pywikibot.Claim(self.repo, 'P4156')
                    icoClaim.setTarget(skola['ico'])

                    oficialniNazevClaim = pywikibot.Claim(self.repo, 'P1448')
                    multilang = pywikibot.WbMonolingualText(skola['plnyNazev'], 'cs')
                    oficialniNazevClaim.setTarget(multilang)

                    cesko = pywikibot.ItemPage(self.repo, 'Q213')

                    statClaim = pywikibot.Claim(self.repo, 'P17')
                    statClaim.setTarget(cesko)

                    try:
                        mestoClaim = pywikibot.Claim(self.repo, 'P131')
                        mestoClaim.setTarget(skola['obec'])
                        skolaItem.addClaim(mestoClaim)
                        mestoClaim.addSources(sources)
                    except KeyError as e:
                        self.logger.logError(skola['ico'] + ' problem s obci')
                        pass

                    try:
                        sidloReditelstvi = pywikibot.Claim(self.repo, 'P159')
                        sidloReditelstvi.setTarget(skola['obec'])
                        skolaItem.addClaim(sidloReditelstvi)
                        sidloReditelstvi.addSources(sources)

                        if skola['PSCRuian'] is not None:
                            psc = pywikibot.Claim(self.repo, 'P281')
                            psc.setTarget(str(skola['PSCRuian']))
                            sidloReditelstvi.addQualifier(psc)

                        if skola['ulice'] is not None:
                            ulice = pywikibot.Claim(self.repo, 'P669')
                            ulice.setTarget(skola['ulice'])
                            sidloReditelstvi.addQualifier(ulice)

                        if skola['cisloDomovni'] is not None:
                            cp = pywikibot.Claim(self.repo, 'P4856')
                            cp.setTarget(str(skola['cisloDomovni']))
                            sidloReditelstvi.addQualifier(cp)

                        if skola['cisloOrientacni'] is not None:
                            co = pywikibot.Claim(self.repo, 'P670')
                            co.setTarget(str(skola['cisloOrientacni']))
                            sidloReditelstvi.addQualifier(co)
                    except KeyError as e:
                        pass

                    skolaItem.addClaim(redizoClaim)
                    skolaItem.addClaim(oficialniNazevClaim)
                    skolaItem.addClaim(statClaim)

                    skolaItem.addClaim(icoClaim)


                    redizoClaim.addSources(sources)
                    oficialniNazevClaim.addSources(sources)
                    statClaim.addSources(sources)

                    icoClaim.addSources(sources)

                    if skola['pravniFormaItem'] is not None:
                        pravniFormaClaim = pywikibot.Claim(self.repo, 'P1454')
                        pravniFormaClaim.setTarget(skola['pravniFormaItem'])
                        skolaItem.addClaim(pravniFormaClaim)
                        pravniFormaClaim.addSources(sources)



                    vroli = pywikibot.Claim(self.repo, 'P2868')
                    zrizovatelrole = pywikibot.ItemPage(self.repo, 'Q61268336')
                    vroli.setTarget(zrizovatelrole)
                    for zriz in skola['zrizovatele']:
                        if (zriz['ico'] != ''):
                            url = "https://tools.wmflabs.org/hub/P4156:" + zriz['ico'] + "?lang=cs&format=json"
                            response = requests.get(url)
                            jsontext = response.text
                            data = json.loads(jsontext)
                            # data['origin']['qid']
                            try:
                                qidzriz = data['origin']['qid']

                                zrizitem = pywikibot.ItemPage(self.repo, qidzriz)
                                zrizovatelClaim = pywikibot.Claim(self.repo, 'P749')
                                zrizovatelClaim.setTarget(zrizitem)

                                skolaItem.addClaim(zrizovatelClaim)

                                zrizovatelClaim.addSources(sources)
                                zrizovatelClaim.addQualifier(qualifier=vroli)
                                # print('neexistuje ic: ' + splitted[0] + ' nazev: ' + splitted[1])
                            except pywikibot.exceptions.OtherPageSaveError:
                                pass
                            except KeyError:
                                pass
                            except json.decoder.JSONDecodeError:
                                pass
                            except BaseException as e:
                                print(e)
                                pass
                    self.logger.logComplete(str(skola['ico']))
            else:
                pass
        # for zrizovatel in zrizovatele.items():
        #     print(zrizovatel)

    def new_streets(self):
        self.logger = Logger(u'rejskol' + u'Fill', 'saved')
        self.site = pywikibot.Site('cs', 'wikipedia')
        self.repo = self.site.data_repository()



        iternum = 0
        for street in self.streets_from_csv:
            # Header
            if iternum == 0:
                iternum = iternum + 1
                continue
            # faster skip lines
            if iternum < 82300:
                iternum = iternum + 1
                continue
            ruian = street[0]  # ruian
            if (not self.logger.isCompleteFile(ruian)):
                # print street[1] #name
                # print street[2] #city_lau
                city_lau = street[2]
                city_item = cities['CZ' + city_lau]

                city_name = city_item.text[u'labels'][u'cs']

                street_name = street[1]
                plati_do = street[4]

                regex = u"náměstí"

                match = re.search(regex, street_name, re.IGNORECASE)
                # match = re.search('[Nn]áměstí', street_name)
                if not match:
                    regex = u"nám."

                    match = re.search(regex, street_name, re.IGNORECASE)
                    if not match:
                        typeOfInstance = 'street'
                    else:
                        typeOfInstance = 'square'
                else:
                    typeOfInstance = 'square'

                sources = []

                source = pywikibot.Claim(self.repo, 'P248')
                source.setTarget(pywikibot.ItemPage(self.repo, 'Q12049125'))

                instance = pywikibot.Claim(self.repo, 'P31')
                if typeOfInstance == 'street':
                    instance.setTarget(pywikibot.ItemPage(self.repo, 'Q79007'))
                else:
                    instance.setTarget(pywikibot.ItemPage(self.repo, 'Q174782'))

                if plati_do != '':
                    instance.setTarget(pywikibot.ItemPage(self.repo, 'Q15893266'))

                country = pywikibot.Claim(self.repo, 'P17')
                country.setTarget(pywikibot.ItemPage(self.repo, 'Q213'))

                admin = pywikibot.Claim(self.repo, 'P131')
                admin.setTarget(city_item)

                try:
                    coords = pywikibot.Claim(self.repo, 'P625')

                    payload = {'query': street_name + ', ' + city_name}
                    r = requests.get(u'https://api.mapy.cz/geocode', params=payload)
                    xml = r.text
                    data = untangle.parse(xml)

                    # print xml
                    # print data
                    # sys.exit()
                    if isinstance(data.result.point.item, list):
                        coordsElem = data.result.point.item[0]
                    else:
                        coordsElem = data.result.point.item
                    assert isinstance(coordsElem, untangle.Element)
                    line = {}
                    lat = coordsElem.get_attribute(u'y')
                    lon = coordsElem.get_attribute(u'x')
                    coord = pywikibot.Coordinate(float(lat), float(lon), precision=1e-07, globe='earth', site=self.repo)
                    coords.setTarget(coord)
                except AttributeError as e:
                    self.logger.logError(street_name)
                    self.logger.logError(u'coord problems')
                    coords = None

                try:
                    code = pywikibot.Claim(self.repo, 'P4533')
                    code.setTarget(ruian)

                    sources.append(source)
                except:
                    self.logger.logError(street_name)
                    self.logger.logError(self.city_name)
                    self.logger.logError(u'street code problem')
                    continue

                data = {}

                data.setdefault('labels', {}).update({
                    'en': {
                        'language': 'en',
                        'value': street_name
                    },
                    'cs': {
                        'language': 'cs',
                        'value': street_name
                    },
                })

                city_name_cs = self.translator.get(city_name, city_name)

                if typeOfInstance == 'street':

                    data.setdefault('descriptions', {}).update({
                        'en': {
                            'language': 'en',
                            'value': 'street in ' + city_name
                        },
                        'cs': {
                            'language': 'cs',
                            'value': 'ulice v ' + city_name_cs
                        },
                    })
                else:
                    data.setdefault('descriptions', {}).update({
                        'en': {
                            'language': 'en',
                            'value': 'square in ' + city_name
                        },
                        'cs': {
                            'language': 'cs',
                            'value': 'náměstí v ' + city_name_cs
                        },
                    })
                try:
                    item = self.create_item_for_page(page=None, data=data, repo=self.repo,
                                                     summary=u'Creating czech street item')
                except pywikibot.OtherPageSaveError as e:
                    self.logger.logError(street_name)

                claimsStreet = item.get()
                if (not claimsStreet[u'claims'].get(u'P31', False)):
                    item.addClaim(instance)
                if (not claimsStreet[u'claims'].get(u'P17', False)):
                    item.addClaim(country)
                if (not claimsStreet[u'claims'].get(u'P131', False)):
                    item.addClaim(admin)
                if (not claimsStreet[u'claims'].get(u'P625', False)):
                    if coords:
                        item.addClaim(coords)
                if (not claimsStreet[u'claims'].get(u'P4533', False)):
                    if code:
                        item.addClaim(code)
                        code.addSources(sources)

                self.logger.logComplete(ruian)





    def create_item_for_page(self, page=None, data=None, summary=None, repo=None):
        if not summary:
            summary = (u'Bot: New item with sitelink from %s'
                       % page.title(asLink=True, insite=self.repo))

        if data is None:
            data = {}
            data.setdefault('sitelinks', {}).update({
                page.site.dbName(): {
                    'site': page.site.dbName(),
                    'title': page.title()
                }
            })
            data.setdefault('labels', {}).update({
                page.site.lang: {
                    'language': page.site.lang,
                    'value': page.title()
                }
            })

        if (not page):
            pywikibot.output('Creating item')
            item = pywikibot.ItemPage(repo)
        else:
            pywikibot.output('Creating item for %s...' % page)
            item = pywikibot.ItemPage(page.site.data_repository())

        result = self.user_edit_entity(item, data, summary=summary)
        if result:
            return item
        else:
            return None

    def user_edit_entity(self, item, data=None, summary=None):
        """
        Edit entity with data provided, with user confirmation as required.

        @param item: page to be edited
        @type item: ItemPage
        @param data: data to be saved, or None if the diff should be created
          automatically
        @kwarg summary: revision comment, passed to ItemPage.editEntity
        @type summary: str
        @kwarg show_diff: show changes between oldtext and newtext (default:
          True)
        @type show_diff: bool
        @kwarg ignore_server_errors: if True, server errors will be reported
          and ignored (default: False)
        @type ignore_server_errors: bool
        @kwarg ignore_save_related_errors: if True, errors related to
          page save will be reported and ignored (default: False)
        @type ignore_save_related_errors: bool
        @return: whether the item was saved successfully
        @rtype: bool
        """
        return self._save_page(item, item.editEntity, data)

    def _save_page(self, page, func, *args):
        """
        Helper function to handle page save-related option error handling.

        @param page: currently edited page
        @param func: the function to call
        @param args: passed to the function
        @param kwargs: passed to the function
        @kwarg ignore_server_errors: if True, server errors will be reported
          and ignored (default: False)
        @kwtype ignore_server_errors: bool
        @kwarg ignore_save_related_errors: if True, errors related to
        page save will be reported and ignored (default: False)
        @kwtype ignore_save_related_errors: bool
        @return: whether the page was saved successfully
        @rtype: bool
        """
        func(*args)
        return True

    def run(self):
        pass

    def load_translator(self):
        self.translator[u'Rovensko pod Troskami'] = 'Rovensku pod Troskami'
        self.translator[u'Lipník nad Bečvou'] = 'Lipníku nad Bečvou'
        self.translator[u'Seč (okres Chrudim)'] = 'Seči'
        self.translator[u'Choltice'] = 'Cholticích'
        self.translator[u'Bystré (okres Svitavy)'] = 'Bystré'
        self.translator[u'Dobřany'] = 'Dobřanech'
        self.translator[u'Netolice'] = 'Netolicích'
        self.translator[u'Boskovice'] = 'Boskovicích'
        self.translator[u'Golčův Jeníkov'] = 'Golčově Jeníkově'
        self.translator[u'Uherský Ostroh'] = 'Uherském Ostrohu'
        self.translator[u'Brumov-Bylnice'] = 'Brumově'
        self.translator[u'Březno (okres Chomutov)'] = 'Březně'
        #
        self.translator[u'Soběslav'] = 'Soběslavi'
        self.translator[u'Studénka'] = 'Studénce'
        self.translator[u'Tachov'] = 'Tachově'
        self.translator[u'Trutnov'] = 'Trutnově'
        self.translator[u'Uherské Hradiště'] = 'Uherském Hradišti'
        self.translator[u'Znojmo'] = 'Znojmě'
        self.translator[u'Žďár nad Sázavou'] = 'Žďáru nad Sázavou'
        self.translator[u'Louny'] = 'Lounech'
        self.translator[u'Kadaň'] = 'Kadani'
        self.translator[u'Cheb'] = 'Chebu'
        self.translator[u'České Budějovice'] = 'Českých Budějovicích'
        self.translator[u'Vimperk'] = 'Vimperku'
        self.translator[u'Brno'] = 'Brně'
        self.translator[u'Praha'] = 'Praze'
        self.translator[u'Opava'] = 'Opavě'
        self.translator[u'Teplice'] = 'Teplicích'
        self.translator[u'Karlovy Vary'] = 'Karlových Varech'
        self.translator[u'Praha'] = 'Praze'
        self.translator[u'Teplice'] = 'Teplicích'
        self.translator[u'Praha'] = 'Praze'
        self.translator[u'Praha'] = 'Praze'
        self.translator[u'Brno'] = 'Brně'

        #
        self.translator[u'Louňovice pod Blaníkem'] = 'Louňovicích pod Blaníkem'
        self.translator[u'Pyšely'] = 'Pyšelech'
        #
        self.translator[u'Sázava (okres Benešov)'] = 'Sázavě'
        self.translator[u'Trhový Štěpánov'] = 'Trhovém Štěpánově'
        self.translator[u'Hostomice (okres Beroun)'] = 'Hostomicích'
        self.translator[u'Hořovice'] = 'Hořovicích'
        self.translator[u'Komárov (okres Beroun)'] = 'Komárově'
        self.translator[u'Králův Dvůr'] = 'Králově Dvoře'
        self.translator[u'Liteň'] = 'Litni'
        self.translator[u'Srbsko (okres Beroun)'] = 'Srbsku'
        self.translator[u'Zdice'] = 'Zdicích'
        self.translator[u'Buštěhrad'] = 'Buštěhradě'
        self.translator[u'Lány (okres Kladno)'] = 'Lány'
        self.translator[u'Stochov'] = 'Stochově'
        self.translator[u'Unhošť'] = 'Unhošti'
        self.translator[u'Velvary'] = 'Velvarech'
        self.translator[u'Zlonice'] = 'Zlonicích'
        self.translator[u'Zásmuky'] = 'Zásmukách'
        self.translator[u'Malešov'] = 'Malešově'
        self.translator[u'Nové Dvory (okres Kutná Hora)'] = 'Nových Dvorech'
        self.translator[u'Uhlířské Janovice'] = 'Uhlířských Janovicích'
        self.translator[u'Kostelec nad Labem'] = 'Kostelci nad Labem'
        self.translator[u'Mšeno'] = 'Mšeně'
        self.translator[u'Všetaty (okres Mělník)'] = 'Všetatech'
        self.translator[u'Veltrusy'] = 'Veltrusech'
        self.translator[u'Bakov nad Jizerou'] = 'Bakově nad Jizerou'
        self.translator[u'Benátky nad Jizerou'] = 'Benátkách nad Jizerou'
        self.translator[u'Katusice'] = 'Katusicích'
        self.translator[u'Mnichovo Hradiště'] = 'Mnichově Hradišti'
        self.translator[u'Rožďalovice'] = 'Rožďalovicích'
        self.translator[u'Sadská'] = 'Sadské'
        self.translator[u'Klecany'] = 'Klecanech'
        self.translator[u'Odolena Voda'] = 'Odolene Vodě'
        self.translator[u'Stříbrná Skalice'] = 'Stříbrné Skalici'
        self.translator[u'Dolní Břežany'] = 'Dolních Břežanech'
        self.translator[u'Hostivice'] = 'Hostivici'
        self.translator[u'Jeneč'] = 'Jenči'
        self.translator[u'Jílové u Prahy'] = 'Jílovém u Prahy'
        self.translator[u'Neratovice'] = 'Neratovicích'
        self.translator[u'Liběchov'] = 'Liběchově'
        self.translator[u'Březnice'] = 'Březnici'
        self.translator[u'Kosova Hora'] = 'Kosově Hoře'
        self.translator[u'Krásná Hora nad Vltavou'] = 'Krásné Hoře nad Vltavou'
        self.translator[u'Nový Knín'] = 'Novém Kníně'
        self.translator[u'Rožmitál pod Třemšínem'] = 'Rožmitále pod Třemšínem'
        self.translator[u'Nové Strašecí'] = 'Novém Strašecí'
        self.translator[u'Chlumec nad Cidlinou'] = 'Chlumci nad Cidlinou'
        self.translator[u'Třebechovice pod Orebem'] = 'Třebechovicích pod Orebem'
        self.translator[u'Kopidlno'] = 'Kopidlně'
        self.translator[u'Libáň'] = 'Libáni'
        self.translator[u'Lázně Bělohrad'] = 'Lázních Bělohrad'
        self.translator[u'Miletín'] = 'Miletíně'
        self.translator[u'Mlázovice'] = 'Mlázovicích'
        self.translator[u'Nová Paka'] = 'Nové Pace'
        self.translator[u'Pecka (okres Jičín)'] = 'Pecce'
        self.translator[u'Železnice (okres Jičín)'] = 'Železnice'
        self.translator[u'Machov'] = 'Machově'
        self.translator[u'Stárkov'] = 'Stárkově'
        self.translator[u'Červený Kostelec'] = 'Červeném Kostelci'
        self.translator[u'Česká Skalice'] = 'České Skalici'
        self.translator[u'Solnice (okres Rychnov nad Kněžnou)'] = 'Solnici'
        self.translator[u'Týniště nad Orlicí'] = 'Týništi nad Orlicí'
        self.translator[u'Malé Svatoňovice'] = 'Malých Svatoňovicích'
        self.translator[u'Pilníkov'] = 'Pilníkově'
        self.translator[u'Svoboda nad Úpou'] = 'Svobodě nad Úpou'
        self.translator[u'Úpice'] = 'Úpici'
        self.translator[u'Černý Důl'] = 'Černém Dole'
        self.translator[u'Žacléř'] = 'Žacléři'
        self.translator[u'Františkovy Lázně'] = 'Františkových Lázních'
        self.translator[u'Luby'] = 'Lubech'
        self.translator[u'Teplá'] = 'Teplé'
        self.translator[u'Bečov nad Teplou'] = 'Bečově nad Teplou'
        self.translator[u'Bochov'] = 'Bochově'
        self.translator[u'Chyše'] = 'Chyši'
        self.translator[u'Horní Blatná'] = 'Horní Blatné'
        self.translator[u'Hroznětín'] = 'Hroznětíně'
        self.translator[u'Jáchymov'] = 'Jáchymově'
        self.translator[u'Nejdek'] = 'Nejdku'
        self.translator[u'Vysoké nad Jizerou'] = 'Vysokém nad Jizerou'
        self.translator[u'Dubá'] = 'Dubé'
        self.translator[u'Stráž pod Ralskem'] = 'Stráži pod Ralskem'
        self.translator[u'Kravaře'] = 'Kravařích'
        self.translator[u'Chrastava'] = 'Chrastavě'
        self.translator[u'Hodkovice nad Mohelkou'] = 'Hodkovicích nad Mohelkou'
        self.translator[u'Hrádek nad Nisou'] = 'Hrádku nad Nisou'
        self.translator[u'Nové Město pod Smrkem'] = 'Novém Městě pod Smrkem'
        self.translator[u'Osečná'] = 'Osečné'
        self.translator[u'Dvorce (okres Bruntál)'] = 'Dvorcích'
        self.translator[u'Město Albrechtice'] = 'Městě Albrechtice'
        self.translator[u'Osoblaha'] = 'Osoblaze'
        self.translator[u'Ryžoviště (okres Bruntál)'] = 'Ryžovišti'
        self.translator[u'Vrbno pod Pradědem'] = 'Vrbně pod Pradědem'
        self.translator[u'Brušperk'] = 'Brušperku'
        self.translator[u'Frýdlant nad Ostravicí'] = 'Frýdlantě nad Ostravicí'
        self.translator[u'Orlová'] = 'Orlové'
        self.translator[u'Bílovec'] = 'Bílovci'
        self.translator[u'Frenštát pod Radhoštěm'] = 'Frenštátě pod Radhoštěm'
        self.translator[u'Fulnek'] = 'Fulneku'
        self.translator[u'Odry'] = 'Odrách'
        self.translator[u'Příbor (okres Nový Jičín)'] = 'Příboře'
        self.translator[u'Žacléř'] = 'Žacléři'
        self.translator[u'Studénka'] = 'Studénce'
        self.translator[u'Štramberk'] = 'Štramberku'
        self.translator[u'Hlučín'] = 'Hlučíně'
        self.translator[u'Vítkov'] = 'Vítkově'
        self.translator[u'Klimkovice'] = 'Klimkovicích'
        self.translator[u'Loštice'] = 'Lošticích'
        self.translator[u'Staré Město (okres Šumperk)'] = 'Starém Městě'
        self.translator[u'Štíty'] = 'Štítech'
        self.translator[u'Javorník (okres Jeseník)'] = 'Javorníku'
        self.translator[u'Vidnava'] = 'Vidnavě'
        self.translator[u'Zlaté Hory'] = 'Zlatých Horách'
        self.translator[u'Křelov-Břuchotín'] = 'Křelově'
        self.translator[u'Moravský Beroun'] = 'Moravském Berouně'
        self.translator[u'Velký Újezd'] = 'Velkém Újezdu'
        self.translator[u'Konice'] = 'Konicích'
        self.translator[u'Kostelec na Hané'] = 'Kostelci na Hané'
        self.translator[u'Brodek u Přerova'] = 'Brodku u Přerova'
        self.translator[u'Dřevohostice'] = 'Dřevohosticích'
        self.translator[u'Hustopeče nad Bečvou'] = 'Hustopečích nad Bečvou'
        self.translator[u'Tovačov'] = 'Tovačově'
        self.translator[u'Chroustovice'] = 'Chroustovicích'
        self.translator[u'Luže'] = 'Lužích'
        self.translator[u'Nasavrky'] = 'Nasavrkách'
        self.translator[u'Proseč'] = 'Proseči'
        self.translator[u'Ronov nad Doubravou'] = 'Ronově nad Doubravou'
        #
        self.translator[u'Trhová Kamenice'] = 'Trhové Kamenici'
        self.translator[u'Třemošnice'] = 'Třemošnici'
        self.translator[u'Žumberk'] = 'Žumberku'
        #
        self.translator[u'Dašice'] = 'Dašicích'
        self.translator[u'Holice'] = 'Holicích'
        self.translator[u'Horní Jelení'] = 'Horním Jelení'
        self.translator[u'Moravany (okres Pardubice)'] = 'Moravanech'
        self.translator[u'Sezemice (okres Pardubice)'] = 'Sezemicích'
        self.translator[u'Březová nad Svitavou'] = 'Březové nad Svitavou'
        #
        self.translator[u'Žacléř'] = 'Žacléři'
        self.translator[u'Svitavy'] = 'Svitavách'
        self.translator[u'Brandýs nad Orlicí'] = 'Brandýse nad Orlicí'
        self.translator[u'Choceň'] = 'Chocni'
        self.translator[u'Jablonné nad Orlicí'] = 'Jablonném nad Orlicí'
        self.translator[u'Bělá nad Radbuzou'] = 'Bělé nad Radbuzou'
        self.translator[u'Nýrsko'] = 'Nýrsku'
        self.translator[u'Plánice'] = 'Plánicích'
        self.translator[u'Rabí'] = 'Rabí'
        self.translator[u'Strážov'] = 'Strážově'
        self.translator[u'Velhartice'] = 'Velharticích'
        self.translator[u'Železná Ruda'] = 'Železné Rudě'
        self.translator[u'Starý Plzenec'] = 'Starém Plzenci'
        self.translator[u'Manětín'] = 'Manětíně'
        self.translator[u'Žacléř'] = 'Žacléři'
        self.translator[u'Blovice'] = 'Blovicích'
        #
        self.translator[u'Žacléř'] = 'Žacléři'
        self.translator[u'Přeštice'] = 'Přešticích'
        self.translator[u'Spálené Poříčí'] = 'Spáleném Poříčí'
        self.translator[u'Kladruby (okres Tachov)'] = 'Kladrubech'
        self.translator[u'Planá'] = 'Plané'
        self.translator[u'Přimda'] = 'Přimdě'
        self.translator[u'Černošín'] = 'Černošíně'
        self.translator[u'Chlum u Třeboně'] = 'Chlumu u Třeboně'
        self.translator[u'Kunžak'] = 'Kunžaku'
        self.translator[u'Husinec (okres Prachatice)'] = 'Husinci'
        #
        self.translator[u'Vimperk'] = 'Vimperku'
        self.translator[u'Vlachovo Březí'] = 'Vlachově Březí'
        self.translator[u'Volary'] = 'Volarech'
        self.translator[u'Mirotice'] = 'Miroticích'
        self.translator[u'Mirovice'] = 'Mirovicích'
        self.translator[u'Bavorov'] = 'Bavorově'
        self.translator[u'Bělčice'] = 'Bělčicích'
        self.translator[u'Vodňany'] = 'Vodňanech'
        self.translator[u'Bechyně'] = 'Bechyni'
        self.translator[u'Jistebnice'] = 'Jistebnici'
        self.translator[u'Borovany'] = 'Borovanech'
        self.translator[u'Nové Hrady'] = 'Nových Hradech'
        self.translator[u'Římov (okres České Budějovice)'] = 'Římově'
        self.translator[u'Žacléř'] = 'Žacléři'
        self.translator[u'Žacléř'] = 'Žacléři'
        self.translator[u'Žacléř'] = 'Žacléři'
        self.translator[u'Frymburk'] = 'Frymburku'
        self.translator[u'Žacléř'] = 'Žacléři'
        self.translator[u'Žacléř'] = 'Žacléři'
        self.translator[u'Kaplice'] = 'Kaplici'
        self.translator[u'Besednice'] = 'Besednici'
        self.translator[u'Žacléř'] = 'Žacléři'
        self.translator[u'Velešín'] = 'Velešíně'
        self.translator[u'Vyšší Brod'] = 'Vyšším Brodě'
        #
        self.translator[u'Kunštát'] = 'Kunštátě'
        self.translator[u'Olešnice (okres Blansko)'] = 'Olešnici'
        self.translator[u'Černá Hora (okres Blansko)'] = 'Černé Hoře'
        self.translator[u'Blučina'] = 'Blučině'
        self.translator[u'Kuřim'] = 'Kuřimi'
        self.translator[u'Modřice'] = 'Modřicích'
        self.translator[u'Pohořelice (okres Brno-venkov)'] = 'Pohořelicích'
        self.translator[u'Veverská Bítýška'] = 'Veverské Bítýšce'
        self.translator[u'Šlapanice'] = 'Šlapanicích'
        self.translator[u'Židlochovice'] = 'Židlochovicích'
        self.translator[u'Drnholec'] = 'Drnholci'
        self.translator[u'Klobouky u Brna'] = 'Kloboukách u Brna'
        self.translator[u'Lanžhot'] = 'Lanžhotě'
        self.translator[u'Lednice (okres Břeclav)'] = 'Lednici'
        self.translator[u'Valtice'] = 'Valticích'
        self.translator[u'Velké Pavlovice'] = 'Velkých Pavlovicích'
        self.translator[u'Bzenec'] = 'Bzenci'
        self.translator[u'Bučovice'] = 'Bučovicích'
        self.translator[u'Bučovice'] = 'Černčíně'
        self.translator[u'Rousínov'] = 'Rousínově'
        self.translator[u'Hnanice'] = 'Hnanicích'
        self.translator[u'Žacléř'] = 'Žacléři'
        self.translator[u'Hrušovany nad Jevišovkou'] = 'Hrušovanech nad Jevišovkou'
        self.translator[u'Jaroslavice'] = 'Jaroslavicích'
        self.translator[u'Miroslav (okres Znojmo)'] = 'Miroslavi'
        self.translator[u'Žacléř'] = 'Žacléři'
        self.translator[u'Vranov nad Dyjí'] = 'Vranově nad Dyjí'
        self.translator[u'Chotěboř'] = 'Chotěboři'
        #
        self.translator[u'Habry'] = 'Habrech'
        self.translator[u'Havlíčkova Borová'] = 'Havlíčkově Borové'
        self.translator[u'Havlíčkův Brod'] = 'Havlíčkově Brodě'
        self.translator[u'Žacléř'] = 'Žacléři'
        self.translator[u'Přibyslav'] = 'Přibyslavi'
        self.translator[u'Batelov'] = 'Batelově'
        self.translator[u'Nová Říše'] = 'Nové Říší'
        self.translator[u'Horní Cerekev'] = 'Horní Cerekvi'
        self.translator[u'Pacov'] = 'Pacově'
        self.translator[u'Žirovnice'] = 'Žirovnici'
        self.translator[u'Žacléř'] = 'Žacléři'
        self.translator[u'Hrotovice'] = 'Hrotovicích'
        self.translator[u'Jaroměřice nad Rokytnou'] = 'Jaroměřicích nad Rokytnou'
        self.translator[u'Žacléř'] = 'Žacléři'
        self.translator[u'Moravské Budějovice'] = 'Moravských Budějovicích'
        self.translator[u'Žacléř'] = 'Žacléři'
        self.translator[u'Želetava'] = 'Želetavě'
        self.translator[u'Bystřice nad Pernštejnem'] = 'Bystřici nad Pernštejnem'
        self.translator[u'Jimramov'] = 'Jimramově'
        self.translator[u'Křižanov (okres Žďár nad Sázavou)'] = 'Křižanově'
        self.translator[u'Velká Bíteš'] = 'Velké Bíteši'
        self.translator[u'Velké Meziříčí'] = 'Velkém Meziříčí'
        self.translator[u'Chropyně'] = 'Chropyni'
        self.translator[u'Hulín'] = 'Hulíně'
        self.translator[u'Žacléř'] = 'Žacléři'
        self.translator[u'Morkovice-Slížany'] = 'Morkovicích-Slížanech'
        self.translator[u'Buchlovice'] = 'Buchlovice'
        self.translator[u'Kunovice'] = 'Kunovicích'
        #
        self.translator[u'Žacléř'] = 'Žacléři'
        self.translator[u'Rožnov pod Radhoštěm'] = 'Rožnově pod Radhoštěm'
        self.translator[u'Valašské Meziříčí'] = 'Valašském Meziříčí'
        #
        self.translator[u'Fryšták'] = 'Fryštáku'
        self.translator[u'Napajedla'] = 'Napajedlech'
        self.translator[u'Otrokovice'] = 'Otrokovicích'
        self.translator[u'Slušovice'] = 'Slušovicích'
        self.translator[u'Tlumačov (okres Zlín)'] = 'Tlumačově'
        self.translator[u'Valašské Klobouky'] = 'Valašských Kloboukách'
        #
        self.translator[u'Jirkov'] = 'Jirkově'
        self.translator[u'Klášterec nad Ohří'] = 'Klášterci nad Ohří'
        self.translator[u'Mašťov'] = 'Mašťově'
        self.translator[u'Bohušovice nad Ohří'] = 'Bohušovicích nad Ohří'
        self.translator[u'Brozany nad Ohří'] = 'Brozanech nad Ohří'
        self.translator[u'Žacléř'] = 'Žacléři'
        self.translator[u'Lovosice'] = 'Lovosicích'
        self.translator[u'Terezín'] = 'Terezíně'
        self.translator[u'Úštěk'] = 'Úštěku'
        self.translator[u'Cítoliby'] = 'Cítolibech'
        self.translator[u'Postoloprty'] = 'Postoloprtech'
        self.translator[u'Vroutek'] = 'Vroutku'
        self.translator[u'Meziboří'] = 'Meziboří'
        self.translator[u'Lom (okres Most)'] = 'Lom'
        self.translator[u'Bílina (město)'] = 'Bílině'
        self.translator[u'Osek (okres Teplice)'] = 'Oseku'
        self.translator[u'Chabařovice'] = 'Chabařovicích'
        self.translator[u'Žacléř'] = 'Žacléři'
        self.translator[u'Blansko'] = 'Blansku'
        self.translator[u'Bor (okres Tachov)'] = 'Boru'
        self.translator[u'Brno'] = 'Brně'
        self.translator[u'Doksy'] = 'Doksech'
        self.translator[u'Duchcov'] = 'Duchcově'
        self.translator[u'Dvůr Králové nad Labem'] = 'Dvoře Králové nad Labem'
        self.translator[u'Havířov'] = 'Havířově'
        self.translator[u'Horní Slavkov'] = 'Horním Slavkově'
        self.translator[u'Horšovský Týn'] = 'Horšovském Týně'
        self.translator[u'Jáchymov'] = 'Jáchymově'
        self.translator[u'Karlovy Vary'] = 'Karlových Varech'
        self.translator[u'Kladruby (okres Tachov)'] = 'Kladrubech'
        self.translator[u'Kojetín'] = 'Kojetíně'
        self.translator[u'Lom (okres Most)'] = 'Lomu'
        self.translator[u'Mladá Boleslav'] = 'Mladé Boleslavi'
        self.translator[u'Neratovice'] = 'Neratovicích'
        self.translator[u'Nové Hrady'] = 'Nových Hradech'
        self.translator[u'Nové Město nad Metují'] = 'Novém Městě nad Metují'
        self.translator[u'Olomouc'] = 'Olomouci'
        self.translator[u'Opava'] = 'Opavě'
        self.translator[u'Oslavany'] = 'Oslavanech'
        self.translator[u'Ostrava'] = 'Ostravě'
        self.translator[u'Pardubice'] = 'Pardubicích'
        self.translator[u'Plzeň'] = 'Plzni'
        self.translator[u'Praha'] = 'Praze'

        #
        self.translator[u'Svratka (okres Žďár nad Sázavou)'] = 'Svratce'
        self.translator[u'Tábor'] = 'Táboře'
        self.translator[u'Vizovice'] = 'Vizovici'
        self.translator[u'Jilemnice'] = 'Jilemnici'
        self.translator[u'Chomutov'] = 'Chomutově'
        self.translator[u'Liberec'] = 'Liberci'
        self.translator[u'Brno'] = 'Brně'
        #
        self.translator[u'Český Dub'] = 'Českém Dubu'
        self.translator[u'Hrob (okres Teplice)'] = 'Hrobě'
        self.translator[u'Holešov'] = 'Holešově'
        self.translator[u'Počátky'] = 'Počátkách'
        self.translator[u'Moravský Krumlov'] = 'Moravském Krumlově'
        self.translator[u'Strážnice (okres Hodonín)'] = 'Strážnici'
        self.translator[u'Hustopeče'] = 'Hustopečích'
        self.translator[u'Týn nad Vltavou'] = 'Týně nad Vltavou'
        self.translator[u'Dačice'] = 'Dačicích'
        self.translator[u'Poběžovice'] = 'Poběžovicích'
        self.translator[u'Žamberk'] = 'Žamberku'
        self.translator[u'Horní Slavkov'] = 'Horním Slavkově'
        self.translator[u'Budišov nad Budišovkou'] = 'Budišově nad Budišovkou'
        #
        self.translator[u'Brno'] = 'Brně'
        # #
        self.translator[u'Frýdek-Místek'] = 'Frýdku'
        self.translator[u'Frýdek-Místek'] = 'Místku'
        self.translator[u'Frýdek-Místek'] = 'Frýdku-Místku'
        # #
        self.translator[u'Jaroměř'] = 'Josefově (Jaroměř)'
        self.translator[u'Kutná Hora'] = 'Kutné Hoře'
        self.translator[u'Ostrava'] = 'Ostravě'
        self.translator[u'Ostrava'] = 'Ostravě'
        self.translator[u'Ostrava'] = 'Ostravě'
        #
        self.translator[u'Adamov (okres Blansko)'] = 'Adamově'
        self.translator[u'Bělá pod Bezdězem'] = 'Bělé pod Bezdězem'
        self.translator[u'Benešov nad Ploučnicí'] = 'Benešově nad Ploučnicí'
        self.translator[u'Beroun'] = 'Berouně'
        self.translator[u'Blansko'] = 'Blansku'
        self.translator[u'Blatná'] = 'Blatné'
        self.translator[u'Bohumín'] = 'Bohumíně'
        self.translator[u'Bor (okres Tachov)'] = 'Boru'
        self.translator[u'Brandýs nad Labem-Stará Boleslav'] = 'Brandýse nad Labem-Staré Boleslavi'
        self.translator[u'Brno'] = 'Brně'
        self.translator[u'Broumov'] = 'Broumově'
        self.translator[u'Brtnice'] = 'Brtnici'
        self.translator[u'Bruntál'] = 'Bruntále'
        self.translator[u'Budyně nad Ohří'] = 'Budyni nad Ohří'
        self.translator[u'Bystřice pod Hostýnem'] = 'Bystřici pod Hostýnem'
        self.translator[u'Břeclav'] = 'Břeclavi'
        #
        self.translator[u'Cheb'] = 'Chebu'
        self.translator[u'Chrast'] = 'Chrasti'
        self.translator[u'Chrudim'] = 'Chrudimi'
        self.translator[u'Cvikov'] = 'Cvikově'
        #
        self.translator[u'Divišov'] = 'Divišově'
        self.translator[u'Dobruška'] = 'Dobrušce'
        self.translator[u'Doksy'] = 'Doksech'
        self.translator[u'Dolní Kounice'] = 'Dolních Kounicích'
        self.translator[u'Domažlice'] = 'Domažlicích'
        self.translator[u'Duchcov'] = 'Duchcově'
        self.translator[u'Dvůr Králové nad Labem'] = 'Dvoře Králové nad Labem'
        self.translator[u'Děčín'] = 'Děčíně'
        #
        self.translator[u'Frýdek-Místek'] = 'Frýdku-Místku'
        self.translator[u'Frýdlant'] = 'Frýdlantě'
        #
        self.translator[u'Havlíčkův Brod'] = 'Havlíčkově Brodě'
        self.translator[u'Havířov'] = 'Havířově'
        self.translator[u'Heřmanův Městec'] = 'Heřmanově Městci'
        self.translator[u'Hlinsko'] = 'Hlinsku'
        self.translator[u'Hodonín'] = 'Hodoníně'
        self.translator[u'Horažďovice'] = 'Horažďovicích'
        self.translator[u'Horšovský Týn'] = 'Horšovském Týně'
        self.translator[u'Hostinné'] = 'Hostinném'
        self.translator[u'Hořice'] = 'Hořicích'
        self.translator[u'Hradec Králové'] = 'Hradci Králové'
        self.translator[u'Hranice (okres Přerov)'] = 'Hranicích'
        self.translator[u'Hronov'] = 'Hronově'
        self.translator[u'Humpolec'] = 'Humpolci'
        #
        self.translator[u'Ivančice'] = 'Ivančicích'
        #
        self.translator[u'Jablonec nad Nisou'] = 'Jablonci nad Nisou'
        self.translator[u'Jablonné v Podještědí'] = 'Jablonném v Podještědí'
        self.translator[u'Jablunkov'] = 'Jablunkově'
        self.translator[u'Jaroměř'] = 'Jaroměři'
        self.translator[u'Jemnice'] = 'Jemnici'
        self.translator[u'Jeseník'] = 'Jeseníku'
        self.translator[u'Jevíčko'] = 'Jevíčku'
        self.translator[u'Jičín'] = 'Jičíně'
        self.translator[u'Jihlava'] = 'Jihlavě'
        #
        self.translator[u'Votice'] = 'Voticích'
        self.translator[u'Kladno'] = 'Kladně'
        self.translator[u'Kolín'] = 'Kolíně'
        self.translator[u'Dobříš'] = 'Dobříši'
        #
        self.translator[u'Jindřichův Hradec'] = 'Jindřichově Hradci'
        #
        self.translator[u'Kadaň'] = 'Kadani'
        self.translator[u'Kamenice nad Lipou'] = 'Kamenici nad Lipou'
        self.translator[u'Kamenický Šenov'] = 'Kamenickém Šenově'
        self.translator[u'Kardašova Řečice'] = 'Kardašově Řečici'
        self.translator[u'Karlovy Vary'] = 'Karlových Varech'
        self.translator[u'Karviná'] = 'Karviné'
        self.translator[u'Kašperské Hory'] = 'Kašperských Horách'
        self.translator[u'Klatovy'] = 'Klatovech'
        self.translator[u'Kojetín'] = 'Kojetíně'
        self.translator[u'Kostelec nad Orlicí'] = 'Kostelci nad Orlicí'
        self.translator[u'Kostelec nad Černými lesy'] = 'Kostelci nad Černými lesy'
        self.translator[u'Kouřim'] = 'Kouřimi'
        self.translator[u'Králíky'] = 'Králíkách'
        self.translator[u'Kralovice'] = 'Kralovicích'
        self.translator[u'Kralupy nad Vltavou'] = 'Kralupech nad Vltavou'
        self.translator[u'Krásná Lípa'] = 'Krásné Lípě'
        self.translator[u'Krnov'] = 'Krnově'
        self.translator[u'Kroměříž'] = 'Kroměříži'
        self.translator[u'Krupka'] = 'Krupce'
        self.translator[u'Krásno'] = 'Krásně'
        self.translator[u'Kutná Hora'] = 'Kutné Hoře'
        self.translator[u'Kyjov'] = 'Kyjově'
        #
        self.translator[u'Lanškroun'] = 'Lanškrouně'
        self.translator[u'Lázně Bohdaneč'] = 'Lázních Bohdaneč'
        self.translator[u'Ledeč nad Sázavou'] = 'Ledči nad Sázavou'
        self.translator[u'Letohrad'] = 'Letohradě'
        self.translator[u'Libochovice'] = 'Libochovicích'
        self.translator[u'Libčice nad Vltavou'] = 'Libčicích nad Vltavou'
        self.translator[u'Litomyšl'] = 'Litomyšli'
        self.translator[u'Litoměřice'] = 'Litoměřicích'
        self.translator[u'Litovel'] = 'Litovli'
        self.translator[u'Litvínov'] = 'Litvínově'
        self.translator[u'Loket (okres Sokolov)'] = 'Lokti'
        self.translator[u'Lomnice (okres Brno-venkov)'] = 'Lomnici'
        self.translator[u'Lomnice nad Lužnicí'] = 'Lomnici nad Lužnicí'
        self.translator[u'Lomnice nad Popelkou'] = 'Lomnici nad Popelkou'
        self.translator[u'Louny'] = 'Lounech'
        self.translator[u'Luhačovice'] = 'Luhačovicích'
        self.translator[u'Lysice'] = 'Lysicích'
        self.translator[u'Lysá nad Labem'] = 'Lysé nad Labem'
        #
        self.translator[u'Mariánské Lázně'] = 'Mariánských Lázních'
        self.translator[u'Mělník'] = 'Mělníku'
        self.translator[u'Mikulov'] = 'Mikulově'
        self.translator[u'Milevsko'] = 'Milevsku'
        self.translator[u'Mimoň'] = 'Mimoni'
        self.translator[u'Mladá Boleslav'] = 'Mladé Boleslavi'
        self.translator[u'Mníšek pod Brdy'] = 'Mníšku pod Brdy'
        self.translator[u'Mohelnice'] = 'Mohelnici'
        self.translator[u'Moravská Třebová'] = 'Moravské Třebové'
        self.translator[u'Most (město)'] = 'Mostě'
        self.translator[u'Město Touškov'] = 'Městě Touškově'
        #
        self.translator[u'Nechanice'] = 'Nechanicích'
        self.translator[u'Nepomuk'] = 'Nepomuku'
        self.translator[u'Neveklov'] = 'Neveklově'
        self.translator[u'Nová Bystřice'] = 'Nové Bystřici'
        self.translator[u'Nové Město na Moravě'] = 'Novém Městě na Moravě'
        self.translator[u'Nové Město nad Metují'] = 'Novém Městě nad Metují'
        self.translator[u'Nový Bor'] = 'Novém Boru'
        self.translator[u'Nový Jičín'] = 'Novém Jičíně'
        self.translator[u'Nymburk'] = 'Nymburku'
        self.translator[u'Náchod'] = 'Náchodě'
        self.translator[u'Náměšť nad Oslavou'] = 'Náměšti nad Oslavou'
        self.translator[u'Němčice nad Hanou'] = 'Němčicích nad Hanou'
        #
        self.translator[u'Olomouc'] = 'Olomouci'
        self.translator[u'Opava'] = 'Opavě'
        self.translator[u'Opočno'] = 'Opočně'
        self.translator[u'Oslavany'] = 'Oslavanech'
        self.translator[u'Ostrava'] = 'Ostravě'
        self.translator[u'Ostrov (okres Karlovy Vary)'] = 'Ostrově'
        #
        self.translator[u'Pardubice'] = 'Pardubicích'
        self.translator[u'Pelhřimov'] = 'Pelhřimově'
        self.translator[u'Peruc'] = 'Peruci'
        self.translator[u'Pečky'] = 'Pečkách'
        self.translator[u'Písek (město)'] = 'Písku'
        self.translator[u'Plumlov'] = 'Plumlově'
        self.translator[u'Plzeň'] = 'Plzni'
        self.translator[u'Poděbrady'] = 'Poděbradech'
        self.translator[u'Police nad Metují'] = 'Polici nad Metují'
        self.translator[u'Polička'] = 'Poličce'
        self.translator[u'Polná'] = 'Polné'
        self.translator[u'Prachatice'] = 'Prachaticích'
        self.translator[u'Praha'] = 'Praze'
        self.translator[u'Prostějov'] = 'Prostějově'
        self.translator[u'Protivín'] = 'Protivíně'
        self.translator[u'Přelouč'] = 'Přelouči'
        self.translator[u'Přerov'] = 'Přerově'
        self.translator[u'Příbram'] = 'Příbrami'
        #
        self.translator[u'Rakovník'] = 'Rakovníku'
        self.translator[u'Rokycany'] = 'Rokycanech'
        self.translator[u'Rokytnice v Orlických horách'] = 'Rokytnici v Orlických horách'
        self.translator[u'Rosice (okres Brno-venkov)'] = 'Rosicích'
        self.translator[u'Roudnice nad Labem'] = 'Roudnici nad Labem'
        self.translator[u'Roztoky (okres Praha-západ)'] = 'Roztokách'
        self.translator[u'Rudná (okres Praha-západ)'] = 'Rudné'
        self.translator[u'Rumburk'] = 'Rumburku'
        self.translator[u'Rychnov nad Kněžnou'] = 'Rychnově nad Kněžnou'
        self.translator[u'Rýmařov'] = 'Rýmařově'
        #
        self.translator[u'Sedlec-Prčice'] = 'Sedleci-Prčicích'
        self.translator[u'Sedlčany'] = 'Sedlčanech'
        self.translator[u'Semily'] = 'Semilech'
        self.translator[u'Sezimovo Ústí'] = 'Sezimově Ústí'
        self.translator[u'Skuteč'] = 'Skutči'
        self.translator[u'Slaný'] = 'Slaném'
        self.translator[u'Slavkov u Brna'] = 'Slavkově u Brna'
        self.translator[u'Slavonice'] = 'Slavonicích'
        self.translator[u'Smržovka'] = 'Smržovce'
        self.translator[u'Sobotka'] = 'Sobotce'
        self.translator[u'Soběslav'] = 'Soběslavi'
        self.translator[u'Sokolov'] = 'Sokolově'
        self.translator[u'Staré Město (okres Uherské Hradiště)'] = 'Starém Městě'
        self.translator[u'Stochov'] = 'Stochově'
        self.translator[u'Strakonice'] = 'Strakonicích'
        self.translator[u'Stráž nad Nežárkou'] = 'Stráži nad Nežárkou'
        self.translator[u'Stříbro (okres Tachov)'] = 'Stříbře'
        self.translator[u'Sušice'] = 'Sušici'
        self.translator[u'Svitavy'] = 'Svitavách'
        #
        #
        #
        self.translator[u'Tachov'] = 'Tachově'
        self.translator[u'Telč'] = 'Telči'
        self.translator[u'Teplice'] = 'Teplicích'
        self.translator[u'Tišnov'] = 'Tišnově'
        self.translator[u'Toužim'] = 'Toužimi'
        self.translator[u'Trhové Sviny'] = 'Trhových Svinech'
        self.translator[u'Trutnov'] = 'Trutnově'
        self.translator[u'Turnov'] = 'Turnově'
        self.translator[u'Třebenice (okres Litoměřice)'] = 'Třebenicích'
        self.translator[u'Třeboň'] = 'Třeboni'
        self.translator[u'Třebíč'] = 'Třebíči'
        self.translator[u'Třešť'] = 'Třešti'
        self.translator[u'Třinec'] = 'Třinci'
        #
        self.translator[u'Uherské Hradiště'] = 'Uherském Hradišti'
        self.translator[u'Uherský Brod'] = 'Uherském Brodě'
        self.translator[u'Uničov'] = 'Uničově'
        #
        self.translator[u'Vamberk'] = 'Vamberku'
        self.translator[u'Veselí nad Lužnicí'] = 'Veselí nad Lužnicí'
        self.translator[u'Veselí nad Moravou'] = 'Veselí nad Moravou'
        #
        self.translator[u'Vlašim'] = 'Vlašimi'
        self.translator[u'Volyně'] = 'Volyni'
        self.translator[u'Vrchlabí'] = 'Vrchlabí'
        self.translator[u'Vsetín'] = 'Vsetíně'
        self.translator[u'Vysoké Mýto'] = 'Vysokém Mýtě'
        self.translator[u'Vyškov'] = 'Vyškově'
        #
        self.translator[u'Zábřeh'] = 'Zábřehu'
        self.translator[u'Zákupy'] = 'Zákupech'
        self.translator[u'Zlín'] = 'Zlíně'
        self.translator[u'Znojmo'] = 'Znojmě'
        self.translator[u'Zruč nad Sázavou'] = 'Zruči nad Sázavou'
        #
        self.translator[u'Ústí nad Labem'] = 'Ústí nad Labem'
        self.translator[u'Ústí nad Orlicí'] = 'Ústí nad Orlicí'
        #
        self.translator[u'Česká Kamenice'] = 'České Kamenici'
        self.translator[u'Česká Lípa'] = 'České Lípě'
        self.translator[u'Česká Třebová'] = 'České Třebové'
        self.translator[u'České Budějovice'] = 'Českých Budějovicích'
        self.translator[u'Český Brod'] = 'Českém Brodě'
        self.translator[u'Český Krumlov'] = 'Českém Krumlově'
        self.translator[u'Český Těšín'] = 'Českém Těšíně'
        self.translator[u'Čáslav'] = 'Čáslavi'
        #
        self.translator[u'Řevnice'] = 'Řevnicích'
        self.translator[u'Říčany'] = 'Říčanech'
        #
        self.translator[u'Šluknov'] = 'Šluknově'
        self.translator[u'Šternberk'] = 'Šternberku'
        self.translator[u'Šumperk'] = 'Šumperku'
        #
        self.translator[u'Žatec'] = 'Žatci'
        self.translator[u'Železný Brod'] = 'Železném Brodě'
        self.translator[u'Žlutice'] = 'Žluticích'
        self.translator[u'Žulová'] = 'Žulové'
        self.translator[u'Žďár nad Sázavou'] = 'Žďáru nad Sázavou'
        #
        self.translator[u'Šestajovice (okres Praha-východ)'] = 'Šestajovicích'
        self.translator[u'Čečelice'] = 'Čečelicích'

        self.translator[u'Čečelice'] = 'Čečelicích'
        self.translator[u'Všetaty (okres Mělník)'] = 'Všetatech'
        self.translator[u'Byšice'] = 'Byšicích'
        self.translator[u'Nové Dvory (okres Kutná Hora)'] = 'Nových Dvorech'
        self.translator[u'Středokluky'] = 'Středoklukách'
        self.translator[u'Všenory'] = 'Všenorech'
        self.translator[u'Máslovice'] = 'Máslovicích'
        self.translator[u'Mšeno'] = 'Mšeně'
        self.translator[u'Liteň'] = 'Bělči (Liteň)'
        self.translator[u'Smržovka'] = 'Smržovce'
        self.translator[u'Tanvald'] = 'Tanvaldu'
        self.translator[u'Jeřmanice'] = 'Jeřmanicích'
        self.translator[u'Mníšek'] = 'Mníšku'
        self.translator[u'Hejnice'] = 'Hejnicích'
        self.translator[u'Mníšek'] = 'Fojtce'
        self.translator[u'Hodkovice nad Mohelkou'] = 'Hodkovicích nad Mohelkou'
        self.translator[u'Hrádek nad Nisou'] = 'Hrádku nad Nisou'
        self.translator[u'Doksy'] = 'Starých Splavech'
        self.translator[u'Stráž pod Ralskem'] = 'Stráži pod Ralskem'
        self.translator[u'Klatovy'] = 'Klatovech'
        self.translator[u'Kladruby (okres Tachov)'] = 'Kladrubech'
        self.translator[u'Stříbro (okres Tachov)'] = 'Stříbře'
        self.translator[u'Tachov'] = 'Tachově'
        self.translator[u'Vlachovo Březí'] = 'Vlachově Březí'
        self.translator[u'Tišnov'] = 'Tišnově'
        self.translator[u'Kunštát'] = 'Kunštátě'
        self.translator[u'Bílovice nad Svitavou'] = 'Bílovicích nad Svitavou'
        self.translator[u'Mariánské Radčice'] = 'Mariánských Radčicích'
        self.translator[u'Meziboří'] = 'Meziboří'
        self.translator[u'Rumburk'] = 'Rumburku'
        self.translator[u'Rumburk'] = 'Dolních Křečanech'
        #
        self.translator[u'Abertamy'] = 'Abertamech'
        self.translator[u'Rybitví'] = 'Rybitví'
        self.translator[u'Samotišky'] = 'Samotiškách'
        self.translator[u'Osek (okres Teplice)'] = 'Oseku'
        self.translator[u'Adamov (okres Blansko)'] = 'Adamově'
        #
        self.translator[u'Bechyně'] = 'Bechyni'
        self.translator[u'Benešov'] = 'Benešově'
        self.translator[u'Beroun'] = 'Berouně'
        self.translator[u'Bečov nad Teplou'] = 'Bečově nad Teplou'
        self.translator[u'Bílina (město)'] = 'Bílině'
        self.translator[u'Blansko'] = 'Blansku'
        self.translator[u'Bohumín'] = 'Bohumíně'
        self.translator[u'Boskovice'] = 'Boskovicích'
        self.translator[u'Brandýs nad Labem-Stará Boleslav'] = 'Brandýse nad Labem'
        self.translator[u'Brandýs nad Orlicí'] = 'Brandýse nad Orlicí'
        self.translator[u'Brno'] = 'Brně'
        self.translator[u'Bruntál'] = 'Bruntále'
        self.translator[u'Budyně nad Ohří'] = 'Budyni nad Ohří'
        self.translator[u'Břeclav'] = 'Břeclavi'
        #
        self.translator[u'Cheb'] = 'Chebu'
        self.translator[u'Chlumec (okres Ústí nad Labem)'] = 'Chlumci'
        self.translator[u'Choceň'] = 'Chocni'
        self.translator[u'Chomutov'] = 'Chomutově'
        self.translator[u'Chotěboř'] = 'Chotěboři'
        self.translator[u'Chrast'] = 'Chrasti'
        self.translator[u'Chrastava'] = 'Chrastavě'
        self.translator[u'Chrudim'] = 'Chrudimi'
        self.translator[u'Cvikov'] = 'Cvikově'
        #
        self.translator[u'Dačice'] = 'Dačicích'
        self.translator[u'Desná'] = 'Desné'
        self.translator[u'Dobřichovice'] = 'Dobřichovicích'
        self.translator[u'Dobříš'] = 'Dobříš'
        self.translator[u'Doksy'] = 'Doksech'
        self.translator[u'Doksy (okres Kladno)'] = 'Doksech'
        self.translator[u'Domažlice'] = 'Domažlicích'
        self.translator[u'Dubá'] = 'Dubé'
        self.translator[u'Duchcov'] = 'Duchcově'
        self.translator[u'Dvorce (okres Bruntál)'] = 'Dvorcích'
        self.translator[u'Dvůr Králové nad Labem'] = 'Dvoře Králové nad Labem'
        self.translator[u'Děčín'] = 'Děčíně'
        #
        self.translator[u'Františkovy Lázně'] = 'Františkových Lázních'
        self.translator[u'Frenštát pod Radhoštěm'] = 'Frenštátě pod Radhoštěm'
        self.translator[u'Frýdek-Místek'] = 'Frýdku-Místku'
        self.translator[u'Frýdlant'] = 'Frýdlantě'
        self.translator[u'Frýdlant nad Ostravicí'] = 'Frýdlantě nad Ostravicí'
        self.translator[u'Fulnek'] = 'Fulneku'
        #
        self.translator[u'Kutná Hora'] = 'Kutné Hoře'
        self.translator[u'Křelov-Břuchotín'] = 'Břuchotíně'
        self.translator[u'Křelov-Břuchotín'] = 'Křelově'
        self.translator[u'Ostrava'] = 'Ostravě'
        self.translator[u'Ostrava'] = 'Ostravě'
        self.translator[u'Ostrava'] = 'Ostravě'
        self.translator[u'Ostrava'] = 'Ostravě'
        self.translator[u'Ostrava'] = 'Ostravě'
        self.translator[u'Říčany'] = 'Kuří'
        self.translator[u'Frýdek-Místek'] = 'Místku'
        self.translator[u'Frýdek-Místek'] = 'Frýdku'
        self.translator[u'Frýdek-Místek'] = 'Frýdku-Místku'
        self.translator[u'Uherské Hradiště'] = 'Rybárnách'
        #
        self.translator[u'Havlíčkův Brod'] = 'Havlíčkově Brodě'
        self.translator[u'Havířov'] = 'Havířově'
        self.translator[u'Hlinsko'] = 'Hlinsku'
        self.translator[u'Hodonín'] = 'Hodoníně'
        self.translator[u'Holešov'] = 'Holešově'
        self.translator[u'Holice'] = 'Holicích'
        self.translator[u'Hora Svaté Kateřiny'] = 'Hoře Svaté Kateřiny'
        self.translator[u'Horní Blatná'] = 'Horní Blatné'
        self.translator[u'Horšovský Týn'] = 'Horšovském Týně'
        self.translator[u'Hostinné'] = 'Hostinném'
        self.translator[u'Hostivice'] = 'Hostivici'
        self.translator[u'Hořice'] = 'Hořicích'
        self.translator[u'Hradec Králové'] = 'Hradci Králové'
        self.translator[u'Hranice (okres Přerov)'] = 'Hranicích'
        self.translator[u'Hronov'] = 'Hronově'
        self.translator[u'Hustopeče'] = 'Hustopečích'
        #
        self.translator[u'Ivančice'] = 'Ivančicích'
        #
        self.translator[u'Jablonné v Podještědí'] = 'Jablonném v Podještědí'
        self.translator[u'Jablonec nad Nisou'] = 'Jablonci nad Nisou'
        self.translator[u'Jaroměř'] = 'Jaroměři'
        self.translator[u'Jesenice (okres Praha-západ)'] = 'Jesenici'
        self.translator[u'Jeseník'] = 'Jeseníku'
        self.translator[u'Jihlava'] = 'Jihlavě'
        self.translator[u'Jilemnice'] = 'Jilemnici'
        self.translator[u'Jílové u Prahy'] = 'Jílovém u Prahy'
        self.translator[u'Jimramov'] = 'Jimramově'
        self.translator[u'Jindřichův Hradec'] = 'Jindřichově Hradci'
        self.translator[u'Jirkov'] = 'Jirkově'
        self.translator[u'Jičín'] = 'Jičíně'
        self.translator[u'Jaroměř'] = 'Josefově (Jaroměř)'
        #
        self.translator[u'Kadaň'] = 'Kadani'
        self.translator[u'Kamenický Šenov'] = 'Kamenickém Šenově'
        self.translator[u'Karlovy Vary'] = 'Karlových Varech'
        self.translator[u'Karviná'] = 'Karviné'
        self.translator[u'Kašperské Hory'] = 'Kašperských Horách'
        self.translator[u'Kladno'] = 'Kladně'
        self.translator[u'Klobouky u Brna'] = 'Kloboukách u Brna'
        self.translator[u'Klášterec nad Ohří'] = 'Klášterci nad Ohří'
        self.translator[u'Kolín'] = 'Kolíně'
        self.translator[u'Kopidlno'] = 'Kopidlně'
        self.translator[u'Kostelec nad Labem'] = 'Kostelci nad Labem'
        self.translator[u'Kostelec nad Orlicí'] = 'Kostelci nad Orlicí'
        self.translator[u'Kostelec nad Černými lesy'] = 'Kostelci nad Černými lesy'
        self.translator[u'Kouřim'] = 'Kouřimi'
        self.translator[u'Kralovice'] = 'Kralovicích'
        self.translator[u'Kralupy nad Vltavou'] = 'Kralupech nad Vltavou'
        self.translator[u'Krásná Lípa'] = 'Krásné Lípě'
        self.translator[u'Krnov'] = 'Krnově'
        self.translator[u'Kroměříž'] = 'Kroměříži'
        self.translator[u'Králův Dvůr'] = 'Králově Dvoře'
        self.translator[u'Kutná Hora'] = 'Kutné Hoře'
        self.translator[u'Kyjov'] = 'Kyjově'
        self.translator[u'Křelov-Břuchotín'] = 'Křelově-Břuchotíně'
        #
        self.translator[u'Libčice nad Vltavou'] = 'Libčicích nad Vltavou'
        self.translator[u'Liběchov'] = 'Liběchově'
        self.translator[u'Lipník nad Bečvou'] = 'Lipníku nad Bečvou'
        self.translator[u'Liteň'] = 'Litni'
        self.translator[u'Litomyšl'] = 'Litomyšli'
        self.translator[u'Litoměřice'] = 'Litoměřicích'
        self.translator[u'Litovel'] = 'Litovli'
        self.translator[u'Litvínov'] = 'Litvínově'
        # f = streets('streets', streetsDict, u'Category:Streets in Loket', 'Loket (okres Sokolov)', 'Lokti', 'Loket',)
        self.translator[u'Lom (okres Most)'] = 'Lomu'
        self.translator[u'Lomnice nad Popelkou'] = 'Lomnici nad Popelkou'
        self.translator[u'Louny'] = 'Lounech'
        self.translator[u'Lovosice'] = 'Lovosicích'
        self.translator[u'Luhačovice'] = 'Luhačovicích'
        self.translator[u'Lysice'] = 'Lysicích'
        self.translator[u'Lysá nad Labem'] = 'Lysé nad Labem'
        #
        self.translator[u'Malešov'] = 'Malešově'
        self.translator[u'Mariánské Lázně'] = 'Mariánských Lázních'
        self.translator[u'Mělník'] = 'Mělníku'
        self.translator[u'Mikulov'] = 'Mikulově'
        self.translator[u'Milevsko'] = 'Milevsku'
        self.translator[u'Mimoň'] = 'Mimoni'
        self.translator[u'Miroslav (okres Znojmo)'] = 'Miroslavi'
        self.translator[u'Mladá Boleslav'] = 'Mladé Boleslavi'
        self.translator[u'Mnichovo Hradiště'] = 'Mnichově Hradišti'
        self.translator[u'Mohelnice'] = 'Mohelnici'
        self.translator[u'Moravská Třebová'] = 'Moravské Třebové'
        self.translator[u'Moravské Budějovice'] = 'Moravských Budějovicích'
        self.translator[u'Most (město)'] = 'Mostě'
        #
        self.translator[u'Napajedla'] = 'Napajedlech'
        self.translator[u'Nechanice'] = 'Nechanicích'
        self.translator[u'Nepomuk'] = 'Nepomuku'
        self.translator[u'Neratovice'] = 'Neratovicích'
        self.translator[u'Neveklov'] = 'Neveklově'
        #
        self.translator[u'Nové Město nad Metují'] = 'Novém Městě nad Metují'
        self.translator[u'Nový Bor'] = 'Novém Boru'
        self.translator[u'Nový Jičín'] = 'Novém Jičíně'
        self.translator[u'Nymburk'] = 'Nymburku'
        self.translator[u'Náchod'] = 'Náchodě'
        #
        self.translator[u'Olomouc'] = 'Olomouci'
        self.translator[u'Opava'] = 'Opavě'
        self.translator[u'Ostrava'] = 'Ostravě'
        self.translator[u'Otrokovice'] = 'Otrokovicích'
        #
        self.translator[u'Pardubice'] = 'Pardubicích'
        self.translator[u'Pelhřimov'] = 'Pelhřimově'
        self.translator[u'Pečky'] = 'Pečkách'
        self.translator[u'Písek (město)'] = 'Písku'
        self.translator[u'Plzeň'] = 'Plzni'
        self.translator[u'Poděbrady'] = 'Poděbradech'
        self.translator[u'Pohled (okres Havlíčkův Brod)'] = 'Pohledu'
        self.translator[u'Polička'] = 'Poličce'
        self.translator[u'Polná'] = 'Polné'
        self.translator[u'Prachatice'] = 'Prachaticích'
        self.translator[u'Příbram'] = 'Příbrami'
        self.translator[u'Prostějov'] = 'Prostějově'
        self.translator[u'Protivín'] = 'Protivíně'
        self.translator[u'Pyšely'] = 'Pyšelech'
        self.translator[u'Přerov'] = 'Přerově'
        self.translator[u'Přeštice'] = 'Přešticích'
        self.translator[u'Přibyslav'] = 'Přibyslavi'
        self.translator[u'Příbor (okres Nový Jičín)'] = 'Příboře'
        #
        self.translator[u'Rakovník'] = 'Rakovníku'
        self.translator[u'Raspenava'] = 'Raspenavě'
        self.translator[u'Rokycany'] = 'Rokycanech'
        self.translator[u'Rosice (okres Brno-venkov)'] = 'Rosicích'
        self.translator[u'Roudnice nad Labem'] = 'Roudnici nad Labem'
        self.translator[u'Roztoky (okres Praha-západ)'] = 'Roztokách'
        self.translator[u'Rožmitál pod Třemšínem'] = 'Rožmitále pod Třemšínem'
        self.translator[u'Rudná (okres Praha-západ)'] = 'Rudné'
        self.translator[u'Rychnov nad Kněžnou'] = 'Rychnově nad Kněžnou'
        self.translator[u'Rýmařov'] = 'Rýmařově'
        #
        self.translator[u'Semily'] = 'Semilech'
        self.translator[u'Sezimovo Ústí'] = 'Sezimově Ústí'
        self.translator[u'Skuteč'] = 'Skutči'
        self.translator[u'Slaný'] = 'Slaném'
        self.translator[u'Slavkov u Brna'] = 'Slavkově u Brna'
        self.translator[u'Slavonice'] = 'Slavonicích'
        self.translator[u'Sloup v Čechách'] = 'Sloupu v Čechách'
        self.translator[u'Sobotka'] = 'Sobotce'
        self.translator[u'Soběslav'] = 'Soběslavi'
        self.translator[u'Sokolov'] = 'Sokolově'
        self.translator[u'Srbsko (okres Beroun)'] = 'Srbsku'
        self.translator[u'Staré Město (okres Uherské Hradiště)'] = 'Starém Městě'
        self.translator[u'Strakonice'] = 'Strakonicích'
        self.translator[u'Stříbrná Skalice'] = 'Stříbrné Skalici'
        self.translator[u'Sušice'] = 'Sušici'
        self.translator[u'Svatava (okres Sokolov)'] = 'Svatavě'
        self.translator[u'Svitavy'] = 'Svitavách'
        self.translator[u'Svoboda nad Úpou'] = 'Svobodě nad Úpou'
        #
        self.translator[u'Tábor'] = 'Táboře'
        self.translator[u'Telč'] = 'Telči'
        self.translator[u'Teplice'] = 'Teplicích'
        self.translator[u'Trutnov'] = 'Trutnově'
        self.translator[u'Turnov'] = 'Turnově'
        self.translator[u'Týn nad Vltavou'] = 'Týně nad Vltavou'
        self.translator[u'Třeboň'] = 'Třeboni'
        self.translator[u'Třešť'] = 'Třešti'
        self.translator[u'Třinec'] = 'Třinci'
        #
        self.translator[u'Uherské Hradiště'] = 'Uherském Hradišti'
        self.translator[u'Uherský Brod'] = 'Uherském Brodě'
        self.translator[u'Uničov'] = 'Uničově'
        #
        self.translator[u'Valašské Meziříčí'] = 'Valašském Meziříčí'
        self.translator[u'Valtice'] = 'Valticích'
        self.translator[u'Vamberk'] = 'Vamberku'
        self.translator[u'Varnsdorf'] = 'Varnsdorfu'
        self.translator[u'Velké Popovice'] = 'Velkých Popovicích'
        self.translator[u'Velké Přílepy'] = 'Velkých Přílepech'
        self.translator[u'Veltrusy'] = 'Veltrusech'
        self.translator[u'Velvary'] = 'Velvarech'
        self.translator[u'Veselí nad Lužnicí'] = 'Veselí nad Lužnicí'
        self.translator[u'Vimperk'] = 'Vimperku'
        self.translator[u'Vítkov'] = 'Vítkově'
        self.translator[u'Volyně'] = 'Volyni'
        self.translator[u'Votice'] = 'Voticích'
        self.translator[u'Vrchlabí'] = 'Vrchlabí'
        self.translator[u'Vsetín'] = 'Vsetíně'
        self.translator[u'Vysoké Mýto'] = 'Vysokém Mýtě'
        self.translator[u'Vyškov'] = 'Vyškově'
        #
        self.translator[u'Zákupy'] = 'Zákupech'
        self.translator[u'Zdice'] = 'Zdici'
        self.translator[u'Zlín'] = 'Zlíně'
        self.translator[u'Znojmo'] = 'Znojmě'
        self.translator[u'Zruč nad Sázavou'] = 'Zruči nad Sázavou'
        self.translator[u'Zábřeh'] = 'Zábřehu'
        #
        self.translator[u'Ústí nad Labem'] = 'Ústí nad Labem'
        self.translator[u'Ústí nad Orlicí'] = 'Ústí nad Orlicí'
        self.translator[u'Úštěk'] = 'Úštěku'
        #
        self.translator[u'Čáslav'] = 'Čáslavi'
        self.translator[u'Černošice'] = 'Černošicích'
        self.translator[u'Česká Lípa'] = 'České Lípě'
        self.translator[u'Česká Třebová'] = 'České Třebové'
        self.translator[u'České Budějovice'] = 'Českých Budějovicích'
        self.translator[u'Český Brod'] = 'Českém Brodě'
        self.translator[u'Český Dub'] = 'Českém Dubu'
        self.translator[u'Český Krumlov'] = 'Českém Krumlově'
        self.translator[u'Český Těšín'] = 'Českém Těšíně'
        #
        self.translator[u'Řevnice'] = 'Řevnicích'
        self.translator[u'Říčany'] = 'Říčanech'
        #
        self.translator[u'Šluknov'] = 'Šluknově'
        self.translator[u'Šternberk'] = 'Šternberku'
        self.translator[u'Štramberk'] = 'Štramberku'
        self.translator[u'Šumperk'] = 'Šumperku'
        #
        self.translator[u'Žatec'] = 'Žatci'
        self.translator[u'Žďár nad Sázavou'] = 'Žďáru nad Sázavou'
        self.translator[u'Žatec'] = 'Žatci'

        self.translator[u'Abertamy'] = 'Abertamech'
        self.translator[u'Adamov'] = 'Adamově'
        self.translator[u'Albrechtice'] = 'Albrechticích'
        self.translator[u'Albrechtice nad Orlicí'] = 'Albrechticích nad Orlicí'
        self.translator[u'Aš'] = 'Aši'
        self.translator[u'Babice'] = 'Babicích'
        self.translator[u'Babice u Rosic'] = 'Babicích u Rosic'
        self.translator[u'Bakov nad Jizerou'] = 'Bakově nad Jizerou'
        self.translator[u'Bařice-Velké Těšany'] = 'Bařicích-Velkých Těšanech'
        self.translator[u'Bašť'] = 'Bašti'
        self.translator[u'Batelov'] = 'Batelově'
        self.translator[u'Bavorov'] = 'Bavorově'
        self.translator[u'Bavoryně'] = 'Bavoryni'
        self.translator[u'Bdeněves'] = 'Bdeněvsi'
        self.translator[u'Bečov nad Teplou'] = 'Bečově nad Teplou'
        self.translator[u'Bedihošť'] = 'Bedihoti'
        self.translator[u'Bechyně'] = 'Bechyni'
        self.translator[u'Bělá nad Radbuzou'] = 'Bělé nad Radbuzou'
        self.translator[u'Bělá pod Bezdězem'] = 'Bělé pod Bezdězem'
        self.translator[u'Bělčice'] = 'Bělčici'
        self.translator[u'Běleč'] = 'Bělči'
        self.translator[u'Benátky nad Jizerou'] = 'Benátkách nad Jizerou'
        self.translator[u'Benešov'] = 'Benešově'
        self.translator[u'Benešov nad Ploučnicí'] = 'Benešově nad Ploučnicí'
        self.translator[u'Bernartice'] = 'Bernartici'
        self.translator[u'Beroun'] = 'Berouně'
        self.translator[u'Běrunice'] = 'Běrunicích'
        self.translator[u'Besednice'] = 'Besednicích'
        self.translator[u'Bezdružice'] = 'Bezdružicích'
        self.translator[u'Bezno'] = 'Bezně'
        self.translator[u'Bílina'] = 'Bílině'
        self.translator[u'Bílovec'] = 'Bílovci'
        self.translator[u'Bílovice nad Svitavou'] = 'Bílovicích nad Svitavou'
        self.translator[u'Blansko'] = 'Blansku'
        self.translator[u'Blatná'] = 'Blatné'
        self.translator[u'Blažovice'] = 'Blažovicích'
        self.translator[u'Blešno'] = 'Blešně'
        self.translator[u'Blovice'] = 'Blovicích'
        self.translator[u'Blšany'] = 'Blšanech'
        self.translator[u'Blučina'] = 'Blučině'
        self.translator[u'Bludov'] = 'Bludově'
        self.translator[u'Bobnice'] = 'Bobnicích'
        self.translator[u'Bohumín'] = 'Bohumíně'
        self.translator[u'Bohuňovice'] = 'Bohuňovicích'
        self.translator[u'Bohuslavice'] = 'Bohuslavicích'
        self.translator[u'Bohušovice nad Ohří'] = 'Bohušovicích nad Ohří'
        self.translator[u'Bochoř'] = 'Bochoři'
        self.translator[u'Bochov'] = 'Bochově'
        self.translator[u'Bojkovice'] = 'Bojkovicích'
        self.translator[u'Bolatice'] = 'Bolaticích'
        self.translator[u'Bor'] = 'Boru'
        self.translator[u'Borek'] = 'Borku'
        self.translator[u'Borohrádek'] = 'Borohrádku'
        self.translator[u'Borovany'] = 'Borovanech'
        self.translator[u'Boršov nad Vltavou'] = 'Boršově nad Vltavou'
        self.translator[u'Bořanovice'] = 'Bořanovicích'
        self.translator[u'Bořitov'] = 'Bořitově'
        self.translator[u'Boskovice'] = 'Boskovicích'
        self.translator[u'Bošovice'] = 'Bošovicích'
        self.translator[u'Bradlec'] = 'Bradleci'
        self.translator[u'Braňany'] = 'Braňanech'
        self.translator[u'Brandov'] = 'Brandově'
        self.translator[u'Brandýs nad Labem-Stará Boleslav'] = 'Brandýsi nad Labem-Staré Boleslavi'
        self.translator[u'Brandýs nad Orlicí'] = 'Brandýse nad Orlicí'
        self.translator[u'Brandýsek'] = 'Brandýsku'
        self.translator[u'Branka u Opavy'] = 'Brance u Opavy'
        self.translator[u'Brankovice'] = 'Brankovicích'
        self.translator[u'Braškov'] = 'Braškově'
        self.translator[u'Brno'] = 'Brně'
        self.translator[u'Brodce'] = 'Brodci'
        self.translator[u'Brodek u Prostějova'] = 'Brodku u Prostějova'
        self.translator[u'Brodek u Přerova'] = 'Brodku u Přerova'
        self.translator[u'Broumov'] = 'Broumově'
        self.translator[u'Broumy'] = 'Broumách'
        self.translator[u'Brozany nad Ohří'] = 'Brozanech nad Ohří'
        self.translator[u'Brtnice'] = 'Brtnici'
        self.translator[u'Brťov-Jeneč'] = 'Brťově-Jenči'
        self.translator[u'Brumov-Bylnice'] = 'Brumově-Bylnici'
        self.translator[u'Brumovice'] = 'Brumovicích'
        self.translator[u'Bruntál'] = 'Bruntále'
        self.translator[u'Brušperk'] = 'Brušperku'
        self.translator[u'Břeclav'] = 'Břeclavi'
        self.translator[u'Břehy'] = 'Břehách'
        self.translator[u'Březí'] = 'Březím'
        self.translator[u'Březnice'] = 'Březnici'
        self.translator[u'Březno'] = 'Březnu'
        self.translator[u'Březová'] = 'Březové'
        self.translator[u'Březová nad Svitavou'] = 'Březové nad Svitavou'
        self.translator[u'Březová-Oleško'] = 'Březové-Olešku'
        self.translator[u'Břidličná'] = 'Břidličné'
        self.translator[u'Břvany'] = 'Břvanech'
        self.translator[u'Bubovice'] = 'Bubovicích'
        self.translator[u'Bučovice'] = 'Bučovicích'
        self.translator[u'Budišov nad Budišovkou'] = 'Budišově nad Budišovkou'
        self.translator[u'Budišovice'] = 'Budišovicích'
        self.translator[u'Budyně nad Ohří'] = 'Budyni nad Ohří'
        self.translator[u'Buchlovice'] = 'Buchlovicích'
        self.translator[u'Buštěhrad'] = 'Buštěhradě'
        self.translator[u'Bystré'] = 'Bystrém'
        self.translator[u'Bystrovany'] = 'Bystrovanech'
        self.translator[u'Bystřany'] = 'Bystřanech'
        self.translator[u'Bystřice'] = 'Bystřici'
        self.translator[u'Bystřice nad Pernštejnem'] = 'Bystřici nad Pernštejnem'
        self.translator[u'Bystřice pod Hostýnem'] = 'Bystřici pod Hostýnem'
        self.translator[u'Byšice'] = 'Byšici'
        self.translator[u'Bzenec'] = 'Bzenci'
        self.translator[u'Cerhenice'] = 'Cerhenicích'
        self.translator[u'Cerhovice'] = 'Cerhovicích'
        self.translator[u'Cetkovice'] = 'Cetkovicích'
        self.translator[u'Církvice'] = 'Církvicích'
        self.translator[u'Cítoliby'] = 'Cítolibech'
        self.translator[u'Cizkrajov'] = 'Cizkrajově'
        self.translator[u'Cvikov'] = 'Cvikově'
        self.translator[u'Cvrčovice'] = 'Cvrčovicích'
        self.translator[u'Čachovice'] = 'Čachovicích'
        self.translator[u'Čakovičky'] = 'Čakovičkách'
        self.translator[u'Čáslav'] = 'Čáslavi'
        self.translator[u'Častolovice'] = 'Častolovicích'
        self.translator[u'Čavisov'] = 'Čavisově'
        self.translator[u'Čečelice'] = 'Čečelicích'
        self.translator[u'Čechtice'] = 'Čechticích'
        self.translator[u'Čechy pod Kosířem'] = 'Čechách pod Kosířem'
        self.translator[u'Čejkovice'] = 'Čejkovicích'
        self.translator[u'Čelákovice'] = 'Čelákovicích'
        self.translator[u'Čelechovice na Hané'] = 'Čelechovice na Hané'
        self.translator[u'Čeperka'] = 'Čeperkách'
        self.translator[u'Čerčany'] = 'Čerčany'
        self.translator[u'Černá Hora'] = 'Černé Hoře'
        self.translator[u'Černčice'] = 'Černčicích'
        self.translator[u'Černé Voděrady'] = 'Černých Voděradech'
        self.translator[u'Černolice'] = 'Černolicích'
        self.translator[u'Černošice'] = 'Černošicích'
        self.translator[u'Černošín'] = 'Černošíně'
        self.translator[u'Černovice'] = 'Černovicích'
        self.translator[u'Černožice'] = 'Černožicích'
        self.translator[u'Červené Pečky'] = 'Červených Pečkách'
        self.translator[u'Červenka'] = 'Července'
        self.translator[u'Červený Kostelec'] = 'Červeném Kostelci'
        self.translator[u'Červený Újezd'] = 'Červeném Újezdě'
        self.translator[u'Česká Kamenice'] = 'České Kamenici'
        self.translator[u'Česká Lípa'] = 'České Lípě'
        self.translator[u'Česká Skalice'] = 'České Skalici'
        self.translator[u'Česká Třebová'] = 'České Třebové'
        self.translator[u'Česká Ves'] = 'České Vsi'
        self.translator[u'České Budějovice'] = 'Českých Budějovicích'
        self.translator[u'České Meziříčí'] = 'Českém Meziříčí'
        self.translator[u'České Velenice'] = 'Českých Velenicích'
        self.translator[u'Český Brod'] = 'Českém Brodě'
        self.translator[u'Český Dub'] = 'Českém Dubu'
        self.translator[u'Český Krumlov'] = 'Českém Krumlově'
        self.translator[u'Český Těšín'] = 'Českém Těšíně'
        self.translator[u'Čestlice'] = 'Čestlicích'
        self.translator[u'Číchov'] = 'Číchově'
        self.translator[u'Čistá'] = 'Čisté'
        self.translator[u'Čížkovice'] = 'Čížkovicích'
        self.translator[u'Dačice'] = 'Dačicích'
        self.translator[u'Dalovice'] = 'Dalovicích'
        self.translator[u'Dambořice'] = 'Dambořicích'
        self.translator[u'Darkovice'] = 'Darkovicích'
        self.translator[u'Dašice'] = 'Dašicích'
        self.translator[u'Davle'] = 'Davli'
        self.translator[u'Děčín'] = 'Děčíně'
        self.translator[u'Děhylov'] = 'Děhylově'
        self.translator[u'Desná'] = 'Desné'
        self.translator[u'Deštná'] = 'Deštné'
        self.translator[u'Divec'] = 'Divci'
        self.translator[u'Divišov'] = 'Divišově'
        self.translator[u'Dlouhá Loučka'] = 'Dlouhé Loučce'
        self.translator[u'Dlouhá Třebová'] = 'Dlouhé Třebové'
        self.translator[u'Dlouhoňovice'] = 'Dlouhoňovicích'
        self.translator[u'Dlouhopolsko'] = 'Dlouhopolsku'
        self.translator[u'Dobrá Voda u Českých Budějovic'] = 'Dobré Vodě u Českých Budějovic'
        self.translator[u'Dobroměřice'] = 'Dobroměřicích'
        self.translator[u'Dobromilice'] = 'Dobromilicích'
        self.translator[u'Dobronín'] = 'Dobroníně'
        self.translator[u'Dobroslavice'] = 'Dobroslavicích'
        self.translator[u'Dobrovice'] = 'Dobrovicích'
        self.translator[u'Dobrovíz'] = 'Dobrovízi'
        self.translator[u'Dobruška'] = 'Dobrušce'
        self.translator[u'Dobřany'] = 'Dobřanech'
        self.translator[u'Dobřejovice'] = 'Dobřejovicích'
        self.translator[u'Dobřichovice'] = 'Dobřichovicích'
        self.translator[u'Dobříň'] = 'Dobříni'
        self.translator[u'Dobříš'] = 'Dobříši'
        self.translator[u'Dobšice'] = 'Dobšicích'
        self.translator[u'Dobšice'] = 'Dobšicích'
        self.translator[u'Doksy'] = 'Doksech'
        self.translator[u'Dolany nad Vltavou'] = 'Dolanech nad Vltavou'
        self.translator[u'Dolní Benešov'] = 'Dolním Benešově'
        self.translator[u'Dolní Beřkovice'] = 'Dolních Beřkovicích'
        self.translator[u'Dolní Bojanovice'] = 'Dolních Bojanovicích'
        self.translator[u'Dolní Bousov'] = 'Dolním Bousově'
        self.translator[u'Dolní Břežany'] = 'Dolních Břežanech'
        self.translator[u'Dolní Bukovsko'] = 'Dolním Bukovsko'
        self.translator[u'Dolní Dunajovice'] = 'Dolních Dunajovicích'
        self.translator[u'Dolní Dvořiště'] = 'Dolním Dvořiště'
        self.translator[u'Dolní Kounice'] = 'Dolních Kounicích'
        self.translator[u'Dolní Kralovice'] = 'Dolních Kralovicích'
        self.translator[u'Dolní Lhota'] = 'Dolní Lhotě'
        self.translator[u'Dolní Lutyně'] = 'Dolní Lutyni'
        self.translator[u'Dolní Němčí'] = 'Dolním Němčí'
        self.translator[u'Dolní Poustevna'] = 'Dolní Poustevně'
        self.translator[u'Dolní Radechová'] = 'Dolní Radechové'
        self.translator[u'Dolní Rychnov'] = 'Dolním Rychnově'
        self.translator[u'Dolní Ředice'] = 'Dolních Ředicích'
        self.translator[u'Dolní Zálezly'] = 'Dolních Zálezlech'
        self.translator[u'Dolní Životice'] = 'Dolních Životicích'
        self.translator[u'Domašov'] = 'Domašově'
        self.translator[u'Domašov nad Bystřicí'] = 'Domašov nad Bystřicí'
        self.translator[u'Domažlice'] = 'Domažlicích'
        self.translator[u'Doubravčice'] = 'Doubravčicích'
        self.translator[u'Doubravice nad Svitavou'] = 'Doubravicích nad Svitavou'
        self.translator[u'Doudleby nad Orlicí'] = 'Doudlebech nad Orlicí'
        self.translator[u'Drahelčice'] = 'Drahelčicích'
        self.translator[u'Dražeň'] = 'Draženi'
        self.translator[u'Dražovice'] = 'Dražovicích'
        self.translator[u'Drmoul'] = 'Drmouli'
        self.translator[u'Drnholec'] = 'Drnholci'
        self.translator[u'Droužkovice'] = 'Droužkovicích'
        self.translator[u'Družec'] = 'Družci'
        self.translator[u'Držovice'] = 'Držovicích'
        self.translator[u'Dřevohostice'] = 'Dřevohosticích'
        self.translator[u'Dřísy'] = 'Dřísech'
        self.translator[u'Dub nad Moravou'] = 'Dubu nad Moravou'
        self.translator[u'Dubá'] = 'Dubé'
        self.translator[u'Dubí'] = 'Dubí'
        self.translator[u'Dubicko'] = 'Dubicku'
        self.translator[u'Dubňany'] = 'Dubňanech'
        self.translator[u'Dubno'] = 'Dubně'
        self.translator[u'Duchcov'] = 'Duchcově'
        self.translator[u'Dvorce'] = 'Dvorcích'
        self.translator[u'Dvůr Králové nad Labem'] = 'Dvoře Králové nad Labem'
        self.translator[u'Dyjice'] = 'Dyjicích'
        self.translator[u'Dymokury'] = 'Dymokurech'
        self.translator[u'Dýšina'] = 'Dýšině'
        self.translator[u'Ejpovice'] = 'Ejpovicích'
        self.translator[u'Františkovy Lázně'] = 'Františkových Lázních'
        self.translator[u'Frenštát pod Radhoštěm'] = 'Frenštátě pod Radhoštěm'
        self.translator[u'Frýdek-Místek'] = 'Frýdku-Místku'
        self.translator[u'Frýdlant'] = 'Frýdlantě'
        self.translator[u'Frýdlant nad Ostravicí'] = 'Frýdlantě nad Ostravicí'
        self.translator[u'Fryšták'] = 'Fryštáku'
        self.translator[u'Fulnek'] = 'Fulneku'
        self.translator[u'Golčův Jeníkov'] = 'Golčově Jeníkově'
        self.translator[u'Grygov'] = 'Grygově'

        self.translator[u'Habartov'] = 'Habartově'
        self.translator[u'Habry'] = 'Habrech'
        self.translator[u'Háj u Duchcova'] = 'Háji u Duchcova'
        self.translator[u'Háj ve Slezsku'] = 'Háji ve Slezsku'
        self.translator[u'Halže'] = 'Halži'
        self.translator[u'Hamr na Jezeře'] = 'Hamru na Jezeře'
        self.translator[u'Hanušovice'] = 'Hanušovicích'
        self.translator[u'Hať'] = 'Hati'
        self.translator[u'Havířov'] = 'Havířově'
        self.translator[u'Havlíčkova Borová'] = 'Havlíčkově Borové'
        self.translator[u'Havlíčkův Brod'] = 'Havlíčkově Brodě'
        self.translator[u'Hejnice'] = 'Hejnicích'
        self.translator[u'Herink'] = 'Herinku'
        self.translator[u'Heřmanova Huť'] = 'Heřmanově Huti'
        self.translator[u'Heřmanův Městec'] = 'Heřmanově Městci'
        self.translator[u'Hladké Životice'] = 'Hladkých Životicích'
        self.translator[u'Hlásná Třebaň'] = 'Hlásné Třebaňi'
        self.translator[u'Hlincová Hora'] = 'Hlincové Hoře'
        self.translator[u'Hlinsko'] = 'Hlinsku'
        self.translator[u'Hlohovec'] = 'Hlohovci'
        self.translator[u'Hlubočky'] = 'Hlubočkách'
        self.translator[u'Hluboká nad Vltavou'] = 'Hluboké nad Vltavou'
        self.translator[u'Hlučín'] = 'Hlučíně'
        self.translator[u'Hluk'] = 'Hluku'
        self.translator[u'Hlušovice'] = 'Hlušovicích'
        self.translator[u'Hnanice'] = 'Hnanicích'
        self.translator[u'Hněvošice'] = 'Hněvošicích'
        self.translator[u'Hodějice'] = 'Hodějicích'
        self.translator[u'Hodkovice nad Mohelkou'] = 'Hodkovicích nad Mohelkou'
        self.translator[u'Hodonice'] = 'Hodonicích'
        self.translator[u'Hodonín'] = 'Hodoníně'
        self.translator[u'Holasice'] = 'Holasicích'
        self.translator[u'Holedeč'] = 'Holedeč'
        self.translator[u'Holešov'] = 'Holešově'
        self.translator[u'Holice'] = 'Holicích'
        self.translator[u'Holohlavy'] = 'Holohlavech'
        self.translator[u'Holubice'] = 'Holubicích'
        self.translator[u'Holýšov'] = 'Holýšově'
        self.translator[u'Homole'] = 'Homoli'
        self.translator[u'Homole u Panny'] = 'Homoli u Panny'
        self.translator[u'Hora Svaté Kateřiny'] = 'Hoře Svaté Kateřiny'
        self.translator[u'Horažďovice'] = 'Horažďovicích'
        self.translator[u'Horka nad Moravou'] = 'Horce nad Moravou'
        self.translator[u'Horní Benešov'] = 'Horním Benešově'
        self.translator[u'Horní Beřkovice'] = 'Horních Beřkovicích'
        self.translator[u'Horní Bezděkov'] = 'Horním Bezděkově'
        self.translator[u'Horní Blatná'] = 'Horní Blatné'
        self.translator[u'Horní Bříza'] = 'Horní Bříze'
        self.translator[u'Horní Cerekev'] = 'Horní Cerekvi'
        self.translator[u'Horní Dubenky'] = 'Horních Dubenkách'
        self.translator[u'Horní Dvořiště'] = 'Horním Dvořišti'
        self.translator[u'Horní Jelení'] = 'Horním Jelení'
        self.translator[u'Horní Jiřetín'] = 'Horním Jiřetíně'
        self.translator[u'Horní Lhota'] = 'Horní Lhotě'
        self.translator[u'Horní Maršov'] = 'Horním Maršově'
        self.translator[u'Horní Moštěnice'] = 'Horních Moštěnicích'
        self.translator[u'Horní Planá'] = 'Horní Plané'
        self.translator[u'Horní Police'] = 'Horní Polici'
        self.translator[u'Horní Slavkov'] = 'Horním Slavkově'
        self.translator[u'Horní Suchá'] = 'Horní Suché'
        self.translator[u'Horní Věstonice'] = 'Horních Věstonicích'
        self.translator[u'Horoměřice'] = 'Horoměřicích'
        self.translator[u'Horoušany'] = 'Horoušanech'
        self.translator[u'Horšice'] = 'Horšicích'
        self.translator[u'Horšovský Týn'] = 'Horšovském Týně'
        self.translator[u'Hořepník'] = 'Hořepníku'
        self.translator[u'Hořice'] = 'Hořicích'
        self.translator[u'Hořín'] = 'Hoříně'
        self.translator[u'Hořovice'] = 'Hořovicích'
        self.translator[u'Hostinné'] = 'Hostinném'
        self.translator[u'Hostivice'] = 'Hostivici'
        self.translator[u'Hostomice'] = 'Hostomicích'
        self.translator[u'Hostomice'] = 'Hostomicích'
        self.translator[u'Hostouň'] = 'Hostouni'
        self.translator[u'Hostouň'] = 'Hostouni'
        self.translator[u'Hoštka'] = 'Hoštce'
        self.translator[u'Hovorčovice'] = 'Hovorčovicích'
        self.translator[u'Hrabětice'] = 'Hraběticích'
        self.translator[u'Hradčany'] = 'Hradčanech'
        self.translator[u'Hradec Králové'] = 'Hradci Králové'
        self.translator[u'Hradec nad Moravicí'] = 'Hradci nad Moravicí'
        self.translator[u'Hrádek'] = 'Hrádku'
        self.translator[u'Hrádek'] = 'Hrádku'
        self.translator[u'Hrádek nad Nisou'] = 'Hrádku nad Nisou'
        self.translator[u'Hradešín'] = 'Hradešíně'
        self.translator[u'Hradiště'] = 'Hradišti'
        self.translator[u'Hradištko'] = 'Hradištko'
        self.translator[u'Hradištko'] = 'Hradištko'
        self.translator[u'Hranice'] = 'Hranicích'
        self.translator[u'Hranice'] = 'Hranicích'
        self.translator[u'Hrdějovice'] = 'Hrdějovicích'
        self.translator[u'Hrob'] = 'Hrobě'
        self.translator[u'Hrobce'] = 'Hrobcích'
        self.translator[u'Hrochův Týnec'] = 'Hrochově Týnci'
        self.translator[u'Hronov'] = 'Hronově'
        self.translator[u'Hrotovice'] = 'Hrotovicích'
        self.translator[u'Hroznětín'] = 'Hroznětíně'
        self.translator[u'Hrušky'] = 'Hruškách'
        self.translator[u'Hrušovany nad Jevišovkou'] = 'Hrušovanech nad Jevišovkou'
        self.translator[u'Hrušovany u Brna'] = 'Hrušovanech u Brna'
        self.translator[u'Hřebeč'] = 'Hřebči'
        self.translator[u'Hudlice'] = 'Hudlicích'
        self.translator[u'Hulín'] = 'Hulíně'
        self.translator[u'Humpolec'] = 'Humpolci'
        self.translator[u'Hůry'] = 'Hůrech'
        self.translator[u'Husinec'] = 'Husinci'
        self.translator[u'Husinec'] = 'Husinci'
        self.translator[u'Hustopeče'] = 'Hustopečích'
        self.translator[u'Hustopeče nad Bečvou'] = 'Hustopečích nad Bečvou'
        self.translator[u'Hvozdná'] = 'Hvozdné'
        self.translator[u'Hýskov'] = 'Hýskově'
        self.translator[u'Chabařovice'] = 'Chabařovicích'
        self.translator[u'Cheb'] = 'Chebu'
        self.translator[u'Chlebičov'] = 'Chlebičově'
        self.translator[u'Chleby'] = 'Chlebech'
        self.translator[u'Chlum Svaté Maří'] = 'Chlumě Svaté Maří'
        self.translator[u'Chlum u Třeboně'] = 'Chlumě u Třeboně'
        self.translator[u'Chlumčany'] = 'Chlumčanech'
        self.translator[u'Chlumec'] = 'Chlumci'
        self.translator[u'Chlumec nad Cidlinou'] = 'Chlumci nad Cidlinou'
        self.translator[u'Choceň'] = 'Chocni'
        self.translator[u'Chodov'] = 'Chodově'
        self.translator[u'Chodová Planá'] = 'Chodové Plané'
        self.translator[u'Choltice'] = 'Cholticích'
        self.translator[u'Chomutov'] = 'Chomutově'
        self.translator[u'Chornice'] = 'Chornicích'
        self.translator[u'Chotěboř'] = 'Chotěboři'
        self.translator[u'Chotěbuz'] = 'Chotěbuzi'
        self.translator[u'Choteč'] = 'Chotči'
        self.translator[u'Chotěšov'] = 'Chotěšově'
        self.translator[u'Chotěšov'] = 'Chotěšově'
        self.translator[u'Chotětov'] = 'Chotětově'
        self.translator[u'Chotoviny'] = 'Chotovinech'
        self.translator[u'Chotutice'] = 'Chotuticích'
        self.translator[u'Chrast'] = 'Chrasti'
        self.translator[u'Chrást'] = 'Chrásti'
        self.translator[u'Chrastava'] = 'Chrastavě'
        self.translator[u'Chrášťany'] = 'Chrášťanech'
        self.translator[u'Chropyně'] = 'Chropyni'
        self.translator[u'Chroustovice'] = 'Chroustovicích'
        self.translator[u'Chrudim'] = 'Chrudimi'
        self.translator[u'Chudenice'] = 'Chudenicích'
        self.translator[u'Chuchelna'] = 'Chuchelně'
        self.translator[u'Chuchelná'] = 'Chuchelné'
        self.translator[u'Chvalčov'] = 'Chvalčově'
        self.translator[u'Chvaletice'] = 'Chvaleticích'
        self.translator[u'Chvalíkovice'] = 'Chvalíkovicích'
        self.translator[u'Chyňava'] = 'Chyňavě'
        self.translator[u'Chýně'] = 'Chýni'
        self.translator[u'Chýnice'] = 'Chýnicích'
        self.translator[u'Chýnov'] = 'Chýnově'
        self.translator[u'Chyše'] = 'Chyši'
        self.translator[u'Ivančice'] = 'Ivančicích'
        self.translator[u'Ivanovice na Hané'] = 'Ivanovicích na Hané'
        self.translator[u'Jablonec nad Nisou'] = 'Jablonci nad Nisou'
        self.translator[u'Jablonné nad Orlicí'] = 'Jablonném nad Orlicí'
        self.translator[u'Jablonné v Podještědí'] = 'Jablonném v Podještědí'
        self.translator[u'Jablunkov'] = 'Jablunkově'
        self.translator[u'Jáchymov'] = 'Jáchymově'
        self.translator[u'Jakubčovice nad Odrou'] = 'Jakubčovicích nad Odrou'
        self.translator[u'Jankov'] = 'Jankově'
        self.translator[u'Janovice nad Úhlavou'] = 'Janovicí nad Úhlavou'
        self.translator[u'Janské Lázně'] = 'Janských Lázních'
        self.translator[u'Jaroměř'] = 'Jaroměři'
        self.translator[u'Jaroměřice nad Rokytnou'] = 'Jaroměřicích nad Rokytnou'
        self.translator[u'Jaroslavice'] = 'Jaroslavicích'
        self.translator[u'Javorník'] = 'Javorníku'
        self.translator[u'Javůrek'] = 'Javůrku'
        self.translator[u'Jedlová'] = 'Jedlové'
        self.translator[u'Jedovnice'] = 'Jedovnicích'
        self.translator[u'Jemnice'] = 'Jemnici'
        self.translator[u'Jeneč'] = 'Jenči'
        self.translator[u'Jenišov'] = 'Jenišově'
        self.translator[u'Jenštejn'] = 'Jenštejně'
        self.translator[u'Jeřmanice'] = 'Jeřmanicích'
        self.translator[u'Jesenice'] = 'Jesenici'
        self.translator[u'Jesenice'] = 'Jesenici'
        self.translator[u'Jeseník'] = 'Jeseníku'
        self.translator[u'Jevany'] = 'Jevanech'
        self.translator[u'Jevíčko'] = 'Jevíčku'
        self.translator[u'Jevišovka'] = 'Jevišovce'
        self.translator[u'Jičín'] = 'Jičíně'
        self.translator[u'Jihlava'] = 'Jihlavě'
        self.translator[u'Jilemnice'] = 'Jilemnici'
        self.translator[u'Jílové'] = 'Jílovém'
        self.translator[u'Jílové u Prahy'] = 'Jílovém u Prahy'
        self.translator[u'Jíloviště'] = 'Jílovišti'
        self.translator[u'Jimramov'] = 'Jimramově'
        self.translator[u'Jince'] = 'Jincích'
        self.translator[u'Jindřichův Hradec'] = 'Jindřichově Hradci'
        self.translator[u'Jinočany'] = 'Jinočanech'
        self.translator[u'Jirkov'] = 'Jirkově'
        self.translator[u'Jirny'] = 'Jirnech'
        self.translator[u'Jiřetín pod Jedlovou'] = 'Jiřetíně pod Jedlovou'
        self.translator[u'Jiřice'] = 'Jiřicích'
        self.translator[u'Jiříkov'] = 'Jiříkově'
        self.translator[u'Jiříkovice'] = 'Jiříkovicích'
        self.translator[u'Jistebnice'] = 'Jistebnicích'
        self.translator[u'Josefov'] = 'Josefově'
        self.translator[u'Kácov'] = 'Kácově'
        self.translator[u'Kačice'] = 'Kačicích'
        self.translator[u'Kačlehy'] = 'Kačlehách'
        self.translator[u'Kadaň'] = 'Kadani'
        self.translator[u'Kájov'] = 'Kájově'
        self.translator[u'Kaliště'] = 'Kališti'
        self.translator[u'Kamenice'] = 'Kamenici'
        self.translator[u'Kamenice nad Lipou'] = 'Kamenici nad Lipou'
        self.translator[u'Kamenický Šenov'] = 'Kamenickém Šenově'
        self.translator[u'Kamenné Žehrovice'] = 'Kamenných Žehrovicích'
        self.translator[u'Kamenný Újezd'] = 'Kamenném Újezdě'
        self.translator[u'Kaplice'] = 'Kaplici'
        self.translator[u'Káraný'] = 'Káraném'
        self.translator[u'Kardašova Řečice'] = 'Kardašově Řečici'
        self.translator[u'Karlík'] = 'Karlíku'
        self.translator[u'Karlovy Vary'] = 'Karlových Varech'
        self.translator[u'Karolinka'] = 'Karolince'
        self.translator[u'Karviná'] = 'Karviné'
        self.translator[u'Kašperské Hory'] = 'Kašperských Horách'
        self.translator[u'Katovice'] = 'Katovicích'
        self.translator[u'Katusice'] = 'Katusicích'
        self.translator[u'Kaznějov'] = 'Kaznějově'
        self.translator[u'Kdyně'] = 'Kdyni'
        self.translator[u'Kejžlice'] = 'Kejžlicích'
        self.translator[u'Kladno'] = 'Kladně'
        self.translator[u'Kladruby'] = 'Kladrubech'
        self.translator[u'Klášterec nad Ohří'] = 'Klášterci nad Ohří'
        self.translator[u'Klatovy'] = 'Klatovech'
        self.translator[u'Klecany'] = 'Klecanech'
        self.translator[u'Kleneč'] = 'Klenči'
        self.translator[u'Klenovice'] = 'Klenovicích'
        self.translator[u'Klíčany'] = 'Klíčanech'
        self.translator[u'Klimkovice'] = 'Klimkovicích'
        self.translator[u'Klíny'] = 'Klínech'
        self.translator[u'Klobouky u Brna'] = 'Kloboukách u Brna'
        self.translator[u'Klobuky'] = 'Klobukách'
        self.translator[u'Kněževes'] = 'Kněževsi'
        self.translator[u'Kněževes'] = 'Kněževsi'
        self.translator[u'Kněžmost'] = 'Kněžmostu'
        self.translator[u'Knyk'] = 'Knyku'
        self.translator[u'Kobeřice'] = 'Kobeřicích'
        self.translator[u'Kobeřice u Brna'] = 'Kobeřicích u Brna'
        self.translator[u'Kobylnice'] = 'Kobylnicích'
        self.translator[u'Kojetice'] = 'Kojeticích'
        self.translator[u'Kojetín'] = 'Kojetíně'
        self.translator[u'Kolín'] = 'Kolíně'
        self.translator[u'Koloveč'] = 'Kolovči'
        self.translator[u'Komárov'] = 'Komárově'
        self.translator[u'Komorovice'] = 'Komorovicích'
        self.translator[u'Konárovice'] = 'Konárovicích'
        self.translator[u'Konice'] = 'Konicích'
        self.translator[u'Konstantinovy Lázně'] = 'Konstantinových Lázních'
        self.translator[u'Kopidlno'] = 'Kopidlně'
        self.translator[u'Kopřivnice'] = 'Kopřivnici'
        self.translator[u'Koryčany'] = 'Koryčanech'
        self.translator[u'Kosmonosy'] = 'Kosmonosech'
        self.translator[u'Kosoř'] = 'Kosoři'
        self.translator[u'Kostelec'] = 'Kostelci'
        self.translator[u'Kostelec na Hané'] = 'Kostelci na Hané'
        self.translator[u'Kostelec nad Černými lesy'] = 'Kostelci nad Černými lesy'
        self.translator[u'Kostelec nad Labem'] = 'Kostelci nad Labem'
        self.translator[u'Kostelec nad Orlicí'] = 'Kostelci nad Orlicí'
        self.translator[u'Kostěnice'] = 'Kostěnicích'
        self.translator[u'Kostice'] = 'Kosticích'
        self.translator[u'Kostomlátky'] = 'Kostomlátkách'
        self.translator[u'Kostomlaty nad Labem'] = 'Kostomlatech nad Labem'
        self.translator[u'Kostomlaty pod Milešovkou'] = 'Kostomlatech pod Milešovkou'
        self.translator[u'Košetice'] = 'Košeticích'
        self.translator[u'Košťany'] = 'Košťanech'
        self.translator[u'Kouřim'] = 'Kouřimi'
        self.translator[u'Kovářská'] = 'Kovářské'
        self.translator[u'Kozmice'] = 'Kozmicích'
        self.translator[u'Kozojedy'] = 'Kozojedech'
        self.translator[u'Kožichovice'] = 'Kožichovicích'
        self.translator[u'Kožlany'] = 'Kožlanech'
        self.translator[u'Kralice na Hané'] = 'Kralicích na Hané'
        self.translator[u'Kralice nad Oslavou'] = 'Kralicích nad Oslavou'
        self.translator[u'Králíky'] = 'Králíkách'
        self.translator[u'Kralovice'] = 'Kralovicích'
        self.translator[u'Královské Poříčí'] = 'Královském Poříčí'
        self.translator[u'Kralupy nad Vltavou'] = 'Kralupech nad Vltavou'
        self.translator[u'Králův Dvůr'] = 'Králově Dvoře'
        self.translator[u'Kraslice'] = 'Kraslicích'
        self.translator[u'Krásná Hora'] = 'Krásné Hoře'
        self.translator[u'Krásná Lípa'] = 'Krásné Lípě'
        self.translator[u'Krásno'] = 'Krásně'
        self.translator[u'Kravaře'] = 'Kravařích'
        self.translator[u'Kravaře'] = 'Kravařích'
        self.translator[u'Krčmaň'] = 'Krčmani'
        self.translator[u'Krhanice'] = 'Krhanicích'
        self.translator[u'Krhová'] = 'Krhové'
        self.translator[u'Krchleby'] = 'Krchlebech'
        self.translator[u'Krmelín'] = 'Krmelíně'
        self.translator[u'Krnov'] = 'Krnově'
        self.translator[u'Kroměříž'] = 'Kroměříži'
        self.translator[u'Krucemburk'] = 'Krucemburku'
        self.translator[u'Krupka'] = 'Krupce'
        self.translator[u'Krušovice'] = 'Krušovicích'
        self.translator[u'Kryry'] = 'Kryrech'
        self.translator[u'Křelov-Břuchotín'] = 'Křelově-Břuchotíně'
        self.translator[u'Křemže'] = 'Křemži'
        self.translator[u'Křenice'] = 'Křenicích'
        self.translator[u'Křenovice'] = 'Křenovicích'
        self.translator[u'Křešice'] = 'Křešicích'
        self.translator[u'Křinec'] = 'Křinci'
        self.translator[u'Křižanov'] = 'Křižanově'
        self.translator[u'Kuchařovice'] = 'Kuchařovicích'
        self.translator[u'Kunice'] = 'Kunicích'
        self.translator[u'Kunovice'] = 'Kunovicích'
        self.translator[u'Kunštát'] = 'Kunštátě'
        self.translator[u'Kunžak'] = 'Kunžaku'
        self.translator[u'Kuřim'] = 'Kuřimi'
        self.translator[u'Kutná Hora'] = 'Kutné Hoře'
        self.translator[u'Kvasice'] = 'Kvasicích'
        self.translator[u'Květnice'] = 'Květnicích'
        self.translator[u'Kyjov'] = 'Kyjově'
        self.translator[u'Kyjov'] = 'Kyjově'
        self.translator[u'Kynšperk nad Ohří'] = 'Kynšperku nad Ohří'
        self.translator[u'Kyšice'] = 'Kyšicích'
        self.translator[u'Kyšice'] = 'Kyšicích'
        self.translator[u'Kyškovice'] = 'Kyškovicích'
        self.translator[u'Ladná'] = 'Ladné'
        self.translator[u'Lahošť'] = 'Lahošti'
        self.translator[u'Lanškroun'] = 'Lanškrouně'
        self.translator[u'Lány'] = 'Lánech'
        self.translator[u'Lány'] = 'Lánech'
        self.translator[u'Lanžhot'] = 'Lanžhotě'
        self.translator[u'Lanžov'] = 'Lanžově'
        self.translator[u'Lázně Bělohrad'] = 'Lázních Bělohrad'
        self.translator[u'Lázně Bohdaneč'] = 'Lázních Bohdaneč'
        self.translator[u'Lázně Kynžvart'] = 'Lázních Kynžvart'
        self.translator[u'Lázně Toušeň'] = 'Lázních Toušeň'
        self.translator[u'Ledeč nad Sázavou'] = 'Ledči nad Sázavou'
        self.translator[u'Ledenice'] = 'Ledenicích'
        self.translator[u'Lednice'] = 'Lednici'
        self.translator[u'Ledvice'] = 'Ledvicích'
        self.translator[u'Lelekovice'] = 'Lelekovicích'
        self.translator[u'Lenešice'] = 'Lenešicích'
        self.translator[u'Leština'] = 'Leštině'
        self.translator[u'Letkov'] = 'Letkově'
        self.translator[u'Letohrad'] = 'Letohradě'
        self.translator[u'Letonice'] = 'Letonicích'
        self.translator[u'Letovice'] = 'Letovicích'
        self.translator[u'Lety'] = 'Letech'
        self.translator[u'Lhenice'] = 'Lhenicích'
        self.translator[u'Lhota'] = 'Lhotě'
        self.translator[u'Lhota'] = 'Lhotě'
        self.translator[u'Lhotka'] = 'Lhotce'
        self.translator[u'Libáň'] = 'Libáni'
        self.translator[u'Libčeves'] = 'Libčevsi'
        self.translator[u'Libčice nad Vltavou'] = 'Libčice nad Vltavou'
        self.translator[u'Liběchov'] = 'Liběchově'
        self.translator[u'Liberec'] = 'Liberci'
        self.translator[u'Líbeznice'] = 'Líbeznicích'
        self.translator[u'Libice nad Cidlinou'] = 'Libici nad Cidlinou'
        self.translator[u'Libice nad Doubravou'] = 'Libici nad Doubravou'
        self.translator[u'Libiš'] = 'Libiši'
        self.translator[u'Libníč'] = 'Libníči'
        self.translator[u'Libochovice'] = 'Libochovicích'
        self.translator[u'Liboš'] = 'Liboši'
        self.translator[u'Libušín'] = 'Libušíně'
        self.translator[u'Lidice'] = 'Lidicích'
        self.translator[u'Líně'] = 'Líni'
        self.translator[u'Lipník nad Bečvou'] = 'Lipníku nad Bečvou'
        self.translator[u'Lipová'] = 'Lipové'
        self.translator[u'Lišany'] = 'Lišanech'
        self.translator[u'Líšnice'] = 'Líšnicích'
        self.translator[u'Lišov'] = 'Lišově'
        self.translator[u'Líšťany'] = 'Líšťanech'
        self.translator[u'Liteň'] = 'Litni'
        self.translator[u'Litoměřice'] = 'Litoměřicích'
        self.translator[u'Litomyšl'] = 'Litomyšli'
        self.translator[u'Litovel'] = 'Litoveli'
        self.translator[u'Litvínov'] = 'Litvínově'
        self.translator[u'Litvínovice'] = 'Litvínovicích'
        self.translator[u'Loděnice'] = 'Loděnicích'
        self.translator[u'Lochovice'] = 'Lochovicích'
        self.translator[u'Loket'] = 'Lokti'
        self.translator[u'Lom'] = 'Lomu'
        self.translator[u'Lomnice'] = 'Lomnici'
        self.translator[u'Lomnice'] = 'Lomnici'
        self.translator[u'Lomnice nad Lužnicí'] = 'Lomnici nad Lužnicí'
        self.translator[u'Lomnice nad Popelkou'] = 'Lomnici nad Popelkou'
        self.translator[u'Loštice'] = 'Lošticích'
        self.translator[u'Loučeň'] = 'Loučeni'
        self.translator[u'Louka u Litvínova'] = 'Louce u Litvínova'
        self.translator[u'Louňovice'] = 'Louňovicích'
        self.translator[u'Louňovice pod Blaníkem'] = 'Louňovicích pod Blaníkem'
        self.translator[u'Louny'] = 'Lounech'
        self.translator[u'Lovosice'] = 'Lovosicích'
        self.translator[u'Lštění'] = 'Lštění'
        self.translator[u'Lubenec'] = 'Lubenci'
        self.translator[u'Luby'] = 'Lubech'
        self.translator[u'Ludgeřovice'] = 'Ludgeřovicích'
        self.translator[u'Luhačovice'] = 'Luhačovicích'
        self.translator[u'Luka nad Jihlavou'] = 'Lukách nad Jihlavou'
        self.translator[u'Lukavec'] = 'Lukavci'
        self.translator[u'Lukov'] = 'Lukově'
        self.translator[u'Lukoveček'] = 'Lukovečku'
        self.translator[u'Luštěnice'] = 'Luštěnicích'
        self.translator[u'Lutín'] = 'Lutíně'
        self.translator[u'Luže'] = 'Luži'
        self.translator[u'Lužec nad Vltavou'] = 'Lužci nad Vltavou'
        self.translator[u'Lužice'] = 'Lužicích'
        self.translator[u'Lužná'] = 'Lužné'
        self.translator[u'Lysá nad Labem'] = 'Lysé nad Labem'
        self.translator[u'Lysice'] = 'Lysicích'
        self.translator[u'Majetín'] = 'Majetíně'
        self.translator[u'Malá Skála'] = 'Malé Skále'
        self.translator[u'Malé Kyšice'] = 'Malých Kyšicích'
        self.translator[u'Malé Přítočno'] = 'Malém Přítočně'
        self.translator[u'Malé Svatoňovice'] = 'Malých Svatoňovicích'
        self.translator[u'Malé Žernoseky'] = 'Malých Žernosekách'
        self.translator[u'Malenice'] = 'Malenicích'
        self.translator[u'Malešov'] = 'Malešově'
        self.translator[u'Malíč'] = 'Malíči'
        self.translator[u'Malovice'] = 'Malovicích'
        self.translator[u'Malšice'] = 'Malšicích'
        self.translator[u'Mariánské Lázně'] = 'Mariánských Lázních'
        self.translator[u'Mariánské Radčice'] = 'Mariánských Radčicích'
        self.translator[u'Markvartovice'] = 'Markvartovicích'
        self.translator[u'Martínkovice'] = 'Martínkovicích'
        self.translator[u'Máslovice'] = 'Máslovicích'
        self.translator[u'Mašťov'] = 'Mašťově'
        self.translator[u'Měčín'] = 'Měčíně'
        self.translator[u'Měděnec'] = 'Měděnci'
        self.translator[u'Měchenice'] = 'Měchenicích'
        self.translator[u'Mělnické Vtelno'] = 'Mělnickém Vtelně'
        self.translator[u'Mělník'] = 'Mělníku'
        self.translator[u'Merklín'] = 'Merklíně'
        self.translator[u'Měřín'] = 'Měříně'
        self.translator[u'Městec Králové'] = 'Městci Králové'
        self.translator[u'Městečko'] = 'Městečku'
        self.translator[u'Město Albrechtice'] = 'Městě Albrechtice'
        self.translator[u'Město Libavá'] = 'Městě Libavá'
        self.translator[u'Město Touškov'] = 'Městě Touškově'
        self.translator[u'Měšice'] = 'Měšicích'
        self.translator[u'Meziboří'] = 'Meziboří'
        self.translator[u'Meziměstí'] = 'Meziměstí'
        self.translator[u'Mikulov'] = 'Mikulově'
        self.translator[u'Mikulov'] = 'Mikulově'
        self.translator[u'Mikulovice'] = 'Mikulovicích'
        self.translator[u'Mikulovice'] = 'Mikulovicích'
        self.translator[u'Miletín'] = 'Miletíně'
        self.translator[u'Milevsko'] = 'Milevsku'
        self.translator[u'Milín'] = 'Milíně'
        self.translator[u'Milotice'] = 'Miloticích'
        self.translator[u'Milovice'] = 'Milovicích'
        self.translator[u'Mimoň'] = 'Mimoni'
        self.translator[u'Miroslav'] = 'Miroslavi'
        self.translator[u'Mirošov'] = 'Mirošově'
        self.translator[u'Mirošovice'] = 'Mirošovicích'
        self.translator[u'Mirotice'] = 'Miroticích'
        self.translator[u'Mírová pod Kozákovem'] = 'Mírové pod Kozákovem'
        self.translator[u'Mirovice'] = 'Mirovicích'
        self.translator[u'Miřejovice'] = 'Miřejovicích'
        self.translator[u'Mířkov'] = 'Mířkově'
        self.translator[u'Mladá Boleslav'] = 'Mladé Boleslavi'
        self.translator[u'Mladá Vožice'] = 'Mladé Vožici'
        self.translator[u'Mlázovice'] = 'Mlázovicích'
        self.translator[u'Mnichovice'] = 'Mnichovicích'
        self.translator[u'Mnichovo Hradiště'] = 'Mnichově Hradišti'
        self.translator[u'Mníšek'] = 'Mníšku'
        self.translator[u'Mníšek pod Brdy'] = 'Mníšku pod Brdy'
        self.translator[u'Modletice'] = 'Modleticích'
        self.translator[u'Modřice'] = 'Modřicích'
        self.translator[u'Mohelnice'] = 'Mohelnicích'
        self.translator[u'Mochov'] = 'Mochově'
        self.translator[u'Mokré'] = 'Mokré'
        self.translator[u'Mokré Lazce'] = 'Mokrých Lazcích'
        self.translator[u'Moravany'] = 'Moravanech'
        self.translator[u'Moravany'] = 'Moravanech'
        self.translator[u'Moravská Nová Ves'] = 'Moravské Nové Vsi'
        self.translator[u'Moravská Třebová'] = 'Moravské Třebové'
        self.translator[u'Moravské Budějovice'] = 'Moravských Budějovicích'
        self.translator[u'Moravské Knínice'] = 'Moravských Knínicích'
        self.translator[u'Moravský Beroun'] = 'Moravském Berouně'
        self.translator[u'Moravský Krumlov'] = 'Moravském Krumlově'
        self.translator[u'Moravský Písek'] = 'Moravském Písku'
        self.translator[u'Moravský Žižkov'] = 'Moravském Žižkově'
        self.translator[u'Morkovice-Slížany'] = 'Morkovicích-Slížanech'
        self.translator[u'Mořkov'] = 'Mořkově'
        self.translator[u'Most'] = 'Mostě'
        self.translator[u'Mostkovice'] = 'Mostkovicích'
        self.translator[u'Mošnov'] = 'Mošnově'
        self.translator[u'Mouchnice'] = 'Mouchnicích'
        self.translator[u'Mrač'] = 'Mrači'
        self.translator[u'Mratín'] = 'Mratíně'
        self.translator[u'Mšené-lázně'] = 'Mšené-lázních'
        self.translator[u'Mšeno'] = 'Mšeně'
        self.translator[u'Mukařov'] = 'Mukařově'
        self.translator[u'Mutějovice'] = 'Mutějovicích'
        self.translator[u'Mutěnice'] = 'Mutěnicích'
        self.translator[u'Mýto'] = 'Mýtě'
        self.translator[u'Načeradec'] = 'Načeradci'
        self.translator[u'Nadějkov'] = 'Nadějkově'
        self.translator[u'Náchod'] = 'Náchodě'
        self.translator[u'Náměšť na Hané'] = 'Náměšti na Hané'
        self.translator[u'Náměšť nad Oslavou'] = 'Náměšti nad Oslavou'
        self.translator[u'Napajedla'] = 'Napajedlech'
        self.translator[u'Nasavrky'] = 'Nasavrkách'
        self.translator[u'Návsí'] = 'Návsí'
        self.translator[u'Neděliště'] = 'Nedělišti'
        self.translator[u'Nehvizdy'] = 'Nehvizdech'
        self.translator[u'Nechanice'] = 'Nechanicích'
        self.translator[u'Nejdek'] = 'Nejdku'
        self.translator[u'Nelahozeves'] = 'Nelahozevsi'
        self.translator[u'Němčice nad Hanou'] = 'Němčicích nad Hanou'
        self.translator[u'Neplachovice'] = 'Neplachovicích'
        self.translator[u'Nepomuk'] = 'Nepomuku'
        self.translator[u'Neratov'] = 'Neratově'
        self.translator[u'Neratovice'] = 'Neratovicích'
        self.translator[u'Neslovice'] = 'Neslovicích'
        self.translator[u'Nespeky'] = 'Nespekách'
        self.translator[u'Netolice'] = 'Netolicích'
        self.translator[u'Netvořice'] = 'Netvořicích'
        self.translator[u'Neuměř'] = 'Neuměři'
        self.translator[u'Neumětely'] = 'Neumětelech'
        self.translator[u'Neveklov'] = 'Neveklově'
        self.translator[u'Nezamyslice'] = 'Nezamyslicích'
        self.translator[u'Nivnice'] = 'Nivnicích'
        self.translator[u'Nižbor'] = 'Nižboru'
        self.translator[u'Nižní Lhoty'] = 'Nižních Lhotách'
        self.translator[u'Nosislav'] = 'Nosislavi'
        self.translator[u'Nošovice'] = 'Nošovicích'
        self.translator[u'Nová Bystřice'] = 'Nové Bystřici'
        self.translator[u'Nová Paka'] = 'Nové Pace'
        self.translator[u'Nová Role'] = 'Nové Roli'
        self.translator[u'Nová Říše'] = 'Nové Říši'
        self.translator[u'Nová Včelnice'] = 'Nové Včelnici'
        self.translator[u'Nová Ves'] = 'Nové Vsi'
        self.translator[u'Nová Ves I'] = 'Nové Vsi I'
        self.translator[u'Nová Ves pod Pleší'] = 'Nové Vsi pod Pleší'
        self.translator[u'Nové Dvory'] = 'Nových Dvorech'
        self.translator[u'Nové Hrady'] = 'Nových Hradech'
        self.translator[u'Nové Město na Moravě'] = 'Novém Městě na Moravě'
        self.translator[u'Nové Město nad Metují'] = 'Novém Městě nad Metují'
        self.translator[u'Nové Město pod Smrkem'] = 'Novém Městě pod Smrkem'
        self.translator[u'Nové Sedlice'] = 'Nových Sedlicích'
        self.translator[u'Nové Sedlo'] = 'Novém Sedle'
        self.translator[u'Nové Sedlo'] = 'Novém Sedle'
        self.translator[u'Nové Strašecí'] = 'Novém Strašecí'
        self.translator[u'Nové Veselí'] = 'Novém Veselí'
        self.translator[u'Novosedlice'] = 'Novosedlicích'
        self.translator[u'Nový Bor'] = 'Novém Boru'
        self.translator[u'Nový Bydžov'] = 'Novém Bydžově'
        self.translator[u'Nový Hrádek'] = 'Novém Hrádku'
        self.translator[u'Nový Jáchymov'] = 'Novém Jáchymově'
        self.translator[u'Nový Jičín'] = 'Novém Jičíně'
        self.translator[u'Nový Knín'] = 'Novém Kníně'
        self.translator[u'Nový Vestec'] = 'Novém Vestci'
        self.translator[u'Nučice'] = 'Nučicích'
        self.translator[u'Nupaky'] = 'Nupakách'
        self.translator[u'Nymburk'] = 'Nymburku'
        self.translator[u'Nýrsko'] = 'Nýrsku'
        self.translator[u'Nýřany'] = 'Nýřanech'
        self.translator[u'Obořiště'] = 'Obořišti'
        self.translator[u'Obrnice'] = 'Obrnicích'
        self.translator[u'Obříství'] = 'Obříství'
        self.translator[u'Odolena Voda'] = 'Odolene Vodě'
        self.translator[u'Odry'] = 'Odrách'
        self.translator[u'Ohrobec'] = 'Ohrobci'
        self.translator[u'Ochoz u Brna'] = 'Ochozu u Brna'
        self.translator[u'Okrouhlice'] = 'Okrouhlicích'
        self.translator[u'Okříšky'] = 'Okříškách'
        self.translator[u'Olbramice'] = 'Olbramicích'
        self.translator[u'Oldřišov'] = 'Oldřišově'
        self.translator[u'Olešnice'] = 'Olešnici'
        self.translator[u'Olešnice'] = 'Olešnici'
        self.translator[u'Olomouc'] = 'Olomouci'
        self.translator[u'Oloví'] = 'Oloví'
        self.translator[u'Olovnice'] = 'Olovnici'
        self.translator[u'Omice'] = 'Omicích'
        self.translator[u'Ondřejov'] = 'Ondřejově'
        self.translator[u'Opatov'] = 'Opatově'
        self.translator[u'Opatovice'] = 'Opatovicích'
        self.translator[u'Opatovice'] = 'Opatovicích'
        self.translator[u'Opatovice nad Labem'] = 'Opatovicívh nad Labem'
        self.translator[u'Opava'] = 'Opavě'
        self.translator[u'Opočno'] = 'Opočně'
        self.translator[u'Oráčov'] = 'Oráčově'
        self.translator[u'Orlová'] = 'Orlové'
        self.translator[u'Ořech'] = 'Ořechu'
        self.translator[u'Ořechov'] = 'Ořechově'
        self.translator[u'Osečná'] = 'Osečné'
        self.translator[u'Osek'] = 'Oseku'
        self.translator[u'Oskořínek'] = 'Oskořínku'
        self.translator[u'Oslavany'] = 'Oslavanech'
        self.translator[u'Osoblaha'] = 'Osoblaze'
        self.translator[u'Ostopovice'] = 'Ostopovicích'
        self.translator[u'Ostrá'] = 'Ostré'
        self.translator[u'Ostrava'] = 'Ostravě'
        self.translator[u'Ostroměř'] = 'Ostroměři'
        self.translator[u'Ostrov'] = 'Ostrově'
        self.translator[u'Ostrovačice'] = 'Ostrovačicích'
        self.translator[u'Ostrožská Nová Ves'] = 'Ostrožské Nové Vsi'
        self.translator[u'Ostřešany'] = 'Ostřešanech'
        self.translator[u'Otice'] = 'Oticích'
        self.translator[u'Otnice'] = 'Otnicích'
        self.translator[u'Otovice'] = 'Otovicích'
        self.translator[u'Otrokovice'] = 'Otrokovicích'
        self.translator[u'Otvice'] = 'Otvicích'
        self.translator[u'Ovčáry'] = 'Ovčárech'
        self.translator[u'Pacov'] = 'Pacově'
        self.translator[u'Panenské Břežany'] = 'Panenských Břežanech'
        self.translator[u'Pardubice'] = 'Pardubicích'
        self.translator[u'Paskov'] = 'Paskově'
        self.translator[u'Pastuchovice'] = 'Pastuchovicích'
        self.translator[u'Pátek'] = 'Pátku'
        self.translator[u'Pavlov'] = 'Pavlově'
        self.translator[u'Pavlov'] = 'Pavlově'
        self.translator[u'Pečky'] = 'Pečkách'
        self.translator[u'Pelhřimov'] = 'Pelhřimově'
        self.translator[u'Pernink'] = 'Perninku'
        self.translator[u'Perštejn'] = 'Perštejnu'
        self.translator[u'Peruc'] = 'Peruci'
        self.translator[u'Petrov'] = 'Petrově'
        self.translator[u'Petrovice'] = 'Petrovicích'
        self.translator[u'Petříkov'] = 'Petříkově'
        self.translator[u'Petřvald'] = 'Petřvaldu'
        self.translator[u'Pchery'] = 'Pcherech'
        self.translator[u'Pilníkov'] = 'Pilníkově'
        self.translator[u'Písek'] = 'Písku'
        self.translator[u'Písková Lhota'] = 'Pískové Lhotě'
        self.translator[u'Píšť'] = 'Píšti'
        self.translator[u'Planá'] = 'Plané'
        self.translator[u'Planá nad Lužnicí'] = 'Plané nad Lužnicí'
        self.translator[u'Plaňany'] = 'Plaňanech'
        self.translator[u'Plánice'] = 'Plánicích'
        self.translator[u'Plasy'] = 'Plasech'
        self.translator[u'Plaveč'] = 'Plavči'
        self.translator[u'Plesná'] = 'Plesné'
        self.translator[u'Pletený Újezd'] = 'Pleteném Újezdu'
        self.translator[u'Plumlov'] = 'Plumlově'
        self.translator[u'Plzeň'] = 'Plzni'
        self.translator[u'Pňov-Předhradí'] = 'Pňovi-Předhradí'
        self.translator[u'Poběžovice'] = 'Poběžovicích'
        self.translator[u'Počátky'] = 'Počátkách'
        self.translator[u'Podbořany'] = 'Podbořanech'
        self.translator[u'Poděbrady'] = 'Poděbradech'
        self.translator[u'Podivín'] = 'Podivíně'
        self.translator[u'Podolanka'] = 'Podolance'
        self.translator[u'Pohled'] = 'Pohledu'
        self.translator[u'Pohořelice'] = 'Pohořelicích'
        self.translator[u'Pohořelice'] = 'Pohořelicích'
        self.translator[u'Police nad Metují'] = 'Polici nad Metují'
        self.translator[u'Polička'] = 'Poličce'
        self.translator[u'Polná'] = 'Polné'
        self.translator[u'Polom'] = 'Polomi'
        self.translator[u'Pomezí nad Ohří'] = 'Pomezí nad Ohří'
        self.translator[u'Popice'] = 'Popicích'
        self.translator[u'Popovičky'] = 'Popovičkách'
        self.translator[u'Popůvky'] = 'Popůvkách'
        self.translator[u'Poříčany'] = 'Poříčanech'
        self.translator[u'Poříčí nad Sázavou'] = 'Poříčí nad Sázavou'
        self.translator[u'Postoloprty'] = 'Postoloprtech'
        self.translator[u'Postřelmov'] = 'Postřelmově'
        self.translator[u'Postřižín'] = 'Postřižíně'
        self.translator[u'Postupice'] = 'Postupicích'
        self.translator[u'Potštát'] = 'Potštátě'
        self.translator[u'Potštejn'] = 'Potštejně'
        self.translator[u'Pouzdřany'] = 'Pouzdřanech'
        self.translator[u'Povrly'] = 'Povrlech'
        self.translator[u'Pozlovice'] = 'Pozlovicích'
        self.translator[u'Pozořice'] = 'Pozořicích'
        self.translator[u'Prace'] = 'Pracích'
        self.translator[u'Prackovice nad Labem'] = 'Prackovicích nad Labem'
        self.translator[u'Praha'] = 'Praze'
        self.translator[u'Prachatice'] = 'Prachaticích'
        self.translator[u'Prachovice'] = 'Prachovicích'
        self.translator[u'Proboštov'] = 'Proboštově'
        self.translator[u'Proseč'] = 'Proseči'
        self.translator[u'Prosenice'] = 'Prosenicích'
        self.translator[u'Prostějov'] = 'Prostějově'
        self.translator[u'Protivanov'] = 'Protivanově'
        self.translator[u'Protivín'] = 'Protivíně'
        self.translator[u'Prštice'] = 'Pršticích'
        self.translator[u'Průhonice'] = 'Průhonicích'
        self.translator[u'Prusinovice'] = 'Prusinovicích'
        self.translator[u'Předboj'] = 'Předboji'
        self.translator[u'Předhradí'] = 'Předhradí'
        self.translator[u'Předklášteří'] = 'Předklášteří'
        self.translator[u'Předměřice nad Labem'] = 'Předměřicích nad Labem'
        self.translator[u'Přední Výtoň'] = 'Přední Výtoni'
        self.translator[u'Přelouč'] = 'Přelouči'
        self.translator[u'Přerov'] = 'Přerově'
        self.translator[u'Přeštice'] = 'Přešticích'
        self.translator[u'Přezletice'] = 'Přezleticích'
        self.translator[u'Příbor'] = 'Příboře'
        self.translator[u'Příbram'] = 'Příbrami'
        self.translator[u'Přibyslav'] = 'Přibyslavi'
        self.translator[u'Přibyslavice'] = 'Přibyslavicích'
        self.translator[u'Přibyslavice'] = 'Přibyslavicích'
        self.translator[u'Přimda'] = 'Přimdě'
        self.translator[u'Přísnotice'] = 'Přísnoticích'
        self.translator[u'Přítluky'] = 'Přítlukách'
        self.translator[u'Psáry'] = 'Psárech'
        self.translator[u'Ptice'] = 'Pticích'
        self.translator[u'Pustá Polom'] = 'Pusté Polomi'
        self.translator[u'Pustiměř'] = 'Pustiměři'
        self.translator[u'Pyšely'] = 'Pyšelech'
        self.translator[u'Radějovice'] = 'Radějovicích'
        self.translator[u'Radnice'] = 'Radnicích'
        self.translator[u'Radomyšl'] = 'Radomyšli'
        self.translator[u'Radonice'] = 'Radonicích'
        self.translator[u'Radostice'] = 'Radosticích'
        self.translator[u'Radovesnice I'] = 'Radovesnici I'
        self.translator[u'Radslavice'] = 'Radslavicích'
        self.translator[u'Raduň'] = 'Raduni'
        self.translator[u'Rájec-Jestřebí'] = 'Rájci-Jestřebí'
        self.translator[u'Ráječko'] = 'Ráječku'
        self.translator[u'Rajhrad'] = 'Rajhradě'
        self.translator[u'Rajhradice'] = 'Rajhradicích'
        self.translator[u'Rakovník'] = 'Rakovníku'
        self.translator[u'Rakvice'] = 'Rakvicích'
        self.translator[u'Ralsko'] = 'Ralsku'
        self.translator[u'Rapotice'] = 'Rapoticích'
        self.translator[u'Rapotín'] = 'Rapotíně'
        self.translator[u'Rapšach'] = 'Rapšachu'
        self.translator[u'Raspenava'] = 'Raspenavě'
        self.translator[u'Rataje nad Sázavou'] = 'Ratajích nad Sázavou'
        self.translator[u'Ratboř'] = 'Ratboři'
        self.translator[u'Ratíškovice'] = 'Ratíškovicích'
        self.translator[u'Rebešovice'] = 'Rebešovicích'
        self.translator[u'Rejštejn'] = 'Rejštejně'
        self.translator[u'Roblín'] = 'Roblíně'
        self.translator[u'Rohatec'] = 'Rohatci'
        self.translator[u'Rohov'] = 'Rohově'
        self.translator[u'Rokycany'] = 'Rokycanech'
        self.translator[u'Rokytnice v Orlických horách'] = 'Rokytnici v Orlických horách'
        self.translator[u'Rokytno'] = 'Rokytně'
        self.translator[u'Ronov nad Doubravou'] = 'Ronově nad Doubravou'
        self.translator[u'Rosice'] = 'Rosicích'
        self.translator[u'Rostoklaty'] = 'Rostoklatech'
        self.translator[u'Rotava'] = 'Rotavě'
        self.translator[u'Roudné'] = 'Roudném'
        self.translator[u'Roudnice nad Labem'] = 'Roudnici nad Labem'
        self.translator[u'Rousínov'] = 'Rousínově'
        self.translator[u'Rovensko pod Troskami'] = 'Rovensku pod Troskami'
        self.translator[u'Rozdrojovice'] = 'Rozdrojovicích'
        self.translator[u'Roztoky'] = 'Roztokách'
        self.translator[u'Roztoky'] = 'Roztokách'
        self.translator[u'Rožďalovice'] = 'Rožďalovicích'
        self.translator[u'Rožmitál na Šumavě'] = 'Rožmitále na Šumavě'
        self.translator[u'Rožmitál pod Třemšínem'] = 'Rožmitále pod Třemšínem'
        self.translator[u'Rožnov pod Radhoštěm'] = 'Rožnově pod Radhoštěm'
        self.translator[u'Rtyně v Podkrkonoší'] = 'Rtyni v Podkrkonoší'
        self.translator[u'Ruda'] = 'Rudě'
        self.translator[u'Ruda nad Moravou'] = 'Rudě nad Moravou'
        self.translator[u'Rudná'] = 'Rudné'
        self.translator[u'Rudolfov'] = 'Rudolfově'
        self.translator[u'Rumburk'] = 'Rumburku'
        self.translator[u'Rybitví'] = 'Rybitví'
        self.translator[u'Rychnov nad Kněžnou'] = 'Rychnově nad Kněžnou'
        self.translator[u'Rychnov u Jablonce nad Nisou'] = 'Rychnově u Jablonce nad Nisou'
        self.translator[u'Rychvald'] = 'Rychvaldu'
        self.translator[u'Rýmařov'] = 'Rýmařově'
        self.translator[u'Rynholec'] = 'Rynholci'
        self.translator[u'Ryžoviště'] = 'Ryžovišti'
        self.translator[u'Řečany nad Labem'] = 'Řečanech nad Labem'
        self.translator[u'Řehenice'] = 'Řehenicích'
        self.translator[u'Řemíčov'] = 'Řemíčově'
        self.translator[u'Řepín'] = 'Řepíně'
        self.translator[u'Řepiště'] = 'Řepišti'
        self.translator[u'Řevnice'] = 'Řevnicích'
        self.translator[u'Řevničov'] = 'Řevničově'
        self.translator[u'Řícmanice'] = 'Řícmanicích'
        self.translator[u'Říčany'] = 'Říčanech'
        self.translator[u'Říčany'] = 'Říčanech'
        self.translator[u'Římov'] = 'Římově'
        self.translator[u'Řitka'] = 'Řitce'
        self.translator[u'Sadská'] = 'Sadské'
        self.translator[u'Samotišky'] = 'Samotiškách'
        self.translator[u'Sány'] = 'Sánech'
        self.translator[u'Sázava'] = 'Sázavě'
        self.translator[u'Seč'] = 'Seči'
        self.translator[u'Sedlčany'] = 'Sedlčanech'
        self.translator[u'Sedlec-Prčice'] = 'Sedleci-Prčicích'
        self.translator[u'Sedlice'] = 'Sedlicích'
        self.translator[u'Semily'] = 'Semilech'
        self.translator[u'Senice'] = 'Senicích'
        self.translator[u'Senice na Hané'] = 'Senici na Hané'
        self.translator[u'Senohraby'] = 'Senohrabech'
        self.translator[u'Senomaty'] = 'Senomatech'
        self.translator[u'Sezemice'] = 'Sezemicích'
        self.translator[u'Sezimovo Ústí'] = 'Sezimově Ústí'
        self.translator[u'Sibřina'] = 'Sibřině'
        self.translator[u'Silůvky'] = 'Silůvkách'
        self.translator[u'Skalná'] = 'Skalné'
        self.translator[u'Skorošice'] = 'Skorošicích'
        self.translator[u'Skrbeň'] = 'Skrbni'
        self.translator[u'Skřivany'] = 'Skřivanech'
        self.translator[u'Skuteč'] = 'Skutči'
        self.translator[u'Slaný'] = 'Slaném'
        self.translator[u'Slatiňany'] = 'Slatiňanech'
        self.translator[u'Slavětín'] = 'Slavětíně'
        self.translator[u'Slavičín'] = 'Slavičíně'
        self.translator[u'Slavkov'] = 'Slavkově'
        self.translator[u'Slavkov u Brna'] = 'Slavkově u Brna'
        self.translator[u'Slavonice'] = 'Slavonicích'
        self.translator[u'Slepotice'] = 'Slepoticích'
        self.translator[u'Sloup v Čechách'] = 'Sloupu v Čechách'
        self.translator[u'Sloveč'] = 'Slovči'
        self.translator[u'Slušovice'] = 'Slušovicích'
        self.translator[u'Smečno'] = 'Smečně'
        self.translator[u'Smidary'] = 'Smidarech'
        self.translator[u'Smiřice'] = 'Smiřicích'
        self.translator[u'Smržice'] = 'Smržicích'
        self.translator[u'Smržovka'] = 'Smržovce'
        self.translator[u'Soběslav'] = 'Soběslavi'
        self.translator[u'Sobotka'] = 'Sobotce'
        self.translator[u'Sokoleč'] = 'Sokolči'
        self.translator[u'Sokolnice'] = 'Sokolnicích'
        self.translator[u'Sokolov'] = 'Sokolově'
        self.translator[u'Solnice'] = 'Solnicích'
        self.translator[u'Sovínky'] = 'Sovínkávh'
        self.translator[u'Spálené Poříčí'] = 'Spálené Poříčí'
        self.translator[u'Spojil'] = 'Spojili'
        self.translator[u'Spořice'] = 'Spořicích'
        self.translator[u'Srbsko'] = 'Srbsku'
        self.translator[u'Srch'] = 'Srchu'
        self.translator[u'Srnojedy'] = 'Srnojedech'
        self.translator[u'Srubec'] = 'Srubci'
        self.translator[u'Stachy'] = 'Stachách'
        self.translator[u'Staňkov'] = 'Staňkově'
        self.translator[u'Staňkovice'] = 'Staňkovicích'
        self.translator[u'Stará Huť'] = 'Staré Huti'
        self.translator[u'Stará Paka'] = 'Staré Pace'
        self.translator[u'Stará Ves'] = 'Staré Vsi'
        self.translator[u'Stará Ves nad Ondřejnicí'] = 'Staré Vsi nad Ondřejnicí'
        self.translator[u'Staré Hodějovice'] = 'Starých Hodějovicích'
        self.translator[u'Staré Hradiště'] = 'Starém Hradišti'
        self.translator[u'Staré Město'] = 'Starém Městě'
        self.translator[u'Staré Město'] = 'Starém Městě'
        self.translator[u'Staré Město'] = 'Starém Městě'
        self.translator[u'Staré Sedlo'] = 'Starém Sedle'
        self.translator[u'Staré Ždánice'] = 'Starých Ždánicích'
        self.translator[u'Starý Jičín'] = 'Starém Jičíně'
        self.translator[u'Starý Kolín'] = 'Starém Kolíně'
        self.translator[u'Starý Plzenec'] = 'Starém Plzenci'
        self.translator[u'Stařeč'] = 'Starči'
        self.translator[u'Staříč'] = 'Staříči'
        self.translator[u'Statenice'] = 'Statenicích'
        self.translator[u'Stehelčeves'] = 'Stehelčevsi'
        self.translator[u'Stěžery'] = 'Stěžerech'
        self.translator[u'Stod'] = 'Stodě'
        self.translator[u'Stochov'] = 'Stochově'
        self.translator[u'Strachotín'] = 'Strachotíně'
        self.translator[u'Strakonice'] = 'Strakonicích'
        self.translator[u'Strančice'] = 'Strančicích'
        self.translator[u'Strání'] = 'Strání'
        self.translator[u'Strašín'] = 'Strašíně'
        self.translator[u'Stráž nad Nežárkou'] = 'Stráiž nad Nežárkou'
        self.translator[u'Stráž nad Nisou'] = 'Stráži nad Nisou'
        self.translator[u'Stráž pod Ralskem'] = 'Stráži pod Ralskem'
        self.translator[u'Strážnice'] = 'Strážnici'
        self.translator[u'Strmilov'] = 'Strmilově'
        self.translator[u'Struhařov'] = 'Struhařově'
        self.translator[u'Strunkovice nad Blanicí'] = 'Strunkovicích nad Blanicí'
        self.translator[u'Středokluky'] = 'Středoklukách'
        self.translator[u'Střelice'] = 'Střelicích'
        self.translator[u'Střelské Hoštice'] = 'Střelských Hošticích'
        self.translator[u'Stříbrná Skalice'] = 'Stříbrné Skalici'
        self.translator[u'Stříbro'] = 'Stříbře'
        self.translator[u'Střílky'] = 'Střílkách'
        self.translator[u'Střítež'] = 'Stříteži'
        self.translator[u'Studená'] = 'Studené'
        self.translator[u'Studénka'] = 'Studénce'
        self.translator[u'Sudice'] = 'Sudicích'
        self.translator[u'Suchdol'] = 'Suchdole'
        self.translator[u'Suchdol nad Lužnicí'] = 'Suchdole nad Lužnicí'
        self.translator[u'Suchdol nad Odrou'] = 'Suchdole nad Odrou'
        self.translator[u'Suchohrdly'] = 'Suchohrdlech'
        self.translator[u'Sulejovice'] = 'Sulejovicích'
        self.translator[u'Sulice'] = 'Sulicích'
        self.translator[u'Sušice'] = 'Sušici'
        self.translator[u'Svárov'] = 'Svárově'
        self.translator[u'Svatava'] = 'Svatavě'
        self.translator[u'Svatobořice-Mistřín'] = 'Svatobořicích-Mistříně'
        self.translator[u'Svatý Jan pod Skalou'] = 'Svatém Janu pod Skalou'
        self.translator[u'Svatý Mikuláš'] = 'Svatém Mikuláši'
        self.translator[u'Svémyslice'] = 'Svémyslicích'
        self.translator[u'Světec'] = 'Světci'
        self.translator[u'Světice'] = 'Světicích'
        self.translator[u'Světlá nad Sázavou'] = 'Světlé nad Sázavou'
        self.translator[u'Sviadnov'] = 'Sviadnově'
        self.translator[u'Svinaře'] = 'Svinařích'
        self.translator[u'Svinařov'] = 'Svinařově'
        self.translator[u'Svitávka'] = 'Svitávce'
        self.translator[u'Svitavy'] = 'Svitavách'
        self.translator[u'Svoboda nad Úpou'] = 'Svobodě nad Úpou'
        self.translator[u'Svobodné Heřmanice'] = 'Svobodných Heřmanicích'
        self.translator[u'Svojetice'] = 'Svojeticích'
        self.translator[u'Svratka'] = 'Svratce'
        self.translator[u'Sýkořice'] = 'Sýkořicích'
        self.translator[u'Šakvice'] = 'Šakvicích'
        self.translator[u'Šanov'] = 'Šanově'
        self.translator[u'Šaratice'] = 'Šaraticích'
        self.translator[u'Šenov'] = 'Šenově'
        self.translator[u'Šenov u Nového Jičína'] = 'Šenově u Nového Jičína'
        self.translator[u'Šestajovice'] = 'Šestajovicích'
        self.translator[u'Ševětín'] = 'Ševětíně'
        self.translator[u'Šilheřovice'] = 'Šilheřovicích'
        self.translator[u'Šimonovice'] = 'Šimonovicích'
        self.translator[u'Šitbořice'] = 'Šitbořicích'
        self.translator[u'Škvorec'] = 'Škvorci'
        self.translator[u'Šlapanice'] = 'Šlapanicích'
        self.translator[u'Šluknov'] = 'Šluknově'
        self.translator[u'Špičky'] = 'Špičkách'
        self.translator[u'Šťáhlavy'] = 'Šťáhlavech'
        self.translator[u'Štěchovice'] = 'Štěchovicích'
        self.translator[u'Štěkeň'] = 'Štěkeni'
        self.translator[u'Štěnovice'] = 'Štěnovicích'
        self.translator[u'Štěpánkovice'] = 'Štěpánkovicích'
        self.translator[u'Štěpánov'] = 'Štěpánově'
        self.translator[u'Štěpánovice'] = 'Štěpánovicích'
        self.translator[u'Šternberk'] = 'Šternberku'
        self.translator[u'Štětí'] = 'Štětí'
        self.translator[u'Štítina'] = 'Štítina'
        self.translator[u'Štíty'] = 'Štítech'
        self.translator[u'Štoky'] = 'Štokách'
        self.translator[u'Štramberk'] = 'Štramberku'
        self.translator[u'Šumperk'] = 'Šumperku'
        self.translator[u'Švihov'] = 'Švihově'
        self.translator[u'Tábor'] = 'Táboře'
        self.translator[u'Tachlovice'] = 'Tachlovicích'
        self.translator[u'Tachov'] = 'Tachově'
        self.translator[u'Tanvald'] = 'Tanvaldě'
        self.translator[u'Tatce'] = 'Tatcích'
        self.translator[u'Tehov'] = 'Tehově'
        self.translator[u'Tehov'] = 'Tehově'
        self.translator[u'Tehovec'] = 'Tehovci'
        self.translator[u'Telč'] = 'Telči'
        self.translator[u'Telnice'] = 'Telnici'
        self.translator[u'Teplá'] = 'Teplé'
        self.translator[u'Teplice'] = 'Teplicích'
        self.translator[u'Teplice nad Metují'] = 'Teplicích nad Metují'
        self.translator[u'Terezín'] = 'Terezíně'
        self.translator[u'Těrlicko'] = 'Těrlicku'
        self.translator[u'Tetčice'] = 'Tetčicích'
        self.translator[u'Tetín'] = 'Tetíně'
        self.translator[u'Tisá'] = 'Tisé'
        self.translator[u'Tišice'] = 'Tišicích'
        self.translator[u'Tišnov'] = 'Tišnově'
        self.translator[u'Tlučná'] = 'Tlučné'
        self.translator[u'Tlumačov'] = 'Tlumačově'
        self.translator[u'Tmaň'] = 'Tmani'
        self.translator[u'Toužim'] = 'Toužimi'
        self.translator[u'Tovačov'] = 'Tovačově'
        self.translator[u'Trhová Kamenice'] = 'Trhové Kamenici'
        self.translator[u'Trhové Sviny'] = 'Trhových Svinech'
        self.translator[u'Trhový Štěpánov'] = 'Trhovém Štěpánově'
        self.translator[u'Trmice'] = 'Trmicích'
        self.translator[u'Trnávka'] = 'Trnávce'
        self.translator[u'Troubky'] = 'Troubkách'
        self.translator[u'Troubsko'] = 'Troubsku'
        self.translator[u'Trubín'] = 'Trubíně'
        self.translator[u'Trutnov'] = 'Trutnově'
        self.translator[u'Třebechovice pod Orebem'] = 'Třebechovicích pod Orebem'
        self.translator[u'Třebenice'] = 'Třebenicích'
        self.translator[u'Třebestovice'] = 'Třebestovicích'
        self.translator[u'Třebíč'] = 'Třebíči'
        self.translator[u'Třebívlice'] = 'Třebívlicích'
        self.translator[u'Třeboň'] = 'Třeboni'
        self.translator[u'Třebotov'] = 'Třebotově'
        self.translator[u'Třemošná'] = 'Třemošné'
        self.translator[u'Třemošnice'] = 'Třemošnici'
        self.translator[u'Třešť'] = 'Třešťi'
        self.translator[u'Třinec'] = 'Třinci'
        self.translator[u'Tuchlovice'] = 'Tuchlovicích'
        self.translator[u'Tuchoměřice'] = 'Tuchoměřicích'
        self.translator[u'Tuklaty'] = 'Tuklatech'
        self.translator[u'Turnov'] = 'Turnově'
        self.translator[u'Tursko'] = 'Tursku'
        self.translator[u'Tvrdonice'] = 'Tvrdonicích'
        self.translator[u'Týn nad Bečvou'] = 'Týně nad Bečvou'
        self.translator[u'Týn nad Vltavou'] = 'Týně nad Vltavou'
        self.translator[u'Týnec'] = 'Týnci'
        self.translator[u'Týnec nad Labem'] = 'Týnci nad Labem'
        self.translator[u'Týnec nad Sázavou'] = 'Týnci nad Sázavou'
        self.translator[u'Týniště nad Orlicí'] = 'Týništi nad Orlicí'
        self.translator[u'Údlice'] = 'Údlicích'
        self.translator[u'Úherce'] = 'Úhercích'
        self.translator[u'Uherské Hradiště'] = 'Uherském Hradišti'
        self.translator[u'Uherský Brod'] = 'Uherském Brodě'
        self.translator[u'Uherský Ostroh'] = 'Uherském Ostrohu'
        self.translator[u'Uhlířské Janovice'] = 'Uhlířských Janovicích'
        self.translator[u'Úholičky'] = 'Úholičkách'
        self.translator[u'Úhonice'] = 'Úhonicích'
        self.translator[u'Újezd u Brna'] = 'Újezdě u Brna'
        self.translator[u'Újezdeček'] = 'Újezdečku'
        self.translator[u'Únětice'] = 'Úněticích'
        self.translator[u'Unhošť'] = 'Unhošť'
        self.translator[u'Uničov'] = 'Uničově'
        self.translator[u'Úpice'] = 'Úpicích'
        self.translator[u'Úsov'] = 'Úsově'
        self.translator[u'Ústí nad Labem'] = 'Ústí nad Labem'
        self.translator[u'Ústí nad Orlicí'] = 'Ústí nad Orlicí'
        self.translator[u'Úštěk'] = 'Úštěku'
        self.translator[u'Úterý'] = 'Úterý'
        self.translator[u'Úvaly'] = 'Úvalech'
        self.translator[u'Úžice'] = 'Úžicích'
        self.translator[u'Vacenovice'] = 'Vacenovicích'
        self.translator[u'Václavovice'] = 'Václavovicích'
        self.translator[u'Valašské Klobouky'] = 'Valašských Kloboukách'
        self.translator[u'Valašské Meziříčí'] = 'Valašském Meziříčí'
        self.translator[u'Valdice'] = 'Valdicích'
        self.translator[u'Valeč'] = 'Valči'
        self.translator[u'Valtice'] = 'Valticích'
        self.translator[u'Valy'] = 'Valech'
        self.translator[u'Valy'] = 'Valech'
        self.translator[u'Vamberk'] = 'Vamberku'
        self.translator[u'Varnsdorf'] = 'Varnsdorfu'
        self.translator[u'Varvažov'] = 'Varvažově'
        self.translator[u'Včelákov'] = 'Včelákově'
        self.translator[u'Včelná'] = 'Včelná'
        self.translator[u'Vědomice'] = 'Vědomicích'
        self.translator[u'Vejprnice'] = 'Vejprnicích'
        self.translator[u'Vejprty'] = 'Vejprtech'
        self.translator[u'Velehrad'] = 'Velehradě'
        self.translator[u'Velemín'] = 'Velemíně'
        self.translator[u'Veleň'] = 'Veleni'
        self.translator[u'Velešín'] = 'Velešíně'
        self.translator[u'Velichovky'] = 'Velichovkách'
        self.translator[u'Veliká Ves'] = 'Veliké Vsi'
        self.translator[u'Velim'] = 'Velimi'
        self.translator[u'Veliny'] = 'Velinech'
        self.translator[u'Velká Bíteš'] = 'Velké Bíteši'
        self.translator[u'Velká Bystřice'] = 'Velké Bystřici'
        self.translator[u'Velká Dobrá'] = 'Velké Dobré'
        self.translator[u'Velká Hleďsebe'] = 'Velké Hleďsebi'
        self.translator[u'Velká Polom'] = 'Velké Polomi'
        self.translator[u'Velké Bílovice'] = 'Velkých Bílovicích'
        self.translator[u'Velké Březno'] = 'Velkém Březně'
        self.translator[u'Velké Heraltice'] = 'Velkých Heralticích'
        self.translator[u'Velké Hoštice'] = 'Velkých Hošticích'
        self.translator[u'Velké Losiny'] = 'Velké Losiny'
        self.translator[u'Velké Meziříčí'] = 'Velkém Meziříčí'
        self.translator[u'Velké Němčice'] = 'Velkých Němčicích'
        self.translator[u'Velké Opatovice'] = 'Velkých Opatovicích'
        self.translator[u'Velké Pavlovice'] = 'Velkých Pavlovicích'
        self.translator[u'Velké Popovice'] = 'Velkých Popovicích'
        self.translator[u'Velké Poříčí'] = 'Velkém Poříčí'
        self.translator[u'Velké Přílepy'] = 'Velkých Přílepech'
        self.translator[u'Velké Přítočno'] = 'Velkém Přítočně'
        self.translator[u'Velký Borek'] = 'Velkém Borku'
        self.translator[u'Velký Osek'] = 'Velkém Oseku'
        self.translator[u'Velký Šenov'] = 'Velkém Šenově'
        self.translator[u'Velký Týnec'] = 'Velkém Týnci'
        self.translator[u'Velký Újezd'] = 'Velkém Újezdě'
        self.translator[u'Veltěže'] = 'Veltěži'
        self.translator[u'Veltruby'] = 'Veltrubech'
        self.translator[u'Veltrusy'] = 'Veltrusech'
        self.translator[u'Velvary'] = 'Velvarech'
        self.translator[u'Verměřovice'] = 'Verměřovicích'
        self.translator[u'Verneřice'] = 'Verneřicích'
        self.translator[u'Veselí nad Lužnicí'] = 'Veselí nad Lužnicí'
        self.translator[u'Veselí nad Moravou'] = 'Veselí nad Moravou'
        self.translator[u'Vestec'] = 'Vestci'
        self.translator[u'Větrušice'] = 'Větrušicích'
        self.translator[u'Větřní'] = 'Větřní'
        self.translator[u'Veverská Bítýška'] = 'Veverské Bítýšce'
        self.translator[u'Vidnava'] = 'Vidnavě'
        self.translator[u'Vikýřovice'] = 'Vikýřovicích'
        self.translator[u'Vilémov'] = 'Vilémově'
        self.translator[u'Vimperk'] = 'Vimperku'
        self.translator[u'Vinařice'] = 'Vinařicích'
        self.translator[u'Vitějovice'] = 'Vitějovicích'
        self.translator[u'Vítkov'] = 'Vítkově'
        self.translator[u'Vizovice'] = 'Vizovicích'
        self.translator[u'Vlachovo Březí'] = 'Vlachově Březí'
        self.translator[u'Vlašim'] = 'Vlašimi'
        self.translator[u'Vlkava'] = 'Vlkavě'
        self.translator[u'Vlkoš'] = 'Vlkoši'
        self.translator[u'Vnorovy'] = 'Vnorovech'
        self.translator[u'Vodňany'] = 'Vodňanech'
        self.translator[u'Vodochody'] = 'Vodochodech'
        self.translator[u'Vojenský újezd Boletice'] = 'Vojenském újezdě Boletice'
        self.translator[u'Vojenský újezd Libavá'] = 'Vojenském újezdě Libavá'
        self.translator[u'Vojkovice'] = 'Vojkovicích'
        self.translator[u'Vojtanov'] = 'Vojtanově'
        self.translator[u'Volary'] = 'Volarech'
        self.translator[u'Volyně'] = 'Volyni'
        self.translator[u'Vonoklasy'] = 'Vonoklasech'
        self.translator[u'Votice'] = 'Voticích'
        self.translator[u'Vrábče'] = 'Vrábči'
        self.translator[u'Vracov'] = 'Vracově'
        self.translator[u'Vrané nad Vltavou'] = 'Vraném nad Vltavou'
        self.translator[u'Vranov nad Dyjí'] = 'Vranově nad Dyjí'
        self.translator[u'Vranovice'] = 'Vranovicích'
        self.translator[u'Vratimov'] = 'Vratimově'
        self.translator[u'Vráto'] = 'Vrátu'
        self.translator[u'Vráž'] = 'Vráži'
        self.translator[u'Vrbice'] = 'Vrbicích'
        self.translator[u'Vrbice'] = 'Vrbicích'
        self.translator[u'Vrbno pod Pradědem'] = 'Vrbnu pod Pradědem'
        self.translator[u'Vrbová Lhota'] = 'Vrbové Lhotě'
        self.translator[u'Vrdy'] = 'Vrdech'
        self.translator[u'Vrchlabí'] = 'Vrchlabí'
        self.translator[u'Vroutek'] = 'Vroutku'
        self.translator[u'Vřesina'] = 'Vřesině'
        self.translator[u'Vřesina'] = 'Vřesině'
        self.translator[u'Vsetín'] = 'Vsetíně'
        self.translator[u'Všejany'] = 'Všejanech'
        self.translator[u'Všemyslice'] = 'Všemyslicích'
        self.translator[u'Všenory'] = 'Všenorech'
        self.translator[u'Všestary'] = 'Všestarech'
        self.translator[u'Všetaty'] = 'Všetatech'
        self.translator[u'Vysoká'] = 'Vysoké'
        self.translator[u'Vysoká nad Labem'] = 'Vysoké nad Labem'
        self.translator[u'Vysoká Pec'] = 'Vysoké Peci'
        self.translator[u'Vysoké Chvojno'] = 'Vysokém Chvojně'
        self.translator[u'Vysoké Mýto'] = 'Vysokém Mýtě'
        self.translator[u'Vysoké nad Jizerou'] = 'Vysokém nad Jizerou'
        self.translator[u'Vysoké Veselí'] = 'Vysokém Veselí'
        self.translator[u'Vysoký Újezd'] = 'Vysokém Újezdě'
        self.translator[u'Vyškov'] = 'Vyškově'
        self.translator[u'Vyšší Brod'] = 'Vyšším Brodě'
        self.translator[u'Vyžlovka'] = 'Vyžlovce'
        self.translator[u'Záboří nad Labem'] = 'Záboří nad Labem'
        self.translator[u'Zábřeh'] = 'Zábřehu'
        self.translator[u'Zadní Třebaň'] = 'Zadní Třebani'
        self.translator[u'Zahnašovice'] = 'Zahnašovicích'
        self.translator[u'Záhornice'] = 'Záhornicích'
        self.translator[u'Zaječí'] = 'Zaječí'
        self.translator[u'Zákupy'] = 'Zákupech'
        self.translator[u'Zápy'] = 'Zápech'
        self.translator[u'Zásmuky'] = 'Zásmukách'
        self.translator[u'Zastávka'] = 'Zastávce'
        self.translator[u'Zbiroh'] = 'Zbirohu'
        self.translator[u'Zborovice'] = 'Zborovicích'
        self.translator[u'Zbraslav'] = 'Zbraslavi'
        self.translator[u'Zbůch'] = 'Zbůchu'
        self.translator[u'Zbuzany'] = 'Zbuzanech'
        self.translator[u'Zbyslavice'] = 'Zbyslavicích'
        self.translator[u'Zbýšov'] = 'Zbýšově'
        self.translator[u'Zdechovice'] = 'Zdechovicích'
        self.translator[u'Zdiby'] = 'Zdibech'
        self.translator[u'Zdice'] = 'Zdicích'
        self.translator[u'Zdobnice'] = 'Zdobnicích'
        self.translator[u'Zdounky'] = 'Zdounkách'
        self.translator[u'Zeleneč'] = 'Zelenči'
        self.translator[u'Zlaté Hory'] = 'Zlatých Horách'
        self.translator[u'Zlatníky-Hodkovice'] = 'Zlatníkách-Hodkovicích'
        self.translator[u'Zlín'] = 'Zlíně'
        self.translator[u'Zliv'] = 'Zlivi'
        self.translator[u'Zlonice'] = 'Zlonicích'
        self.translator[u'Znojmo'] = 'Znojmě'
        self.translator[u'Zruč nad Sázavou'] = 'Zruči nad Sázavou'
        self.translator[u'Zruč-Senec'] = 'Zruči-Senci'
        self.translator[u'Zubří'] = 'Zubří'
        self.translator[u'Zvánovice'] = 'Zvánovicích'
        self.translator[u'Zvěřínek'] = 'Zvěřínku'
        self.translator[u'Zvíkov'] = 'Zvíkově'
        self.translator[u'Zvole'] = 'Zvoli'
        self.translator[u'Žabčice'] = 'Žabčicích'
        self.translator[u'Žabovřesky nad Ohří'] = 'Žabovřeskách nad Ohří'
        self.translator[u'Žacléř'] = 'Žacléři'
        self.translator[u'Žalany'] = 'Žalanech'
        self.translator[u'Žamberk'] = 'Žamberku'
        self.translator[u'Žandov'] = 'Žandově'
        self.translator[u'Žatec'] = 'Žatci'
        self.translator[u'Ždánice'] = 'Ždánicích'
        self.translator[u'Žďár nad Sázavou'] = 'Žďáru nad Sázavou'
        self.translator[u'Ždírec nad Doubravou'] = 'Ždírci nad Doubravou'
        self.translator[u'Žebrák'] = 'Žebráku'
        self.translator[u'Žehušice'] = 'Žehušicích'
        self.translator[u'Želechovice nad Dřevnicí'] = 'Želechovicích nad Dřevnicí'
        self.translator[u'Želenice'] = 'Želenicích'
        self.translator[u'Želenice'] = 'Želenicích'
        self.translator[u'Želešice'] = 'Želešicích'
        self.translator[u'Želetava'] = 'Želetavě'
        self.translator[u'Železná Ruda'] = 'Železné Rudě'
        self.translator[u'Železnice'] = 'Železnicích'
        self.translator[u'Železný Brod'] = 'Železném Brodě'
        self.translator[u'Židlochovice'] = 'Židlochovicích'
        self.translator[u'Žilina'] = 'Žilině'
        self.translator[u'Žirovnice'] = 'Žirovnicích'
        self.translator[u'Žitenice'] = 'Žitenicích'
        self.translator[u'Žiželice'] = 'Žiželicích'
        self.translator[u'Žižkovo Pole'] = 'Žižkově Poli'
        self.translator[u'Žleby'] = 'Žlebech'
        self.translator[u'Žlutice'] = 'Žluticích'
        self.translator[u'Žulová'] = 'Žulové'

rejstrik = rejskol()