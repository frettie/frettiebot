#!/usr/bin/python
# -*- coding: utf-8 -*-

v = 1
c = 1
e = 10
r = 1
a = 10
s = 1
u = 1
n = 1
l = 1
k = 1

col1 = a + v + u + a + l + r
upr1 = int(str(col1)[1:2])
plus1 = int(str(col1)[0:1])

if a == upr1:
    print "ok 1"

col2 = plus1 + r + r + r + e + e
upr2 = int(str(col2)[1:2])
plus2 = int(str(col2)[0:1])

if r == upr2:
    print "ok 2"

col3 = plus2 + e + e + s + c
upr3 = int(str(col3)[1:2])
plus3 = int(str(col3)[0:1])

if e == upr3:
    print "ok 3"

col4 = plus3 + c + s + c + a + a
upr4 = int(str(col4)[1:2])
plus4 = int(str(col4)[0:1])

if c == upr4:
    print "ok 4"

col5 = plus4 + v + e + n + k
upr5 = int(str(col5)[1:2])
plus5 = int(str(col5)[0:1])

if a == upr5:
    print "ok 5"

col6 = plus5 + k
upr6 = int(str(col6)[1:2])
plus6 = int(str(col6)[0:1])

if k == upr6:
    print "ok 6"

#       V 	 Č 	 E 	 R 	 A 
#    	   	   	   	   	 V 
#    	   	 Š 	 E 	 R 	 U 
#  V 	 E 	 Č 	 E 	 R 	 A 
#    	 N 	 A 	 Š 	 E 	 L 
#    	 K 	 A 	 Č 	 E 	 R 
# ---	---	---	---	---	---
#  K 	 A 	 Č 	 E 	 R 	 A 
