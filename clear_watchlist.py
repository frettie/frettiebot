import pywikibot
from pywikibot import pagegenerators
site = pywikibot.Site('cs', 'wikipedia')
commons = site.image_repository()
pages = commons.watched_pages(force=True)
commons.login()
# for page in pages:
#     title=page.title()
#     print("Unwatching",title)
#     page.watch(unwatch=True)
user = 'Frettie'
data = commons.logevents(logtype='upload', user=user, reverse=True)
for file in data:
    page = file.page()
    while page.isRedirectPage() != False:
        page = page.getRedirectTarget()
    print("Watching", page.title())
    page.watch()