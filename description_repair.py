import pywikibot


class repair:

    def __init__(self):
        self.getCounties()

    def getCounties(self):
        query = '''
        PREFIX schema: <http://schema.org/>
SELECT ?human ?humanLabel ?desc WHERE {
  #?human wdt:P31 wd:Q337912.
   ?human wdt:P17 wd:Q213.       

  ?human schema:description ?desc .
      SERVICE wikibase:label { bd:serviceParam wikibase:language "cs" . }
FILTER (CONTAINS(?desc, 'county'))    
    filter (lang(?desc) = "en") .

}

        '''
        from pywikibot.data import sparql
        query_object = sparql.SparqlQuery()
        data = query_object.select(query, full_data=True)
        site = pywikibot.Site('wikidata', 'wikidata')
        repo = site.data_repository()
        if data:
            for r in data:
                human = r['human'].getID()
                item = pywikibot.ItemPage(repo,human)
                item.get()
                newdesc = str(r['desc']).replace('county','District')
                descs = {}
                descs['en'] = newdesc
                item.editDescriptions(descs)



repair()

