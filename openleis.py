#!/usr/bin/python
# -*- coding: utf-8 -*-
import urllib
import json
import csv
import cStringIO
import types
import codecs
import unicodecsv
from logger import Logger


class UnicodeWriter:
    """
    A CSV writer which will write rows to CSV file "f",
    which is encoded in the given encoding.
    """

    def __init__(self, f, dialect=csv.excel, encoding="utf-8", **kwds):
        # Redirect output to a queue
        self.queue = cStringIO.StringIO()
        self.writer = csv.writer(self.queue, dialect=dialect, **kwds)
        self.stream = f
        self.encoder = codecs.getincrementalencoder(encoding)()

    def writerow(self, row):

        list = []

        self.writer.writerow(list)
        # Fetch UTF-8 output from the queue ...
        data = self.queue.getvalue()
        data = data.decode("utf-8")
        # ... and reencode it into the target encoding
        data = self.encoder.encode(data)
        # write to the target stream
        self.stream.write(data)
        # empty queue
        self.queue.truncate(0)

    def writerows(self, rows):
        for row in rows:
            self.writerow(row)

logger = Logger(u'openleis','saved')
pages = 302
country = "CZ"

f = urllib.urlopen("http://openleis.com/legal_entities/search/page/3/registered_country_code/" + country + ".json")
jsondata = f.read()
parsed_json = json.loads(jsondata)
# print parsed_json
dictfirst = parsed_json[0].keys()
# f = UnicodeWriter(open("openleis.csv", "ab+"))
f = unicodecsv.DictWriter(open("openleis.csv", "ab+"), fieldnames=dictfirst)
header_only_once = True
for page in range(300, pages):
    str_page = str(page)
    if not logger.isCompleteFile(str_page):
        print str_page
        webfile = urllib.urlopen("http://openleis.com/legal_entities/search/page/" + str_page + "/registered_country_code/" + country + ".json")
        jsondata = webfile.read()
        parsed_json = json.loads(jsondata)

        # if header_only_once:
        #     f.writerow(parsed_json[0].keys())  # header row
        #     header_only_once = False

        for line in parsed_json:

            # other attributes
            attrs = u""
            for key, value in line[u'other_attributes'].iteritems():
                if type(value) == types.ListType:
                    names = u""
                    for x in value:
                        if len(names) == 0:
                            names = names + x[u"name"]
                        else:
                            names = names + u"," + x[u"name"]
                    new_val = names
                elif type(value) == types.NoneType:
                    new_val = u""
                else:
                    new_val = value

                if len(attrs) == 0:
                    attrs = attrs + key + u":" + new_val
                else:
                    attrs = attrs + u"," + key + u":" + new_val

            line[u'other_attributes'] = attrs

            # other names
            names = u""
            if not type(line[u'other_names']) == types.NoneType:
                for x in line[u'other_names']:
                    if len(names) == 0:
                        names = names + x[u"name"]
                    else:
                        names = names + u"," + x[u"name"]
            line[u'other_names'] = names
            f.writerow(line)

        logger.logComplete(str_page)
