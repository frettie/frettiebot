#!/usr/bin/python
# -*- coding: utf-8 -*-
from time import strftime

import logging
import pandas as pd
import re
import unicodecsv

import pywikibot


class Coordinate:
    pass


class NotHaveBothCoordinates(BaseException):
    def __str__(self):
        msg = self.__getattribute__('message')
        return msg.encode('ascii', 'replace')

    pass


class NotHaveCoordinates(BaseException):
    def __str__(self):
        msg = self.__getattribute__('message')
        return msg.encode('ascii', 'replace')

    pass


class Coordinator:
    printExceptions = False
    name = u'coordinator main'
    category = ''
    pages = list()
    coordinates = list()
    fileName = ''
    categoryWithCoords = ''

    def run(self):
        print("start")
        print(strftime("%Y-%m-%d %H:%M:%S"))
        print("--------")
        try:
            print("start getting pages from category")
            print(strftime("%Y-%m-%d %H:%M:%S"))
            print("--------")
            self.get_pages_from_category()
            print("stop getting pages from category")
            print(strftime("%Y-%m-%d %H:%M:%S"))
            print("--------")
            print("start rotating pages and getting coordinates")
            print(strftime("%Y-%m-%d %H:%M:%S"))
            print("--------")
            self.pages_rotator()
            print("stop rotating pages and getting coordinates")
            print(strftime("%Y-%m-%d %H:%M:%S"))
            print("--------")
            print("start writing csv file")
            print(strftime("%Y-%m-%d %H:%M:%S"))
            print("--------")
            self.csv_write()
            print("stop writing csv file")
            print(strftime("%Y-%m-%d %H:%M:%S"))
            print("--------")
        except ValueError as val:
            print(val)

        print("end")
        print(strftime("%Y-%m-%d %H:%M:%S"))
        print("--------")

    def pages_rotator(self):
        for page in self.pages:
            self.get_coordinates_list_from_page(page)

    def get_coordinates_list_from_page(self, page):
        print(page.title())

    def set_category(self, value):
        self.category = value

    @staticmethod
    def template_to_dict(template):
        template_dict = dict()
        for param in template[1]:
            res = re.split('=', param)
            if len(res) > 1:
                template_dict[res[0]] = res[1]

        return template_dict

    def is_category_with_coordinates(self, template):
        res = re.split(':', template[0].title())
        if res[1] == self.categoryWithCoords:
            return True
        else:
            return False

    def add_coordinate(self, lat, lon, name, page_name):
        coordinate = Coordinate()
        coordinate.lat = lat
        coordinate.lon = lon
        coordinate.name = name
        coordinate.pageName = page_name
        self.coordinates.append(coordinate)

    def get_pages_from_category(self):
        site = pywikibot.Site('cs', 'wikipedia')
        if self.category != '':
            self.pages = pywikibot.Category(site, self.category).articles(recurse=4)
        else:
            raise ValueError('Category is not set')

    def csv_write(self):
        with open(self.fileName, 'w') as csvfile:
            fieldnames = ['lat', 'lon', 'name', 'pageName']
            writer = unicodecsv.DictWriter(csvfile, fieldnames=fieldnames)
            writer.writeheader()
            for coordinate in self.coordinates:
                writer.writerow({'lat': coordinate.lat, 'lon': coordinate.lon, 'name': coordinate.name,
                                 'pageName': coordinate.pageName})


class CultureMonumentCoordinator(Coordinator):
    category = u'Seznamy kulturních památek v Česku'
    categoryWithCoords = u'Památky v Česku'
    actualPage = ''
    fileName = 'culturemonuments.csv'

    @staticmethod
    def have_picture(template_dict):
        try:
            if len(template_dict[u'Obrázek']) > 0:
                return True
            else:
                return False
        except KeyError as ke:
            print(ke)
            return False

    def have_coordinates(self, template_dict):
        try:
            if len(template_dict[u'Zeměpisná_délka']) > 0 and len(template_dict[u'Zeměpisná_šířka']) > 0:
                return True
            else:
                if (
                        len(template_dict[u'Zeměpisná_délka']) > 0 and len(template_dict[u'Zeměpisná_šířka']) == 0
                        or
                        len(template_dict[u'Zeměpisná_délka']) == 0 and len(template_dict[u'Zeměpisná_šířka']) > 0
                ):
                    raise NotHaveBothCoordinates(u'Not both coordinates in page: ' + self.actualPage.title())
                return False
        except KeyError as ke:
            print(ke)
            return False

    def get_coordinates_list_from_page(self, page):
        self.actualPage = page
        print(self.actualPage.title())
        for template in page.templatesWithParams():
            if self.is_category_with_coordinates(template):
                dictated_template = self.template_to_dict(template)
                if not self.have_picture(dictated_template):
                    # not have picture, continue pls
                    try:
                        if self.have_coordinates(dictated_template):
                            # have coords, continue pls
                            self.add_coordinate(dictated_template[u'Zeměpisná_šířka'],
                                                dictated_template[u'Zeměpisná_délka'], dictated_template[u'Název'],
                                                self.actualPage.title())
                        else:
                            raise NotHaveCoordinates(u'Not coordinates of monument ' + dictated_template[
                                u'Název'] + u' in page ' + self.actualPage.title())
                    except NotHaveBothCoordinates as nhbc:
                        if self.printExceptions:
                            print(nhbc)
                    except NotHaveCoordinates as nhc:
                        if self.printExceptions:
                            print(nhc)


class JewishMonumentsCoordinator(Coordinator):
    category = u'Seznamy židovských památek v Česku podle krajů'
    categoryWithCoords = u'Památky v Česku'
    actualPage = ''
    fileName = 'jewishmonuments.csv'

    def get_coordinates_list_from_page(self, page):
        self.actualPage = page
        # print self.actualPage.title()
        # TODO 1F?
        # print self.actualPage.get()
        table = self.grab_only_table()
        table_data = self.get_tables(table)
        # print table_data.T.to_dict()
        for row in table_data.T.to_dict().values():
            have_pics = self.have_pictures(row)
            clean_name = self.clean_name(row)
            print(clean_name + u' : ' + have_pics.__str__())
            if not have_pics:
                coordinates = self.get_coordinates(row)
                if len(coordinates):
                    self.add_coordinate(coordinates['lat'], coordinates['lon'], clean_name, self.actualPage.title())

    @staticmethod
    def clean_name(row):
        cleaned = row[u'Název'].replace('[[', '').replace(']]', '').replace('[', '')
        if row[u'Název'].find('|'):
            return cleaned.split('|')[0].strip()
        else:
            return cleaned.strip()

    @staticmethod
    def get_coordinates(row):
        start_template = row[u'Místo'].find('{{')
        end_template = row[u'Místo'].find('}}')
        if row[u'Místo'].find('}}') > 0:
            coordinates = dict()
            coordinates['lat'] = row[u'Místo'][start_template + 2:end_template].split('|')[1:][0]
            coordinates['lon'] = row[u'Místo'][start_template + 2:end_template].split('|')[1:][1]
            return coordinates
        else:
            return list()

    def have_pictures(self, row):
        # if (row[u'Název'].find('apech') > 0):
        #     print "zh"
        if row[u'Obrázek'].find('<!--') > 0:
            # not have picture
            return False
        else:
            commons_category_name = self.get_commons_category_from_crazy_text(row[u'Galerie Commons'])
            if commons_category_name == '':
                return False

            picture_count = self.get_count_of_commons_pictures(commons_category_name)
            # picture_count = 500
            object_type = row[u'Druh'].replace('[[', '').replace(']]', '').strip()

            if object_type.find('|') > 1:
                object_type = object_type.split('|')[1]

            logging.basicConfig(filename='types.txt', level=logging.INFO)
            logging.info(object_type)

            if object_type == u'Synagoga' or object_type == u'synagoga':
                if picture_count >= 3:
                    return True
                else:
                    return False
            elif object_type == u'Židovský hřbitov' or object_type == u'židovský hřbitov':
                if picture_count >= 7:
                    return True
                else:
                    return False
            elif object_type == u'Židovská škola':
                if picture_count >= 1:
                    return True
                else:
                    return False
            elif object_type == u'Muzeum':
                if picture_count >= 1:
                    return True
                else:
                    return False
            elif object_type == u'Obřadní síň':
                if picture_count >= 1:
                    return True
                else:
                    return False
            elif (
                    object_type == u'Židovská ulice'
                    or object_type == u'Židovská čtvrť'
                    or object_type == u'Židovská radnice'
                    or object_type == u'Modlitebna'
                    or object_type == u'Památník'
            ):
                if picture_count >= 1:
                    return True
                else:
                    return False

            else:
                # jiný druh
                return False

    @staticmethod
    def get_commons_category_from_crazy_text(crazy_text):
        if crazy_text.find('{{') >= 0:
            if crazy_text.find('<!--') >= 0:
                return ''
            else:
                return crazy_text.replace('{{', '').replace('}}', '').split('|')[1]
        else:
            return ''

    @staticmethod
    def get_count_of_commons_pictures(commons_category):
        site = pywikibot.Site('commons', 'commons')
        return pywikibot.Category(site, commons_category).categoryinfo['files']

    def grab_only_table(self):
        cleaned_table = self.actualPage.get().replace(' class="unsortable"|', '')
        start_table = cleaned_table.find('{|')
        end_table = cleaned_table.find('|}')
        return cleaned_table[start_table:end_table + 2]

    @staticmethod
    def get_tables(wiki):
        rows = wiki.split('|-')
        header = []
        table = []
        for i in rows:
            line = i.strip()
            if line.startswith('!'):
                header = line[1:].split('!!')
                for h in range(len(header) - 1):
                    header[h] = header[h].strip().replace('<br />', '')
            elif line.startswith('|') and line.strip() != '|}':
                splitted = line[2:].split('||')
                table.append(splitted)

        data = {}

        for i in range(len(header) - 1):
            col = []
            for row in table:
                col.append(row[i])
            data[header[i]] = col

        df = pd.DataFrame(data)

        return df


coordinator = CultureMonumentCoordinator()
coordinator.run()

coordinatorJew = JewishMonumentsCoordinator()
coordinatorJew.run()
